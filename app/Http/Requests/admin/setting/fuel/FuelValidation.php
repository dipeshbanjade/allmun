<?php

namespace App\Http\Requests\admin\setting\fuel;

use Illuminate\Foundation\Http\FormRequest;

class FuelValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:fuels',
            'nameEng' => 'required|min:4:min:50'
        ];
    }
}
