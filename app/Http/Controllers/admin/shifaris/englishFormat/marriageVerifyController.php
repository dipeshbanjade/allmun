<?php

namespace App\Http\Controllers\admin\shifaris\englishFormat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\shifaris\ApplicantUser;
use App\model\shifaris\MarriageVerify;
use App\Http\Requests\admin\shifaris\marriageVerifyVal;
use App\model\mylogic;
use DB;
use Auth;


class MarriageVerifyController extends Controller
{
  protected $viewPath = 'admin.shifaris.englishFormat.marriageVerify';
  protected $shifarisTableName     = 'marriage_verifies';
  protected $appUsrTable     = 'applicant_users';
  protected $deginationsTbl = 'deginations';
  protected $prefix;


  public function __construct()
  {
    $this->middleware('auth');
    $this->prefix      = config('activityMessage.marVerifyPrefix');

  }

  public function index()
  {
    $page['page_title']       = 'Marriage Verification';
    $page['page_description'] = 'Marriage Verification';

    
    $marriageVerify = ApplicantUser::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

  }

  public function create()
  {
    $page['page_title']       = 'Marriage Verification : create';
    $page['page_description'] = 'Marriage Verification details';

    $getUniqueCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);


    $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationsTbl, $orderColumn = 'nameNep', $orderby = 'ASC');
    return view($this->viewPath . '.create', compact(['page', 'getUniqueCode', 'deginationsId']));

  }

  public function store(marriageVerifyVal $request)
  {

   DB::beginTransaction();
   try {

    //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


    $isShifarisExist = isset($request->shifarisId) ? MarriageVerify::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
    
    $save = !empty($isShifarisExist['id']) ? MarriageVerify::findOrfail($isShifarisExist['id']) : new MarriageVerify;

    $save->createdBy = Auth::user()->id;
    
    $save->refCode  = $request->refCode;

    $save->wards_id  = Auth::user()->wards_id ?? NULL;

    $save->municipilities_id = getMunicipalityData()['id'] ?? "";
    $save->issuedDate  = $request->issuedDate;
    $save->chalaniNum  = $request->chalaniNum;
    $save->letterSub  = $request->letterSub;
    $save->brideName  = $request->brideName;
    $save->bridefatherName  = $request->bridefatherName;
    $save->brideGroomName  = $request->brideGroomName;
    $save->brideGroomfatherName  = $request->brideGroomfatherName; 
    $save->municipality  = $request->municipality;
    $save->ward  = $request->ward;
    $save->dateInBS  = $request->dateInBS;
    $save->dateInAD  = $request->dateInAD;
    $save->authorizedPerson  = $request->authorizedPerson;
    $save->deginations_id  = $request->deginations_id;

    $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

    $mySave = $save->save();

               //end for login
    $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

        // $appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $save->id, $applicantName, $applicantAddress, $citizenshipNumber, $phoneNumber, $email, $dob, $fatherName, $profilePic, $citizenshipImgPath);


                $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
      DB::commit();
      return response()->json([
        'success' => true,
        'message' => config('activityMessage.saveMessage')
      ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
    }else{
      return response()->json([
        'success' => false,
        'message' => config('activityMessage.unSaveMessage')
      ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
    }
  }catch (Exception $e) {
    DB::rollback();
    return back()->withMessage(config('activityMessage.dataNotInserted'));
  }


}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
