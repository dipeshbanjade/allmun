<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class CitizenType extends Model
{
    protected $table = 'citizen_types';
    protected $fillable = ['refCode','nameNep','nameEng','status','softDelete'];
}
