<?php

namespace App\Http\Requests\admin\setting\taxType;

use Illuminate\Foundation\Http\FormRequest;

class TaxTypeValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:3|max:40|unique:tax_types',
            'nameEng' => 'required|min:3|max:30'
        ];
    }
}
