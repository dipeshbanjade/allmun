<?php

namespace App\Http\Controllers\admin\system;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\admin\FieldVisit\FieldVisit;
use App\Http\Requests\admin\fieldVisit\fieldVisitValidation;
use App\model\admin\municipility\StaffMunicipility;
use App\model\mylogic;
use App\model\admin\ward\WardStaff;
use DB;
use Auth;

class fieldVisitController extends Controller
{
    protected $viewPath     = 'admin.system.fieldVisit';
    protected $visitTypeTbl = 'visit_types';
    protected $tbl          = 'field_visits';
    protected $refCode;

    public function __construct()
    {
        $this->middleware('auth');
        $this->refCode  = config('activityMessage.visitTypePrefix');
    }

    public function index()
    {
        $page['page_title']       = 'Field visit list';
        $page['page_description'] = 'field visit list';
        $authUsr  = Auth::user()->userLevel;
        if ($authUsr == 'dev') {
          return redirect()->route('home')->withMessage('Oops your are developer');
        }
        
        if ($authUsr == 'mun') {
            $fieldVisit = FieldVisit::where(['status'=>0, 'softDelete'=> 0, 'staff_municipilities_id' => Auth::user()->staffs_id])->paginate(config('activityMessage.pagination'));
                return view($this->viewPath . '.index', compact(['page', 'fieldVisit']));
        }
        if ($authUsr == 'wrd') {
            $fieldVisit = FieldVisit::where(['status'=>0, 'softDelete'=> 0, 'wards_staff_id' => Auth::user()->staffs_id])->paginate(config('activityMessage.pagination'));
              return view($this->viewPath . '.index', compact(['page', 'fieldVisit']));
        }
        return redirect()->route('home')->withMessage(config('activityMessage.idNotFound'));
    }

    public function adminViewFieldVisit(){
      $page['page_title']       = 'AdminView Daily Report';
      $page['page_description'] = 'Monitor staff  daily report by admin';
      
      $authUsr          = Auth::user()->userLevel;
      /*muncipility*/
      if ($authUsr == 'mun') {
          $fieldVst = FieldVisit::where(['status'=>0, 'softDelete'=> 0])->where('staff_municipilities_id', '<>', '')->paginate(config('activityMessage.pagination'));
          foreach($fieldVst as $staffDept){
            $fieldVst->departments_id = $this->getMunStaffDepartment($staffDept->staff_municipilities_id); //getting staff department id
          }
          return view($this->viewPath . '.adminView', compact(['page', 'fieldVst']));
      }
      /*wards*/
      if ($authUsr == 'wrd') {
          $fieldVst = FieldVisit::where(['status'=>0, 'softDelete'=> 0, 'wards_id' => Auth::user()->wards_id])->where('wards_staff_id', '<>', '')->paginate(config('activityMessage.pagination'));
          return view($this->viewPath . '.adminView', compact(['page', 'fieldVst']));
      }
        return redirect()->route('home')->withMessage(config('Oops your are developer'));
    }

    private function getMunStaffDepartment($id){
      $getDepartment = StaffMunicipility::select('departments_id')->where(['id' => $id])->first();
      return $getDepartment;
      // $deptName = $getDepartment->departments->nameNep . $getDepartment->departments->nameEng;
      // return $deptName = $getDepartment->departments->nameNep . $getDepartment->departments->nameEng;
    }

    public function approveFieldVisit(Request $request){   // approve by admin
      $id        = $request->id;
      if ($request->status == 1) {
            $status = 0;
        }
        if ($request->status == 0) {
             $status = 1;
        }
      $change = FieldVisit::findOrFail($id);
      $change->approveStatus = $status;
      $change->approveBy = Auth::user()->staffs_id;
      $update = $change->update();
      
      if ($update) {
          return MyLogic::successChangeStatus($code = 200);
      }else{
          return MyLogic::idNotFoundMessage($code = 301);
      }

       // $id        = $request->id;
       // $status    = $request->status == 1 ? 0 : 1;
       // $authUsr          = Auth::user()->userLevel;
       // DB::beginTransaction();
       // try {
       //  $change = FieldVisit::findOrFail($id);
       //  $change->approveStatus = $status;
       //  $change->approveBy = Auth::user()->staffs_id;
       //  $update = $change->update();

       //  $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.approveFieldVisit') . $change->refCode, $tableName = $this->tbl, $tblId = $change->id);

       //  if ($update && $log) {
       //      DB::commit();
       //      return back()->withMessage(config('activityMessage.saveMessage') .$change->refCode);
       //      }else{
       //         return back()->withMessage(config('activityMessage.unSaveMessage'));
       //     }
       // }catch (Exception $e) {
       //     DB::rollback();
       //     return back()->withMessage(config('activityMessage.dataNotInserted'));
       // }
    }

    public function create()
    {
        $page['page_title']       = 'Create Field Visit Report';
        $page['page_description'] = 'Create New Field visit report list';

        // $refCode                   = mylogic::sysRefCode($tablename = $this->tbl, $prefix = $this->refCode);

        $fieldVisitId              = mylogic::getDrowDownData($tablename = $this->visitTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');

        return view($this->viewPath . '.create', compact(['page', 'refCode', 'fieldVisitId']));
    }

    public function store(fieldVisitValidation $request)
    {
        // $tblDailyReport = Auth::user()->userLevel == 'mun' ? 'mun_staff_daily_reports' : 'w_r_d_staff_daily_reports';
        $authUsr = Auth::user()->userLevel;
        $refCode = mylogic::sysRefCode($tablename = $this->tbl, $prefix = $this->refCode);
        DB::beginTransaction();
        try {
        // $save  = $authUsr == 'mun' ? new MunStaffDailyReport : new wardStaffDailyReport;

        $save  = new FieldVisit;
        $save->refCode          = $refCode;
        $save->startDate        = $request->startDate;
        $save->startTime        = $request->startTime;

        $save->endDate          = $request->endDate;
        $save->endTime          = $request->endTime;

        $save->visit_types_id   = $request->visit_types_id;

        $save->status           = 0;
        $save->softDelete       = 0;
        $save->shortDesc        = $request->shortDesc;
        $save->createBy         = Auth::user()->id;
        
        if ($authUsr ==='mun') {
          $save->staff_municipilities_id = Auth::user()->staffs_id;
        }
        if ($authUsr ==='wrd') {
          $save->wards_staff_id = Auth::user()->staffs_id;
          $save->wards_id       = Auth::user()->wards_id;
        }
        $mySave = $save->save();

         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createFieldVisit') . $refCode, $tableName = $this->tbl, $tblId = $save->id);

         if ($mySave && $log) {
             DB::commit();
             return redirect()->route('fieldVisit.index')->withMessage(config('activityMessage.saveMessage') .$refCode);
             }else{
                return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $page['page_title']       = 'Edit : field visit';
        $page['page_description'] = 'edit staff field visit';
        $authUsr          = Auth::user()->userLevel;
        $data = FieldVisit::findOrFail($id);
        $fieldVisitId              = mylogic::getDrowDownData($tablename = $this->visitTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        switch ($authUsr) {
          case 'dev':
            return back()->withMessage('Oops your are developer');
            break;
          case 'mun':
            $usr = StaffMunicipility::where(['id' => Auth::user()->staffs_id])->first();
            if ($usr) {
              return view($this->viewPath . '.edit', compact(['page', 'data', 'fieldVisitId']));
            }else{
              return back()->withMessage(config('activityMessage.unAuthorized'));
            }
            break;

           case 'wrd':
             $usr = WardStaff::where(['id' => Auth::user()->staffs_id])->first();
             if ($usr) {
               return view($this->viewPath . '.edit', compact(['page', 'data', 'fieldVisitId']));
             }else{
               return back()->withMessage(config('activityMessage.unAuthorized'));
             }
             break;
          
          default:
            return back()->withMessage(config('activityMessage.idNotFound'));
            break;
        }       
    }

    public function update(Request $request, $id)
    {
        $authUsr          = Auth::user()->userLevel;
        DB::beginTransaction();
        try {
        $upd  = FieldVisit::findOrFail($id);
        $upd->startDate        = $request->startDate;
        $upd->startTime        = $request->startTime;

        $upd->endDate          = $request->endDate;
        $upd->endTime          = $request->endTime;

        $upd->visit_types_id   = $request->visit_types_id;

        $upd->status           = 0;
        $upd->softDelete       = 0;
        $upd->shortDesc        = $request->shortDesc;
        $upd->createBy         = Auth::user()->id;
        
        if ($authUsr ==='mun') {
          $upd->staff_municipilities_id = Auth::user()->staffs_id;
        }
        if ($authUsr ==='wrd') {
          $upd->wards_staff_id = Auth::user()->staffs_id;
          $upd->wards_id       = Auth::user()->wards_id;
        }
        $update = $upd->update();

         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateFieldVisit') . $upd->refCode, $tableName = $this->tbl, $tblId = $upd->id);

         if ($update && $log) {
             DB::commit();
             return redirect()->route('fieldVisit.index')->withMessage(config('activityMessage.updateMessage') .$upd->refCode);
             }else{
                return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
    }

    public function destroy($id)
    {
      if ($id) {
        $del = FieldVisit::findOrFail($id);
         DB::beginTransaction();
        try {
          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.deleteFieldVisit') . $del->refCode, $tableName = $this->tbl, $tblId = $del->id);

          $deleteMe = $del->delete();
          
          if ($deleteMe && $log) {
              DB::commit();
              return redirect()->route('fieldVisit.index')->withMessage(config('activityMessage.deleteMessage'));
              }else{
                 return back()->withMessage(config('activityMessage.unDeleteMessage'));
             }

        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
      }else{
        return back()->withMessage(config('activityMessage.idNotFound'));
      }
    } 
}
