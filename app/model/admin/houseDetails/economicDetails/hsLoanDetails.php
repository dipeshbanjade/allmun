<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class hsLoanDetails extends Model
{
    protected $table = 'hs_loan_details';
}
