<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class Electricity extends Model
{
	protected $table = 'electricities';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
