<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditvehicle']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of vehicle is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmvehicle']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of vehicle is required"
            }
        });
	var btnAddVehicle = $('.btnAddVehicle');
	btnAddVehicle.on('click', function(e){
		$('#vehicleModel').modal('show'); 
	});

	var btnEditvehicle = $('.btnEditvehicle');
	btnEditvehicle.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.vehicleId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editvehicleModel").modal('show');
			}
		})
	});

	var frmUpdatevehicle = $('#frmUpdatevehicle');
  frmUpdatevehicle.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.vehicleId').val();
    var url  = "{{URL::to('/')}}" + "/setting/vehicle/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editvehicleModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editvehicleModel').modal('hide');
});
});
</script>