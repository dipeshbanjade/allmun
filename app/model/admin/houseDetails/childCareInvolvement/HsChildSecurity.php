<?php

namespace App\model\admin\houseDetails\childCareInvolvement;

use Illuminate\Database\Eloquent\Model;

class HsChildSecurity extends Model
{
	protected $table = 'hs_child_securities';
}
