<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class businessLicenseVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:nirman_bebasayas',
            'issuedDate' => 'required',
            'fiscalYear' => 'required', 
            'address' => 'required',
            'companyName' => 'required',
        ];
    }
}
