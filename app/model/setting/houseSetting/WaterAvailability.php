<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class WaterAvailability extends Model
{
	protected $table = 'water_availabilities';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
