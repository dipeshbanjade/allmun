<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffAttendancesTable extends Migration
{

    public function up()
    {
        Schema::create('staff_attendances', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->integer('municipilities_id')->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('staff_municipilities_id')->unsigned();
            $table->foreign('staff_municipilities_id')->references('id')->on('staff_municipilities')->onDelete('cascade');

            $table->integer('wards_staff_id')->unsigned()->nullable();
            $table->foreign('wards_staff_id')->references('id')->on('ward_staffs')->onDelete('cascade'); 

            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('staff_attendances');
    }
}
