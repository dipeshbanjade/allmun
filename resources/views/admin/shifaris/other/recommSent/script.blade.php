<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardrecommSent']").validate({
			rules:{
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 2,
					maxlength : 30
				},
				companyHead : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				companyAddress : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				mailNumber : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				mailDate : {
					required : true,
					date: true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				companyHead : "Company Head is required",
				companyAddress : "Company Address is required",
				mailDate : "Invalid mail date",
				mailNumber : "Mail Number is required",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});
</script>