<?php $checker =  getMunicipalityData(); ?>
<div class="right_col nep" role="main" style="min-height: 521px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">जेट मेशिन सिफारिस</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

         @include('admin.shifaris.municipalityDetail')
         
         <div class="row top-part">
          <div class="col-md-6">
           <p align="left">
            <b>@lang('commonField.extra.refCode'):</b>
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
            @endif
          </p>
          <p align="left">
            <b> च. नं.:
              {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
            </b>
          </p>
        </div>
        <div class="col-md-6">
          <p align="right">
            <b class="mt">मिति :
             {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
           </b>
         </p>
       </div>
     </div>
     <div class="row title-left">
      <div class="col-md-12">
        <p align="left">
          {{ Form::text('companyHead', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyHead', 'required' => 'required']) }}
          ,</p>
          <p align="left">
            {{ Form::text('companyName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyName', 'required' => 'required']) }}।
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
          </p><h4 align="center"><b>विषय: जेट मेशिन उपलब्ध गराउने सिफारिस।</b> </h4>
          {{ Form::hidden('letter_subject', 'जेट मेशिन उपलब्ध गराउने सिफारिस', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
          <p></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 content-para">
          <p align="left">
           <b> 
            {{ Form::text('municipality', isset($data->municipality) ? $data->municipality : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
             

           </b> 
           वडा न.
           {{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }} 
           अन्तर्गत उल्लेखित स्थानमा रहेको 
           <b>
             <select onchange="changeSelect(this)" name="reason">
              <option value="सार्वजनिक">सार्वजनिक</option>
              <option value="निजी">निजी</option>
            </select>
          </b> 
          ढल जाम भएको हुनाले सो स्थानको लागि जेट मेशिन उपलब्ध गराई दिनहुन अनुरोध गरिन्छ।
        </p>
        <p>
          <b> जेट मेसिन उपलब्ध गराउन पर्ने स्थान:
            {{ Form::text('jetReason', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'jetReason', 'required' => 'required']) }}
          </b> 
        </p>
        <p>
          <b>बाटोको नाम : 
            {{ Form::text('roadName', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'roadName', 'required' => 'required']) }}
          </b> 
        </p>
      </div>
      <div class="col-md-6">
        </div>
      </div>
      <div class="text-right btm-last">
        <p>
          {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} 
        </p>
        <p> <b> 
          <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
            @if(count($deginationsId) > 0)
            @foreach($deginationsId as $deg)
            <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
              {{ $deg->nameNep }} 
            </option>
            @endforeach
            @endif
          </select>
        </b> </p>
      </div>
      <!--views for nibedak detail -->
      <div class="clearfix"></div>
      <hr>
      @include('admin.shifaris.nibedakCommonField')
      <!-- END -->
      <div class="col-md-12">
        <hr>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>