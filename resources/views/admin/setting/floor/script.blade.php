<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditfloor']").validate({
            rules:{
                nameEng : {
                    required : true,
                    number: true
                }
            },
            messages: {
                nameEng : "The name of Floor is required and should be numeric"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmfloor']").validate({
            rules:{

                nameEng : {
                    required : true,
                    number: true
                }
            },
            messages: {
          
                nameEng : "The name of Floor is required and should be numeric"
            }
        });
	var btnAddFloor = $('.btnAddFloor');
	btnAddFloor.on('click', function(e){
		$('#floorModel').modal('show'); 
	});

	var btnEditfloor = $('.btnEditfloor');
	btnEditfloor.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.floorId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editfloorModel").modal('show');
			}
		})
	});

	var frmUpdatefloor = $('#frmUpdatefloor');
  frmUpdatefloor.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.floorId').val();
    var url  = "{{URL::to('/')}}" + "/setting/floor/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editfloorModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editfloorModel').modal('hide');
});
});
</script>