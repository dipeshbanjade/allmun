<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class marriedVerifyNepVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:married_verify_neps',
            'chalaniNumber' => 'required|numeric|unique:married_verify_neps',
            
            'husbandGrandFather' => 'required',
            'husbandFather' => 'required',
            'husbandMother' => 'required',
            'husbandAddress' => 'required',
            'husbandSabikAddress' => 'required',
            'husbandName' => 'required',
            'wifeGrandFather' => 'required',
            'wifeFather' => 'required',
            'wifeMother' => 'required',
            'wifeAddress' => 'required',
            'wifeSabikAddress' => 'required',
            'wifeName' => 'required',
            'marriedDate' => 'required',
        ];
    }
}
