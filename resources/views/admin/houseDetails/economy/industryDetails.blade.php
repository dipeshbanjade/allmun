<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
               <!-- 9 -->
                <div class="form-group col-md-12">
                    <p>
                       ९. तपाईको परिवारका कुनै सदस्यले यस महानगरपालिका भित्र उद्योग / व्यवसाय / प्रतिष्ठान संचालन गर्नु भएको छ ?  
                    </p>
                    <div>
                        <label class="radio-inline">
                             १. छ
                            <input type="radio" name="haveIndustryBusiness" value="1" class="rdoClickMe" checked="">
                        </label> 
                        <label class="radio-inline">
                            २. छैन
                            <input type="radio" name="haveIndustryBusiness" value="0" class="rdoClickMe" {{ isset($industryBusinessData->haveIndustryBusiness) && $industryBusinessData->haveIndustryBusiness == '0' ? 'checked' : '' }}>
                        </label>
                    </div>

                    <div class="col-md-12">
                        <p>
                            यदि भएमा परिवार (सवै सदस्यको जोडेर) ले संचालनमा ल्याएका कति वटा छन् ?   
                        </p>
                        <label class="radio-inline">
                            {{ Form::text('industryBusinessNum', isset($industryBusinessData->industryBusinessNum) ? $industryBusinessData->industryBusinessNum : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'वटा', 'id' => '']) }}
                        </label>
                    </div>
                </div>
                <!-- 10 -->
                <div class="form-group col-md-12">
                    <p>
                       १०. तपाईको परिवारले संचालन गरेका प्रतिष्ठान सरकारी निकायमा दर्ता गर्नु भएको छ ? 
                    </p>
                    <div>
                        <label class="radio-inline">
                             १. छ
                            <input type="radio" name="haveRegistered" value="1" checked="">
                        </label> 
                        <label class="radio-inline">
                            २. छैन
                            <input type="radio" name="haveRegistered" value="0" {{ isset($industryBusinessData->haveRegistered) && $industryBusinessData->haveRegistered == '0' ? 'checked' : '' }}>
                             <a href="#pratisthan" style="text-decoration: none">
                                goto ->१४
                            </a>
                             
                        </label>
                    </div>
                    <div class="col-md-12">
                        <p>
                            यदि १ भन्दा बढी प्रतिष्ठान भएमा,
                        </p>
                        <label class="radio-inline">
                            १. दर्ता गरेको संख्या ः
                            {{ Form::text('registeredNum', isset($industryBusinessData->registeredNum) ? $industryBusinessData->registeredNum : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'number', 'id' => '']) }}
                        </label>
                        <label class="radio-inline">
                            २. दर्ता नगरेको संख्या ः
                            {{ Form::text('nonRegisteredNum', isset($industryBusinessData->nonRegisteredNum) ? $industryBusinessData->nonRegisteredNum : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'number', 'id' => '']) }}
                        </label>
                    </div>
                </div>
                <!-- 11 -->
                <div class="form-group col-md-12">
                    <p>
                       ११. तपाईको परिवारले संचालन गरेको व्यवसायको स्थायी लेखा नम्बर लिनु भएको छ ?
                    </p>
                    <div>
                        <label class="radio-inline">
                             १. छ
                            <input type="radio" name="haveAccountNum" value="1" {{ isset($industryBusinessData->haveAccountNum) && $industryBusinessData->haveAccountNum == '1' ? 'checked' : 'checked' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन
                            <input type="radio" name="haveAccountNum" value="0" {{ isset($industryBusinessData->haveAccountNum) && $industryBusinessData->haveAccountNum == '0' ? 'checked' : '' }}>
                            <a href="#pratisthan" style="text-decoration: none">
                               goto ->१४
                            </a>
                        </label>
                    </div>
                </div>
                <!-- 12 -->
                <?php
        $jsonIndBusiness = isset($industryBusinessData->govOfficeRegDetail) ? json_decode($industryBusinessData->govOfficeRegDetail, true) : '';
                //dd($agrProdArr);
        ?> 
                <div class="form-group col-md-12">
                    <p>
                       १२. तपाईको परिवारले संचालन गरेको प्रतिष्ठान / व्यवसाय कुन कुन सरकारी निकायमा दर्ता छ ? 
                    </p>
                    <div>
                        <label class="radio-inline">
                            <input type="checkbox" name="govOfficeRegDetail[घरेलु तथा साना उद्योग कार्यालय / शाखा]" value="घरेलु तथा साना उद्योग कार्यालय / शाखा" {{ isset($jsonIndBusiness['घरेलु तथा साना उद्योग कार्यालय / शाखा']) ? 'checked' : '' }}>
                             (क) घरेलु तथा साना उद्योग कार्यालय / शाखा
                        </label><br /> 
                        <label class="radio-inline">
                            <input type="checkbox" name="govOfficeRegDetail[कम्पनी रजिष्टारको कार्यालय]" value="कम्पनी रजिष्टारको कार्यालय" {{ isset($jsonIndBusiness['कम्पनी रजिष्टारको कार्यालय']) ? 'checked' : '' }}>
                            (ख) कम्पनी रजिष्टारको कार्यालय 
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="govOfficeRegDetail[उद्योग विभाग]" value="उद्योग विभाग" {{ isset($jsonIndBusiness['उद्योग विभाग']) ? 'checked' : '' }}>
                            (ग) उद्योग विभाग 
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="govOfficeRegDetail[वाणिज्य विभाग]" value="वाणिज्य विभाग" {{ isset($jsonIndBusiness['वाणिज्य विभाग']) ? 'checked' : '' }}>
                            (घ) वाणिज्य विभाग 
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="govOfficeRegDetail[जिल्ला समन्वय समिति]" value="जिल्ला समन्वय समिति" {{ isset($jsonIndBusiness['जिल्ला समन्वय समिति']) ? 'checked' : '' }}>
                            (ङ) जिल्ला समन्वय समिति
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="govOfficeRegDetail[पो.ले.महानगरपालिका]" value="पो.ले.महानगरपालिका" {{ isset($jsonIndBusiness['पो.ले.महानगरपालिका']) ? 'checked' : '' }}>
                            (च) पो.ले.महानगरपालिका 
                        </label><br />
                        <label class="radio-inline">
                            {{ Form::text('otherGovOfficeReg', isset($industryBusinessData->otherGovOfficeReg) ? $industryBusinessData->otherGovOfficeReg : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (उल्लेख गर्ने)', 'id' => '']) }}
                        </label>
                    </div>
                </div>
                <!-- 14 -->
                <div class="form-group col-md-12">
                    <p>
                      १३. तपाईले संचालन गर्नु भएको प्रतिष्ठान / व्यवसाय / फर्म तथा कम्पनीको लेखा / श्रेस्ता राख्ने गरेको छ ?  
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" name="haveAccountRecord" value="1"  {{ isset($industryBusinessData->haveAccountNum) && $industryBusinessData->haveAccountNum == '1' ? 'checked' : 'checked' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" name="haveAccountRecord" value="0"  {{ isset($industryBusinessData->haveAccountNum) && $industryBusinessData->haveAccountNum == '0' ? 'checked' : '' }}> 
                        </label>
                    </div>
                </div>
            </div>
            <div class="msgDisplay"></div>
        
    </div>
</div>