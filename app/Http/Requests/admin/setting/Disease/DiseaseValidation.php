<?php

namespace App\Http\Requests\admin\setting\Disease;

use Illuminate\Foundation\Http\FormRequest;

class DiseaseValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'nameEng' => 'required|min:3|max:50',
            'tag'   => 'required'
            
        ];
    }
}
