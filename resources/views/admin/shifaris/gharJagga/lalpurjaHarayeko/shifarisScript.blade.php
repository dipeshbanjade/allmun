<script type="text/javascript">

	$(function(){
		/*add buttoin*/
		$("form[name='formLalpurjaHarayeko']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wards_id : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number : true
				},
				municipilities_id : {
					required : true,
					minlength : 1,
					maxlength : 30
				},
				issuedDate : {
					required : true					
				},
				chalaniNumber : {
					required : true,
					minlength : 1,
					maxlength : 30
				},
				accountHolderName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				accountHolderAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				applicantName1 : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber1 : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number : true
				},
				address1 : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				kittaNumber : {
					required : true,
					minlength : 1,
					maxlength : 30
				},
				area : {
					required : true,
					minlength : 1,
					maxlength : 10
				},
				wardNumber2 : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number : true
				},
				address2 : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				applicantName2 : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				applicant : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				citizenshipNumber : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDate2 : {
					required : true
				},
				fatherName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				grandFatherName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}

			},
			messages: {


				refCode : "Ref Code is required",
				wards_id : "Wards Id is required",
				municipilities_id : "Municipality Id is required",
				issuedDate : "Issued Date is required",
				chalaniNumber : "Chalini Number is required",
				accountHolderName : "Account Holder Name is required",
				accountHolderAddress : "Account Holder Address is required",
				applicantName1 : "Applicant Name is required",
				municipalityName : "Municipality Valid is required",
				wardNumber1 : "Ward Number is required and Numeric",
				address1 : "Address is required",
				kittaNumber : "Kitta Number is required",
				area : "Area is required",
				wardNumber2 : "Ward Number is required",
				address2 : "Address is required",
				applicantName2 : "Applicant Name is required",
				applicant : "Applicant is required",
				citizenshipNumber : "Citizenship Number is required",
				issuedDate2 : "Issued Date is required",
				fatherName : "Father Name is required",
				grandFatherName : "Grand Father Name  is required",
				authorizedPerson : "Authorized Person is required",
				deginations_id : "Degination Id is required"

			}
		});
});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='applicant']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	                  $("input[name='citizenshipNo']").val(response.citizenNo).trigger('input');
	                  $("input[name='issuedDate2']").val(response.citizenshipIssueDate).trigger('input');

	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>
