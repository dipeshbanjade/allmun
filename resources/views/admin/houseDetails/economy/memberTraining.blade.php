<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row color_red">
     <!-- 15-->
     <div class="form-group col-md-12">
      <p>
       १५. परिवारका कुनै सदस्यले विगत ३ बर्षमा व्यवसायिक सीप तालिम प्राप्त गरेका छन ?
     </p>
     <div class="col-md-12">
      <?php 
      $retMemData = (isset($memTrain) ? (array)$memTrain : '');
      $memTrainJsonData = isset($retMemData['hsMemberTraining']) ? json_decode($retMemData['hsMemberTraining'], true) : '';  

      //dd($memTrainJsonData);     

      ?>
      <label class="radio-inline">
        १. <?php echo config('variable.common.have')   ?>
        <input type="radio" name="haveMemberTraining" value="1" checked>
      </label> 
      <label class="radio-inline">
        २. <?php echo config('variable.common.donHave')   ?>
        <input type="radio" name="haveMemberTraining" value="0" {{ isset($retMemData['havememberTraining']) && $retMemData['havememberTraining'] == '0' ? 'checked' : '' }}>
      </label>
      <br>
      यदि भए सोको विवरण दिनुहोस् । 
    </div>
    <div class="col-md-12">
      <table class="table">
        <tr>
          <th></th>
          <th>सीप विकासका क्षेत्रहरु</th>
          <th>महिला</th>
          <th>पुरुष</th>
        </tr>
        <tr>
          <td>
            <input type="checkbox" name="training[CEMRC][status]" value="1" {{ isset($memTrainJsonData['CEMRC']) && $memTrainJsonData['CEMRC']['status'] == 1 ? 'checked' : '' }}>
          </td>
          <td>
            <?php echo config('variable.memberTraining.CEMRC') ?>   
          </td>
          <td>
            {{ Form::number('training[CEMRC][female]', isset($memTrainJsonData['CEMRC']) ? $memTrainJsonData['CEMRC']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'number']) }}
          </td>
          <td>
            {{ Form::number('training[CEMRC][male]',  isset($memTrainJsonData['CEMRC']) ? $memTrainJsonData['CEMRC']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'number']) }}
          </td>
        </tr>
        <tr>
          <td>
            <input type="checkbox" name="training[womanTraining][status]" value="1" {{ isset($memTrainJsonData['womanTraining']) && $memTrainJsonData['womanTraining']['status'] == 1 ? 'checked' : '' }}>
          </td>
          <td>
           <!-- सिलाई बुनाई, बुटिक, सृंगार, पार्लर आदि  -->
           <?php echo config('variable.memberTraining.womanTraining') ?>   

         </td>
         <td>
          {{ Form::number('training[womanTraining][female]', isset($memTrainJsonData['womanTraining']) ? $memTrainJsonData['womanTraining']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
        </td>
        <td>
          {{ Form::number('training[womanTraining][male]', isset($memTrainJsonData['womanTraining']) ? $memTrainJsonData['womanTraining']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
        </td>
      </tr>
      <tr>
        <td>
          <input type="checkbox" name="training[building][status]" value="1" {{ isset($memTrainJsonData['building']) && $memTrainJsonData['building']['status'] == 1 ? 'checked' : '' }}>
        </td>
        <td>
         <?php 
         echo config('variable.memberTraining.building') 
         ?>  
       </td>
       <td>
        {{ Form::number('training[building][female]', isset($memTrainJsonData['building']) ? $memTrainJsonData['building']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
      </td>
      <td>
        {{ Form::number('training[building][male]', isset($memTrainJsonData['building']) ? $memTrainJsonData['building']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
      </td>
    </tr>
    <tr>
      <td>
        <input type="checkbox" name="training[engMotor][status]" value="1" {{ isset($memTrainJsonData['engMotor']) && $memTrainJsonData['engMotor']['status'] == 1 ? 'checked' : '' }}>
      </td>

      <td>
       <?php 
       echo config('variable.memberTraining.engMotor') 
       ?>
       <!-- इञ्जिनियरिङ्ग, अटोमोवाइल र मेकानिक्स         -->
     </td>
     <td>
      {{ Form::number('training[engMotor][female]', isset($memTrainJsonData['engMotor']) ? $memTrainJsonData['engMotor']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
    </td>
    <td>
      {{ Form::number('training[engMotor][male]', isset($memTrainJsonData['engMotor']) ? $memTrainJsonData['engMotor']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
    </td>
  </tr>
  <tr>
    <td>
      <input type="checkbox" name="training[agriSector][status]" value="1" {{ isset($memTrainJsonData['agriSector']) && $memTrainJsonData['agriSector']['status'] == 1 ? 'checked' : '' }}>
    </td>

    <td>
     <?php 
     echo config('variable.memberTraining.agriSector') 
     ?>

     <!--  कृषि सम्बन्धी (जेटी, जेटीए र खाद्य प्रशोधन आदि)  -->         
   </td>
   <td>
    {{ Form::number('training[agriSector][female]', isset($memTrainJsonData['agriSector']) ? $memTrainJsonData['agriSector']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
  </td>
  <td>
    {{ Form::number('training[agriSector][male]', isset($memTrainJsonData['agriSector']) ? $memTrainJsonData['agriSector']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
  </td>
</tr>
<tr>
  <td>
    <input type="checkbox" name="training[pubHealth][status]" value="1" {{ isset($memTrainJsonData['pubHealth']) && $memTrainJsonData['pubHealth']['status'] == 1 ? 'checked' : '' }}>
  </td>

  <td>
   <?php 
   echo config('variable.memberTraining.pubHealth') 
   ?>

   <!-- जनस्वास्थ्य सम्बन्धी           -->
 </td>
 <td>
  {{ Form::number('training[pubHealth][female]', isset($memTrainJsonData['pubHealth']) ? $memTrainJsonData['pubHealth']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
<td>
  {{ Form::number('training[pubHealth][male]', isset($memTrainJsonData['pubHealth']) ? $memTrainJsonData['pubHealth']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
</tr>
<tr>
  <td>
    <input type="checkbox" name="training[AnimalHealth][status]" value="1" {{ isset($memTrainJsonData['AnimalHealth']) && $memTrainJsonData['AnimalHealth']['status'] == 1 ? 'checked' : '' }}>
  </td>

  <td>
   <?php 
   echo config('variable.memberTraining.AnimalHealth') 
   ?>
   <!-- पशुस्वास्थ्य सम्बन्धी           -->
 </td>
 <td>
  {{ Form::number('training[AnimalHealth][female]', isset($memTrainJsonData['AnimalHealth']) ? $memTrainJsonData['AnimalHealth']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
<td>
  {{ Form::number('training[AnimalHealth][male]', isset($memTrainJsonData['AnimalHealth']) ? $memTrainJsonData['AnimalHealth']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
</tr>
<tr>
  <td>
    <input type="checkbox" name="training[forestry][status]" value="1" {{ isset($memTrainJsonData['forestry']) && $memTrainJsonData['forestry']['status'] == 1 ? 'checked' : '' }}>
  </td>

  <td>
   <?php 
   echo config('variable.memberTraining.forestry') 
   ?>
   <!-- वन सम्बन्धी           -->
 </td>
 <td>
  {{ Form::number('training[forestry][female]', isset($memTrainJsonData['forestry']) ? $memTrainJsonData['forestry']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
<td>
  {{ Form::number('training[forestry][male]', isset($memTrainJsonData['forestry']) ? $memTrainJsonData['forestry']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
</tr>
<tr>
  <td>
    <input type="checkbox" name="training[tourism][status]" value="1" {{ isset($memTrainJsonData['tourism']) && $memTrainJsonData['tourism']['status'] == 1 ? 'checked' : '' }}>
  </td>

  <td>
   <?php 
   echo config('variable.memberTraining.tourism') 
   ?>
   <!-- पर्यटन टुर गाइड, ट्राभल र सत्कार         -->
 </td>
 <td>
  {{ Form::number('training[tourism][female]', isset($memTrainJsonData['tourism']) ? $memTrainJsonData['tourism']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
<td>
  {{ Form::number('training[tourism][male]', isset($memTrainJsonData['tourism']) ? $memTrainJsonData['tourism']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
</tr>
<tr>
  <td>
    <input type="checkbox" name="training[arts][status]" value="1" {{ isset($memTrainJsonData['arts']) && $memTrainJsonData['arts']['status'] == 1 ? 'checked' : '' }}>
  </td>

  <td>
   <?php 
   echo config('variable.memberTraining.arts') 
   ?>
   <!-- कला सम्बन्धी           -->
 </td>
 <td>
  {{ Form::number('training[arts][female]', isset($memTrainJsonData['arts']) ? $memTrainJsonData['arts']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
<td>
  {{ Form::number('training[arts][male]', isset($memTrainJsonData['arts']) ? $memTrainJsonData['arts']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
</tr>
<tr>
  <td>
    <input type="checkbox" name="training[other][status]" value="1" {{ isset($memTrainJsonData['other']) && $memTrainJsonData['other']['status'] == 1 ? 'checked' : '' }}>
  </td>

  <td>
   <?php 
   echo config('variable.common.other') 
   ?>
   <!-- अन्य            -->
 </td>
 <td>
  {{ Form::number('training[other][female]', isset($memTrainJsonData['other']) ? $memTrainJsonData['other']['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
<td>
  {{ Form::number('training[other][male]', isset($memTrainJsonData['other']) ? $memTrainJsonData['other']['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'']) }}
</td>
</tr>
</table>
</div>
</div>
</div>
<div class="msgDisplay"></div>
</div>
</div>