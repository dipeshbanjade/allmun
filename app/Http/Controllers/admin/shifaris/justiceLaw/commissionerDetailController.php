<?php

namespace App\Http\Controllers\admin\shifaris\justiceLaw;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\CommissionerDetail;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\commissionerDetailVal;
use DB;
use Auth;

use App\model\mylogic;

class CommissionerDetailController extends Controller
{
    protected $viewPath = 'admin.shifaris.justiceLaw.commissionerDetail';
    protected $shifarisTableName     = 'commissioner_details';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;
    protected $refCode;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixCommissionerDetail');
        $this->refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

    }

    public function index()
    {
        $page['page_title']       = 'Commissioner Detail Recommendation';
        $page['page_description'] = 'Commissioner Detail Recommendation';

        //TODO accordkign to ward user dispaly recored
        $instRecomd = CommissionerDetail::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        
    }

    public function create()
    {
        $page['page_title']       = 'Commissioner Detail Recommendation : create';
        $page['page_description'] = 'Commissioner Detail Recommendation details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);
        
        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(commissionerDetailVal $request)
    {
        
     DB::beginTransaction();
     try {

        $isShifarisExist = isset($request->shifarisId) ? CommissionerDetail::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
        $save = !empty($isShifarisExist['id']) ? CommissionerDetail::findOrfail($isShifarisExist['id']) : new CommissionerDetail;

        $save->createdBy = Auth::user()->id;

        $save->refCode = $this->refCode;

        $save->wards_id = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";
        
        $save->issuedDate = $request->issuedDate;
        $save->chalaniNum = $request->chalaniNum;
        $save->municipalityName1  = $request->municipalityName1;
        $save->municipalityName2  = $request->municipalityName2;
        $save->wardNumber = $request->wardNumber;
        $save->badi = $request->badi;
        $save->pratibadi = $request->pratibadi;
        $save->caseName = $request->caseName;
        $save->complaintRequest = $request->complaintRequest;
        $save->writtenAnswer = $request->writtenAnswer;
        $save->agreedOn = $request->agreedOn;
        $save->dafaNumber = $request->dafaNumber;
        $save->municipalityName3 = $request->municipalityName3;
        $save->badiWard = $request->badiWard;
        $save->badiName = $request->badiName;
        $save->municipalityName4 = $request->municipalityName4;
        $save->pratibadiWard = $request->pratibadiWard;
        $save->pratibadiName = $request->pratibadiName;
        $save->year = $request->year;
        $save->month = $request->month;
        $save->day = $request->day;
        $save->eachday = $request->eachday;

        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

        $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
        if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }

  
}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
