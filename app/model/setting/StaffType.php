<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class StaffType extends Model
{
    protected $table = 'staff_types';
    protected $fillable = ['nameNep', 'nameEng', 'status', 'softDelete'];
}
