@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
	       <i class="fa fa-plus"></i>
	 </div>

	 <div class="card-head">
            <header>Daily Sarash Report</header>
	           {{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	         </div>

     {{-- 
        
        <div class="right_col dash-pg" role="main" style="min-height: 900px;">
  <div class="clearfix"></div>
  <div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">वडागत सारश रिपोर्ट </h3>
          
          <div class="clearfix"></div>
        </div>

        <div class="x_content" id="block">

          <form class="form-inline" id="palika_daily_summary_report_filter_form">
            <fieldset>
              <legend></legend>


              <div class="form-group mx-sm-3 mb-2">
                <label for="text">वार्ड &nbsp;</label>


                <select onchange="changeSelect(this)" name="ward_no" class="form-control" id="ward_no" required="required">
<option value="" selected="selected">---वार्ड छान्नुहोस---</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>
              </div>

              <div class="form-group mx-sm-3 mb-2">
                <label for="text"> विषयगत प्रकार &nbsp;</label>


                <select onchange="changeSelect(this)" name="main_sifaris" class="form-control" id="main_sifaris">
<option value="" selected="selected">---मुख्य छान्नुहोस---</option>
<option value="1">घर / जग्गा जमिन</option>
<option value="2">संघ सस्था</option>
<option value="3">ब्यबसाय / ब्यबसाय</option>
<option value="4">English Format</option>
<option value="5">शैक्षिक</option>
<option value="6">आर्थिक</option>
<option value="7">समाजिक / पारिवारिक</option>
<option value="8">भौतिक निर्माण</option>
<option value="9">सूचना तथा संचार</option>
<option value="10">न्याय / कानुन</option>
<option value="11">अन्य</option>
<option value="12">राष्ट्रिय पञ्जीकरण</option>
<option value="13">बैंक / वित्तीय संस्था </option>
<option value="15">खुल्ला ढाँचा</option>
</select>
              </div>

              <div class="form-group mx-sm-2 mb-2">
                <label for="text">मिति &nbsp;</label>
                <input type="text" class="form-control ndp-nepali-calendar date-input-field" id="from_date" name="from_date" onfocus="showNdpCalendarBox('from_date')"> 
              </div>
              <div class="form-group mx-sm-2 mb-2">
                <label for="text">देखि &nbsp;</label>

                <input type="text" class="form-control ndp-nepali-calendar date-input-field" id="to_date" name="to_date" onfocus="showNdpCalendarBox('to_date')"> 
                <label for="text">सम्म &nbsp;</label>
              </div>










              <div class="form-group mx-sm-3 mb-2">
                <button type="submit" class="  btn btn-primary bttn"><span class="glyphicon glyphicon-search"></span> खोज्नुहोस</button>
              </div>
            </fieldset>
          </form>

          <hr>


          
          <div id="palika_daily_summary_report_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dt-buttons btn-group"><a class="btn btn-default buttons-copy buttons-html5" tabindex="0" aria-controls="palika_daily_summary_report" href="#"><span>Copy</span></a><a class="btn btn-default buttons-csv buttons-html5" tabindex="0" aria-controls="palika_daily_summary_report" href="#"><span>CSV</span></a><a class="btn btn-default buttons-excel buttons-html5" tabindex="0" aria-controls="palika_daily_summary_report" href="#"><span>Excel</span></a><a class="btn btn-default buttons-print" tabindex="0" aria-controls="palika_daily_summary_report" href="#"><span>Print</span></a></div><div id="palika_daily_summary_report_processing" class="dataTables_processing panel panel-default" style="display: none;">Processing...</div><table class="table table-bordered table-striped dataTable no-footer" id="palika_daily_summary_report" role="grid" aria-describedby="palika_daily_summary_report_info">
            <caption id="caption">ईलाम नगरपालिका सबै वडा मा आर्थिक वर्ष २०७५/०७६ मा  मिति२०७५-०५-२१ देखि२०७५-०५-२१ सम्मको जारी सिफारिस रिपोर्ट</caption>

            <thead>
              <tr role="row"><th class="sorting" tabindex="0" aria-controls="palika_daily_summary_report" rowspan="1" colspan="1" aria-label="क्र.स.: activate to sort column ascending">क्र.स.</th><th class="sorting" tabindex="0" aria-controls="palika_daily_summary_report" rowspan="1" colspan="1" aria-label="विषयगत सेवा : activate to sort column ascending">विषयगत सेवा </th><th class="sorting" tabindex="0" aria-controls="palika_daily_summary_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="palika_daily_summary_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="palika_daily_summary_report" rowspan="1" colspan="1" aria-label="कैफियत : activate to sort column ascending">कैफियत </th></tr>
             
            </thead>
            <tbody>

            <tr class="odd"><td valign="top" colspan="5" class="dataTables_empty">No data available in table</td></tr></tbody>
          </table><div class="dataTables_info" id="palika_daily_summary_report_info" role="status" aria-live="polite">Showing 0 to 0 of 0 entries</div></div>
        </div>
      </div>
    </div>
  </div>
</div>
        
        
        --}}
	 </div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
	 @include('admin.shifaris.script')
@endsection