<div class="row top-part">
  <div class="col-md-6">
   <p align="left">
     <b class="ps">पत्र संख्याः
      @if(isset($data->refCode))
      {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
      @else
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
      @endif
    </b>
    
  </p>
  <p align="left">
   <b> च. नं.:
     {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
   </b>
 </p>
</div>
<div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    <h4 align="center">
      <b>विषय: <u>स्थायी बसोबास सिफारिस</u> ।</b>
    </h4>
  </div>
</div>

<div class="row">
  <div class="col-md-12 content-para">
    <p>निवेदक श्री/सुश्री/श्रीमती 
      <b>

        {{ Form::text('residentApplicant', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'residentApplicant', 'required' => 'required']) }}
      </b> 
      ले पेश गर्नुभएको निबेदनका आधारमा निज श्री/सुश्री/श्रीमती
      {{ Form::text('nijName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'nijName', 'required' => 'required']) }}

      {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
      महा/उपमाहा/नगरपालिका/गाउँपालिका  वडा नं. 
      <b>
        {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
      </b>
      (साबिकको ठेगाना 
      <!--  -->
      {{ Form::text('sabikAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'sabikAddress', 'required' => 'required']) }}
      <!--  -->)
      अन्तर्गत तल उल्लेखित स्थानमा विगत मिति 
      {{ Form::text('bigatDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'bigatDate',  'onfocus' => 'showNdpCalendarBox("bigatDate")']) }}
      देखि स्थायी बसोबास गर्दै आउनु भएको व्यहोरा सिफरिस साथ अनुरोध गरिन्छ ।
    </p> 
    <p style="font-size: 14px;"><i>
     बसोबास गर्नेको ना. प्र. प. नं.
     {{ Form::text('nagarikata', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'nagarikata', 'required' => 'required']) }}
     जिल्ला - 
     {{ Form::text('residentDistrict', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'residentDistrict', 'required' => 'required']) }}
     / जरीमिति -
     {{ Form::text('issuedDateTwo', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDateTwo',  'onfocus' => 'showNdpCalendarBox("issuedDateTwo")']) }}.
   </i></p>
 </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center"><b><u>बसोबास स्थान</u></b></p>
    <div class="table-responsive">
      <table class="table table-bordered" id="residentTable">
        <thead>
          <tr><th>क्र.स.</th>
            <th>टोल</th>
            <th>बाटोको नाम</th>
            <th>घर नं.</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $count = 1;
          $tableData = !empty($data) ? json_decode($data->tableData, true) : [''];
          //dd($tableData);
          foreach($tableData as $singleData){         
            ?>
            <tr>
              <td>
                {{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
              </td>
              <td>
                {{ Form::text('tole[]', !empty($singleData['tole']) ? $singleData['tole'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
              </td>
              <td>
                {{ Form::text('streetName[]', !empty($singleData['streetName']) ? $singleData['streetName'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
              </td>
              <td>
                {{ Form::text('houseNo[]', !empty($singleData['houseNo']) ? $singleData['houseNo'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
              </td>

              @if($count == 2)
              
              <td class="add-btns" style="border: 0px;">
                <button type="button" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button>
              </td>
              @else
              <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
              @endif

              <!-- <td><a href="" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></a></td> -->
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="text-right btm-last">
    <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required"></p>
    <p> 
      <b>
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b>
    </p>
  </div>
  <!--views for nibedak detail -->
  <div class="clearfix"></div>
  <hr>
  @include('admin.shifaris.nibedakCommonField')
  <!-- END -->
  <div class="col-md-12">
    <hr>
  </div>