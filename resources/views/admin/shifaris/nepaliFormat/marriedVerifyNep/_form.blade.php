<div class="row top-part">
	<div class="col-md-6">
		<p align="left">
			<b>पत्र संख्य
				@if(isset($data->refCode))
				{{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
				@else
				{{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
				@endif
			</b>

		</p>
		<p align="left">
			<b> च. नं.:
				{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
			</b>
		</p>
	</div>
	<div class="col-md-6">
		<p align="right">
			<b class="mt">मिति :
				{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
			</b>
		</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<p align="center" class="font-size-24"></p>

		<h4 align="center">
			<b>बिषय: विवाहित प्रमाणित</b>
		</h4>
		{{ Form::hidden('letter_subject', 'खाता बन्द गरिदिने सम्बन्धमा', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
	</div>
</div>



<div class="row">
	<div class="col-md-12 content-para">
		<p>श्री
			<b>
				{{ Form::text('husbandGrandFather', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'husbandGrandFather', 'required' => 'required']) }}				
			</b> 
			को नाती श्री
			<b>
				{{ Form::text('husbandFather', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'husbandFather', 'required' => 'required']) }}
			</b>
			तथा श्रीमती
			<!--  -->
			{{ Form::text('husbandMother', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'husbandMother', 'required' => 'required']) }}
			<!--  -->)
			को छोरा 
			<!--  -->
			{{ Form::text('husbandAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'husbandAddress', 'required' => 'required']) }}
			<!--  -->
			(साबिकको ठेगाना
			<!--  -->
			{{ Form::text('husbandSabikAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'husbandSabikAddress', 'required' => 'required']) }}
			<!--  -->
			) निवासी श्री 
			{{ Form::text('husbandName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'husbandName', 'required' => 'required']) }}
			र श्री 
			{{ Form::text('wifeGrandFather', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wifeGrandFather', 'required' => 'required']) }}
			को नातिनी श्री

			{{ Form::text('wifeFather', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wifeFather', 'required' => 'required']) }}
			तथा श्रीमती
			{{ Form::text('wifeMother', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wifeMother', 'required' => 'required']) }}
			को छोरी,
			{{ Form::text('wifeAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wifeAddress', 'required' => 'required']) }}
			(साबिकको ठेगाना 
			{{ Form::text('wifeSabikAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wifeSabikAddress', 'required' => 'required']) }}
			)
			निवासी सुश्री
			{{ Form::text('wifeName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wifeName', 'required' => 'required']) }}
			बीच मिति:
			{{ Form::text('marriedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'marriedDate',  'onfocus' => 'showNdpCalendarBox("marriedDate")']) }}
			मा विवाह भएको व्यहोरा प्रमाणित गरिन्छ 
		</p> 
	</div>
</div>
<div class="text-right btm-last">
	<p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required" value="{{!empty($data) ? $data->authorizedPerson : ''}}"></p>
	<p> 
		<b>
			<select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
				@if(count($deginationsId) > 0)
				@foreach($deginationsId as $deg)
				<option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
					{{ $deg->nameNep }} 
				</option>
				@endforeach
				@endif
			</select>
		</b>
	</p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
	<hr>
</div>