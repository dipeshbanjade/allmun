<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row color_red">
           <!-- 4 -->
           <div class="form-group col-md-12">
            <p>
             ४. तपाईको परिवारले कुनै चौपाया तथा पंक्षी पाल्नु भएको छ ? 
         </p>
         <div class="col-md-12">
            <label class="radio-inline">
                १. छ 
                <input type="radio" name="haveHusbandry" value="1" checked="">
            </label> 
            <label class="radio-inline">
                २. छैन्
                <input type="radio" name="haveHusbandry" value="0" {{ isset($animalProduct->haveHusbandry) && $animalProduct->haveHusbandry == '0' ? 'checked' : '' }} >
                Goto -> ८.६
            </label>
        </div>
    </div>
    <!-- 5 -->
    <div class="form-group col-md-12">
        <p>
         ५. यदि छ भने, चौपाया तथा पशुपंक्षीहरुको विवरण दिनुहोस (संख्या उल्लेख गर्ने) । 
     </p>
     <div class="col-md-12">
        <?php $count = 0;                                 
        $animalProdArr = isset($animalProduct->hsAnimalProDetails) ? json_decode($animalProduct->hsAnimalProDetails, true) : '';
                //dd($agrProdArr);
        ?> 
        <table class="table">            
            <tr>
                <th>क्र.सं</th>
                <th>चौपाया तथा पशुपंक्षी</th>
                <th>संख्या</th>
                <th>दुध उत्पादन(लिटर)</th>
                <th>हाड / छाला(के.जि.)</th>
                <th>अन्य</th>
                <th>विक्रीबाट बार्षिक आम्दानी (रु.)</th>
            </tr>
            <tr>
                <td>१.</td>
                <td>गाई </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गाई][संख्या]', isset($animalProdArr['गाई']['संख्या']) ? $animalProdArr['गाई']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गाई][दुध उत्पादन(लिटर)]',  isset($animalProdArr['गाई']['दुध उत्पादन(लिटर)']) ? $animalProdArr['गाई']['दुध उत्पादन(लिटर)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गाई][हाड / छाला(के.जि.)]',  isset($animalProdArr['गाई']['हाड / छाला(के.जि.)']) ? $animalProdArr['गाई']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गाई][अन्य]',  isset($animalProdArr['गाई']['अन्य']) ? $animalProdArr['गाई']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गाई][विक्रीबाट बार्षिक आम्दानी (रु.)]',  isset($animalProdArr['गाई']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['गाई']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>


            </tr>
            <tr>
                <td>२.</td>
                <td> भैसी </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भैसी][संख्या]', isset($animalProdArr['भैसी']['संख्या']) ? $animalProdArr['भैसी']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भैसी][दुध उत्पादन(लिटर)]', isset($animalProdArr['भैसी']['दुध उत्पादन(लिटर)']) ? $animalProdArr['भैसी']['दुध उत्पादन(लिटर)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भैसी][हाड / छाला(के.जि.)]', isset($animalProdArr['भैसी']['हाड / छाला(के.जि.)']) ? $animalProdArr['भैसी']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भैसी][अन्य]', isset($animalProdArr['भैसी']['अन्य']) ? $animalProdArr['भैसी']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भैसी][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['भैसी']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['भैसी']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>



            </tr>
        </table>

        <table class="table">
            <tr>
                <th>क्र.सं</th>
                <th>चौपाया तथा पशुपंक्षी</th>
                <th>संख्या</th>
                <th>मासु उत्पादन(के.जि.)</th>
                <th>हाड / छाला(के.जि.)</th>
                <th>अन्य</th>
                <th>विक्रीबाट बार्षिक आम्दानी (रु.)</th>
            </tr>
            <tr>
                <td>१.</td>
                <td>गोरु </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गोरु][संख्या]', isset($animalProdArr['गोरु']['संख्या']) ? $animalProdArr['गोरु']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गोरु][मासु उत्पादन(के.जि.)]', isset($animalProdArr['गोरु']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['गोरु']['मासु उत्पादन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गोरु][हाड / छाला(के.जि.)]', isset($animalProdArr['गोरु']['हाड / छाला(के.जि.)']) ? $animalProdArr['गोरु']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गोरु][अन्य]', isset($animalProdArr['गोरु']['अन्य']) ? $animalProdArr['गोरु']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[गोरु][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['गोरु']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['गोरु']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>

            </tr>
            <tr>
                <td>२.</td>
                <td>राँगा </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[राँगा][संख्या]', isset($animalProdArr['राँगा']['संख्या']) ? $animalProdArr['राँगा']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[राँगा][मासु उत्पादन(के.जि.)]', isset($animalProdArr['राँगा']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['राँगा']['मासु उत्पादन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[राँगा][हाड / छाला(के.जि.)]', isset($animalProdArr['राँगा']['हाड / छाला(के.जि.)']) ? $animalProdArr['राँगा']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[राँगा][अन्य]', isset($animalProdArr['राँगा']['अन्य']) ? $animalProdArr['राँगा']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[राँगा][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['राँगा']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['राँगा']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>


            </tr>
            <tr>
                <td>3.</td>
                <td>बाखा </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाखा][संख्या]', isset($animalProdArr['बाखा']['संख्या']) ? $animalProdArr['बाखा']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाखा][मासु उत्पादन(के.जि.)]', isset($animalProdArr['बाखा']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['बाखा']['मासु उत्पादन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाखा][हाड / छाला(के.जि.)]', isset($animalProdArr['बाखा']['हाड / छाला(के.जि.)']) ? $animalProdArr['बाखा']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाखा][अन्य]', isset($animalProdArr['बाखा']['अन्य']) ? $animalProdArr['बाखा']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाखा][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['बाखा']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['बाखा']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>


            </tr>
            <tr>
                <td>४.</td>
                <td>सुँगुर / वंगुर</td>
                <td>
                    {{ Form::text('hsAnimalProDetails[सुँगुर / वंगुर][संख्या]', isset($animalProdArr['सुँगुर / वंगुर']['संख्या']) ? $animalProdArr['सुँगुर / वंगुर']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[सुँगुर / वंगुर][मासु उत्पादन(के.जि.)]', isset($animalProdArr['सुँगुर / वंगुर']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['सुँगुर / वंगुर']['मासु उत्पादन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[सुँगुर / वंगुर][हाड / छाला(के.जि.)]', isset($animalProdArr['सुँगुर / वंगुर']['हाड / छाला(के.जि.)']) ? $animalProdArr['सुँगुर / वंगुर']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[सुँगुर / वंगुर][अन्य]', isset($animalProdArr['सुँगुर / वंगुर']['अन्य']) ? $animalProdArr['सुँगुर / वंगुर']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[सुँगुर / वंगुर][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['सुँगुर / वंगुर']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['सुँगुर / वंगुर']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>

            </tr>
            <tr>
                <td>5.</td>
                <td>पाडापाडी</td>
                <td>
                    {{ Form::text('hsAnimalProDetails[पाडापाडी][संख्या]', isset($animalProdArr['पाडापाडी']['संख्या']) ? $animalProdArr['पाडापाडी']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[पाडापाडी][मासु उत्पादन(के.जि.)]', isset($animalProdArr['पाडापाडी']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['पाडापाडी']['मासु उत्पादन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[पाडापाडी][हाड / छाला(के.जि.)]', isset($animalProdArr['पाडापाडी']['हाड / छाला(के.जि.)']) ? $animalProdArr['पाडापाडी']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[पाडापाडी][अन्य]', isset($animalProdArr['पाडापाडी']['अन्य']) ? $animalProdArr['पाडापाडी']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[पाडापाडी][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['पाडापाडी']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['पाडापाडी']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>

            </tr>
            <tr>
                <td>6.</td>
                <td>बाच्छाबाच्छी</td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाच्छाबाच्छी][संख्या]', isset($animalProdArr['बाच्छाबाच्छी']['संख्या']) ? $animalProdArr['बाच्छाबाच्छी']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाच्छाबाच्छी][मासु उत्पादन(के.जि.)]', isset($animalProdArr['बाच्छाबाच्छी']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['बाच्छाबाच्छी']['मासु उत्पादन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाच्छाबाच्छी][हाड / छाला(के.जि.)]', isset($animalProdArr['बाच्छाबाच्छी']['हाड / छाला(के.जि.)']) ? $animalProdArr['बाच्छाबाच्छी']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाच्छाबाच्छी][अन्य]', isset($animalProdArr['बाच्छाबाच्छी']['अन्य']) ? $animalProdArr['बाच्छाबाच्छी']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[बाच्छाबाच्छी][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['बाच्छाबाच्छी']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['बाच्छाबाच्छी']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>

            </tr>
        </table>
        <table class="table">
            <tr>
                <th>क्र.सं</th>
                <th>चौपाया तथा पशुपंक्षी</th>
                <th>संख्या</th>
                <th>उन(के.जि.)</th>
                <th>मासु उत्पादन(के.जि.)</th>
                <th>हाड / छाला(के.जि.)</th>
                <th>अन्य</th>
                <th>विक्रीबाट बार्षिक आम्दानी (रु.)</th>
            </tr>
            <tr>
                <td>1.</td>
                <td>भेडा</td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भेडा][संख्या]', isset($animalProdArr['भेडा']['संख्या']) ? $animalProdArr['भेडा']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भेडा][उन(के.जि.)]', isset($animalProdArr['भेडा']['उन(के.जि.)']) ? $animalProdArr['भेडा']['उन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भेडा][मासु उत्पादन(के.जि.)]', isset($animalProdArr['भेडा']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['भेडा']['मासु उत्पादन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भेडा][हाड / छाला(के.जि.)]', isset($animalProdArr['भेडा']['हाड / छाला(के.जि.)']) ? $animalProdArr['भेडा']['हाड / छाला(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>  
                <td>
                    {{ Form::text('hsAnimalProDetails[भेडा][अन्य]', isset($animalProdArr['भेडा']['अन्य']) ? $animalProdArr['भेडा']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text('hsAnimalProDetails[भेडा][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['भेडा']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['भेडा']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>


            </tr>
        </table>

        <table class="table">
            <tr>
              <th>क्र.सं</th>
              <th>चौपाया तथा पशुपंक्षी</th>
              <th>संख्या</th>
              <th>मासु उत्पादन(के.जि.)</th>
              <th>अण्डा(संख्या)</th>

              <th>अन्य</th>
              <th>विक्रीबाट बार्षिक आम्दानी (रु.)</th>
          </tr>
          <tr>

          </tr>


          <tr>
            <td>१.</td>
            <td>कुखुरा / हाँस</td>
            <td>
                {{ Form::text('hsAnimalProDetails[कुखुरा / हाँस][संख्या]', isset($animalProdArr['कुखुरा / हाँस']['संख्या']) ? $animalProdArr['कुखुरा / हाँस']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text('hsAnimalProDetails[कुखुरा / हाँस][मासु उत्पादन(के.जि.)]', isset($animalProdArr['कुखुरा / हाँस']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['कुखुरा / हाँस']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text('hsAnimalProDetails[कुखुरा / हाँस][अण्डा(संख्या)]', isset($animalProdArr['कुखुरा / हाँस']['अण्डा(संख्या)']) ? $animalProdArr['कुखुरा / हाँस']['अण्डा(संख्या)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text('hsAnimalProDetails[कुखुरा / हाँस][अन्य]', isset($animalProdArr['कुखुरा / हाँस']['अन्य']) ? $animalProdArr['कुखुरा / हाँस']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text('hsAnimalProDetails[कुखुरा / हाँस][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['कुखुरा / हाँस']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['कुखुरा / हाँस']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>

        </tr>
        <tr>
            <td>२.</td>
            <td>परेवा / बट्टाई</td>
            <td>
                {{ Form::text('hsAnimalProDetails[परेवा / बट्टाई][संख्या]', isset($animalProdArr['परेवा / बट्टाई']['संख्या']) ? $animalProdArr['परेवा / बट्टाई']['संख्या'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text('hsAnimalProDetails[परेवा / बट्टाई][मासु उत्पादन(के.जि.)]', isset($animalProdArr['परेवा / बट्टाई']['मासु उत्पादन(के.जि.)']) ? $animalProdArr['परेवा / बट्टाई']['मासु उत्पादन(के.जि.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text('hsAnimalProDetails[परेवा / बट्टाई][अण्डा(संख्या)]', isset($animalProdArr['परेवा / बट्टाई']['अण्डा(संख्या)']) ? $animalProdArr['परेवा / बट्टाई']['अण्डा(संख्या)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text('hsAnimalProDetails[परेवा / बट्टाई][अन्य]', isset($animalProdArr['परेवा / बट्टाई']['अन्य']) ? $animalProdArr['परेवा / बट्टाई']['अन्य'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text('hsAnimalProDetails[परेवा / बट्टाई][विक्रीबाट बार्षिक आम्दानी (रु.)]', isset($animalProdArr['परेवा / बट्टाई']['विक्रीबाट बार्षिक आम्दानी (रु.)']) ? $animalProdArr['परेवा / बट्टाई']['विक्रीबाट बार्षिक आम्दानी (रु.)'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>

        </tr>
    </table>
</div>
</div>
</div>
<div class="msgDisplay"></div>
</div>
</div>