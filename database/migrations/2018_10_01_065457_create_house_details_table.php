
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('house_details', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('refCode', 100)->unique();
            $table->string('identifier', 100)->nullable();
            
            $table->integer('createdBy')->unsigned()->nullable();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('citizen_infos_id')->unsigned();
            $table->foreign('citizen_infos_id')->references('id')->on('citizen_infos')->onDelete('cascade');

            $table->string('surName')->nullable();
            $table->string('surPhone')->nullable();

            $table->string('toleName')->nullable();
            $table->string('streetName')->nullable();
            $table->string('hsNum');
            $table->string('munHsNum')->nullable();
            $table->string('fmTypeNam')->nullable();

            
        
            $table->string('fmHeadPh')->nullable();
            $table->string('hsLandLineNum')->nullable();
            $table->string('hsTypeNam')->nullable();

            $table->string('noOfRom')->nullable();
            $table->string('romUsgForNam')->nullable();
            $table->string('wrdKhanda')->nullable();
            $table->string('locAreaNam')->nullable();
            $table->string('fmMem')->nullable();
            $table->string('memLiving')->nullable();
            $table->string('hsSamipto')->nullable();
            $table->string('otherHsSamipto')->nullable();
            
            $table->string('landSamipto')->nullable();
            $table->string('otherLandSamipto')->nullable();
            $table->string('numOfHs')->nullable();
            $table->string('florNum')->nullable();
            $table->string('romNum')->nullable();
            $table->string('hsUsgFor')->nullable();
            $table->string('areaOfHs')->nullable();
            $table->string('hsEstd')->nullable();
            $table->string('hsFndNam')->nullable();
            $table->string('romFlorNam')->nullable();
            $table->string('otherRomFlorNam')->nullable();
            $table->string('hsWallNam')->nullable();
            $table->string('otherHsWallNam')->nullable();
            $table->string('hsRofNam')->nullable();
            $table->string('otherHsRofNam')->nullable();
        
            $table->enum('isHsMadeByRule', ['छ', 'छैन', 'थाहा छैन']);            
            $table->enum('isHsEqResis', ['छ', 'छैन', 'थाहा छैन']);
            $table->enum('haveParking', ['छ', 'छैन']);
            $table->enum('havePlantation', ['छ', 'छैन']);
            $table->enum('haveGarden', ['छ', 'छैन']);
            $table->enum('isHsOwnByHead', ['छ', 'छैन']);

            $table->string('longi')->nullable();
            $table->string('lati')->nullable();
            $table->string('hsImgPath')->nullable();
            $table->boolean('haveSameKitchen')->default(0);

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('house_details');
    }
}
