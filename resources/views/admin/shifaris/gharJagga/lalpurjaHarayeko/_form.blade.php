<div class="row top-part">
  <div class="col-md-6">
   <p align="left">
     <b>पत्र संख्य
      @if(isset($data->refCode))
      {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
      @else
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
      @endif
    </b>

  </p>
  <p align="left">
   <b> च. नं.:
     {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
   </b>
 </p>
</div>
<div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">
     <b>श्री 
       {{ Form::text('accountHolderName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderName', 'required' => 'required']) }}
     </b>,
   </p>
   <p align="left">
     <b>
       {{ Form::text('accountHolderAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderAddress', 'required' => 'required']) }}।
     </b>
   </p>
 </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    
    <h4 align="center">
      <b>विषय: सिफारिस सम्बन्धमा।</b>
    </h4>
    {{ Form::hidden('letter_subject', 'खाता बन्द गरिदिने सम्बन्धमा', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
  </div>
</div>



<div class="row">
  <div class="col-md-12 content-para">
    <p>उपरोक्त सम्बन्धमा मेरो तथा श्री/सुश्री/श्रीमती 
      <b>
       {{ Form::text('applicantName1', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'applicantName1', 'required' => 'required']) }}
     </b> 

     को नाममा समेत संयुक्त दर्ता श्रेष्ता भएको
     <b>
      {{ Form::text('municipalityName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
    </b>
    महा/उपमाहा/नगरपालिका/गाउँपालिका वडा नं. 
    <!--  -->
    {{ Form::text('wardNumber1', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber1', 'required' => 'required']) }}
    <!--  -->)
    (साबिकको ठेगाना 
    <!--  -->
    {{ Form::text('address1', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'address1', 'required' => 'required']) }}
    <!--  -->
    ) कि. नं. 
    <!--  -->
    {{ Form::text('kittaNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'kittaNumber', 'required' => 'required']) }}
    <!--  -->
    को क्षे. फ
    {{ Form::text('area', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'area', 'required' => 'required']) }}
    भएको जग्गाधनी लाल पुर्जा हराएकोले सोको प्रतिलिपिको सिफारिस गरी पाऊँ भनी वडा नं. 
    {{ Form::text('wardNumber2', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber2', 'required' => 'required']) }}
    ) 
    (साबिकको ठेगाना
    {{ Form::text('address2', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'address2', 'required' => 'required']) }}
    ) बस्ने श्री/सुश्री/श्रीमती  
    {{ Form::text('applicantName2', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'applicantName2', 'required' => 'required']) }}
    ले यस वडा कार्यालयमा निवेदलन दिनुभएको हुँदा सो सम्बन्धमा त्याहाको नियमानुसार जग्गाधनी प्रमाण पसर्जाको प्रतिलिपि उपलब्ध गराईदिनहुन सिफारिसि गरिन्छ ।
  </p> 
  <p>
    निवेदकः 
    {{ Form::text('applicant', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'applicant', 'required' => 'required']) }}
    <br>
    ना. प्र. नं. 
    {{ Form::text('citizenshipNo', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'citizenshipNo', 'required' => 'required']) }}
    <br>
    जारीमितिः
    {{ Form::text('issuedDate2', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate2',  'onfocus' => 'showNdpCalendarBox("issuedDate2")']) }}    
    <br>
    पिताः 
    {{ Form::text('fatherName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'fatherName', 'required' => 'required']) }}
    <br>
    बाजे    ः 
    {{ Form::text('grandFatherName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'grandFatherName', 'required' => 'required']) }}
  </p>
</div>
</div>
<div class="text-right btm-last">
  <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required" value="{{!empty($data) ? $data->authorizedPerson : '' }}"></p>
  <p> 
    <b>
      <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
        @if(count($deginationsId) > 0)
        @foreach($deginationsId as $deg)
        <option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
          {{ $deg->nameNep }} 
        </option>
        @endforeach
        @endif
      </select>
    </b>
  </p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>