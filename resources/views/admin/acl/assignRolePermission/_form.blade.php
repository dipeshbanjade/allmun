@if(!isset($datas))
 <div class="form-group">
      <label for="input-search" class="sr-only" >Search Tree:
      </label>
      <select name="roles_id" class="form-control col-sm-3 col-md-3">
       <option value="">@lang('commonField.extra.selectRole')</option>
        @if(count($roleData) > 0)
           @foreach($roleData as $role)
               <option value="{{ $role->id }}">
                 {{ $role->roleName }}
               </option>
           @endforeach
        @endif

     </select>
   </div>
@endif
   <div class="checkbox chk-left" style="color: black;">
    @if(count($permissionData) > 0)
        <div class="row">
             @foreach($permissionData as $permission => $items) 
                   <div class="col-sm-3 col-md-3">
                       <h3>{{ $permission }}</h3>
                       @foreach($items as $sysPer)
                         <label>
                           <input name="permissions_id[]" type="checkbox" class="checkbox" id="chk-ignore-case" value="{{ $sysPer->id }}"
                           <?php  if(isset($datas)){
                                    foreach($datas as $data){
                                      if($data->permission_id == $sysPer->id){ 
                                            echo "checked";
                                      }
                                    }
                                  }     
                            ?>> 
                            {{ $sysPer->perName }}
                         </label><br>
                       @endforeach
                       
                   </div>
             @endforeach
        </div>
    @endif
    </div>


