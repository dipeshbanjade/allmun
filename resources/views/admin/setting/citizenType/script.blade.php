<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditcitizenType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 200
                }
            },
            messages: {
                nameEng : "The name of citizenType is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmcitizenType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 200
                }
            },
            messages: {
          
                nameEng : "The name of citizenType is required"
            }
        });
	  var btnAddCitizenType = $('.btnAddCitizenType');
  btnAddCitizenType.on('click', function(e){
      $('#citizenModel').modal('show'); 
  });

  var btnEditCitizen = $('.btnEditCitizen');
  btnEditCitizen.on('click', function(e) {
      e.preventDefault();
      var url = $(this).data('url');
      $.ajax({
        'type': 'GET',
        'url': url,
        success: function (response) {
            $('.citizenId').val(response.id);
            $('.txtRefCode').val(response.refCode).attr('disabled',true);
            $('.txtNameNep').val(response.nameNep);
            $('.txtNameEng').val(response.nameEng);
            $("#editCitizenModel").modal('show');
        }
    })
  });

  var frmUpdateCitizenType = $('#frmUpdateCitizenType');
  frmUpdateCitizenType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.citizenId').val();

    var url  = "{{URL::to('/')}}" + "/setting/citizenType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editCitizenModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editCitizenModel').modal('hide');
});
});

</script>