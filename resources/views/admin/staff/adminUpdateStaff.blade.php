@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
       @if(count($updateUsr) > 0)
       <div class="col-md-12 col-sm-12">
           <div class="card card-box">
               <div class="card-head">
                   <header>@lang('commonField.personal_information.updateStaffDetails')</header>
               </div>
               <div class="card-body col-md-8 col-lg-6 col-sm-12 pw-card" id="bar-parent">
                    <h3>
                      <label>
                          <i class="fa fa-email"></i>{{ $updateUsr->email }}<br>
                            {!! $updateUsr->name !!}
                          </label>
                      </h3><hr>
                       {{ Form::open(['route' => 'adminUpdateStaffDetails', 'name' => 'frmChangePassword']) }}
                       <div class="form-group">
                           <label for="name">@lang('commonField.personal_information.name')</label>
                           {{ Form::text('name', $updateUsr->name, ['class'=>'form-control']) }}
                       </div>
                       <!--  -->
                       <div class="form-group">
                           <label for="email">@lang('commonField.personal_information.email')</label>
                           {{ Form::email('email', $updateUsr->email, ['class'=>'form-control']) }}
                       </div>
                       <!--  -->
                       <div class="form-group">
                           <label for="role">@lang('commonField.extra.selectRole')</label>
                           <select name="roles_id" class="form-control">
                               <option value="">@lang('commonField.extra.selectRole')</option>
                                @if(count($roles) > 0)
                                   @foreach($roles as $role)
                                     <option value="{{ $role->id }}" {{ isset($updateUsr->roles_id)  &&  $updateUsr->roles_id == $role->id ? 'selected' : ''}}>
                                          {{ $role->roleName }}
                                     </option>
                                   @endforeach
                                @endif
                           </select>
                       </div>
                         {{ Form::hidden('userId', $updateUsr->id) }}
                       {{ Form::submit('Update', ['class' => 'btn btn-success']) }}
                      {{ Form::close() }}
               </div>
           </div>
       </div>
       @endif
@endsection
@section('custom_script')
   @include('admin.staff.script')
@endsection