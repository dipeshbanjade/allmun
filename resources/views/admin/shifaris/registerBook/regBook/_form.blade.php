<div class="right_col dash-pg nep" role="main" style="min-height: 882px;">
  <div class="clearfix"></div>
  <div class="row">


    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_title">
          <h3 class="title-x">दर्ता किताबमा नया रेकर्ड थप्नुहोस </h3>

          <div class="clearfix"></div>
        </div>


        <div class="x_content" id="block">

          <div class="col-md-12">

           
          



          <div class="col-md-9">


            <form action="https://project.hovertechnepal.com/DartaBook/saveDartaRecord" method="post" role="form" class="form-horizontal" enctype="multipart/form-data">

              <div class="form-group">
                <label class="col-md-3">दर्ता नं:<span class="red">*</span> </label>
                <div class="col-md-7">

                  <input type="text" required="required" class="form-control" name="registration_no" value="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">दर्ता मिति:<span class="red">*</span></label>
                <div class="col-md-7">

                  <input type="text" required="required" class="form-control ndp-nepali-calendar date-input-field" id="registration_date" name="registration_date" value="" onfocus="showNdpCalendarBox('registration_date')">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">प्राप्त भएको(पत्र संख्या):<span class="red">*</span></label>
                <div class="col-md-7">

                  <input type="text" required="required" class="form-control" name="patra_sankya" value="">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3">प्राप्त भएको(पत्रको मिति):<span class="red">*</span></label>
                <div class="col-md-7">

                  <input type="text" required="required" class="form-control ndp-nepali-calendar date-input-field" id="letter_date" name="letter_date" value="" onfocus="showNdpCalendarBox('letter_date')">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">पठाउने अफिसको नाम:<span class="red">*</span></label>
                <div class="col-md-7">

                  <input type="text" required="required" list="letter_sender_office_name" class="form-control" name="letter_sender_office_name" autocomplete="off" value="">

                  <datalist id="letter_sender_office_name">
                                            <option value="चाँगुनारायण नगरपालिका">


                                                </option><option value="office1">


                                                </option><option value="Office 1 ">


                                                </option><option value="divash office ">


                                                </option><option value="raj office ">


                                                </option><option value="Office by Nabaraj ">


                                                </option><option value="सुर्यबिनायक नगरपालीका">


                                                </option><option value="नापी कार्यलय">


                                                </option><option value="जिल्ला अायोजना कार्यान्वयन इकाइ (भवन)">


                                                </option><option value="अार्थिक मामिला तथा योजना मन्त्रालय">


                                                </option><option value="संघीय मामिला तथा सामान्य प्रशासन मन्त्रालय">


                                                </option><option value="१ नं वडा कार्यालय">


                                                </option><option value="२ नं वडा कार्यालय">


                                                </option><option value="३ नं वडा कार्यालय">


                                                </option><option value="४ नं वडा कार्यालय">


                                                </option><option value="५ नं वडा कार्यालय">


                                                </option><option value="६ नं वडा कार्यालय">


                                                </option><option value="७ नं वडा कार्यालय">


                        



                      </option></datalist>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3">विषय:<span class="red">*</span> </label>
                    <div class="col-md-7">

                      <input type="text" required="required" class="form-control" name="letter_subject" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3">बुझिलिने फाटवाला(नाम):<span class="red">*</span> </label>
                    <div class="col-md-7">

                      <input type="text" required="required" class="form-control" name="receiver_name" value="">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3">बुझिलिने फाटवाला(मिति):<span class="red">*</span> </label>
                    <div class="col-md-7">

                      <input type="text" required="required" class="form-control ndp-nepali-calendar date-input-field" id="receiver_sign_date" name="receiver_sign_date" value="" onfocus="showNdpCalendarBox('receiver_sign_date')">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3">डकुमेन्टको स्क्यान फोटो :<span class="red">*</span> </label>
                    <div class="col-md-7">

                      <input type="file" name="document_file">
                    </div>
                  </div>


                  <div class="form-group">
                    <label class="col-md-3">कैफियत:</label>
                    <div class="col-md-7">

                      <textarea name="remarks" class="form-control"></textarea> 
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3">&nbsp;</label>
                    <div class="col-md-7">

                      <input type="hidden" value="1" name="save_darta_record">

                      <button type="submit" class="btn bttn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span>रेकर्ड थप्नुहोस</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="col-md-3">

                  <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    * लागेको फिल्डहरु अनिवार्य रुपमा भर्नुपर्ने छ |
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>