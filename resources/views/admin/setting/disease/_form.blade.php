<div class="card-body row">

	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
		{{ Form::text('nameNep', null, ['class'=>'UnicodeKeyboardTextarea mdl-textfield__input txtNameNep', 'placeholder'=>'']) }}
		<label class = "mdl-textfield__label">@lang('commonField.personal_information.nameNep')
		</label>
	</div>
	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
		{{ Form::text('nameEng', null, ['class'=>'mdl-textfield__input txtNameEng', 'placeholder'=>'']) }}
		<label class = "mdl-textfield__label">@lang('commonField.personal_information.nameEng')</label>
	</div>
	<div class="col-lg-12 p-t-20"> 
	   <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
	       <select name="tag" class="txttag form-control" required="true">
	           <option value="">Select A Tag</option>
	     		<option value="long">Long Term Disease</option>
	     		<option value="short">Short Term Disease</option>
	     		<option value="both">both</option>
	       </select>
	  </div>
	</div>

</div>