<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class naturalCalamitiesVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:nirman_bebasayas',
            'issuedDate' => 'required',
            'chalaniNum' => 'required',
            'companyHead' => 'required',
            'companyAddress' => 'required',
            'candidateAddress' => 'required',
            'candidateName' => 'required',
            'calamitiesLocation' => 'required',
            'calamitiesDate' => 'required',
            'calamitiesName' => 'required',
            'jaminAddress' => 'required',
            'jaminMan' => 'required',
            'jaminTotalMan' => 'required',
            'bodartha' => 'required',
            'authorizedPerson' => 'required'
            
        ];
    }
}
