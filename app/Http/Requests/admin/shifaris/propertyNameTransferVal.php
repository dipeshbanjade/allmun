<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class propertyNameTransferVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'chalaniNumber' => 'required|min:3|unique:property_name_transfers',
            'accountHolderName' => 'required',
            'accountHolderAddress' => 'required',
            'ownerName' => 'required',
            'nata' => 'required',
            'natedarName' => 'required',
            'date' => 'required',
            'applicant' => 'required'
        ];
    }
}
