<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherOfficesTable extends Migration
{
    public function up()
    {
        Schema::create('other_offices', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('users_id')->nullable()->unsigned();
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('nameEng')->nullable();
            $table->string('nameNep')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('landlineNumber')->nullable();
            $table->string('email')->nullable();
            $table->string('addr')->nullable();
            $table->text('companyDetails')->nullable();

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('other_offices');
    }
}
