<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantUsersTable extends Migration
{
    public function up()
    {
        Schema::create('applicant_users', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('applicantName');
            $table->string('applicantAddress');
            $table->string('citizenshipNumber', 100)->unique();
            $table->string('phoneNumber', 100)->unique()->nullable();
            $table->string('email', 100)->unique()->nullable();
            $table->date('dob')->nullable();
            $table->string('fatherName')->nullable();
            $table->string('profilePic')->nullable();
            $table->string('citizenshipImgPath')->nullable();
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('applicant_users');
    }
}
