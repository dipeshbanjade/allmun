<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\Degination;
use App\model\myLogic;
use App\Http\Requests\admin\setting\degination\deginationValidation;
use Validator;



class deginationController extends Controller
{
    protected $viewPath = 'admin.setting.degination';
    protected $deginationTbl = 'deginations';
    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.deginationPrefix');
    }

    public function index()
    {
        $page['page_title']       = 'degination';
        $page['page_description'] = 'list of all degination';

        $allDegination = Degination::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));


        $prefix   = myLogic::sysRefCode($tablename = $this->deginationTbl, $prefix = $this->prefix);
        return view($this->viewPath . '.index', compact(['page', 'allDegination', 'prefix']));
    }

    public function store(deginationValidation $request)
    {
        $mySave = Degination::create([
                   'refCode' => $request->refCode,
                   'nameNep' => $request->nameNep ?? 'N/A',
                   'nameEng' => $request->nameEng,
                   'status'  => 0,
                   'softDelete' => 0
                 ]);
        if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function edit($id)
    {

        $data = Degination::findOrFail($id);
        if ($data) {
            return response()->json([
                    'success' => true,
                    'id'      => $data->id,
                    'refCode' => $data->refCode,
                    'nameNep' => $data->nameNep,
                    'nameEng' => $data->nameEng
                   ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.dataNotFound')
                ]);
        }
    }

   public function update(Request $request, $id)
       {
            $update = Degination::findOrFail($id);
            $update->refCode = $request->refCode;
           $update->nameNep   = $request->nameNep;
           $update->nameEng  = $request->nameEng;

           $myUpdate = $update->update();

           if ($myUpdate) {
               return response()->json([
                   'success' => true,
                   'message' => config('activityMessage.updateMessage')
                   ]);
           }else{
               return response()->json([
                   'success'  => false,
                   'message'  => config('activityMessage.notUpdated')
                   ]);
           }
       }

    public function destroy($id)
    {
        $del = Degination::findOrFail($id);
        $del->softDelete = 1;
        $myDel = $del->update();
        if ($myDel) {
          return MyLogic::successChangeStatus($code = 200);
        }else{
           return MyLogic::successChangeStatus($code = 301);
        }
    }

    public function changeStatus(Request $request){
      $id  = $request->id;
      $status = $request->status==1 ? 0 : 1;
      $change = mylogic::sysChangeStatus($tablename = $this->deginationTbl, $Id = $id, $status = $status);

      if ($change) {
          return MyLogic::successChangeStatus($code = 200);
      }else{
          return MyLogic::idNotFoundMessage($code = 301);
      }
    }
}
