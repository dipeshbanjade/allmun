<div class="sidebar-container">
 <div class="sidemenu-container navbar-collapse collapse fixed-menu">
  <div id="remove-scroll" class="left-sidemenu">
    <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
      <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
          <span></span>
        </div>
      </li>
      @if(Auth::user()->userLevel === 'dev')
      <li class="nav-item active">
        <a href="#" class="nav-link nav-toggle "><i class="material-icons f-left">assignment_ind</i>
         <span class="title">
          ACL
        </span>
        <span class="arrow open"></span>
      </a>
      <ul class="sub-menu">
        <li class="nav-item active">
          <a href="{{ route('system.role.index') }}" class="nav-link " title="create role">
            <span class="title">Role</span>
            <span class="selected"></span>
          </a>
        </li>

        <li class="nav-item ">
          <a href="{{ route('system.permission.index') }}" class="nav-link " title="create permission">
            <span class="title">Permission</span>
          </a>
        </li>
        <!--  -->
        <li class="nav-item ">
          <a href="{{ route('system.assignRolePermission.index') }}" class="nav-link " title="create permission">
            <span class="title">Role & Permission</span>
          </a>
        </li>
        <!--  -->
        <li class="nav-item ">
          <a href="{{ route('system.userDetails.index') }}" class="nav-link " title="assign role permission">
            <span class="title">User</span>
          </a>
        </li>

      </ul>
    </li>
    @endif


@if(Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun')
<li class="nav-item">
  <a href="#" class="nav-link nav-toggle"> 
   <i class="material-icons f-left">account_balance</i>
   <span class="title">
     @lang('sidebarMenu.system_setting.municipility')
   </span> <span class="arrow"></span>
 </a>
 <ul class="sub-menu">
  @if(in_array('admin.mun-set.index', $myPermission))
  <li class="nav-item">
    <a href="{{ route('admin.mun-set.index') }}" class="nav-link "> <span class="title">
      @lang('sidebarMenu.system_setting.municipility')
    </span>
  </a>
</li>
@endif
</ul>
</li>
<!--  -->
@if(in_array('admin.munStaff.index',$myPermission) || in_array('admin.munStaff.create',$myPermission) || in_array('admin.munAtten.index',$myPermission) || in_array('adminViewLeave',$myPermission) || in_array('dailyReport.adminView',$myPermission))
<li class="nav-item">
  <a href="#" class="nav-link nav-toggle">
   <i class="material-icons">business</i>
   <span class="title">@lang('sidebarMenu.system_setting.municipalityAdmin')</span> 
   <span class="arrow"></span>
 </a>
 <ul class="sub-menu">
  @if(in_array('admin.munStaff.index', $myPermission))
  <li class="nav-item">
    <a href="{{route('admin.munStaff.index') }}" class="nav-link "> 
      <span class="title">@lang('sidebarMenu.staff_setting.staff')</span>
    </a>
  </li>
  @endif

  @if(in_array('admin.munStaff.create', $myPermission))
  <li class="nav-item">
    <a href="{{ route('admin.munStaff.create') }}" class="nav-link "> <span class="title">  @lang('sidebarMenu.staff_setting.createStaff')</span>
    </a>
  </li>
  @endif

  @if(in_array('admin.munAtten.index', $myPermission))
  <li class="nav-item">
    <a href="{{ route('admin.munAtten.index') }}" class="nav-link "> <span class="title">
      @lang('commonField.employer.attendance')
    </span>
  </a>
</li>
@endif

@if(in_array('adminViewLeave', $myPermission))
<li class="nav-item">
  <a href="{{ route('adminViewLeave') }}" class="nav-link "> <span class="title">
    @lang('sidebarMenu.extra.leave')
  </span>
</a>
</li>
@endif
<!--  -->

@if(in_array('dailyReport.adminView', $myPermission))
<li class="nav-item">
  <a href="{{ route('dailyReport.adminView') }}" class="nav-link" title="view daily report by admin">
   <span class="title">
    @lang('commonField.extra.dailyReport')
  </span>
</a>
</li>
@endif
<!--  -->
@if(in_array('fieldVisit.adminView', $myPermission))
<li class="nav-item">
  <a href="{{ route('fieldVisit.adminView') }}" class="nav-link" title="view staff field visit..">
   <span class="title">
    @lang('commonField.links.fieldVisit')
  </span>
</a>
</li>
@endif

@if(in_array('setUpCompanyLeave', $myPermission))
<li class="nav-item">
  <a href="{{ route('LeaveForShow') }}" class="nav-link" title="set official leave">
   <span class="title">
    @lang('commonField.links.officialLeave')
  </span>
</a>
</li>
@endif

</ul>
</li>
@endif

@if(in_array('wardDetails.index',$myPermission) || in_array('wardDetails.create',$myPermission) || in_array('wardAdmin',$myPermission) || in_array('createWardAdmin',$myPermission))

<li class="nav-item">
  <a href="#" class="nav-link nav-toggle">
    <i class="material-icons f-left">home</i>
    <span class="title">@lang('sidebarMenu.system_setting.ward')</span> <span class="arrow"></span>
  </a>
  <ul class="sub-menu">
@if(in_array('wardDetails.index', $myPermission))
   <li class="nav-item"> 
    <a href="{{ route('wardDetails.index') }}" class="nav-link "> 
     <span class="title">
      @lang('sidebarMenu.system_setting.ward')
    </span>
  </a>
</li>
@endif

@if(in_array('wardDetails.create', $myPermission))
<li class="nav-item">
  <a href="{{ route('wardDetails.create') }}" class="nav-link ">
   <span class="title">@lang('sidebarMenu.ward_setting.createWard')</span>
 </a>
</li>
@endif

@if(in_array('wardAdmin', $myPermission))
<li class="nav-item">
  <a href="{{ route('wardAdmin') }}" class="nav-link "> <span class="title">
    @lang('commonField.links.wardAdmin')
  </span>
</a>
</li>
@endif

@if(in_array('createWardAdmin', $myPermission))
<li class="nav-item">
  <a href="{{ route('createWardAdmin') }}" class="nav-link "> <span class="title">
    @lang('commonField.links.createWardAdmin')
  </span>
</a>
</li>
@endif
</ul>
</li>
@endif
@endif

<!-- ward admin -->
@if(Auth::user()->userLevel == 'wrd' || Auth::user()->userLevel == 'dev')
@if(in_array('wardStaff.index', $myPermission))
<li class="nav-item">
  <a href="#" class="nav-link nav-toggle">
    <i class="material-icons f-left">home</i>
    <span class="title">@lang('sidebarMenu.system_setting.ward_admin')</span> <span class="arrow"></span>
  </a>
  <ul class="sub-menu">
   <li class="nav-item"> 
    <a href="{{ route('wardStaff.index') }}" class="nav-link "> 
     <span class="title">
      @lang('sidebarMenu.staff_setting.staff')
    </span>
  </a>
</li>
@endif

<!--  -->
@if(in_array('wardStaff.create', $myPermission))
<li class="nav-item"> 
  <a href="{{ route('wardStaff.create') }}" class="nav-link "> 
   <span class="title">
    @lang('sidebarMenu.staff_setting.createStaff')
  </span>
</a>
</li>
@endif
@if(in_array('admin.munAtten.index', $myPermission))
  <li class="nav-item">
    <a href="{{ route('admin.munAtten.index') }}" class="nav-link "> <span class="title">
      @lang('commonField.employer.attendance')
    </span>
  </a>
</li>
@endif

@if(in_array('AdminListLeaveForm', $myPermission))
<li class="nav-item"> 
  <a href="{{ route('AdminListLeaveForm') }}" class="nav-link " title="list of all staff leave form"> 
   <span class="title">
    @lang('sidebarMenu.system_setting.staffLeave')
  </span>
</a>
</li>
@endif
<!--  -->

@if(in_array('admin.munAtten.index', $myPermission))
<li class="nav-item">
  <a href="{{ route('admin.munAtten.index') }}" class="nav-link "> <span class="title">
    @lang('sidebarMenu.extra.attendance')
  </span>
</a>
</li>
@endif

@if(in_array('dailyReport.adminView', $myPermission))
<li class="nav-item">
  <a href="{{ route('dailyReport.adminView') }}" class="nav-link "> <span class="title">
    @lang('commonField.extra.dailyReport')
  </span>
</a>
</li>
@endif

@if(in_array('fieldVisit.adminView', $myPermission))
<li class="nav-item">
  <a href="{{ route('fieldVisit.adminView') }}" class="nav-link" title="view staff field visit">
   <span class="title">
    @lang('commonField.links.fieldVisit')
  </span>
</a>
</li>
@endif
<!--  -->
@if(in_array('company.index', $myPermission))
<li class="nav-item">
  <a href="{{ route('company.index') }}" class="nav-link" title="Company details">
   <span class="title">
    @lang('commonField.dartaChalani.office')
  </span>
</a>
</li>
@endif

@if(in_array('setUpCompanyLeave', $myPermission))
<li class="nav-item">
  <a href="{{ route('LeaveForShow') }}" class="nav-link" title="set official leave">
   <span class="title">
    @lang('commonField.links.officialLeave')
  </span>
</a>
</li>
@endif

</ul>
</li>
@endif
<!-- ward admin -->

<!-- Citizen Form -->
@if(in_array('citizenInfo.index', $myPermission) || in_array('citizenInfo.create', $myPermission))
   <li class="nav-item">
     <a href="#" class="nav-link nav-toggle">
       <i class="material-icons">person</i>
       <span class="title">
         <!-- @lang('sidebarMenu.system_setting.createSystemSetting') -->
         @lang('commonField.extra.citizen')
       </span><span class="arrow"></span></a>
       <ul class="sub-menu">
       @if(in_array('citizenInfo.index', $myPermission))
         <li class="nav-item">
           <a href="{{ route('citizenInfo.index') }}" class="nav-link ">
             <span class="title">
              <!-- @lang('sidebarMenu.system_setting.province') -->
              @lang('commonField.extra.citizen')      
            </span>
          </a>
        </li>
        @endif

       @if(in_array('citizenInfo.create', $myPermission))
            <li class="nav-item">
            <a href="{{ route('citizenInfo.create') }}" class="nav-link ">
               <span class="title">
                <!-- @lang('sidebarMenu.system_setting.province') -->
                @lang('commonField.extra.createCitizen')      
              </span>
            </a>
          </li>
      @endif
    </ul>
   </li>
@endif
<!-- Citizen Form -->
<!-- house Details -->
@if(in_array('houseDetails.index', $myPermission))
  <li class="nav-item">
    <a href="#" class="nav-link nav-toggle">
      <i class="fa fa-home"></i>
      <span class="title">
        <!-- @lang('sidebarMenu.system_setting.createSystemSetting') -->
        @lang('sidebarMenu.houseDetails.individualHouseDetails')
      </span><span class="arrow"></span></a>
      <ul class="sub-menu">
        <li class="nav-item">
          <a href="{{ route('houseDetails.index') }}" class="nav-link ">
            <span class="title">
             <!-- @lang('sidebarMenu.system_setting.province') -->
             @lang('sidebarMenu.houseDetails.individualHouseDetails')    
           </span>
         </a>
       </li>
           <li class="nav-item">
           <a href="{{ route('houseDetails.create') }}" class="nav-link ">
              <span class="title">
               <!-- @lang('sidebarMenu.system_setting.province') -->
               @lang('sidebarMenu.houseDetails.individualHouseDetailsCreate')      
             </span>
           </a>
         </li>
   </ul>
  </li>
  @endif
<!-- ending of house Details -->
<!-- darta chalani -->
@if(in_array('challaniIndex', $myPermission) || in_array('dartaIndex', $myPermission))
  <li class="nav-item">
    <a href="#" class="nav-link nav-toggle">
      <i class="fa fa-home"></i>
      <span class="title">
        <!-- @lang('sidebarMenu.system_setting.createSystemSetting') -->
        @lang('sidebarMenu.dartaChalani.dartaChalani')
      </span><span class="arrow"></span></a>
      <ul class="sub-menu">
        <li class="nav-item"> 
           <a href="{{ route('challaniIndex') }}" class="nav-link" title="chalani details">
             <span class="title">
              @lang('sidebarMenu.dartaChalani.chalani')    
            </span>
          </a>
        </li>
        <!--  -->
        <li class="nav-item">
          <a href="{{ route('dartaIndex') }}" class="nav-link" title="darta details">
            <span class="title">
             @lang('sidebarMenu.dartaChalani.darta')    
           </span>
         </a>
       </li>
       <!--  -->
   </ul>
  </li>
  @endif
<!-- darta chalani -->

<!-- Shifaris NAV -->
@if(in_array('allShifaris', $myPermission))
<li class="nav-item">
  <a href="#" class="nav-link nav-toggle">
    <i class="fa fa-home"></i>
    <span class="title">
      <!-- @lang('sidebarMenu.system_setting.createSystemSetting') -->
      @lang('sidebarMenu.extra.shifaris')
    </span><span class="arrow"></span></a>
    <ul class="sub-menu">
      <li class="nav-item">
        <a href="{{ route('allShifaris') }}" class="nav-link ">
          <span class="title">
           <!-- @lang('sidebarMenu.system_setting.province') -->
           @lang('sidebarMenu.extra.shifaris')    
         </span>
       </a>
     </li>
     <li class="nav-item">
     <a href="{{ route('searchShifarisIndex') }}" class="nav-link ">
       <span class="title">
        <!-- @lang('sidebarMenu.system_setting.province') -->
        @lang('sidebarMenu.extra.searchShifaris')
      </span>
    </a>
  </li>
 </ul>
</li>
@endif


</ul>
</div>
</div>
</div>