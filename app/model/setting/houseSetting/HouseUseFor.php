<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class HouseUseFor extends Model
{
    protected $table = 'house_use_fors';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
