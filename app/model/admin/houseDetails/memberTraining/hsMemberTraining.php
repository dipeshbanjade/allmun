<?php

namespace App\model\admin\houseDetails\memberTraining;

use Illuminate\Database\Eloquent\Model;

class hsMemberTraining extends Model
{
    protected $table = 'hs_member_trainings';
}
