@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
    <div class="col-sm-12 col-md-12" id="myDIV" >
      <div class="card-box">
        <div class="card-body ">
         <a href="{{ route('admin.munStaffLeave.create') }}" class="btn btn-primary pull-right">
            <i class="fa fa-plus">&nbsp;@lang('commonField.extra.createLeave')</i>
         </a>
         
         <div class="card-head">
          <header>@lang('commonField.extra.muncipility')&nbsp;@lang('commonField.extra.createLeave')</header>
        </div>
    <div class="table-scrollable" style="color:black;">
      <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle"  id="example4">
        <thead>
          <tr>
            <th width="3%">@lang('commonField.extra.sn')</th>
            <th>@lang('commonField.extra.refCode')</th>
            <th width="20%">@lang('commonField.extra.leaveType')</th>
            <th width="10%">@lang('commonField.extra.noOfDays')</th>
            <th>@lang('commonField.extra.startDate')</th>
            <th>@lang('commonField.extra.appliedOn')</th>
            <th>@lang('commonField.extra.approvedBy')</th>
            <th>@lang('commonField.extra.action')</th>
          </tr>
        </thead>
        <tbody>
           <?php  $count = 1; ?>
           @if(count($myLeaveForm))
                @foreach($myLeaveForm as $leave)
                    <?php 
                       $leaveTypeName = getleaveType($leave->leave_types_id);
                       $leaveName = $leaveTypeName['nameNep'] .'-'. $leaveTypeName['nameEng'];
                       $approveId =  getMunStaff($leave->approvedBy);  
                       $approveByName = $approveId['firstNameEng']  . $approveId['middleNameEng'] . $approveId['lastNameEng'];
                     ?>
                    <tr class="odd gradeX">
                      <td>
                           {{ $count ++ }}
                      </td>
                      <td>{{ $leave->refCode }}</td>
                      <td>{{ $leaveName }}</td>  <!-- TODO define relation ship -->
                      <td >{{ $leave->noOfDays }} days</td>
                      <td>{{ $leave->startDate }} - {{ $leave->endDate }}</td>
                      <td>{{ $leave->created_at->diffForHumans() }}</td>
                      <td>
                          <button class="{{ $leave->approvedBy == '' ? 'btn btn-danger btn-sm' : 'btn btn-success btn-sm' }}">
                            {{ $approveByName }}
                          </button>
                      </td>
                      <td>
                        <!-- if approved cannot edit or delete -->
                        @if($leave->approvedBy == '')
                          <a href="{{ route('admin.munStaffLeave.edit', $leave->id) }}" class="btn btn-primary btn-xs" title="edit leave form">
                              <i class="fa fa-pencil"></i>
                          </a>


                          <a href="javascript.void(0)" class="deleteMe" data-url="{!! route('admin.munStaffLeave.delete', $leave->id ) !!}" title="delete leave form" data-name="{{ $leave->refCode }}" data-id = "{{ $leave->id }}">

                              <button class="btn btn-danger btn-xs">
                                    <i class="fa fa-trash-o "></i>
                               </button>
                          </a>
                          @endif
                        </td>
                      </tr>
                @endforeach
           @endif
          </tbody>
        </table>
        {{ $myLeaveForm->links() }}
      </div>
    </div>
  </div>
  </div>
  @endsection