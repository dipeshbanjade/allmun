<div class="row top-part">
  <div class="col-md-6">
   <p align="left">
    <b>@lang('commonField.extra.refCode'):</b>
    @if(isset($data->refCode))
    {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
    @else
    {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
    @endif
  </p>
  <p align="left">
    <b> च. नं.:
      {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
    </b>
  </p>
</div>
<div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
     {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
   </b>
 </p>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24">
    </p><h4 align="center"><b>विषय: उपभोक्ता समिति गठन सम्बन्धमा।</b></h4>
    {{ Form::hidden('letter_subject', 'उपभोक्ता समिति गठन सम्बन्धमा।', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
    <p></p>
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">श्री 
     {{ Form::text('orgName', isset($data->orgName) ? $data->orgName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'orgName', 'required' => 'required']) }}
   ,</p>
   <p align="left">  {{ Form::text('orgLocation', isset($data->district) ? $data->district : getChecker()->districts->districtNameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'orgLocation', 'required' => 'required']) }}।</p>
 </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">
    <p> 
      उपरोक्त सम्बन्धमा तहाँ कार्यालयबाट चालु आ.व. 
      <b>
        {{ Form::text('fiscalYear', '२०७५/०७६', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'fiscalYear', 'required' => 'required']) }}
      </b> मा यस <b>
        {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
      </b> वडा नं.  <b> 
        {{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNum', 'required' => 'required']) }}
      </b> (साविक 
      <!--SABIK ADDRESS START -->
      {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationAddress', 'required' => 'required']) }}
      <!-- SABIK ADDRESS END -->
      <select onchange="changeSelect(this)" name="organizationType">
        <option value="गा.वि.स." 
        {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
        <option value="नगरपालिका" 
        {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
        <option value="उप महानगरपालिका" 
        {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
        <option value="महानगरपालिका" 
        {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
      </select>
      , वडा नं.  
      {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field star', 'placeholder'=>'    *', 'id' => 'organizationWard', 'required' => 'required']) }}
      )   को  
      {{ Form::text('businessName', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'businessName', 'required' => 'required']) }}
      आयोजनाको लागि रकम रु 
      {{ Form::text('projectAmount', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'projectAmount', 'required' => 'required']) }}
      मात्र विनियोजन भएकोमा सो योजना संचालन गर्न तपसिल बमोजिमको उपभोक्ता समिति गठन भएकोले उक्त उपभोक्ता समिति संग सम्झौता गरी योजना सञ्चालन गर्न आवश्यक व्यवस्था मिलाई दिनुहुन अनुरोध गरिन्छ।
    </p>
  </div>
</div> 
<h4 align="center"><b>उपभोक्ता समितिका पदाधिकारीहरूको विवरण</b> </h4>
<div class="col-md-12">
  <div class="table-responsive">
    <table class="table table-bordered" id="accountTable">
      <thead>
        <tr>
          <th width="2%">क्र.स.</th>
          <th>व्यक्तिको नाम </th>
          <th>पद </th>
          <th>कैफियत </th>
        </tr></thead>
        <tbody>
          <?php 
          $count = 1;
          $tableData = !empty($data) ? json_decode($data->meetingStatement, true) : [1];
          //dd($tableData);
          foreach($tableData as $singleData){         

            ?>
            <tr>
              <td>{{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => '', 'required' => 'required']) }} </td>
              <td>{{ Form::text('name[]', !empty($singleData['name']) ? $singleData['name'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'name', 'required' => 'required']) }} </td>
              <td>{{ Form::text('post[]', !empty($singleData['post']) ? $singleData['post'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'post', 'required' => 'required']) }} </td>
              <td>{{ Form::text('remark[]', !empty($singleData['remark']) ? $singleData['remark'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'remark', 'required' => 'required']) }} </td>

              @if($count == 2)              
              <td class="add-btns" style="border: 0px;"><a href="" id="add" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
              @else
              <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
              @endif
            </tr>
            <?php } ?>
          </tbody>
        </table>  
      </div>
      <div class="text-right btm-last">
        <p>  {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} </p>
        <p> <b>
          <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
            @if(count($deginationsId) > 0)
            @foreach($deginationsId as $deg)
            <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
              {{ $deg->nameNep }} 
            </option>
            @endforeach
            @endif
          </select>
        </b></p>
      </div>
      <!--views for nibedak detail -->
      <div class="clearfix"></div>
      <hr>
      @include('admin.shifaris.nibedakCommonField')
      <!-- END -->
      <div class="col-md-12">
        <hr>
      </div>
    </div>