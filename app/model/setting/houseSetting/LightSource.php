<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class LightSource extends Model
{
 	protected $table = 'light_sources';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
