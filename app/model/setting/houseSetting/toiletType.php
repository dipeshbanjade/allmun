<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class toiletType extends Model
{
    protected $table = 'toilet_types';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
