<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardinstituteRegistration']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 15
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 1,
					maxlength : 30				
				},
				operatorName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				companyName : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				municipalityName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				organizationType : {
					required : true
				},
				organizationWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				organizationName : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				orgType : {
					required : true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				operatorName : "Operator name is required",
				companyName : "Company Name is required",
				municipalityName : "Municipality Name is required",
				wardNumber : "Ward Number is required",
				organizationAddress : "Organization Address is required",
				organizationType : "Organization Type is required",
				organizationWard : "Organization Ward is required",
				organizationName : "Organization Name is required",
				orgType : "Organization Type is required",
				authorizedPerson : "Authorizing person is required"
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	             
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>