<style type="text/css">
table th, table td{
    border: 1px solid black;
    text-align: center;
}
</style>
<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <p>
                    १.तपाईका परिवारका सदस्यहरुको निम्न क्रियाकलापहरुमा हात धुने बानी कस्तो रहेको छ उल्लेख गर्नुहोस् ।  
                </p>
                <br>
                <table class="table">
                  <tr>
                    <th rowspan="2">सि.न.</th>
                    <th rowspan="2">क्रियाकलाप</th>
                    <th rowspan="2">क्रियाकलाप</th>
                    <th colspan="5">हात धुने तरिका (कोडमा अंक लेख्ने)</th>
                </tr>
                <tr>
                    <td>कोड</td>
                    <td>१. साबुन+पानी</td>
                    <td>२. खरानी+पानी</td>
                    <td>३. पानी मात्र</td>
                    <td>४. नधुने</td>
                </tr>
                <?php 
                $handWashProcessArr = [
                    ' दिसा गरेपछि ',
                    ' खाना खानु अगाडि ',
                    'खाना खाएपछि',
                    ' बच्चालाई खाना ख्वाउनु अगाडि ',
                    ' बच्चाको दिसा धोइसकेपछि '                        
                ];                    
                $handWashProcessDetail = isset($hsHWP->hsHandWashProcessDetails) ? json_decode($hsHWP->hsHandWashProcessDetails) : '';

                for($count = 1; $count <= count($handWashProcessArr); $count++ ){
                    $handWashProcessSingle = moreHouseDetailsFormUpdate($count, $handWashProcessDetail, $compareJsonKey = 'handWashProcessRef');
                    ?>
                    <tr>
                        <td>
                            {{ $count }}
                        </td>
                        <td>
                            <input type="hidden" name="hsHandWashProcessDetails[<?php echo $count ?>][handWashProcessRef]" value="{{ $count }}">

                            <input type="checkbox" name="hsHandWashProcessDetails[<?php echo $count ?>][handWashProcessUsed]" value="1" {{ isset($handWashProcessSingle['handWashProcessUsed']) ? 'checked' : '' }} >                        
                        </td>
                        <td>
                            {{ $handWashProcessArr[$count - 1] }}
                        </td>
                        <td>
                            {{ Form::text("hsHandWashProcessDetails[$count][code]", isset($handWashProcessSingle['code']) ? $handWashProcessSingle['code'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'code', 'id' => '']) }}
                        </td>
                        <td>
                            <input type="checkbox" name="hsHandWashProcessDetails[<?php echo $count ?>][soapWater]" value="1" {{ isset($handWashProcessSingle['soapWater']) ? 'checked' : '' }} >

                        </td>
                        <td>
                            <input type="checkbox" name="hsHandWashProcessDetails[<?php echo $count ?>][ashWater]" value="1" {{ isset($handWashProcessSingle['ashWater']) ? 'checked' : '' }} >

                        </td>
                        <td>
                            <input type="checkbox" name="hsHandWashProcessDetails[<?php echo $count ?>][waterOnly]" value="1" {{ isset($handWashProcessSingle['waterOnly']) ? 'checked' : '' }} >

                        </td>
                        <td>
                            <input type="checkbox" name="hsHandWashProcessDetails[<?php echo $count ?>][noWash]" value="1" {{ isset($handWashProcessSingle['noWash']) ? 'checked' : '' }} >
                        </td>
                    </tr>
                    <?php
                }
                ?>      

            </table>
            <br>
        </div>     
        <div class="msgDisplay"></div>
    </div>
</div>
</div>