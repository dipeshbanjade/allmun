<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditanimal']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of animal is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmanimal']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of animal is required"
            }
        });
	var btnAddAnimal = $('.btnAddAnimal');
	btnAddAnimal.on('click', function(e){
		$('#animalModel').modal('show'); 
	});

	var btnEditanimal = $('.btnEditanimal');
	btnEditanimal.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.animalId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editanimalModel").modal('show');
			}
		})
	});

	var frmUpdateanimal = $('#frmUpdateanimal');
    frmUpdateanimal.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.animalId').val();
    var url  = "{{URL::to('/')}}" + "/setting/animal/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editanimalModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editanimalModel').modal('hide');
});
});
</script>