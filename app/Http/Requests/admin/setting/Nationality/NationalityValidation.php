<?php

namespace App\Http\Requests\admin\setting\Nationality;

use Illuminate\Foundation\Http\FormRequest;

class NationalityValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:30|unique:occupations',
            'nameEng' => 'required|min:3|max:30'
        ];
    }
}
