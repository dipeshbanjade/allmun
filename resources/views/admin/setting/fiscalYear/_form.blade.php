<div class="card-body row">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('startDate', null, ['class'=>'mdl-textfield__input txtstartDate', 'placeholder'=>'', 'id' => 'nepDate']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.extra.startDate')</label>
    </div>

    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('endDate', null, ['class'=>'mdl-textfield__input txtendDate', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.extra.endDate')</label>
    </div>   
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('startShort', null, ['class'=>'mdl-textfield__input txtstartShort', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.extra.startShort')</label>
    </div>   
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('endShort', null, ['class'=>'mdl-textfield__input txtendShort', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.extra.endShort')</label>
    </div>                                             
</div>