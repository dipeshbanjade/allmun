<?php

namespace App\model\admin\houseDetails\childCareInvolvement;

use Illuminate\Database\Eloquent\Model;

class HsFmMemWorkingDetail extends Model
{
	protected $table = 'hs_fm_mem_working_details';
}
