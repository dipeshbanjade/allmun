<?php

namespace App\Http\Requests\admin\setting\staffType;

use Illuminate\Foundation\Http\FormRequest;

class staffTypeValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'nameEng' => 'required|min:3|max:30'
        ];
    }
}
