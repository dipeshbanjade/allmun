<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Municipality - software</title>
    <link rel="shortcut icon" href="{{ asset('lib/img/npicon.png') }}" /> 
        
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('lib/css/extra_pages.css') }}" rel="stylesheet">
        
        {!! Html::style('lib/fonts/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('lib/bootstrap/bootstrap.min.css') !!}
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/style.css')}}">

    </head>
    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                        <div class="limiter" style="background: #FFF;">
                            <div class="container-login100 page-background">
                                <div class="wrap-login100">
                            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                 <span class="login100-form-logo">
                                     <img alt="" src="{{ asset('lib/img/nepal.png') }}">
                                 </span>
                                 <span class="login100-form-title p-b-34 p-t-27">
                                     Log in
                                 </span>
                                @csrf
                                <div class="form-group">
                                     <i class="fa fa-user"></i>
                                     <label for="simpleFormEmail">Username</label>
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group">
                                        <i class="fa fa-lock"></i>
                                        <label for="simpleFormPassword">Password</label>
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                </div>

                                <div class="form-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Login') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                            </div>
                            </div>
                            </div>
                </div>
            </div>
        </div>
    </body>
</html>
