<script type="text/javascript">
	
$(function(){
	/*add buttoin*/
	$("form[name='frmWardbirthVerify']").validate({
		rules:{
			refCode : {
				required : true,
				minlength : 3
			},
			issuedDate : {
				required : true,
				date : true
			},
			title:{
				required : true
			},
			Name:{
				required : true,				
				minlength : 3,
				maxlength : 30
			},
			appRelation:{
				required : true
			},
			fatherName:{
				required : true,
				minlength : 3,
				maxlength : 30
			},
			motherName:{
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipality:{
				required : true,
				minlength : 3,
				maxlength : 30
			},
			ward:{
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			district:{
				required : true,
				minlength : 3,
				maxlength : 30
			},
			former:{
				required : true
			},
			orgAddr:{
				required : true,
				minlength : 3,
				maxlength : 30
			},
			orgType:{
				required : true
			},
			orgWard:{
				required : true,
				minlength : 3,
				maxlength : 30
			},
			appNameOpt:{
				required : true,
				maxlength : 30
			},
			citizenIssuedDistrict:{
				required : true,
				minlength : 3,
				maxlength : 30
			},
			appTitleOption:{
				required : true
			},
			dateInBS : {
				required : true,
				date : true
			},
			dateInAD : {
				required : true,
				date : true
			},
			authorizedPerson:{
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			refCode : "RefCode is required",
			issuedDate : "Issue date is required",
			title : "Title is required",
			Name : "Name is required",			
			fatherName : "Fathername is required",
			motherName : "Mothername is required",
			municipality : "Municipality is required",
			ward : "Ward is required",
			district : "District is required",
			former : "Former is required",
			orgAddr : "Organization Address is required",
			orgType : "Organization Type  is required",
			orgWard : "Organization Ward is required",
			appNameOpt : "Applicant Name is required",
			citizenIssuedDistrict : "Citizen Issued District is required",
			appTitleOption : "App Title is required",
			dateInBS : "Date is required",
			dateInAD : "Date is required",
			authorizedPerson : "Date is required"
		}
	});
});

$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                  $("input[name='Name']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
            
          },complete:function(){
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>