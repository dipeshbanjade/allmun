<script type="text/javascript">

	var addButton = $('#jaggaBibaranAdd');
	var clickCount = 1;

	addButton.on('click', function(e){
		e.preventDefault();
		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var kittaNo = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="kittaNo[]">';

		var seatNo = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="seatNo[]">';

		var area = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="area[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+kittaNo+"</td>"
		+"<td>"+seatNo+"</td>"
		+"<td>"+area+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#jaggaBibaran').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});


	var addButton = $('#tinPusteAdd');
	var counter = 1;

	addButton.on('click', function(e){
		e.preventDefault();
		counter++;

		var serialNo = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + counter + '">';

		var name = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="name[]">';

		var relation = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="relation[]">';

		var nagarikata = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="nagarikata[]">';

		var dateOfIssue = '<input class="dashed-input-field ndp-nepali-calendar date-input-field" placeholder="  *" id="dateOfIssue'+ counter +'" required="required" onfocus="showNdpCalendarBox(\'dateOfIssue'+ counter +'\')" name="dateOfIssue[]" type="text" autocomplete="off">';

		var district = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="district[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNo+"</td>"
		+"<td>"+name+"</td>"
		+"<td>"+relation+"</td>"
		+"<td>"+nagarikata+"</td>"
		+"<td>"+dateOfIssue+"</td>"
		+"<td>"+district+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#tinPuste').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmFamilyHierarchy']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 10
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number: true
				},
				sabikAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				applicantName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
			},
			messages: {
				refCode : "Reference Code is required",
				issuedDate : "Issue Date is required",
				chalaniNum : "Chalani Number must be number",
				sabikAddress : "Address must be number",
				municipalityName : "Municipality is required",
				wardNumber : "Ward Number must be number",
				applicantName : "Applicant Name is required",
				authorizedPerson : "Authorized Person is required"
			}
		});
	});

	
	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='applicantName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>