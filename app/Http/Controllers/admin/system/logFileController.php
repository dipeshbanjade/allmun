<?php

namespace App\Http\Controllers\admin\system;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\admin\log\LogFile;
use Auth;

class logFileController extends Controller
{
    protected $viewPath = 'admin.system.logs';
    public function __construct()
    {
        $this->middleware('auth');

    }
    public function index()
    {
        $page['page_title']       = 'System log File';
        $page['page_description'] =  'list of all user log activities';

        $usrType = Auth::user()->userLevel;
        $logData = LogFile::with('users')->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

        switch ($usrType) {
            case 'dev':
                return view($this->viewPath . '.index', compact(['page', 'logData']));
                break;
            case 'mun':
                $logData = $logData->filter(function ($logData) {
                                         return $logData->users->userLevel == 'mun';
                                        })->values();
                return view($this->viewPath . '.index', compact(['page', 'logData']));
                break;

            case 'wrd':
                $logData = $logData->filter(function ($logData) {
                                         return $logData->users->userLevel == 'wrd' && $logData->users->wards_id === Auth::user()->wards_id;
                                        })->values();
                return view($this->viewPath . '.index', compact(['page', 'logData']));
                break;
            
            default:
               return redirect()->route('home')->withMessage(config('activityMessage.idNotFound'));
        }
    }
    public function destroy($id)
    {
       $delete = LogFile::findOrFail($id);
       $myDel = $delete->delete();
       return back()->withMessage(config('activityMessage.deleteMessage'));
    }
}
