<div class="container">
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12">
      <div class="box-body">
            <div class="form-group">
              <label>@lang('commonField.personal_information.nameNep')</label>
              {{ Form::text('nameNep', null, ['class'=> 'form-control txtNameNep', 'placeholder'=>'Company name nepali', 'title'=>'enter Company name']) }}
            </div>
      </div>
      <div class="box-body">
            <div class="form-group">
              <label>@lang('commonField.personal_information.nameEng')</label>
              {{ Form::text('nameEng', null, ['class'=> 'form-control txtNameEng', 'placeholder'=>'Company name english', 'title'=>'enter Company name']) }}
            </div>
      </div>
      <div class="box-body">
            <div class="form-group">
              <label>@lang('commonField.personal_information.phoneNumber')</label>
              {{ Form::text('phoneNumber', null, ['class'=> 'form-control txtPhoneNumber', 'placeholder'=>'10 digit phone number', 'title'=>'10 digit phone number']) }}
            </div>
      </div>

      <div class="box-body">
            <div class="form-group">
              <label>@lang('commonField.personal_information.landLineNumber')</label>
              {{ Form::text('landlineNumber', null, ['class'=> 'form-control txtLandLineNumber', 'placeholder'=>'Land line number', 'title'=>'9 digit Land line number']) }}
            </div>
      </div>

    </div>   
    <div class="col-md-6 col-lg-6 col-sm-12">
      <div class="box-body">
            <div class="form-group">
               <label>@lang('commonField.personal_information.email')</label>
              {{ Form::email('email', null, ['class'=> 'form-control txtEmail', 'placeholder'=>'Company email address', 'title'=>'enter email details']) }}
            </div>
      </div>

      <div class="box-body">
            <div class="form-group">
               <label>@lang('commonField.personal_information.address')</label>

              {{ Form::text('addr', null, ['class'=> 'form-control txtAddr', 'placeholder'=>'Company address', 'title'=>'enter Company address']) }}
            </div>
      </div>
      <div class="box-body">
            <div class="form-group">
               <label>@lang('commonField.personal_information.details')</label>

              {{ Form::textarea('companyDetails', null, ['class'=> 'form-control txtcompanyDetails', 'placeholder'=>'Company short Details', 'title'=>'company Details Details']) }}
            </div>
      </div>
    </div>
  </div>
</div>

