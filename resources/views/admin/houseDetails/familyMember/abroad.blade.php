<div class="card ">
      <div class="card-body">
          <div class="table-scrollable">
              <?php
                   // data come from controller to display added data in form
                   $abroadmem = isset($hsAbroadMem->hsAbroadDetails) ? json_decode($hsAbroadMem->hsAbroadDetails) : '';
              ?>
              @if(isset($getMyFamilyDetails) && $getMyFamilyDetails)
                    <table class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>@lang('commonField.extra.sn')</th>
                                <th>@lang('commonField.houseHoldSetting.hasGoneAbroad')</th>
                                <th  width="40%">@lang('commonField.personal_information.fullName')</th>
                                <th width="20%">@lang('commonField.address_information.country')</th>
                                <th width="20%">@lang('commonField.address_information.otherCountry')</th>
                                <th>अवधि</th>
                              </tr>
                        </thead>
                        <tbody>
                           <?php 
                               $abroadMember = getParentsName($hsDetails->citizen_infos_id);
                               $abCnt = 1;

                               $singleAbMem = moreHouseDetailsFormUpdate($hsDetails->citizen_infos_id, $abroadmem, $compareJsonKey = 'citizenId');  // 
                           ?>
                           <input type="hidden" name="hsAbroad[<?php echo $abCnt;  ?>][citizenId]" value="<?php echo $hsDetails->citizen_infos_id; ?>">
                           <tr>
                              <td>1.</td>
                              <td>
                                  <label class="radio-inline">
                                     <input type="checkbox" onchange="makeRequired(this)" class="hsAbroad" name="hsAbroad[<?php echo $abCnt  ?>][haveGone]" value="1" 
                                     {{ $singleAbMem['haveGone'] ==1 ? 'checked' : '' }}>
                                       @lang('commonField.extra.yes')
                                  </label>
                              </td>

                               <td>
                                   <?php 
                                        $fullName = $lan =='nep' ? $HouseOwnerDetails->fnameNep .''. $abroadMember->mnameNep .' '.$abroadMember->lnameNep :  $abroadMember->fnameEng .''. $abroadMember->mnameEng .' '.$abroadMember->lnameEng;
                                    ?>
                                    {{ Form::text("hsAbroad[$abCnt][fullName]", $fullName ? $fullName : '', ['class'=>'form-control', 'readonly']) }}
                               </td>
                               <td>
                                   <select name="hsAbroad[<?php echo $abCnt   ?>][country]" class="form-control abroadCountry">
                                     <option value="">@lang('commonField.address_information.country')</option>
                                     @if(count($countryId) > 0)
                                        @foreach($countryId as $countryName)
                                           <option value="{{ $countryName->id }}"
                                           {{ $singleAbMem['country'] == $countryName->id ? 'selected' : '' }}>
                                                {{ 
                                                   $lan == 'nep' ? $countryName->nameNep : $countryName->nameEng 
                                                }}
                                           </option> 
                                           <!-- <input type="hidden" name="hsAbroad[<?php echo $abCnt   ?>][countryName]" value="{{  $lan == 'nep' ? $countryName->nameNep : $countryName->nameEng  }}"> -->
                                        @endforeach
                                     @endif
                                   
                                   </select>                                   
                               </td>
                               <td>{{ Form::text("hsAbroad[$abCnt][otherCountry]", $singleAbMem['otherCountry'] ?  $singleAbMem['otherCountry'] : '', ['class'=>'mdl-textfield__input yearDuration', 'placeholder'=>'otherCountry']) }}</td>

                               <td>{{ Form::text("hsAbroad[$abCnt][duration]", $singleAbMem['duration'] ?  $singleAbMem['duration'] : '', ['class'=>'mdl-textfield__input yearDuration', 'placeholder'=>'year duration']) }}</td>
                               
                           </tr>
                           <!--  -->
                            <?php 
                                $abCnt = 2;
                                $dataCount = 1;

                                /*getMyFamilyDetails comes from controller */
                            ?>
                            @foreach($getMyFamilyDetails as $abroadMem)
                            <?php  
                               $dataCount ++;
                               $abMem = getParentsName($abroadMem->relation_id); 
                               $otherMemName =  $lan =='nep' ? $abMem->fnameNep .''. $abMem->mnameNep .' '.$abMem->lnameNep :  $abMem->fnameEng .''. $abMem->mnameEng .' '.$abMem->lnameEng; 

                                  $existsMem = moreHouseDetailsFormUpdate($abMem->id, $abroadmem, $compareJsonKey = 'citizenId');  
                            ?>
                            <input type="hidden"   name="hsAbroad[<?php echo $abCnt;  ?>][citizenId]" value="<?php echo $abMem->id;   ?>">
                             <!--  -->
                             <tr>
                                <td>{{ $abCnt ++ }}</td>
                                <td>
                                    <label class="radio-inline">
                                       <input type="checkbox" onchange="makeRequired(this)" name="hsAbroad[<?php echo $dataCount ?>][haveGone]" class="hsAbroad" value="1"
                                       {{ $existsMem['haveGone'] ==1 ? 'checked' : '' }}>
                                       @lang('commonField.extra.yes')
                                    </label>
                                </td>
                                 <td>
                                     {{ Form::text("hsAbroad[$dataCount][otherMemName]", $otherMemName ? $otherMemName : '', ['class'=>'form-control', 'readonly']) }}
                                 </td>
                                 <td>
                                     <select name="hsAbroad[<?php echo $dataCount ?>][country]" class="form-control abroadCountry">
                                       <option value="">@lang('commonField.address_information.country')</option>
                                         @if(count($countryId) > 0)
                                            @foreach($countryId as $countryName)
                                               <option value="{{ $countryName->id }}" <?php echo $existsMem['country'] == $countryName->id ? 'selected' : ''  ?>>
                                                    {{ 
                                                       $lan == 'nep' ? $countryName->nameNep : $countryName->nameEng 
                                                    }}
                                               </option> 
                                               <!-- <input type="hidden" name="hsAbroad[<?php echo $dataCount   ?>][countryName]" value="{{  $lan == 'nep' ? $countryName->nameNep : $countryName->nameEng  }}"> -->
                                            @endforeach
                                         @endif
                                     </select>
                                 </td>
                                 <td>{{ Form::text("hsAbroad[$dataCount][otherCountry]", $existsMem['otherCountry'] ?  $existsMem['otherCountry'] : '', ['class'=>'mdl-textfield__input yearDuration', 'placeholder'=>'otherCountry']) }}</td>
                                 
                                 <td>{{ Form::text("hsAbroad[$dataCount][duration]", $existsMem['duration'] ? $existsMem['duration'] : '' , ['class'=>'mdl-textfield__input yearDuration', 'placeholder'=>'year duration']) }}</td>

                            
                             </tr>
                            @endforeach
                        </tbody>
                    </table>
                 @else
                 <h4>Please insert memeber first</h4>
              @endif
          </div>
            <div class="msgDisplay"></div>
      </div>
  </div>
