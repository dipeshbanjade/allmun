<script type="text/javascript">
alert('working');

	$.validator.addMethod("minAge", function(value, element, min) {
		var today = new Date();
		var birthDate = new Date(value);
		var age = today.getFullYear() - birthDate.getFullYear();

		if (age > min+1) {
			return true;
		}

		var m = today.getMonth() - birthDate.getMonth();

		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		return age >= min;
	}, "You are not old enough!");


	$(function(){
		$("form[name='frmMunicipalityStaff']").validate({
			rules:{
				refCode:{
					required: true,
					minlength: 7
				},
				firstNameNep : {
					minlength : 3,
					maxlength : 30
				},
				wardNo : {
					required : true,
					number   : true
				},
				firstNameEng : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				lastNameNep : {
					minlength : 3,
					maxlength :30
				},
				lastNameEng : {
					required :true,
					minlength : 3,
					maxlength : 30
				},
				password : {
					required : true,
					minlength : 8,
					maxlength : 30
				},
				citizenNo : {
					required : true,
					minlength : 3,
					maxlength: 15
				},
				roles_id:{
					required: true
				},
				departments_id:{
					required: true
				},
				joingDateNep : {
					required : true,
					date: true
				},
				wardNo : {
					required : true,
					minlength: 1,
					maxlength: 3,
					number: true
				},
				villageNep : {
					minlength : 3,
					maxlength : 30
				},
				villageEng : {
					required : true,
					minlength : 5,
					maxlength : 30
				},
				phoneNumber : {
					required : true,
					number : true,
					minlength: 10,
					maxlength: 10
				},
				profilePic : {
					extension: "jpg|png|jpeg"
				},

				// citizenImagePath : {
				// 	extension: "jpg|png|jpeg"
				// },
				email:{
					email: true
				},
				staff_types_id: {
					required: true
				},
				deginations_id:{
					required: true
				},
				provinces_id:{
					required: true
				},
				districts_id:{
					required: true
				},
				dob:{
					required: true,
					// minAge: 18,
					date: true
				}
			},
			messages: {
				firstnameNep : "The First name in Nepali is required",
				firstnameEng : "The First name is required",
				lastNameEng : "The Last Name  is required",
				lastNameNep : "The Last Name in Nepali is required",
				password : "Enter a password with minimun 8 character long ",
				citizenNo : "The Citizen No field is required",
				joiningDate : "Joining Date is required",
				wardNo : "Ward Number field is required",
				villageEng : "Village Name field is required with minimun 3 character and maximum 30 character",
				phoneNumber : "Phone Number must be 10 digit",
				profilePic  : "file extension is not match",
				citizenImagePath : "select correct file format required",
				wardNo     : "ward number is needed with numberic value",
				email: "Email must be valid",
				roles_id: "Please select roles to assign",
				departments_id: "Please select department",
				staff_types_id: "Please select type of staff",
				deginations_id: "Please select type of degination",
				provinces_id: "Please select province",
				districts_id: "Please select district",
				joingDateNep : 'joining date is needed',
				dob: {
					required: "Please enter you date of birth.",
					// minAge: "You must be at least 18 years old!"
				},
			}
		});
		/*----------------------*/
	})
</script>