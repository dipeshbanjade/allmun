<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardaccountClose']").validate({
			rules:{
				refCode: {
					required: true,
					minlength: 3,
					maxlength: 20
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum: {
					required: true,
					minlength: 1,
					maxlength: 30					
				},
				accountHolderName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				accountHolderLocation : {
					required : true,
					minlength: 3,
					maxlength: 30
				},
				municipalityName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				wardNum : {
					required : true,
					minlength: 1,
					maxlength: 3,
					number: true
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				organizationType : {
					required : true
				},
				organizationWard : {
					required : true,
					minlength: 1,
					maxlength: 3,
					number: true
				},
				workName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				accountNum : {
					required : true,
					minlength: 7,
					maxlength: 20,
					number: true
				},
				authorizedPerson : {
					required : true,
					minlength: 3,
					maxlength: 20
				}
			},
			messages: {
				refCode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNum: "Chalani Number should be numeric",
				accountHolderName : "Account Holder Name is required",
				accountHolderLocation : "Account Holder Location is required",
				municipalityName : "Municipality Name is required",
				wardNum : "Ward Number should be numeric",
				organizationWard : "Organization Ward should be numeric",
				organizationAddress : "Organization Address is required",
				organizationType : "Organization Type is required",
				workName : "Work Name is required",
				accountNum : "Account Number should be numeric",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});
</script>