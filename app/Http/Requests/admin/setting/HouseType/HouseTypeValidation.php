<?php

namespace App\Http\Requests\admin\setting\HouseType;

use Illuminate\Foundation\Http\FormRequest;

class HouseTypeValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:30|unique:house_types',
            'houseEng' => 'required|min:3|max:100'
        ];
    }
}
