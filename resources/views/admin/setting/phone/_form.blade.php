<div class="card-body row">
   <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('refCode', isset($prefix) ? $prefix : '', ['class'=>'mdl-textfield__input txtRefCode', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">@lang('commonField.extra.refCode')</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('name', null, ['class'=>'mdl-textfield__input txtName', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">Name
        </label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('mobileTag', null, ['class'=>'mdl-textfield__input txtMobileTag', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">Brand Name</label>
    </div>

</div>