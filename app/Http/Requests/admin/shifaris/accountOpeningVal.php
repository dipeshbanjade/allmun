<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class accountOpeningVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:account_openings',
            'chalaniNum' => 'required|unique:account_openings',
            'issuedDate' => 'required',
            'nameOfOffice' => 'required', 
            'locationOfOffice' => 'required',
            'municipalityName' => 'required', 
            'wardNum' => 'required', 
            'organizationType' => 'required',
            'organizationAddress' => 'required',
            'organizationWard' => 'required',
            'workName' => 'required',
            'personName' => 'required',
            'personPost' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
