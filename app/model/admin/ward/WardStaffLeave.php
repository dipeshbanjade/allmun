<?php

namespace App\model\admin\ward;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\model\setting\LeaveType;

class WardStaffLeave extends Model
{
    protected $table = 'ward_staff_leaves';

    public function leaveTypes(){
    	return $this->belongsTo(LeaveType::class);
    }

    public function users(){
    	return $this->belongsTo(User::class);
    }
}
