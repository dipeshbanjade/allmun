<?php

namespace App\Http\Controllers\admin\shifaris\gharJagga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\InternalMigration;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\internalMigrationVal;
use DB;
use Auth;

use App\model\mylogic;

class internalMigrationController extends Controller
{

    protected $viewPath = 'admin.shifaris.gharJagga.internalMigration';
    protected $shifarisTableName     = 'internal_migrations';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;
    protected $refCode;

    public function __construct(){
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixInternalMigration');
        $this->refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['page_title']       = 'Internal Migration Shifarish';
        $page['page_description'] = 'Internal Migration Shifarish';


        $instRecomd = InternalMigration::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $page['page_title']       = 'Internal Migration Shifarish';
        $page['page_description'] = 'Internal Migration Shifarish';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 

        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(internalMigrationVal $request)
    {

        DB::beginTransaction();
        try {

            $internalMigrationCount = count($request->nameCaste);

            $nameCaste = $request->nameCaste;
            $relation = $request->relation;
            $citizenBirthNo = $request->citizenBirthNo;
            $houseNo = $request->houseNo;
            $streetName = $request->streetName;
            $age = $request->age;

            $internalMigrationArray = [];

            for ($i=0; $i < $internalMigrationCount; $i++) {
                array_push($internalMigrationArray, [
                    'nameCaste' => $nameCaste[$i],
                    'relation' => $relation[$i],
                    'citizenBirthNo' => $citizenBirthNo[$i],
                    'houseNo' => $houseNo[$i],
                    'streetName' => $streetName[$i],
                    'age' => $age[$i]
                ]);
            }
            $internalMigrationJson = json_encode($internalMigrationArray);

                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $this->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


            $isShifarisExist = isset($request->shifarisId) ? InternalMigration::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? InternalMigration::findOrfail($isShifarisExist['id']) : new InternalMigration;


            $save->createdBy = Auth::user()->id;

            $save->refCode = $request->refCode;

            $save->wards_id = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";

            $save->issuedDate = $request->issuedDate;

            $save->chalaniNumber = $request->chalaniNumber;
            $save->accountHolderName = $request->accountHolderName;
            $save->accountHolderAddress = $request->accountHolderAddress;
            $save->applicant = $request->applicant;
            $save->moveDate = $request->moveDate;
            $save->district = $request->district;
            $save->municipalityName = $request->municipalityName;
            $save->wardFrom = $request->wardFrom;
            $save->municipalityName2 = $request->municipalityName2;
            $save->ward2 = $request->ward2;
            $save->address = $request->address;
            $save->dataTable = $internalMigrationJson;
            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;
            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
            $request->chalaniNum = $request->chalaniNumber;
            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
               DB::commit();
               return response()->json([
                   'success' => true,
                   'message' => config('activityMessage.saveMessage')
               ]);
                   // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
           }else{
               return response()->json([
                   'success' => false,
                   'message' => config('activityMessage.unSaveMessage')
               ]);
                   // return back()->withMessage(config('activityMessage.unSaveMessage'));
           }
       }catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
    }

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
