<script type="text/javascript">
$(function(){
  $("form[name='frmMarriageDetail']").validate({

  });
}); 

// Frm Name fmMarriageDetail

function displayMarriageDetailTbl(select){
	if(select.value == 1){
		$(".marriageDetailTbl").show();
	}else{
		$(".marriageDetailTbl").hide();
		$("input[name^=marriageDetail]").prop("checked",false);
		$("input[name^=marriageDetail]").filter("input[name*='[marriedDate]']").each(function(){
			$(this).val("");
		});
		$("input[name^=marriageDetail]").filter("input[name*='[ageAtMarriage]']").each(function(){
			$(this).val("");
		});
	}
}

// Require Marriage Age and Date 
function requireMarriageDetail(select){
	var index= $('.isMarried').index(select) +1;
	if(select.checked == true){
		$('input[name^="marriageDetail['+index+'][ageAtMarriage]"]').rules('add',{
					required:true,
					number:true,
					messages:{
						required:"Marriage age is required"
					}
				});
		$('input[name^="marriageDetail['+index+'][marriedDate]"]').rules('add',{
					required:true,
					date:true,
					messages:{
						required:"Marriage Date is required"
					}		
			});		
	}else{
		$('input[name^="marriageDetail['+index+'][ageAtMarriage]"]').rules('add',{
					required:false,
				});
		$('input[name^="marriageDetail['+index+'][marriedDate]"]').rules('add',{
					required:false,
			});
		$('input[name^="marriageDetail['+index+'][ageAtMarriage]"]').val('');
		$('input[name^="marriageDetail['+index+'][marriedDate]"]').val('');
	}
	
}

/*---------------FmLivingDetail-----------------*/
function displayFmLivingTbl(select){
	if(select.value == 1){
		$(".fmLivingTbl").show();
	}else{
		$("input[name^=familyLivingDetails]").prop('checked',false);
		$(".fmLivingTbl").hide();
	}
}

/*---------------------------fmOtherMemLiving--------------------*/

function displayOhterMemTbl(select){
	if(select.value == 1){
		$(".otherMemLivingTbl").show();
	}else{
		$(".otherMemLivingTbl").hide();
		$("input[name^=otherLivingDetailOne]").val("");
	}	
}

function displayWorkingTbl(select){ // display below 18 working table
	if(select.value == 1){
		$(".workingTbl").show();
	}else{
		$(".workingTbl").hide();
		$("input[name^=otherLivingDetailTwo").val("");

	}
}

/*----------------------fmWorkersDetail------------------*/
function displayFmWorkingTbl(select){
	if(select.value== 1){
	 $(".fmWorkingDetailTbl").show();
	}else{
	 $(".fmWorkingDetailTbl").hide();
	 $("input[name^=familyWorkingDetails]").prop("checked",false);
	 $("input[name*=jobStartDate]").val("");
	 $("input[name*=salaryBased]").val("");
	 $("input[name*=workType]").val("");
	 $("input[name*=monthlySalary]").val("");
	 $("input[name*=muliName]").val("");
	 $("input[name*=wardNo]").val("");
	}
}

//
function requireWorkingDetail(select){
	var name= select.name;
	var index= name.substr(21,1);
	var familyWorkingDetails= $("input[name^=familyWorkingDetails]")
	$("form[name='frmFamilyMemWorkDetail']").validate();		

	if(select.checked== true){
		familyWorkingDetails.filter('input[name*="['+index+'][jobStartDate]"]').each(function(){			
			$(this).rules("add", {
		       required: true,
		       messages:{
		       				required:"Working start date is required"
		       			}
		    });
 		});		
 		familyWorkingDetails.filter('input[name*="['+index+'][salaryBased]"]').each(function(){			
			$(this).rules("add", {
		       required: true,
		       number:true,
		       messages:{
		       		required:"Salray type is required"
		       	}
		    });
 		});
 	
 		familyWorkingDetails.filter('input[name*="['+index+'][workType]"]').each(function(){			
			$(this).rules("add", {
		       required: true,
		       messages:{
       				required:"Work type is required"
       			}
		    });
 		});
 		familyWorkingDetails.filter('input[name*="['+index+'][monthlySalary]"]').each(function(){			
			$(this).rules("add", {
		       required: true,
		       number:true,
		       messages:{
       				required:"Work type is required"
       			}
		    });
 		});
 		familyWorkingDetails.filter('input[name*="['+index+'][muliName]"]').each(function(){			
			$(this).rules("add", {
		       required: true,
		       minlength:3,
		       maxlength:50,
		       messages:{
       				required:"Name is required"
       			}
		    });
 		});
 		familyWorkingDetails.filter('input[name*="['+index+'][wardNo]"]').each(function(){			
			$(this).rules("add", {
		       required: true,
		       number:true,
		       messages:{
       				required:"Ward number is required"
       			}
		    });
 		});
 		 	 	
	}else
	{
		familyWorkingDetails.filter('input[name*="['+index+'][jobStartDate]"]').each(function(){			
			$(this).rules("add", {
		       required: false
		    });
		    $(this).val('');
 		});		
 		familyWorkingDetails.filter('input[name*="['+index+'][salaryBased]"]').each(function(){			
			$(this).rules("add", {
		       required: false,
		    });
		    $(this).val('');
 		});
 	
 		familyWorkingDetails.filter('input[name*="['+index+'][workType]"]').each(function(){			
			$(this).rules("add", {
		       required: false,
		    });
		    $(this).val('');
 		});
 		familyWorkingDetails.filter('input[name*="['+index+'][monthlySalary]"]').each(function(){			
			$(this).rules("add", {
		       required: false,
		    });
		    $(this).val('');
 		});
 		familyWorkingDetails.filter('input[name*="['+index+'][muliName]"]').each(function(){			
			$(this).rules("add", {
		       required: false,
		    });
		    $(this).val('');
 		});
 		familyWorkingDetails.filter('input[name*="['+index+'][wardNo]"]').each(function(){			
			$(this).rules("add", {
		       required: false,
		    });
		    $(this).val('');
 		});
	}
}
</script>