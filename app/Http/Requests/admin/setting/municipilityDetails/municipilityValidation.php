<?php

namespace App\Http\Requests\admin\setting\municipilityDetails;

use Illuminate\Foundation\Http\FormRequest;

class municipilityValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'nameNep'        => 'required|min:3|max:60',
            'nameEng'        => 'required|min:3|max:60',
            'landLineNumber' => 'required|min:7|max:10',
            'addrNep'        => 'required|min:3|max:60',
            'provinces_id'   => 'required',
            'districts_id'   => 'required'
        ];
    }
}
