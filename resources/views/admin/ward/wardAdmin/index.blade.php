@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12">
    <div class="card-box">
      <div class="card-head">
       <header><i class="fa fa-eye"><span>@lang('commonField.extra.wardAdminList')</span></i></header>
       <a href="{{ route('createWardAdmin') }}" class="btn btn-primary btn-xs btn btn-default btn-lg m-b-10 pull-right"><i class="fa fa-plus"><span>Add</span></i></a>
     </div>
     <div class="card-body ">
      <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
        <!-- table start  -->
        <?php     
             $lan = getLan();
  
        ?>
   
        <div class="table-scrollable">
           <table class="table table-striped table-bordered table-hover" id="example4">
             <thead>
               <tr>
                 <th width=6%>@lang('commonField.extra.sn')</th>
                 <th>@lang('commonField.address_information.wardNo')</th>
                 <th>@lang('commonField.personal_information.name')</th>
                 <th>@lang('commonField.personal_information.username')</th>
                 <th>@lang('commonField.extra.role')</th>
                 <th>@lang('commonField.extra.action')</th>
               </tr>
             </thead>
               <tbody>
                   <?php $count = 1;   ?>
                   @if(count($wardAdmin) > 0)
                      @foreach($wardAdmin as $adm)
                         <?php $wardDetails = getWardDetails($adm->wards_id)   ?>
                         <tr>
                            <td>{{ $count ++ }}</td>
                            <td>
                            @if($lan == 'np')
                                {{ isset($adm->wards_id) ? $wardDetails->nameNep : '' }}
                                @else
                                {{ isset($adm->wards_id) ? $wardDetails->nameEng : '' }}
                                
                            @endif

                            </td>
                            <td>{!! $adm->name !!}</td>
                            <td>{{ $adm->email }}</td>
                            <td>{{ isset($adm->roles) ? $adm->roles->roleName : '' }}</td>
                            <td>
                               @if($adm->idEncrip)
                                <a href="{{ route('editWardAdmin', $adm->idEncrip) }}" class="btn btn-success btn-xs" title="edit ward admin details">
                                  <i class="fa fa-edit"></i>
                                </a>

                                <a href="{{ route('adminChangePwd', $adm->idEncrip) }}" class="btn btn-primary btn-xs" title="change password">
                                    <i class="fa fa-lock"></i>
                                </a> 
                               @endif

                                <a href="javascript:void(0)" class="btn btn-danger btn-xs">
                                  <i class="deleteMe fa fa-trash-o" data-url="{{ route('deleteWardAdmin', $adm->id) }}" data-name="{{ $adm->email }}"></i>
                                </a>
                            </td>
                         </tr>
                      @endforeach
                   @endif
               </tbody>
             </table>
             </div>
            {{ $wardAdmin->links() }}
         </div>
       </div>
     </div>
   </div>
@endsection