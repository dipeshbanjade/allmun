  <script type="text/javascript">
  	$(function(){
  		/*add buttoin*/
  		$("form[name='frmedithouseRoof']").validate({
  			rules:{
  				nameEng : {
  					required : true,
  					minlength : 3,
  					maxlength : 30
  				}
  			},
  			messages: {
  				nameEng : "The name of House Roof is required"
  			}
  		});
  	});
  	$(function(){
  		/*add buttoin*/
  		$("form[name='frmhouseRoof']").validate({
  			rules:{

  				nameEng : {
  					required : true,
  					minlength : 3,
  					maxlength : 30
  				}
  			},
  			messages: {

  				nameEng : "The name of House Roof is required"
  			}
  		});
  		var btnAddHouseRoof = $('.btnAddHouseRoof');
  		btnAddHouseRoof.on('click', function(e){
  			$('#houseRoofModel').modal('show'); 
  		});

  		var btnEdithouseRoof = $('.btnEdithouseRoof');
  		btnEdithouseRoof.on('click', function(e) {

  			e.preventDefault();
  			var url = $(this).data('url');
  			$.ajax({
  				'type': 'GET',
  				'url': url,
  				success: function (response) {
  					$('.houseRoofId').val(response.id);
  					$('.txtRefCode').val(response.refCode).attr('disabled',true);
  					$('.txtNameNep').val(response.nameNep);
  					$('.txtNameEng').val(response.nameEng);
  					$("#edithouseRoofModel").modal('show');
  				}
  			})
  		});

  		var frmUpdatehouseRoof = $('#frmUpdatehouseRoof');
  		frmUpdatehouseRoof.on('submit', function(e){
  			e.preventDefault();
  			var data = $(this).serialize();
  			var id   = $('.houseRoofId').val();
  			var url  = "{{URL::to('/')}}" + "/disable/"+id+"/update";
  			$.ajax({
  				'type' : 'POST',
  				'url'  : url,
  				'data'    : data,
  				success : function(response){
  					console.log(response);
  					if (response.success==true) {
  						$('.infoDiv').append(response.message).addClass('alert alert-success');
  					}
  					if (response.success==false) {
  						$.each(response.message, function(key, value){
  							$('.infoDiv').append(value).addClass('alert alert-danger');
  						})
  					}
  				},complete:function(){
  					$("#edithouseRoofModel").modal('hide');
  					setTimeout(location.reload.bind(location), 1500);
  				}
  			})
  			.fail(function (response) {
  				alert('data already exists');
  			});
  			$('#edithouseRoofModel').modal('hide');
  		});
  	});
  </script>
