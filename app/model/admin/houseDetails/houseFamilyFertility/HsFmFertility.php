<?php

namespace App\model\admin\houseDetails\houseFamilyFertility;

use Illuminate\Database\Eloquent\Model;

class HsFmFertility extends Model
{
	protected $table = 'hs_fm_fertilities';
}
