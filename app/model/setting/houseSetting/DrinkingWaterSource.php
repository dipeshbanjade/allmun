<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class DrinkingWaterSource extends Model
{
	protected $table = 'drinking_water_sources';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
