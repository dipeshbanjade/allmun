<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php
                $wildAnimalProblem     = isset($wildAnimalProblem) ? $wildAnimalProblem : '';
                $jsonWildAnimalProblem = isset($wildAnimalProblem->problemFace) ? json_decode($wildAnimalProblem->problemFace) : '';
                // dd($jsonWildAnimalProblem);
            ?>
            <div class="row color_red">
                <div class="form-group col-md-12">
                    <p>
                       ४. तपाईको परिवारले विगत १ वर्ष भित्र वन्यजन्तुको कारणबाट समस्या भोग्नुपरेको छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" name="haveWildAnimalPro" value="1" class="rdoClickMe" {{ isset($wildAnimalProblem->haveWildAnimalPro) && $wildAnimalProblem->haveWildAnimalPro == 1 ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" name="haveWildAnimalPro" value="0" class="rdoClickMe" {{ isset($wildAnimalProblem->haveWildAnimalPro) && $wildAnimalProblem->haveWildAnimalPro == 0 ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline col-md-12">
                          <p>
                              यदि छ भने, बन्यजन्तुको कारणले कस्तो कस्तो किसिमको समस्या भोग्नु परेको छ ? 
                          </p>
                        </label>
                        <label class="radio-inline">
                            <input type="checkbox" name="problemFace[]" value="परिवारका सदस्यको मृत्यु" 
                            {{ !empty($wildAnimalProblem->problemFace) && in_array('परिवारका सदस्यको मृत्यु', $jsonWildAnimalProblem) ? 'checked' : '' }}>
                            १. परिवारका सदस्यको मृत्यु
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="problemFace[]" value="परिवारका सदस्यलाई आक्रमण । अंगभंग । घाइते" {{ !empty($wildAnimalProblem->problemFace) && in_array('परिवारका सदस्यलाई आक्रमण । अंगभंग । घाइते', $jsonWildAnimalProblem) ? 'checked' : '' }}>
                            २. परिवारका सदस्यलाई आक्रमण । अंगभंग । घाइते
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="problemFace[]" value="घरको क्षति" {{ !empty($wildAnimalProblem->problemFace) && in_array('घरको क्षति', $jsonWildAnimalProblem) ? 'checked' : '' }}>
                            ३. घरको क्षति
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="problemFace[]" value="चौपायाको अंगभंग । घाइते" {{ !empty($wildAnimalProblem->problemFace) && in_array('चौपायाको अंगभंग । घाइते', $jsonWildAnimalProblem) ? 'checked' : '' }}>
                            ५. चौपायाको अंगभंग । घाइते 
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="problemFace[]" value="">
                            ६. बालीनालीको क्षति
                        </label><br />
                        <label class="radio-inline">
                             {{ Form::text('otherProblemFace', isset($wildAnimalProblem->otherProblemFace) ? $wildAnimalProblem->otherProblemFace : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
                        </label>
                    </div>
            <div class="msgDisplay"></div>
                    
                </div> 
            </div>
    </div>
</div>