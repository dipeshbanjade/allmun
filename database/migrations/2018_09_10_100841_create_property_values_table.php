<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_values', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->string('refCode', 100)->unique();

            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->date('issuedDate');
            $table->text('chalaniNum');
            $table->enum('title', ['Mr', 'Mrs', 'Miss']);
            $table->string('personName');
            $table->string('municipality');
            $table->string('ward');
            $table->string('district');
            $table->enum('former', ['Former', 'Previously designated as']);
            $table->string('orgAddr');
            $table->enum('orgType', ['V.D.C', 'Municipality', 'Sub- Metropolitian', 'Metropolitian']);
            $table->string('orgWard');
            $table->text('propertyStatement');
            
            $table->string('totalPropertyValueInNrs');
            $table->string('perDollarPriceNpr');
            $table->string('ctype')->nullable();
            $table->enum('currencyType', ['USD($)', 'AUD($)', 'Pound(£)', 'Euro(€)']);
            $table->string('totalPropertyValueInDollar');

            $table->string('authorizedPerson');

            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');

            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_values');
    }
}
