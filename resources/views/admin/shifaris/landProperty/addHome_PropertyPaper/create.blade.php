@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Add House in Property Paper</header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>
	

	{{ Form::open(['route' => 'admin.addHomePropertyPaper.store', 'name' => 'frmWardaddHome_PropertyPaper']) }}
	@include('admin.shifaris.landProperty.addHome_PropertyPaper._form')
	<!-- <p class="pull-right">
		<button type="submit" class="btn btn-success">CREATE</button>
		<button type="submit" class="btn btn-active" onclick="">PRINT</button>
	</p> -->
	@include('admin.shifaris.commonButton')
	{{ Form::close() }}
</div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
@include('admin.shifaris.script')
@include('admin.shifaris.landProperty.addHome_PropertyPaper.script')
@endsection