<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class TaxType extends Model
{
    protected $table = 'tax_types';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
