<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="" class="gharBibaran">
            <div class="row color_red">
                <div class="form-group col-md-12">
                    <p>
                       ६. तपाईको विचारमा यस स्थानमा जातजाति, लिगं वा समुदाय विशेष (जस्तै महिला, दलित, आदि ...) भएकै आधारमा भेदभावको अनुभव गर्नु भएको छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline">
                            १. छ
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline">
                            २. छैन
                            <input type="radio" name="radio" value="">
                        </label>
                    </div>
                </div> 
            </div>
        </form>
    </div>
</div>