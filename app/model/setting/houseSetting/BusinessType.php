<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
    protected $table = 'business_types';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
