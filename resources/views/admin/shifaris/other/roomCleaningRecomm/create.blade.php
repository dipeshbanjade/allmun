@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Room Cleaning Recommendation<small> कोठा खाली गर्ने सिफारिस</small></header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>
	<div class="right_col nep" role="main">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h3 class="title-x">कोठा खाली गर्ने सिफारिस</h3>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" id="block">
						@include('admin.shifaris.municipalityDetail')
						
						{{ Form::open(['route' => 'admin.roomCleaningRecomm.store', 'name' => 'frmWardroomCleaningRecomm','class'=>'sifarishSubmitForm']) }}
						@include('admin.shifaris.other.roomCleaningRecomm._form')
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- <p class="pull-right">
					<button type="submit" class="btn btn-success">CREATE</button>
					<button type="submit" class="btn btn-active" onclick="">PRINT</button>
				</p> -->
				@include('admin.shifaris.commonButton')
				{{ Form::close() }}
				<div class="msgDisplay" style="z-index: 999999 !important"></div>

			</div>
		</div>
	</div>
</div>
@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
@include('admin.shifaris.script')
@include('admin.shifaris.other.roomCleaningRecomm.script')

@endsection