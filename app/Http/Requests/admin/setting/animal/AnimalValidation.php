<?php

namespace App\Http\Requests\admin\setting\animal;

use Illuminate\Foundation\Http\FormRequest;

class AnimalValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:animals',
            'nameEng' => 'required|min:4|max:50'
        ];
    }
}
