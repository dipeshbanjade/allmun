<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
            <div class="row color_red">
                <div class="form-group col-md-6">{{-- 7 --}}                                
                    <p>
                        ७. तपाईको परिवारमा खाना पकाउन प्रयोग गर्ने मुख्य इन्धन कुन हो?
                    </p> <br>
                    @if (isset($cookingFuel) && count($cookingFuel) > 0)                            
                    @foreach ($cookingFuel as $cFuel)
                    <label class="radio-inline">
                        <input type="radio" class="rdoClickMe" title="double click for reset" name="cookingFuelNam" value="{{$cFuel->nameNep}}" {{ isset($hsKitchenData) && $hsKitchenData->cookingFuelNam == $cFuel->nameNep ? 'checked="checked"' : ''  }}>
                        {{$cFuel->nameNep}}
                    </label>
                    <br />
                    @endforeach
                    @else
                    <label>Insert data from setting</label>
                    @endif
                    {{ Form::text('otherCookingFuelNam', isset($hsKitchenData->otherCookingFuelNam) ? $hsKitchenData->otherCookingFuelNam : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'others', 'id' => '']) }}

                </div>

                <div class="form-group col-md-6">{{-- 8 --}}
                    <p>
                        ८. तपाईको परिवारमा खाना पकाउन मुख्य रुपमा कुन प्रकारको चुल्हो प्रयोग गर्नुहुन्छ ?<br>
                    </p> <br>
                    
                    @if (isset($ovenType) && count($ovenType) > 0)
                    @foreach ($ovenType as $oType)
                    <label class="radio-inline">
                        <input type="radio" class="rdoClickMe" title="double click for reset" name="ovenTypeNam" value="{{$oType->nameNep}}" {{ isset($hsKitchenData) && $hsKitchenData->ovenTypeNam == $oType->nameNep ? 'checked="checked"' : ''  }}>
                        {{$oType->nameNep}}
                    </label> 
                    <br />
                    @endforeach
                    @endif        

                    {{ Form::text('otherOvenTypeNam', isset($hsKitchenData->otherOvenTypeNam) ? $hsKitchenData->otherOvenTypeNam : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'others', 'id' => '']) }}
                </label>
                <br>
            </div>
            <div class="msgDisplay"></div>
        </div>
    </div>
</div>