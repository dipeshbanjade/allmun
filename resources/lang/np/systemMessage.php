<?php
   return [
        'crudMessage' => [
          'createMessage' => 'सन्देश सिर्जना गर्नुहोस्',
          'updateMessage' => 'सन्देश अपडेट गर्नुहोस्',
          'deleteMessage' => 'सन्देश मेट्नुहोस्'
        ], 
        'system_message' => [
        ]
   ];
?>