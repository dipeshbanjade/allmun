<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditvehicle']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of fuel is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmfuel']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of fuel is required"
            }
        });
	var btnAddFuel = $('.btnAddFuel');
	btnAddFuel.on('click', function(e){
		$('#fuelModel').modal('show'); 
	});

	var btnEditFuel = $('.btnEditFuel');
	btnEditFuel.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.fuelId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editfuelModel").modal('show');
			}
		})
	});

	var frmUpdatefuel = $('#frmUpdatefuel');
  frmUpdatefuel.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.fuelId').val();
    var url  = "{{URL::to('/')}}" + "/setting/fuel/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editfuelModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editfuelModel').modal('hide');
});
});
</script>