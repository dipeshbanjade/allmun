<div class="right_col nep" role="main" style="min-height: 882px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">पुर्जामा घर कायम गर्न सिफारिस</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
         @include('admin.shifaris.municipalityDetail')
           
       <div class="row top-part">
        <div class="col-md-6">
         <p align="left"><b class="ps">प. सं.:<input type="text" class="dashed-input-field" placeholder="   *" name="refCode" id="refCode" value="{{ isset($refCode) ? $refCode : '' }}" required="required"></b></p>
         <p align="left"><b class="cn">च. नं.:<input type="text" class="dashed-input-field" name="chalaniNumber" id="chalaniNumber"></b></p>
       </div>
       <div class="col-md-6">
        <p align="right"><b class="mt">मिति :<input type="text" class="dashed-input-field ndp-nepali-calendar date-input-field" placeholder="   *" name="issuedDate" id="issued_date" required="required" onfocus="showNdpCalendarBox('issued_date')"></b></p>
      </div>
    </div>
    <div class="row title-left">
     <div class="col-md-12">
      <p align="left">श्री
        <input type="text" class="dashed-input-field" placeholder="   *" name="officeName" id="officeName" value="" required="required">,</p>
        <p align="left"><input type="text" class="dashed-input-field" placeholder="   *" name="officeAddress" id="officeAddress" required="required">।</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p align="center" class="font-size-24">
        </p><h4 align="center"><b> विषय: सिफारिस सम्बन्धमा।
          <input type="hidden" name="letter_subject" value="सिफारिस सम्बन्धमा">
        </b> </h4>  
        <p></p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 content-para">
        <p>उपरोक्त सम्बन्धमा मेरो/हाम्रो नाममा एकलौटी/संयुक्त दर्ता श्रेष्ता भएको वडा नं. <b><input type="text" class="dashed-input-small-field" placeholder="   *" name="wardNumber" value="" required="required"></b> (साबिकको ठेगाना 
          <input type="text" class="dashed-input-field star" placeholder="   *" name="organizationAddress" id="organizationAddress" required="required"> कि.नं. <input type="text" class="dashed-input-field star" placeholder="   *" name="" id="" required="required"> क्षे.फ. <input type="text" class="dashed-input-field star" placeholder="   *" name="" id="" required="required"> जग्गाको जग्गाधनी श्रेष्ता पुर्जामा जग्गा मात्र भएको तर फिल्डमा घर भएकोले जग्गाधनी श्रेष्ता पुर्जामा घर कायम गर्न सिफारिस गरी पाऊँ भनी घरजग्गाधनी श्री/सुश्री/श्रीमती <input type="text" class="dashed-input-field star" placeholder="   *" name="" id="" required="required"> ले यस वडा कार्यालयमा निवेदन दिनुभएको हुँदा सो सम्बन्धमा त्यहाँको नियमानुसार जग्गाधनी प्रमाण पुर्जामा घर कायम गरिदिनुहुन सिफारिस गरिन्छ।
    </div>
    <div class="text-right btm-last">
      <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required"></p>
      <p> <b> 
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b> </p>
    </div>
    <!--Nibedak Block  -->
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr>
    @include('admin.shifaris.nibedakCommonField')
 <!-- END -->
 <div class="col-md-12">
  <hr>
</div>
</div>
</div>
</div>
</div>