<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\setting\Province;
use App\Http\Requests\admin\setting\province\provinceValidation;
use Validator;
use Auth;

class provincesController extends Controller
{
    protected $viewPath = 'admin.setting.province';

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $page['page_title']       = 'List Of Province';
        $page['page_description'] = 'list of all province in nepal';

        $allProvince = Province::orderby('created_at', 'ASC')->where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

        return view($this->viewPath . '.index', compact(['page', 'allProvince']));


    }

    public function store(provinceValidation $request)
    {
        if (Auth::user()->userLevel === 'dev') {
            $mySave = Province::create([
                     'name'          => $request->name,
                     'capital'       => $request->capital,
                     'governor'      => $request->governor,
                     'chiefMinister' => $request->chiefMinister,
                     'districtNo'    => $request->districtNo,
                     'status'        => 0,
                     'softDelete'    => 0 
                     ]);

            if ($mySave) {
                return back()->withMessage(config('activityMessage.saveMessage'));
            }else{
                return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }else{
             return back()->withMessage(config('activityMessage.accessDenied'));
        }
    }

    public function edit($id)
    {
         $page['page_title']       = 'Edit Province';
         $page['page_description'] = 'edit province';

        if($id){
            if (Auth::user()->userLevel === 'dev'  || Auth::user()->userLevel === 'mun') {  // only developer and municipility user can edit province form
                $data = Province::findOrFail($id);
                return view($this->viewPath . '.edit', compact(['page', 'data']));
            }else{
                return back()->withMessage(config('activityMessage.unAuthorized'));
            }
        }else{
                return back()->withMessage(config('activityMessage.idNotFound'));
        }
       
    }


    public function update(provinceValidation $request, $id)
    {
        if (Auth::user()->userLevel === 'dev'  || Auth::user()->userLevel === 'mun') {
           $update = Province::findOrFail($id);
           $update->name              = $request->name; 
           $update->capital           = $request->capital; 
           $update->governor          = $request->governor; 
           $update->chiefMinister     = $request->chiefMinister; 
           $update->districtNo         = $request->districtNo;

           $myUpdate = $update->update();
           if ($myUpdate) {
               return redirect()->route('admin.province.index')->withMessage(config('activityMessage.updateMessage') . $request->name);
           }else{
            return back()->withInput()->withMessage(config('activityMessage.notUpdated'));
           }
        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.idNotFound'));
        }
    }
    public function destroy($id)
    {
        //
    }
}
