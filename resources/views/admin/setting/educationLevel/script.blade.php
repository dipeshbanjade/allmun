<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmediteducationLevel']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of education level is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmeducationLevel']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of education level is required"
            }
        });
	var btnAddeducationLevel = $('.btnAddeducationLevel');
	btnAddeducationLevel.on('click', function(e){
		$('#educationLevelModel').modal('show'); 
	});

	var btnEditeducationLevel = $('.btnEditeducationLevel');
	btnEditeducationLevel.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.educationLevelId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editeducationLevelModel").modal('show');
			}
		})
	});

	var frmUpdateeducationLevel = $('#frmUpdateeducationLevel');
  frmUpdateeducationLevel.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.educationLevelId').val();
    var url  = "{{URL::to('/')}}" + "/setting/educationLevel/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editeducationLevelModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editeducationLevelModel').modal('hide');
});
});
</script>