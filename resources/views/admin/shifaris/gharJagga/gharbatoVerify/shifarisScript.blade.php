<script type="text/javascript">
	var btnAddTableRow = $('#addTableRow');
	var clickCount = 1;
	btnAddTableRow.on('click', function(e){
		e.preventDefault();

		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var wardNo = '<input type="text" class="dashed-input-small-field" required="required" placeholder="   *" name="wardNo[]">';

		var seatNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="seatNo[]"> ';

		var kittaNo = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="kittaNo[]"> ';

		var area = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="area[]">';

		var gharStatus = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="gharStatus[]">';

		var streetType = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="streetType[]">';

		var remarks = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="remarks[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+wardNo+"</td>"
		+"<td>"+seatNo+"</td>"
		+"<td>"+kittaNo+"</td>"
		+"<td>"+area+"</td>"
		+"<td>"+gharStatus+"</td>"
		+"<td>"+streetType+"</td>"
		+"<td>"+remarks+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#house_kayem_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardgharbatoVerify']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 15
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required: true,
					minlength: 1				
				},
				accountHolderName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				accountHolderAddress : {
					required : true,
					minlength: 5,
					maxlength: 255
				},
				municipalityName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				sabikAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				shrestaName : {
					required : true,
					minlength: 3,
					maxlength : 255
				},
				authorizedPerson : {
					required : true,
					minlength: 3,
					maxlength: 20
				}
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Num must be numeric",
				accountHolderName : "Applicant name is required",
				accountHolderAddress : "Applicant address is required",
				municipalityName : "Municipality name is required",
				wardNumber : "Ward Number should be numeric",
				sabikAddress : "Sabik Address is required",
				shrestaName : "Shresta Name is required",
				authorizedPerson : "Authorizing person is required"
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='shrestaName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>