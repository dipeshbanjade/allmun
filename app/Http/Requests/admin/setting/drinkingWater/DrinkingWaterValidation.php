<?php

namespace App\Http\Requests\admin\setting\drinkingWater;

use Illuminate\Foundation\Http\FormRequest;

class DrinkingWaterValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:drinking_waters',
            'nameEng' => 'required|min:4|max:50'
        ];
    }
}
