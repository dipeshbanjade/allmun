<script type="text/javascript">
	var btnAddTableRow = $('#btnAddTableRow');
	var clickCount = 1;
	btnAddTableRow.on('click', function(e){
		e.preventDefault();

		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var tole = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="tole[]"> ';

		var streetName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="streetName[]">';

		var houseNo = '<input type="text" class="dashed-input-field" required="required" name="houseNo[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+tole+"</td>"
		+"<td>"+streetName+"</td>"
		+"<td>"+houseNo+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#residentTable').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/

		$("form[name='formPermanentResident']").validate({
			rules:{
				refCode:{
					required: true,
					minlength: 2,
					maxlength: 10
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength: 1,
					maxlength: 10,
					number: true
				},
				residentApplicant : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				nijName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				sabikAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				bigatDate : {
					required : true,
					date: true
				},
				nagarikata : {
					required : true,
					minlength : 10,
					maxlength : 30
				},
				residentDistrict : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDateTwo : {
					required : true,
					date: true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				refCode: "Patra Sankha is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				residentApplicant : "Applicant name is required",
				nijName : "Nij Name is required",
				municipalityName : "Municipality is required",
				wardNumber : "Ward Number must be number",
				sabikAddress : "Address is required",
				bigatDate : "Bigat Date is required",
				nagarikata : "Citizenship number is required",
				residentDistrict : "Resident District is required",
				issuedDateTwo : "Issued date is required",
				authorizedPerson : "Authorized person is required"

			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='residentApplicant']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>