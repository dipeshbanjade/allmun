<?php

namespace App\Http\Controllers\admin\ward;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\ward\WardStaffLeave;
use App\Http\Requests\admin\ward\wardStaffLeaveVal;
use App\model\mylogic;
use DB;
use Auth;
use Validator;

class wardStaffLeaveController extends Controller
{
       protected $viewPath         = 'admin.ward.wardStaffLeave';
       protected $wardLeaveTbl     = 'ward_staff_leaves';
       protected $leaveTbl         = 'leave_types';
       protected $prefix;
       protected $munId;

       public function __construct()
       {
           $this->middleware('auth');
           $this->prefix      = config('activityMessage.leavePrefix');
           $this->munId        = myLogic::getMunicipalityDetails()['id'];
       }

    public function index()
    {
        $page['page_title']       = 'Ward Staff Leave';
        $page['page_description'] = 'Ward Staff Leave Description';

        //TODO accordkign to ward user dispaly recored
        $wardStaffLeave = WardStaffLeave::with(['leaveTypes', 'users'])->where(['softDelete' => 0, 'users_id' => Auth::user()->staffs_id, 'wards_id' => Auth::user()->wards_id])->paginate(config('activityMessage.pagination'));

        return view($this->viewPath . '.index', compact(['page', 'wardStaffLeave']));
    }

    public function getAllStaffLeave(){
        $page['page_title']       = 'Ward Staff Leave';
        $page['page_description'] = 'Ward Staff Leave Description';
        
        $allLeave = myLogic::getWardStaffOnLeave();

        return view($this->viewPath . '.adminListLeave', compact(['page', 'allLeave']));

    }
    public function create()
    {
        $page['page_title']       = 'Wards : create';
        $page['page_description'] = 'Wards details';
        // $refCode  = mylogic::sysRefCode($tablename = $this->wardLeaveTbl, $prefix = $this->prefix);
       
        $leaveType = mylogic::getDrowDownData($tablename = $this->leaveTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd') {
             return view($this->viewPath . '.create', compact(['page', 'refCode', 'leaveType']));
        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }
    public function store(wardStaffLeaveVal $request)
    {
        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd') {
            DB::beginTransaction();
            $refCode = mylogic::sysRefCode($tablename = $this->wardLeaveTbl, $prefix = $this->prefix);
            try {
                $save = new WardStaffLeave;
                $save->refCode            = $refCode;
                $save->users_id           = Auth::user()->staffs_id;
                $save->municipilities_id  = $this->munId;
                $save->wards_id           = Auth::user()->wards_id;
                $save->leave_types_id     = $request->leave_types_id;
                $save->noOfDays           = $request->noOfDays;
                $save->shortNoteNep       = $request->shortNoteNep;
                $save->shortNoteEng       = $request->shortNoteEng;
                $save->startDate          = $request->startDate;
                $save->endDate             = $request->endDate;
                $save->status              = 0;
                $save->softDelete          = 0;

                $mySave = $save->save();

                $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createLeaveForm'), $tableName = $this->wardLeaveTbl, $tblId = $refCode);
                 if ($mySave && $log) {
                     DB::commit();
                    return redirect()->route('wardStaffLeave.index')->withMessage(config('activityMessage.saveMessage') .$refCode);
                }else{
                    return back()->withMessage(config('activityMessage.unSaveMessage'));
                }
            } catch (Exception $e) {
                DB::rollback();
                return back()->withMessage(config('activityMessage.dataNotInserted'));
            }
        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }

    public function edit($id)
    {
      $page['page_title']       = 'Leave form';
      $page['page_description'] = 'ward staff leave form details';

      $data = WardStaffLeave::findOrFail($id);
      $leaveType = mylogic::getDrowDownData($tablename = $this->leaveTbl, $orderColumn = 'nameEng', $orderby = 'ASC');



      return view($this->viewPath . '.edit', compact(['page', 'data', 'leaveType'])); 
    }

    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(), [
         'noOfDays' => 'required|numeric',
         'startDate' => 'required|date_format:"Y-m-d"',
         'endDate'   => 'required|date_format:"Y-m-d"',
         'leave_types_id' => 'required',
         'shortNoteEng'   => 'required|min:6|max:60000'
       ]);

      if ($validator->fails()) {
         return back()->withErrors($validator)->withInput();
      } 
      /*-----------------------------------*/
        DB::beginTransaction();
        try {
            $update = WardStaffLeave::findOrFail($id);
            $update->leave_types_id = $request->leave_types_id;
            $update->noOfDays       = $request->noOfDays;
            $update->shortNoteEng   = $request->shortNoteEng;
            $update->startDate      = $request->startDate;
            $update->endDate        = $request->endDate;

            $myupdate = $update->update();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm'), $tableName = $this->wardLeaveTbl, $tblId = $update->id);
             if ($myupdate && $log) {
                 DB::commit();
                return redirect()->route('wardStaffLeave.index')->withMessage(config('activityMessage.saveMessage') .$update->refCode);
            }else{
                return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        } catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
    }

    public function destroy($id)
    {
       if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd') {
           DB::beginTransaction();
           try {
               $del = WardStaffLeave::findOrFail($id);
               $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.deleteMessage'), $tableName = $this->wardLeaveTbl, $tblId = $del->refCode);
                if ($del && $log) {
                    $del->delete();
                    DB::commit();
                   return redirect()->route('wardStaffLeave.index')->withMessage(config('activityMessage.deleteMessage'));
               }else{
                   return back()->withMessage(config('activityMessage.unDeleteMessage'));
               }
           } catch (Exception $e) {
               DB::rollback();
               return back()->withMessage(config('activityMessage.dataNotInserted'));
           }

       }else{
           return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
       } 
    }

    public function changeStatus(Request $request){  // change status either present or absent
        $id        = $request->id;
        $status    = $request->status==1 ? 0 : 1;

        $change = WardStaffLeave::findOrFail($id);
        $change->status  = $status;
        $change->approvedBy = Auth::user()->staffs_id;;
        $update = $change->update();
        
        if ($update) {
            return MyLogic::successChangeStatus($code = 200);
        }else{
            return MyLogic::idNotFoundMessage($code = 301);
        }
    }
}
