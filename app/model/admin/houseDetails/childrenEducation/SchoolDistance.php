<?php

namespace App\model\admin\houseDetails\childrenEducation;

use Illuminate\Database\Eloquent\Model;

class SchoolDistance extends Model
{
    protected $table = 'school_distances';
}
