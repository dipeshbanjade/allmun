<?php

namespace App\model\admin\dartaChalani;

use Illuminate\Database\Eloquent\Model;
use App\model\admin\otherOffice\otherOffice;
use App\model\setting\Department;

class Chalani extends Model
{
    protected $table = 'chalanis';

    
    public function otherOffices(){
        return $this->belongsTo(otherOffice::class);
    }

    public function departments(){
    	return $this->belongsTo(Department::class);
    }
}
