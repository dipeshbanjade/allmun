<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class instituteRecommendationVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          //
            'refCode' => 'required|min:3|unique:institute_recommendations',
            'issuedDate' => 'required',
            'chalaniNumber' => 'required|unique:institute_recommendations',
            'operatorName' => 'required',
            'companyName' => 'required',
            'municipalityName' => 'required',
            'wardNumber' => 'required|numeric',
            'organizationAddress' => 'required',
            'organizationType' => 'required',
            'organizationWard' => 'required',
            'organizationName' => 'required',
            'instituteChalaniNumber' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
