<script type="text/javascript">
	$(function(){
	    $("form[name='frmDrinkingWater']").validate({ // Drinking water form validation
	    	rules:{
	    		sourceNam:{
	    			required: true
	    		},
	    		ownershipNam:{
	    			required:function(element){
	    				return $('[name=sourceNam]:checked').length>0;
	    			}
	    		},
	    		waterQty:{
	    			required:true
	    		}
	    	},
	    	messages:{
	    		sourceNam: "Water source is required",
	    		ownershipNam: " Ownership type is required",
	    		waterQty: "Quality of water is required"
	    	}

	    });
	    $("form[name='frmKitchenFrm']").validate({ // Kitchen from validation
	    	rules:{
	    		otherCookingFuelNam:{
	    			required:function(ele){
	    				return $('[name=cookingFuelNam]:checked').length<=0;
	    			}
	    		},
	    		otherOvenTypeNam:{
	    			required:function(ele){
	    				return $('[name=ovenTypeNam]:checked').length<=0;
	    			}
	    		}
	    	},
	    	messages:{
				otherCookingFuelNam:"The Cooking fule type is required",
				otherOvenTypeNam:"The oven type is required"
			}

	    });
	    $("form[name='frmLightFrm']").validate({ // check light from
	    	rules:{
	    		lightSrc:{
	    			required:true
	    		},
	    		whyNotReasonNam:{ // it is required only if other filed is selected in the light source
	    			required:function(ele){
	    				var lightSrc=$('input[name=lightSrc]');
	    				var i;
	    				for(i=0; i<lightSrc.length; i++){
	    					if(lightSrc[i].checked == true && lightSrc[i].value == 'other'){
	    						return true
	    					}
	    				}	    				
	    				
	    			}
	    		}
	    	},
	    	messages:{
	    		lightSrc:"The source of light is required",
	    		whyNotReasonNam:"The reason for not using electricity is required"
	    	}
	    });
	    $("form[name='frmToiletFrm']").validate({ // check toilet from
	    	rules:{
	    		toiletTypeNam:{
	    			required:true
	    		},
	    		haveShareToilet:{
	    			required:true
	    		}
	    	},
	    	messages:{
		    	toiletTypeNam:"Toilet type is required",
		    	haveShareToilet:"The sharing activity of toilet is required"
	    	}

	    });
	    $("form[name='frmWasteFrm']").validate({ // check waste from
	    	rules:{
	    		otherSolidWasteMntNam:{
	    			required:function(ele){
	    				return $('[name=solidWasteMntNam]:checked').length<=0;
	    			}
	    		},
	    		otherContWtrMntNam:{
	    			required:function(ele){
	    				return $('[name=contWtrMntNam]:checked').length<=0;
	    			}
	    		}
	    	},
	    	messages:{
	    		otherSolidWasteMntNam: "Solid waste disposal method is required",
	    		otherContWtrMntNam:"Waste water disposal method is required"
	    	}
	    });
	    $("form[name='frmRoadFrm']").validate({ // check waste from
	    	rules:{
	    		roadTypeNam:{
	    			required:true
	    		},
	    		roadWidth:{
	    			required:true
	    		}
	    	},
	    	messages:{
	    		roadTypeNam: "Type of road is required",
	    		roadWidth: "The width of road is required"
	    	}

	    });


	});	

	function requireFacility(select){
		var name= select.name;
		var index = name.substr(11,1);
		$("form[name='frmFacilitiesFrm']").validate();
			
		if(select.value==1){
			$('input[name^="deviceName['+index+'][number]"]').rules("add",{
				required:true,
				number:true
			});
		}else{
			$('input[name^="deviceName['+index+'][number]"]').rules("add",{
				required:false
			});
			$('input[name^="deviceName['+index+'][number]"]').val("");
		}
	}

</script>