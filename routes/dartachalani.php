<?php 
  /*-----Note this file is not included--------*/
	 Route::prefix('challani')->group(function(){
       /*challani*/
       Route::get('/challani',             'admin\dartachallani\dartachalaniController@challaniList')->name('challaniIndex');
       Route::get('/challani/create',             'admin\dartachallani\dartachalaniController@createChallani')->name('challaniCreate');
       Route::post('/challani/store',             'admin\dartachallani\dartachalaniController@storeChallani')->name('challaniStore');
       Route::get('/editChalani/{slug}', 'admin\dartachallani\dartachalaniController@editChalani')->name('editChalani');

       Route::get('/challaniDetails/{slug}',             'admin\dartachallani\dartachalaniController@chalaniDetails')->name('challaniShow');
       Route::post('/challani/{id}/update', 'admin\dartachallani\dartachalaniController@updateChallani')->name('updateChallani');
       Route::get('/chalaniDelete/{id}', 'admin\dartachallani\dartachalaniController@deleteChallani')->name('deleteChallani');
         /*----------------------darta-------------------------*/
  });

  Route::prefix('darta')->group(function(){
      Route::get('/darta',             'admin\dartachallani\dartachalaniController@dartaIndex')->name('dartaIndex');
      Route::post('/darta/store',             'admin\dartachallani\dartachalaniController@storeDarta')->name('dartaStore');
      Route::get('darta/{slug}',   'admin\dartachallani\dartachalaniController@editDarta')->name('dartaEdit');
      Route::post('/darta/{id}/update', 'admin\dartachallani\dartachalaniController@updateDarta')->name('updateDarta');
      Route::get('/dartaDelete/{id}', 'admin\dartachallani\dartachalaniController@deleteDarta')->name('deleteDarta');
     Route::get('/dartaDetails/{slug}',             'admin\dartachallani\dartachalaniController@dartaDetails')->name('dartaShow');

      Route::get('/getStaffByDept/{slug}', 'admin\dartachallani\dartachalaniController@getStaffByDepartmentWise')->name('getStaffByDeptId');
    });
?>