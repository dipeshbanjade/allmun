<script type="text/javascript">
	var addRowBtn = $('#addNewRowInTable');
	var addRowBtn2 = $('#addNewRowInTable2');

	var clickCount = 1;
	var clickCount2 = 1;
	

	addRowBtn.on('click', function(e){		
		e.preventDefault();
		clickCount++;

		var no = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="" value="'+clickCount+'">';

		var hakdarName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="hakdarName[]">';

		var nata = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="nataRelation[]">';

		var fathHubName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="fathHubName[]">';

		var citizenNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="citizenNo[]">';

		var houseNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="houseNo[]">';

		var kittaNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="kittaNo[]">';

		var batoName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="batoName[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertNewRow = "<tr class='tblRow'>"
		+"<td>"+no+"</td>"
		+"<td>"+hakdarName+"</td>"
		+"<td>"+nata+"</td>"
		+"<td>"+fathHubName+"</td>"
		+"<td>"+citizenNo+"</td>"
		+"<td>"+houseNo+"</td>"
		+"<td>"+kittaNo+"</td>"
		+"<td>"+batoName+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#tableBody1').append(insertNewRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});
	});


	addRowBtn2.on('click', function(e){
		e.preventDefault();
		clickCount2++;

		var no = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="" value="'+clickCount2+'">';

		var wardNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="wardNo[]">';

		var seatNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="seatNo[]">';

		var kitta = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="kitta[]">';

		var area = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="area[]">';

		var houseNo2 = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="houseNo2[]">';

		var kittaNo2 = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="kittaNo2[]">';

		var streetType = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="streetType[]">';

		var kaifiyat = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="kaifiyat[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertNewRow = "<tr class='tblRow'>"
		+"<td>"+no+"</td>"
		+"<td>"+wardNo+"</td>"
		+"<td>"+seatNo+"</td>"
		+"<td>"+kitta+"</td>"
		+"<td>"+area+"</td>"
		+"<td>"+houseNo2+"</td>"
		+"<td>"+kittaNo2+"</td>"
		+"<td>"+streetType+"</td>"
		+"<td>"+kaifiyat+"</td>"		
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#tableBody2').append(insertNewRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});
	});

	$(function(){
		/*add buttoin*/
		
		$("form[name='formPropertyNameTransfer']").validate({
			rules:{

				issuedDate : {
					required: true,
					date: true
				},
				chalaniNumber : {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				accountHolderName : {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				accountHolderAddress : {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				ownerName : {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				nata : {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				natedarName : {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				date : {
					required: true,
					date: true
				},
				applicant : {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				authorizedPerson : {
					required: true,
					minlength: 3,
					maxlength: 30
				}				
			},
			messages: {
				issuedDate : 'Issued Date is required ',
				chalaniNumber : 'Chalani Number is required ',
				accountHolderName : 'Account Holder Name is required ',
				accountHolderAddress : 'Account Holder Address is required ',
				ownerName : 'Owner Name is required ',
				nata : 'Nata is required ',
				natedarName : 'Natedar name is required ',
				date : 'Date is required ',
				applicant : 'Applicant is required '
			}
		});
	});
	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='ownerName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>
