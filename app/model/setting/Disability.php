<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class Disability extends Model
{
    protected $table = 'disabilities';
    protected $fillable = ['refCode', 'nameNep','nameEng', 'status', 'softDelete'];
}
