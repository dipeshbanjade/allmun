@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="container">
    @if($wardDetails)
    <div class="wardInfo p-4 bg-white">
        <div class="row" style="border-bottom: 3px solid #7B30FF">
            <div class="col-sm-12">
                <p align="right">
                  <a class="btn btn-success">
                  <i class='fa fa-eye'></i><span>
                  @lang('commonField.employer.attendance')</span></a>
                 </p>   
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-12">
                <address style="color: #000;">
                    <h4>@lang('commonField.address_information.wardNo') 
                    <strong>{{ $lang =='np' ? $wardDetails->nameNep : $wardDetails->nameEng  }}</strong>.</h4> 
                    <address>
                        <label>
                           <i class="fa fa-phone"></i>
                        <span>{{ $wardDetails->landLineNumber }}</span>                               
                        </label>
                        <label>
                            <i class="fa fa-location"></i>
                            <span>
                              {{ $lang == 'np' ? $wardDetails->addrNep :  $wardDetails->addrEng }}
                            </span>
                        </label>
                    </address>
                    <h4>
                    <i class="fa fa-user"></i>
                    <span>
                       @lang('commonField.extra.wardHead') <br>
                        &nbsp;&nbsp;&nbsp;{{ $wardDetails->muncipilityHead }}
                    </span>
                </address>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12">
                <!-- logo here -->
            </div>
        </div>
        <div class="row p-4">
            <table class="table table-striped" style="color: black;">
                <tbody>
                    <tr>
                        <th><i class="fa fa-user"></i>
                        <span>
                            @lang('commonField.extra.staff')
                        </span></th>
                        <td>{{ $wardStaffNo ? $wardStaffNo : 0 }}</td>
                    </tr>
                    <tr>
                        <th><i class="fa fa-user"></i>
                        <span>
                           @lang('commonField.extra.citizen')    
                        </span>
                        </th>
                        <td>{{ $totalCitizen ? $totalCitizen : 0 }}</td>
                    </tr>

                    <tr>
                        <th><i class="fa fa-home"></i>
                          <span>
                              @lang('commonField.extra.houseNum')
                          </span>
                        </th>                 
                        <td>{{ $totalHouse ? $totalHouse : 0 }}</td>
                    </tr>
                </tbody>
            </table>
        </div>                                                                                            
    </div>
    @endif      
</div>
@endsection