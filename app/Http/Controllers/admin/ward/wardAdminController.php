<?php

namespace App\Http\Controllers\admin\ward;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\ward\WardStaff;
use App\model\myLogic;
use App\Http\Requests\admin\ward\wardStaffValidation;
use Validator;
use Auth;
use App\model\admin\acl\Role;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;


class wardAdminController extends Controller
{
      protected $viewPath      = 'admin.ward.wardAdmin';
      protected $wardStaffTbl  = 'ward_staffs';

      protected $staffTypeTbl = 'staff_types';
      protected $provincesTbl = 'provinces';
      protected $deginationTbl = 'deginations';
      protected $districtTbl = 'districts';
      protected $deptTbl     = 'departments';
      
       public function __construct()
       {
          $this->middleware('auth');
          $this->prefix      = config('activityMessage.wardPrefix');
          
          $this->imgProfile   = config('activityMessage.wardStaffImg'); 
          $this->imgCitizen   = config('activityMessage.wardStaffCitizenImg');
          $this->munId        = myLogic::getMunicipalityDetails()['id'];
       }

    public function index()
    {
        $page['page_title']       = 'List of ward admin';   //TODO change lana
        $page['page_description'] = 'all ward admin';

        $checkCount = myLogic::getMunicipalityDetails();
        if (!$checkCount > 0) {
            return back()->withMessage(config('activityMessage.createMunicipilityFirst'));
        }

        $wardAdmin = User::with('roles')->orderBy('created_at', 'DESC')->whereNotNull('wards_id')->where(['status' => 0, 'softDelete' => 0, 'userLevel' => 'wrd'])->paginate(30);

        return view($this->viewPath . '.index', compact(['page', 'wardAdmin']));
    }

    public function create()
    {
       $page['page_title']       = 'Create Ward Admin';   //TODO change lana
       $page['page_description'] = 'create all ward admin';

       $staffTypeId = mylogic::getDrowDownData($tablename = $this->staffTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
       $deginationId = mylogic::getDrowDownData($tablename = $this->deginationTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
       $provincesId = mylogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
       $districtId = mylogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');
       
       // $refCode  = mylogic::sysRefCode($tablename = $this->wardStaffTbl, $prefix = $this->prefix);
       $getWard = mylogic::getAllWardDetails();

       $deptId = mylogic::getDrowDownData($tablename = $this->deptTbl, $orderColumn = 'nameEng', $orderby = 'ASC');

       $roles = myLogic::getSysRole($roleFor = 'ward');
       if (Auth::user()->userLevel =='mun' || Auth::user()->userLevel =='dev') 
       {
          return view($this->viewPath . '.create', compact(['page', 'staffTypeId', 'deginationId', 'provincesId', 'districtId', 'getWard', 'roles', 'deptId']));
         
       }else{
         return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
       }
    }

    public function store(wardStaffValidation $request)
    {

       if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
         DB::beginTransaction();
         try {            
                $refCode = mylogic::sysRefCode($tablename = $this->wardStaffTbl, $prefix = $this->prefix);
                 $save = new WardStaff;
                 $save->municipilities_id  = myLogic::getMunicipalityDetails()['id'];
                 
                 // $save->wards_id       = Auth::user()->userLevel === 'dev' ? $request->wards_id : Auth::user()->wards_id;  
                 $save->wards_id       = $request->wards_id; 
                 $save->refCode        = $refCode;

                 if ($request->hasFile('profilePic')) {
                     $imgFile = $request->file('profilePic');
                     $filename = str_replace(' ', '', $refCode). '.' . $imgFile->getClientOriginalExtension();

                     $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgProfile, $filename);
                     $save->profilePic =  $this->imgProfile . $filename;
                 }
                 $save->firstNameNep     = $request->firstNameNep ?? '';
                 $save->firstNameEng     = $request->firstNameEng;

                 $save->middleNameNep    = $request->middleNameNep ?? ''; 
                 $save->middleNameEng    = $request->middleNameEng ?? ''; 
                 $save->lastNameNep      = $request->lastNameNep ?? ''; 
                 $save->lastNameEng      = $request->lastNameEng;
                 $save->email            = $request->email ?? 'N/A';
                 $save->citizenNo        = $request->citizenNo;
                 $save->staff_types_id   = $request->staff_types_id;
                 $save->deginations_id   = $request->deginations_id;
                 $save->provinces_id    = $request->provinces_id; 
                 $save->districts_id     = $request->districts_id;

                 $save->departments_id     = $request->departments_id; 

                 $save->joingDateNep      = $request->joingDateNep ?? 'N/A';
                 $save->joingDateEng      = $request->joingDateEng ?? 'N/A';
                 $save->wardNo           = $request->wardNo ?? 'N/A';
                 $save->villageNep       = $request->villageNep ?? 'N/A'; 
                 $save->villageEng       = $request->villageEng; 
                 $save->dob              = $request->dob ?? 'N/A'; 
                 $save->dobEng              = $request->dobEng ?? 'N/A'; 
                 $save->phoneNumber      = $request->phoneNumber;

                 if ($request->hasFile('citizenImagePath')) {
                  $imgFile = $request->file('citizenImagePath');
                  $filename = str_replace(' ', '', $refCode). '.' . $imgFile->getClientOriginalExtension();

                  $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgCitizen, $filename);

                  $save->citizenImagePath =  $this->imgCitizen . $filename;
              }
               $mySave = $save->save();
               
               // create login 

               $validator = Validator::make($request->all(), [
                          'email' => 'required|email|unique:users',
                      ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                }
               
               $encriptId = myLogic::idEncription($save->id);

               $login = new User;
               $login->name               = $request->firstNameEng .'&nbsp;'. $request->middleNameEng .'&nbsp;'. $request->lastName;
               $login->email              = $request->email;
               $login->password           = Hash::make($request->password);
               $login->userLevel          = 'wrd';
               $login->municipilities_id  = $this->munId;
               $login->wards_id           = $request->wards_id;
               $login->roles_id           = $request->roles_id;
               $login->staffs_id           = $save->id;
               $login->idEncrip           = $encriptId;
               $login->isAdmin            = 1;
               $login->isExists           = 1;
               $roleLogin =  $login->save();

               $save->idEncrip  = $encriptId;  // making Id encription
               $save->save();
               // login

               $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createAction'), $tableName = $this->staffTypeTbl, $tblId = $login->id);

              if ($mySave && $roleLogin && $log) {
                  DB::commit();
                  return redirect()->route('wardAdmin')->withMessage(config('activityMessage.saveMessage') .$refCode);
                 // return back()->withMessage(config('activityMessage.saveMessage') .$request->refCode);
             }else{
                 return back()->withMessage(config('activityMessage.unSaveMessage'));
             }
         } catch (Exception $e) {
              DB::rollback();
              return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
               /*----------------------------------------------------------------*/
       }else{
           return back()->withMessage(config('activityMessage.accessDenied'));
       }
    }

    public function edit($slug)
    {
      $page['page_title']         = 'Municipality : Ward Admin edit';
      $page['page_description']   = 'Edit wards admin details';
      if ($slug) {
        $staffTypeId = mylogic::getDrowDownData($tablename = $this->staffTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        $deginationId = mylogic::getDrowDownData($tablename = $this->deginationTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        $provincesId = mylogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
        $districtId = mylogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');
        
        // $refCode  = mylogic::sysRefCode($tablename = $this->wardStaffTbl, $prefix = $this->prefix);
        $getWard = mylogic::getAllWardDetails();

        $deptId = mylogic::getDrowDownData($tablename = $this->deptTbl, $orderColumn = 'nameEng', $orderby = 'ASC');

        $roles = myLogic::getSysRole($roleFor = 'ward');
        $data = WardStaff::where(['status'=> 0, 'softDelete'=>0, 'idEncrip'=>$slug])->first();
        $usrRoleId = User::select('roles_id')->where(['staffs_id'=>$data->id])->first();

        $data->roles_id = $usrRoleId['roles_id'];

        return view($this->viewPath. '.edit', compact(['page', 'data', 'staffTypeId', 'deginationId', 'provincesId', 'districtId', 'getWard', 'deptId', 'roles']));
      }else{
        return back()->withMessage(config('activityMessage.idNotFound'));
      }
    }

    public function update(Request $request, $id)
    {
          $validator = Validator::make($request->all(), [
            'firstNameNep'   => 'required|min:3|max:60',
            'firstNameEng'   => 'required|min:3|max:60',
            'lastNameNep'   => 'required|min:3|max:60',
            'lastNameEng'   => 'required|min:3|max:60',
            'citizenNo'   => 'required|min:4|max:60|unique:ward_staffs,citizenNo,'.$request->id,
            'joingDateNep'   => 'required',
            'phoneNumber'   => 'required|min:10|max:15|unique:ward_staffs,phoneNumber,'.$request->id,
            'roles_id'  => 'required',
            'email'     =>'required|email|unique:ward_staffs,email,'.$request->id,
            ]);

          if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
          }
          DB::beginTransaction();
          try {
             $update = WardStaff::findOrFail($id);
             $update->firstNameNep  = $request->firstNameNep;
             $update->firstNameEng  = $request->firstNameEng;
             $update->middleNameNep  = $request->middleNameNep ?? 'N/A'; 
             $update->middleNameEng  = $request->middleNameEng ?? 'N/A'; 
             $update->lastNameNep  = $request->lastNameNep; 
             $update->lastNameEng  = $request->lastNameEng;
             $update->email  = $request->email;
             $update->citizenNo  = $request->citizenNo;
             $update->joingDateNep  = $request->joingDateNep;
             $update->joingDateEng  = $request->joingDateEng;
             $update->phoneNumber  = $request->phoneNumber;
             $update->wards_id     = $request->wards_id;
             $update->deginations_id     = $request->deginations_id;
             /*--------------------------------------------*/
             if ($request->hasFile('profilePic')) {
                 $imgFile = $request->file('profilePic');
                 $filename = str_replace(' ', '', mylogic::getRandNumber()). '.' . $imgFile->getClientOriginalExtension();
                 $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgProfile, $filename);
                 $update->profilePic =  $this->imgProfile . $filename;
             }
             /*----------------------------------------------*/
             $myUpdate = $update->update();

             $updUser = User::where(['staffs_id' => $update->id])->first();
             $updUser->roles_id  = $request->roles_id;
             $updUser->email     = $request->email;
             $updUser->name      = $request->firstNameEng .'&nbsp;'. $request->middleNameEng .'&nbsp;'. $request->lastName;
             $usrUpdate = $updUser->update();
             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateWardStaff'), $tableName = $this->wardStaffTbl, $tblId = $update->id);

             if ($myUpdate && $usrUpdate && $log) {
                 DB::commit();
                 return redirect()->route('wardAdmin')->withMessage(config('activityMessage.updateMessage') .$request->citizenNo);
            }else{
                return back()->withMessage(config('activityMessage.notUpdated'));
            }

          }catch (Exception $e) {
              DB::rollback();
              return back()->withMessage(config('activityMessage.dataNotInserted'));
          }
    }

    public function destroy($id)
    {
        $del = WardStaff::findOrFail($id);
        
        $del->softDelete = 1;
        $myDel = $del->update();

        $delUsr = User::where('staffs_id', $id)->first();
        $delUsr->isExists = 1;
        $delUsr->status   = 1;
        $userDelete = $delUsr->update();
        /*------------------------------*/
        if ($myDel && $userDelete) {
          return MyLogic::successChangeStatus($code = 200);
        }else{
           return MyLogic::successChangeStatus($code = 301);
        }
    }
}
