<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenRelationsTable extends Migration
{
    public function up()
    {
        Schema::create('citizen_relations', function (Blueprint $table) {
            $table->integer('citizens_id')->nullable()->unsigned();
            $table->foreign('citizens_id')->references('id')->on('citizen_infos')->onDelete('cascade');

            $table->string('relationName');

            $table->integer('relation_id')->nullable()->unsigned();
            $table->foreign('relation_id')->references('id')->on('citizen_infos')->onDelete('cascade');
            $table->string('orderColumn')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('citizen_relations');
    }
}
