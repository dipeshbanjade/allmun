<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class batoKayamVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:bato_kayams',
            'wardNumber' => 'required',
            'municipalityName' => 'required',
            'issuedDate' => 'required',
            'chalaniNumber' => 'required|unique:bato_kayams',
            'accountHolderName' => 'required',
            'accountHolderAddress' => 'required',
            'municipalityName' => 'required',
            'wardNumber' => 'required',
            'address' => 'required',
            'plotNumber' => 'required',
            'area' => 'required',
            'eastWidth' => 'required',
            'length' => 'required',
            'landOwnerName' => 'required',
            'deginations_id' => 'required',
            'authorizedPerson' => 'required',
            // 'applicant_users_id' => 'required'
        ];
    }
}
