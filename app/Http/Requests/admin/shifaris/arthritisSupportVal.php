<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class ArthritisSupportVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:arthritis_supports',
            'chalaniNumber' => 'required|unique:arthritis_supports',
            'issuedDate' => 'required',
            'officeName' => 'required',
            'officeAddress' => 'required',
            'wardNumber' => 'required|numeric',
            'organizationType' => 'required',
            'organizationAddress' => 'required',
            'organizationWard' => 'required|numeric',
            'patientPrefix' => 'required',
            'patientName' => 'required',
            'disease' => 'required',
            'hospital' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
