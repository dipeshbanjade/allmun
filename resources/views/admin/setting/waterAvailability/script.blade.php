<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditwaterAvailability']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 200
                }
            },
            messages: {
                nameEng : "The name of water availabililty is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmwaterAvailability']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 200
                }
            },
            messages: {
          
                nameEng : "The name of water availabililty is required"
            }
        });
	var btnAddwaterAvailability = $('.btnAddwaterAvailability');
	btnAddwaterAvailability.on('click', function(e){
		$('#waterAvailabilityModel').modal('show'); 
	});

	var btnEditwaterAvailability = $('.btnEditwaterAvailability');
	btnEditwaterAvailability.on('click', function(e) {
		e.preventDefault();
        var url = $(this).data('url');
        $.ajax({
            'type': 'GET',
            'url': url,
            success: function (response) {
				$('.waterAvailabilityId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editwaterAvailabilityModel").modal('show');
			}
		})
	});

	var frmUpdatewaterAvailability = $('#frmUpdatewaterAvailability');
  frmUpdatewaterAvailability.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.waterAvailabilityId').val();
    var url  = "{{URL::to('/')}}" + "/setting/waterAvailability/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editwaterAvailabilityModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editwaterAvailabilityModel').modal('hide');
});
});
</script>
