<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardvarasisAuthiorities']").validate({
			rules:{
				successorAge : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				successorName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				successorMunicipality : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				successorWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				caseName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				casePerson : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				caseMunicipality : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				casereason : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				caseRunMunicipality : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				agreedOn : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				manisMunicipality : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				manisWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				manisAge : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				manisName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				caseOnMunicipality : {
					required : true,
					minlength : 1,
					maxlength : 255
				},
				rohabar : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				transfer : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				year : {
					required : true,
					minlength : 2,
					maxlength : 2,
					number: true
				},
				month : {
					required : true,
					minlength : 1,
					maxlength : 2,
					number: true
				},
				day : {
					required : true,
					minlength : 1,
					maxlength : 2,
					number: true
				},
				eachday : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number: true
				}
			},
			messages: {
				successorAge : "Successor Age should be numberic",
				successorName : "Successor Name is required",
				successorMunicipality : "Successor Municipality is required",
				successorWard : "Successor Ward should be numberic",
				casePerson : "Cased Person is required",
				caseName : "Case Name is required",
				caseMunicipality : "Case Municipality is required",
				casereason : "Case Reason is required",
				caseRunMunicipality : "Case-run Municipality is required",
				agreedOn : "Agreed On is required",
				manisMunicipality : "Person Municipality is required",
				manisWard : "Person Ward should be numberic",
				manisAge : "Person Age should be numberic",
				manisName : "Person Name is required",
				caseOnMunicipality : "Case of which Municipality is required",
				rohabar : "Rohabar is required",
				transfer : "Trasfer is required",
				year : "Year should be numberic",
				month : "Month should be numberic",
				day : "Day should be numberic",
				eachday : "Per Day should be numberic"
			}
		});
	});
</script>