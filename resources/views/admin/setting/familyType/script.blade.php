<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmeditfamilyType']").validate({
			rules:{
				nameNep : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				nameEng : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				nameNep : "The name of Family Type in Nepali is required",
				nameEng : "The name of Family Type is required"
			}
		});
	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmfamilyType']").validate({
			rules:{
				nameNep : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				nameEng : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				nameNep : "The name of Family Type in Nepali is required",
				nameEng : "The name of Family Type is required"
			}
		});

		var btnAddfamilyType = $('.btnAddfamilyType');
		btnAddfamilyType.on('click', function(){
			
			$('#familyTypeModel').modal('show');
		});

		
		var btnEditFamilyType = $('.btnEditFamilyType');
		btnEditFamilyType.on('click', function(e){
			e.preventDefault();
			var url = $(this).data('url');
			$.ajax({
				'type': 'GET',
				'url': url,
				success: function (response) {
					console.log(response);
					$('.familyTypeId').val(response.id),
					$('.txtnameNep').val(response.nameNep);
					$('.txtnameEng').val(response.nameEng);
					$("#editFamilyTypeModel").modal('show');
				}
			})
		});
		var frmUpdateFamilyType = $('#frmUpdateFamilyType');
		frmUpdateFamilyType.on('submit', function(e){
			e.preventDefault();
			var data = $(this).serialize();
			var id   = $('.familyTypeId').val();
			var url  = "{{URL::to('/')}}" + "/setting/familyType/"+id+"/update";
			$.ajax({
				'type' : 'POST',
				'url'  : url,
				'data'    : data,
				success : function(response){
					console.log(response);
					if (response.success==true) {
						$('.infoDiv').append(response.message).addClass('alert alert-success');
					}
					if (response.success==false) {
						$.each(response.message, function(key, value){
							$('.infoDiv').append(value).addClass('alert alert-danger');
						})
					}
				},complete:function(){
					$("#editFamilyTypeModel").modal('hide');
					setTimeout(location.reload.bind(location), 1500);
				}
			})
			.fail(function (response) {
				alert('data already exists');
			});
			$('#editFamilyTypeModel').modal('hide');
		});
	});
</script>