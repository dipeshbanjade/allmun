<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitTypesTable extends Migration
{
    public function up()
    {
        Schema::create('visit_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode', 100)->unique();
            $table->string('nameNep');
            $table->string('nameEng');
            $table->string('desc');
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('visit_types');
    }
}
