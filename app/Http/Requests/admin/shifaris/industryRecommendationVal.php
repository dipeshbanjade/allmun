<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class industryRecommendationVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:industry_recommendations',
            'issuedDate' => 'required',
            'officeAddress' => 'required', 
            'receivedDate' => 'required',
            'receivedDate' => 'required',
            'estPerson' => 'required',
            'organizationAddress' => 'required',
            'address' => 'required',
            'areaDetails' => 'required',
            'objective' => 'required',
            'occupationName' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'

        ];
    }
}
