@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
    <div class="card-body">
    </div>
    <div class="card-head">
        <header>
            <i class="fa fa-edit">
                @lang('commonField.dartaChalani.editDarta')
            </i>
        </header>
    </div>
    {!! Form::model($data, ['method' => 'POST','route' => ['updateDarta', $data->id], 'files'=>true, 'name' => 'updateChalani']) !!}
    @include('admin.dartaChalani.darta._form')
    <!--  -->
    <p class="p-4">
        <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> 
            <span>@lang('commonField.button.back')</span> 
        </button>
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success pull-right']) }}
        {{ Form::close() }}
    </p>
</div>
@endsection
@section('custom_script')
    @if(isset($data))
        <script type="text/javascript">
            var deptId    = <?php  echo $data->departments_id;     ?>;
            var dartaFor  = <?php  echo $data->dartaIsFor; ?>; 
            $('.cboDeptId').val(deptId).trigger('change');
        </script>       
    @endif
<script type="text/javascript">
      $("form[name='frmdarta']").validate({
       rules:{
        dartaDate:{
          required:true,
          date:true
        },
        dartaSubject:{
          required:true,
          minlength:5,
          maxlength:200,
          alphanumeric:true
        },
        dartaNo:{
          required:true,
          number:true
        },
        dartaPage:{
          required:true
        },
        dartaPageDate:{
          required:true
        },
        reciveDate:{
          required:true
        },
        department_id:{
          required:true
        },
        other_office_id:{
          required:true
        },
        leterFor:{
          required:true
        }
       },
       messages: {
        dartaDate :" Darta date is required ",
        dartaSubject :" Darta subject date is required ",
        dartaNo :" Darta number is required",
        dartaPage :" Darta page is required ",
        dartaPageDate :" Darta page date is required ",
        reciveDate: " Recived date is required",
        department_id :" Department is required",
        other_office_id :" Office is required",
        leterFor :" The letter for is required ",
       }
      });
        
    $('.cboDeptId').on('change', function(){   // extracting staff according to department wise
         $('#dartaIsFor').empty();
         var deptId = $(this).val();
         if (deptId) {
            var url  = "{{URL::to('/')}}" + "/dashboard/getStaffByDept/"+deptId;
            $('#dartaIsFor').val('');
            $.ajax({
              'type' : 'GET',
              'url'  : url,
              success:function(response){
                if (response.success == true) {
                    $.each(response.data, function(index, element) {
                        $('#dartaIsFor').append($('<option>', {
                            value: element.id,
                            text: element.firstNameNep + ' '+ element.middleNameNep +' '+ element.lastNameNep  
                        }));
                    });
                }
                
              }
            }).fail(function (response){
              infoDiv.addClass('alert alert-danger').append(response.message);
            });
         }
    });
            $('.cboDeptId').trigger('change');
            $('#dartaIsFor').val(dartaFor).trigger('change');
</script>
@endsection