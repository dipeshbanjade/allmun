<?php

namespace App\Http\Requests\admin\setting\QualityWater;

use Illuminate\Foundation\Http\FormRequest;

class QualityWaterValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
              'nameEng' => 'required|min:4|max:50'
            
        ];
    }
}
