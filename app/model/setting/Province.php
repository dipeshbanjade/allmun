<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    protected $fillable = ['name', 'capital', 'governor', 'chiefMinister', 'districtNo',  'status', 'softDelete'];
}
