<script type="text/javascript">
		$(function(){
		     $("form[name='frmDailyReport']").validate({
		      rules:{
		       sub : {
		         required : true,
		         minlength : 3,
		         maxlength : 100
		       },
		       date : {
		       	required : true,
		       	date     : true
		       },
		       startTime : {
		         required : true,
		         time      : true
		       },
		       endTime : {
		         required : true
		       },
		       desc : {
		         required :true,
		         minlength : 3,
		         maxlength : 60000
		       }
		     },
		     messages: {
		       sub : "subject field is needed minimun 3 maximum 100",
		       date : "Please select current date",
		       startTime : "Start time is needed",
		       endTime : "End time is needed",
		       desc : "short description is needed"
		     }
		   });
	       /*----------------------*/
		   });
</script>