<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
        $myLan = Config::get('app.locale');
        View::share('lang', $myLan);
    }
    public function register()
    {
        //
    }
}
