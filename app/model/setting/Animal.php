<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    protected $table = 'animals';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
