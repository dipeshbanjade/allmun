<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class phone extends Model
{
    protected $table = 'phones';
    protected $fillable = ['refCode','name','mobileTag','status','softDelete'];
}
