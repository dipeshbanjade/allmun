<?php
Route::prefix('shifaris')->group(function(){
	Route::get('/allShifaris', 'HomeController@allShifaris')->name('allShifaris');

	Route::get('/searchShifarisIndex', 'HomeController@searchShifarisIndex')->name('searchShifarisIndex');
	Route::post('/searchShifaris', 'HomeController@shifarisSearch')->name('shifarisSearch');
	Route::get('/shifarisEdit/{slug}/{tableName}/{viewPath}', 'HomeController@shifarisEdit')->name('shifarisEdit');
	Route::get('/getScanShifarisData/{id}', 'HomeController@getShifarisData')->name('shifarisData');

	Route::get('/nonProfitOrgRegistration',             'admin\shifaris\unionOrganization\nonProfitOrgRegistrationController@index')->name('admin.nonProfitOrgRegistration.index');
	
	Route::get('/nonProfitOrgRegistration/create', 'admin\shifaris\unionOrganization\nonProfitOrgRegistrationController@create')->name('admin.nonProfitOrgRegistration.create');
	Route::post('/nonProfitOrgRegistration/store',      'admin\shifaris\unionOrganization\nonProfitOrgRegistrationController@store')->name('admin.nonProfitOrgRegistration.store');
	Route::get('/nonProfitOrgRegistration/{id}/edit',   'admin\shifaris\unionOrganization\nonProfitOrgRegistrationController@edit')->name('admin.nonProfitOrgRegistration.edit');
	Route::post('/nonProfitOrgRegistration/{id}/update', 'admin\shifaris\unionOrganization\nonProfitOrgRegistrationController@update')->name('admin.nonProfitOrgRegistration.update');
	Route::get('/nonProfitOrgRegistration/{id}',        'admin\shifaris\unionOrganization\nonProfitOrgRegistrationController@destroy')->name('admin.nonProfitOrgRegistration.delete');
	Route::get('/nonProfitOrgRegistrationChangeStatus', 'admin\shifaris\unionOrganization\nonProfitOrgRegistrationController@changeStatus')->name('admin.nonProfitOrgRegistration.changeStatus');

	/*--------------------------------------------- Institution Registration Recommendation ---------------------------------------------*/
	Route::get('/instituteRegistration',             'admin\shifaris\unionOrganization\instituteRegistrationController@index')->name('admin.instituteRegistration.index');
	Route::get('/instituteRegistration/create', 'admin\shifaris\unionOrganization\instituteRegistrationController@create')->name('admin.instituteRegistration.create');
	Route::post('/instituteRegistration/store',      'admin\shifaris\unionOrganization\instituteRegistrationController@store')->name('admin.instituteRegistration.store');
	Route::get('/instituteRegistration/{id}/edit',   'admin\shifaris\unionOrganization\instituteRegistrationController@edit')->name('admin.instituteRegistration.edit');
	Route::post('/instituteRegistration/{id}/update', 'admin\shifaris\unionOrganization\instituteRegistrationController@update')->name('admin.instituteRegistration.update');

	/*--------------------------------------------- Institutionalization Recommendation ---------------------------------------------*/
	Route::get('/instituteRecommendation',             'admin\shifaris\unionOrganization\instituteRecommendationController@index')->name('admin.instituteRecommendation.index');
	Route::get('/instituteRecommendation/create', 'admin\shifaris\unionOrganization\instituteRecommendationController@create')->name('admin.instituteRecommendation.create');
	Route::post('/instituteRecommendation/store',      'admin\shifaris\unionOrganization\instituteRecommendationController@store')->name('admin.instituteRecommendation.store');
	Route::get('/instituteRecommendation/{id}/edit',   'admin\shifaris\unionOrganization\instituteRecommendationController@edit')->name('admin.instituteRecommendation.edit');
	Route::post('/instituteRecommendation/{id}/update', 'admin\shifaris\unionOrganization\instituteRecommendationController@update')->name('admin.instituteRecommendation.update');

	/*--------------------------------------------- Business Building License ---------------------------------------------*/
	Route::get('/businessLicense',             'admin\shifaris\business\businessLicenseController@index')->name('admin.businessLicense.index');
	Route::get('/businessLicense/create', 'admin\shifaris\business\businessLicenseController@create')->name('admin.businessLicense.create');
	Route::post('/businessLicense/store',      'admin\shifaris\business\businessLicenseController@store')->name('admin.businessLicense.store');
	Route::get('/businessLicense/{id}/edit',   'admin\shifaris\business\businessLicenseController@edit')->name('admin.businessLicense.edit');
	Route::post('/businessLicense/{id}/update', 'admin\shifaris\business\businessLicenseController@update')->name('admin.businessLicense.update');

	/*--------------------------------------------- Business Registration Rate Form ---------------------------------------------*/
	Route::get('/businessRegistration',             'admin\shifaris\business\businessRegistrationController@index')->name('admin.businessRegistration.index');
	Route::get('/businessRegistration/create', 'admin\shifaris\business\businessRegistrationController@create')->name('admin.businessRegistration.create');
	Route::post('/businessRegistration/store',      'admin\shifaris\business\businessRegistrationController@store')->name('admin.businessRegistration.store');
	Route::get('/businessRegistration/{id}/edit',   'admin\shifaris\business\businessRegistrationController@edit')->name('admin.businessRegistration.edit');
	Route::post('/businessRegistration/{id}/update', 'admin\shifaris\business\businessRegistrationController@update')->name('admin.businessRegistration.update');

	/*--------------------------------------------- Turn Off Business ---------------------------------------------*/
	Route::get('/closeBusiness',             'admin\shifaris\business\closeBusinessController@index')->name('admin.closeBusiness.index');
	Route::get('/closeBusiness/create', 'admin\shifaris\business\closeBusinessController@create')->name('admin.closeBusiness.create');
	Route::post('/closeBusiness/store',      'admin\shifaris\business\closeBusinessController@store')->name('admin.closeBusiness.store');
	Route::get('/closeBusiness/{id}/edit',   'admin\shifaris\business\closeBusinessController@edit')->name('admin.closeBusiness.edit');
	Route::post('/closeBusiness/{id}/update', 'admin\shifaris\business\closeBusinessController@update')->name('admin.closeBusiness.update');

	/*--------------------------------------------- New Permanent Accounting No. ---------------------------------------------*/
	Route::get('/permAccounting',             'admin\shifaris\business\permAccountingController@index')->name('admin.permAccounting.index');
	Route::get('/permAccounting/create', 'admin\shifaris\business\permAccountingController@create')->name('admin.permAccounting.create');
	Route::post('/permAccounting/store',      'admin\shifaris\business\permAccountingController@store')->name('admin.permAccounting.store');
	Route::get('/permAccounting/{id}/edit',   'admin\shifaris\business\permAccountingController@edit')->name('admin.permAccounting.edit');
	Route::post('/permAccounting/{id}/update', 'admin\shifaris\business\permAccountingController@update')->name('admin.permAccounting.update');

	/*--------------------------------------------- Business Accounting more permanent no. ---------------------------------------------*/
	Route::get('/businessAccounting',             'admin\shifaris\business\businessAccountingController@index')->name('admin.businessAccounting.index');
	Route::get('/businessAccounting/create', 'admin\shifaris\business\businessAccountingController@create')->name('admin.businessAccounting.create');
	Route::post('/businessAccounting/store',      'admin\shifaris\business\businessAccountingController@store')->name('admin.businessAccounting.store');
	Route::get('/businessAccounting/{id}/edit',   'admin\shifaris\business\businessAccountingController@edit')->name('admin.businessAccounting.edit');
	Route::post('/businessAccounting/{id}/update', 'admin\shifaris\business\businessAccountingController@update')->name('admin.businessAccounting.update');

	/*--------------------------------------------- Industry Registration Recommendation ---------------------------------------------*/
	Route::get('/industryRegistrationRecomm',             'admin\shifaris\business\industryRegistrationRecommController@index')->name('admin.industryRegistrationRecomm.index');
	Route::get('/industryRegistrationRecomm/create', 'admin\shifaris\business\industryRegistrationRecommController@create')->name('admin.industryRegistrationRecomm.create');
	Route::post('/industryRegistrationRecomm/store',      'admin\shifaris\business\industryRegistrationRecommController@store')->name('admin.industryRegistrationRecomm.store');
	Route::get('/industryRegistrationRecomm/{id}/edit',   'admin\shifaris\business\industryRegistrationRecommController@edit')->name('admin.industryRegistrationRecomm.edit');
	Route::post('/industryRegistrationRecomm/{id}/update', 'admin\shifaris\business\industryRegistrationRecommController@update')->name('admin.industryRegistrationRecomm.update');

	/*--------------------------------------------- Scholarship Recommendation ---------------------------------------------*/
	Route::get('/scholarshipRecomm',             'admin\shifaris\academic\scholarshipRecommController@index')->name('admin.scholarshipRecomm.index');
	Route::get('/scholarshipRecomm/create', 'admin\shifaris\academic\scholarshipRecommController@create')->name('admin.scholarshipRecomm.create');
	Route::post('/scholarshipRecomm/store',      'admin\shifaris\academic\scholarshipRecommController@store')->name('admin.scholarshipRecomm.store');
	Route::get('/scholarshipRecomm/{id}/edit',   'admin\shifaris\academic\scholarshipRecommController@edit')->name('admin.scholarshipRecomm.edit');
	Route::post('/scholarshipRecomm/{id}/update', 'admin\shifaris\academic\scholarshipRecommController@update')->name('admin.scholarshipRecomm.update');

	/*--------------------------------------------- Widespread Recommendation ---------------------------------------------*/
	Route::get('/widespreadRecomm',             'admin\shifaris\academic\widespreadRecommController@index')->name('admin.widespreadRecomm.index');
	Route::get('/widespreadRecomm/create', 'admin\shifaris\academic\widespreadRecommController@create')->name('admin.widespreadRecomm.create');
	Route::post('/widespreadRecomm/store',      'admin\shifaris\academic\widespreadRecommController@store')->name('admin.widespreadRecomm.store');
	Route::get('/widespreadRecomm/{id}/edit',   'admin\shifaris\academic\widespreadRecommController@edit')->name('admin.widespreadRecomm.edit');
	Route::post('/widespreadRecomm/{id}/update', 'admin\shifaris\academic\widespreadRecommController@update')->name('admin.widespreadRecomm.update');

	/*--------------------------------------------- Annual Income Verification ---------------------------------------------*/
	Route::get('/verifyAnnualIncome',             'admin\shifaris\englishFormat\verifyAnnualIncomeController@index')->name('admin.verifyAnnualIncome.index');
	Route::get('/verifyAnnualIncome/create', 'admin\shifaris\englishFormat\verifyAnnualIncomeController@create')->name('admin.verifyAnnualIncome.create');
	Route::post('/verifyAnnualIncome/store',      'admin\shifaris\englishFormat\verifyAnnualIncomeController@store')->name('admin.verifyAnnualIncome.store');
	Route::get('/verifyAnnualIncome/{id}/edit',   'admin\shifaris\englishFormat\verifyAnnualIncomeController@edit')->name('admin.verifyAnnualIncome.edit');
	Route::post('/verifyAnnualIncome/{id}/update', 'admin\shifaris\englishFormat\verifyAnnualIncomeController@update')->name('admin.verifyAnnualIncome.update');

	/*--------------------------------------------- Annual Income Verification Japan ---------------------------------------------*/
	Route::get('/verifyAnnualIncomeJpn',             'admin\shifaris\englishFormat\verifyAnnualIncomeJpnController@index')->name('admin.verifyAnnualIncomeJpn.index');
	Route::get('/verifyAnnualIncomeJpn/create', 'admin\shifaris\englishFormat\verifyAnnualIncomeJpnController@create')->name('admin.verifyAnnualIncomeJpn.create');
	Route::post('/verifyAnnualIncomeJpn/store',      'admin\shifaris\englishFormat\verifyAnnualIncomeJpnController@store')->name('admin.verifyAnnualIncomeJpn.store');
	Route::get('/verifyAnnualIncomeJpn/{id}/edit',   'admin\shifaris\englishFormat\verifyAnnualIncomeJpnController@edit')->name('admin.verifyAnnualIncomeJpn.edit');
	Route::post('/verifyAnnualIncomeJpn/{id}/update', 'admin\shifaris\englishFormat\verifyAnnualIncomeJpnController@update')->name('admin.verifyAnnualIncomeJpn.update');

	/*--------------------------------------------- Property Valuation ---------------------------------------------*/
	Route::get('/propertyVal',             'admin\shifaris\englishFormat\propertyValController@index')->name('admin.propertyVal.index');
	Route::get('/propertyVal/create', 'admin\shifaris\englishFormat\propertyValController@create')->name('admin.propertyVal.create');
	Route::post('/propertyVal/store',      'admin\shifaris\englishFormat\propertyValController@store')->name('admin.propertyVal.store');
	Route::get('/propertyVal/{id}/edit',   'admin\shifaris\englishFormat\propertyValController@edit')->name('admin.propertyVal.edit');
	Route::post('/propertyVal/{id}/update', 'admin\shifaris\englishFormat\propertyValController@update')->name('admin.propertyVal.update');

	/*--------------------------------------------- Tax Clearance Certificate ---------------------------------------------*/
	Route::get('/taxClearCert',             'admin\shifaris\englishFormat\taxClearCertController@index')->name('admin.taxClearCert.index');
	Route::get('/taxClearCert/create', 'admin\shifaris\englishFormat\taxClearCertController@create')->name('admin.taxClearCert.create');
	Route::post('/taxClearCert/store',      'admin\shifaris\englishFormat\taxClearCertController@store')->name('admin.taxClearCert.store');
	Route::get('/taxClearCert/{id}/edit',   'admin\shifaris\englishFormat\taxClearCertController@edit')->name('admin.taxClearCert.edit');
	Route::post('/taxClearCert/{id}/update', 'admin\shifaris\englishFormat\taxClearCertController@update')->name('admin.taxClearCert.update');

	/*--------------------------------------------- Relationship Verification ---------------------------------------------*/
	Route::get('/relationVerify',             'admin\shifaris\englishFormat\relationVerifyController@index')->name('admin.relationVerify.index');
	Route::get('/relationVerify/create', 'admin\shifaris\englishFormat\relationVerifyController@create')->name('admin.relationVerify.create');
	Route::post('/relationVerify/store',      'admin\shifaris\englishFormat\relationVerifyController@store')->name('admin.relationVerify.store');
	Route::get('/relationVerify/{id}/edit',   'admin\shifaris\englishFormat\relationVerifyController@edit')->name('admin.relationVerify.edit');
	Route::post('/relationVerify/{id}/update', 'admin\shifaris\englishFormat\relationVerifyController@update')->name('admin.relationVerify.update');

	/*--------------------------------------------- Birth Date Verification ---------------------------------------------*/
	Route::get('/birthVerify',             'admin\shifaris\englishFormat\birthVerifyController@index')->name('admin.birthVerify.index');
	Route::get('/birthVerify/create', 'admin\shifaris\englishFormat\birthVerifyController@create')->name('admin.birthVerify.create');
	Route::post('/birthVerify/store',      'admin\shifaris\englishFormat\birthVerifyController@store')->name('admin.birthVerify.store');
	Route::get('/birthVerify/{id}/edit',   'admin\shifaris\englishFormat\birthVerifyController@edit')->name('admin.birthVerify.edit');
	Route::post('/birthVerify/{id}/update', 'admin\shifaris\englishFormat\birthVerifyController@update')->name('admin.birthVerify.update');

	/*--------------------------------------------- Unmarried Verification ---------------------------------------------*/
	Route::get('/unmarriedVerify',             'admin\shifaris\englishFormat\unmarriedVerifyController@index')->name('admin.unmarriedVerify.index');
	Route::get('/unmarriedVerify/create', 'admin\shifaris\englishFormat\unmarriedVerifyController@create')->name('admin.unmarriedVerify.create');
	Route::post('/unmarriedVerify/store',      'admin\shifaris\englishFormat\unmarriedVerifyController@store')->name('admin.unmarriedVerify.store');
	Route::get('/unmarriedVerify/{id}/edit',   'admin\shifaris\englishFormat\unmarriedVerifyController@edit')->name('admin.unmarriedVerify.edit');
	Route::post('/unmarriedVerify/{id}/update', 'admin\shifaris\englishFormat\unmarriedVerifyController@update')->name('admin.unmarriedVerify.update');

	/*--------------------------------------------- Marriage Verification ---------------------------------------------*/
	Route::get('/marriageVerify',             'admin\shifaris\englishFormat\marriageVerifyController@index')->name('admin.marriageVerify.index');
	Route::get('/marriageVerify/create', 'admin\shifaris\englishFormat\marriageVerifyController@create')->name('admin.marriageVerify.create');
	Route::post('/marriageVerify/store',      'admin\shifaris\englishFormat\marriageVerifyController@store')->name('admin.marriageVerify.store');
	Route::get('/marriageVerify/{id}/edit',   'admin\shifaris\englishFormat\marriageVerifyController@edit')->name('admin.marriageVerify.edit');
	Route::post('/marriageVerify/{id}/update', 'admin\shifaris\englishFormat\marriageVerifyController@update')->name('admin.marriageVerify.update');

	/*--------------------------------------------- Address Verification ---------------------------------------------*/
	Route::get('/addressVerify',             'admin\shifaris\englishFormat\addressVerifyController@index')->name('admin.addressVerify.index');
	Route::get('/addressVerify/create', 'admin\shifaris\englishFormat\addressVerifyController@create')->name('admin.addressVerify.create');
	Route::post('/addressVerify/store',      'admin\shifaris\englishFormat\addressVerifyController@store')->name('admin.addressVerify.store');
	Route::get('/addressVerify/{id}/edit',   'admin\shifaris\englishFormat\addressVerifyController@edit')->name('admin.addressVerify.edit');
	Route::post('/addressVerify/{id}/update', 'admin\shifaris\englishFormat\addressVerifyController@update')->name('admin.addressVerify.update');

	/*--------------------------------------------- Occupational Verification ---------------------------------------------*/
	Route::get('/occupationVerify',             'admin\shifaris\englishFormat\occupationVerifyController@index')->name('admin.occupationVerify.index');
	Route::get('/occupationVerify/create', 'admin\shifaris\englishFormat\occupationVerifyController@create')->name('admin.occupationVerify.create');
	Route::post('/occupationVerify/store',      'admin\shifaris\englishFormat\occupationVerifyController@store')->name('admin.occupationVerify.store');
	Route::get('/occupationVerify/{id}/edit',   'admin\shifaris\englishFormat\occupationVerifyController@edit')->name('admin.occupationVerify.edit');
	Route::post('/occupationVerify/{id}/update', 'admin\shifaris\englishFormat\occupationVerifyController@update')->name('admin.occupationVerify.update');

	/*--------------------------------------------- Identity Verification 1 ---------------------------------------------*/
	Route::get('/identityVerificationOne',             'admin\shifaris\englishFormat\identityVerificationOneController@index')->name('admin.identityVerificationOne.index');
	Route::get('/identityVerificationOne/create', 'admin\shifaris\englishFormat\identityVerificationOneController@create')->name('admin.identityVerificationOne.create');
	Route::post('/identityVerificationOne/store',      'admin\shifaris\englishFormat\identityVerificationOneController@store')->name('admin.identityVerificationOne.store');
	Route::get('/identityVerificationOne/{id}/edit',   'admin\shifaris\englishFormat\identityVerificationOneController@edit')->name('admin.identityVerificationOne.edit');
	Route::post('/identityVerificationOne/{id}/update', 'admin\shifaris\englishFormat\identityVerificationOneController@update')->name('admin.identityVerificationOne.update');

	/*--------------------------------------------- Identity Verification 2 ---------------------------------------------*/
	Route::get('/identityVerificationTwo',             'admin\shifaris\englishFormat\identityVerificationTwoController@index')->name('admin.identityVerificationTwo.index');
	Route::get('/identityVerificationTwo/create', 'admin\shifaris\englishFormat\identityVerificationTwoController@create')->name('admin.identityVerificationTwo.create');
	Route::post('/identityVerificationTwo/store',      'admin\shifaris\englishFormat\identityVerificationTwoController@store')->name('admin.identityVerificationTwo.store');
	Route::get('/identityVerificationTwo/{id}/edit',   'admin\shifaris\englishFormat\identityVerificationTwoController@edit')->name('admin.identityVerificationTwo.edit');
	Route::post('/identityVerificationTwo/{id}/update', 'admin\shifaris\englishFormat\identityVerificationTwoController@update')->name('admin.identityVerificationTwo.update');

	/*--------------------------------------------- Arthritis Support Recommendation ---------------------------------------------*/
	Route::get('/arthritisSupport',             'admin\shifaris\economic\arthritisSupportController@index')->name('admin.arthritisSupport.index');
	Route::get('/arthritisSupport/create', 'admin\shifaris\economic\arthritisSupportController@create')->name('admin.arthritisSupport.create');
	Route::post('/arthritisSupport/store',      'admin\shifaris\economic\arthritisSupportController@store')->name('admin.arthritisSupport.store');
	Route::get('/arthritisSupport/{id}/edit',   'admin\shifaris\economic\arthritisSupportController@edit')->name('admin.arthritisSupport.edit');
	Route::post('/arthritisSupport/{id}/update', 'admin\shifaris\economic\arthritisSupportController@update')->name('admin.arthritisSupport.update');

	/*--------------------------------------------- Yearly Income Certified ---------------------------------------------*/
	Route::get('/yearlyIncomeCert',             'admin\shifaris\economic\yearlyIncomeCertController@index')->name('admin.yearlyIncomeCert.index');
	Route::get('/yearlyIncomeCert/create', 'admin\shifaris\economic\yearlyIncomeCertController@create')->name('admin.yearlyIncomeCert.create');
	Route::post('/yearlyIncomeCert/store',      'admin\shifaris\economic\yearlyIncomeCertController@store')->name('admin.yearlyIncomeCert.store');
	Route::get('/yearlyIncomeCert/{id}/edit',   'admin\shifaris\economic\yearlyIncomeCertController@edit')->name('admin.yearlyIncomeCert.edit');
	Route::post('/yearlyIncomeCert/{id}/update', 'admin\shifaris\economic\yearlyIncomeCertController@update')->name('admin.yearlyIncomeCert.update');

	/*--------------------------------------------- Account Opening Recommendation ---------------------------------------------*/
	Route::get('/accountOpening',             'admin\shifaris\bankFinance\accountOpeningController@index')->name('admin.accountOpening.index');
	Route::get('/accountOpening/create', 'admin\shifaris\bankFinance\accountOpeningController@create')->name('admin.accountOpening.create');
	Route::post('/accountOpening/store',      'admin\shifaris\bankFinance\accountOpeningController@store')->name('admin.accountOpening.store');
	Route::get('/accountOpening/{id}/edit',   'admin\shifaris\bankFinance\accountOpeningController@edit')->name('admin.accountOpening.edit');
	Route::post('/accountOpening/{id}/update', 'admin\shifaris\bankFinance\accountOpeningController@update')->name('admin.accountOpening.update');

	/*--------------------------------------------- Account Closed Recommendation ---------------------------------------------*/
	Route::get('/accountClose',             'admin\shifaris\bankFinance\accountCloseController@index')->name('admin.accountClose.index');
	Route::get('/accountClose/create', 'admin\shifaris\bankFinance\accountCloseController@create')->name('admin.accountClose.create');
	Route::post('/accountClose/store',      'admin\shifaris\bankFinance\accountCloseController@store')->name('admin.accountClose.store');
	Route::get('/accountClose/{id}/edit',   'admin\shifaris\bankFinance\accountCloseController@edit')->name('admin.accountClose.edit');
	Route::post('/accountClose/{id}/update', 'admin\shifaris\bankFinance\accountCloseController@update')->name('admin.accountClose.update');

	/*--------------------------------------------- Account Navigation Recommendation ---------------------------------------------*/
	Route::get('/accountNavigate',             'admin\shifaris\bankFinance\accountNavigateController@index')->name('admin.accountNavigate.index');
	Route::get('/accountNavigate/create', 'admin\shifaris\bankFinance\accountNavigateController@create')->name('admin.accountNavigate.create');
	Route::post('/accountNavigate/store',      'admin\shifaris\bankFinance\accountNavigateController@store')->name('admin.accountNavigate.store');
	Route::get('/accountNavigate/{id}/edit',   'admin\shifaris\bankFinance\accountNavigateController@edit')->name('admin.accountNavigate.edit');
	Route::post('/accountNavigate/{id}/update', 'admin\shifaris\bankFinance\accountNavigateController@update')->name('admin.accountNavigate.update');

	/*--------------------------------------------- Money Transfer Recommendation ---------------------------------------------*/
	Route::get('/moneyTransfer',             'admin\shifaris\bankFinance\moneyTransferController@index')->name('admin.moneyTransfer.index');
	Route::get('/moneyTransfer/create', 'admin\shifaris\bankFinance\moneyTransferController@create')->name('admin.moneyTransfer.create');
	Route::post('/moneyTransfer/store',      'admin\shifaris\bankFinance\moneyTransferController@store')->name('admin.moneyTransfer.store');
	Route::get('/moneyTransfer/{id}/edit',   'admin\shifaris\bankFinance\moneyTransferController@edit')->name('admin.moneyTransfer.edit');
	Route::post('/moneyTransfer/{id}/update', 'admin\shifaris\bankFinance\moneyTransferController@update')->name('admin.moneyTransfer.update');

	/*--------------------------------------------- Reconciliation Recommendation ---------------------------------------------*/
	Route::get('/reconciliation',             'admin\shifaris\justiceLaw\reconciliationController@index')->name('admin.reconciliation.index');
	Route::get('/reconciliation/create', 'admin\shifaris\justiceLaw\reconciliationController@create')->name('admin.reconciliation.create');
	Route::post('/reconciliation/store',      'admin\shifaris\justiceLaw\reconciliationController@store')->name('admin.reconciliation.store');
	Route::get('/reconciliation/{id}/edit',   'admin\shifaris\justiceLaw\reconciliationController@edit')->name('admin.reconciliation.edit');
	Route::post('/reconciliation/{id}/update', 'admin\shifaris\justiceLaw\reconciliationController@update')->name('admin.reconciliation.update');

	/*--------------------------------------------- Commissioner Detail Recommendation ---------------------------------------------*/
	Route::get('/commissionerDetail',             'admin\shifaris\justiceLaw\commissionerDetailController@index')->name('admin.commissionerDetail.index');
	Route::get('/commissionerDetail/create', 'admin\shifaris\justiceLaw\commissionerDetailController@create')->name('admin.commissionerDetail.create');
	Route::post('/commissionerDetail/store',      'admin\shifaris\justiceLaw\commissionerDetailController@store')->name('admin.commissionerDetail.store');
	Route::get('/commissionerDetail/{id}/edit',   'admin\shifaris\justiceLaw\commissionerDetailController@edit')->name('admin.commissionerDetail.edit');
	Route::post('/commissionerDetail/{id}/update', 'admin\shifaris\justiceLaw\commissionerDetailController@update')->name('admin.commissionerDetail.update');

	/*--------------------------------------------- Varasis Authorities Recommendation ---------------------------------------------*/
	Route::get('/varasisAuthiorities',             'admin\shifaris\justiceLaw\varasisAuthioritiesController@index')->name('admin.varasisAuthiorities.index');
	Route::get('/varasisAuthiorities/create', 'admin\shifaris\justiceLaw\varasisAuthioritiesController@create')->name('admin.varasisAuthiorities.create');
	Route::post('/varasisAuthiorities/store',      'admin\shifaris\justiceLaw\varasisAuthioritiesController@store')->name('admin.varasisAuthiorities.store');
	Route::get('/varasisAuthiorities/{id}/edit',   'admin\shifaris\justiceLaw\varasisAuthioritiesController@edit')->name('admin.varasisAuthiorities.edit');
	Route::post('/varasisAuthiorities/{id}/update', 'admin\shifaris\justiceLaw\varasisAuthioritiesController@update')->name('admin.varasisAuthiorities.update');

	/*--------------------------------------------- In Nepali Language ---------------------------------------------*/
	Route::get('/nepLanguage',             'admin\shifaris\openFormat\nepLanguageController@index')->name('admin.nepLanguage.index');
	Route::get('/nepLanguage/create', 'admin\shifaris\openFormat\nepLanguageController@create')->name('admin.nepLanguage.create');
	Route::post('/nepLanguage/store',      'admin\shifaris\openFormat\nepLanguageController@store')->name('admin.nepLanguage.store');
	Route::get('/nepLanguage/{id}/edit',   'admin\shifaris\openFormat\nepLanguageController@edit')->name('admin.nepLanguage.edit');
	Route::post('/nepLanguage/{id}/update', 'admin\shifaris\openFormat\nepLanguageController@update')->name('admin.nepLanguage.update');

	/*--------------------------------------------- In English Language ---------------------------------------------*/
	Route::get('/engLanguage',             'admin\shifaris\openFormat\engLanguageController@index')->name('admin.engLanguage.index');
	Route::get('/engLanguage/create', 'admin\shifaris\openFormat\engLanguageController@create')->name('admin.engLanguage.create');
	Route::post('/engLanguage/store',      'admin\shifaris\openFormat\engLanguageController@store')->name('admin.engLanguage.store');
	Route::get('/engLanguage/{id}/edit',   'admin\shifaris\openFormat\engLanguageController@edit')->name('admin.engLanguage.edit');
	Route::post('/engLanguage/{id}/update', 'admin\shifaris\openFormat\engLanguageController@update')->name('admin.engLanguage.update');

	/*--------------------------------------------- Different Individuals Certified ---------------------------------------------*/
	Route::get('/diffIndividualCert',             'admin\shifaris\other\diffIndividualCertController@index')->name('admin.diffIndividualCert.index');
	Route::get('/diffIndividualCert/create', 'admin\shifaris\other\diffIndividualCertController@create')->name('admin.diffIndividualCert.create');
	Route::post('/diffIndividualCert/store',      'admin\shifaris\other\diffIndividualCertController@store')->name('admin.diffIndividualCert.store');
	Route::get('/diffIndividualCert/{id}/edit',   'admin\shifaris\other\diffIndividualCertController@edit')->name('admin.diffIndividualCert.edit');
	Route::post('/diffIndividualCert/{id}/update', 'admin\shifaris\other\diffIndividualCertController@update')->name('admin.diffIndividualCert.update');

	/*--------------------------------------------- Different Names are Certified ---------------------------------------------*/
	Route::get('/diffNameCert',             'admin\shifaris\other\diffNameCertController@index')->name('admin.diffNameCert.index');
	Route::get('/diffNameCert/create', 'admin\shifaris\other\diffNameCertController@create')->name('admin.diffNameCert.create');
	Route::post('/diffNameCert/store',      'admin\shifaris\other\diffNameCertController@store')->name('admin.diffNameCert.store');
	Route::get('/diffNameCert/{id}/edit',   'admin\shifaris\other\diffNameCertController@edit')->name('admin.diffNameCert.edit');
	Route::post('/diffNameCert/{id}/update', 'admin\shifaris\other\diffNameCertController@update')->name('admin.diffNameCert.update');

	/*--------------------------------------------- Different Birth Date Certified ---------------------------------------------*/
	Route::get('/diffBDCert',             'admin\shifaris\other\diffBDCertController@index')->name('admin.diffBDCert.index');
	Route::get('/diffBDCert/create', 'admin\shifaris\other\diffBDCertController@create')->name('admin.diffBDCert.create');
	Route::post('/diffBDCert/store',      'admin\shifaris\other\diffBDCertController@store')->name('admin.diffBDCert.store');
	Route::get('/diffBDCert/{id}/edit',   'admin\shifaris\other\diffBDCertController@edit')->name('admin.diffBDCert.edit');
	Route::post('/diffBDCert/{id}/update', 'admin\shifaris\other\diffBDCertController@update')->name('admin.diffBDCert.update');

	/*--------------------------------------------- Different English Spell Certified ---------------------------------------------*/
	Route::get('/diffEngSpellCert',             'admin\shifaris\other\diffEngSpellCertController@index')->name('admin.diffEngSpellCert.index');
	Route::get('/diffEngSpellCert/create', 'admin\shifaris\other\diffEngSpellCertController@create')->name('admin.diffEngSpellCert.create');
	Route::post('/diffEngSpellCert/store',      'admin\shifaris\other\diffEngSpellCertController@store')->name('admin.diffEngSpellCert.store');
	Route::get('/diffEngSpellCert/{id}/edit',   'admin\shifaris\other\diffEngSpellCertController@edit')->name('admin.diffEngSpellCert.edit');
	Route::post('/diffEngSpellCert/{id}/update', 'admin\shifaris\other\diffEngSpellCertController@update')->name('admin.diffEngSpellCert.update');

	/*--------------------------------------------- Recommendation Sent ---------------------------------------------*/
	Route::get('/recommSent',             'admin\shifaris\other\recommSentController@index')->name('admin.recommSent.index');
	Route::get('/recommSent/create', 'admin\shifaris\other\recommSentController@create')->name('admin.recommSent.create');
	Route::post('/recommSent/store',      'admin\shifaris\other\recommSentController@store')->name('admin.recommSent.store');
	Route::get('/recommSent/{id}/edit',   'admin\shifaris\other\recommSentController@edit')->name('admin.recommSent.edit');
	Route::post('/recommSent/{id}/update', 'admin\shifaris\other\recommSentController@update')->name('admin.recommSent.update');

	/*--------------------------------------------- Jet Machine Recommendation ---------------------------------------------*/
	Route::get('/jetMachineRecomm',             'admin\shifaris\other\jetMachineRecommController@index')->name('admin.jetMachineRecomm.index');
	Route::get('/jetMachineRecomm/create', 'admin\shifaris\other\jetMachineRecommController@create')->name('admin.jetMachineRecomm.create');
	Route::post('/jetMachineRecomm/store',      'admin\shifaris\other\jetMachineRecommController@store')->name('admin.jetMachineRecomm.store');
	Route::get('/jetMachineRecomm/{id}/edit',   'admin\shifaris\other\jetMachineRecommController@edit')->name('admin.jetMachineRecomm.edit');
	Route::post('/jetMachineRecomm/{id}/update', 'admin\shifaris\other\jetMachineRecommController@update')->name('admin.jetMachineRecomm.update');

	/*--------------------------------------------- Room Cleaning Recommedation ---------------------------------------------*/
	Route::get('/roomCleaningRecomm',             'admin\shifaris\other\roomCleaningRecommController@index')->name('admin.roomCleaningRecomm.index');
	Route::get('/roomCleaningRecomm/create', 'admin\shifaris\other\roomCleaningRecommController@create')->name('admin.roomCleaningRecomm.create');
	Route::post('/roomCleaningRecomm/store',      'admin\shifaris\other\roomCleaningRecommController@store')->name('admin.roomCleaningRecomm.store');
	Route::get('/roomCleaningRecomm/{id}/edit',   'admin\shifaris\other\roomCleaningRecommController@edit')->name('admin.roomCleaningRecomm.edit');
	Route::post('/roomCleaningRecomm/{id}/update', 'admin\shifaris\other\roomCleaningRecommController@update')->name('admin.roomCleaningRecomm.update');

	/*--------------------------------------------- Recommendation not to Court ---------------------------------------------*/
	Route::get('/recommCourt',             'admin\shifaris\other\recommCourtController@index')->name('admin.recommCourt.index');
	Route::get('/recommCourt/create', 'admin\shifaris\other\recommCourtController@create')->name('admin.recommCourt.create');
	Route::post('/recommCourt/store',      'admin\shifaris\other\recommCourtController@store')->name('admin.recommCourt.store');
	Route::get('/recommCourt/{id}/edit',   'admin\shifaris\other\recommCourtController@edit')->name('admin.recommCourt.edit');
	Route::post('/recommCourt/{id}/update', 'admin\shifaris\other\recommCourtController@update')->name('admin.recommCourt.update');

	/*--------------------------------------------- Recommendation Of Class Additional Approve ---------------------------------------------*/
	Route::get('/classAddRecomm',             'admin\shifaris\other\classAddRecommController@index')->name('admin.classAddRecomm.index');
	Route::get('/classAddRecomm/create', 'admin\shifaris\other\classAddRecommController@create')->name('admin.classAddRecomm.create');
	Route::post('/classAddRecomm/store',      'admin\shifaris\other\classAddRecommController@store')->name('admin.classAddRecomm.store');
	Route::get('/classAddRecomm/{id}/edit',   'admin\shifaris\other\classAddRecommController@edit')->name('admin.classAddRecomm.edit');
	Route::post('/classAddRecomm/{id}/update', 'admin\shifaris\other\classAddRecommController@update')->name('admin.classAddRecomm.update');

	/*--------------------------------------------- Attendance ---------------------------------------------*/
	Route::get('/attendanceCert',             'admin\shifaris\other\attendanceCertController@index')->name('admin.attendanceCert.index');
	Route::get('/attendanceCert/create', 'admin\shifaris\other\attendanceCertController@create')->name('admin.attendanceCert.create');
	Route::post('/attendanceCert/store',      'admin\shifaris\other\attendanceCertController@store')->name('admin.attendanceCert.store');
	Route::get('/attendanceCert/{id}/edit',   'admin\shifaris\other\attendanceCertController@edit')->name('admin.attendanceCert.edit');
	Route::post('/attendanceCert/{id}/update', 'admin\shifaris\other\attendanceCertController@update')->name('admin.attendanceCert.update');

	/*--------------------------------------------- Consumer Committee Constituted ---------------------------------------------*/
	Route::get('/consumerCommitte',             'admin\shifaris\other\consumerCommitteController@index')->name('admin.consumerCommitte.index');
	Route::get('/consumerCommitte/create', 'admin\shifaris\other\consumerCommitteController@create')->name('admin.consumerCommitte.create');
	Route::post('/consumerCommitte/store',      'admin\shifaris\other\consumerCommitteController@store')->name('admin.consumerCommitte.store');
	Route::get('/consumerCommitte/{id}/edit',   'admin\shifaris\other\consumerCommitteController@edit')->name('admin.consumerCommitte.edit');
	Route::post('/consumerCommitte/{id}/update', 'admin\shifaris\other\consumerCommitteController@update')->name('admin.consumerCommitte.update');

	/*--------------------------------------------- Provider Wood Reccomendation ---------------------------------------------*/
	Route::get('/woodProviderRecomm',             'admin\shifaris\other\woodProviderRecommController@index')->name('admin.woodProviderRecomm.index');
	Route::get('/woodProviderRecomm/create', 'admin\shifaris\other\woodProviderRecommController@create')->name('admin.woodProviderRecomm.create');
	Route::post('/woodProviderRecomm/store',      'admin\shifaris\other\woodProviderRecommController@store')->name('admin.woodProviderRecomm.store');
	Route::get('/woodProviderRecomm/{id}/edit',   'admin\shifaris\other\woodProviderRecommController@edit')->name('admin.woodProviderRecomm.edit');
	Route::post('/woodProviderRecomm/{id}/update', 'admin\shifaris\other\woodProviderRecommController@update')->name('admin.woodProviderRecomm.update');

	/*--------------------------------------------- Natural Calamities Recommendation ---------------------------------------------*/
	Route::get('/naturalCalamities',             'admin\shifaris\other\naturalCalamitiesController@index')->name('admin.naturalCalamities.index');
	Route::get('/naturalCalamities/create', 'admin\shifaris\other\naturalCalamitiesController@create')->name('admin.naturalCalamities.create');
	Route::post('/naturalCalamities/store',      'admin\shifaris\other\naturalCalamitiesController@store')->name('admin.naturalCalamities.store');
	Route::get('/naturalCalamities/{id}/edit',   'admin\shifaris\other\naturalCalamitiesController@edit')->name('admin.naturalCalamities.edit');
	Route::post('/naturalCalamities/{id}/update', 'admin\shifaris\other\naturalCalamitiesController@update')->name('admin.naturalCalamities.update');

	/*--------------------------------------------- Handicap Identity Card Recommendation ---------------------------------------------*/
	Route::get('/handicapId',             'admin\shifaris\other\handicapIdController@index')->name('handicapId.index');
	Route::get('/handicapId/create', 'admin\shifaris\other\handicapIdController@create')->name('handicapId.create');
	Route::post('/handicapId/store',      'admin\shifaris\other\handicapIdController@store')->name('handicapId.store');
	Route::get('/handicapId/{id}/edit',   'admin\shifaris\other\handicapIdController@edit')->name('handicapId.edit');
	Route::post('/handicapId/{id}/update', 'admin\shifaris\other\handicapIdController@update')->name('handicapId.update');

	/*--------------------------------------------- Register Book ---------------------------------------------*/
	Route::get('/regBook',             'admin\shifaris\registerBook\regBookController@index')->name('admin.regBook.index');
	Route::get('/regBook/create', 'admin\shifaris\registerBook\regBookController@create')->name('admin.regBook.create');
	Route::post('/regBook/store',      'admin\shifaris\registerBook\regBookController@store')->name('admin.regBook.store');
	Route::get('/regBook/{id}/edit',   'admin\shifaris\registerBook\regBookController@edit')->name('admin.regBook.edit');
	Route::post('/regBook/{id}/update', 'admin\shifaris\registerBook\regBookController@update')->name('admin.regBook.update');

	/*--------------------------------------------- Rolling Book ---------------------------------------------*/
	Route::get('/rollBook',             'admin\shifaris\registerBook\rollBookController@index')->name('admin.rollBook.index');
	Route::get('/rollBook/create', 'admin\shifaris\registerBook\rollBookController@create')->name('admin.rollBook.create');
	Route::post('/rollBook/store',      'admin\shifaris\registerBook\rollBookController@store')->name('admin.rollBook.store');
	Route::get('/rollBook/{id}/edit',   'admin\shifaris\registerBook\rollBookController@edit')->name('admin.rollBook.edit');
	Route::post('/rollBook/{id}/update', 'admin\shifaris\registerBook\rollBookController@update')->name('admin.rollBook.update');

	/*--------------------------------------------- Daily report ---------------------------------------------*/
	Route::get('/dailyReport',             'admin\shifaris\report\dailyReportController@index')->name('admin.dailyReport.index');
	Route::get('/dailyReport/create', 'admin\shifaris\report\dailyReportController@create')->name('admin.dailyReport.create');
	Route::post('/dailyReport/store',      'admin\shifaris\report\dailyReportController@store')->name('admin.dailyReport.store');
	Route::get('/dailyReport/{id}/edit',   'admin\shifaris\report\dailyReportController@edit')->name('admin.dailyReport.edit');
	Route::post('/dailyReport/{id}/update', 'admin\shifaris\report\dailyReportController@update')->name('admin.dailyReport.update');

	/*--------------------------------------------- Daily Sarash report ---------------------------------------------*/
	Route::get('/dailySarashReport',             'admin\shifaris\report\dailySarashReportController@index')->name('admin.dailySarashReport.index');
	Route::get('/dailySarashReport/create', 'admin\shifaris\report\dailySarashReportController@create')->name('admin.dailySarashReport.create');
	Route::post('/dailySarashReport/store',      'admin\shifaris\report\dailySarashReportController@store')->name('admin.dailySarashReport.store');
	Route::get('/dailySarashReport/{id}/edit',   'admin\shifaris\report\dailySarashReportController@edit')->name('admin.dailySarashReport.edit');
	Route::post('/dailySarashReport/{id}/update', 'admin\shifaris\report\dailySarashReportController@update')->name('admin.dailySarashReport.update');

	/*--------------------------------------------- Register report ---------------------------------------------*/
	Route::get('/regReport',             'admin\shifaris\report\regReportController@index')->name('admin.regReport.index');
	Route::get('/regReport/create', 'admin\shifaris\report\regReportController@create')->name('admin.regReport.create');
	Route::post('/regReport/store',      'admin\shifaris\report\regReportController@store')->name('admin.regReport.store');
	Route::get('/regReport/{id}/edit',   'admin\shifaris\report\regReportController@edit')->name('admin.regReport.edit');
	Route::post('/regReport/{id}/update', 'admin\shifaris\report\regReportController@update')->name('admin.regReport.update');

	/*--------------------------------------------- Currency report ---------------------------------------------*/
	Route::get('/currReport',             'admin\shifaris\report\currReportController@index')->name('admin.currReport.index');
	Route::get('/currReport/create', 'admin\shifaris\report\currReportController@create')->name('admin.currReport.create');
	Route::post('/currReport/store',      'admin\shifaris\report\currReportController@store')->name('admin.currReport.store');
	Route::get('/currReport/{id}/edit',   'admin\shifaris\report\currReportController@edit')->name('admin.currReport.edit');
	Route::post('/currReport/{id}/update', 'admin\shifaris\report\currReportController@update')->name('admin.currReport.update');

	/*--------------------------------------------- Bato Kayam ---------------------------------------------*/
	Route::get('/batoKayam',             'admin\shifaris\gharJagga\batoKayamController@index')->name('batoKayam.index');
	Route::get('/batoKayam/create', 'admin\shifaris\gharJagga\batoKayamController@create')->name('batoKayam.create');
	Route::post('/batoKayam/store',      'admin\shifaris\gharJagga\batoKayamController@store')->name('batoKayam.store');
	Route::get('/batoKayam/{id}/edit',   'admin\shifaris\gharJagga\batoKayamController@edit')->name('batoKayam.edit');
	Route::post('/batoKayam/{id}/update', 'admin\shifaris\gharJagga\batoKayamController@update')->name('batoKayam.update');

	/*--------------------------------------------- Lalpurja Harayeko ---------------------------------------------*/
	Route::get('/lalpurjaHarayeko',             'admin\shifaris\gharJagga\lalpurjaHarayekoController@index')->name('lalpurjaHarayeko.index');
	Route::get('/lalpurjaHarayeko/create', 'admin\shifaris\gharJagga\lalpurjaHarayekoController@create')->name('lalpurjaHarayeko.create');
	Route::post('/lalpurjaHarayeko/store',      'admin\shifaris\gharJagga\lalpurjaHarayekoController@store')->name('lalpurjaHarayeko.store');
	Route::get('/lalpurjaHarayeko/{id}/edit',   'admin\shifaris\gharJagga\lalpurjaHarayekoController@edit')->name('lalpurjaHarayeko.edit');
	Route::post('/lalpurjaHarayeko/{id}/update', 'admin\shifaris\gharJagga\lalpurjaHarayekoController@update')->name('lalpurjaHarayeko.update');

	/*--------------------------------------------- Mohi Kitta  ---------------------------------------------*/
	Route::get('/mohiKitta',             'admin\shifaris\gharJagga\mohiKittaController@index')->name('mohiKitta.index');
	Route::get('/mohiKitta/create', 'admin\shifaris\gharJagga\mohiKittaController@create')->name('mohiKitta.create');
	Route::post('/mohiKitta/store',      'admin\shifaris\gharJagga\mohiKittaController@store')->name('mohiKitta.store');
	Route::get('/mohiKitta/{id}/edit',   'admin\shifaris\gharJagga\mohiKittaController@edit')->name('mohiKitta.edit');
	Route::post('/mohiKitta/{id}/update', 'admin\shifaris\gharJagga\mohiKittaController@update')->name('mohiKitta.update');

	/*--------------------------------------------- Ghar Patal ---------------------------------------------*/
	Route::get('/gharPatal',             'admin\shifaris\gharJagga\gharPatalController@index')->name('gharPatal.index');
	Route::get('/gharPatal/create', 'admin\shifaris\gharJagga\gharPatalController@create')->name('gharPatal.create');
	Route::post('/gharPatal/store',      'admin\shifaris\gharJagga\gharPatalController@store')->name('gharPatal.store');
	Route::get('/gharPatal/{id}/edit',   'admin\shifaris\gharJagga\gharPatalController@edit')->name('gharPatal.edit');
	Route::post('/gharPatal/{id}/update', 'admin\shifaris\gharJagga\gharPatalController@update')->name('gharPatal.update');

	/*--------------------------------------------- KittaKat shifaris ---------------------------------------------*/
	Route::get('/kittaKatShifaris',             'admin\shifaris\gharJagga\kittaKatShifarisController@index')->name('kittaKatShifaris.index');
	Route::get('/kittaKatShifaris/create', 'admin\shifaris\gharJagga\kittaKatShifarisController@create')->name('kittaKatShifaris.create');
	Route::post('/kittaKatShifaris/store',      'admin\shifaris\gharJagga\kittaKatShifarisController@store')->name('kittaKatShifaris.store');
	Route::get('/kittaKatShifaris/{id}/edit',   'admin\shifaris\gharJagga\kittaKatShifarisController@edit')->name('kittaKatShifaris.edit');
	Route::post('/kittaKatShifaris/{id}/update', 'admin\shifaris\gharJagga\kittaKatShifarisController@update')->name('kittaKatShifaris.update');

	/*--------------------------------------------- KittaKat shifaris ---------------------------------------------*/
	Route::get('/charKillaBibaran',             'admin\shifaris\gharJagga\charKillaBibaranController@index')->name('charKillaBibaran.index');
	Route::get('/charKillaBibaran/create', 'admin\shifaris\gharJagga\charKillaBibaranController@create')->name('charKillaBibaran.create');
	Route::post('/charKillaBibaran/store',      'admin\shifaris\gharJagga\charKillaBibaranController@store')->name('charKillaBibaran.store');
	Route::get('/charKillaBibaran/{id}/edit',   'admin\shifaris\gharJagga\charKillaBibaranController@edit')->name('charKillaBibaran.edit');
	Route::post('/charKillaBibaran/{id}/update', 'admin\shifaris\gharJagga\charKillaBibaranController@update')->name('charKillaBibaran.update');

	/*--------------------------------------------- khulaiPathayeko shifaris ---------------------------------------------*/
	Route::get('/khulaiPathayeko',             'admin\shifaris\other\khulaiPathayekoController@index')->name('khulaiPathayeko.index');
	Route::get('/khulaiPathayeko/create', 'admin\shifaris\other\khulaiPathayekoController@create')->name('khulaiPathayeko.create');
	Route::post('/khulaiPathayeko/store',      'admin\shifaris\other\khulaiPathayekoController@store')->name('khulaiPathayeko.store');
	Route::get('/khulaiPathayeko/{id}/edit',   'admin\shifaris\other\khulaiPathayekoController@edit')->name('khulaiPathayeko.edit');
	Route::post('/khulaiPathayeko/{id}/update', 'admin\shifaris\other\khulaiPathayekoController@update')->name('khulaiPathayeko.update');

	/*--------------------------------------------- khulaiPathayeko shifaris ---------------------------------------------*/
	Route::get('/propertyNameTransfer',             'admin\shifaris\gharJagga\propertyNameTransferController@index')->name('propertyNameTransfer.index');
	Route::get('/propertyNameTransfer/create', 'admin\shifaris\gharJagga\propertyNameTransferController@create')->name('propertyNameTransfer.create');
	Route::post('/propertyNameTransfer/store',      'admin\shifaris\gharJagga\propertyNameTransferController@store')->name('propertyNameTransfer.store');
	Route::get('/propertyNameTransfer/{id}/edit',   'admin\shifaris\gharJagga\propertyNameTransferController@edit')->name('propertyNameTransfer.edit');
	Route::post('/propertyNameTransfer/{id}/update', 'admin\shifaris\gharJagga\propertyNameTransferController@update')->name('propertyNameTransfer.update');

	/*--------------------------------------------- Add house in Property Paper ---------------------------------------------*/
	Route::get('/addHomePropertyPaper',             'admin\shifaris\gharJagga\addHomePropertyPaperController@index')->name('addHomePropertyPaper.index');
	Route::get('/addHomePropertyPaper/create', 'admin\shifaris\gharJagga\addHomePropertyPaperController@create')->name('addHomePropertyPaper.create');
	Route::post('/addHomePropertyPaper/store',      'admin\shifaris\gharJagga\addHomePropertyPaperController@store')->name('addHomePropertyPaper.store');
	Route::get('/addHomePropertyPaper/{id}/edit',   'admin\shifaris\gharJagga\addHomePropertyPaperController@edit')->name('addHomePropertyPaper.edit');
	Route::post('/addHomePropertyPaper/{id}/update', 'admin\shifaris\gharJagga\addHomePropertyPaperController@update')->name('addHomePropertyPaper.update');

	
	/*--------------------------------------------- Add house in Property Paper ---------------------------------------------*/
	Route::get('/internalMigration',             'admin\shifaris\gharJagga\internalMigrationController@index')->name('internalMigration.index');
	Route::get('/internalMigration/create', 'admin\shifaris\gharJagga\internalMigrationController@create')->name('internalMigration.create');
	Route::post('/internalMigration/store',      'admin\shifaris\gharJagga\internalMigrationController@store')->name('internalMigration.store');
	Route::get('/internalMigration/{id}/edit',   'admin\shifaris\gharJagga\internalMigrationController@edit')->name('internalMigration.edit');
	Route::post('/internalMigration/{id}/update', 'admin\shifaris\gharJagga\internalMigrationController@update')->name('internalMigration.update');


	
	/*---------------------------------------------Ghar Bato Verify shifaris ---------------------------------------------*/
	Route::get('/gharbatoVerify',             'admin\shifaris\gharJagga\gharbatoVerifyController@index')->name('gharbatoVerify.index');
	Route::get('/gharbatoVerify/create', 'admin\shifaris\gharJagga\gharbatoVerifyController@create')->name('gharbatoVerify.create');
	Route::post('/gharbatoVerify/store',      'admin\shifaris\gharJagga\gharbatoVerifyController@store')->name('gharbatoVerify.store');
	Route::get('/gharbatoVerify/{id}/edit',   'admin\shifaris\gharJagga\gharbatoVerifyController@edit')->name('gharbatoVerify.edit');
	Route::post('/gharbatoVerify/{id}/update', 'admin\shifaris\gharJagga\gharbatoVerifyController@update')->name('gharbatoVerify.update');

	/*---------------------------------------------Ghar Kayam ---------------------------------------------*/
	Route::get('/gharKayam',             'admin\shifaris\gharJagga\gharKayamController@index')->name('gharKayam.index');
	Route::get('/gharKayam/create', 'admin\shifaris\gharJagga\gharKayamController@create')->name('gharKayam.create');
	Route::post('/gharKayam/store',      'admin\shifaris\gharJagga\gharKayamController@store')->name('gharKayam.store');
	Route::get('/gharKayam/{id}/edit',   'admin\shifaris\gharJagga\gharKayamController@edit')->name('gharKayam.edit');
	Route::post('/gharKayam/{id}/update', 'admin\shifaris\gharJagga\gharKayamController@update')->name('gharKayam.update');

	/*---------------------------------------------Bijuli Jadan ---------------------------------------------*/
	Route::get('/BijuliJadanShifaris',             'admin\shifaris\gharJagga\BijuliJadanController@index')->name('BijuliJadanShifaris.index');
	Route::get('/BijuliJadanShifaris/create', 'admin\shifaris\gharJagga\BijuliJadanController@create')->name('BijuliJadanShifaris.create');
	Route::post('/BijuliJadanShifaris/store',      'admin\shifaris\gharJagga\BijuliJadanController@store')->name('BijuliJadanShifaris.store');
	Route::get('/BijuliJadanShifaris/{id}/edit',   'admin\shifaris\gharJagga\BijuliJadanController@edit')->name('BijuliJadanShifaris.edit');
	Route::post('/BijuliJadanShifaris/{id}/update', 'admin\shifaris\gharJagga\BijuliJadanController@update')->name('BijuliJadanShifaris.update');

	/*---------------------------------------------Dhara Jadan ---------------------------------------------*/
	Route::get('/DharaJadan',             'admin\shifaris\gharJagga\DharaJadanController@index')->name('DharaJadan.index');
	Route::get('/DharaJadan/create', 'admin\shifaris\gharJagga\DharaJadanController@create')->name('DharaJadan.create');
	Route::post('/DharaJadan/store',      'admin\shifaris\gharJagga\DharaJadanController@store')->name('DharaJadan.store');
	Route::get('/DharaJadan/{id}/edit',   'admin\shifaris\gharJagga\DharaJadanController@edit')->name('DharaJadan.edit');
	Route::post('/DharaJadan/{id}/update', 'admin\shifaris\gharJagga\DharaJadanController@update')->name('DharaJadan.update');

	/*--------------------------------------------- Relation Identification ---------------------------------------------*/
	Route::get('/relationIdentify',             'admin\shifaris\defineRelation\relationIdentifyController@index')->name('relationIdentify.index');
	Route::get('/relationIdentify/create', 'admin\shifaris\defineRelation\relationIdentifyController@create')->name('relationIdentify.create');
	Route::post('/relationIdentify/store',      'admin\shifaris\defineRelation\relationIdentifyController@store')->name('relationIdentify.store');
	Route::get('/relationIdentify/{id}/edit',   'admin\shifaris\defineRelation\relationIdentifyController@edit')->name('relationIdentify.edit');
	Route::post('/relationIdentify/{id}/update', 'admin\shifaris\defineRelation\relationIdentifyController@update')->name('relationIdentify.update');

	/*--------------------------------------------- Permanent Resident Proposal ---------------------------------------------*/
	Route::get('/permanentResidentProposal',             'admin\shifaris\gharJagga\permanentResidentProposalController@index')->name('permanentResidentProposal.index');
	Route::get('/permanentResidentProposal/create', 'admin\shifaris\gharJagga\permanentResidentProposalController@create')->name('permanentResidentProposal.create');
	Route::post('/permanentResidentProposal/store',      'admin\shifaris\gharJagga\permanentResidentProposalController@store')->name('permanentResidentProposal.store');
	Route::get('/permanentResidentProposal/{id}/edit',   'admin\shifaris\gharJagga\permanentResidentProposalController@edit')->name('permanentResidentProposal.edit');
	Route::post('/permanentResidentProposal/{id}/update', 'admin\shifaris\gharJagga\permanentResidentProposalController@update')->name('permanentResidentProposal.update');

	/*--------------------------------------------- Temporary Resident Proposal ---------------------------------------------*/
	Route::get('/temporaryResidentProposal',             'admin\shifaris\gharJagga\temporaryResidentProposalController@index')->name('temporaryResidentProposal.index');
	Route::get('/temporaryResidentProposal/create', 'admin\shifaris\gharJagga\temporaryResidentProposalController@create')->name('temporaryResidentProposal.create');
	Route::post('/temporaryResidentProposal/store',      'admin\shifaris\gharJagga\temporaryResidentProposalController@store')->name('temporaryResidentProposal.store');
	Route::get('/temporaryResidentProposal/{id}/edit',   'admin\shifaris\gharJagga\temporaryResidentProposalController@edit')->name('temporaryResidentProposal.edit');
	Route::post('/temporaryResidentProposal/{id}/update', 'admin\shifaris\gharJagga\temporaryResidentProposalController@update')->name('temporaryResidentProposal.update');

	/*--------------------------------------------- Mritak Hakdar Shifaris ---------------------------------------------*/
	Route::get('/mritakHakdarShifaris',             'admin\shifaris\defineRelation\mritakHakdarShifarisController@index')->name('mritakHakdarShifaris.index');
	Route::get('/mritakHakdarShifaris/create', 'admin\shifaris\defineRelation\mritakHakdarShifarisController@create')->name('mritakHakdarShifaris.create');
	Route::post('/mritakHakdarShifaris/store',      'admin\shifaris\defineRelation\mritakHakdarShifarisController@store')->name('mritakHakdarShifaris.store');
	Route::get('/mritakHakdarShifaris/{id}/edit',   'admin\shifaris\defineRelation\mritakHakdarShifarisController@edit')->name('mritakHakdarShifaris.edit');
	Route::post('/mritakHakdarShifaris/{id}/update', 'admin\shifaris\defineRelation\mritakHakdarShifarisController@update')->name('mritakHakdarShifaris.update');

	/*--------------------------------------------- Family Hierarchy Shifaris ---------------------------------------------*/
	Route::get('/familyHierarchy',             'admin\shifaris\defineRelation\familyHierarchyController@index')->name('familyHierarchy.index');
	Route::get('/familyHierarchy/create', 'admin\shifaris\defineRelation\familyHierarchyController@create')->name('familyHierarchy.create');
	Route::post('/familyHierarchy/store',      'admin\shifaris\defineRelation\familyHierarchyController@store')->name('familyHierarchy.store');
	Route::get('/familyHierarchy/{id}/edit',   'admin\shifaris\defineRelation\familyHierarchyController@edit')->name('familyHierarchy.edit');
	Route::post('/familyHierarchy/{id}/update', 'admin\shifaris\defineRelation\familyHierarchyController@update')->name('familyHierarchy.update');


	// Nepali Format
	/*--------------------------------------------- Unmarried Verify ---------------------------------------------*/
	Route::get('/unmarriedVerifyNep',             'admin\shifaris\nepaliFormat\unmarriedVerifyNepController@index')->name('unmarriedVerifyNep.index');
	Route::get('/unmarriedVerifyNep/create', 'admin\shifaris\nepaliFormat\unmarriedVerifyNepController@create')->name('unmarriedVerifyNep.create');
	Route::post('/unmarriedVerifyNep/store',      'admin\shifaris\nepaliFormat\unmarriedVerifyNepController@store')->name('unmarriedVerifyNep.store');
	Route::get('/unmarriedVerifyNep/{id}/edit',   'admin\shifaris\nepaliFormat\unmarriedVerifyNepController@edit')->name('unmarriedVerifyNep.edit');
	Route::post('/unmarriedVerifyNep/{id}/update', 'admin\shifaris\nepaliFormat\unmarriedVerifyNepController@update')->name('unmarriedVerifyNep.update');

	/*--------------------------------------------- married Verify ---------------------------------------------*/
	Route::get('/marriedVerifyNep',             'admin\shifaris\nepaliFormat\marriedVerifyNepController@index')->name('marriedVerifyNep.index');
	Route::get('/marriedVerifyNep/create', 'admin\shifaris\nepaliFormat\marriedVerifyNepController@create')->name('marriedVerifyNep.create');
	Route::post('/marriedVerifyNep/store',      'admin\shifaris\nepaliFormat\marriedVerifyNepController@store')->name('marriedVerifyNep.store');
	Route::get('/marriedVerifyNep/{id}/edit',   'admin\shifaris\nepaliFormat\marriedVerifyNepController@edit')->name('marriedVerifyNep.edit');
	Route::post('/marriedVerifyNep/{id}/update', 'admin\shifaris\nepaliFormat\marriedVerifyNepController@update')->name('marriedVerifyNep.update');

	/*--------------------------------------------- Birthdate verify Nepali ---------------------------------------------*/
	Route::get('/birthDateVerifyNep',             'admin\shifaris\nepaliFormat\birthDateVerifyNepController@index')->name('birthDateVerifyNep.index');
	Route::get('/birthDateVerifyNep/create', 'admin\shifaris\nepaliFormat\birthDateVerifyNepController@create')->name('birthDateVerifyNep.create');
	Route::post('/birthDateVerifyNep/store',      'admin\shifaris\nepaliFormat\birthDateVerifyNepController@store')->name('birthDateVerifyNep.store');
	Route::get('/birthDateVerifyNep/{id}/edit',   'admin\shifaris\nepaliFormat\birthDateVerifyNepController@edit')->name('birthDateVerifyNep.edit');
	Route::post('/birthDateVerifyNep/{id}/update', 'admin\shifaris\nepaliFormat\birthDateVerifyNepController@update')->name('birthDateVerifyNep.update');
});
?>