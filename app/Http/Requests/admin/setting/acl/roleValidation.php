<?php

namespace App\Http\Requests\admin\setting\acl;

use Illuminate\Foundation\Http\FormRequest;

class roleValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'roleName' => 'required|min:3|max:30'
        ];
    }
}
