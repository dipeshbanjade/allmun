<?php

namespace App\Http\Requests\admin\setting\phone;

use Illuminate\Foundation\Http\FormRequest;

class PhoneValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:phones',
            'name' => 'required|min:3|max:50',
            'mobileTag' => 'required|min:3|max:50'
        ];
    }
}
