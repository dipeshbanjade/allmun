<?php

namespace App\Http\Requests\admin\setting\handWashMethod;

use Illuminate\Foundation\Http\FormRequest;

class HandWashMethodVal extends FormRequest
{

    public function authorize()
    {
        return true;
    }

     public function rules()
    {
        return [
            'nameEng' => 'required|min:4:min:50'
        ];
    }
}
