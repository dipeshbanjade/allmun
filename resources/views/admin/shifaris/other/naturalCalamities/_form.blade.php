<div class="right_col nep" role="main" style="min-height: 882px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">दैविक प्रकोप सिफारिस</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

          @include('admin.shifaris.municipalityDetail')

          <div class="row top-part">
            <div class="col-md-6">
             <p align="left">
              <b>@lang('commonField.extra.refCode'):</b>
              @if(isset($data->refCode))
              {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
              @else
              {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
              @endif
            </p>
            <p align="left">
              <b> च. नं.:
                {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
              </b>
            </p>
          </div>
          <div class="col-md-6">
            <p align="right">
              <b class="mt">मिति :
               {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
             </b>
           </p>
         </div>
       </div>
       <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
          </p><h4 align="center"><b>विषय: प्रमाणित सिफारिस गरिएको बारे।</b> </h4>
          {{ Form::hidden('letter_subject', 'प्रमाणित सिफारिस गरिएको बारे', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}

        </div>
      </div>
      <div class="row title-left">
        <div class="col-md-12">
          <p>श्री  {{ Form::text('companyHead', 'जिल्ला दैविक प्रकोप उद्धार', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyHead', 'required' => 'required']) }}</p>
          <p> {{ Form::text('companyAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyAddress', 'required' => 'required']) }}।</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 content-para">
          <p align="justify">
            उपरोक्त विषयमा 
            {{ Form::text('candidateAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'candidateAddress', 'required' => 'required']) }}
            बस्ने  
            {{ Form::text('candidateName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'candidateName', 'required' => 'required']) }}
            ले 
            {{ Form::text('calamitiesLocation', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'calamitiesLocation', 'required' => 'required']) }}
            मा मिति 
            {{ Form::text('calamitiesDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'calamitiesDate',  'onfocus' => 'showNdpCalendarBox("calamitiesDate")']) }}
            गतेका दिन 
            {{ Form::text('calamitiesName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'calamitiesName', 'required' => 'required']) }}
            भएको हुँदा राहातको लागि सिफारिस पाऊँ भनी दिनु भएको निवेदन अनुसार सम्बन्धित स्थानमा गई गाँउ घर छर छिमेक जम्मा गरी सर्जमिन 
            बुझ्दा 
            {{ Form::text('jaminAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'jaminAddress', 'required' => 'required']) }}
            बस्ने 
            {{ Form::text('jaminMan', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'jaminMan', 'required' => 'required']) }}समेत  
            बस्ने 
            {{ Form::text('jaminTotalMan', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'jaminTotalMan', 'required' => 'required']) }}
            जना बसी गरी दिनु भएको सर्जमिन 
            मुचुल्का यसै पत्र संलग्न राखी पिडित परिवार लाई राहत उपलब्ध गरी दिनुहुन सिफारिस गरिएको व्यहोरा अनुरोध छ।
          </p> 
        </div>
        <div class="col-md-12">
          <h2>बोधार्थ&nbsp;  </h2>
          <table class="table table-bordered" id="sabik_address_table">
            <tbody>
              <?php 
              $count = 1;
              $tableData = !empty($data) ? json_decode($data->bodartha, true) : [1];
          //dd($tableData);
              foreach($tableData as $singleData){         
                $count++;
                ?>
                <tr>
                  <td>{{ Form::text('bodartha[]', !empty($singleData) ? $singleData : NULL, ['class'=>'form-control', 'placeholder'=>'', 'id' => 'bodartha']) }}</td>                
                  @if($count == 2)                
                  <td class="add-btns" style="border: 0px;"><a href="" id="btnAddBodartha" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
                  @else
                  <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                  @endif
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-12">
            <div class="text-right btm-last">
              <p>{{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }}</p>
              <p> <b> 
                <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
                  @if(count($deginationsId) > 0)
                  @foreach($deginationsId as $deg)
                  <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                    {{ $deg->nameNep }} 
                  </option>
                  @endforeach
                  @endif
                </select>
              </b> </p>
            </div>
            <!--views for nibedak detail -->
            <div class="clearfix"></div>
            <hr>
            @include('admin.shifaris.nibedakCommonField')

            <!-- END -->
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>