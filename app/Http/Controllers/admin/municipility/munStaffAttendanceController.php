<?php

namespace App\Http\Controllers\admin\municipility;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\municipility\StaffMunicipility;
use Auth;

class munStaffAttendanceController extends Controller
{
    protected $viewPath = 'admin.municipility.attendance';

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $page['page_title']       = 'Attendance';
        $page['page_description'] = 'staff attendance';
        $staff = StaffMunicipility::select('firstNameEng', 'middleNameEng', 'lastNameEng', 'firstNameNep', 'middleNameNep', 'lastNameNep', 'departments_id')->get();
        return view($this->viewPath . '.index', compact(['page', 'staff']));
    }

    public function getStaffAttendance(Request $request){
      $tag = $request->tag;
      switch ($tag) {
          case 'single':
              
              break;

          case 'department':
              # code...
              break;

           case 'all':
              # code...
              break;
          
          default:
              # code...
              break;
      }
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
