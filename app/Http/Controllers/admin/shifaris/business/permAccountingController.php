<?php

namespace App\Http\Controllers\admin\shifaris\business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\PermAccounting;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\permAccountingVal;
use DB;
use Auth;


use App\model\mylogic;

class PermAccountingController extends Controller
{
    protected $viewPath = 'admin.shifaris.business.permAccounting';
    protected $shifarisTableName = 'perm_accountings';
    protected $appUsrTable = 'applicant_users';
    protected $deg = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixPermAccounting');

    }

    public function index()
    {
        $page['page_title']       = 'New Business PAN Number';
        $page['page_description'] = 'New Business PAN Number';

        
        $pan = PermAccounting::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));


    }

    public function create()
    {
        $page['page_title']       = 'New Business PAN Number : create';
        $page['page_description'] = 'New Business PAN Number : details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
            // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(permAccountingVal $request)
    {
        $bodarthaCount = count($request->bodarthaCopy);
        $bodartha = $request->bodarthaCopy;
        $bodarthaArray = [];
        for ($i=0; $i < $bodarthaCount; $i++) {
            array_push($bodarthaArray, $bodartha[$i]);
        }
        DB::beginTransaction();
        try {

            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

            $isShifarisExist = isset($request->shifarisId) ? PermAccounting::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? PermAccounting::findOrfail($isShifarisExist['id']) : new PermAccounting;

            $save->createdBy = Auth::user()->id;
            
            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";
            $save->issuedDate  = $request->issuedDate;
            $save->chalaniNum  = $request->chalaniNum;
            $save->officeName = $request->officeName;
            $save->officeAddress  = $request->officeAddress;
            $save->municipalityName  = $request->municipalityName;
            $save->wardNum = $request->wardNum;
            $save->organizationAddress = $request->organizationAddress;
            $save->organizationType = $request->organizationType;
            $save->organizationWard = $request->organizationWard;
            $save->title = $request->title;

            $save->bodarthaCopy = json_encode($bodarthaArray);
            $save->name = $request->name;
            $save->panDate = $request->panDate;
            $save->shopAddress = $request->shopAddress;
            $save->shopName = $request->shopName;
            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id = $request->deginations_id;
                // $bodarthaCopyJson = json_encode($request->bodarthaCopy);
                // $save->bodarthaCopy = $bodarthaCopyJson;

            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

                    //end for login
            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

                // $appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $save->id, $applicantName, $applicantAddress, $citizenshipNumber, $phoneNumber, $email, $dob, $fatherName, $profilePic, $citizenshipImgPath);
            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function changeStatus(){

    }
}
