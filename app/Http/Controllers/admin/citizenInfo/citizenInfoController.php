<?php
namespace App\Http\Controllers\admin\citizenInfo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\citizenInfo\citizenInfoVal;

use App\model\admin\citizen\CitizenInfo;
use DB;
use Auth;
use App\model\mylogic;
use App\model\admin\houseDetails\houseDetails;
use Validator;
use App\model\admin\citizen\CitizenRelation;
use App\model\setting\FamilyRelation;

use App\model\admin\houseDetails\abroad\AbroadDetails;
use App\model\admin\houseDetails\migration\migrationDetails;
use App\model\admin\houseDetails\deathDetails\fmDeathDetails;
use App\model\admin\houseDetails\houseFamilyFertility\HsFmFertility;

class citizenInfoController extends Controller
{
  protected $viewPath          = 'admin.citizenInfo';
  protected $table             = 'citizen_infos';
  protected $provisionTbl      = 'provinces';
  protected $qualificationTbl  = 'qualifications';
  protected $occupationTbl     = 'occupations';
  protected $citizenTypesTbl   = 'citizen_types';
  protected $religiousesTbl    = 'religiouses';
  protected $sysLangTbl        = 'sys_languages';
  protected $districtTbl       = 'districts';
  protected $nationalityTbl    = 'nationalities';
  protected $jatjatis          = 'jatjatis';

  protected $disabilityTbl     = 'disabilities';
  protected $maritailTbl       = 'maritial_statuses';

  protected $citizenship;
  protected $personalImage;
  protected $prefix;

  protected $tblAbroad = 'abroad_details';

  public function __construct()
  {
    $this->middleware('auth');
    $this->prefix        = config('activityMessage.prefixCitizenInfo');
    $this->citizenship   = config('activityMessage.citizenship');
    $this->personalImage = config('activityMessage.citizenImage');

  }

  public function index()
  {
    $page['page_title']       = 'Citizen Form';
    $page['page_description'] = 'Citizen Form';

    if (Auth::user()->userLevel == 'mun') {
      $citizenInfo = CitizenInfo::select('fnameNep', 'id', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'citizenNo', 'profilePic', 'phoneNumber', 'houseNumber', 'idEncrip')->with(['provinces', 'districts', 'nationalities', 'qualifications', 'occupations', 'citizenTypes', 'sys_languages', 'religiouses'])->where(['softDelete' => 0, 'isHouseOwner'=>1])->where('citizenNo', '<>', '')->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

      $withOutCitizen = CitizenInfo::select('fnameNep', 'id', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'citizenNo', 'profilePic', 'phoneNumber', 'houseNumber', 'idEncrip')->with(['provinces', 'districts', 'nationalities', 'qualifications', 'occupations', 'citizenTypes', 'sys_languages', 'religiouses'])->where(['softDelete' => 0])->whereNull('citizenNo')->orderby('created_at', 'DESC')->paginate(100);

      if (!empty($citizenInfo)) {
            foreach ($citizenInfo as &$fatherName) {  // with citizen
             $myFatherName = $citizenInfo->fatherName = $this->getParentId($tagName = 'father', $id = $fatherName->id);
             $fatherName->fatherNameNep = $myFatherName ? $myFatherName->fnameNep .' '. $myFatherName->mnameNep . ' '. $myFatherName->lnameNep: 'N/A';

             $fatherName->fatherNameEng = $myFatherName ? $myFatherName->fnameEng .' '. $myFatherName->mnameEng . ' '. $myFatherName->lnameEng: 'N/A';
                 // $citizenInfo->push(['myFather' => $myFatherName ? $myFatherName->fnameNep : 'N/A']);
           }
         }
         /*---------------------*/
         if (!empty($withOutCitizen)) {
          foreach ($withOutCitizen as &$father) {  // without citizen
           $father = $withOutCitizen->fatherName = $this->getParentId($tagName = 'father', $id = $fatherName->id);
           $fatherName->fatherNameNep = $father ? $father->fnameNep .' '. $father->mnameNep . ' '. $father->lnameNep: 'N/A';
           $fatherName->fatherNameEng = $father ? $father->fnameEng .' '. $father->mnameEng . ' '. $father->lnameEng: 'N/A';
               // $citizenInfo->push(['myFather' => $myFatherName ? $myFatherName->fnameNep : 'N/A']);
         }
       }
    }
    $citizenInfo = CitizenInfo::select('fnameNep', 'id', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'citizenNo', 'profilePic', 'phoneNumber', 'houseNumber', 'idEncrip')->with(['provinces', 'districts', 'nationalities', 'qualifications', 'occupations', 'citizenTypes', 'sys_languages', 'religiouses'])->where(['softDelete' => 0, 'wards_id'=>Auth::user()->wards_id, 'isHouseOwner'=>1])->where('citizenNo', '<>', '')->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

    $withOutCitizen = CitizenInfo::select('fnameNep', 'id', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'citizenNo', 'profilePic', 'phoneNumber', 'houseNumber', 'idEncrip')->with(['provinces', 'districts', 'nationalities', 'qualifications', 'occupations', 'citizenTypes', 'sys_languages', 'religiouses'])->where(['softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->whereNull('citizenNo')->orderby('created_at', 'DESC')->paginate(100);

    if (!empty($citizenInfo)) {
          foreach ($citizenInfo as &$fatherName) {  // with citizen
           $myFatherName = $citizenInfo->fatherName = $this->getParentId($tagName = 'father', $id = $fatherName->id);
           $fatherName->fatherNameNep = $myFatherName ? $myFatherName->fnameNep .' '. $myFatherName->mnameNep . ' '. $myFatherName->lnameNep: 'N/A';

           $fatherName->fatherNameEng = $myFatherName ? $myFatherName->fnameEng .' '. $myFatherName->mnameEng . ' '. $myFatherName->lnameEng: 'N/A';
               // $citizenInfo->push(['myFather' => $myFatherName ? $myFatherName->fnameNep : 'N/A']);
         }
       }
       /*---------------------*/
       if (!empty($withOutCitizen)) {
        foreach ($withOutCitizen as &$fatherName) {  // without citizen
         $father = $withOutCitizen->fatherName = $this->getParentId($tagName = 'father', $id = $fatherName->id);
         $fatherName->fatherNameNep = $father ? $father->fnameNep .' '. $father->mnameNep . ' '. $father->lnameNep: 'N/A';
         $fatherName->fatherNameEng = $father ? $father->fnameEng .' '. $father->mnameEng . ' '. $father->lnameEng: 'N/A';
             // $citizenInfo->push(['myFather' => $myFatherName ? $myFatherName->fnameNep : 'N/A']);
       }
     }
     return view($this->viewPath .'.index', compact(['page','citizenInfo', 'withOutCitizen']));
   }


   public function create($tag = null)
   {
    $page['page_title']       = 'Citizen Form';
    $page['page_description'] = 'Citizen Form';

    $provisonId         = mylogic::getDrowDownData($tablename = $this->provisionTbl, $orderColumn = 'created_at', $orderby = 'ASC');
    $qualificationId    = mylogic::getDrowDownData($tablename = $this->qualificationTbl, $orderColumn = 'created_at', $orderby = 'ASC');
    $occupationId = mylogic::getDrowDownData($tablename = $this->occupationTbl, $orderColumn = 'created_at', $orderby = 'ASC');
    $citizenTypesId = mylogic::getDrowDownData($tablename = $this->citizenTypesTbl, $orderColumn = 'created_at', $orderby = 'ASC');
    $religiousId = mylogic::getDrowDownData($tablename = $this->religiousesTbl, $orderColumn = 'created_at', $orderby = 'ASC');
    $syslangId = mylogic::getDrowDownData($tablename = $this->sysLangTbl, $orderColumn = 'created_at', $orderby = 'ASC');
    $districtId = mylogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'created_at', $orderby = 'ASC');

    $nationalityId = mylogic::getDrowDownData($tablename = $this->nationalityTbl, $orderColumn = 'created_at', $orderby = 'ASC');

    $jatJatiId = mylogic::getDrowDownData($tablename = $this->jatjatis, $orderColumn = 'created_at', $orderby = 'ASC');

    $familyLivingId = mylogic::getDrowDownData($tablename = 'living_fors', $orderColumn = 'created_at', $orderby = 'ASC');

    $wardsDetails = mylogic::getAllWardDetails();

    $disablitiyStatus = mylogic::getDrowDownData($tablename = $this->disabilityTbl, $orderColumn = 'created_at', $orderby = 'ASC');

    $maritialStatus = mylogic::getDrowDownData($tablename = $this->maritailTbl, $orderColumn = 'created_at', $orderby = 'ASC');

    return view($this->viewPath . '.create', compact(['page','provisonId','qualificationId','occupationId','citizenTypesId','religiousId','syslangId','districtId', 'nationalityId', 'jatJatiId', 'familyLivingId', 'wardsDetails', 'disablitiyStatus', 'maritialStatus']));
  }

  public function store(citizenInfoVal $request)
  {
    if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd' || Auth::user()->userLevel === 'mun') {
     DB::beginTransaction();
     $date = mylogic::getRandNumber();
     $date2 = mylogic::getRandNumber();
     try {
      $refCode = mylogic::getRandNumber();
      $myCitizen = $this->isExistsCitizen($request->citizenNo);

      if ($myCitizen['citizenNo']) {
       return back()->withInput()->withMessage(config('activityMessage.dataAlreadyExists'). 'Citizenship found');
     }

     $save  = new CitizenInfo;
     if ($request->hasFile('profilePic')) {
       $imgFile = $request->file('profilePic');
       $filename = str_replace(' ', '', $date). '.' . $imgFile->getClientOriginalExtension();

       $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->personalImage, $filename);
       $save->profilePic =  $this->personalImage . $filename;
     }
     $save->refCode   = $refCode;
     $save->createdBy  = Auth::user()->id;
     $save->municipilities_id  = getMunicipalityData()['id'];
     $save->wards_id           = Auth::user()->userLevel == 'mun' ? $request->wards_id : Auth::user()->wards_id;


     $save->citizenNo  = $request->citizenNo;
     $save->annualIncome  = $request->annualIncome ?? 'N/A';
     $save->nationalities_id  = $request->nationalities_id;
     $save->fnameNep  = $request->fnameNep;
     $save->mnameNep  = $request->mnameNep;
     $save->lnameNep  = $request->lnameNep;

     $save->printCount  = 0;
     $save->haveCitizen  = $request->haveCitizen =='Y' ? 1 : 0;
     $save->gender   = $request->gender;
     $save->fnameEng  = $request->fnameEng;
     $save->mnameEng  = $request->mnameEng;
     $save->lnameEng  = $request->lnameEng;


     $save->dobAD  = $request->dobAD;
     $save->dobBS  = $request->dobBS;

     $save->provinces_id  = $request->provinces_id;
     $save->districts_id  = $request->districts_id;

     $save->qualifications_id  = $request->qualifications_id;
     $save->occupations_id  = $request->occupations_id;

     $save->citizen_types_id  = $request->citizen_types_id;
     $save->religiouses_id  = $request->religiouses_id;
     $save->sys_languages_id  = $request->sys_languages_id;

     $save->jatjatis_id  = $request->jatjatis_id;

     $save->wardNo  = Auth::user()->wards_id;

     $save->villageNameEng  = $request->villageNameEng;
     $save->villageNameNep  = $request->villageNameNep;
     $save->phoneNumber  = $request->phoneNumber;
     $save->email  = $request->email;
     $save->citizenshipIssuePlace  = $request->citizenshipIssuePlace;
     $save->citizenshipIssueDate  = $request->citizenshipIssueDate;

     $save->birthplace  = $request->birthplace;
     $save->birthplace  = $request->birthplace;

     $save->son       = $request->son;
     $save->daughter  = $request->daughter;
     $save->other     = $request->other;
     $save->isHouseOwner = 1;
     $save->disabilities_id = $request->disabilities_id;
     $save->maritialStauts_id = $request->maritialStauts_id;

     if ($request->hasFile('citizenImgPath')) {
       $imgFile = $request->file('citizenImgPath');
       $filename = str_replace(' ', '', $date2). '.' . $imgFile->getClientOriginalExtension();

       $request->file('citizenImgPath')->move($this->citizenship,$filename);
       $save->citizenImgPath =  $this->citizenship . $filename;
     } 
     $mySave = $save->save();

     $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createCitizenInfo') . $save->id, $tableName = $this->table, $tblId = $save->id);

  //myEncriptId
     if (isset($save)) {
    $myEncriptId       = myLogic::idEncription($save->id);  // my encription Id
    $save->idEncrip  =  $myEncriptId;
    $save->gharmuli_id = $save->id;
    $save->save();
  }

  if ($mySave && $log) {
    DB::commit();
    return back()->withMessage(config('activityMessage.saveMessage'));
  }else{
   return back()->withMessage(config('activityMessage.unSaveMessage'));
 }
}catch (Exception $e) {
  DB::rollback();
  return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}else{
  return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
}
}
public function insertCitizenFromHouse(Request $request){   // inserting citizen 
  if (empty($request->gharmuli_id)) {
    return back()->withMessage('gharmuli needed');
  }
  DB::beginTransaction();
  try{
    $dobBs    = $request->dobBS;
    $member  = new CitizenInfo;
    $refCode = mylogic::getRandNumber();
    $member->createdBy  = Auth::user()->id;
    $member->municipilities_id  = getMunicipalityData()['id'];
    $member->wards_id           = Auth::user()->wards_id;
    $member->refCode            = $refCode;
    $member->gender             = $request->gender;
    $member->isdisabled       = $request->isdisabled;
    $member->fnameNep           = $request->fnameNep;
    $member->mnameNep           = $request->mnameNep;
    $member->lnameNep           = $request->lnameNep;

    $member->fnameEng           = $request->fnameEng;
    $member->mnameEng           = $request->mnameEng;
    $member->lnameEng           = $request->lnameEng;

    $member->dobBS              = $request->dobBS;
    $member->dobAD              = $request->dobAD;
    $member->jatjatis_id        = $request->jatjatis_id;
    $member->sys_languages_id   = $request->sys_languages_id;
    $member->religiouses_id     = $request->religiouses_id;
    $member->qualifications_id  = $request->qualifications_id;
    $member->occupations_id     = $request->occupations_id;
    $member->family_living_id   = $request->family_living_id ;
    $member->disabilities_id = $request->disabilities_id;
    $member->maritialStauts_id = $request->maritialStauts_id;

    $mySave = $member->save();

    if (isset($mySave)) {
     $myEncriptId       = myLogic::idEncription($member->id);  // my encription Id
     $member->idEncrip  =  $myEncriptId;
     $member->gharmuli_id = $request->gharmuli_id;
     $member->save();
   }
   
   $getRelationAndOrderColumn = $this->getRelationWithOrderColumn($request->family_relations_id);
     if ($mySave) {  // defining relationship to the member
      $this->defineRelation($request->citizen_infos_id, $getRelationAndOrderColumn['nameEng'], $member->id, $orderColumn=$getRelationAndOrderColumn['orderColumn']);
      
      if (!empty($request->grandFather_id)) {
        $this->defineRelation($member->id, 'grandFather', $request->grandFather_id, '1');
      }
      if (!empty($request->fathers_id)) {
       $this->defineRelation($member->id, 'father', $request->fathers_id, '2');
     }
     if (!empty($request->mothers_id)) {
       $this->defineRelation($member->id, 'mother', $request->mothers_id, '3');
     }

     if (!empty($request->wifeHusband_id)) {
       $this->defineRelation($member->id, 'wife/husband', $request->wifeHusband_id, '5');
     }
   }
    // dd( $member->id);
    $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createCitizenInfo'), $tableName = $this->table, $tblId = $member->id);  // inserting in log file

    if ($mySave && $log) {
      DB::commit();
        // return back()->withMessage(config('activityMessage.saveMessage'));
      return redirect()->back()->withMessage(config('activityMessage.saveMessage'))->with(['mainTab'=>'familyMember', 'helpTab' => 'houseMem']);
    }else{
     return back()->withMessage(config('activityMessage.unSaveMessage'));
   } 

 }catch (Exception $e) {
  DB::rollback();
  return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}

public function getRelationWithOrderColumn($Id){  // getting relation and order column
 return FamilyRelation::select('id', 'nameNep', 'orderColumn', 'nameEng')->where(['id'=>$Id])->first();
}

protected function isExistsCitizen($citizenNum){
  $fountCitizen = CitizenInfo::select('citizenNo', 'id')->where(['citizenNo' => $citizenNum])->first();
  return $fountCitizen;
}

protected function defineRelation($id, $relationName, $relationId, $orderColumn=null){
  //dd($id. ' ' .$relationId);
  $reqData = CitizenRelation::where(['citizens_id'=>$id, 'relationName' => $relationName])->first();

  if (!empty($reqData)) {
    $reqData->delete();
  }

  $relationId = DB::table('citizen_relations')->insert(
   array('citizens_id' => $id,
     'relationName'   => $relationName,
     'relation_id'   =>$relationId,
     'orderColumn'   => $orderColumn,
     "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
     "updated_at" => \Carbon\Carbon::now()
   )
 );
  if ($relationId) {
    return true;
  }
}

public function show($slug)
{
  $page['page_title']       = 'Show Citizen Details';
  $page['page_description'] = 'Citizen details information';

  if ($slug) {
    $citizen         = CitizenInfo::where(['status'=>0, 'softDelete'=>0, 'idEncrip'=>$slug])->first();
    $grandFatherData = $this->getParentId($tag = 'grandFather', $id = $citizen->id);
    $fatherData      = $this->getParentId($tag = 'father',      $id = $citizen->id);
    $motherData      = $this->getParentId($tag = 'mother',      $id = $citizen->id);
    $marriageData    = $this->getParentId($tag = 'wife/husband',    $id = $citizen->id);

    return view($this->viewPath . '.show', compact(['page', 'citizen', 'grandFatherData', 'fatherData', 'motherData', 'marriageData']));
  }else{
    return back()->withMessage('activityMessage.idNotFound');
  }
}

public function edit($slug)
{
 $page['page_title']       = 'Edit citizen Information';
 $page['page_description'] = 'Edit citizen information';

 $prefix             = mylogic::sysRefCode($tablename = $this->table, $prefix = $this->prefix);
 $provisonId         = mylogic::getDrowDownData($tablename = $this->provisionTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $qualificationId    = mylogic::getDrowDownData($tablename = $this->qualificationTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $occupationId       = mylogic::getDrowDownData($tablename = $this->occupationTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $citizenTypesId     = mylogic::getDrowDownData($tablename = $this->citizenTypesTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $religiousId        = mylogic::getDrowDownData($tablename = $this->religiousesTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $syslangId          = mylogic::getDrowDownData($tablename = $this->sysLangTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $districtId         = mylogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $nationalityId      = mylogic::getDrowDownData($tablename = $this->nationalityTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $jatJatiId          = mylogic::getDrowDownData($tablename = $this->jatjatis, $orderColumn = 'created_at', $orderby = 'ASC');

 $familyLivingId = mylogic::getDrowDownData($tablename = 'living_fors', $orderColumn = 'created_at', $orderby = 'ASC');

 $disablitiyStatus = mylogic::getDrowDownData($tablename = $this->disabilityTbl, $orderColumn = 'created_at', $orderby = 'ASC');

 $maritialStatus = mylogic::getDrowDownData($tablename = $this->maritailTbl, $orderColumn = 'created_at', $orderby = 'ASC');

    // $houseNumber        = $this->getHouseNumber();
 $data            = CitizenInfo::where(['status'=>0, 'softDelete'=>0, 'idEncrip'=>$slug])->first();
 // dd($data->provinces_id);
 $allCitizen      = mylogic::getMyFamilyDetails($data->gharmuli_id);

 $grandFatherId   = $this->getRelationId($tag = 'grandFather', $id = $data->id)['relation_id'];
 $fatherData      = $this->getRelationId($tag = 'father', $id = $data->id)['relation_id'];
 $motherData      = $this->getRelationId($tag = 'mother', $id = $data->id)['relation_id'];
 $marriageData    = $this->getRelationId($tag = 'wife/husband', $id = $data->id)['relation_id'];
 $wardsDetails    = mylogic::getAllWardDetails();


 return view($this->viewPath . '.edit', compact(['page','data', 'prefix','provisonId','qualificationId','occupationId','citizenTypesId','religiousId','syslangId','districtId', 'nationalityId', 'jatJatiId','fatherData', 'motherData', 'familyLivingIds', 'wardsDetails', 'disablitiyStatus', 'maritialStatus', 'allCitizen', 'grandFatherId', 'marriageData']));
}
/*parent id*/

protected function getParentId($tagName = null, $id = null){  //error 
  $getId = CitizenRelation::select('relation_id')->where(['citizens_id'=>$id, 'relationName'=>$tagName])->first(); 

  if ($getId) {
    $citizenDetails = CitizenInfo::findOrFail($getId->relation_id, ['fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'citizenshipIssuePlace', 'citizenshipIssueDate', 'citizenNo', 'son', 'daughter', 'other', 'gender']);
    return $citizenDetails;
  }
  return false;
}

protected function getRelationId($tagName = null, $id = null){
  return CitizenRelation::select('relation_id')->where(['citizens_id'=>$id, 'relationName'=>$tagName])->first(); 
}

public function printCard($slug){
  $page['page_title']       = 'Citizen Card';
  $page['page_description'] =  'Print citizen card id';
  if (!$slug) {
   return back()->withMessage(config('idNotFound'));
 }
 $printCitizen = CitizenInfo::where(['status'=>0, 'softDelete'=>0, 'idEncrip'=>$slug])->first();

 $grandFather = $this->getFatherMotherName($id = $printCitizen->id, $tagName = 'grandFather');
 $fatherName = $this->getFatherMotherName($id = $printCitizen->id, $tagName = 'father');
 $motherName = $this->getFatherMotherName($id = $printCitizen->id, $tagName = 'mother');
 return view($this->viewPath . '.printCitizenCart', compact(['page', 'printCitizen', 'fatherName', 'motherName']));
}

protected function getFatherMotherName($id, $tagName){
  $fmId = CitizenRelation::select('relation_id')->where(['citizens_id'=>$id, 'relationName'=>$tagName])->first();

  if ($fmId) {
    $citizenDetails = CitizenInfo::select('fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng')->where(['id'=>$fmId->relation_id])->first();
    // $citizenDetails = CitizenInfo::findOrFail($fmId->relation_id, ['fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng']);
    return $citizenDetails;
  }
}

public function update(Request $request, $id)
{
  $date = mylogic::getRandNumber();
  $date2 = mylogic::getRandNumber();
  $validator = Validator::make($request->all(), [
   'gender'                    => 'required',
   'fnameEng'                  => 'required|min:3|max:30',
   'lnameEng'                  => 'required|min:3|max:30',
   'dobBS'                     => 'required|date_format:Y-m-d',
   'provinces_id'             => 'required',
   'districts_id'              => 'required',
   'religiouses_id'            => 'required',
   'sys_languages_id'          => 'required',
   'jatjatis_id'               => 'required'
 ]);
  if ($validator->fails()) {
   return back()->withErrors($validator)->withInput();
 } 

 if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd' || Auth::user()->userLevel === 'mun') {
   DB::beginTransaction();
   try {
    $update  = CitizenInfo::findOrFail($id);
    if ($request->hasFile('profilePic')) {
     $imgFile = $request->file('profilePic');
     $filename = str_replace(' ', '', $date2). '.' . $imgFile->getClientOriginalExtension();

     $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->personalImage, $filename);

     $update->profilePic =  $this->personalImage . $filename;
   }

   $update->createdBy  = Auth::user()->id;
   $update->citizenNo  = $request->citizenNo;
   $update->gender   = $request->gender;
   $update->annualIncome  = $request->annualIncome ?? 'N/A';
   $update->nationalities_id  = $request->nationalities_id;
   $update->fnameNep  = $request->fnameNep;
   $update->mnameNep  = $request->mnameNep;
   $update->lnameNep  = $request->lnameNep;

   $update->fnameEng  = $request->fnameEng;
   $update->mnameEng  = $request->mnameEng;
   $update->lnameEng  = $request->lnameEng;
   $update->disabilities_id = $request->disabilities_id;
   $update->maritialStauts_id = $request->maritialStauts_id;


   $update->dobAD  = $request->dobAD;
   $update->dobBS  = $request->dobBS;

   $update->provinces_id  = $request->provinces_id;
   $update->districts_id  = $request->districts_id;

   $update->qualifications_id  = $request->qualifications_id;
   $update->occupations_id  = $request->occupations_id;

   $update->citizen_types_id  = $request->citizen_types_id;
   $update->religiouses_id  = $request->religiouses_id;
   $update->sys_languages_id  = $request->sys_languages_id;

   $update->jatjatis_id  = $request->jatjatis_id;

   $update->villageNameEng  = $request->villageNameEng;
   $update->villageNameNep  = $request->villageNameNep;
   $update->phoneNumber  = $request->phoneNumber;
   $update->email  = $request->email;
   $update->citizenshipIssuePlace  = $request->citizenshipIssuePlace;
   $update->citizenshipIssueDate  = $request->citizenshipIssueDate;

   $update->birthplace  = $request->birthplace;
   $update->birthplace  = $request->birthplace;

   if ($request->hasFile('citizenImgPath')) {
     $imgFile = $request->file('citizenImgPath');
     $filename = str_replace(' ', '', $date). '.' . $imgFile->getClientOriginalExtension();

     $request->file('citizenImgPath')->move($this->citizenship,$filename);
     // $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->citizenship, $filename);

     $update->citizenImgPath =  $this->citizenship . $filename;
   }
   $myupdate = $update->update();

   /*--------------------------------*/
   if (!empty($request->grandFather_id)) {
     $this->defineRelation($update->id, 'grandFather', $request->grandFather_id, '1');
   }
   if (!empty($request->fathers_id)) {
    $this->defineRelation($update->id, 'father', $request->fathers_id, '2');
  }
  if (!empty($request->mothers_id)) {
    $this->defineRelation($update->id, 'mother', $request->mothers_id, '3');
  }

  if (!empty($request->wifeHusband_id)) {
    $this->defineRelation($update->id, 'wife/husband', $request->wifeHusband_id, '5');
  }
  /*------------------------------*/

  $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updatedCitizenInfo') . $update->refCode, $tableName = $this->table, $tblId = $update->id);

  if ($myupdate && $log) {
    DB::commit();
    return back()->withMessage(config('activityMessage.updatedCitizenInfo') .$request->refCode);
  }else{
   return back()->withMessage(config('activityMessage.unSaveMessage'));
 }
}catch (Exception $e) {
  DB::rollback();
  return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}else{
  return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
}
}

public function destroy($slug)
{
 DB::beginTransaction();
 try {
  $citizenId = CitizenInfo::where(['idEncrip'=>$slug])->first();
  $delId = CitizenInfo::findOrFail($citizenId->id);
  $myDel = $delId->delete();
  $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.deleteMessage'), $tableName = $this->table, $tblId = $citizenId->id);
  if ($myDel && $log) {
    DB::commit();
    return response()->json([
        'success' => true,
        'message' =>config('activityMessage.deleteMessage')
      ]);
  }else{
    return response()->json([
        'success' => true,
        'message' =>config('activityMessage.idNotFound')
      ]);
  }
}catch (Exception $e) {
  DB::rollback();
  return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}

public function getParentData(){  //house hold details
  $data = CitizenInfo::select('fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'id', 'refCode', 'citizenNo')->whereNotNull('citizenNo')->orderBy('citizenNo', 'ASC')->get();
  return $data ? $data : '';
}

public function getRelationData(Request $request){  // for ajax resquest
  $citizenNum        = $request->citizenNo;  
  $citizen = CitizenInfo::select('fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameNep', 'citizenshipIssuePlace', 'citizenshipIssueDate')->where(['status'=>0, 'softDelete'=>0, 'citizenNo'=>$citizenNum])->first();
  if($citizen){
    return response()->json(
      [
       'success' => true,
       'fnameNep' => $citizen['fnameNep'],
       'mnameNep' => $citizen['mnameNep'],
       'lnameNep' => $citizen['lnameNep'],

       'fnameEng' => $citizen['fnameEng'],
       'mnameEng' => $citizen['mnameEng'],
       'lnameEng' => $citizen['lnameEng'],

       'citizenshipIssuePlace' => $citizen['citizenshipIssuePlace'],
       'citizenshipIssueDate' => $citizen['citizenshipIssueDate'],
     ]
   );
  }
}

// abroad gone memeber details
public function insertAbroadGoneMember(Request $request){
  $abroadGoneMem = $request->hsAbroad;
  $abroadMember = [];
  foreach ($abroadGoneMem as $abMem) {
     // if (isset($abMem['citizenId'])) {   // updating citizen table is death
     //  $countryName[] =  $abMem['country'];
     //    $updateAbroadGone = CitizenInfo::findOrfail($abMem['citizenId']);
     //    $updateAbroadGone->abroadGone  = isset($abMem['country']) ? json_encode($countryName) : '';
     //    $updateAbroadGone->update();
     // }
   if (isset($abMem['haveGone']) && $abMem['haveGone'] == 1) {
     $abroadMember[] = $abMem;
   }
 }
 $abroadMemJson = json_encode($abroadMember);
  // dd($abroadMember);

 if ($request->myHouseId) {
   $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
   $isExistsHsNum = AbroadDetails::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
   if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
    DB::beginTransaction(); 
    try {
             $abMem  = !$isExistsHsNum['house_details_id'] ? new AbroadDetails : AbroadDetails::findOrfail($isExistsHsNum['id']);   // inserting abroad gone member

             $abMem->createdBy         = Auth::user()->id;
             $abMem->municipilities_id = $ishouseExists['municipilities_id'];
             $abMem->wards_id          = $ishouseExists['wards_id'];
             $abMem->house_details_id  = $ishouseExists['id'];
             $abMem->hsAbroadDetails   = $abroadMemJson;
             $abMem->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID
             $abMem->status            = 0;
             $abMem->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $abMem->save() : $abMem->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = $this->tblAbroad, $tblId = $abMem->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                 'success' => true,
                 'message' => config('activityMessage.saveMessage')
               ]);
             }else{
               return response()->json([
                 'success' => false,
                 'message' => config('activityMessage.unSaveMessage')
               ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
             }
           } catch (Exception $e) {
             DB::rollback();
             return back()->withMessage(config('activityMessage.dataNotInserted'));
           }
         }else{ 
           return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{ 
         return back()->withMessage('House number is missing please try to insert it from browser');
       }
     }

     public function migrationDetails(Request $request){
      if ($request->myHouseId) {
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = migrationDetails::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        DB::beginTransaction(); 
        try {
           $migDetails  = !$isExistsHsNum['house_details_id'] ? new migrationDetails : migrationDetails::findOrfail($isExistsHsNum['id']);   // inserting abroad gone member

           $migDetails->createdBy         = Auth::user()->id;
           $migDetails->municipilities_id = $ishouseExists['municipilities_id'];
           $migDetails->wards_id          = $ishouseExists['wards_id'];
           $migDetails->house_details_id  = $ishouseExists['id'];
           $migDetails->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID
           $migDetails->haveMigration  = $request->haveMigration;
           $migDetails->migrationDate  = $request->migrationDate;
           $migDetails->migrationPlace  = $request->migrationPlace;
           $migDetails->migrationReason  = $request->migrationReason;

           $migDetails->status            = 0;
           $migDetails->softDelete        = 0;
           $mySave = !$isExistsHsNum['house_details_id'] ? $migDetails->save() : $migDetails->update();

           $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'migration_details', $tblId = $migDetails->id);
           if ($mySave && $log) {
             DB::commit();
             return response()->json([
               'success' => true,
               'message' => config('activityMessage.saveMessage')
             ]);
             // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'familyMember', 'helpTab' => 'migration']);
           }else{
            return response()->json([
             'success' => false,
             'message' => config('activityMessage.unSaveMessage')
           ]);
            // return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
        } catch (Exception $e) {
         DB::rollback();
         return back()->withMessage(config('activityMessage.dataNotInserted'));
       }
     }else{ 
       return back()->withMessage(config('activityMessage.idNotFound'));
     }
   }else{ 
     return back()->withMessage('House number is missing please try to insert it from browser');
   }
 }

public function insertHsFertility(Request $request){  // details of death member details
 DB::beginTransaction(); 

 if ($request->myHouseId && $request->fm_member_id) {
  $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

  $hsFertilityInfo = json_encode($request->hsFertilityInfo);


  $isExistsHsNum = HsFmFertility::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
  if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
   try {
              $fertilityDetails  = !$isExistsHsNum['house_details_id'] ? new HsFmFertility : HsFmFertility::findOrfail($isExistsHsNum['id']);   // inserting abroad gone member

              $fertilityDetails->createdBy         = Auth::user()->id;
              $fertilityDetails->municipilities_id = $ishouseExists['municipilities_id'];
              $fertilityDetails->wards_id          = $ishouseExists['wards_id'];
              $fertilityDetails->house_details_id  = $ishouseExists['id'];

              $fertilityDetails->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

              $fertilityDetails->haveGivenBirth = $request->haveGivenBirth;
              $fertilityDetails->hsFertilityInfo = $hsFertilityInfo;

              $fertilityDetails->status            = 0;
              $fertilityDetails->softDelete        = 0;
              $mySave = !$isExistsHsNum['house_details_id'] ? $fertilityDetails->save() : $fertilityDetails->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_fm_fertilities', $tblId = $fertilityDetails->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                ]);
              }else{
                return response()->json([
                 'success' => false,
                 'message' => config('activityMessage.unSaveMessage')
               ]);
               // return back()->withMessage(config('activityMessage.unSaveMessage'));
              }
            } catch (Exception $e) {
              DB::rollback();
              return back()->withMessage(config('activityMessage.dataNotInserted'));
            }
          }else{ 
            return back()->withMessage(config('activityMessage.idNotFound'));
          }
        }else{ 
          return back()->withMessage('House number is missing please try to insert it from browser');
        }
      }
public function insertDeathMember(Request $request){  // details of death member details
   $deathMemDetails = $request->deathMem; // cacheing array data
   $deathMemberArray = []; 
   DB::beginTransaction(); 
   foreach ($deathMemDetails as $dm) {
       if (isset($dm['citizenId'])) {   // updating citizen table is death
        $updateCitizenDeath = CitizenInfo::findOrfail($dm['citizenId']);
        $updateCitizenDeath->isDeath = isset($dm['isDeath']) ? 1 : 0;
        $updateCitizenDeath->update();
      }
      if (isset($dm['isDeath']) && $dm['isDeath'] == 1) {
        $deathMemberArray[] = $dm;
      }
    }
    $deathMemJson = json_encode($deathMemberArray);
    if ($request->myHouseId && $request->fm_member_id) {
      $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

      $isExistsHsNum = fmDeathDetails::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
      if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
       try {
              $deathMemDetails  = !$isExistsHsNum['house_details_id'] ? new fmDeathDetails : fmDeathDetails::findOrfail($isExistsHsNum['id']);   // insert only who is death

              $deathMemDetails->createdBy         = Auth::user()->id;
              $deathMemDetails->municipilities_id = $ishouseExists['municipilities_id'];
              $deathMemDetails->wards_id          = $ishouseExists['wards_id'];
              $deathMemDetails->house_details_id  = $ishouseExists['id'];

              $deathMemDetails->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

              $deathMemDetails->hsDeathReport     = $deathMemJson;
              $deathMemDetails->status            = 0;
              $deathMemDetails->softDelete        = 0;
              $mySave = !$isExistsHsNum['house_details_id'] ? $deathMemDetails->save() : $deathMemDetails->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'fm_death_details', $tblId = $deathMemDetails->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                ]);
              }else{
                return response()->json([
                 'success' => false,
                 'message' => config('activityMessage.unSaveMessage')
               ]);
              }
            } catch (Exception $e) {
              DB::rollback();
              return back()->withMessage(config('activityMessage.dataNotInserted'));
            }
          }else{ 
            return back()->withMessage(config('activityMessage.idNotFound'));
          }
        }else{ 
          return back()->withMessage('House number is missing please try to insert it from browser');
        }
      }
}