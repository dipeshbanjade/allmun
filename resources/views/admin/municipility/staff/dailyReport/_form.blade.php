{{-- Create Form for daily report --}}
<div class="col-sm-12">
    <div class="card-box">
        <div class="card-head">
            <header>@lang('commonField.extra.createDailyReport')</header>
        </div>
        <div class="card-body row">
            <div class="col-lg-6 p-t-20"> 
                <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                    <input class = "mdl-textfield__input" type = "text" id = "">
                    <label class = "mdl-textfield__label">@lang('commonField.extra.subject')</label>
                </div>
            </div>
            <div class="col-lg-6 p-t-20"> 
                <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                    <input class = "mdl-textfield__input" type = "text" id = "nepDate">
                    <label class = "mdl-textfield__label" >@lang('commonField.extra.date')</label>
                </div>
            </div>
            <div class="col-lg-6 p-t-20"> 
                <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                    <input class = "mdl-textfield__input time_input" type = "time" id = "">
                    <label class = "mdl-textfield__label" >@lang('commonField.extra.startTime')</label>
                </div>
            </div>
            <div class="col-lg-6 p-t-20"> 
                <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                    <input class = "mdl-textfield__input time_input" type = "time" id = "">
                    <label class = "mdl-textfield__label" >@lang('commonField.extra.endTime')</label>
                </div>
            </div>
            <div class="col-lg-12 p-t-20"> 
                <div class="pull-right">
                    <a class="changeLanguage dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  data-url="{{ route('changeLanguage', 'en') }}" data-val="en">
                        <img src="{{ asset('lib/img/gb.png') }}" class="position-left" alt="english logo" title="English">
                    </a>
                    <a class="changeLanguage dropdown-toggle flag" data-toggle="dropdown" aria-expanded="false" data-url="{{ route('changeLanguage', 'np') }}" data-val="np">
                        <img src="{{ asset('lib/img/flag.png') }}" class="position-left" alt="Nepal logo" title="Nepali">
                    </a>
                </div>
                <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                    <textarea class = "mdl-textfield__input" rows =  "4" 
                    id = "text7" ></textarea>
                    <label class = "mdl-textfield__label" for = "text7">@lang('commonField.extra.desc')</label>
                </div>
            </div>
        </div>
    </div>
</div> 
{{-- end of create form for daily report --}}