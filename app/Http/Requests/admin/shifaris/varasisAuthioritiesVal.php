<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class varasisAuthioritiesVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'successorAge' => 'required|numeric',
            'successorName' => 'required',
            'successorMunicipality' => 'required',
            'successorWard' => 'required|numeric',
            'casePerson' => 'required',
            'caseName' => 'required',
            'caseMunicipality' => 'required',
            'casereason' => 'required',
            'caseRunMunicipality' => 'required',
            'agreedOn' => 'required',
            'manisMunicipality' => 'required',
            'manisWard' => 'required|numeric',
            'manisAge' => 'required|numeric',
            'manisName' => 'required',
            'caseOnMunicipality' => 'required',
            'rohabar' => 'required',
            'transfer' => 'required',
            'year' => 'required|numeric',
            'month' => 'required|numeric',
            'day' => 'required|numeric',
            'eachday' => 'required|numeric',
            'citizenshipNumber' =>'required'
        ];
    }
}
