<?php

namespace App\Http\Requests\admin\setting\acl;

use Illuminate\Foundation\Http\FormRequest;

class userDetailsVal extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            // 'refCode'   => 'required|min:3|max:16|unique:users',
            'name'     => 'required|min:3|max:30',
            'email'   => 'required|email|unique:users',
            'roles_id'   => 'required',
            'departments_id'   => 'required',
            'staff_types_id'   => 'required',
            'departments_id'   => 'required',
            'deginations_id'   => 'required',
            'provinces_id'   => 'required',
            'districts_id'   => 'required',
            'password'      => 'required|min:4|max:16'
        ];
    }
}
