<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $table = 'qualifications';
    protected $fillable = ['refCode','nameNep','nameEng','softDelete','status'];
}
