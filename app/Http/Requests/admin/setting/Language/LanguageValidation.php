<?php

namespace App\Http\Requests\admin\setting\Language;

use Illuminate\Foundation\Http\FormRequest;

class LanguageValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:30|unique:sys_languages',
            'langEng' => 'required|min:3|max:30'
        ];
    }
}
