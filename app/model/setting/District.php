<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';
    protected $fillable = ['districtNameNep', 'districtNameEng', 'status', 'softDelete'];
}
