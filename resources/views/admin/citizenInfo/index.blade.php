@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-md-12">
    <div class="card-box">
        <div class="panel tab-border">
               <header class="panel-heading  custom-tab tab-head">
                   <ul class="nav nav-tabs">
                       <li class="nav-item"><a href="#withCtznTab" data-toggle="tab" class="active">@lang('commonField.personal_information.houseOwner')</a>
                       </li>
                       <li class="nav-item"><a href="#withoutCtznTab" data-toggle="tab">Without Citizen</a>
                       </li>
                   </ul>
               </header>
               <div class="panel-body">
                   <div class="tab-content">
                       <div class="tab-pane active" id="withCtznTab">
                           <div class="col-sm-12 col-md-12">
                          <div class="card-box">
                            <div class="card-body ">
                              <a href="{{ route('citizenInfo.create') }}" id="btnAddDepartment" class="btn btn-primary pull-right"><i class="fa fa-plus"></i><span> @lang('commonField.extra.createCitizen')</span></a>
                              <div class="table-scrollable" style="color:black;">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
                                  <thead>
                                    <tr>
                                      <th width="5%">@lang('commonField.extra.sn')</th>
                                      <th width="5%">@lang('commonField.extra.house')</th>
                                      <th width="7%">@lang('commonField.personal_information.image')</th>
                                      <th width="10%">@lang('commonField.extra.citizenNo')</th>
                                      <th>@lang('commonField.personal_information.fullName')</th>
                                      <th>@lang('commonField.extra.fatherName')</th>
                                      <th>@lang('commonField.personal_information.phoneNumber')</th>
                                      <th width="25%"> @lang('commonField.extra.action') </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?php 
                                          $count = 1;
                                          $lan = getLan();
                                      ?>
                                      @if(count($citizenInfo) > 0)
                                                @foreach($citizenInfo as $citizen)
                                                    <tr class="odd gradeX">
                                                      <td>
                                                          {{ $count ++ }}
                                                        </td>
                                                        <td title="create House Details">
                                                            {{ $citizen->houseNumber }}
                                                        </td>
                                                      <td><img src="{{ $citizen->profilePic ? asset($citizen->profilePic) : asset('image/defaultUser') }}" width="40" height="40"></td>
                                                        <!--  -->
                                                      <td>
                                                            {{ $citizen->citizenNo }}
                                                        </td>
                                                      <td>
                                                      <a href="{{ isset($citizen->idEncrip) ? route('citizenInfo.show', $citizen->idEncrip) : '#' }}">
                                                      {{ $lan =='np' ?  $citizen->fnameNep .' '. $citizen->mnameNep .' '. $citizen->lnameNep : $citizen->fnameEng .' '. $citizen->mnameEng  .' '. $citizen->lnameEng }}</a></td>
                                                        <!--  -->
                                                      <td>
                                                        {{ $lan =='np' ? $citizen->fatherNameNep : $citizen->fatherNameEng }}
                                                      </td>

                                                      <td>{{ $citizen->phoneNumber }}</td>

                                                      <td>
                                                            <a href="{{ isset($citizen->idEncrip) ? route('citizenPrintCard', $citizen->idEncrip) : '#' }}">
                                                              <i class="fa fa-print btn btn-info btn-xs"></i>
                                                            </a>

                                                            <a href="{{ isset($citizen->idEncrip) ? route('citizenInfo.edit', $citizen->idEncrip) : '#' }}">
                                                              <i class="fa fa-edit btn btn-info btn-xs"></i>
                                                            </a>
                                                          
                                                            <a href="#" class="deleteMe" id="" data-url="{{ route('citizenInfo.delete', $citizen->id )}}" data-name="{{ $citizen->fnameEng}}"><button class="btn btn-danger btn-xs">
                                                              <i class="fa fa-trash-o "></i></button></a>
                                                          </td>
                                                        </tr>
                                                      
                                                @endforeach
                                      @endif
                                      </tbody>
                                    </table>
                                  </div>
                                            {{ $citizenInfo->render() }}
                                </div>
                              </div>
                  </div>
                       </div>
                       <div class="tab-pane" id="withoutCtznTab">
                           <div class="col-sm-12 col-md-12">
                          <div class="card-box">
                            <div class="card-body ">
                              <a href="{{ route('citizenInfo.create') }}" id="btnAddDepartment" class="btn btn-primary pull-right"><i class="fa fa-plus"></i><span> @lang('commonField.extra.createCitizen')</span></a>
                              <div class="table-scrollable" style="color:black;">
                                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="withOutcitizen">
                                  <thead>
                                    <tr>
                                      <th width="5%">@lang('commonField.extra.sn')</th>
                                      <th width="5%">@lang('commonField.extra.house')</th>
                                      <th width="7%">@lang('commonField.personal_information.image')</th>
                                      <th>@lang('commonField.personal_information.fullName')</th>
                                      <th>@lang('commonField.extra.fatherName')</th>
                                      <th>@lang('commonField.personal_information.phoneNumber')</th>
                                      <th width="25%"> @lang('commonField.extra.action') </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?php 
                                          $count = 1;
                                          $lan = getLan();
                                      ?>
                                      @if(isset($withOutCitizen))
                                                @foreach($withOutCitizen as $noCitizen)
                                                    <tr class="odd gradeX">
                                                      <td>
                                                          {{ $count ++ }}
                                                        </td>
                                                        <td title="create House Details">
                                                            {{ $noCitizen->houseNumber ?? '' }}
                                                        </td>
                                                      <td><img src="{{ asset($noCitizen->profilePic) ?? asset('image/defaultUser.png') }}" width="40" height="40"></td>
                                                      <td>
                                                      <a href="{{ isset($noCitizen->idEncrip) ? route('citizenInfo.show', $noCitizen->idEncrip) : '#' }}">
                                                      {{ $lan =='np' ?  $noCitizen->fnameNep .' '. $noCitizen->mnameNep .' '. $noCitizen->lnameNep : $noCitizen->fnameEng .' '. $noCitizen->mnameEng  .' '. $noCitizen->lnameEng }}</a></td>
                                                        <!--  -->
                                                      <td>
                                                        {{ $lan =='np' ? $noCitizen->fatherNameNep : $noCitizen->fatherNameEng }}
                                                      </td>

                                                      <td>{{ $noCitizen->phoneNumber }}</td>

                                                      <td>
                                                            <a href="{{ isset($noCitizen->idEncrip) ? route('citizenPrintCard', $noCitizen->idEncrip) : '#' }}">
                                                              <i class="fa fa-print btn btn-info btn-xs"></i>
                                                            </a>
                                                          
                                                            <a href="#" class="deleteMe" id="" data-url="{{ route('citizenInfo.delete', $noCitizen->id )}}" data-name="{{ $noCitizen->fnameEng}}"><button class="btn btn-danger btn-xs">
                                                              <i class="fa fa-trash-o "></i></button></a>
                                                          </td>
                                                        </tr>
                                                      
                                                @endforeach
                                      @endif
                                      </tbody>
                                    </table>
                                            {{ $withOutCitizen->links() }}
                                  </div>
                                </div>
                              </div>
                  </div>
                       </div>
                   </div>
               </div>
           </div>
    </div>
</div>
@endsection