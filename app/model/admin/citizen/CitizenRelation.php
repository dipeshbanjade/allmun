<?php

namespace App\model\admin\citizen;

use Illuminate\Database\Eloquent\Model;

class CitizenRelation extends Model
{
    protected $table = 'citizen_relations';

    public function relations(){
    	return $this->belongsTo('App\model\admin\citizen\CitizenInfo'::class);
    }
}
