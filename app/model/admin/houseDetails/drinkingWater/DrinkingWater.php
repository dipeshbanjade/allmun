<?php

namespace App\model\admin\houseDetails\drinkingWater;

use Illuminate\Database\Eloquent\Model;

class DrinkingWater extends Model
{
    protected $table = 'house_drinking_waters';
}
