<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
      <div class="form-group col-md-12">
        <p>
            १७.  निम्न कुन कुन सुविधाहरु तपाईको परिवारले उपभोग गरेको छ ?
        </p>
        <br>
        <table border="1">
            <thead>
                <tr>
                    <th>@lang('commonField.extra.sn')</th>
                    <th width="50%">सुविधाहरु	</th>
                    <th>उपभोग गरेको (१. छ, २. छैन)</th>
                    <th>number</th>
                </tr>
            </thead>
            <tbody>
                <?php
                     $hsDevices = isset($hsEdData->hsDevices) ? json_decode($hsEdData->hsDevices) : '';
                     // dd($hsDevices);
                ?>
                
                <?php $count = 1;    ?>
                @if(isset($electricalDevice) && $electricalDevice)
                    @foreach($electricalDevice as $device)
                       <?php //dd($device);   ?>
                           <tr>   
                           <td>{{ $count ++ }}</td>                                   
                           <td>{{ $device->nameNep }}</td>
                               <input type="hidden" name="deviceName[<?php echo $count;  ?>][deviceName]" value="{{ $device->nameNep }}">
                                
                               <?php 
                                  $number = updateDevices($device->nameNep, $hsDevices);
                                  //dd($number);
                                ?>
                               <td class="text-center">
                                   <label class="radio-inline">
                                       <input type="radio"  class="yesFacilities" onchange="requireFacility(this)" name="deviceName[<?php echo $count;  ?>][haveDevice]" value="1" <?php echo $number != null ? 'checked' : ''   ?>>
                                       छ 
                                   </label> 
                                   <label class="radio-inline " " >
                                      <input type="radio" class="noFacility" onchange="requireFacility(this)" name="deviceName[<?php echo $count;  ?>][haveDevice]" value="0">
                                       छैन्
                                   </label> 
                               </td>

                               <td>
                                   {{ Form::text("deviceName[$count][number]", $number != null ? $number : '', ['class'=>'form-control qtyFacilitiy', 'placeholder'=>'number of devices']) }}
                               </td>
                           </tr>
                    @endforeach
                @endif
                
            </tbody>
        </table>
        <br>
      </div> 
    </div>
  </div>
</div>
<div class="msgDisplay"></div>
<?php
     function updateDevices($deviceName, $hsDevices){
        //$hsDevices = isset($hsEdData->hsDevices) ? json_decode($hsEdData->hsDevices) : '';
        // global $hsDevices;
        // dd($device['deviceName']);
        $hsDevices =  (array) $hsDevices;
        if(isset($hsDevices[0]) and $hsDevices[0] == ""){}
        else{
          // dd($hsDevices);
          $found = false;
          foreach ($hsDevices as $device) {
            $device =  (array) $device;
            // dd($device['\'number\'']);
            //dd($device['number']);
             if ($deviceName == $device['deviceName']) {
               $found = true;
               return $device['number'];
             }
          }
        }
     }
?>