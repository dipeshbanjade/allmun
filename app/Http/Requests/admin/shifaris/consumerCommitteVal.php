<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class consumerCommitteVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:consumer_committes',
            'issuedDate' => 'required',
            'chalaniNum' => 'required|unique:consumer_committes',
            'orgName' => 'required',
            'orgLocation' => 'required',
            'fiscalYear' => 'required',
            'municipalityName' => 'required',
            'wardNum' => 'required',
            'organizationAddress' => 'required',
            'organizationType' => 'required',
            'organizationWard' => 'required',
            'businessName' => 'required',
            'projectAmount' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
