<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class unmarriedVerifyNepVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:unmarried_verify_neps',
            'issuedDate' => 'required', 
            'chalaniNumber' => 'required|numeric|unique:unmarried_verify_neps',
            'municipalityName' => 'required', 
            'wardNumber' => 'required', 
            'sabikAddress' => 'required', 
            'fatherName' => 'required', 
            'motherName' => 'required', 
            'unmarriedPerson' => 'required', 
            'authorizedPerson' => 'required'
        ];
    }
}
