<?php

namespace App\Http\Controllers\admin\setting\acl\rolePermission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\acl\AssignPermissionRole;
use App\model\myLogic;
use App\model\admin\acl\Permission;
use App\model\admin\acl\Role;

class assignPermissionRoleController extends Controller
{
   protected $viewPath = 'admin.acl.assignRolePermission';
   protected $roleTable       = 'roles';
   protected $permissionTable = 'permissions';

   public function __construct()
   {
       $this->middleware('auth');
   }


    public function index()
    {
        $page['page_title']        = 'List AssignRole Permission';
        $page['page_description'] =  'List AssignRole Permission';

        $myRole = Role::with('permissions')->get();

        return view($this->viewPath . '.index', compact(['page', 'myRole']));
    }
    public function create()
    {
        $page['page_title']        = 'Create  AssignRole Permission';
        $page['page_description']   =  'Create AssignRole Permission';

        $permissionData = Permission::orderby('pername')->where(['softDelete' => 0])->get();

        $permissionData = $permissionData->groupBy(function ($per) {
            return $per->moduleName;
        })->all();

        $roleData  = mylogic::getDrowDownData($tablename = $this->roleTable, $orderColumn = 'roleName', $orderby = 'ASC');

        return view($this->viewPath . '.create', compact(['page', 'roleData', 'permissionData']));
    }

    public function store(Request $request)
    {
        $roleId  = $request->roles_id;
        $permissionId = $request->permission_id;

        foreach ($permissionId as $permission) {
            $save = new AssignPermissionRole;
            $save->role_id = $request->roles_id;
            $save->permission_id = $permission;

            $mySave = $save->save();
        }
        if ($mySave) {
            return back()->withMessage('successfully save record');
        }else{
            return back()->withMessage('Oops try it again some things goes wrong');
        }
    }

    public function edit($id)
    {
        $page['page_title']        = 'Edit role and permission';
        $page['page_description']   =  'edit role and permission for system';
         if ($id) {
           $edit['id'] = $id;
           $edit['name'] = myLogic::getRoleBy($id)['roleName'];

           $permissionData = Permission::orderby('pername')->where(['softDelete' => 0])->get();

           $permissionData = $permissionData->groupBy(function ($per) {
               return $per->moduleName;
           })->all();   // extra permission of
           
           $datas = AssignPermissionRole::where('role_id',$id)->get(); // extract all permission related of role
           return view($this->viewPath . '.edit', compact(['page', 'permissionData', 'datas', 'edit']));

           }else{
            return back()->withMessage(config('activityMessage.idNotFound'));
           }
    }

    public function update(Request $request, $id)
    {
        if ($id) {
            $role = Role::findOrFail($id);
            $role->permissions()->detach();
            foreach ($request->permissions_id as $per) {
               $role->permissions()->attach($per);
            }
            return redirect()->route('system.assignRolePermission.index')->withMessage('successfully udpated....');
        }else{
            return redirect()->route('home')->withMessage('Oops Id not found');
        }
    }
}
