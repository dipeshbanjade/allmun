<?php

namespace App\Http\Requests\admin\setting\chuloType;

use Illuminate\Foundation\Http\FormRequest;

class chuloTypeValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:chulo_types',
            'nameEng' => 'required|min:4|max:50'
        ];
    }
}
