<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row color_red">
     <!-- 16 -->
     <div class="form-group col-md-12">
      <p>
       १६. परिवारको वार्षिक आम्दानी र खर्च (वितेको १२ महिनामा आधारित) सम्बन्धी विवरण
     </p>
     <div class="col-md-12">
      <?php $count = 0;                                 
      $incomeArr = isset($incomeAndExpensesData->incomeDetails) ? json_decode($incomeAndExpensesData->incomeDetails, true) : '';

      $expensesArr = isset($incomeAndExpensesData->expensesDetails) ? json_decode($incomeAndExpensesData->expensesDetails, true) : '';
               // dd($incomeArr);
      ?> 
      <table class="table">
        <tr>
          <th>आम्दानी विवरण</th>
          <th>रकम रु. (हजारमा)</th>
          <th>खर्च विवरण</th>
          <th>रकम रु. (हजारमा)</th>
        </tr>
        <tr>
          <td>
           कृषि तथा पशुपालनबाट          
         </td>
         <td>
          {{ Form::number('incomeDetails[कृषि तथा पशुपालनबाट][amount]', isset($incomeArr['कृषि तथा पशुपालनबाट']['amount']) ? $incomeArr['कृषि तथा पशुपालनबाट']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field income-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumIncome()']) }}
        </td>
        <td>
          खाद्यान्न तथा खाना खर्च
        </td>
        <td>
          {{ Form::number('expensesDetails[खाद्यान्न तथा खाना खर्च][amount]', isset($expensesArr['खाद्यान्न तथा खाना खर्च']['amount']) ? $expensesArr['खाद्यान्न तथा खाना खर्च']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field expense-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumExpense()']) }}
        </td>
      </tr>
      <tr>
        <td>
         व्यापार तथा उद्योगबाट          
       </td>
       <td>
        {{ Form::number('incomeDetails[व्यापार तथा उद्योगबाट][amount]',  isset($incomeArr['व्यापार तथा उद्योगबाट']['amount']) ? $incomeArr['व्यापार तथा उद्योगबाट']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field income-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumIncome()']) }}
      </td>
      <td>
        लत्ता कपडा सम्बन्धी खर्च 
      </td>
      <td>
        {{ Form::number('expensesDetails[लत्ता कपडा सम्बन्धी खर्च][amount]', isset($expensesArr['लत्ता कपडा सम्बन्धी खर्च']['amount']) ? $expensesArr['लत्ता कपडा सम्बन्धी खर्च']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field expense-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumExpense()']) }}
      </td>
    </tr>
    <tr>
      <td>
       नोकरी तथा पेन्सनबाट         
     </td>
     <td>
      {{ Form::number('incomeDetails[नोकरी तथा पेन्सनबाट][amount]',  isset($incomeArr['नोकरी तथा पेन्सनबाट']['amount']) ? $incomeArr['नोकरी तथा पेन्सनबाट']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field income-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumIncome()']) }}
    </td>
    <td>
      शिक्षा सम्बन्धी खर्च
    </td>
    <td>
      {{ Form::number('expensesDetails[शिक्षा सम्बन्धी खर्च][amount]', isset($expensesArr['शिक्षा सम्बन्धी खर्च']['amount']) ? $expensesArr['शिक्षा सम्बन्धी खर्च']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field expense-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumExpense()']) }}
    </td>
  </tr>
  <tr>
    <td>
     वैदेशिक रोजगारबाट         
   </td>
   <td>
    {{ Form::number('incomeDetails[वैदेशिक रोजगारबाट][amount]',  isset($incomeArr['वैदेशिक रोजगारबाट']['amount']) ? $incomeArr['वैदेशिक रोजगारबाट']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field income-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumIncome()']) }}
  </td>
  <td>
    स्वास्थ्य उपचार खर्च
  </td>
  <td>
    {{ Form::number('expensesDetails[स्वास्थ्य उपचार खर्च][amount]', isset($expensesArr['स्वास्थ्य उपचार खर्च']['amount']) ? $expensesArr['स्वास्थ्य उपचार खर्च']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field expense-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumExpense()']) }}
  </td>
</tr>
<tr>
  <td>
   ज्याला मजदुरीबाट         
 </td>
 <td>
  {{ Form::number('incomeDetails[ज्याला मजदुरीबाट][amount]',  isset($incomeArr['ज्याला मजदुरीबाट']['amount']) ? $incomeArr['ज्याला मजदुरीबाट']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field income-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumIncome()']) }}
</td>
<td>
  चाडपर्व तथा मनोरञ्जन खर्च
</td>
<td>
  {{ Form::number('expensesDetails[चाडपर्व तथा मनोरञ्जन खर्च][amount]', isset($expensesArr['चाडपर्व तथा मनोरञ्जन खर्च']['amount']) ? $expensesArr['चाडपर्व तथा मनोरञ्जन खर्च']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field expense-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumExpense()']) }}
</td>
</tr>
<tr>
  <td>
   अन्य         
 </td>
 <td>
  {{ Form::number('incomeDetails[अन्य][amount]',  isset($incomeArr['अन्य']['amount']) ? $incomeArr['अन्य']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field income-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumIncome()']) }}
</td>
<td>
  कृषि तथा अन्य उत्पादन खर्च
</td>
<td>
  {{ Form::number('expensesDetails[कृषि तथा अन्य उत्पादन खर्च][amount]', isset($expensesArr['कृषि तथा अन्य उत्पादन खर्च']['amount']) ? $expensesArr['कृषि तथा अन्य उत्पादन खर्च']['amount'] : '', ['class'=>'mdl-textfield__input dashed-input-field expense-field', 'placeholder'=>'', 'id' => '', 'onkeyup' => 'sumExpense()']) }}
</td>
</tr>
<tr>
  <th>
   जम्मा                 
 </th>
 <td>
  {{ Form::number('totalIncome', isset($incomeAndExpensesData->totalIncome) ? $incomeAndExpensesData->totalIncome : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => 'totalIncome', 'readonly' => '']) }}
</td>
<th>
  जम्मा   
</th>
<td>
  {{ Form::number('totalExpense', isset($incomeAndExpensesData->totalExpense) ? $incomeAndExpensesData->totalExpense : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => 'totalExpense',  'readonly' => '']) }}
</td>
</tr>
</table>
</div>
</div>
<!-- 17 -->
<div class="form-group col-md-12">
  <p>
   १७. परिवारका कुनै सदस्यको वैंक तथा वित्तीय संस्थामा खाता छ ?
 </p>
 <div class="col-md-12">
  <label class="radio-inline">
    १. छ 
    <input type="radio" name="haveAccountInBank" value="1" checked="">
  </label> 
  <label class="radio-inline">
    २. छैन्
    <input type="radio" name="haveAccountInBank" value="0"  {{ isset($incomeAndExpensesData->haveAccountInBank) && $incomeAndExpensesData->haveAccountInBank == '0' ? 'checked' : '' }}>
  </label>
</div>
</div>
</div>
<div class="msgDisplay"></div>

</div>
</div>

<script type="text/javascript">
  function sumIncome(){
    var field = document.getElementsByClassName('income-field');
    var total = 0;
    for(var i = 0; i < field.length; i++){
      if (field[i].value != "") {            
        total =  total + parseFloat(field[i].value);        
      }
    }        
    // alert(total);
    document.getElementById('totalIncome').value = total;
  }

  function sumExpense(){
        // alert('asdf');
        var field = document.getElementsByClassName('expense-field');
        var total = 0;
        for(var i = 0; i < field.length; i++){
          if (field[i].value != "") {            
            total =  total + parseFloat(field[i].value);        
          }
        }

        document.getElementById('totalExpense').value = total;
      }
      document.addEventListener("DOMContentLoaded", function(event) {

        sumExpense();
        sumIncome();
      });
    </script>