<div class="right_col nep" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row top-part">
    <div class="col-md-6">
      <p align="left">
        <b class="ps">प. सं.:
          @if(isset($data->refCode))
          {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
          @else
          {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
          @endif
        </b></p>
        <p align="left"><b class="cn">च. नं.:
          {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
        </b></p>
      </div>
      <div class="col-md-6">
        <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p align="center" class="font-size-24">
        </p><h4 align="center"><b>विषय: विपन्नको सिफारिस सम्बन्धमा।</b> </h4>
        {{ Form::hidden('letter_subject', 'विपन्नको सिफारिस सम्बन्धमा') }}
        <p></p>
      </div>
    </div>
    <div class="row title-left">
      <div class="col-md-12">
        <p align="left"><b>जो सँग सम्बन्धित छ।</b></p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 content-para">
        <p align="left"> उपरोक्त विषयमा <b> 
          {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
        </b> वडा नं.  <b> {{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b> (साविक
         <!--  -->
         {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
         <!--  -->
         <b>
          <select onchange="changeSelect(this)" name="organizationType">
            <option value="गा.वि.स." 
            {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
            <option value="नगरपालिका" 
            {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
            <option value="उप महानगरपालिका" 
            {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
            <option value="महानगरपालिका" 
            {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
           
          </select>
        </b>, वडा नं. 
        {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} )
        निवासी  
        <b>
          <select onchange="changeSelect(this)" name="childrenTitle">
            <option value="श्री" {{!empty($data->childrenTitle) && $data->childrenTitle == 'श्री' ? 'selected' :'' }}>श्री</option>
            <option value="सुश्री" {{!empty($data->childrenTitle) && $data->childrenTitle == 'सुश्री' ? 'selected' :'' }} >सुश्री</option>
            <option value="श्रीमती" {{!empty($data->childrenTitle) && $data->childrenTitle == 'श्रीमती' ? 'selected' :'' }}>श्रीमती</option>
          </select>
        </b>
        {{ Form::text('nameOfChildren', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} ले  मेरो पारिवारिक आर्थिक स्थिति  नाजुक भएको कारणले विपन्न भएको हुनाले मेरो परिवार मेरो उच्च शिक्षाको खर्च जुटाउन असमर्थ भएकोले सो खुलाई सिफारिस पाँऊ भनी यस कार्यालयमा निवेदन पेश गरेकोले सो सम्बन्धमा बुझ्दा  जानेबुझे सम्म व्यहोरा मनासिब भएकोले निजलाई विपन्न व्यक्तिका लागि आरक्षित गरिएको स्थानमा सहभागी  हुन पाउने व्यवस्था गरी दिनुहुन सिफारिस गरिएको छ।</p>
      </div>
    </div>
    <div class="text-right btm-last">
      <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
      <p> <b>
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b></p>
    </div>
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr>
    @include('admin.shifaris.nibedakCommonField')
    <!-- END -->
    <div class="col-md-12">
      <hr>
    </div>