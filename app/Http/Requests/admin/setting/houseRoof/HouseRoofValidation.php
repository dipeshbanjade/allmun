<?php

namespace App\Http\Requests\admin\setting\houseRoof;

use Illuminate\Foundation\Http\FormRequest;

class HouseRoofValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:house_roofs',
            'nameEng' => 'required|min:4|max:50'
        ];
    }
}
