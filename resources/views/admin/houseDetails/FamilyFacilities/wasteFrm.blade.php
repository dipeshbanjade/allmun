<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
            <div class="row color_red">
                <div class="form-group col-md-6">
                    <p>
                        १३. तपाईको परिवारले घरबाट निस्कने ठोस फोहरमैला अक्सर कहाँ फाल्नुहुन्छ ? 
                    </p>
                    <br>
                    @if (isset($wasteMangement) && count($wasteMangement)>0)
                        @foreach ($wasteMangement as $wManagement)
                            <label class="radio-inline">    
                                <input type="radio" class="rdoClickMe" title="double click for reset" name="solidWasteMntNam" value="{{$wManagement->nameNep}}" {{ isset($hsWasteData) && $hsWasteData->solidWasteMntNam == $wManagement->nameNep ? 'checked="checked"' : '' }}>
                                {{$wManagement->nameNep}}
                            </label>
                            <br />
                        @endforeach
                    @else
                        <label>Insert data from setting</label>
                    @endif
                    {{ Form::text('otherSolidWasteMntNam', isset($hsWasteData->otherSolidWasteMntNam) ? $hsWasteData->otherSolidWasteMntNam : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'others']) }}
                    </label>
                    <br>
                </div>

                <div class="form-group col-md-6">
                    <p>
                        १४. तपाईको घरबाट निस्कने फोहोर पानी कहाँ व्यवस्थापन गर्नुहुन्छ ?
                    </p><br>
                        @if (isset($wasteWaterMangement) && count($wasteWaterMangement)>0)
                            @foreach ($wasteWaterMangement as $wWaterManagement)
                                <label class="radio-inline">
                                    <input type="radio" class="rdoClickMe" title="double click for reset" name="contWtrMntNam" value="{{$wWaterManagement->nameNep}}" {{ isset($hsWasteData) && $hsWasteData->contWtrMntNam == $wWaterManagement->nameNep ? 'checked="checked"' : '' }}>
                                    {{$wWaterManagement->nameNep}}
                                </label>
                                <br /> 
                            @endforeach
                        @else
                            <label>Insert data from setting</label>
                        @endif
                    {{ Form::text('otherContWtrMntNam', isset($hsWasteData->otherContWtrMntNam) ? $hsWasteData->otherContWtrMntNam : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'others']) }}
                    </label>
                    <br>
                </div>
                    
            </div>
            <div class="msgDisplay"></div>
        </div>
</div>