<div class="card-body row">

	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
		{{ Form::text('nameNep', null, ['class'=>'UnicodeKeyboardTextarea mdl-textfield__input txtNameNep', 'placeholder'=>'']) }}
		<label class = "mdl-textfield__label">@lang('commonField.personal_information.nameNep')
		</label>
	</div>
	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
		{{ Form::text('nameEng', null, ['class'=>'mdl-textfield__input txtNameEng', 'placeholder'=>'']) }}
		<label class = "mdl-textfield__label">@lang('commonField.personal_information.nameEng')</label>
	</div>
	<div class="col-lg-12 p-t-20">
		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
			<select name="tag" class="txttag form-control" required="true">
				<option value="">Select A Tag</option>
				<option value="tetanus">Tetanus-Diphtheria</option>
				<option value="hepatitisA">Hepatitis A</option>
				<option value="typhoid">Typhoid</option>
				<option value="hepatitisB">Hepatitis B</option>
				<option value="vaccine">Vaccine Name</option>
			</select>
		</div>
	</div>

</div>