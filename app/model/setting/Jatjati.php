<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class Jatjati extends Model
{
    protected $table = 'jatjatis';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
