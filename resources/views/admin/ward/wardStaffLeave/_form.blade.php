<div class="row">
      <div class="col-md-4 col-sm-12"> 
          <div class="form-group">
              <label> @lang('commonField.extra.noOfDays') </label>
              {{ Form::text('noOfDays', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'please select number of days', 'id' => 'leaveDays']) }}
          </div>
          <div class="form-group">
             <label>@lang('commonField.extra.startDate') </label>
             {{ Form::text('startDate', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'id'=>'nepDate', 'oninput'=>'inputEngDate()']) }}
          </div>
          <div class="form-group">
              <label>@lang('commonField.extra.endDate') </label>
              {{ Form::text('endDate', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'' , 'id'=>'myEngDate']) }}
          </div>
      </div>
      <div class="col-md-8  col-sm-12">
        <!-- summernote -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            
            <div class="form-group">
                  <!--  -->
                  <div class="mail-list">
                         <div class="compose-mail">
                               <label>@lang('commonField.extra.leaveType_id')</label>
                               <select name="leave_types_id" class="form-control">
                                 <option value="">
                                    @lang('commonField.extra.leaveType_id')
                                 </option>
                                 @if(count($leaveType) > 0)  <!-- selecting leave type -->
                                     @foreach($leaveType as $leave)
                                       <option value="{{ $leave->id }}" {{ isset($data->leave_types_id) ? 'selected' : '' }}>
                                           {{ $leave->nameNep }} - {{ $leave->nameEng }}
                                       </option>
                                     @endforeach
                                 @endif
                               </select>
                             <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                 <label>@lang('commonField.extra.desc')</label>
                                {{ Form::textarea('shortNoteEng', null, ['class'=>'summernote mdl-textfield__input', 'placeholder'=>'', 'rows' => '3']) }}
                             </div>
                         </div>
                  </div>
                  {!! Form::hidden('engDate',null, ['id'=>'engDate']) !!}
              </div>
            </div>
        </div>
      </div>
</div>