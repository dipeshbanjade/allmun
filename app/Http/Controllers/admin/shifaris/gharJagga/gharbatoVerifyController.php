<?php

namespace App\Http\Controllers\admin\shifaris\gharJagga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\GharbatoVerify;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\gharBatoVerfiyVal;
use DB;
use Auth;

use App\model\mylogic;

class gharbatoVerifyController extends Controller
{
    protected $viewPath = 'admin.shifaris.gharJagga.gharbatoVerify';
    protected $shifarisTableName     = 'gharbato_verifies';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixGharBatoVerify');

    }
    public function index()
    {
        //
        $page['page_title']       = 'Ghar Bato Verification';
        $page['page_description'] = 'Ghar Bato Verification';

        $instRecomd = GharbatoVerify::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page['page_title']       = 'Ghar Bato Verification : create';
        $page['page_description'] = 'Ghar Bato Verification details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));

    }

    
    public function store(gharBatoVerfiyVal $request)
    {
        //

        DB::beginTransaction();
        try {
            $gharbatoVerifyCount = count($request->wardNo);

            $wardNo = $request->wardNo;
            $seatNo = $request->seatNo;
            $kittaNo = $request->kittaNo;
            $area = $request->area;
            $gharStatus = $request->gharStatus;
            $streetType = $request->streetType;

            $remarks = $request->remarks;

            $gharbatoVerifyArray = [];

            for ($i=0; $i < $gharbatoVerifyCount; $i++) {
                array_push($gharbatoVerifyArray, [
                    'wardNo' => $wardNo[$i],
                    'seatNo' => $seatNo[$i],
                    'kittaNo' => $kittaNo[$i],
                    'area' => $area[$i],
                    'gharStatus' => $gharStatus[$i],
                    'streetType' => $streetType[$i],
                    'remarks' => $remarks[$i]
                ]);
            }
            $gharbatoVerifyJson = json_encode($gharbatoVerifyArray);


                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

            $isShifarisExist = isset($request->shifarisId) ? GharbatoVerify::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? GharbatoVerify::findOrfail($isShifarisExist['id']) : new GharbatoVerify;

            $save->createdBy = Auth::user()->id;

            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";
            $save->issuedDate  = $request->issuedDate;            
            $save->chalaniNumber = $request->chalaniNumber;
            $save->accountHolderName = $request->accountHolderName;
            $save->accountHolderAddress = $request->accountHolderAddress;
            $save->municipalityName = $request->municipalityName;
            $save->wardNumber = $request->wardNumber;
            $save->sabikAddress = $request->sabikAddress;
            $save->shrestaName = $request->shrestaName;

            $save->tableData = $gharbatoVerifyJson;                

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;
            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
            $request->chalaniNum = $request->chalaniNumber;
            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
             DB::commit();
             return response()->json([
                 'success' => true,
                 'message' => config('activityMessage.saveMessage')
             ]);
                   // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
         }else{
             return response()->json([
                 'success' => false,
                 'message' => config('activityMessage.unSaveMessage')
             ]);
                   // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }
  

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
