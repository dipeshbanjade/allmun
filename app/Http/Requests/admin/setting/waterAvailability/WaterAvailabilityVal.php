<?php

namespace App\Http\Requests\admin\setting\waterAvailability;

use Illuminate\Foundation\Http\FormRequest;

class WaterAvailabilityVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameEng' => 'required|min:4|max:200'
        ];
    }
}
