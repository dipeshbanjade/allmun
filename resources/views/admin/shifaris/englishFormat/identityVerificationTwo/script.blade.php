<script type="text/javascript">
$(function(){
	/*add buttoin*/
	$("form[name='frmWardidentityVerificationTwo']").validate({
		rules:{
			refCode : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			issuedDate : {
				required : true,
				date : true
			},
			letterSub : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			appName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			appTitle : {
				required : true				
			},
			municipality : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			ward : {
				required : true,
				minlength : 1,
				maxlength : 3
			},
			district : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			former : {
				required : true,
			},
			orgAddr : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			orgType : {
				required : true
			},
			orgWard : {
				required : true,
				minlength : 1,
				maxlength : 3
			},
			citizenshipNo : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			passportNo : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			name : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			citizenshipDate : {
				required : true,
				date : true
			},
			passportDate : {
				required : true,
				date : true
			},
			marriageNo : {
				required : true,
			},
			marriageDate : {
				required : true,
				date : true
			},
			FemaleName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			actualName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			nameOpt : {
				required : true
			},
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			deginations_id : {
				required : true,				
			}
		},
		messages: {
			refCode : "Ref Code is required",
			issuedDate : "Issue Date is required",
			letterSub : "Letter Sub is required",
			appName : "App Name is required",
			appTitle : "App Title is required",
			municipality : "Municipality is required",
			ward : "Ward is required",
			district : "District is required",
			former : "Former is required",
			orgAddr : "Org Address is required",
			orgType : "Org Type is required",
			orgWard : "Org ward is required",
			citizenshipNo : "Citizenship No is required",
			passportNo : "Passport No is required",
			name : "Name is required",
			citizenshipDate : "Citizenship Date is required",
			passportDate : "Passport Date is required",
			marriageNo : "Migration No is required",
			marriageDate : "Migration Date is required",
			FemaleName : "Female Name is required",
			actualName : "Actual name is required",
			nameOpt : "Name Opt is required",
			authorizedPerson : "Authorized person is required",
			deginations_id : "Degination ID is required"
		}
	});
});


$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                  $("input[name='appName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
            
          },complete:function(){
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>