<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
                <!-- 7 -->
                <?php
                   $yearlySurvival = $yearlySurvival ? $yearlySurvival : '';
                ?>
                <div class="form-group col-md-12">
                    <p>
                       ७. आफ्नो कृषि उत्पादनले तपाईको परिवारलाई कति महिना खान पुग्छ ? (एकमा मात्र ठीक चिन्ह लगाउनुहोस) ।
                    </p>
                    <div>
                        <label class="radio-inline">
                            <input type="radio" name="yearlySustain" value="3 mon" class="rdoClickMe"  {{ isset($yearlySurvival->survivalTime) && $yearlySurvival->survivalTime == '3 mon' ? 'checked' : '' }}>
                            १) ३ महिनासम्म
                        </label><br /> 
                        <label class="radio-inline">
                            <input type="radio" name="yearlySustain" value="4-6 mon" class="rdoClickMe" {{ isset($yearlySurvival->survivalTime) && $yearlySurvival->survivalTime == '4-6 mon' ? 'checked' : '' }}>
                            २) ४ देखि ६ महिना सम्म
                        </label><br />
                        <label class="radio-inline">
                            <input type="radio" name="yearlySustain" value="7-9 mon" class="rdoClickMe" {{ isset($yearlySurvival->survivalTime) && $yearlySurvival->survivalTime == '7-9 mon' ? 'checked' : '' }}>
                            ३) ७ देखि ९ महिनासम्म
                        </label><br />
                        <label class="radio-inline">
                            <input type="radio" name="yearlySustain" value="9 above" class="rdoClickMe" {{ isset($yearlySurvival->survivalTime) && $yearlySurvival->survivalTime == '9 above' ? 'checked' : '' }}>
                            ४) ९ महिना भन्दा बढी 
                        </label><br />
                    </div>
                </div> 
            </div>
            <div class="msgDisplay"></div>
            
    </div>
</div>