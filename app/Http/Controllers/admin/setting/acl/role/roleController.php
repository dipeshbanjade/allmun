<?php

namespace App\Http\Controllers\admin\setting\acl\role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\acl\Role;
use App\model\admin\acl\Permission;
use App\Http\Requests\admin\setting\acl\roleValidation;

class roleController extends Controller
{
    protected $viewPath = 'admin.acl.role';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       $page['page_title']         = 'Dashboard Role';
       $page['page_description']   = 'Dashboard role define';
       $roleData = Role::orderby('roleName', 'ASC')->where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

       return view($this->viewPath . '.index', compact(['page', 'roleData']));
    }

    public function store(roleValidation $request)
    {   
       $mySave = new Role;
       $mySave->roleName = $request->roleName;
       $mySave->desc = $request->desc ?? 'N/A';
       $mySave->roleFor = $request->roleFor;

       $mySave->status = 0;
       $mySave->softDelete = 0;
       $save = $mySave->save();
        if ($mySave) {
          return back()->withMessage(config('activityMessage.saveMessage'));
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function show($id){

    }

    public function edit($id)
    {
        $edit = Role::findOrFail($id);
        return response()->json($edit);
    }


    public function update(Request $request, $id)
    {
        
    }


    public function destroy($id)
    {
      $del = Role::findOrFail($id);
      $del->permissions->detach();
      $myDelete = $del->delete();
      if ($myDelete) {
        return response()->json([
           'success' => true,
           'message' => 'successfully deleted'
           ]);
      }else{
        return response()->json([
            'success' => false,
            'message' => 'Oops getting error try it again'
          ]);
      }
    }
}
