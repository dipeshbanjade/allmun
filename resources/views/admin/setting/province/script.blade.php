<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmProvince']").validate({
			rules:{
				name : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				capital : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				governor : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				chiefMinister : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				districtNo : {
					required : true,
					number :true 
				}
			},
			messages: {
				name : "The name of Province is required with min 3 char and max 30 char",
				capital : "The name of Capital is required with min 3 char and max 30 char",
				governor : "The name of Governor is required with min 3 char and max 30 char",
				chiefMinister : "The name of Chief Minister is required with min 3 char and max 30 char",
				districtNo : "The field of districtNo is required with alphabetic Character",


			}
		});
	});
</script>