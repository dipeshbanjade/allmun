<?php

namespace App\model\admin\acl;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $fillable = ['perName', 'moduleName','routeName', 'desc', 'status', 'softDelete'];

    public function roles()
    {
        return $this->belongsToMany(Role::class , 'permission_role');
    }
}
