<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class EducationLevel extends Model
{
    protected $table = 'education_levels';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
