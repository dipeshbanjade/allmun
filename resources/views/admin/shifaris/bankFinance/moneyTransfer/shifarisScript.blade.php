<script type="text/javascript">
var btnAddTableRow = $('#addTableRow');
var clickCount = 1;
btnAddTableRow.on('click', function(e){
	e.preventDefault();

	clickCount++;

	var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

	var personName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="runningAccNum[]"> ';
	var personPost = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="amount[]">';
	var remarks = '<input type="text" class="dashed-input-field" placeholder="   *" name="amountInWords[]">';

	var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

	var insertRow = "<tr class='tblRow'>"
	+"<td>"+serialNumber+"</td>"
	+"<td>"+personName+"</td>"
	+"<td>"+personPost+"</td>"
	+"<td>"+remarks+"</td>"
	+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
	+"</tr>";

	$('#house_kayem_table').append(insertRow);

	$('.btnRemoveItem').on('click', function(e){
		$(this).parent().parent().remove();
		return false;
	});

});
$(function(){
	/*add buttoin*/
	$("form[name='frmWardmoneyTransfer']").validate({
		rules:{
			refCode: {
				required: true,
				minlength: 3,
				maxlength: 30
			},
			issuedDate : {
				required : true,
				date : true
			},
			chalaniNum: {
				required: true,
				minlength: 1,
				maxlength: 30				
			},
			nameOfOffice : {
				required : true,
				minlength : 3,
				maxlength : 255
			},
			districtOfOffice : {
				required : true,
				minlength : 3,
				maxlength : 255
			},
			mainAccountNumber : {
				required : true,
				minlength : 7,				
				number: true
			},
			authorizedPerson : {
				required : true,
				minlength: 3,
				maxlength: 20
			}
		},
		messages: {
			refCode: "Patra Sankhya is required",
			issuedDate : "Issue Date is required",
			chalaniNum: "Chalani Number must be numeric",
			nameOfOffice : "Name of office is required",
			districtOfOffice : "Office district is required",
			mainAccountNumber : "Account Number must be number",
			authorizedPerson : "Authorizing person is required"
		}
	});
});
</script>