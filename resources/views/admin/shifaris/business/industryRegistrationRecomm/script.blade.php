<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardindustryRegistrationRecomm']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 10
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum : {
					required : true,
					minlength : 1,
					maxlength : 30					
				},
				officeChairman : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				receivedDate : {
					required : true,
					date: true
				},
				officeAddress : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				receivedNum : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number: true
				},
				estPerson : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				address : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				areaDetails : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				objective : {
					required : true,
					minlength : 5,
					maxlength : 30
				},
				occupationName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				refCode : "Reference Code is required",
				issuedDate : "Issue Date is required",
				chalaniNum : "Chalani Number is required",
				officeChairman : "Office Chairman name is required",
				receivedDate : "Invalid received date",
				officeAddress : "Office Address is required",
				receivedNum : "Received Number is required",
				address : "Address is required",
				estPerson : "Established Person is required",
				organizationAddress : "Organization Address is required",
				areaDetails : "Area Details is required",
				objective : "Objective is required",
				occupationName : "Occupation Name is required",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});

	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='estPerson']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>