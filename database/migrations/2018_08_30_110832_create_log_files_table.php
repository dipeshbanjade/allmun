<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogFilesTable extends Migration
{
    public function up()
    {
        Schema::create('log_files', function (Blueprint $table) {
            $table->increments('id',100);
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('action');
            $table->string('tblName');
            $table->string('tblId');
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);

            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('log_files');
    }
}
