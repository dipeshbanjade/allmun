<?php

namespace App\Http\Controllers\admin\municipility;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\mylogic;
use App\model\admin\municipility\MunicipilityDetails;

use App\model\admin\ward\WardDetails;
use App\model\admin\municipility\StaffMunicipility;

// use App\model\admin\municipility\MunicipilityDetails;

use App\Http\Requests\admin\setting\municipilityDetails\municipilityValidation;
use DB;
use Auth;
use Validator;


class municipilityDetailsController extends Controller
{
    protected $viewPath = 'admin.municipility.details';
    protected $munTable = 'municipilities';
    protected $provincesTbl = 'provinces';
    protected $districtTbl = 'districts';
    protected $prefix;
    protected $imagePath;
    protected $munLogoPath;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixMun');
        $this->munLogoPath = config('activityMessage.munLogo');
        $this->imagePath   = config('activityMessage.munLogo');

    }

    public function index()
    {


        $page['page_title']       = 'Municipility';
        $page['page_description'] = 'Municipilityn Details';

        $munDetails = MunicipilityDetails::where(['softDelete' => 0, 'status'=>0])->first();

        $provincesId = myLogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
        $districtId =  myLogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');

        $staffNum = StaffMunicipility::count();
        $wardNum = WardDetails::count();

        $getUniqueCode  = mylogic::sysRefCode($tablename = $this->munTable, $prefix = $this->prefix);
        //TODO  display municipility according to municipility 
        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
           return view($this->viewPath . '.index', compact(['page', 'munDetails', 'getUniqueCode', 'provincesId', 'districtId', 'staffNum', 'wardNum']));
        }else{
             return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }

    public function store(municipilityValidation $request)
    {
        $checkExists = MunicipilityDetails::first();
        if (count($checkExists) > 0)  {
            return back()->withMessage(configt('activityMessage.dataAlreadyExists'));
        }  // if already exists
        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
            DB::beginTransaction();
            try {
                    $save = new MunicipilityDetails;
                    $save->uniqueCode  = $request->uniqueCode;
                    $save->identifier  = mylogic::getRandNumber();
                    $save->nameNep  = $request->nameNep;
                    $save->nameEng  = $request->nameEng;
                    $save->landLineNumber  = $request->landLineNumber;
                    $save->faxNumber  = $request->faxNumber;
                    $save->addrNep  = $request->addrNep;
                    $save->addrEng  = $request->addrEng;
                    $save->provinces_id  = $request->provinces_id;
                    $save->districts_id  = $request->districts_id;
                    $save->muncipilityHead  = $request->muncipilityHead;
                    $save->bioDeviceId  = $request->bioDeviceId;

                 if ($request->hasFile('munLogo')) {
                   $imgFile = $request->file('munLogo');
                   $filename = str_replace(' ', '', $request->nameEng). '.' . $imgFile->getClientOriginalExtension();

                   $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->munLogoPath, $filename);
                   $save->munLogo =  $this->munLogoPath . $filename;
                }
                $mySave = $save->save();

                //end for login
                $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->munTable, $tblId = $request->uniqueCode);


                if ($mySave && $log) {
                     DB::commit();
                    return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
                }else{
                    return back()->withMessage(config('activityMessage.unSaveMessage'));
                }

            }catch (Exception $e) {
               DB::rollback();
               return back()->withMessage(config('activityMessage.dataNotInserted'));
            }
            
        }else{
             return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }
    public function edit($id)
    {
        $page['page_title']       = 'Municipility :edit Details';   //TODO change lana
        $page['page_description'] = 'Municipilityn Details';

        $provincesId = myLogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
        $districtId = myLogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');

        $data = MunicipilityDetails::findOrFail($id);

        return view($this->viewPath . '.edit', compact(['page', 'data', 'provincesId', 'districtId']));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
           'nameNep'   => 'required|min:3|max:60',
           'nameEng'   => 'required|min:3|max:60',
           'landLineNumber'  => 'required|min:7|max:10',
           'provinces_id'   => 'required',
            'districts_id'   => 'required'
         ]);

        if ($validator->fails()) {
           return back()->withErrors($validator)->withInput();
        }

        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
            DB::beginTransaction();
            try {
                $update = MunicipilityDetails::findOrFail($id);
                $update->nameNep  = $request->nameNep;
                $update->nameEng  = $request->nameEng;
                $update->landLineNumber  = $request->landLineNumber;
                $update->faxNumber  = $request->faxNumber;
                $update->addrNep  = $request->addrNep;
                $update->addrEng  = $request->addrEng;
                $update->provinces_id  = $request->provinces_id;
                $update->districts_id  = $request->districts_id;
                $update->muncipilityHead  = $request->muncipilityHead;
                $update->bioDeviceId       = $request->bioDeviceId;

                 if ($request->hasFile('munLogo')) {
                   $imgFile = $request->file('munLogo');
                   $filename = str_replace(' ', '', $request->nameEng). '.' . $imgFile->getClientOriginalExtension();

                   $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imagePath, $filename);
                   $update->munLogo =  $this->imagePath.$filename;
                }

                $myUpdate = $update->update();

                $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') .'Municipality', $tableName = $this->munTable, $tblId = $request->uniqueCode);


                if ($myUpdate) {
                    DB::commit();
                    return redirect()->route('admin.mun-set.index')->withMessage(config('activityMessage.updateMessage'));
                }else{
                    return back()->withMessage(config('activityMessage.notUpdated'));
                }
            }catch (Exception $e) {
               DB::rollback();
               return back()->withMessage(config('activityMessage.dataNotInserted'));
            }

        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }   
    }
    // public function destroy($id)
    // {
    //    $delId = MunicipilityDetails::findOrFail($id);
    //    $delId->softDelete = 1;
    //       $myDel = $delId->update();
    //    if ($myDel) {
    //        return mylogic::successDeleteMessage($code = 200);
    //     }else{
    //          return mylogic::unSuccessDeleteMessage($code = 401);
    //     }
    // }

    private function getNumberOfWard(){
      $wardCount =  WardDetails::count();
      if ($wardCount) {
          return $wardCount;
      }else{
        return 0;
      }
    }

    private function getNumberOfStaff(){
      $munStaff =  StaffMunicipility::count();
      if ($munStaff) {
          return $munStaff;
      }else{
        return 0;
      }
    }
}
