<div class="card-body row">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('name', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'row' => 4]) }}    
        <label class = "mdl-textfield__label">@lang('commonField.address_information.province')</label>
    </div>  

    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('capital', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.address_information.capital')</label>
    </div>


       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('governor', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.extra.governor')</label>
    </div>   

       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('chiefMinister', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.extra.chiefMinister')</label>
    </div> 

       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('districtNo', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.address_information.district')</label>
    </div>                                              
</div>