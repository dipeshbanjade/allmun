<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class taxClearCertVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:tax_clear_certifications',
            'issuedDate' => 'required',
            'title' => 'required',
            'personName' => 'required',
            'fatherTitle' => 'required',
            'fatherName' => 'required',
            'grandFatherTitle' => 'required',
            'grandFatherName' => 'required',
            'municipality' => 'required',
            'ward' => 'required',
            'district' => 'required',
            'former' => 'required',
            'orgAddr' => 'required',
            'orgType' => 'required',
            'orgWard' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
