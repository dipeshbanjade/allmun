<div class="card-box" style="background: #FFFFFF">
  <div class="card-body ">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-sm-6">
          <div class="form-group">
            <?php
            $relCitizen = getRelation($hsDetails->citizen_infos->id);
            ?>
            <label style="margin-right: 15px;">
              @lang('commonField.personal_information.gender'){{ $hsDetails->citizen_infos->id }}</label>
              <label class="radio-inline">
                <?php $checkMe = "checked='checked'";   ?>
                <input type="radio" name="gender" value="male" {{ isset($data) && $data->gender =='male' ? ' checked="checked"' : ''}}>
                @lang('commonField.personal_information.male')
              </label> 
              <label class="radio-inline">
                <input type="radio" name="gender" value="female" {{ isset($data) && $data->gender =='female' ? ' checked="checked"' : ''}}>
                @lang('commonField.personal_information.female')
              </label> 
              <label class="radio-inline">
                <input type="radio" name="gender" value="other" {{ isset($data) && $data->gender =='others' ? ' checked="checked"' : ''}}>
                @lang('commonField.personal_information.others')
              </label> <br><hr>
            </div>
          </div>
          <div class="col-sm-6 pull-right">
            <div class="form-group">
              <label style="margin-right: 15px;">@lang('commonField.personal_information.handicappedCondition')</label>
              <label class="radio-inline">
                <input type="radio" name="isdisabled" value="0" checked>
                @lang('commonField.extra.no')
              </label> 
              <label class="radio-inline">
                <input type="radio" name="isdisabled" value="1" {{ isset($data) && $data->gender =='female' ? ' checked="checked"' : ''}}>
                @lang('commonField.extra.yes')
              </label> 
            </div>
          </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <label>@lang('commonField.personal_information.firstNameNep')</label>
            {{ Form::text('fnameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'First Name Nepali']) }} 
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <label>@lang('commonField.personal_information.middleNameNep')</label>
            {{ Form::text('mnameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Middle Name Nepali']) }}
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <label>@lang('commonField.personal_information.lastNameNep')</label>
            {{ Form::text('lnameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Surname Nepali']) }}
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <label>@lang('commonField.personal_information.firstNameEng')</label>
            {{ Form::text('fnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'First Name']) }}
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <label>@lang('commonField.personal_information.middleNameEng')</label>
            {{ Form::text('mnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Middle Name']) }}
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <label>@lang('commonField.personal_information.lastNameEng')</label>
            {{ Form::text('lnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Surname']) }}
            <br>
          </div>
        </div>
      <!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <div class="form-group">
          <label>@lang('commonField.personal_information.dob') (B.S.)</label>
          {{ Form::text('dobBS', null, ['class'=>' mdl-textfield__input', 'id'=>'myDate', 'placeholder' => '2047-06-05']) }}
        </div>
      </div> -->

      <!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <div class="form-group">
          <label>@lang('commonField.personal_information.dob') (A.D.)</label>
          {{ Form::text('dobAD', null, ['class'=>'ndp-nepali-calendar mdl-textfield__input', 'id'=>'myEngDate', 'placeholder'=>'1990-09-21']) }}
        </div>
      </div> -->
{{--       <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <div class="form-group">
          <label>@lang('commonField.personal_information.dob')</label>
          {{ Form::text('dobBS', null, ['class'=>'ndp-nepali-calendar mdl-textfield__input', 'placeholder'=>'age']) }}
        </div>
      </div>
      --}}
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <div class="form-group">
          <label>@lang('commonField.personal_information.dob') (B.S.)</label>
          <!--  <input type="text" class="form-control input-md" id="myDate" name="dobBS"> -->
          
          {{ Form::text('dobBS', null, ['class'=>'mdl-textfield__input', 'placeholder' => '2047-06-05', 'id'=>'myDate']) }}
        </div>
      </div>
      
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <div class="form-group">
          <label>@lang('commonField.personal_information.dob') (A.D.)</label>
          <!--  <input type="text" class="form-control input-md dashed-input-field ndp-nepali-calendar" id="myEngDate" name="dobAD"> -->
          
          {{ Form::text('dobAD', null, ['class'=>'ndp-nepali-calendar mdl-textfield__input', 'id'=>'myEngDate', 'placeholder'=>'1990-09-21']) }}
        </div>
      </div>
      <!--  -->
       <div class="col-xs-12 col-sm-6 col-md-4">
         <div class="form-group">
           <label>@lang('commonField.extra.jatjati')</label>
           <select class="form-control" name="jatjatis_id">
             <option value="">@lang('commonField.extra.jatjati')</option>
             @if(count($jatJatiId) > 0)
             @foreach($jatJatiId as $jatJati)
             <option value="{{ $jatJati->id }}" {{ isset($data->jatjatis_id)  && $data->jatjatis_id == $jatJati->id ? 'selected' : ''}} {{(old('jatJati')==$jatJati->id)? 'selected':''}}>
             {{ $jatJati->refCode }} - {{ $jatJati->nameNep }} - {{ $jatJati->nameEng}}
            </option>
            @endforeach
            @endif
          </select>
        </div>
      </div>
      <!-- jatjati -->
        <div class="col-xs-12 col-sm-6 col-md-4">
         <div class="form-group">
           <label>@lang('commonField.personal_information.religion')</label>
           <select class="form-control" name="religiouses_id">
             <option value="">@lang('commonField.extra.religious_id')</option>
             @if(count($religiousId) > 0)  <!-- selecting leave type -->
             @foreach($religiousId as $religious)
             <option value="{{ $religious->id }}" {{ isset($data->religiouses_id)  && $data->religiouses_id == $religious->id ? 'selected' : ''}} {{(old('religious')==$religious->id)? 'selected':''}}>
             {{ $religious->refCode }} -  {{ $religious->nameNep }} - {{ $religious->nameEng}}
            </option>
            @endforeach
            @endif
          </select>
        </div>
      </div>
      <!-- religious -->



        <div class="col-xs-12 col-sm-6 col-md-4">
         <div class="form-group">
           <label>@lang('commonField.personal_information.motherLang')</label>
           <select class="form-control" name="sys_languages_id">
             <option value="">@lang('commonField.extra.language_id')</option>
             @if(count($syslangId) > 0)  <!-- selecting leave type -->
             @foreach($syslangId as $lang)
             <option value="{{ $lang->id }}" {{ isset($data->sys_languages_id)  && $data->sys_languages_id == $lang->id ? 'selected' : ''}} {{(old('lang')==$lang->id)? 'selected':''}}>
              {{ $lang->refCode }} - {{ $lang->langNep }} - {{ $lang->langEng}}
            </option>
            @endforeach
            @endif
          </select>
        </div>
      </div>

       <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="form-group">
          <label>@lang('commonField.houseHoldSetting.educationLevelAbove5')</label>

          <select class="form-control" name="qualifications_id">
            <option value="">@lang('commonField.extra.qualification_id')</option>
            @if(count($qualificationId) > 0)  <!-- selecting leave type -->
            @foreach($qualificationId as $qua)
            <option value="{{ $qua->id }}" {{ isset($data->qualifications_id)  && $data->qualifications_id == $qua->id ? 'selected' : ''}} {{(old('qua')==$qua->id)? 'selected':''}}>
             {{ $qua->refCode }} - {{ $qua->nameNep }} - {{ $qua->nameEng}}
           </option>
           @endforeach
           @endif
         </select>
         <br>
       </div>
      </div>
      <!-- basobasko awastha -->
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="form-group">
          <label>@lang('commonField.extra.familyLiving')</label>
          <select class="form-control" name="family_living_id">
           <option value="">@lang('commonField.extra.familyLiving')</option>
           <?php //dd($familyLivingId); ?>
           @if(count($familyLivingId) > 0) 
           @foreach($familyLivingId as $fmLiv)
           <option value="{{ $fmLiv->id }}">
            {{ $fmLiv->refCode }} - {{ $fmLiv->nameNep }} - {{ $fmLiv->nameEng}}
          </option>
          @endforeach
          @endif
        </select>
      </div>
      </div>
      <!--occutpation  -->
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="form-group">
          <label>@lang('commonField.personal_information.occupation10above')</label>
          <select class="form-control" name="occupations_id">
           <option value="">@lang('commonField.extra.occupation_id')</option>
           @if(count($occupationId) > 0) 
           @foreach($occupationId as $occup)
           <option value="{{ $occup->id }}" {{ isset($data->occupations_id)  && $data->occupations_id == $occup->id ? 'selected' : ''}} {{(old('occup')==$occup->id)? 'selected':''}}>
             {{ $occup->refCode }} - {{ $occup->nameNep }} - {{ $occup->nameEng}}
          </option>
          @endforeach
          @endif
        </select>
      </div>
      </div>
      <!-- maritial status  -->
      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="form-group">
          <label>@lang('commonField.extra.maritialStatus') </label>
          <select class="form-control" name="maritialStauts_id">
            <option value="">@lang('commonField.extra.maritialStatus')</option>
            @if(count($maritialStatus) > 0)  <!-- selecting leave type -->
            @foreach($maritialStatus as $maritail)
            <option value="{{ $maritail->id }}" {{ isset($data->maritialStauts_id)  && $data->maritialStauts_id == $maritail->id ? 'selected' : ''}} {{(old('maritail')==$maritail->id)? 'selected':''}}>
             {{ $maritail->refCode  }} - {{ $maritail->nameNep }} - {{ $maritail->nameEng}}
           </option>
           @endforeach
           @endif
         </select>
       </div>
      </div>
      <!--  -->

      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="form-group">
          <label>@lang('commonField.personal_information.disablityStatus') </label>
          <select class="form-control" name="disabilities_id">
            <option value="">@lang('commonField.personal_information.disablityStatus')</option>
            @if(count($disablitiyStatus) > 0)  <!-- selecting leave type -->
            @foreach($disablitiyStatus as $disablity)
            <option value="{{ $disablity->id }}" {{ isset($data->disabilities_id)  && $data->disabilities_id == $disablity->id ? 'selected' : ''}} {{(old('disablity')==$disablity->id)? 'selected':''}}>
             {{ $disablity->refCode  }} - {{ $disablity->nameNep }} - {{ $disablity->nameEng}}
           </option>
           @endforeach
           @endif
         </select>
       </div>
      </div>


      <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div class="form-group">
          <label style="margin-right: 15px;">@lang('commonField.extra.maritialStatus')?</label>
          <label class="radio-inline">
            <input type="radio" name="maritialStauts" class="marrExists" value="0" checked="checked">
            @lang('commonField.extra.no')
          </label> 
          <label class="radio-inline">
            <input type="radio" name="maritialStauts" class="marrExists" value="1">
            @lang('commonField.extra.yes')
          </label> 
        </div>
      </div> -->
    </div>
  </div>
  <fieldset>
    <div class="form-row">
      


<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
  <div class="form-group">
    <label>@lang('commonField.extra.withOwnerRelation')</label>
    <select class="form-control" name="family_relations_id">
     <option value="">@lang('commonField.extra.familyRelation')</option>
     @if(count($fmRelationId) > 0) 
     @foreach($fmRelationId as $fmRel)
     <option value="{{ $fmRel->id }}">
      {{ $fmRel->nameNep }} - {{ $fmRel->nameEng}}
    </option>
    @endforeach
    @endif
  </select>
</div>
</div>

<p class="pull-right">
   <br><hr> <label class="text-danger">नभएको खण्डमा खाली छोडनु होला</label>
</p>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
  <div class="form-group">
   <label>
     @lang('commonField.extra.grandFatherName')
   </label>
   <select name="grandFather_id" class="form-control">
    <option value="">@lang('commonField.personal_information.selectFatherName')</option>
    <?php
    $getGrandFather = getCitizenDetails($hsDetails->citizen_infos_id);               
    ?>
    <option value="{{ $getGrandFather->id }}">
      {{ 
        $getGrandFather->fnameNep .' '. $getGrandFather->mnameNep .' '. $getGrandFather->lnameNep
      }} 
    </option>

    @if(count($fmRelationId) > 0) 
    @foreach($relCitizen as $father)
    <?php
    $getFather = getCitizenDetails($father->relation_id);               
    ?>
    <option value="{{ $getFather->id }}">
      {{ 
        $getFather->fnameNep .' '. $getFather->mnameNep .' '. $getFather->lnameNep
      }} 
    </option>
    @endforeach 
    @endif
  </select>
</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
  <div class="form-group">
   <label>
     @lang('commonField.personal_information.selectFatherName')
   </label>
   <select name="fathers_id" class="form-control">
    <option value="">@lang('commonField.personal_information.selectFatherName')</option>
    <?php
    $getFather = getCitizenDetails($hsDetails->citizen_infos_id);              
    ?>
    <option value="{{ $getFather->id }}">
      {{ 
        $getFather->fnameNep .' '. $getFather->mnameNep .' '. $getFather->lnameNep
      }} 
    </option>
    @if(count($fmRelationId) > 0) 
    @foreach($relCitizen as $father)
    <?php
    $getFather = getCitizenDetails($father->relation_id);               
    ?>
    <option value="{{ $getFather->id }}">
      {{ 
        $getFather->fnameNep .' '. $getFather->mnameNep .' '. $getFather->lnameNep
      }} 
    </option>
    @endforeach 
    @endif
  </select>
</div>
</div>
<!--  -->
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
  <div class="form-group">
   <label>
     @lang('commonField.personal_information.selectMotherName')
   </label>
   <select name="mothers_id" class="form-control">
    <option value="">@lang('commonField.personal_information.selectMotherName')</option>
    <?php
    $getMother = getCitizenDetails($hsDetails->citizen_infos_id);              
    ?>
    <option value="{{ $getMother->id }}">
      {{ 
        $getMother->fnameNep .' '. $getMother->mnameNep .' '. $getMother->lnameNep
      }} 
    </option>
    @foreach($relCitizen as $mother)
    <?php
    $getMother = getCitizenDetails($mother->relation_id);               
    ?>
    <option value="{{ $getMother->id }}">
      {{ 
        $getMother->fnameNep .' '. $getMother->mnameNep .' '. $getMother->lnameNep
      }} 
    </option>
    @endforeach 
  </select>
</div>
</div> 
<!--  -->
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
  <div class="form-group">
   <label>
     @lang('commonField.extra.husbandWifeName')
   </label>
   <select name="wifeHusband_id" class="form-control">
    <option value="">@lang('commonField.extra.husbandWifeName')</option>
    <?php
    $getWifeHus = getCitizenDetails($hsDetails->citizen_infos_id);              
    ?>
    <option value="{{ $getWifeHus->id }}">
      {{ 
        $getWifeHus->fnameNep .' '. $getWifeHus->mnameNep .' '. $getWifeHus->lnameNep
      }} 
    </option>
    @foreach($relCitizen as $wifeHus)
    <?php
    $getWifeH = getCitizenDetails($wifeHus->relation_id);               
    ?>
    <option value="{{ $getWifeH->id }}">
      {{ 
        $getWifeH->fnameNep .' '. $getWifeH->mnameNep .' '. $getWifeH->lnameNep
      }} 
    </option>
    @endforeach 
  </select>
  <input type="hidden" name="gharmuli_id" value="{{ $hsDetails->citizen_infos->id }}" >
</div>
</div> 




</div>
</fieldset>
<div class="msgDisplay"></div>
</div>