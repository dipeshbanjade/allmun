<?php

namespace App\model\admin\municipility;

use Illuminate\Database\Eloquent\Model;

class MunStaffDailyReport extends Model
{
    protected $table = 'mun_staff_daily_reports';
}
