<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmedithouseType']").validate({
            rules:{
 
                houseEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 100
                }
            },
            messages: {
   
                houseEng : "The name of house Type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmhouseType']").validate({
            rules:{

                houseEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 100
                }
            },
            messages: {
         
                houseEng : "The name of house Type is required"
            }
        });
	var btnAddHouseType = $('.btnAddHouseType');
  btnAddHouseType.on('click', function(e){
      $('#houseTypeModel').modal('show'); 
  });

  var btnEdithouseType = $('.btnEdithouseType');
  btnEdithouseType.on('click', function(e) {
      e.preventDefault();
      var url = $(this).data('url');
      alert(url);
      $.ajax({
        'type': 'GET',
        'url': url,
        success: function (response) {
            $('.houseTypeId').val(response.id);
            $('.txtRefCode').val(response.refCode).attr('disabled',true);
            $('.txtNameNep').val(response.houseNep);
            $('.txtNameEng').val(response.houseEng);
            $("#editHouseModel").modal('show');
        }
    })
  });


  var frmUpdatehouseType = $('#frmUpdatehouseType');
  frmUpdatehouseType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.houseTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/houseType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editHouseModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editHouseModel').modal('hide');
});
      });
</script>