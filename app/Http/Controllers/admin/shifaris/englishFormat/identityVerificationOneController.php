<?php

namespace App\Http\Controllers\admin\shifaris\englishFormat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\shifaris\IdentityVerifyOneValidation;
use App\model\shifaris\ApplicantUser;
use App\model\shifaris\IdentityVerificationONE;
use App\model\mylogic;
use DB;
use Auth;

class IdentityVerificationOneController extends Controller
{
    protected $viewPath = 'admin.shifaris.englishFormat.identityVerificationOne';
    protected $shifarisTableName     = 'identity_verification_o_n_es';
    protected $appUsrTable     = 'applicant_users';
    protected $deginationsTbl = 'deginations';
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.IdentityVerificationOne');

    }

    public function index()
    {
        $page['page_title']       = 'Identity Verification 1';
        $page['page_description'] = 'Identity Verification 1';
        
        $IdentityVerificationOne = ApplicantUser::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));


    }

    public function create()
    {
        $page['page_title']       = 'Identity Verification 1 : create';
        $page['page_description'] = 'Identity Verification 1 details';
        $getUniqueCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationsTbl, $orderColumn = 'nameNep', $orderby = 'ASC');
        return view($this->viewPath . '.create', compact(['page', 'getUniqueCode', 'deginationsId']));

    }

    public function store(IdentityVerifyOneValidation $request)
    {

     DB::beginTransaction();
     try {

        // $appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Shifaris', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


        $isShifarisExist = isset($request->shifarisId) ? IdentityVerificationONE::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? IdentityVerificationONE::findOrfail($isShifarisExist['id']) : new IdentityVerificationONE;

        $save->createdBy = Auth::user()->id;
        
        $save->refCode  = $request->refCode;

        $save->wards_id  = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";
        $save->issuedDate  = $request->issuedDate;
        $save->chalaniNum  = $request->chalaniNum;
        $save->letterSub = $request->letterSub;
        $save->appName  = $request->appName;
        $save->appTitle  = $request->appTitle;
        $save->municipality  = $request->municipality;
        $save->ward  = $request->ward;
        $save->district  = $request->district;
        $save->former  = $request->former;
        $save->orgAddr  = $request->orgAddr;
        $save->orgType  = $request->orgType;
        $save->orgWard  = $request->orgWard;
        $save->docName  = $request->docName;
        $save->docNo  = $request->docNo;
        $save->userName  = $request->userName;
        $save->userNameTwo  = $request->userNameTwo;
        $save->docNameTwo  = $request->docNameTwo;
        $save->docNoTwo  = $request->docNoTwo; 
        $save->docDate  = $request->docDate;
        $save->genderType  = $request->genderType;
        $save->genderTypeTwo  = $request->genderTypeTwo;
        $save->authorizedPerson  = $request->authorizedPerson;
        $save->deginations_id  = $request->deginations_id;

        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

               //end for login
        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Shifaris', $tableName = $this->shifarisTableName, $tblId = $save->refCode);

        // $appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $save->id, $applicantName, $applicantAddress, $citizenshipNumber, $phoneNumber, $email, $dob, $fatherName, $profilePic, $citizenshipImgPath);


                    $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }

    }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }


}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
