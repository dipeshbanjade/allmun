<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveisforTable extends Migration
{
    public function up()
    {
        Schema::create('leaveisfor', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->integer('municipilities_id')->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('createdBy')->nullable()->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->date('dateFromEng')->nullable();
            $table->date('dateFromNep')->nullable();
            $table->string('leaveFor')->nullable();

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('leaveisfor');
    }
}
