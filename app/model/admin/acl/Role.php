<?php

namespace App\model\admin\acl;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['roleName', 'desc', 'status', 'softDelete', 'id', 'roleFor'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class , 'permission_role');
    }

    // public function users()
    // {
    //     return $this->belongsToMany(User::class);
    // }
}
