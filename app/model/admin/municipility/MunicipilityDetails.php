<?php

namespace App\model\admin\municipility;

use Illuminate\Database\Eloquent\Model;

class MunicipilityDetails extends Model
{
    protected $table = 'municipilities';

    public function provinces(){
    	return $this->belongsTo('App\model\setting\Province'::class);
    }

    public function districts(){
    	return $this->belongsTo('App\model\setting\District'::class);
    }
}
