<script type="text/javascript">
    $(function(){
        $("form[name='frmEditVisitType']").validate({
         rules:{
          nameEng : {
            required: true,
            minlength : 3,
            maxlength : 60
          },
          refCode : {
            required : true
          }
         },
         messages: {
           nameEng  : "visit type is required",
           refCode  : "refCode required"
         }
        });
        });
    $(function(){
        $("form[name='frmVisitType']").validate({
         rules:{
          nameEng : {
            required: true,
            minlength : 3,
            maxlength : 60
          },
          refCode : {
            required : true
          }
         },
         messages: {
           nameEng  : "visit type is required",
           refCode  : "refCode required"
         }
        });

       var btnAddVisitType = $('.btnAddVisitType');
        btnAddVisitType.on('click', function(){
            $('#frmAddVisitType').modal('show');
        });
        /*updating process*/
        var updateVisitType = $('.updateVisitType');
            updateVisitType.on('click', function(e) {
              e.preventDefault();
                var url = $(this).data('url');
                $.ajax({
                    'type': 'GET',
                    'url': url,
                    success: function (response) {
                        $('.visitTypeId').val(response.id),
                        $('.txtRefCode').val(response.refCode),
                        $('.txtNameNep').val(response.nameNep);
                        $('.txtNameEng').val(response.nameEng);
                        $('.txtDesc').val(response.desc);
                        $("#frmUpdateVisitType").modal('show');
                    }
                })
            });

       /*update form code*/
        var frmUpdateVisitTyp = $('#frmUpdateVisitTyp');
        frmUpdateVisitTyp.on('submit', function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var id   = $('.visitTypeId').val();
        var url  = "{{URL::to('/')}}" + "/setting/visitType/"+id+"/update";
        $.ajax({
          'type' : 'POST',
          'url'  : url,
          data    : data,
          success : function(response){
           console.log(response);
           if (response.success==true) {
               $('.infoDiv').append(response.message).addClass('alert alert-success');
           }
           if (response.success==false) {
              $.each(response.message, function(key, value){
                  $('.infoDiv').append(value).addClass('alert alert-danger');
             })
           }
          },complete:function(){
           $("#frmUpdateVisitType").modal('hide');
           setTimeout(location.reload.bind(location), 1500);
          }
        })
        .fail(function (response) {
            alert('data already exists');
        });
        $('#frmUpdateVisitType').modal('hide');
        });


     });


</script>