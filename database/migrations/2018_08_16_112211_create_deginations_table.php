<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeginationsTable extends Migration
{
    public function up()
    {
        Schema::create('deginations', function (Blueprint $table) {
            $table->increments('id');
        
            $table->string('refCode', 100)->unique();
            $table->string('nameNep', 100);
            $table->string('nameEng', 100)->nullable();

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('deginations');
    }
}
