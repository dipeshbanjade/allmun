@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="card card-box">
                <div class="card-head">
                    <header>@lang('commonField.links.fieldVisit')</header>
                </div>
                {{ Form::open(['route' => 'fieldVisit.store', 'name' => 'frmFieldVisit']) }}
                <div class="card-body " id="bar-parent2">
                   @include('admin.system.fieldVisit._form')
                </div>
                   <div class="text-right"> 
                       <button type="button" class="btn btn-danger pull-right" onclick="history.back()">@lang('commonField.button.back')
                       </button>
                        {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
                  </div>
                  {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom_script')
   @include('admin.system.fieldVisit.script')
@endsection