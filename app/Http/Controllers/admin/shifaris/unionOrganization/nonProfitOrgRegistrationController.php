<?php

namespace App\Http\Controllers\admin\shifaris\unionOrganization;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\shifaris\NonProfitOrgRegistration;
use App\Http\Requests\admin\shifaris\nonProfitOrgVal;
use DB;
use Auth;

// use App\model\admin\ward\union;

use App\model\mylogic;

class NonProfitOrgRegistrationController extends Controller
{
    protected $viewPath = 'admin.shifaris.unionOrganization.nonProfitOrgRegistration';
    protected $shifarisTableName     = 'non_profit_org_registrations';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;
    // protected $wardTbl     = 'wards';
    // protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixNonProfitOrg');

    }

    public function index()
    {
        $page['page_title']       = 'Non Profit Organization Registration';
        $page['page_description'] = 'Non Profit Organization Registration';

        
        $nirman = NonProfitOrgRegistration::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        
    }

    public function create()
    {
        $page['page_title']       = 'Non Profit Organization Registration : create';
        $page['page_description'] = 'Non Profit Organization Registration details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        

        return view($this->viewPath . '.create', compact(['page']));
    }

    public function store(nonProfitOrgVal $request)
    {   
        // dd('Dashain Ayo');
     
     DB::beginTransaction();
     try {
            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


        $isShifarisExist = isset($request->shifarisId) ? NonProfitOrgRegistration::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? NonProfitOrgRegistration::findOrfail($isShifarisExist['id']) : new NonProfitOrgRegistration;

        $save->createdBy = Auth::user()->id;
        
        $save->refCode  = $request->refCode;

                // $save->wards_id  = Auth::user()->wards_id ? Auth::user()->wards_id : NULL;

        $save->wards_id  = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? NULL;
        $save->issuedDate  = $request->issuedDate;
        $save->chalaniNumber = $request->chalaniNumber;
        $save->dartaCode = $request->dartaCode;
        $save->dartaDate = $request->dartaDate;
        $save->companyName = $request->companyName;
        $save->companyAddress = $request->companyAddress;
        $save->subjectArea = $request->subjectArea;
        $save->businessStartDate = $request->businessStartDate;
        $save->companyEmail = $request->companyEmail;
        $save->companyContact = $request->companyContact;
        $save->operatorName = $request->operatorName;
        $save->operatorAddress = $request->operatorAddress;
        $save->operatorEmail = $request->operatorEmail;
        $save->operatorContact = $request->operatorContact;
        $save->authorizedPerson = $request->authorizedPerson;
        $save->deginations_id  = $request->deginations_id;
        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

        $request->chalaniNum = $request->chalaniNumber;
        $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
        if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }

  
}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
