<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditageGroup']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of filter Process is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmageGroup']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of filter Process is required"
            }
        });
	var btnAddageGroup = $('.btnAddageGroup');
	btnAddageGroup.on('click', function(e){
		$('#ageGroupModel').modal('show'); 
	});

	var btnEditageGroup = $('.btnEditageGroup');
	btnEditageGroup.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.ageGroupId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editageGroupModel").modal('show');
			}
		})
	});

	var frmUpdateageGroup = $('#frmUpdateageGroup');
  frmUpdateageGroup.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.ageGroupId').val();
    var url  = "{{URL::to('/')}}" + "/setting/ageGroup/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editageGroupModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editageGroupModel').modal('hide');
});
});
</script>