<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='formKulaiPathayeko']").validate({
			rules:{
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 2,
					maxlength : 30
				},
				companyHead : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				companyName : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				reason : {
					required : true
				},
				jetReason : {
					required : true
				},
				roadName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				companyHead : "Company Head is required",
				companyName : "Company Name is required",
				roadName : "Road Name is required",
				reason : "Reason is required",
				jetReason : "Jet Reason is required",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});
</script>