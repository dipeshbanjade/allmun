@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
	<div class="card-box">
		<div class="card-body ">
			<div class="card-head">
				<header><i class="fa fa-eye"></i>&nbsp;@lang('commonField.extra.department')</header>
			</div>
			<button id="" class="btnAddDepartment btn btn-primary pull-right"><i class="fa fa-plus"></i><span> @lang('commonField.extra.createDepartment')</span></button>
			<div class="table-scrollable" style="color:black;">
				<table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
					<thead>
						<tr>
							<th>@lang('commonField.extra.sn')</th>
							<th>@lang('commonField.personal_information.name') </th>
							<th> @lang('commonField.extra.action') </th>
						</tr>
					</thead>
					<tbody>
						<tr class="odd gradeX">
							@php  $count = 1  @endphp
							@if(count($allDepartments) > 0)
							@foreach($allDepartments as $dep)
							<td>{{ $count ++ }}</td>
							<td>{{ $dep->nameNep ?? 'N/A'}} {{ $dep->nameEng }}</td>
							<td>
								<a href="" class="btnEditDep btn btn-primary btn-xs" id="" data-url="{{ route('admin.department.edit',$dep->id)}}">
									<i class="fa fa-pencil"></i>
								</a>
								<a href="#" class="deleteMe" data-url="{{ route('admin.department.delete',$dep->id)}}" data-name="{{ $dep->nameEng }}">
								<button class="btn btn-danger btn-xs">
									<i class="fa fa-trash-o "></i></button>
									</a>
									<a href="#" class="delete_data" id="" ><button class="btn btn-danger btn-xs">
										<i class="fa fa-eye-slash"></i></button></a>
									</td>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		@endsection
		@section('modelSection')
		<div class="modal" id="DepModel">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.addDepartment')</i></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						{{ Form::open(['route' => 'admin.department.store', 'name'=>'frmDepartment']) }}
						@include('admin.setting.department._form')
						<div class="col-lg-12 p-t-20 text-right"> 
							{{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
							<button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
						</div>
						{{ Form::close() }}
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">

					</div>

				</div>
			</div>
		</div>
		<!-- Edit department Model -->
		<div class="modal" id="editDepModel">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.editDepartment')</i></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						 {{ Form::open(['name'=>'frmeditDepartment', 'id' => 'frmUpdateDept']) }}
						     @include('admin.setting.department._form')
						     {{ Form::hidden('depId', null, ['class' => 'depId']) }}
							<div class="col-lg-12 p-t-20 text-right"> 
								{{ Form::submit('CREATE', ['class' => 'btn btn-success']) }}
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
					  {{ Form::close() }}
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">

					</div>

				</div>
			</div>
		</div>
@endsection
@section('custom_script')
@include('admin.setting.department.script')
@endsection