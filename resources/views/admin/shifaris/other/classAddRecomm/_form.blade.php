    <div class="row top-part">
      <div class="col-md-6">
       <p align="left">
        <b>@lang('commonField.extra.refCode'):</b>
        @if(isset($data->refCode))
        {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
        @else
        {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
        @endif
      </p>
      <p align="left">
        <b> च. नं.:
          {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
        </b>
      </p>
    </div>
    <div class="col-md-6">
      <p align="right">
        <b class="mt">मिति :
          {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
        </b>
      </p>
    </div>
  </div>
  <div class="row title-left">
   <div class="col-md-12">
    <p align="left">श्री 
      {{ Form::text('organizationChairman', isset($data->organizationChairman) ? $data->organizationChairman : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationChairman', 'required' => 'required']) }}</p>
      <p align="left">
        <b>
         {{ Form::text('organizationAddress', isset($data->district) ? $data->district : getChecker()->districts->districtNameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationAddress', 'required' => 'required']) }}
       </b>
       ।</p>
     </div>
   </div>
   <div class="row">
    <div class="col-md-12">
      <p align="center" class="font-size-24">
      </p><h4 align="center"><b>विषय: सिफारिस सम्बन्धमा।</b> </h4>
      {{ Form::hidden('letter_subject', 'सिफारिस सम्बन्धमा।', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}

    </div>
  </div>
  <div class="row">
    <div class="col-md-12 content-para">
      <p align="left"> उपरोक्त सम्बन्धमा 
        <b>
         {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
       </b> 
       वडा नं. 
       <b>
         {{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNum', 'required' => 'required']) }}
       </b> अन्तर्गत रहेको 
       <b>
        {{ Form::text('schoolName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'schoolName', 'required' => 'required']) }}
      </b>
      मा 
      <b>
        {{ Form::text('class', null, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => 'class', 'required' => 'required']) }}
      </b>
      कक्षा सम्म पठनपाठन भैरहेको र हाल 
      <b>
        {{ Form::text('applicationReason', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'applicationReason', 'required' => 'required']) }}
      </b>
      कारणले  कक्षा <b> 
      <select onchange="changeSelect(this)" name="addRemove">
        <option value="थप" {{ !empty($data->addRemove) && $data->addRemove == 'थप' ? 'selected' : '' }}>थप</option>
        <option value="घट" {{ !empty($data->addRemove) && $data->addRemove == 'घट' ? 'selected' : '' }}>घट</option>
      </select>
    </b> गर्नुपर्ने भएकोले सो को  लागि सिफारिस गरी पाऊँ भनी यस वडा कार्यालयमा पर्न आएको निवेदन सम्बन्धमा त्यहाँको नियमानुसार कक्षा 
    {{ Form::text('classthapghat', 'थप', ['class'=>'dashed-input-small-field star', 'placeholder'=>'    *', 'id' => 'classthapghat', 'required' => 'required', 'readonly' => 'readonly']) }}
    स्वीकृतिको लागि सिफारिस गरी पठाइएको व्यहोरा अनुरोध गरिन्छ।</p>
    <div class="text-right btm-last">
      <p>
       {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} 
     </p>
     <p> 
      <b>
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b>
    </p>
  </div>
  <!--views for nibedak detail -->
  <div class="clearfix"></div>
  <hr>
  @include('admin.shifaris.nibedakCommonField')
  <!-- END -->
  <div class="col-md-12">
    <hr>
  </div>
</div>
</div>
