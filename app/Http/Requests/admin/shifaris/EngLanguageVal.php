<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class EngLanguageVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:eng_languages',
            'issuedDate' => 'required',
            'nameOfOffice' => 'required',
            'officeAddress' => 'required',
            'officeAddress2' => 'required',
            'subject' => 'required',
            'letterDesc' => 'required',
            'authorizedPerson' => 'required'

        ];
    }
}
