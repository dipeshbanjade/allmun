<style type="text/css">
input{
    width: 70px;
}
.lblPadding{
   margin: 10px 0px 10px 0;
}

</style>


<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
        <div class="form-group col-md-12">{{-- 12 --}}
            <p>
            १. तपाईको परिवारमा एक बर्ष भन्दा कम उमेरका बालबालिका छन् भने निम्न खोपहरु कति पटक लगाए ? संख्या उल्लेख गर्नु होस् ।                     
            </p>
            <br>
            <table border="1">
            <?php
                 // data come from controller to display added data in form
                 $updateInfantVac = isset($infantVaccine->hsInfantVacDetails) ? json_decode($infantVaccine->hsInfantVacDetails) : '';

            ?>
                <thead>
                    <tr>
                        <th>@lang('commonField.extra.sn')</th>
                        <th>खोप लगाएको</th>
                        <th>बालबालिकाको</th>
                        <th>खोपहरु</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="input_width">
                        <?php   
                            $count =1;
                            foreach($getMyFamilyDetails as $infantName){
                                $infantName = getParentsName($infantName->relation_id);
                                $infantAge = convertMyAge($infantName['dobAD']);
                                // dd($infantAge);

                                $upDateInfantVac = moreHouseDetailsFormUpdate($compareKey = $infantName->id, $jsonData = $updateInfantVac, $compareJsonKey = 'infantId');

                                $infantNameList =  $lan =='nep' ? $infantName->fnameNep .''. $infantName->mnameNep .' '.$infantName->lnameNep :  $infantName->fnameEng .''. $infantName->mnameEng .' '.$infantName->lnameEng;

                                if($infantAge < 1 && $infantAge !=='NULL'){ ?>
                                <tr>
                                   <td>
                                       {{ $count ++ }}                                    
                                   </td>
                                   <td>
                                       <input type="checkbox" class="haveVaccine"  name="infantVac[<?php echo $count  ?>][haveVaccine]" value="1" {{ $upDateInfantVac['haveVaccine'] ==1 ? 'checked' : '' }}>
                                   </td>

                                   <td>
                                       {{ $infantNameList }}
                                       <input type="hidden" name="infantVac[<?php echo $count  ?>][infantId]" value="{{ $infantName->id }}">
                                   </td>
                                   <td>
                                    <div class="row">
                                        <div class="col-md-12 lblPadding">
                                             
                                         @if($vaccineName)
                                            @foreach($vaccineName as $vaccine)
                                              <label class="radio-inline">
                                                    {{ $vaccine->nameNep }}
                                                      <input  type="number" name="infantVac[<?php echo $count  ?>][times][<?php echo $vaccine->nameNep  ?>]" 
                                                       placeholder="time of">
                                              </label> 
                                            @endforeach
                                          @endif 
                                        </div>
                                    </div>
                                   </td>
                                <?php
                               }

                            }                       
                          ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="msgDisplay"></div>
  </div>
</div>