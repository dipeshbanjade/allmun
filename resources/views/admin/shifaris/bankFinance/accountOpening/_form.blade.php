<div class="row top-part">
  <div class="col-md-6">
   <p align="left">   
    <b>@lang('commonField.extra.refCode'):</b>
    @if(isset($data->refCode))
    {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
    @else
    {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
    @endif
  </p>

  <p align="left">
    <b> च. नं.:
      {{ Form::text('chalaniNum', null, ['class'=>'shifaris_input', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
    </b>
  </p>
</div>
<div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    <h4 align="center">
      <b>विषय:नयाँ खाता खोली दिने सम्बन्धमा।</b>
    </h4>
    {{ Form::hidden('letter_subject', 'गैर नाफामूलक संस्था दर्ता प्रमाण पत्र', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left"> 
      <b>श्री 
        {{ Form::text('nameOfOffice', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'nameOfOffice', 'required' => 'required']) }}
      </b>,
    </p>
    <p align="left">
      <b> 
       {{ Form::text('locationOfOffice', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'locationOfOffice', 'required' => 'required']) }}।
     </b>
   </p>
 </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">
    <p>प्रस्तुत विषयमा
     <b> 
       {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
     </b> वडा नं.
     <b> 
       {{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNum', 'required' => 'required']) }}
     </b>
     (साविक 
     <!--  -->
     {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationAddress', 'required' => 'required']) }}
     <!--  -->
     <b>
       <select onchange="changeSelect(this)" name="organizationType">
        <option value="गा.वि.स." 
         {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
         <option value="नगरपालिका" 
         {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
         <option value="उप महानगरपालिका" 
         {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
         <option value="महानगरपालिका" 
         {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
      </select>
    </b>
    , वडा नं.  
    {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationWard', 'required' => 'required']) }}
    )    
    द्धारा संचालन हुने  
    {{ Form::text('workName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'workName', 'required' => 'required']) }}
    कार्यको लागि निम्न उल्लेखित व्यक्तिहरुको नाममा संयुक्त खाता संचालन  गरी दिनुहुन अनुरोध गरिन्छ।
  </p> 
</div>
</div> 
<h4 align="center"><b>व्यक्तिहरुको विवरण</b> </h4>
<div class="col-md-12">
  <div class="table-responsive">
    <table class="table table-bordered" id="house_kayem_table">
      <thead>
        <tr>
          <th width="2%">क्र.स.</th>
          <th>व्यक्तिको नाम </th>
          <th>पद </th>
          <th>कैफियत </th>
        </tr>
      </thead>
      <tbody>
        <?php
          $count = 1;
          $accountOpeningDetail = !empty($data) ? json_decode($data->accountOpeningDetail,true):[1];
        ?>
        @foreach ($accountOpeningDetail as $singleData)
          <tr>
            <td>{{$count++}}</td>
            <td> {{ Form::text('personName[]', !empty($singleData['personName'])? $singleData['personName'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'personName', 'required' => 'required']) }}</td>
            <td> {{ Form::text('personPost[]', !empty($singleData['personPost'])? $singleData['personPost'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'personPost', 'required' => 'required']) }}</td>
            <td>{{ Form::text('remarks[]', !empty($singleData['remarks'])? $singleData['remarks'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'remarks', 'required' => 'required']) }} </td>

            @if ($count==2)
              <td class="add-btns" style="border: 0px;"><button type="button" id="addTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button></td>
              {{-- expr --}}
            @else
              <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
            @endif
          </tr>
          {{-- expr --}}
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="text-right btm-last">
    <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required" value="{{ !empty($data)? $data->authorizedPerson: NULL}}"></p>
    <p> <b>
      <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
        @if(count($deginationsId) > 0)
        @foreach($deginationsId as $deg)
        <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
          {{ $deg->nameNep }} 
        </option>
        @endforeach
        @endif
      </select>
    </b></p>
  </div>
  <!--views for nibedak detail -->
  <div class="clearfix"></div>
  <hr>
  @include('admin.shifaris.nibedakCommonField')

  <!-- END -->
</div>
<div class="col-md-12">
  <hr>
</div>
</div>
</div>
</div>