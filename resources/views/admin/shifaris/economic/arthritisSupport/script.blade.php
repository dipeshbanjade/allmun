<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardarthritisSupport']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 1,
					maxlength : 30
				},
				officeName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				officeAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityName : {
					required : true,
					minlength : 5,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				organizationType : {
					required : true
				},
				organizationWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				patientPrefix : {
					required : true
				},
				patientName : {
					required : true,
					minlength : 5,
					maxlength : 30
				},
				disease : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				hospital : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				officeName : "Office Name is required",
				officeAddress : "office Address is required",
				municipalityName : "Municipality name is required",
				chalaniNumber : "Chalani Number is required",
				wardNumber : "Ward Number is required and numeric",
				organizationWard : "Organization Ward must be number",
				organizationAddress : "Organization Address is required",
				organizationType : "Organization Type is required",
				patientPrefix : "Patient Prefix is required",
				patientName : "Patient Name is required",
				disease : "Disease is required",
				hospital : "Hospital is required",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});
	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='patientName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	                  // $("input[name='fatherName']").trigger('change');
	            
	          },complete:function(){
	              // setTimeout(location.reload.bind(location), 1500);
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});

</script>