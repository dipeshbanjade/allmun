<?php

namespace App\Http\Controllers\admin\dartachallani;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\dartaChalani\Chalani;
use App\Http\Requests\admin\dartaChalani\chalaniVal;

use App\model\admin\dartaChalani\Darta;
use App\model\admin\otherOffice\otherOffice;
use App\model\setting\Department;
use App\model\setting\Degination;
use App\model\admin\ward\WardStaff;
use App\model\admin\municipility\StaffMunicipility;

use App\model\mylogic;
use Validator;
use Auth;
use DB;

class dartachalaniController extends Controller
{
    protected $challaniViewPath = 'admin.dartaChalani.chalani';
    protected $dartaViewpath    = 'admin.dartaChalani.darta';
    protected $chalaniTable     = 'chalanis';
    protected $dartaTable       = 'dartas';
    protected $dartaImagePath;
    protected $chalaniImagePath;

    public function __construct()
    {
      $this->middleware('auth');
      $this->dartaImagePath = config('activityMessage.dartaImagePath');
      $this->chalaniImagePath = config('activityMessage.chalaniImagePath');

    }

    public function challaniList()   // list of challani
    {
        $page['page_title']       = 'Challani List';
        $page['page_description'] = 'List of all challani';

        if (Auth::user()->userLevel == 'mun') {
            $companyList = otherOffice::where(['status' => 0, 'softDelete' => 0])->whereNotNull('municipilities_id')->orderBy('nameEng', 'ASC')->get();

            $challaniList = Chalani::with('otherOffices')->where(['status' => 0, 'softDelete' => 0])->whereNotNull('municipilities_id')->orderBy('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

            return view($this->challaniViewPath . '.index', compact(['page', 'challaniList', 'companyList']));
        }
        /*----------------------------------------------------*/
        $companyList = otherOffice::where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->orderBy('nameEng', 'ASC')->get();

        $challaniList = Chalani::with('otherOffices')->where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->orderBy('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

        return view($this->challaniViewPath . '.index', compact(['page', 'challaniList','companyList']));
    }


    public function storeChallani(chalaniVal $request)
    {
        DB::beginTransaction();
        $authUsr = Auth::user()->userLevel;
        try {
        $save  = new Chalani;
        if ($authUsr =='mun') {
          $save->municipilities_id = Auth::user()->municipilities_id;
        }
        if ($authUsr =='wrd') {
          $save->wards_id       = Auth::user()->wards_id;
        }

        if($files = $request->file('chalaniImage')){
           $images=[];
            foreach($files as $file){
                $name = str_replace(' ', '', 'chalani'.mylogic::getRandNumber()). '.' . $file->getClientOriginalExtension();
                $filePath = $this->chalaniImagePath.$name;
                array_push($images, [
                  'chalaniImage' => $filePath
                  ]);
                $file->move($this->chalaniImagePath,$name);
            }
            $save->imagePath = json_encode($images);
        }

        $save->users_id   = Auth::user()->id;

        $save->chalaniNo        = $request->chalaniNo;
        $save->chalaniDate      = $request->chalaniDate;
        $save->chalaniPage      = $request->chalaniPage;

        $save->chalaniPageDate  = $request->chalaniPageDate;
        $save->chalaniSubject   = $request->chalaniSubject;

        $save->other_offices_id = $request->other_offices_id;
        $save->chalaniTicket    = $request->chalaniTicket;

        $save->chalaniKaifiyat    = $request->chalaniKaifiyat;
        $save->chalaniStorePlace    = $request->chalaniStorePlace;
        $save->status     = 0;
        $save->softDelete = 0;
        $mySave = $save->save();

        if ($mySave) {
            $save->idEncript = mylogic::idEncription($save->id);
            $save->save();
        }
         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createdChalani') . $request->chalaniSubject, $tableName = $this->chalaniTable, $tblId = $save->id);

         if ($mySave && $log) {
             DB::commit();
             return redirect()->route('challaniIndex')->withMessage(config('activityMessage.saveMessage') .$request->chalaniNo);
             }else{
                return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
    }

    public function chalaniDetails($slug){
       if (!$slug) {
        return back()->withMessage(config('activityMessage.idNotFound'));
       }
           $page['page_title']       = 'Chalani Details';
           $page['page_description'] = 'chalani details'; 

           $data =  Chalani::where(['idEncript'=>$slug, 'status' => 0, 'softDelete' => 0])->first();
           return view($this->challaniViewPath . '.show', compact(['page', 'data']));
       
    }
    /**/

    public function editChalani($slug)   // edit chalani 
    {
        $page['page_title']       = 'Chalani Edit';
        $page['page_description'] = 'edit chalani details'; 
        if ($slug) {
          $data = Chalani::where(['idEncript'=>$slug])->first();
          $companyList = otherOffice::where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->orderBy('nameEng', 'ASC')->get();
          if ($data) {
             return view($this->challaniViewPath . '.edit', compact(['page', 'data', 'companyList']));
          }else{
            return back()->withMessage(config('activityMessage.idNotFound'));
          }
        }
    }
    /*-------------------*/
    public function updateChallani(Request $request, $id)   // update chalani
    {
      if ($id) {
         $validator = Validator::make($request->all(), [
                         'chalaniDate' => 'required',
                         'chalaniNo' => 'required|min:3|max:100|unique:chalanis,chalaniNo,'.$request->id,
              ]);
              if ($validator->fails()) {
                 return back()->withErrors($validator)->withInput();
              }  
         DB::beginTransaction();
         try {
          $update =  Chalani::findOrFail($id);
          $update->chalaniDate     = $request->chalaniDate;
          $update->chalaniNo       = $request->chalaniNo;
          $update->chalaniPage     = $request->chalaniPage;
          $update->chalaniPageDate = $request->chalaniPageDate;
          $update->chalaniStorePlace = $request->chalaniStorePlace;
          $update->other_offices_id  = $request->other_offices_id;
          $update->chalaniTicket     = $request->chalaniTicket;
          $update->chalaniKaifiyat   = $request->chalaniKaifiyat;
          $update->chalaniSubject   = $request->chalaniSubject;

          if($files = $request->file('chalaniImage')){
             $images=[];
              foreach($files as $file){
                  $name = str_replace(' ', '', 'chalani'.mylogic::getRandNumber()). '.' . $file->getClientOriginalExtension();
                  $filePath = $this->chalaniImagePath.$name;
                  array_push($images, [
                    'chalaniImage' => $filePath
                    ]);
                  $file->move($this->chalaniImagePath,$name);
              }
              $update->imagePath = json_encode($images);
          }

          $myUpdate = $update->update();

          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateMessage') .  $request->chalaniNo, $tableName = $this->chalaniTable, $tblId = $update->id);

          if ($myUpdate && $log) {
              DB::commit();
              return redirect()->route('challaniIndex')->withMessage(config('activityMessage.updateMessage'));
              }
              else{
                 return back()->withMessage(config('activityMessage.notUpdated'));
             }
         }catch (Exception $e) {
              DB::rollback();
              return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
      }

    }

    public function deleteChallani($id)
    {
       if ($id) {    
          DB::beginTransaction();
          try {
          $delChalani = Chalani::findOrFail($id);
           $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.deleteMessage') . $delChalani->chalani, $tableName = $this->chalaniTable, $tblId = $delChalani->id);
          $deleteMe  = $delChalani->delete();

           if ($deleteMe && $log) {
               DB::commit();
               return redirect()->route('challaniIndex')->withMessage(config('activityMessage.saveMessage'));
               }else{
                  return back()->withMessage(config('activityMessage.unSaveMessage'));
              }
          }catch (Exception $e) {
              DB::rollback();
              return back()->withMessage(config('activityMessage.dataNotInserted'));
          }
          
       }
    }

    /*---------------darta----------------------------*/
    public function dartaIndex(){
      $page['page_title']       = 'Darta List';
      $page['page_description'] = 'List of all darta challani';
      $department = Department::where(['status'=> 0, 'softDelete'=>0])->get();
      $post = $this->getPostList();
      if (Auth::user()->userLevel == 'mun') {
        $companyList = otherOffice::where(['status' => 0, 'softDelete' => 0])->whereNotNull('municipilities_id')->orderBy('nameEng', 'ASC')->get();
          $dartaList = Darta::with('otherOffices')->where(['status' => 0, 'softDelete' => 0])->whereNotNull('municipilities_id')->orderBy('created_at', 'DESC')->paginate(config('activityMessage.pagination'));
          return view($this->dartaViewpath . '.index', compact(['page', 'dartaList', 'companyList', 'department', 'post']));
      }
      /*----------------------------------------------------*/
      $companyList = otherOffice::where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->orderBy('nameEng', 'ASC')->get();

      $dartaList = Darta::with('otherOffices')->where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->orderBy('created_at', 'DESC')->paginate(config('activityMessage.pagination'));
      return view($this->dartaViewpath . '.index', compact(['page', 'dartaList', 'companyList', 'department', 'post']));
    }


    protected function getPostList(){
      return Degination::where(['status'=> 0, 'softDelete'=>0])->get();
    }

    public function editDarta($slug){   // edit darta 
      $page['page_title']       = 'Edit darta';
      $page['page_description'] = 'edit darta details';
      if ($slug) {
      $post = $this->getPostList();
        if (Auth::user()->userLevel == 'mun') {
           $companyList = otherOffice::where(['status' => 0, 'softDelete' => 0])->whereNotNull('municipilities_id')->orderBy('nameEng', 'ASC')->get();

        }
        if (Auth::user()->userLevel == 'wrd') {
          $companyList = otherOffice::where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->orderBy('nameEng', 'ASC')->get();
        }
        $data = Darta::where(['idEncript'=>$slug])->first();
        
        $department = Department::where(['status'=> 0, 'softDelete'=>0])->get();
        if ($data) {
           return view($this->dartaViewpath . '.edit', compact(['page', 'data', 'companyList', 'department', 'post']));
        }else{
          return back()->withMessage(config('activityMessage.idNotFound'));
        }
      }
    }

    public function dartaDetails($slug){
      if (!$slug) {
       return back()->withMessage(config('activityMessage.idNotFound'));
      }
          $page['page_title']       = 'darta Details';
          $page['page_description'] = 'darta details'; 

          $data =  Darta::where(['idEncript'=>$slug, 'status' => 0, 'softDelete' => 0])->first();
          return view($this->dartaViewpath . '.show', compact(['page', 'data']));
    }

    public function storeDarta(Request $request){  // save darta 
      $validator = Validator::make($request->all(), [
          'dartaNo' => 'required|min:3|max:60|unique:dartas',
          'dartaDate'=> 'required',
          'other_offices_id' => 'required'
        ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }

      DB::beginTransaction();
      $authUsr = Auth::user()->userLevel;
      try {
      $save  = new Darta;
      if ($authUsr =='mun') {
        $save->municipilities_id = Auth::user()->municipilities_id;
      }
      if ($authUsr =='wrd') {
        $save->wards_id       = Auth::user()->wards_id;
      }
      
       if($files = $request->file('dartaImage')){
          $images=[];
           foreach($files as $file){
               $name = str_replace(' ', '', 'darta_'.mylogic::getRandNumber()). '.' . $file->getClientOriginalExtension();
               $filePath = $this->dartaImagePath.$name;
               array_push($images, $filePath);
               $file->move($this->dartaImagePath,$name);
           }
           $save->imagePath = json_encode($images);
       }

      $save->users_id   = Auth::user()->id;
      $save->dartaNo        = $request->dartaNo;
      $save->dartaDate      = $request->dartaDate;
      $save->dartaPage      = $request->dartaPage;
      $save->dartaPageDate  = $request->dartaPageDate;
      $save->other_offices_id  = $request->other_offices_id;
      $save->dartaSubject   = $request->dartaSubject;
      $save->dartaIsFor        = $request->dartaIsFor; 
      $save->dartaReceiveDate  = $request->dartaReceiveDate;
      $save->dartaKaifiyat    = $request->dartaKaifiyat;
      $save->dartaStorePlace    = $request->dartaStorePlace;
      $save->departments_id    = $request->departments_id;
      $save->dartaReciverDate  = $request->dartaReciverDate;
      $save->status     = 0;
      $save->softDelete = 0;
      $mySave = $save->save();

      if ($mySave) {
          $save->idEncript = mylogic::idEncription($save->id);
          $save->save();
      }
       $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createdDarta') . $request->dartaSubject, $tableName = $this->dartaTable, $tblId = $save->id);

       if ($mySave && $log) {
           DB::commit();
           return redirect()->route('dartaIndex')->withMessage(config('activityMessage.saveMessage') .$request->chalaniNo);
           }else{
              return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
      }catch (Exception $e) {
          DB::rollback();
          return back()->withMessage(config('activityMessage.dataNotInserted'));
      }
    }

    public function showDarta($slug){

    }

    public function updateDarta(Request $request, $id){
      $validator = Validator::make($request->all(), [
          'dartaNo' => 'required|min:3|max:100|unique:dartas,dartaNo,'.$request->id,
          'dartaDate'=> 'required',
          'other_offices_id' => 'required'
        ]);

      if ($validator->fails()) {
        return back()->withErrors($validator)->withInput();
      }
      DB::beginTransaction();
      $authUsr = Auth::user()->userLevel;
      try {
      $update  = Darta::findOrFail($id);
      if ($authUsr =='mun') {
        $update->municipilities_id = Auth::user()->municipilities_id;
      }
      if ($authUsr =='wrd') {
        $update->wards_id       = Auth::user()->wards_id;
      }
      if($files = $request->file('dartaImage')){
         $images=[];
          foreach($files as $file){
              $name = str_replace(' ', '', 'darta_'.mylogic::getRandNumber()). '.' . $file->getClientOriginalExtension();
              $filePath = $this->dartaImagePath.$name;
              array_push($images, $filePath);
              $file->move($this->dartaImagePath,$name);
          }
          $update->imagePath = json_encode($images);
      }

      $update->users_id   = Auth::user()->id;
      $update->dartaNo        = $request->dartaNo;
      $update->dartaDate      = $request->dartaDate;
      $update->dartaPage      = $request->dartaPage;
      $update->dartaPageDate  = $request->dartaPageDate;
      $update->other_offices_id  = $request->other_offices_id;
      $update->dartaSubject   = $request->dartaSubject;
      $update->dartaIsFor        = $request->dartaIsFor; 
      $update->dartaReceiveDate  = $request->dartaReceiveDate;
      $update->dartaKaifiyat    = $request->dartaKaifiyat;
      $update->dartaStorePlace    = $request->dartaStorePlace;
      $update->departments_id    = $request->departments_id;
      $update->dartaReciverDate  = $request->dartaReciverDate;
      $update->status     = 0;
      $update->softDelete = 0;
      $myUpdate = $update->update();

     $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateDarta') . $request->update, $tableName = $this->dartaTable, $tblId = $update->id);

       if ($myUpdate && $log) {
           DB::commit();
           return redirect()->route('dartaIndex')->withMessage(config('activityMessage.updateMessage') .$request->dartaNo);
           }else{
              return back()->withMessage(config('activityMessage.notUpdated'));
          }
      }catch (Exception $e) {
          DB::rollback();
          return back()->withMessage(config('activityMessage.dataNotInserted'));
      }

    }
    /*----------------*/

    public function deleteDarta($id){
      if ($id) {    
         DB::beginTransaction();
         try {
         $delDarta = Darta::findOrFail($id);
          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.deleteMessage') . $delDarta->dartaNo, $tableName = $this->dartaTable, $tblId = $delDarta->id);
         $deleteMe  = $delDarta->delete();

          if ($deleteMe && $log) {
              DB::commit();
              return redirect()->route('dartaIndex')->withMessage(config('activityMessage.saveMessage'));
              }else{
                 return back()->withMessage(config('activityMessage.unSaveMessage'));
             }
         }catch (Exception $e) {
             DB::rollback();
             return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
         
      }
    }

    public function getStaffByDepartmentWise($id){   // getStaff according to departmentwise
       if ($id) {
           $usrLevel = Auth::user()->userLevel;
           if ($usrLevel == 'mun') {
             $staffDetails = StaffMunicipility::select('firstNameEng','middleNameEng','lastNameEng','firstNameNep','middleNameNep','lastNameNep', 'id', 'departments_id')->where(['departments_id'=>$id, 'status'=>0, 'softDelete'=>0])->get();
              
              if ($staffDetails) {
                  return response()->json([
                      'success' => true,
                      'data'    => $staffDetails
                   ]);
              }else{
                  return response()->json([
                      'success' => false,
                      'message' => config('activityMessage.dataNotFound')
                   ]);
              }
           }
           /*ward staff details*/
           if ($usrLevel == 'wrd') {
             $staffDetails = WardStaff::select('firstNameEng','middleNameEng','lastNameEng','firstNameNep','middleNameNep','lastNameNep', 'id', 'departments_id')->where(['departments_id'=>$id, 'status'=>0, 'softDelete'=>0])->get();
              
              if ($staffDetails) {
                  return response()->json([
                      'success' => true,
                      'data'    => $staffDetails
                   ]);
              }else{
                  return response()->json([
                      'success' => false,
                      'message' => config('activityMessage.dataNotFound')
                   ]);
              }
           }
       }
    }
}
