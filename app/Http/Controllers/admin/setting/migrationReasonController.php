<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\MigrationReason;
use App\model\mylogic;
use Validator;
use App\Http\Requests\admin\setting\MigrationReason\MigrationReasonValidation;

class migrationReasonController extends Controller
{
  
     protected $viewPath  = 'admin.setting.migrationReason';
    protected $migrationReasonTbl = 'migration_reasons';
    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.migrationReasonPrefix');
    }

    public function index()
    {
        $page['page_title']       = 'Migration Reason';
        $page['page_description'] = 'list of all Migration';

        $allMigrationReason = MigrationReason::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        $prefix   = myLogic::sysRefCode($tablename = $this->migrationReasonTbl, $prefix = $this->prefix);
        return view($this->viewPath . '.index', compact(['page', 'allMigrationReason', 'prefix'])); 
    }


    public function store(MigrationReasonValidation $request)
    {
        $mySave = MigrationReason::create([
          'refCode' => $request->refCode,
          'nameNep' => $request->nameNep ?? 'N/A',
          'nameEng' => $request->nameEng,
          'desc'    => $request->desc,
          'status'  => 0,
          'softDelete' => 0
          ]);
        if ($mySave) {
         return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
     }else{
         return back()->withMessage(config('activityMessage.unSaveMessage'));
     }
 }

 public function edit($id)
 {
   $data = MigrationReason::findOrFail($id);
   if ($data) {
    return response()->json([
      'success' => true,
      'refCode' => $data->refCode,
      'id'      => $data->id,
      'nameNep' => $data->nameNep,
      'nameEng' => $data->nameEng,
       'desc'    => $data->desc
      ]);
}else{
    return response()->json([
      'success' => false,
      'message' => config('activityMessage.dataNotFound')
      ]);
}
}


public function update(Request $request, $id)
{
  $validator = Validator::make($request->all(), [
     'nameEng'  => 'required|min:3|max:50',
     'nameNep'  => 'min:3|max:50',
      'desc'    => 'required|min:20|max:2000'
     ]);

  if ($validator->fails()) {
      return response()->json([
       'success' => false,
       'message' => $validator->errors()->all()
       ]);
  }
  $update          = MigrationReason::findOrFail($id);
  $update->nameNep = $request->nameNep ?? 'N/A';
  $update->nameEng = $request->nameEng;
  $update->desc = $request->desc;

  $myUpdate = $update->update();
  if ($myUpdate) {
    return response()->json([
      'success' => true,
      'message' => config('activityMessage.updateMessage')
      ]);
}else{
    return response()->json([
      'success'  => false,
      'message'  => config('activityMessage.notUpdated')
      ]);
}
}

public function destroy($id)
{
    $del = MigrationReason::findOrFail($id);
    $del->softDelete = 1;
    $myDel = $del->update();
    if ($myDel) {
       return MyLogic::successChangeStatus($code = 200);
   }else{
      return MyLogic::successChangeStatus($code = 301);
  }
}
}
