<?php

namespace App\Http\Controllers\admin\setting\houseSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseSetting\swamipto;
use App\model\mylogic;
use App\Http\Requests\admin\setting\swamipto\SwamiptoValidation;
use Validator;
class swamiptoController extends Controller
{
       protected $viewPath  = 'admin.setting.swamipto';
       protected $swamiptoTbl = 'swamiptos';
       protected $prefix;

       public function __construct()
       {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.swamiptoPrefix');
    }

    public function index()
    {
        $page['page_title']       = 'Swamipto';
        $page['page_description'] = 'list of all Swamipto';

        $allSwamipto = swamipto::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        return view($this->viewPath . '.index', compact(['page', 'allSwamipto', 'prefix'])); 
    }

    public function store(SwamiptoValidation $request)
    {
        $prefix   = myLogic::sysRefCode($tablename = $this->swamiptoTbl, $prefix = $this->prefix);

           $mySave = swamipto::create([
             'refCode' => $prefix,
             'nameNep' => $request->nameNep ?? 'N/A',
             'nameEng' => $request->nameEng,
             'tagName' => $request->tagName,
             'status'  => 0,
             'softDelete' => 0
             ]);
           if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }


    public function edit($id)
    {
        $data = swamipto::findOrFail($id);
             if ($data) {
                return response()->json([
                  'success' => true,
                  'refCode' => $data->refCode,
                  'id'      => $data->id,
                  'nameNep' => $data->nameNep,
                  'nameEng' => $data->nameEng,
                  'tagName' => $data->tagName,
                  ]);
            }else{
                return response()->json([
                  'success' => false,
                  'message' => config('activityMessage.dataNotFound')
                  ]);
            }
    }


    public function update(Request $request, $id)
    {
           $validator = Validator::make($request->all(), [
               'nameEng'  => 'required|min:4|max:50',
               'nameNep'  => 'min:4|max:50',
               ]);

           if ($validator->fails()) {
              return response()->json([
                 'success' => false,
                 'message' => $validator->errors()->all()
                 ]);
          }
          $update          = swamipto::findOrFail($id);
          $update->nameNep = $request->nameNep ?? 'N/A';
          $update->nameEng = $request->nameEng;
          $update->tagName = $request->tagName;

          $myUpdate = $update->update();
          if ($myUpdate) {
            return response()->json([
              'success' => true,
              'message' => config('activityMessage.updateMessage')
              ]);
        }else{
            return response()->json([
              'success'  => false,
              'message'  => config('activityMessage.notUpdated')
              ]);
        }
    }

    public function destroy($id)
    {
         $del = swamipto::findOrFail($id);
         $del->softDelete = 1;
         $myDel = $del->update();
         if ($myDel) {
             return MyLogic::successChangeStatus($code = 200);
         }else{
            return MyLogic::successChangeStatus($code = 301);
        }
    }
}
