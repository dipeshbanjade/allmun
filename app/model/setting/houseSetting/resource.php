<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class resource extends Model
{
    protected $table = 'resources';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
