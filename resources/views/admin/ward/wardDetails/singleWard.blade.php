@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
    <div class="card-body">

    <div class="munCreate">
          @if(!count($wardDetails) > 0)
              <button class="btn btn-primary pull-right" onclick="myFunction()">
              <i class="fa fa-plus"></i>&nbsp;@lang('commonField.extra.createMunicipility')</button> 
          <h1 class="fa fa-plus fontColorBlack">
            @lang('commonField.extra.createMunicipility')
          </h1>
             @else
                  <div class="card-head">
                      <header><i class="fa fa-eye">&nbsp;@lang('commonField.extra.muncipility')</i></header>
                  </div>
             @endif
    </div>
          @if(count($wardDetails) > 0)
            <div class="card-body row">
              <div style="margin: 0 auto;" class="col-sm-8 row paddingImg">
                <div class="alert alert-default tableContent col-sm-10 row profileImg ">
                  <div class="col-sm-8 " style="padding-left: 8em;">
                    <img src="{{ isset($wardDetails) ? asset($wardDetails->munLogo) : asset('image/defaultUser.png') }}" width="150" height="150" class="img-circle user-img-circle" alt="User Image" />
                    <h3 align="center" class="fontColorBlack"><small>Ref Code : {{ $wardDetails->uniqueCode }}</small>
                    <br>
                    {{ $wardDetails->nameNep }} - {{ $wardDetails->nameEng }}  
                    </h3>
                  </div>
                  <div class="clearfix"></div>
                  <table class="table table-striped">
                    <tr>
                      <th>@lang('sidebarMenu.system_setting.municipility')</th>
                      <td>{{ $wardDetails->nameNep }} - {{ $wardDetails->nameEng }}</td>
                    </tr>

                    <tr>
                      <th>@lang('commonField.address_information.phone')</th>
                      <td>{{ $wardDetails->landLineNumber }} &nbsp; @lang('commonField.address_information.faxNumber') : {{ $wardDetails->faxNumber }}</td>
                    </tr>
                    
                    <tr>
                        <th>@lang('commonField.address_information.province')</th>
                        <td>{{ $wardDetails->provinces_id }}</td>
                    </tr>

                    <tr>
                        <th>@lang('commonField.address_information.district')</th>
                        <td>{{ $wardDetails->districts_id }}</td>
                    </tr>


                    <tr>
                      <th>@lang('commonField.address_information.address')</th>
                      <td>
                        {{ $wardDetails->addrNep }}   - {{ $wardDetails->addrEng }}  
                      </td>
                    </tr>
                    <tr>
                      <th>@lang('commonField.extra.muncipilityHead')</th>
                      <td>
                        {{ $wardDetails->muncipilityHead }} 
                      </td>
                    </tr>

                  </table>
                  
                <hr>
                <p class="pull-right">
                <button type="button" class="btn btn-danger pull-right" onclick="history.back()"><i class="fa fa-arrow-left">Back</i>
                </button>
                <a href="{{ route('wardDetails.edit', $wardDetails->id) }}" type="button" class="btn btn-success pull-right">
                    <i class="fa fa-edit">Edit</i>
                </a>
                </p>
              </div>
            </div>
            </div>
           @else
          <h3 align="center" class="fontColorBlack">
            @lang('commonField.extra.createMunicipilityFirst')
          </h3><hr>
          @endif
        </div>
      </div>
    </div>

</div>
@endsection
