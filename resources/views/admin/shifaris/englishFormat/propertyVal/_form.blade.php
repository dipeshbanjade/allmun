<div class="right_col eng" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x eng">Property Valuation</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
          @include('admin.shifaris.municipalityDetail')
          <div class="row top-part">
            <div class="col-md-6 ps-cn">
              <p><b class="ps">प. सं.:</b>
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
                @endif
              </p>
              <p align="left" class="star">
                <b class="cn">च. नं.:</b>
                {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
              </p>
            </div>
            <div class="col-md-6">
              <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
            </div>
          </div>
          <div class="row top-part">
            <div class="col-md-12">
             <h3 align="center"><b>  Subject:Property Valuation </b></h3>
             {{ Form::hidden('letter_subject', 'Property Valuation') }}
             <h4 align="center"><b> To Whom It May Concern</b></h4>
           </div>
         </div>
         <div class="row">
          <div class="col-md-12 content-para">
            <p align="left">This is to certify that <b> <select onchange="changeSelect(this)" name="title">
              <option value="Mr" {{(!empty($data) && $data->title == "Mr") ? 'selected' : ''}}>Mr</option>
              <option value="Mrs"  {{(!empty($data) && $data->title == "Mrs") ? 'selected' : ''}}>Mrs</option>
              <option value="Miss" {{(!empty($data) && $data->title == "Miss") ? 'selected' : ''}}>Miss</option>
            </select>
          </b> {{ Form::text('personName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
          permanent resident of <b>{{ Form::text('municipality', isset($data->municipality) ? $data->municipality : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b>, Ward No <b>{{ Form::text('ward', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>,<b>{{ Form::text('district', isset($data->district) ? $data->district : getChecker()->districts->districtNameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b> Nepal(
          <select onchange="changeSelect(this)" name="former">
            <option value="Former" {{(!empty($data) && $data->former == 'Former') ? 'selected' : ''}}>Former</option>
            <option value="Previously designated as" {{(!empty($data) && $data->former == 'Previously designated as') ? 'selected' : ''}}>Previously designated as</option>
          </select>
          <b>
            <!--  -->
            {{ Form::text('orgAddr', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'orgAddr']) }}
            <!--  -->
          </b><b> <select onchange="changeSelect(this)" name="orgType">
            <option value="V.D.C" {{(!empty($data) && $data->orgType == "V.D.C")? 'selected' :''}}>V.D.C</option>
            <option value="Municipality" {{(!empty($data) && $data->orgType == "Municipality")? 'selected' :''}}>Municipality</option>
            <option value="Sub- Metropolitian" {{(!empty($data) && $data->orgType == "Sub- Metropolitian")? 'selected' :''}}>Sub- Metropolitian</option>
            <option value="Metropolitian" {{(!empty($data) && $data->orgType == "Metropolitian")? 'selected' :''}}> Metropolitian</option>
         
          </select>
        </b>, Ward No. {{ Form::text('orgWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}) owns following values of property as mentioned below.
      </p>
    </div>
    <div class="col-md-12">
      <h4><b>PROPERTY VALUATION</b></h4>
      <div class="table-responsive">
        <table class="table table-bordered" id="property_table">
          <thead>
            <tr><th>S.N.</th>
              <th>Property Type </th>
              <th>Owner</th>
              <th>Location</th>
              <th>Plot No.</th>
              <th>Area </th>
              <th>Total Value </th>
            </tr></thead>
            <tbody>
              <?php
                $count=1;
                $propertyStatement = !empty($data) ? json_decode($data->propertyStatement,true) : [1];
              ?>
              @foreach ($propertyStatement as $singleData)
                <tr>
                  <td>{{$count++}}</td>
                  <td>{{ Form::text('property[]', !empty($singleData['property']) ? $singleData['property'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                  <td>{{ Form::text('owner[]', !empty($singleData['owner']) ? $singleData['owner'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                  <td>{{ Form::text('location[]', !empty($singleData['location']) ? $singleData['location'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                  <td>{{ Form::text('plotNo[]', !empty($singleData['plotNo']) ? $singleData['plotNo'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                  <td>{{ Form::text('area[]', !empty($singleData['area']) ? $singleData['area'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                  <td>{{ Form::text('totalValue[]', !empty($singleData['totalValue']) ? $singleData['totalValue'] : NULL , ['class'=>'dashed-input-field total_value', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalValue']) }}</td>
                  @if ($count == 2)
                    <td class="add-btns" style="border: 0px;"><a href="" id="add_more_property" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
                  @else
                    <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td> 
                  @endif
                </tr>

              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-12">
        <p> Total Valuation of property is NRs. {{ Form::text('totalPropertyValueInNrs', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'total_property_value_nrs']) }}<br> </p>
        <p>Note: 1
          <select onchange="changeSelect(this)" name="currencyType">
            <option value="USD($)" {{(!empty($data) && $data->currencyType == 'USD($)') ? 'selected' :''}}>USD($)</option>
            <option value="AUD($)" {{(!empty($data) && $data->currencyType == 'AUD($)') ? 'selected' :''}}>AUD($)</option>
            <option value="Pound(£)" {{(!empty($data) && $data->currencyType == 'Pound(£)') ? 'selected' :''}}>Pound(£)</option>
            <option value="Euro(€)" {{(!empty($data) && $data->currencyType == 'Euro(€)') ? 'selected' :''}}>Euro(€)</option>
          </select>
          equivalent to NPR. {{ Form::text('perDollarPriceNpr', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'per_dollar_price_npr']) }}<br>which is equivalent to {{ Form::text('ctype', 'USD($)' , ['class'=>'dashed-input-md-field', 'id' => 'ctype', 'readonly' => 'readonly']) }} {{ Form::text('totalPropertyValueInDollar', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'total_property_value_dollar']) }} </p>
          <p> (Source : Nepal Rastra Bank)</p>
        </div>
        <div class="col-md-12">
          <p>Note:<br>
            4 Daam(D) = 1 Paisa(P)<br>
            4 Paisa(P) = 1 Anna(A)<br>
            16 Anna(A) = 1 Ropani(R)<br>
            1 Anna(A) = 342.25 Sq Ft<br>
          </p>
          <div class="text-right btm-last">
            <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
            <p>   <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">
              <option value="">@lang('commonField.extra.deginations_id')</option>
              @if(count($deginationsId) > 0)  <!-- selecting leave type -->
              @foreach($deginationsId as $deg)
              <option value="{{ $deg->id }}" {{(!empty($data) && $data->deginations_id == $deg->id)? 'selected' :''}}>
                {{ $deg->nameNep }}
              </option>
              @endforeach
              @endif
            </select>
          </p>
        </div>
        <!--views for nibedak detail -->
        <div class="clearfix"></div>
        <hr>
        @include('admin.shifaris.nibedakCommonField')
        <!-- END -->
      </div>
    </div>
    <div class="col-md-12">
      <hr>
    </div>

  </div>
</div>
</div>
</div>
</div>