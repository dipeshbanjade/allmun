<?php

namespace App\Http\Controllers\admin\municipility;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\setLeave\leaveisFor;
use App\Http\Requests\admin\leaveIsFor\leaveIsForVal;
use App\model\mylogic;
use Auth;
use DB;

class LeaveIsForController extends Controller
{

    public $viewPath = 'admin.LeaveIsFor';
    public $leaveIsFor = 'leaveisfor';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $currDate = date('Y');
        $page['page_title'] = 'municipility : list';
        $page['page_description'] = 'list of all municipility leave';
        if (Auth::user()->userLevel == 'mun' || Auth::user()->userLevel == 'dev') {
           $leave = leaveisFor::select('dateFromEng', 'dateFromNep', 'leaveFor', 'id')->where(['status'=>0, 'softDelete'=>0])->where('dateFromEng', 'like', $currDate . '%')->whereNull('wards_id')->get();
        }else{
           $leave = leaveisFor::select('dateFromEng', 'dateFromNep', 'leaveFor', 'id')->where(['status'=>0, 'softDelete'=>0, 'wards_id'=>Auth::user()->wards_id])->where('dateFromEng', 'LIKE', $currDate.'%')->get();
        }
        return view($this->viewPath . '.index',compact('leave', 'page'));
    }

    public function create(){
        $page['page_title'] = 'municipility : list';
        $page['page_title'] = 'list of all municipility leave';
        return view($this->viewPath . '.create', compact(['page']));
    }

    public function store(leaveIsForVal $request){
      $hiddenId = $request->hiddenId;
      if ($hiddenId) {
        $dateFromNep = $request->dateFromNep;
        $dateFromEng = $request->dateFromEng;
        $leaveFor    = $request->leaveFor;
         $this->myUpdate($hiddenId, $dateFromNep, $dateFromEng, $leaveFor);
         return back()->withMessage(config('activityMessage.updateMessage'));
      }else{
       if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd' ||  Auth::user()->userLevel === 'mun') {
         DB::beginTransaction();
          try {
                  $save = new leaveisFor;
                  $save->dateFromNep = $request->dateFromNep;
                  $save->dateFromEng = $request->dateFromEng;
                  $save->leaveFor = $request->leaveFor;
                  $save->municipilities_id  = Auth::user()->municipilities_id;
                  $save->wards_id           = Auth::user()->userLevel == 'wrd' ? Auth::user()->wards_id : NULL;
                  $save->createdBy          = Auth::user()->id;
                  $mySave = $save->save();

                  $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createAction'), $tableName = $this->leaveIsFor, $tblId = $save->id);

                   if ($mySave && $log) {
                       DB::commit();
                      return back()->withMessage(config('activityMessage.saveMessage'));
                  }else{
                      return back()->withMessage(config('activityMessage.unSaveMessage'));
                  }
              }catch(Exception $e) {
                 DB::rollback();
                 return back()->withMessage(config('activityMessage.dataNotInserted'));
          }
        }
        
      }
    }

    public function edit($id){
      if ($id) {
         $leave = leaveisFor::findorfail($id);
         if ($leave) {
            return response()->json([
                  'success'     => true,
                  'id'          => $leave->id,
                  'dateFromEng' => $leave->dateFromEng,
                  'dateFromNep' => $leave->dateFromNep,
                  'leaveFor'    => $leave->leaveFor
              ]);
         }else{
          return response()->json([
              'success' => false,
              'message' => 'Oops id not found'
            ]);
         } 
      }
    }

    public function myUpdate($id, $dateFromNep, $dateFromEng, $leaveFor){
      if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd' ||  Auth::user()->userLevel === 'mun') {
        DB::beginTransaction();
         try {
                 $update = leaveisFor::findorfail($id);
                 $update->dateFromNep        = $dateFromNep;
                 $update->dateFromEng        = $dateFromEng;
                 $update->leaveFor           = $leaveFor;
                 $update->municipilities_id  = Auth::user()->municipilities_id;
                 $update->wards_id           = Auth::user()->userLevel == 'wrd' ? Auth::user()->wards_id : NULL;
                 $update->createdBy          = Auth::user()->id;
                 $myUpdate = $update->save();

                 $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateMessage'), $tableName = $this->leaveIsFor, $tblId = $update->id);

                  if ($myUpdate && $log) {
                      DB::commit();
                     return back()->withMessage(config('activityMessage.updateMessage'));
                 }else{
                     return back()->withMessage(config('activityMessage.unUpdateMessage'));
                 }
             }catch(Exception $e) {
                DB::rollback();
                return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
    }
  }

  public function destroy($id)
  {
    $delId = leaveisFor::findOrFail($id);
        $myDel = $delId->delete();
     if ($myDel) {
         return back()->withMessage(config('activityMessage.deleteMessage'));
         // return mylogic::successDeleteMessage($code = 200);
      }else{
           return back()->withMessage(config('activityMessage.unDeleteMessage'));
           // return mylogic::unSuccessDeleteMessage($code = 401);
      }
  }


}
