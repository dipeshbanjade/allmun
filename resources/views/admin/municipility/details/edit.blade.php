@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
   <div class="col-sm-12 col-md-12">
     <div class="card-box">
       <div class="card-head">
       	<header>
       			<h3>
              <i class="fa fa-edit">
            <span>
               @lang('commonField.extra.edit')
            </span></h3>
       		</i>
           <h3 class="pull-right">@lang('sidebarMenu.system_setting.municipility')</h3>
        </header>
       </div>

       <div class="card-body">
            {!! Form::model($data, ['method' => 'POST','route' => ['admin.mun-set.update', $data->id], 'files'=>true, 'name' => 'createMunFrm']) !!}
                 @include('admin.municipility.details._form')

                 <p class="pull-right">
                 	  <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
                 	      @lang('commonField.button.back')
                 	  </button>
                     {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success pull-right']) }}
                 </p>
            {{ Form::close() }}
       </div>
       </div>
     </div>
@endsection
@section('custom_script')
  @include('admin.municipility.details.script')
@endsection