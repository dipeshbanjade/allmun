@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
 <div class="card-box">
  <div class="card-body">
    <div class="card-head">
      <header><i class="fa fa-eye">
       &nbsp;@lang('commonField.houseHoldSetting.houseFoundation')</i>
     </header>
   </div>
   <button id="" class="btnAddhouseFoundation btn btn-primary pull-right">
     <i class="fa fa-plus"></i>
     <span> @lang('commonField.houseHoldSetting.createHouseFoundation')</span>
   </button>

   @if(count($allhouseFoundationType) > 0)
   <div class="table-scrollable" id="dropLabel">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
     <thead>
      <tr>
       <th>@lang('commonField.extra.sn')</th>
       <th>@lang('commonField.extra.refCode')</th>
       <th>@lang('commonField.houseHoldSetting.houseFoundation')</th>
       <th>@lang('commonField.extra.action') </th>
     </tr>
   </thead>
   <tbody>
    @php $count = 1  @endphp
    @foreach($allhouseFoundationType as $houseFoundation)
    <tr class="odd gradeX">
     <td>{{ $count ++ }}</td>
     <td>{{ $houseFoundation->refCode }}</td>
     <td >{{ $houseFoundation->foundationNameNep ?? 'N/A' }} - {{ $houseFoundation->foundationNameEng }} </td>

     <td class="left" >
       @if(Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun')
       <a href="#" data-url="{{ route('houseFoundation.edit', $houseFoundation->id) }}" class="btnEdithouseFoundation btn btn-primary btn-xs" id="" >
         <i class="fa fa-pencil"></i>
       </a>
       <a href="#" class="deleteMe" data-url="{{ route('houseFoundation.delete', $houseFoundation->id )}}" data-name="{{ $houseFoundation->nameEng }}">
        <button class="btn btn-danger btn-xs">
          <i class="fa fa-trash-o "></i>
          </button>
        </a>
        @else
        <small>@lang('commonField.extra.limited')</small>
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endif
</div>
</div>
</div>
@endsection
  @section('modelSection')
<div class="modal" id="houseFoundationModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.houseHoldSetting.addHouseFoundation')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ Form::open(['route' => 'houseFoundation.store', 'name'=>'frmhouseFoundation']) }}
        @include('admin.setting.houseFoundation._form')
        <div class="col-lg-12 p-t-20 text-right"> 
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
        </div>
        {{ Form::close() }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

      </div>

    </div>
  </div>
</div>


<!-- edit model for houseFoundation starts here -->

<div class="modal" id="edithouseFoundationModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.houseHoldSetting.edithouseFoundation')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       {{ Form::open(['name'=>'frmedithouseFoundation', 'id' => 'frmUpdatehouseFoundation']) }}
       @include('admin.setting.houseFoundation._form')
       {{ Form::hidden('houseFoundationId', null, ['class' => 'houseFoundationId']) }}
       <div class="col-lg-12 p-t-20 text-right"> 
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
      </div>
      {{ Form::close() }}
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">

    </div>

  </div>
</div>
</div>

<!-- edit model for houseFoundation ends here -->
@endsection

@section('custom_script')
@include('admin.setting.houseFoundation.script')
@endsection

