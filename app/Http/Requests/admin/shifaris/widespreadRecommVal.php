<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class WidespreadRecommVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:widespread_recomms',
            'chalaniNum' => 'required|unique:widespread_recomms',
            'issuedDate' => 'required',
            'wardNum' => 'required|numeric',
            'municipalityName' => 'required', 
            'organizationAddress' => 'required',
            'organizationType' => 'required',
            'organizationWard' => 'required|numeric',
            'childrenTitle' => 'required',
            'nameOfChildren' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
