<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class swamipto extends Model
{
    protected $table = 'swamiptos';
    protected $fillable = ['refCode','nameEng','nameNep','tagName','status','softDelete'];
}
