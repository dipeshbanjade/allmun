<script type="text/javascript">
    $(function(){
        /*add buttoin*/
        $("form[name='frmeditmigrationReason']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                },
                desc : {
                    required :true,
                    minlength : 20,
                    maxlength : 2000
                }
            },
            messages: {
                nameEng : "The name of Migration Reason is required",
                desc : "the Description is required with minimum 20 character and maximun 2000"
            }
        });
    });
    $(function(){
        /*add buttoin*/
        $("form[name='frmmigrationReason']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                },
                desc : {
                    required :true,
                    minlength : 20,
                    maxlength : 2000
                }
            },
            messages: {

                nameEng : "The name of Migration Reason is required",
                desc : "the Description is required with minimum 20 character and maximun 2000"

            }
        });
        var btnAddmigrationReason = $('.btnAddmigrationReason');
        btnAddmigrationReason.on('click', function(e){
          $('#migrationReasonModel').modal('show'); 
      });

        var btnEditmigrationReason = $('.btnEditmigrationReason');
        btnEditmigrationReason.on('click', function(e) {
          e.preventDefault();
          var url = $(this).data('url');
            alert(url);
          $.ajax({
             'type': 'GET',
             'url': url,
             success: function (response) {
                console.log(response.id);
                $('.migrationReasonId').val(response.id);
                $('.txtRefCode').val(response.refCode).attr('disabled',true);
                $('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
                $('.txtDesc').val(response.desc);
                $("#editmigrationReasonModel").modal('show');
            }
        })
      });

        var frmUpdatemigrationReason = $('#frmUpdatemigrationReason');
        frmUpdatemigrationReason.on('submit', function(e){
            e.preventDefault();
            var data = $(this).serialize();
            var id   = $('.migrationReasonId').val();
            var url  = "{{URL::to('/')}}" + "/setting/migrationReason/"+id+"/update";
            $.ajax({
                'type' : 'POST',
                'url'  : url,
                'data'    : data,
                success : function(response){
                    console.log(response);
                    if (response.success==true) {
                        $('.infoDiv').append(response.message).addClass('alert alert-success');
                    }
                    if (response.success==false) {
                        $.each(response.message, function(key, value){
                            $('.infoDiv').append(value).addClass('alert alert-danger');
                        })
                    }
                },complete:function(){
                    $("#editmigrationReasonModel").modal('hide');
                    setTimeout(location.reload.bind(location), 1500);
                }
            })
            .fail(function (response) {
                alert('data already exists');
            });
            $('#editmigrationReasonModel').modal('hide');
        });
    });
</script>