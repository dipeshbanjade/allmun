@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
   <div class="card-body row">
     <div class="col-sm-12">
       <div class="panel">
         <header class="panel-heading panel-heading-red">
           Edit Role Permission </header>
           <div class="panel-body">
             <div class="row">
               <div class="col-sm-12 col-md-12">
                 <h2 style="color: black;">
                      <h3 class="fontColorBlack">
                        <i class="fa fa-edit">
                          Role : {{ $edit['name'] }}
                        </i>
                      </h3><hr>
                 </h2>
                
                 {{ Form::open(['route' =>['system.assignRolePermission.update', $edit['id']], 'name' => 'frmPerRole', 'method' => 'post']) }}
                      @include('admin.acl.assignRolePermission._form')
                       {{ Form::submit('Update', ['class' => 'btn btn-success pull-right']) }}
		               <button type="button" class="btn btn-default pull-right" id="btn-clear-search">Clear</button>
		             {{ Form::close() }}
           
             </div>

           </div>
         </div>
       </div>
     </div>
     </div>
@endsection