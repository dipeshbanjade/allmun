        <div class="row top-part">
          <div class="col-md-6">
           <p align="left"><b class="ps">प. सं.:
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
            @endif
          </b></p>
          <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNum']) }}</b></p>
        </div>
        <div class="col-md-6">
          <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
        </div>
      </div>
      <div class="row title-left">
       <div class="col-md-12">
        <p align="left">श्री {{ Form::text('organizationName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'organizationName']) }}, </p>
        <p align="left">{{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'organizationAddress', 'title' => 'Office Address']) }}</p>
        <p align="left">{{ Form::text('orgAddressSec', null, ['class'=>'dashed-input-field', 'title' => 'Office Address']) }}|</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p align="center" class="font-size-24">
        </p><h4 align="center"><b>विषय: {{ Form::text('shifarishSubject', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'shifarishSubject']) }}।</b></h4>
        {{ Form::hidden('letter_subject', 'खुल्ला ढाँचा') }}
        <p></p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 content-para">
        {!! Form::textarea('note', null, ['class'=>'summernote', 'rows' => '10', 'cols' => '30']) !!}
      </div>
    </div>
    <div class="col-md-12">
      <hr>
      <h2>बोधार्थ&nbsp;  </h2>
      <table class="table table-bordered" id="bodarthaTable">
        <tbody>
          <?php
            $count = 1;
            $bodarthaData = !empty($data) ? json_decode($data->bodarthaStatement, true) :[1];
            ?>
            @foreach ($bodarthaData as $singleData) 
               <tr>
                 <?php $count++?>
                <td>{{ Form::text('bodartha[]', !empty($singleData['bodartha'])? $singleData['bodartha']: NULL, ['class'=>'form-control']) }}</td>
                @if ($count==2)
                  <td class="add-btns" style="border: 0px;"><a href="" id="add" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
                @else
                  <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                @endif
              </tr>
           
            @endforeach

          {{-- ?> --}}
      </tbody>
    </table>
    <div class="text-right btm-last">
      <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
      <p> <b>
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b></p>
    </div>
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr>
    @include('admin.shifaris.nibedakCommonField')
  </div>
  <div class="col-md-12">
    <hr>
  </div>
