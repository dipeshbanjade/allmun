@extends('main.app')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/shifarisSearch.css') }}" media="print,screen">
<div class="container">
	<div class="row wrapper1">		
		{{ Form::open(['route' => 'shifarisSearch', 'name' => 'frmSearchShifaris', 'class'=>'form-inline']) }}
		<!-- insert citizen number here -->
		<div class="form-group col-md-8">
			<input type="text" class="form-control" name="citizenId" placeholder="Scan Id">		
			<label>OR</label>
			<input type="text" class="form-control" name="citizenNum" placeholder="Citizen Number">
		</div>
		<!-- select shifarish from here -->
		<div class="form-group col-md-2">
			<select id="shifaris" class="form-control sysSelect2" name="shifarisTable">
				<option selected>Choose one</option>
				<option value="all">All</option>
				<option value="scholarship_recomms">Scholarship Recommendation</option>
				<option value="widespread_recomms">Wide Spread Recommendation</option>
				<option value="arthritis_supports">Financial Treatment Support Recommendation</option>
				<option value="yearly_income_certs">Yearly Income Certified</option>
				<option value="nep_languages">Nepali Languages</option>
				<option value="eng_languages">English Languages</option>
				<option value="non_profit_org_registrations">Non Profit Organization Register</option>
				<option value="institute_registrations">Institution Registration Recommendation</option>
				<option value="institute_recommendations">Institutionalization Recommendation</option>
				<option value="reconciliations">Reconciliation Recommendation</option>
				<option value="commissioner_details">Comissioner Detail Recommendation</option>
				<option value="varasis_authiorities">Varasis Authiorities Recommendation</option>
				<option value="account_openings">Account Opening Recommendation</option>
				<option value="account_closings">Account Close Recommendation</option>
				<option value="account_navigates">Account Navigation Recommendation</option>
				<option value="money_transfers">Money Transfer Recommendation</option>
				<option value="verify_annual_incomes">Verification Of Annual Income</option>
				<option value="annual_income_verify_japans">Verification Of Annual Income For Japan</option>
				<option value="property_values">Property Valuation</option>
				<option value="tax_clear_certifications">Tax Clearance Certificate</option>
				<option value="relation_verifies">Relationship Verification</option>
				<option value="birth_date_verifications">Birth Verification</option>
				<option value="unmarried_verifications">Unmarried Verification</option>
				<option value="marriage_verifies">Marriage Verification</option>
				<option value="address_verifies">Address Verification</option>
				<option value="occupation_verifications">Occupation Verification</option>
				<option value="identity_verification_o_n_es">Identity Verification One</option>
				<option value="identify_verify_twos">Identity Verification Two</option>
				<option value="bato_kayams">Bato Kayam Sifarish </option>
				<option value="lalpurja_harayekos">Lalpurja Harayeko </option>
				<option value="mohi_kittas">Mohi Katta Sifarish</option>
				<option value="ghar_patals">Ghar Patal</option>
				<option value="char_killa_bibarans">Char Killa Pramanit</option>
				<option value="property_name_transfers">Property Name Transfer</option>
				<option value="add_home_property_papers">Add House in Property Paper</option>
				<option value="unmarried_verify_neps">Unmarried Verify Nepali</option>
				<option value="married_verify_neps">Married Verify Nepali</option>
				<option value="birth_date_verify_neps">Birth Date Verify Nepali </option>

				{{-- k --}}
				<option value="nirman_bebasayas">Business License</option>
				<option value="business_registration_certificate">Business Registration</option>
				<option value="close_businesses">Close Business</option>
				<option value="perm_accountings">Pan Number Accounting</option>
				<option value="business_accountings">Business Accounting</option>
				<option value="industry_recommendations">Industry Registration</option>
				<option value="diff_individual_certs">Different Individual</option>
				<option value="diff_name_certs">Different Name</option>
				<option value="diff_b_d_certs">Different Birthdate</option>
				<option value="diff_eng_spell_certs">Different English Spelling</option>
				<option value="recomm_sents">Recommendation send</option>
				<option value="jet_machine_recomms">Jet Machine</option>
				<option value="room_cleaning_recomms">Room Cleaning</option>
				<option value="court_fees">Court Fees</option>
				<option value="class_add_recomms">Add class Recomendation</option>
				<option value="consumer_committes">Consumer Committee</option>
				<option value="wood_provider_recomms">Wood Provider</option>
				<option value="natural_calamities">Natural Calamities</option>
				<option value="handicap_ids">Handicap Id card</option>
				<option value="internal_migrations">Internal Migration</option>
				<option value="gharbato_verifies">Ghar Bato verify</option>
				<option value="ghar_kayams">Ghar Kayam</option>
				<option value="bijuli_jadan_shifaris">Bijuli Jadan</option>
				<option value="dhara_jadans">Dhara Jadan</option>
				<option value="permanent_resident_proposals">Permanent Resident Proposal</option>
				<option value="temporary_resident_proposals">Temporary Resident Proposal</option>
				<option value="relation_identifies">Relation Identities</option>
				<option value="mritak_hakdar_shifaris">Mritak Hakdar Shifaris</option>
				<option value="family_hierarchies">Famiy Hierarchies</option>

			</select>
		</div>
		<!-- search button -->
		<div class="col-md-2">
			<button type="submit" style="top: -6px;" class="btn btn-primary btn-md"><i class="fa fa-search"></i>search</button>
		</div>
		{{ Form::close() }}
	</div>
	<div class="clear"></div>
	<div class="row wrapper1">
		<!-- list ofshifarish and action buttons -->
		<table class="table">
			<thead class="th_style">
				<tr>
					<th scope="col">S.N</th>
					<th scope="col">Shifaris Name</th>
					<th scope="col">Chalani Number</th>
					<th scope="col">Issue Date</th>
					<th scope="col">Time</th>
					<th scope="col">Actions</th>
				</tr>
			</thead>
			<tbody style="margin-top: 10px;">
				@if(!empty($allShifarisData))
				@php $count = 1 @endphp
				@foreach($allShifarisData as $shifarisData)
				<tr>
					<th scope="row">{{ $count++ }}</th>
					<td>{{ $shifarisData->shifarish_name }}</td>
					<td>{{ $shifarisData->chalaniNum }}</td>
					<td>{{ $shifarisData->issuedDate }}</td>
					<td>{{ $shifarisData->created_at->diffForHumans() }}</td>
					<td>
						<span><a href="#" class="btn-danger btn-sm"><i class="fa fa-trash"></i></a></span>
						<span><a href="{{ $shifarisData->shifarish_id ? route('shifarisEdit', [$shifarisData->shifarish_id, $shifarisData->shifarish_name, $shifarisData->viewPath]) : '#' }}" class="btn-success btn-sm"><i class="fa fa-edit"></i></a></span>
					</td>
				</tr>
				@endforeach
				@elseif(!empty($shifarisData))
				@php $count = 1 @endphp
				@foreach($shifarisData as $singleShifarisData)
				<tr>
					<th scope="row">{{ $count++ }}</th>
					<td>{{ $singleShifarisData->shifarish_name }}</td>
					<td>{{ $singleShifarisData->chalaniNum }}</td>
					<td>{{ $singleShifarisData->issuedDate }}</td>
					<td>{{ $singleShifarisData->created_at->diffForHumans() }}</td>
					<td>
						<span><a href="#" class="btn-danger btn-sm"><i class="fa fa-trash"></i></a></span>
						<span><a href="{{ $singleShifarisData->shifarish_id ? route('shifarisEdit', [$singleShifarisData->shifarish_id, $singleShifarisData->shifarish_name, $singleShifarisData->viewPath]) : '#' }}" class="btn-success btn-sm"><i class="fa fa-edit"></i></a></span>
					</td>
				</tr>
				@endforeach
				@endif			
			</tbody>
		</table>
	</div>
</div>
@endsection
