<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class Religious extends Model
{
   protected $table = 'religiouses';
   protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
