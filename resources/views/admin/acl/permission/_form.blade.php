<!-- form is created for role  -->
<div class="card-body row">
<!-- input field for role name -->
	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
	{{ Form::text('perName', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}	
		<label class = "mdl-textfield__label">Permission Name</label>
	</div>

		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
		<select name="moduleName" class="form-control" required>
			<option value="">Select access level</option>
			<option value="setting">Setting module</option>
			<option value="dashboard">Dashboard module</option>
			<option value="municipility">Municipality</option>
			<option value="ward">Ward module</option>
			<option value="sifarish">Sifarish</option>
			<option value="chalani">Chalani</option>
			<option value="darta">Darta</option>
			<option value="setUpLeave">setUp Leave</option>
		</select>
	</div>


		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
	{{ Form::text('routeName', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}	
		<label class = "mdl-textfield__label">Route Name</label>
	</div>
<!-- input field for description -->
	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
	    {{ Form::textarea('desc', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'rows'=>'3']) }}	
		<label class = "mdl-textfield__label">Description</label>
	</div>
<!-- form buttom submit and cancel is created  -->
</div>
<!-- form end here  -->