<?php

namespace App\Http\Controllers\admin\shifaris\economic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\yearlyIncomeCert;
use App\model\shifaris\ApplicantUser;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\yearlyIncomeVal;
use DB;
use Auth;

use App\model\mylogic;

class YearlyIncomeCertController extends Controller
{
    protected $viewPath = 'admin.shifaris.economic.yearlyIncomeCert';
    protected $shifarisTableName     = 'yearly_income_certs';
    protected $appUsrTable     = 'applicant_users';
    protected $deginationsTbl = 'deginations';

    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixYearlyIncomeCert');
 
    }

    public function index()
    {
        $page['page_title']       = 'Yearly Income Certification';
        $page['page_description'] = 'Yearly Income Certification';

        
           $propertyVal = yearlyIncomeCert::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        

    }

    public function create()
    {
        $page['page_title']       = 'Yearly Income Certification : create';
        $page['page_description'] = 'Yearly Income Certification details';

        // dd($this->prefix);

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $this->prefix);
     
        // dd($getUniqueCode);
        
            $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationsTbl, $orderColumn = 'nameNep', $orderby = 'ASC');
            return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(yearlyIncomeVal $request)
    {
        //
        
           DB::beginTransaction();
            try {

                // $yearlyIncomeDetail = count($request->detail);

                // $detail = $request->detail;
                // $areaDetail = $request->areaDetail;
                // $gharNo = $request->gharNo;
                // $monthlyIncome = $request->monthlyIncome;
                // $yearlyIncome = $request->yearlyIncome;
                // $remarks = $request->remarks;

                // $yearlyIncomeArray = [];

                // for ($i=0; $i < $yearlyIncomeDetail; $i++) {
                //     array_push($scholarShipArray, [
                //         'detail' => $detail[$i],
                //         'areaDetail' => $areaDetail[$i],
                //         'gharNo' => $gharNo[$i],
                //         'monthlyIncome' => $monthlyIncome[$i],
                //         'yearlyIncome' => $yearlyIncome[$i],
                //         'remarks' => $remarks[$i],
                //     ]);
                // }
                // $yearlyIncomeJson = json_encode($yearlyIncomeArray);
                
                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

                $isShifarisExist = isset($request->shifarisId) ? yearlyIncomeCert::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
                
                $save = !empty($isShifarisExist['id']) ? yearlyIncomeCert::findOrfail($isShifarisExist['id']) : new yearlyIncomeCert;

                $save->createdBy = Auth::user()->id;

                                $save->refCode  = $request->refCode;

                $save->wards_id  = Auth::user()->wards_id ?? NULL;

                $save->municipilities_id = getMunicipalityData()['id'] ?? "";
                $save->issuedDate  = $request->issuedDate;
                $save->chalaniNum = $request->chalaniNum;
                $save->topic  = $request->topic;
                $save->personAddress  = $request->personAddress;
                $save->personDetails  = $request->personDetails;
                
                $incomeCount = count($request->areaDetail);
                $detail = $request->detail;
                $areaDetail = $request->areaDetail;
                $monthlyIncome = $request->monthlyIncome;
                $rate = $request->rate;
                $yearlyIncome = $request->yearlyIncome;
                $remarks = $request->remarks;
                $incomeArray = [];

                for ($i=0; $i < $incomeCount; $i++) {
                    array_push($incomeArray, [
                            'detail' => $detail[$i],
                            'areaDetail' => $areaDetail[$i],
                            'monthlyIncome' => $monthlyIncome[$i],
                            'rate' => $rate[$i],
                            'yearlyIncome' => $yearlyIncome[$i],
                            'remarks' => $remarks[$i] ?? 'N/A'
                        ]);
                }
                $save->yearlyIncomeStatement = json_encode($incomeArray);

                $save->authorizedPerson  = $request->authorizedPerson;
                $save->deginations_id  = $request->deginations_id;

                $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

                $mySave = $save->save();

                       //end for login
                $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

                           $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                DB::commit();
                return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
            }else{
                return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
            }catch (Exception $e) {
                DB::rollback();
                return back()->withMessage(config('activityMessage.dataNotInserted'));
            }
        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function changeStatus(){

    }
}
