<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class VaccineType extends Model
{
protected $table = 'vaccine_types';
	protected $fillable = ['refCode','nameEng','nameNep','tag','status','softDelete'];
}
