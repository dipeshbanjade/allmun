<?php

namespace App\Http\Requests\admin\setting\WasteManagement;

use Illuminate\Foundation\Http\FormRequest;

class WasteManagValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'nameEng' => 'required|min:4|max:100'
        ];
    }
}
