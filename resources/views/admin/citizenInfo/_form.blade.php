<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="personalInfo-tab" data-toggle="tab" href="#personalInfo" role="tab" aria-controls="personalInfo" aria-selected="true">Personal Info</a>
  </li>

</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="personalInfo" role="tabpanel" aria-labelledby="personalInfo-tab">
   <div class="row">
   <?php
       $currentRoute = \Request::route()->getName();
   ?>
   @if(Auth::user()->userLevel == 'mun')
      <div class="col-sm-6 col-md-6 col-sm-offset-6 col-md-offset-6 pull-right">
             <!-- sdfsdf -->

             <label>@lang('commonField.extra.wards_id')</label><br>
             @if(isset($wardsDetails))
                 <select name="wards_id" class="sysSelect2 form-control" required>
                     <option value="">@lang('commonField.extra.wards_id')</option>
                     @foreach($wardsDetails as $ward)
                         <option value="{{ $ward->id }}" {{ isset($data->wards_id) && $data->wards_id == $ward->id ? 'selected' : '' }}>
                             {{ $ward->nameEng }} | {{ $ward->nameNep }}  
                         </option>
                     @endforeach
                 </select>
             @endif
      </div>
   @endif
     <div class="col-sm-6 col-md-6 col-sm-offset-6 col-md-offset-6 pull-right">
            <!-- sdfsdf -->
            @if(isset($data))
                <img src="{{ asset($data->profilePic) }}" width="200" height="200">
            @endif
            <input type="file" name="profilePic" id="thebox">
     </div>
 </div><hr>
 <!--  -->
 <div class="">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
                <header><i class="fa fa-user"></i>&nbsp;@lang('commonField.extra.nameAndAddress')</header>
            </div>
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
                        <div class="form-group">
                            <label style="margin-right: 15px;">@lang('commonField.personal_information.gender')</label>
                            <label class="radio-inline">
                                <?php $checkMe = "checked='checked'";   ?>
                                <input type="radio" name="gender" value="male" {{ isset($data) && $data->gender =='male' ? ' checked="checked"' : ''}}>
                                @lang('commonField.personal_information.male')
                            </label> 
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="female" {{ isset($data) && $data->gender =='female' ? ' checked="checked"' : ''}}>
                                @lang('commonField.personal_information.female')
                            </label> 
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="others" {{ isset($data) && $data->gender =='others' ? ' checked="checked"' : ''}}>
                                @lang('commonField.personal_information.others')
                            </label> <br><hr>
                        </div>
                    </div>
                    <div class="citizenDiv row">
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>@lang('commonField.extra.citizenNo')</label>
                                {{ Form::text('citizenNo', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Citizenship number', $currentRoute =='citizenInfo.create' ? 'required' : '']) }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>@lang('commonField.extra.citizenshipIssueDate')(B.S)</label>
                                 {{ Form::text('citizenshipIssueDate', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'yyyy-mm-dd', 'id'=>'myDate2']) }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>@lang('commonField.extra.placeofIssueCitizenship')</label>
                                {{ Form::text('citizenshipIssuePlace', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Place of Issue']) }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>@lang('commonField.extra.citizenType')</label>
                                <select class="form-control" name="citizen_types_id">
                                    <option value="">@lang('commonField.extra.citizenTypeId')</option>
                                    @if(count($citizenTypesId) > 0)  <!-- selecting leave type -->
                                    @foreach($citizenTypesId as $citizentyp)
                                    <option value="{{ $citizentyp->id }}" {{ isset($data->citizen_types_id)  && $data->citizen_types_id == $citizentyp->id ? 'selected' : ''}}>
                                       {{ $citizentyp->nameNep }} - {{ $citizentyp->nameEng}}
                                   </option>
                                   @endforeach
                                   @endif
                               </select>
                           </div>
                        </div>
                    </div><br><hr>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameNep')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="First Name Nepali" name="fnameNep"> -->

                            {{ Form::text('fnameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'First Name Nepali']) }} 
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameNep')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="Middle Name Nepali" name="mnameNep"> -->
                            {{ Form::text('mnameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Middle Name Nepali']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.lastNameNep')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="Surname Nepali" name="lnameNep"> -->
                            {{ Form::text('lnameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Surname Nepali']) }}
                        </div>
                    </div>
                  
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameEng')</label>
                            <!--  <input type="text" class="form-control input-md" placeholder="First Name" name="fnameEng"> -->
                            {{ Form::text('fnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'First Name']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameEng')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="Middle Name" name="mnameEng"> -->
                            {{ Form::text('mnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Middle Name']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.lastNameEng')</label>
                            <!--  <input type="text" class="form-control input-md" placeholder="Surname" name="lnameEng"> -->
                            {{ Form::text('lnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Surname']) }}
                            <br>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<fieldset>
<div class="">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
                <header><i class="fa fa-map-marker"></i>&nbsp;@lang('commonField.address_information.addressDetails')</header>
            </div>
            <div class="form-row">
                    <legend>
                         
                    </legend>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.birthPlace')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="place of Birth" name="birthplace"> -->
              
                            {{ Form::text('birthplace', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'place of Birth']) }}
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.dob') (B.S.)</label>
                           <!--  <input type="text" class="form-control input-md" id="myDate" name="dobBS"> -->
              
                            {{ Form::text('dobBS', null, ['class'=>'mdl-textfield__input', 'placeholder' => '2047-06-05', 'id'=>'myDate']) }}
                        </div>
                    </div>
              
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.dob') (A.D.)</label>
                           <!--  <input type="text" class="form-control input-md dashed-input-field ndp-nepali-calendar" id="myEngDate" name="dobAD"> -->
              
                            {{ Form::text('dobAD', null, ['class'=>'ndp-nepali-calendar mdl-textfield__input', 'id'=>'myEngDate', 'placeholder'=>'1990-09-21']) }}
                        </div>
                    </div>
                    <!--  -->
                    <!-- <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.ward')</label>
                            {{ Form::text('wardNo', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Ward Number']) }}
                        </div>
                    </div> -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.address_information.villageEng')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="Town/Village" name="villageNameEng"> -->
                            {{ Form::text('villageNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Town/Village']) }}
                        </div>
                    </div> 
                    
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.address_information.villageNep')</label>
                            <!--  <input type="text" class="form-control input-md" placeholder="Town/Village" name="villageNameNep"> -->

                            {{ Form::text('villageNameNep', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Town/Village']) }}
                        </div>
                    </div>
      
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.email')</label>
                            <!-- <input type="email" class="form-control input-md" placeholder="Email" name="email"> -->
                    
                            {{ Form::email('email', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'email']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.phoneNumber')</label>
                            <!-- <input type="number" class="form-control input-md" placeholder="Phone Number" maxlength="10" name="phoneNumber"> -->
                    
                            {{ Form::text('phoneNumber', null, ['class'=>'phoneNumber mdl-textfield__input', 'placeholder' => 'Phone Number']) }}
                        </div>
                    </div>
            </div>
            <div class="form-row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.address_information.district')</label>
                            <select class="form-control mdl-textfield__input" name="districts_id">
                                <option value="">@lang('commonField.extra.districts_id')</option>
                                @if(count($districtId) > 0)  <!-- selecting leave type -->
                                @foreach($districtId as $dis)
                                <option value="{{ $dis->id }}" {{ isset($data->districts_id)  && $data->districts_id == $dis->id ? 'selected' : ''}} {{(old('dis')==$dis->id)? 'selected':''}}>
                                   {{ $dis->districtNameNep }} - {{ $dis->districtNameEng }}
                               </option>
                               @endforeach
                               @endif
                           </select>
              
                       </div>
                   </div>
                   <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                        <label>@lang('commonField.address_information.province')</label>
                        <select class="form-control mdl-textfield__input" name="provinces_id">
                            <option value="">@lang('commonField.extra.provisions_id')</option>
                            @if(count($provisonId) > 0)  <!-- selecting leave type -->
                            @foreach($provisonId as $pro)
                            <option value="{{ $pro->id }}" {{ isset($data->provinces_id) && $data->provinces_id == $pro->id ? 'selected' : ''}} {{(old('pro')==$pro->id)? 'selected':''}}>
                               {{ $pro->name }}
                           </option>

                           @endforeach
                           @endif
                       </select>
                   </div>
               </div>
            </div>
    </div>
</div>
</fieldset> 

<div class="card-body">
    <div class="card-head">
          <header><label> <i class="fa fa-user"></i> Identity Details</label></header>
    </div>
    <fieldset>

          <div class="form-row">
          <!--  -->
              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.jatjati')</label>
                      <select class="form-control" name="jatjatis_id">
                          <option value="">@lang('commonField.extra.jatjati')</option>
                          @if(count($jatJatiId) > 0)  <!-- selecting leave type -->
                          @foreach($jatJatiId as $jatJati)
                          <option value="{{ $jatJati->id }}" {{ isset($data->jatjatis_id)  && $data->jatjatis_id == $jatJati->id ? 'selected' : ''}} {{(old('jatJati')==$jatJati->id)? 'selected':''}}>
                             {{ $jatJati->refCode }} - {{ $jatJati->nameNep }} - {{ $jatJati->nameEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.nationality')</label>
                      <select name="nationalities_id" class="form-control">
                            <option value="">@lang('commonField.extra.nationality')</option>
                             @if(count($nationalityId) > 0)
                                @foreach($nationalityId as $nation)
                                     <option value="{{ $nation->id }}" {{ isset($data->nationalities_id)  && $data->nationalities_id == $nation->id ? 'selected' : ''}} {{(old('nation')==$nation->id)? 'selected':''}}>
                                            {{ $nation->refCode }} - {{ $nation->nameNep }}  - {{ $nation->nameEng }}
                                     </option>
                                @endforeach
                             @endif
                      </select>
                  </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.language') </label>
                      <select class="form-control" name="sys_languages_id">
                          <option value="">@lang('commonField.extra.language_id')</option>
                          @if(count($syslangId) > 0)  <!-- selecting leave type -->
                          @foreach($syslangId as $lang)
                          <option value="{{ $lang->id }}" {{ isset($data->sys_languages_id)  && $data->sys_languages_id == $lang->id ? 'selected' : ''}} {{(old('lang')==$lang->id)? 'selected':''}}>
                             {{ $lang->refCode }} - {{ $lang->langNep }} - {{ $lang->langEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.religious') </label>
                      <select class="form-control" name="religiouses_id">
                          <option value="">@lang('commonField.extra.religious_id')</option>
                          @if(count($religiousId) > 0)  <!-- selecting leave type -->
                          @foreach($religiousId as $religious)
                          <option value="{{ $religious->id }}" {{ isset($data->religiouses_id)  && $data->religiouses_id == $religious->id ? 'selected' : ''}} {{(old('religious')==$religious->id)? 'selected':''}}>
                             {{ $religious->refCode }} - {{ $religious->nameNep }} - {{ $religious->nameEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>
              <!--  -->
              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                      <label>@lang('commonField.personal_information.disablityStatus') </label>
                      <select class="form-control" name="disabilities_id">
                          <option value="">@lang('commonField.personal_information.disablityStatus')</option>
                          @if(count($disablitiyStatus) > 0)  <!-- selecting leave type -->
                          @foreach($disablitiyStatus as $disablity)
                          <option value="{{ $disablity->id }}" {{ isset($data->disabilities_id)  && $data->disabilities_id == $disablity->id ? 'selected' : ''}} {{(old('disablity')==$disablity->id)? 'selected':''}}>
                             {{ $disablity->refCode  }} - {{ $disablity->nameNep }} - {{ $disablity->nameEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>
              <!--  -->
              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.maritialStatus') </label>
                      <select class="form-control" name="maritialStauts_id">
                          <option value="">@lang('commonField.extra.maritialStatus')</option>
                          @if(count($maritialStatus) > 0)  <!-- selecting leave type -->
                          @foreach($maritialStatus as $maritail)
                          <option value="{{ $maritail->id }}" {{ isset($data->maritialStauts_id)  && $data->maritialStauts_id == $maritail->id ? 'selected' : ''}} {{(old('maritail')==$maritail->id)? 'selected':''}}>
                             {{ $maritail->refCode  }} - {{ $maritail->nameNep }} - {{ $maritail->nameEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>
          </div>
      </fieldset> 
</div>

<div class="card-box">
    <div class="card-body">
        <div class="card-head">
             <header><label><i class="fa fa-info"></i> other Information</label></header>
        </div>


        <div class="form-row">

         <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
              <label>@lang('commonField.personal_information.qualification')</label>
              <select class="form-control" name="qualifications_id">
                  <option value="">@lang('commonField.extra.qualification_id')</option>
                  @if(count($qualificationId) > 0)  <!-- selecting leave type -->
                  @foreach($qualificationId as $qua)
                  <option value="{{ $qua->id }}" {{ isset($data->qualifications_id)  && $data->qualifications_id == $qua->id ? 'selected' : ''}} {{(old('qua')==$qua->id)? 'selected':''}}>
                    {{ $qua->refCode  }} - {{ $qua->nameNep }} - {{ $qua->nameEng}}
                 </option>
                 @endforeach
                 @endif
             </select>
         </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label>@lang('commonField.extra.occupation')</label>
                <select class="form-control" name="occupations_id">
                    <option value="">@lang('commonField.extra.occupation_id')</option>
                    @if(count($occupationId) > 0)  <!-- selecting leave type -->
                    @foreach($occupationId as $occup)
                    <option value="{{ $occup->id }}" {{ isset($data->occupations_id)  && $data->occupations_id == $occup->id ? 'selected' : ''}} {{(old('occup')==$occup->id)? 'selected':''}}>
                       {{ $occup->refCode  }} - {{ $occup->nameNep }} - {{ $occup->nameEng}}
                   </option>
                   @endforeach
                   @endif
               </select>
           </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label style="margin-right: 15px;">@lang('commonField.extra.passportExists')?</label>
                <label class="radio-inline">
                    <input type="radio" name="havePassport" class="pwdExists" value="N" checked="checked">
                    @lang('commonField.extra.no')
                </label> 
                <label class="radio-inline">
                    <input type="radio" name="havePassport" class="pwdExists" value="Y">
                    @lang('commonField.extra.yes')
                </label> 
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 passportDiv hide">
            <div class="form-group">
                <label> @lang('commonField.extra.passportNum')</label>
                <!-- <input type="text" class="form-control input-md" placeholder="Passport Number" name="passportNumber"> -->
                {{ Form::text('passportNumber', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Passport Number']) }}
            </div>
        </div>
        <!--  -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label>@lang('commonField.extra.annualIncome')</label>
               <!--  <input type="text" class="form-control input-md" placeholder="Annual Income" name="annualIncome"> -->
                {{ Form::text('annualIncome', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Annual Income']) }}
            </div>
        </div>
        <!--  -->
         <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
           <div class="form-group">
             <label>@lang('commonField.houseHoldSetting.livingFor')</label>
             <select class="form-control" name="family_living_id">
              <option value="">@lang('commonField.houseHoldSetting.livingFor')</option>
              @if(isset($familyLivingId) && count($familyLivingId) > 0) 
              @foreach($familyLivingId as $fmLiv)
              <option value="{{ $fmLiv->id }}" {{ isset($data->family_living_id) && $data->family_living_id == $fmLiv->id ? 'selected' : '' }}>
               {{ $fmLiv->refCode  }} - {{ $fmLiv->nameNep }} - {{ $fmLiv->nameEng}}
             </option>
             @endforeach
             @endif
           </select>
         </div>
         </div>
         </div>
        <!--  -->
        @if(!empty($data))
           <div class="row">
              <p class="pull-right">
                 <br><hr> 
                 <label class="text-danger">नभएको खण्डमा खाली छोडनु होला</label>
              </p>
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="form-group">
                 <label>
                   @lang('commonField.extra.grandFatherName')
                 </label>
                 <select name="grandFather_id" class="form-control">
                  <option value="">@lang('commonField.personal_information.selectFatherName')</option>
                  @if(count($allCitizen) > 0) 
                  @foreach($allCitizen as $gfather)
                  <?php
                  $getGf = getCitizenDetails($gfather->relation_id);               
                  ?>
                  <option value="{{ $getGf->id }}" {{ !empty($grandFatherId) && $getGf->id == $grandFatherId ? 'selected' : ''  }}>
                    {{ 
                      $getGf->fnameNep .' '. $getGf->mnameNep .' '. $getGf->lnameNep
                    }} 
                  </option>
                  @endforeach 
                  @endif
                </select>
              </div>
              </div>
           <!--  -->
           <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
             <div class="form-group">
              <label>
                @lang('commonField.personal_information.selectFatherName')
              </label>
              <select name="fathers_id" class="form-control">
               <option value="">@lang('commonField.personal_information.selectFatherName')</option>
               @if(count($allCitizen) > 0) 
               @foreach($allCitizen as $father)
               <?php
               $getFather = getCitizenDetails($father->relation_id);               
               ?>
               <option value="{{ $getFather->id }}" {{ !empty($fatherData) && $getFather->id == $fatherData ? 'selected' : ''  }}>
                 {{ 
                   $getFather->fnameNep .' '. $getFather->mnameNep .' '. $getFather->lnameNep
                 }} 
               </option>
               @endforeach 
               @endif
             </select>
           </div>
           </div>
           <!--  -->
           <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
             <div class="form-group">
              <label>
                @lang('commonField.personal_information.selectMotherName')
              </label>
              <select name="mothers_id" class="form-control">
               <option value="">@lang('commonField.personal_information.selectMotherName')</option>
              
               @foreach($allCitizen as $mother)
               <?php
               $getMother = getCitizenDetails($mother->relation_id);               
               ?>
               <option value="{{ $getMother->id }}" {{ !empty($motherData) && $getMother->id == $motherData ? 'selected' : ''  }}>
                 {{ 
                   $getMother->fnameNep .' '. $getMother->mnameNep .' '. $getMother->lnameNep
                 }} 
               </option>
               @endforeach 
             </select>
           </div>
           </div> 
           <!-- wife and husband   -->
           <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
             <div class="form-group">
              <label>
                @lang('commonField.extra.husbandWifeName')
              </label>
              <select name="wifeHusband_id" class="form-control">
               <option value="">@lang('commonField.extra.husbandWifeName')</option>
              
               @foreach($allCitizen as $wifehus)
               <?php
               $getWifeHus = getCitizenDetails($wifehus->relation_id);               
               ?>
               <option value="{{ $getWifeHus->id }}" {{ !empty($marriageData) && $getWifeHus->id == $marriageData ? 'selected' : ''  }}>
                 {{ 
                   $getWifeHus->fnameNep .' '. $getWifeHus->mnameNep .' '. $getWifeHus->lnameNep
                 }} 
               </option>
               @endforeach 
             </select>
           </div>
           </div> 
           <!--  -->

        </div>
        @endif
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label> @lang('commonField.extra.citizenImage')</label>
                <div class="form-group imgCitizen">
                  <p class="pull-right">
                      <label>
                       <br>
                       <img src="{{ isset($data->citizenImgPath) ? asset($data->citizenImgPath) : '' }}" width="120" height="80" class="img img-thumbnai" id="imgCitizen"><br>
                        <input name="citizenImgPath" type='file' onchange="displayImage(this, 'imgCitizen');" title="select citizenship" />
                      </label>
                  </p>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
<!-- identity Detials -->
   

</div>
</div>
</div>