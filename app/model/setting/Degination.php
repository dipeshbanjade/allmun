<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class Degination extends Model
{
    protected $table  = 'deginations';
    protected $fillable = ['refCode', 'nameNep', 'nameEng', 'status', 'softDelete'];
}
