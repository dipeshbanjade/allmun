<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class FilterProcess extends Model
{
    protected $table = 'filter_processes';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
