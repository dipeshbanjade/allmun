<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
      <div class="form-group col-md-6">
        <p>
          १. तपाईको परिवारले प्रयोग गर्ने खानेपानीको मुख्य स्रोत के हो ?  
        </p><br>
        @if(isset($waterSource) && count($waterSource) > 0)
        @foreach($waterSource as $ws)
        <label class="radio-inline">
         <input type="radio" class="rdoClickMe" title="double click for reset" name="sourceNam" value="{{ $ws->nameNep}}"  {{ isset($hsDrinkingWaterData) && $hsDrinkingWaterData->sourceNam == $ws->nameNep ? 'checked="checked"' : ''  }}>
         {{ $ws->nameNep }}
       </label>  <br>
       @endforeach
       @else
       <label>Please insert data from setting</label>
       @endif

     </div>

     <div class="form-group col-md-6">
      <p>
        २. यस परिवारले पाइपधाराबाट आउने पानी प्रयोग गर्नुहुन्छ भने त्यसको स्वामित्व कस्तो हो ?
      </p><br />
      @if (isset($samiptoHsId) && count($samiptoHsId) > 0)
      @foreach ($samiptoHsId as $sampitoWs)
      <label class="radio-inline">
        <input type="radio" class="rdoClickMe" title="double click for reset" name="ownershipNam" value="{{$sampitoWs->nameNep}}" {{ isset($hsDrinkingWaterData) && $hsDrinkingWaterData->ownershipNam == $sampitoWs->nameNep ? 'checked="checked"' : ''  }}>
        {{$sampitoWs->nameNep}}
      </label> 
      <br />
      @endforeach
      @else
      <label>Please insert data from setting</label>
      @endif
      {{ Form::text('otherOwnershipNam', isset($hsDrinkingWaterData->otherOwnershipNam) ? $hsDrinkingWaterData->otherOwnershipNam : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'others']) }}
  </div>

  <div class="form-group col-md-6">{{-- 3 --}}                                
    <p>
      ३. तपाईको घरमा निजी धारा जडान भएको छैन भने किन निजी धारा जडान नगर्नु भएको हो ?
    </p>
    @if(isset($whyNotId) && count($whyNotId) > 0)
    @foreach($whyNotId as $wNot)
    <label class="radio-inline">
      <input type="radio" class="rdoClickMe" title="double click for reset" name="whyNotsNam" value="{{$wNot->nameNep}}" {{ isset($hsDrinkingWaterData) && $hsDrinkingWaterData->whyNotsNam == $wNot->nameNep ? 'checked="checked"' : ''  }}>
      {{$wNot->nameNep}}
    </label>
    <br />
    @endforeach
    @else
    <label>Please insert data from setting</label>
    @endif                                  
    {{ Form::text('otherWhyNotsNam', isset($hsDrinkingWaterData->otherWhyNotsNam) ? $hsDrinkingWaterData->otherWhyNotsNam : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'others']) }}
</div>

<div class="form-group col-md-6">{{-- 4 --}}
  <p>
    ४. तपाईको परिवारलाई पिउनेपानीको मुख्य श्रोत (पाइपधाराबाट) बाट प्राप्त खानेपानीको पर्याप्तता कस्तो छ ?<br>
  </p>
  @if (isset($waterAvailabilities) && count($waterAvailabilities) > 0)

  @foreach($waterAvailabilities as $wA)
  <label class="radio-inline">
    <input type="radio" class="rdoClickMe" title="double click for reset" name="availability" value="{{$wA->nameNep}}" {{ isset($hsDrinkingWaterData) && $hsDrinkingWaterData->availability == $wA->nameNep ? 'checked="checked"' : ''  }}>
    {{$wA->nameNep}}
  </label>
  <br />
  @endforeach 
  @else
  <label>Please insert data from setting</label>    

  @endif

</div>

<div class="form-group col-md-6">{{-- 5 --}}
  <p>
    ५. तपाईको परिवारले पिउने पानीको गुणस्तर कस्तो छ ?
    <br/>
  </p>
  @if (isset($qualityWaters) && count($qualityWaters) > 0)
  @foreach($qualityWaters as $qualityW)
  <label class="radio-inline">
    <input type="radio" class="rdoClickMe" title="double click for reset" name="waterQty" value="{{$qualityW->nameNep}}" {{ isset($hsDrinkingWaterData) && $hsDrinkingWaterData->waterQty == $qualityW->nameNep ? 'checked="checked"' : '' }}>
    {{$qualityW->nameNep}}
  </label>
  <br />
  @endforeach  
  @else
  <label>Pleassse insert data from setting</label>                                         
  @endif                                
</div>

<div class="form-group col-md-6">{{-- 6 --}}
  <p>
    ६. तपाईको घरबाट खानेपानी ल्याउन (जाँदा आउँदा) लाग्ने कति समयको दुरीमा रहेको छ ?
  </p><br>
  <!--  {{ Form::text('distToSrcTime', isset($hsDrinkingWaterData->distToSrcTime) ? $hsDrinkingWaterData->distToSrcTime : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'distance to source']) }} -->
  <select class="form-control" name="distToSrcTime" required>
   <option value="">Select time</option>
   <option value="घर नजिक / घरैमा  भएको"  {{ !empty($hsDrinkingWaterData->distToSrcTime) && $hsDrinkingWaterData->distToSrcTime == 'घर नजिक / घरैमा  भएको' ? 'selected' : ''  }}>
   घर नजिक / घरैमा  भएको</option>
   <option value="१५ मिनेटसम्मको  दुरीमा"  {{ !empty($hsDrinkingWaterData->distToSrcTime) && $hsDrinkingWaterData->distToSrcTime == '१५ मिनेटसम्मको  दुरीमा' ? 'selected' : ''  }}>
   १५ मिनेटसम्मको  दुरीमा</option>
   <option value="१५-३० मिनेटको दुरीमा" {{ !empty($hsDrinkingWaterData->distToSrcTime) && $hsDrinkingWaterData->distToSrcTime =='१५-३० मिनेटको दुरीमा' ? 'selected' : ''  }}>१५ - ३० मिनेटको दुरीमा</option>
   <option value="३० मिनेटभन्दा बढी समय लाग्ने" {{ !empty($hsDrinkingWaterData->distToSrcTime) &&  $hsDrinkingWaterData->distToSrcTime == '३० मिनेटभन्दा बढी समय लाग्ने' ? 'selected' : ''  }}>३० मिनेटभन्दा बढी समय लाग्ने</option>
 </select>
</div>     
</div>  
</div>
</div>
<div class="msgDisplay"></div>