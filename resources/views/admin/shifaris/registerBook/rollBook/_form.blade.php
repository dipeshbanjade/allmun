<div class="right_col nep dash-pg" role="main" style="min-height: 882px;">
    <div class="clearfix"></div>
    <div class="row">
  
  
          <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
  
          <div class="x_title">
            <h2 class="title-x">चलानी किताब </h2>
            
            <div class="clearfix"></div>
          </div>
  
          <div class="x_content" id="block">
            <div class="col-md-12">
  
              
            
  
  
            <div class="col-md-9">
  
             <form id="chalani_form" action="https://project.hovertechnepal.com/ChalaniBook/saveChalaniRecord" method="post" class="form-horizontal" enctype="multipart/form-data">
  
              <div class="form-group">
                <label class="col-md-3">चलानी नं: <span class="red">*</span></label>
                <div class="col-md-7">
                 
                  <input type="text" required="required" value="" class="form-control" name="chalani_no">
                </div>
              </div>
  
              
  
  
              <div class="form-group">
                <label class="col-md-3">चलानी हुने पत्रको मिति: <span class="red">*</span></label>
                <div class="col-md-7">
  
                  <input type="text" required="required" value="" class="form-control ndp-nepali-calendar date-input-field" id="chalani_date" name="chalani_date" onfocus="showNdpCalendarBox('chalani_date')">
                </div>
              </div>
  
  
              <div class="form-group">
                <label class="col-md-3">पत्र संख्या: <span class="red">*</span></label>
                <div class="col-md-7">
  
                  <input type="text" required="required" class="form-control" name="patra_sankya" value="">
                </div>
              </div>
  
              <div class="form-group">
                <label class="col-md-3">पत्रको मिति: <span class="red">*</span></label>
                <div class="col-md-7">
  
                  <input type="text" required="required" class="form-control ndp-nepali-calendar date-input-field" id="letter_date" name="letter_date" value="" onfocus="showNdpCalendarBox('letter_date')">
                </div>
              </div>
  
              <div class="form-group">
                <label class="col-md-3">विषय:<span class="red">*</span></label>
                <div class="col-md-7">
  
                  <input type="text" required="required" class="form-control" name="letter_subject" value="">
                </div>
              </div>
  
              <div class="form-group">
                <label class="col-md-3">पत्र चलान गर्ने अफिसको नाम: <span class="red">*</span></label>
                <div class="col-md-7">
  
                  <input type="text" required="required" list="patra_chalan_office" class="form-control" name="patra_chalan_office" value="">
  
  
                  <datalist id="patra_chalan_office">
                                          <option value="चाँगुनारायण नगरपालिका">
  
  
                                              </option><option value="office1">
  
  
                                              </option><option value="Office 1 ">
  
  
                                              </option><option value="divash office ">
  
  
                                              </option><option value="raj office ">
  
  
                                              </option><option value="Office by Nabaraj ">
  
  
                                              </option><option value="सुर्यबिनायक नगरपालीका">
  
  
                                              </option><option value="नापी कार्यलय">
  
  
                                              </option><option value="जिल्ला अायोजना कार्यान्वयन इकाइ (भवन)">
  
  
                                              </option><option value="अार्थिक मामिला तथा योजना मन्त्रालय">
  
  
                                              </option><option value="संघीय मामिला तथा सामान्य प्रशासन मन्त्रालय">
  
  
                                              </option><option value="१ नं वडा कार्यालय">
  
  
                                              </option><option value="२ नं वडा कार्यालय">
  
  
                                              </option><option value="३ नं वडा कार्यालय">
  
  
                                              </option><option value="४ नं वडा कार्यालय">
  
  
                                              </option><option value="५ नं वडा कार्यालय">
  
  
                                              </option><option value="६ नं वडा कार्यालय">
  
  
                                              </option><option value="७ नं वडा कार्यालय">
  
  
                                            </option></datalist>
                    </div>
                  </div>
  
  
  
                  <div class="form-group">
                    <!-- <button type="button" onclick="scanToJpg();">Scan</button> -->
  
                    <label class="col-md-3">डकुमेन्टको स्क्यान फोटो: <span class="red">*</span></label>
                    <div class="col-md-7">
  
                      <input type="file" name="document_file_name" required="required">
                    </div>
                  </div>
  
  
                  <div class="form-group">
                    <label class="col-md-3">टिकट <span class="red">*</span> </label>
                    <div class="col-md-7">
  
                      <input type="text" class="form-control" name="ticket" value="">
                    </div>
                  </div>
  
                  <div class="form-group">
                    <label class="col-md-3">कैफियत</label>
                    <div class="col-md-7">
  
                      <textarea name="remarks" class="form-control"></textarea> 
                    </div>
                  </div>
  
                  <div class="form-group">
                    <label class="col-md-3">&nbsp;</label>
                    <div class="col-md-7">
  
                      <input type="hidden" value="1" name="save_chalani_record">
                      <input type="hidden" value="" name="chalani_form_status">
  
                      <input type="hidden" name="letter_id" value="">
  
                      <button type="submit" class="btn bttn btn-success">
                        <span class="glyphicon glyphicon-plus"></span>रेकर्ड थप्नुहोस</button>
                      </div>
                    </div>
                  </form></div>
  
  
                  <div class="col-md-3">
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      * लागेको फिल्डहरु अनिवार्य रुपमा भर्नुपर्ने छ |
                    </div>
                  </div>
  
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>