<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class permAccountingVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:perm_accountings',
            'issuedDate' => 'required',
            'officeAddress' => 'required',
            'wardNum' => 'required',
            'organizationAddress' => 'required',
            'organizationWard' => 'required',
            'name' => 'required',
            'panDate' => 'required',
            'shopAddress' => 'required',
            'shopName' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'

        ];
    }
}
