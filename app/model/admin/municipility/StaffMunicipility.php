<?php

namespace App\model\admin\municipility;

use Illuminate\Database\Eloquent\Model;

class StaffMunicipility extends Model
{
    protected $table = 'staff_municipilities';

    public function provinces(){
    	return $this->belongsTo('App\model\setting\Province'::class);
    }

    public function districts(){
    	return $this->belongsTo('App\model\setting\District'::class);
    }

    public function staff_types(){
    	return $this->belongsTo('App\model\setting\StaffType'::class);
    }

    public function deginations(){
    	return $this->belongsTo('App\model\setting\Degination'::class);
    }

    public function departments(){
    	return $this->belongsTo('App\model\setting\Department'::class);
    }


}
