<?php

namespace App\Http\Requests\admin\setting\Floor;

use Illuminate\Foundation\Http\FormRequest;

class floorValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:30|unique:floors',
            'nameEng' => 'required|numeric'
        ];
    }
}
