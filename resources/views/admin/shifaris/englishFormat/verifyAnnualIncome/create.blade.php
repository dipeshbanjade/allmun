@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')

<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Verification Of Annual Income</header>

	</div>

	{{ Form::open(['route' => 'admin.verifyAnnualIncome.store', 'name' => 'frmWardverifyAnnualIncome', 'class'=>'sifarishSubmitForm']) }}
	@include('admin.shifaris.englishFormat.verifyAnnualIncome._form')
	
	@include('admin.shifaris.commonButton')
	{{ Form::close() }}
	<div class="msgDisplay" style="z-index: 999999 !important"></div>

</div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
@include('admin.shifaris.script')
@include('admin.shifaris.englishFormat.verifyAnnualIncome.script')
@endsection