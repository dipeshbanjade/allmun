<div class="right_col nep" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">व्यवसाय बन्द</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
         @include('admin.shifaris.municipalityDetail')
         <div class="row top-part">
          <div class="col-md-6">
           <p align="left"><b class="ps">प. सं.:
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
            @endif
          </b></p>
          <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNumber']) }}
          </b></p>
        </div>
        <div class="col-md-6">
          <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
          </p><h4 align="center"><b>विषय: व्यवसाय बन्द बारे।</b></h4>
          {{ Form::hidden('letter_subject', 'व्यवसाय बन्द बारे') }}
          <p></p>
        </div>
      </div>
      <div class="row content-para">
        <div class="col-md-12">
          <p>उपरोक्त विषयमा <b>
            {{ Form::text('municipality', isset($data->municipality) ? $data->municipality : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </b> वडा नं.  
          <b>
            {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
          </b> 
          (
          साविक 
          {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'organizationAddress']) }}
          <b>
            <select onchange="changeSelect(this)" name="organizationType">
              <option value="गा.वि.स." {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
              <option value="नगरपालिका" {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
              <option value="उप महानगरपालिका" {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
              <option value="महानगरपालिका" {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
            </select>
          </b>, वडा नं. 
          {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          )
          मा रहेका 
          <b><select onchange="changeSelect(this)" name="title">
            <option value="श्री" {{ !empty($data->title) && $data->title == 'श्री' ? 'selected' : '' }}>श्री</option>
            <option value="सुश्री" {{ !empty($data->title) && $data->title == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
            <option value="श्रीमती" {{ !empty($data->title) && $data->title == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
          </select>
        </b>
        {{ Form::text('name', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} को नाममा रहेको निम्न उल्लेखित व्यवसाय मिति {{ Form::text('closedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'closedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("closedDate")']) }} देखि बन्द भएको भनी निजले पेश गरेको निवेदन उपर स्थल सर्जमिन बुझ्दा मिति {{ Form::text('viewDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'viewDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("viewDate")']) }} को स्थलगत सर्जमिन बमोजिम निजले पेश गरेको व्यहोरा मनासिव देखिएको हुँदा सोही  प्रमाणित गरिन्छ।</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered" id="business_close_table">
            <thead>
              <tr><th>क्र.स.</th>
                <th>व्यवसायको नाम </th>
                <th>व्यवसायको प्रकृति </th>
                <th>वार्ड नं.</th>
                <th>दर्ता नं. </th>
                <th>टोलको नाम</th>
                <th>बाटोको नाम</th>
                <th>घर नं.</th>
              </tr></thead>
              <tbody>
                <?php 
                $count = 1;
                $tableData = !empty($data) ? json_decode($data->businessCloseDetail, true) : [1];
          //dd($tableData);
                foreach($tableData as $singleData){         
                  ?>
                  <tr>
                    <td>
                      <?php $count++ ?> 
                      {{ Form::text('', '१', ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                      
                    </td>
                    <td>
                      {{ Form::text('businessCategory[]', !empty($singleData['businessCategory']) ? $singleData['businessCategory'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('businessNature[]', !empty($singleData['businessNature']) ? $singleData['businessNature'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('ward[]', !empty($singleData['ward']) ? $singleData['ward'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('regNo[]', !empty($singleData['regNo']) ? $singleData['regNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('tollNo[]', !empty($singleData['tollNo']) ? $singleData['tollNo'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('bhatoNaam[]', !empty($singleData['bhatoNaam']) ? $singleData['bhatoNaam'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('gharNo[]', !empty($singleData['gharNo']) ? $singleData['gharNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>

                    @if($count == 2)
                    <td class="add-btns" style="border: 0px;">
                      <button type="button" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button>
                    </td>
                    @else
                    <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                    @endif

                    <!-- <td><a href="" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></a></td> -->
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="text-right btm-last">
            <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
            <p> <b>       
             <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
              @if(count($deginationsId) > 0)
              @foreach($deginationsId as $deg)
              <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                {{ $deg->nameNep }} 
              </option>
              @endforeach
              @endif
            </select>
          </b> </p>
        </div>
        <!--views for nibedak detail -->
        <div class="clearfix"></div>
        <hr>
        @include('admin.shifaris.nibedakCommonField')
        <!-- END -->
      </div>
      <div class="col-md-12">
        <hr>
      </div>
    </div>
  </div>
</div>