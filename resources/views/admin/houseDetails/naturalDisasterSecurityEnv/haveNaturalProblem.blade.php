 <div class="form-group col-md-12">
    <?php
       $haveGetDisProblem = isset($haveDisProblem) ? $haveDisProblem : '';
       $jsonProblemName  = isset($haveGetDisProblem->problemName) ?  json_decode($haveGetDisProblem->problemName) : '';
    ?>
    <p>
       २. तपाईको परिवार प्राकृतिक प्रकोपको कारणले विगत १ वर्षभित्र पिडित हुनु भएको छ ?
    </p>
    <div class="col-md-12">
        <label class="radio-inline">
            १. छ 
            <input type="radio" name="haveGetDiseasterProblem" value="1" class="rdoClickMe" {{ isset($haveGetDisProblem->haveGetDiseasterProblem) && $haveGetDisProblem->haveGetDiseasterProblem == 1 ? 'checked' : '' }}>
        </label> 
        <label class="radio-inline">
            २. छैन्
            <input type="radio" name="haveGetDiseasterProblem" value="0" class="rdoClickMe" {{ isset($haveGetDisProblem->haveGetDiseasterProblem) && $haveGetDisProblem->haveGetDiseasterProblem == 0 ? 'checked' : '' }}>
        </label>
        <label class="radio-inline col-md-12">
          <p>
              यदि छ भने प्राकृतिक प्रकोपका कारण विगत १ वर्ष भित्र तपाईको परिवारले के के समस्या भोग्नु पर्यो ?(बहुउत्तर सम्भव)
          </p>
        </label>
        <!--  -->
        <label class="radio-inline">
            <input type="checkbox" name="problemName[]" value="परिवारका सदस्यको मृत्यु" {{ !empty($haveGetDisProblem->problemName) && in_array('परिवारका सदस्यको मृत्यु', $jsonProblemName) ? 'checked' : '' }}>
            १. परिवारका सदस्यको मृत्यु
        </label><br />
        <label class="radio-inline">
            <input type="checkbox" name="problemName[]" value="परिवारका सदस्यको अंगभंग / घाइते"  {{ !empty($haveGetDisProblem->problemName) && in_array('परिवारका सदस्यको अंगभंग / घाइते', $jsonProblemName) ? 'checked' : '' }}>
            २. परिवारका सदस्यको अंगभंग / घाइते
        </label><br />
        <label class="radio-inline">
            <input type="checkbox" name="problemName[]" value="घर जग्गाको क्षति"  {{ !empty($haveGetDisProblem->problemName) && in_array('घर जग्गाको क्षति', $jsonProblemName) ? 'checked' : '' }}>
            ३. घर जग्गाको क्षति
        </label><br />
        <label class="radio-inline">
            <input type="checkbox" name="problemName[]" value="चौपायाको अंगभंग / घाइते">
            ५. चौपायाको अंगभंग / घाइते
        </label><br />
        <label class="radio-inline">
            <input type="checkbox" name="problemName[]" value="बालीनालीको क्षति"  {{ !empty($haveGetDisProblem->problemName) && in_array('बालीनालीको क्षति', $jsonProblemName) ? 'checked' : '' }}>
            ६. बालीनालीको क्षति
        </label><br />
        <label class="radio-inline">
             {{ Form::text('otherProblemName', isset($haveGetDisProblem->otherProblemName) ? $haveGetDisProblem->otherProblemName : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
        </label>
    </div>
     <div class="msgDisplay"></div>
</div> 
