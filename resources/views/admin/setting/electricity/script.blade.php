<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditelectricity']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of electricity is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmelectricity']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of electricity is required"
            }
        });
	var btnAddElectricity = $('.btnAddElectricity');
	btnAddElectricity.on('click', function(e){
		$('#electricityModel').modal('show'); 
	});

	var btnEditelectricity = $('.btnEditelectricity');
	btnEditelectricity.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.electricityId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editelectricityModel").modal('show');
			}
		})
	});

	var frmUpdateelectricity = $('#frmUpdateelectricity');
  frmUpdateelectricity.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.electricityId').val();
    var url  = "{{URL::to('/')}}" + "/setting/electricity/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editelectricityModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editelectricityModel').modal('hide');
});
});
</script>