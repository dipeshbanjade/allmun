@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
 <div class="card-box">
  <div class="card-body">
    <div class="card-head">
      <header><i class="fa fa-eye">
       &nbsp;@lang('commonField.extra.citizenType')</i>
     </header>
   </div>
   <button id="" class="btnAddCitizenType btn btn-primary pull-right">
     <i class="fa fa-plus"></i>
     <span> @lang('commonField.extra.createCitizenType')</span>
   </button>

   @if(count($allCitizenType) > 0)
   <div class="table-scrollable" id="dropLabel">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
     <thead>
      <tr>
       <!--       <th>@lang('commonField.extra.sn')</th> -->
       <th>@lang('commonField.extra.sn')</th>
       <th>@lang('commonField.extra.refCode')</th>
       <th>@lang('commonField.extra.department')</th>
       <th>@lang('commonField.extra.action') </th>
     </tr>
   </thead>
   <tbody>
    @php $count = 1  @endphp
    @foreach($allCitizenType as $citizenType)
    <tr class="odd gradeX">
     <td>{{ $count ++ }}</td>
     <td>{{ $citizenType->refCode }}</td>
     <td >{{ $citizenType->nameNep ?? 'N/A' }} - {{ $citizenType->nameEng }} </td>

     <td class="left" >
       @if(Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun')
       <a href="#" data-url="{{ route('citizenType.edit', $citizenType->id) }}" class="btnEditCitizen btn btn-primary btn-xs" id="" >
         <i class="fa fa-pencil"></i>
       </a>
       <a href="#" class="deleteMe" data-url="{{ route('citizenType.delete', $citizenType->id )}}" data-name="{{ $citizenType->nameEng }}">
        <button class="btn btn-danger btn-xs">
          <i class="fa fa-trash-o "></i>
        </button>
      </a>
      @else
      <small>@lang('commonField.extra.limited')</small>
      @endif
    </td>
  </tr>
  @endforeach
</tbody>
</table>
</div>
@endif
</div>
</div>
<div class="modal" id="citizenModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.addCitizenType')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ Form::open(['route' => 'citizenType.store', 'name'=>'frmcitizenType']) }}
        @include('admin.setting.citizenType._form')
        <div class="col-lg-12 p-t-20 text-right"> 
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
        </div>
        {{ Form::close() }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

      </div>

    </div>
  </div>
</div>


<!-- edit model for citizenType starts here -->

<div class="modal" id="editCitizenModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.editCitizenType')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       {{ Form::open(['name'=>'frmeditcitizenType', 'id' => 'frmUpdateCitizenType']) }}
       @include('admin.setting.citizenType._form')
       {{ Form::hidden('citizenId', null, ['class' => 'citizenId']) }}
       <div class="col-lg-12 p-t-20 text-right"> 
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
      </div>
      {{ Form::close() }}
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">

    </div>

  </div>
</div>
</div>

<!-- edit model for citizenType ends here -->
</div>
@endsection




@section('custom_script')
@include('admin.setting.citizenType.script')
@endsection