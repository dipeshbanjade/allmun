<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\StaffType;

use App\Http\Requests\admin\setting\staffType\staffTypeValidation;


class staffTypeController extends Controller
{
    protected $viewPath      = 'admin.setting.staffType';
    protected $deginationTbl = 'staff_types';

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $page['page_title']       = 'Staff Type : List';
        $page['page_description'] = 'list of staff type';

        $allStaffType = StaffType::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

        return view($this->viewPath . '.index', compact(['page', 'allStaffType']));
    }

    public function store(staffTypeValidation $request)
    {
        $mySave = StaffType::create([
                    'nameNep' => $request->nameNep ? $request->nameNep : 'N/A',
                    'nameEng' => $request->nameEng,
                    'status'  => 0,
                    'softDelete' => 0
                  ]);
        if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage'));
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }
   
    public function edit($id)
    {
        if ($id) {
           $edit = StaffType::findorFail($id);
                 return response()->json([
                          'success' => true,
                          'id'      => $edit->id,
                          'nameNep' => $edit->nameNep,
                          'nameEng' => $edit->nameEng
                         ]);
         }else{
            return response()->json([
                   'success' => false,
                   'message' => config('activityMessage.idNotFound')
                   ]);
         }
    }

    public function update(Request $request, $id)
        {
            $update = StaffType::findOrFail($id);
            $update->nameNep   = $request->nameNep;
            $update->nameEng  = $request->nameEng;

           $myUpdate = $update->update();

           if ($myUpdate) {
                return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.updateMessage')
                    ]);
            }else{
                return response()->json([
                    'success'  => false,
                    'message'  => config('activityMessage.notUpdated')
                    ]);
            }
        }


       public function destroy($id)
        {
            $del = StaffType::findOrFail($id);
           $del->softDelete = 1;
           $myDel = $del->update();
           if ($myDel) {
             return MyLogic::successChangeStatus($code = 200);
           }else{
              return MyLogic::successChangeStatus($code = 301);
           }
        }

    public function changeStatus($id){

    }
}
