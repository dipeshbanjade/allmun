<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
                <!-- 1 -->
                <div class="form-group col-md-12">
                    <p>
                       १. तपाईको परिवार बसोवास गरेको स्थानमा प्राकृतिक विपद वा प्रकोपको खतरा तथा भू धरातलीय जोखिमहरु छन् ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-12">
                            <p>
                                यदि छ भने तपाईको परिवार बसोवास गरेको स्थानमा कस्तो कस्तो किसिमको प्राकृतिक विपद वा प्रकोपको खतराहरु छन् ? (बहुउत्तर सम्भव छ) 
                            </p>
                        </label>
                        <label class="radio-inline col-md-6">
                            १. सुख्खा खडेरी
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            २. आगलागी (बस्तीमा)
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३. बन डडेलो
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ४. बाढी / ठूलो भल (पानी) आउने
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ५. डुवान
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ६. आँधी÷हुरीबतास
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ७. चट्याङ्ग 
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                             ८. असिना
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ९. जमिन भासिने / भ्वाङ पर्ने
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            १०. अतिवृष्टि
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ११. खण्डबृष्टि
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            १२. पहिरो / भूक्षय
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            १३. कृषि बालीमा रोग÷किराको प्रकोप
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                             {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)', 'id' => '']) }}
                        </label>
                    </div>
                </div> 
                <div class="form-group col-md-12">
                    <p>
                       २. तपाईको परिवार प्राकृतिक प्रकोपको कारणले विगत १ वर्षभित्र पिडित हुनु भएको छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-12">
                          <p>
                              यदि छ भने प्राकृतिक प्रकोपका कारण विगत १ वर्ष भित्र तपाईको परिवारले के के समस्या भोग्नु पर्यो ?(बहुउत्तर सम्भव)
                          </p>
                        </label>
                        <label class="radio-inline col-md-6">
                            १. परिवारका सदस्यको मृत्यु
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            २. परिवारका सदस्यको अंगभंग / घाइते
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३. घर जग्गाको क्षति
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ५. चौपायाको अंगभंग / घाइते
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ६. बालीनालीको क्षति
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                             {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
                        </label>
                    </div>
                <div class="form-group col-md-12">
                    <p>
                       ३. तपाईको परिवार प्राकृतिक प्रकोपको कारणबाट विस्थापित हुनु पर्ने अवस्थामा छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                        </label>
                    </div>
                </div> 
                </div> 
                <div class="form-group col-md-12">
                    <p>
                       ४. तपाईको परिवारले विगत १ वर्ष भित्र वन्यजन्तुको कारणबाट समस्या भोग्नुपरेको छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-12">
                          <p>
                              यदि छ भने, बन्यजन्तुको कारणले कस्तो कस्तो किसिमको समस्या भोग्नु परेको छ ? 
                          </p>
                        </label>
                        <label class="radio-inline col-md-6">
                            १. परिवारका सदस्यको मृत्यु
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            २. परिवारका सदस्यलाई आक्रमण । अंगभंग । घाइते
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३. घरको क्षति
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ५. चौपायाको अंगभंग । घाइते 
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ६. बालीनालीको क्षति
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                             {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
                        </label>
                    </div>
                </div> 
                <div class="form-group col-md-12">
                    <p>
                       ५ तपाईको परिवार विगत १ वर्षभित्र निम्न अपराधिक घटनाबाट पिडित हुनु भएको छ ? (बहुउत्तर सम्भव)
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. हत्या
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            २. बलात्कार
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३. अपहरण
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ४. डर, धम्की
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ५. चोरी
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ६. डकैती
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ७. चेलीबेटी बेचबिखन
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ९.अन्य
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            १०. कुनै पनि घटनाबाट पिडित नभएको 
                            <input type="checkbox" name="radio" value="">
                        </label>
                    </div>
                </div> 
                <div class="form-group col-md-12">
                    <p>
                       ६. तपाईको विचारमा यस स्थानमा जातजाति, लिगं वा समुदाय विशेष (जस्तै महिला, दलित, आदि ...) भएकै आधारमा भेदभावको अनुभव गर्नु भएको छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            २. छैन
                            <input type="radio" name="radio" value="">
                        </label>
                    </div>
                </div>

                <div class="form-group col-md-12">
                    <p>
                       ७. तपाईको परिवार बसोवास गरेको स्थान वरीपरिको मुख्य वातावरणीय समस्या के रहेको छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. प्रदुषित पानी
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            २. अव्यवस्थित ठोस फोहोर
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३. अव्यवस्थित ढल निकास
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ४. धुलो धुवाँ
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ५. ध्वनी प्रदुषण
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ६. जथाभावी पार्किङ
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ७. भिडभाड 
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                           ८. कुनै समस्या नभएको
                            <input type="checkbox" name="radio" value="">
                        </label>
                    </div>
                </div>
            </div>
            <div class="msgDisplay"></div>
            
    </div>
</div>