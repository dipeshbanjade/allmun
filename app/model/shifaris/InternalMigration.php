<?php

namespace App\model\shifaris;

use Illuminate\Database\Eloquent\Model;

class InternalMigration extends Model
{
    protected $table = 'internal_migrations';
}
