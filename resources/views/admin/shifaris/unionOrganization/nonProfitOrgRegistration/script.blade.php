<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardNonProfitOrgRegistration']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber: {
					required : true,
					minlength: 1,
					maxlength: 30
				},
				dartaCode : {
					required : true,
					minlength : 1,
					maxlength : 30					
				},
				dartaDate : {
					required : true,
					date: true
				},
				companyName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				companyAddress : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				subjectArea : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				businessStartDate : {
					required : true,
					date: true
				},
				companyEmail : {
					required : true,
					email : true
				},
				companyContact : {
					required : true,
					minlength : 5,
					maxlength : 15,
					number: true
				},
				operatorName : {
					required : true,
					minlength : 5,
					maxlength : 30
				},
				operatorAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				operatorEmail : {
					required : true,
					email : true
				},
				operatorContact : {
					required : true,
					minlength : 7,
					maxlength : 15,
					number: true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number must be numberic",
				dartaCode : "Darta code is required",
				dartaDate : "Invalid darta date",
				companyName : "Company name is required",
				companyAddress : "Company Address is required",
				companyEmail : "Company Email is required",
				subjectArea : "Subject area is required",
				businessStartDate : "Invalid date entry",
				companyContact : "Company contact number is required",
				operatorName : "Operator Name is required",
				operatorAddress : "Operator Address is required",
				operatorEmail : "Operator Email id required",
				operatorContact : "Operator contact number is required",
				authorizedPerson : "Authorizing person is required"
			}
		});
	});


	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='operatorName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	                  $("input[name='operatorAddress']").val(response.villageNameNep).trigger('input');
	                  $("input[name='operatorEmail']").val(response.email).trigger('input');
	                  $("input[name='operatorContact']").val(response.phoneNumber).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>