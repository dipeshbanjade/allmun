<div class="row top-part">
  <div class="col-md-6">
   <p align="left">
     <b>@lang('commonField.extra.refCode'):</b>
     @if(isset($data->refCode))
     {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
     @else
     {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
     @endif
   </p>
   <p align="left">
     <b> च. नं.:
       {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
     </b>
   </p>
 </div>
 <div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    <h4 align="center">
      <b>विषय: खाता बन्द गरिदिने सम्बन्धमा।</b>
    </h4>
    {{ Form::hidden('letter_subject', 'खाता बन्द गरिदिने सम्बन्धमा', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">
     <b>श्री 
       {{ Form::text('accountHolderName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderName', 'required' => 'required']) }}
     </b>,
   </p>
   <p align="left">
     <b>
       {{ Form::text('accountHolderLocation', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderLocation', 'required' => 'required']) }}।
     </b>
   </p>
 </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">
    <p>प्रस्तुत विषयमा 
      <b>
       {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
     </b> 
     वडा नं. 
     <b>
      {{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNum', 'required' => 'required']) }}
    </b>
    (साविक 
    <!--  -->
    {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationAddress', 'required' => 'required']) }}
    <!--  -->
    <b>
      <select onchange="changeSelect(this)" name="organizationType">
        <option value="गा.वि.स." {{!empty($data->organizationType) && $data->organizationType == 'गा.वि.स.'? 'selected' : '' }}>गा.वि.स.</option>
        <option value="नगरपालिका" {{!empty($data->organizationType) && $data->organizationType == 'नगरपालिका'? 'selected' : '' }}>नगरपालिका</option>
        <option value="उप महानगरपालिका" {{!empty($data->organizationType) && $data->organizationType == 'महानगरपालिका'? 'selected' : '' }}>उप महानगरपालिका</option>
        <option value="महानगरपालिका" {{!empty($data->organizationType) && $data->organizationType == 'महानगरपालिका'? 'selected' : '' }}> महानगरपालिका</option>
      </select>
    </b>,
    वडा नं.  
    {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationWard', 'required' => 'required']) }}
    ) 
    द्धारा  संचालन भएको  
    {{ Form::text('workName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'workName', 'required' => 'required']) }}
    कार्य सम्पन्न भएकोले 
    उक्त कार्य सम्पन्न भएकोले सो  कार्यका लागि                 संचालन भएको खाता नं.
    {{ Form::text('accountNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountNum', 'required' => 'required']) }}
    को खाता बन्द गरी दिनुहुन अनुरोध गरिन्छ।
  </p> 
</div>
</div>
<div class="text-right btm-last">
  <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required" value="{{!empty($data)? $data->authorizedPerson : NULL}}"></p>
  <p> 
    <b>
      <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
        @if(count($deginationsId) > 0)
        @foreach($deginationsId as $deg)
        <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
          {{ $deg->nameNep }} 
        </option>
        @endforeach
        @endif
      </select>
    </b>
  </p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>