<div class="right_col nep" role="main" style="min-height: 521px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">फरक फरक जन्म मिति प्रमाणित</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

         @include('admin.shifaris.municipalityDetail')

         <div class="row top-part">
          <div class="col-md-6">
           <p align="left">
            <b>@lang('commonField.extra.refCode'):</b>
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
            @endif
          </p>
          <p align="left">
            <b> च. नं.:
              {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
            </b>
          </p>
        </div>
        <div class="col-md-6">
          <p align="right">
            <b class="mt">मिति :
              {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
            </b>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24"></p>
          <h4 align="center">
            <b>विषय: फरक फरक जन्म मिति प्रमाणित।</b>
          </h4>
          {{ Form::hidden('letter_subject', 'फरक फरक जन्म मिति प्रमाणित।', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
          <p></p>
        </div>
      </div>
      <div class="row content-para">
        <div class="col-md-12">
          <p align="left">
           <b>
             {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
           </b>
           महा/उपमाहा/नगरपालिका/गाउँपालिका  वडा नं. 
           <b>
             {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
           </b>
           (साविक 
           <!--SABIK ADDRESS START -->
           {{ Form::text('municipalityAddress', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'municipalityAddress', 'required' => 'required']) }}
           <!-- SABIK ADDRESS END -->
           <b>
             <select onchange="changeSelect(this)" name="municipalityType">
              <option value="गा.वि.स." 
              {{ !empty($data->municipalityType) && $data->municipalityType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
              <option value="नगरपालिका" 
              {{ !empty($data->municipalityType) && $data->municipalityType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
              <option value="उप महानगरपालिका" 
              {{ !empty($data->municipalityType) && $data->municipalityType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
              <option value="महानगरपालिका" 
              {{ !empty($data->municipalityType) && $data->municipalityType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
            </select>
          </b>
          , वडा नं.
          {{ Form::text('municipalityWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityWard', 'required' => 'required']) }}
          )
          निवासी
          <b>
            <select onchange="changeSelect(this)" name="personPrefix">
              <option value="श्री"
              {{ !empty($data->personPrefix) && $data->personPrefix == 'श्री' ? 'selected' : '' }}>श्री</option>
              <option value="सुश्री"
              {{ !empty($data->personPrefix) && $data->personPrefix == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
              <option value="श्रीमती"
              {{ !empty($data->personPrefix) && $data->personPrefix == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
            </select>
          </b>
          {{ Form::text('personName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'personName', 'required' => 'required']) }} को निम्न उल्लेखित विवरण अनुसार कागजातमा जन्म मिति फरक हुन गएको हुनाले सो फरक हुन् गएको जन्म मिति भएको व्यक्ति एकै भएको प्रमाणित गरि पाउन भनी यस कार्यालयमा पेश हुन आएको निवेदन तथा सोसथ संलग्न कागजातका आधारमा निजले पेश गरेको व्होरा मनासिव सो फरक जन्म मिति भएको व्येक्ति एकै भएको व्यहोरा सिफारिस गरिन्छ। </p>
        </div>
        <p align="center"><b>फरक जन्म मिति र कागजात विवरण </b></p>
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-bordered" id="diff_date_table">
              <thead>
                <tr>
                  <th>हुन पर्ने जन्म मिति</th>
                  <th>फरक भएको जन्म मिति </th>
                  <th>फरक भएको कागजात</th>
                </tr></thead>
                <tbody>
                  <?php 
                  $count = 1;
                  $tableData = !empty($data) ? json_decode($data->tableData, true) : [1];
          //dd($tableData);
                  foreach($tableData as $singleData){         
                    ?>
                    <tr>
                      <td>
                        {{ Form::text('actualDate[]', !empty($singleData['actualDate']) ? $singleData['actualDate'] : NULL, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'actual_date',  'onfocus' => 'showNdpCalendarBox("actual_date")']) }}
                      </td>
                      <td>
                        {{ Form::text('diffDate[]', !empty($singleData['diffDate']) ? $singleData['diffDate'] : NULL, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'diff_date',  'onfocus' => 'showNdpCalendarBox("diff_date")']) }}
                      </td>
                      <td>
                        {{ Form::text('diffPaper[]', !empty($singleData['diffPaper']) ? $singleData['diffPaper'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                      </td>
                      <td class="add-btns" style="border: 0px;">
                        <button type="button" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button>
                      </td>
                      <!-- <td><a href="" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></a></td> -->
                    </tr>
                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="text-right btm-last">
            <p>
             {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} 
           </p>
           <p><b>  
            <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
              @if(count($deginationsId) > 0)
              @foreach($deginationsId as $deg)
              <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                {{ $deg->nameNep }} 
              </option>
              @endforeach
              @endif
            </select>
          </b> </p>
        </div>
        <!--views for nibedak detail -->
        <div class="clearfix"></div>
        <hr>
        @include('admin.shifaris.nibedakCommonField')
        <!-- END -->
        <div class="col-md-12">
          <hr>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
