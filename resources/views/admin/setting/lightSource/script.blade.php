<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditlightSource']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of light source is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmlightSource']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of light source is required"
            }
        });
	var btnAddlightSource = $('.btnAddlightSource');
	btnAddlightSource.on('click', function(e){
		$('#lightSourceModel').modal('show'); 
	});

	var btnEditlightSource = $('.btnEditlightSource');
	btnEditlightSource.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.lightSourceId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editlightSourceModel").modal('show');
			}
		})
	});

	var frmUpdatelightSource = $('#frmUpdatelightSource');
  frmUpdatelightSource.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.lightSourceId').val();
    var url  = "{{URL::to('/')}}" + "/setting/lightSource/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editlightSourceModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editlightSourceModel').modal('hide');
});
});
</script>