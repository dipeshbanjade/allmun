<?php

namespace App\Http\Controllers\admin\shifaris\nepaliFormat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\BirthDateVerifyNep;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\birthDateVerifyNepVal;
use DB;
use Auth;

use App\model\mylogic;
class birthDateVerifyNepController extends Controller
{
  protected $viewPath = 'admin.shifaris.nepaliFormat.birthDateVerifyNep';
  protected $shifarisTableName     = 'birth_date_verify_neps';
  protected $appUsrTable     = 'applicant_users';
  protected $deg             = 'deginations';
  protected $prefix;
  protected $refCode;


  public function __construct()
  {
    $this->middleware('auth');
    $this->prefix      = config('activityMessage.prefixBirthDateVerifyNep');
    $this->refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

  }

  public function index()
  {
    $page['page_title']       = 'Birth Date Verify Nepali Recommendation';
    $page['page_description'] = 'Birth Date Verify Nepali Recommendation';

        //TODO accordkign to ward user dispaly recored
    $instRecomd = BirthDateVerifyNep::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
    
  }

  public function create()
  {
    $page['page_title']       = 'Birth Date Verify Nepali Recommendation : create';
    $page['page_description'] = 'Birth Date Verify Nepali Recommendation details';

    $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);
    
    
    $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
    return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
    
  }

  public function store(birthDateVerifyNepVal $request)
  {
    
   DB::beginTransaction();
   try {
            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $this->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


    $isShifarisExist = isset($request->shifarisId) ? BirthDateVerifyNep::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
    
    $save = !empty($isShifarisExist['id']) ? BirthDateVerifyNep::findOrfail($isShifarisExist['id']) : new BirthDateVerifyNep;


    $save->createdBy = Auth::user()->id;

    $save->refCode = $this->refCode;

    $save->wards_id = Auth::user()->wards_id ?? NULL;

    $save->municipilities_id = getMunicipalityData()['id'] ?? "";
    
    $save->issuedDate = $request->issuedDate;
    $save->chalaniNumber = $request->chalaniNumber;
    $save->fatherName = $request->fatherName;
    $save->motherName = $request->motherName;
    $save->municipalityName = $request->municipalityName;
    $save->wardNumber = $request->wardNumber;
    $save->sabikAddress = $request->sabikAddress;
    $save->personName = $request->personName;
    $save->birthDate = $request->birthDate;
    $save->birthPlace = $request->birthPlace;


    $save->authorizedPerson = $request->authorizedPerson;
    $save->deginations_id = $request->deginations_id;

    $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
    $save->applicant_users_id  = $citizenInfo[0];             

    $mySave = $save->save();

    $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
    $request->chalaniNum = $request->chalaniNumber;
    $citizenShifarish = true;
    if(empty($isShifarisExist['id'])){
      $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
    }            
    if ($mySave && $log && $citizenShifarish) {
     DB::commit();
     return response()->json([
       'success' => true,
       'message' => config('activityMessage.saveMessage')
     ]);
               // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
   }else{
     return response()->json([
       'success' => false,
       'message' => config('activityMessage.unSaveMessage')
     ]);
               // return back()->withMessage(config('activityMessage.unSaveMessage'));
   }
 }catch (Exception $e) {
  DB::rollback();
  return back()->withMessage(config('activityMessage.dataNotInserted'));
}


}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
