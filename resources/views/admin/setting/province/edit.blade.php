@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
   <div class="col-sm-12 col-md-12">
     <div class="card-box">
       <div class="card-body">
       <div class="card-head">
           <header><i class="fa fa-eye">
             &nbsp;@lang('commonField.button.update')</i>
           </header>
       </div>

            {{ Form::model($data, ['route' => ['admin.province.update', $data->id], 'method' => 'post', 'name' => 'frmProvince']) }}
                 @include('admin.setting.province._form')
                 <div class="col-lg-12 p-t-20 text-right"> 
                     {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}

                     <button type="button" class="btn btn-danger" onclick="history.go(-1)">@lang('commonField.button.back')</button>
                 </div>
            {{ Form::close() }}
       </div>
       </div>
     </div>
@endsection
@section('custom_script')
  @include('admin.setting.province.script')
@endsection
