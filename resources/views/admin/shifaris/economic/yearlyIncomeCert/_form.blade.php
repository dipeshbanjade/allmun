<div class="row top-part">
	<div class="col-md-6">
		<p align="left"><b class="ps">प. सं.:
			@if(isset($data->refCode))
			{{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
			@else
			{{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
			@endif
		</b></p>
		<p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}</b></p>
	</div>
	<div class="col-md-6">
		<p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<p align="center" class="font-size-24">
		</p><h4 align="center"><b>विषय: प्रमाणित सिफारिस गरिएको बारे।</b> </h4>
		{{ Form::hidden('letter_subject', 'प्रमाणित सिफारिस गरिएको बारे') }}
		<p></p>
	</div>
</div>
<div class="row title-left">
	<div class="col-md-12">
		<p>श्री यो जो सँग सम्बन्ध छ।</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12 content-para">
		<p align="justify">
			उपरोक्त विषयमा {{ Form::text('topic', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'topic']) }} बस्ने {{ Form::text('personAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'personAddress']) }} ले दिनु भएको निवेदन अनुसार
			{!! Form::textarea('personDetails', null, ['class'=>'form-control', 'required'=>'required', 'id' => 'personDetails', 'rows' => '4', 'cols' => '50']) !!} आम्दानी आयस्रोत भएको प्रमाणित सिफारिस गरिएको व्यहोरा अनुरोध छ।
		</p> 
	</div>
</div>
<div class="col-md-12">
	<div class="table-responsive">
		<table class="table table-bordered" id="amdani_bibaran_table">
			<thead>
				<tr><th>क्र. स.</th>
					<th>विवरण</th>
					<th>ज.वि.</th>
					<th>मन/मासिक रु. </th>
					<th>दर रु.</th>
					<th>बार्षिक रु.</th>
					<th>कैफियत </th>
				</tr></thead>
				<tbody>
					<?php
						$count=1;
						$tableData= !empty($data)? json_decode($data->yearlyIncomeStatement,true):[1];

						foreach ($tableData as $singleData) {
					?>
						<tr>
							<td>{{$count++ }}</td>
							<td>{{ Form::text('detail[]', !empty($singleData['detail'])? $singleData['detail']: NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
							<td>{{ Form::text('areaDetail[]', !empty($singleData['areaDetail'])? $singleData['areaDetail']: NULL, ['class'=>'dashed-input-small-field']) }}</td>
							<td>{{ Form::text('monthlyIncome[]', !empty($singleData['monthlyIncome'])? $singleData['monthlyIncome']: NULL, ['class'=>'dashed-input-field']) }}</td>
							<td>{{ Form::text('rate[]', !empty($singleData['rate'])? $singleData['rate']: NULL, ['class'=>'dashed-input-field']) }}</td>
							<td>{{ Form::text('yearlyIncome[]', !empty($singleData['yearlyIncome'])? $singleData['yearlyIncome']: NULL, ['class'=>'dashed-input-field']) }}</td>
							<td>{{ Form::text('remarks[]', !empty($singleData['remarks'])? $singleData['remarks']: NULL, ['class'=>'dashed-input-field']) }}</td>
							@if ($count == 2)
								{{-- expr --}}
							<td class="add-btns" style="border: 0px;"><a href="" id="add_more_barsik_amdani" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
							@else
							<td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
							@endif
						</tr>
						<?php }?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12">
		<div class="text-right btm-last">
			<p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
			<p> <b>
				<select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
					@if(count($deginationsId) > 0)
					@foreach($deginationsId as $deg)
					<option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
						{{ $deg->nameNep }} 
					</option>
					@endforeach
					@endif
				</select>
			</b></p>
		</div>
		<!--views for nibedak detail -->
		<div class="clearfix"></div>
		<hr>
		@include('admin.shifaris.nibedakCommonField')
		<!-- END -->
	</div>
</div>
<div class="col-md-12">
	<hr>
</div>

