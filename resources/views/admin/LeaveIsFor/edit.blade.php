@extends('main.app')
@section('custom_css')
    @include('admin.LeaveIsFor.style')
@endsection
@section('content')
    <div class="container">
        <div class="wrap row">
            <div class="col-md-6 col-lg-6 col-sm-12">
                <h4>Edit Official Leave</h1>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12">
                <a href="/leaveList" class="btn btn-primary create-button pull-right" >Holiday's List</a>
            </div>
        </div>
        <div class="clear"></div>
        <form action="/leaveisfor/{{$leave->id}}/update" method="post">
            {{ csrf_field() }}
            <div class="wrap" style="padding-bottom: 55px;">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-12"><label class = "mdl-textfield__label center" style="margin-top: 25px;">@lang('commonField.extra.startDate')</label></div>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
                            <input type="text" class="mdl-textfield__input" name="dateFromNep" value="{{$leave->dateFromNep}}" autocomplete="off" id="myDate" required>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input type="text" class="mdl-textfield__input" name="dateFromEng" value="{{$leave->dateFromEng}}" autocomplete="off" id="myEngDate" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-12">  <label class = "mdl-textfield__label center" style="margin-top: 25px;">@lang('commonField.extra.endDate')</label></div>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
                            <input type="text" class="mdl-textfield__input" name="dateToNep" value="{{$leave->dateToNep}}" autocomplete="off" id="myDateLeave" required>    
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input type="text" class="mdl-textfield__input" name="dateToEng" value="{{$leave->dateToEng}}" autocomplete="off" id="myEngDateLeave" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-12"><label class = "mdl-textfield__label center" style="margin-top: 25px;">@lang('commonField.extra.leaveReason')</label></div>
                    <div class="col-md-8 col-lg-8 col-sm-12">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                            <input type="text" class="mdl-textfield__input" name="leaveFor" value="{{$leave->leaveFor}}" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <button type="submit" class='btn btn-success pull-right'>@lang('commonField.button.submit')</button>
                
            </div>
        </form>
    </div>
    
@endsection
@section('custom_script')
    @include('admin.LeaveIsFor.script')
@endsection