<?php

namespace App\Http\Requests\admin\setting\houseWallType;

use Illuminate\Foundation\Http\FormRequest;

class HouseWallTypeValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:house_wall_types',
            'nameEng' => 'required|min:4|max:50'
        ];
    }
}
