<?php

namespace App\Http\Controllers\admin\shifaris\englishFormat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\admin\shifaris\IdentifyVerifyTwoValidation;
use App\model\shifaris\IdentifyVerifyTwo;
use App\model\shifaris\ApplicantUser;
use App\model\mylogic;
use DB;
use Auth;


class IdentityVerificationTwoController extends Controller
{
    protected $viewPath = 'admin.shifaris.englishFormat.identityVerificationTwo';
    protected $shifarisTableName     = 'identify_verify_twos';
    protected $appUsrTable     = 'applicant_users';
    protected $deginationsTbl = 'deginations';
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.IdentifyVerifyTwoPrefix');

    }

    public function index()
    {
        $page['page_title']       = 'Identity Verification Two';
        $page['page_description'] = 'Identity Verification Two';


        
        $identify_verify_twos = ApplicantUser::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

    }

    public function create()
    {
        $page['page_title']       = 'Identity Verification Two : create';
        $page['page_description'] = 'Identity Verification Two details';

        $getUniqueCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);
        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationsTbl, $orderColumn = 'nameNep', $orderby = 'ASC');
        return view($this->viewPath . '.create', compact(['page', 'getUniqueCode', 'deginationsId']));

    }

    public function store(IdentifyVerifyTwoValidation $request)
    {
        
       DB::beginTransaction();
       try {

        // $appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Shifaris', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


        $isShifarisExist = isset($request->shifarisId) ? IdentifyVerifyTwo::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? IdentifyVerifyTwo::findOrfail($isShifarisExist['id']) : new IdentifyVerifyTwo;

        $save->createdBy = Auth::user()->id;
        
        $save->refCode  = $request->refCode;

        $save->wards_id  = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";
        $save->issuedDate  = $request->issuedDate;
        $save->chalaniNum  = $request->chalaniNum;
        $save->letterSub = $request->letterSub;
        $save->appName  = $request->appName;
        $save->appTitle  = $request->appTitle;
        $save->municipality  = $request->municipality;
        $save->ward  = $request->ward;
        $save->district  = $request->district;
        $save->former  = $request->former;
        $save->orgAddr  = $request->orgAddr;
        $save->orgType  = $request->orgType;
        $save->orgWard  = $request->orgWard;
        $save->citizenshipNo  = $request->citizenshipNo;
        $save->passportNo  = $request->passportNo;
        $save->name  = $request->name;
        $save->citizenshipDate  = $request->citizenshipDate;
        $save->passportDate  = $request->passportDate;
        $save->marriageNo  = $request->marriageNo; 
        $save->marriageDate  = $request->marriageDate;
        $save->FemaleName  = $request->FemaleName;
        $save->actualName  = $request->actualName;
        $save->nameOpt  = $request->nameOpt;
        $save->authorizedPerson  = $request->authorizedPerson;
        $save->deginations_id  = $request->deginations_id;

        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

               //end for login
        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Shifaris', $tableName = $this->shifarisTableName, $tblId = $save->refCode);

        
                    $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }

    }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }


}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
