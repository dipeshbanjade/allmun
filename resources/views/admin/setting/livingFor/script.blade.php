<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditlivingFor']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of living For is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmlivingFor']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of Living For is required"
            }
        });
	var btnAddLivingFor = $('.btnAddLivingFor');
	btnAddLivingFor.on('click', function(e){
		$('#livingForModel').modal('show'); 
	});

	var btnEditlivingFor = $('.btnEditlivingFor');
	btnEditlivingFor.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.livingForId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editlivingForModel").modal('show');
			}
		})
	});

	var frmUpdatelivingFor = $('#frmUpdatelivingFor');
  frmUpdatelivingFor.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.livingForId').val();
    var url  = "{{URL::to('/')}}" + "/setting/livingFor/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editlivingForModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editlivingForModel').modal('hide');
});
});
</script>