<script type="text/javascript">

	function myFunction(e) {
		document.getElementById("paperWrong").value = e.target.value
		document.getElementById("paperWrong1").value = e.target.value
		document.getElementById("paperWrong2").value = e.target.value
	}
	function relation(e) {
		if(e.target.value == 'श्रीमान' || e.target.value == 'श्रीमती'){
			document.getElementById("rel").value = e.target.value + 'को';
		}
		else{
			document.getElementById("rel").value = '';
		}

	}

	$(function(){
		/*add buttoin*/
		$("form[name='frmWarddiffIndividualCert']").validate({
			rules:{

				refCode : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 2
				},
				municipalityAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityType : {
					required : true				
				},
				municipalityWard : {
					required : true,
					minlength : 1,
					maxlength : 3
				},
				personPrefix : {
					required : true				
				},
				personName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				citizenshipNumber : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				citizenshipDate : {
					required : true,
					date : true
				},
				mistakeType : {
					required : true				
				},
				wrongDetail : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				rel_type : {
					required : true
				},
				paperHolder : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				paperType : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				paperWrongInfo : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				paperName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				paperWrongInfo2 : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				name : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				documentName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				documentWrongInfo : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				refCode : "Ref Code is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				municipalityName : "Municipality Name is required",
				wardNumber : "Ward Number is required",
				municipalityAddress : "Municipality Address is required",
				municipalityType : "Municipality Type is required",
				municipalityWard : "Municipality Ward is required",
				personPrefix : "Person Prefix is required",
				personName : "Person Name is required",
				citizenshipNumber : "Citizenship Number is required",
				citizenshipDate : "Citizenship Date is required",
				mistakeType : "Mistake Type is required",
				wrongDetail : "Wrong Detial is required",
				rel_type : "Relation Type is required",
				paperHolder : "Paper Holder Name  is required",
				paperType : "Paper Type is required",
				paperWrongInfo : "Paper Wrong Info is required",
				paperName : "Paper Name is required",
				paperWrongInfo2 : "Paper Wrong INfo is required",
				name : "Name is required",
				documentName : "Document Name is required",
				documentWrongInfo : "Document Wrong Info is required",
				authorizedPerson : "Authorized person is required"
			}
		});
	});


	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='personName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>