<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class WasteManagement extends Model
{
    protected $table = 'waste_managements';
    protected $fillable = ['refCode','nameEng','nameNep','tag','status','softDelete'];
}
