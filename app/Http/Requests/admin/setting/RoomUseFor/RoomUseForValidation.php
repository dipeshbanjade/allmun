<?php

namespace App\Http\Requests\admin\setting\RoomUseFor;

use Illuminate\Foundation\Http\FormRequest;

class RoomUseForValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'nameEng' => 'required|min:4:min:50'
        ];
    }
}