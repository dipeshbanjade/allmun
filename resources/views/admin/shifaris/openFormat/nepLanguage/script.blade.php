<script type="text/javascript">
	var btnAddBodartha = $('#add');
	btnAddBodartha.on('click', function(e){
		e.preventDefault();		
		var bodartha = '<input type="text" class="form-control" name="bodartha[]">';
		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+bodartha+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#bodarthaTable').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});
	});
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardnepLanguage']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum : {
					required: true,
					minlength: 2,
					maxlength: 30,					
				},
				organizationName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				shifarishSubject : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				note : {
					required : true,
					minlength : 20,
					maxlength : 3000
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				refCode : "Ref code is required",
				issuedDate : "Issue Date is required",
				organizationName : "Name Of Office is required",
				organizationAddress : "Office address is required",
				shifarishSubject : "Subject is required",
				note : "Letter Description is required",
				authorizedPerson : "Authorized Person is required"
			}
		});
	});
</script>
