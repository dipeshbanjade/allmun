@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-md-12 col-sm-12">
    <div class="card card-box">
        <div class="card-head">
            <header>Password Change</header>
        </div>
        <div class="card-body col-md-8 col-lg-6 col-sm-12 pw-card" id="bar-parent">
             <h3><label><i class="fa fa-email">{{ Auth::user()->email }}</i></label></h3><hr>
                {{ Form::open(['route' => 'changePassword.save', 'name' => 'frmChangePassword']) }}
                <div class="form-group">
                    <label for="simpleFormEmail">Old Password</label>
                    <input type="text" name="oldPassword" class="form-control" placeholder="Enter old password">
                </div>
                <div class="form-group">
                    <label for="New Password">New Password</label>
                    <input type="password" name="password" class="form-control" placeholder="new password">
                </div>
                <div class="form-group">
                    <label for="Confirm Password">Confirm Password</label>
                    <input type="password" name="confirmPassword" class="form-control" placeholder="confirm password">
                </div>
                {{ Form::submit('Change Password', ['class' => 'btn btn-success']) }}
               {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
@section('custom_script')
  @include('admin.staff.script')
@endsection