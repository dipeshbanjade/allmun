<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseTypesTable extends Migration
{
    public function up()
    {
        Schema::create('house_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode', 100)->unique();
            $table->string('houseNep')->nullable();
            $table->string('houseEng')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('house_types');
    }
}
