<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class hsIncomeExpensDetails extends Model
{
    protected $table = 'hs_income_expens_details';
}
