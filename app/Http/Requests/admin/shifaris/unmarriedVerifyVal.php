<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class unmarriedVerifyVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:unmarried_verifications',
            'issuedDate' => 'required',
            'letterSub' => 'required',
            'appName' => 'required',
            'appRelation' => 'required',
            'fatherName' => 'required',
            'motherName' => 'required',
            'citizenNo' => 'required',
            'municipality' => 'required',
            'ward' => 'required',
            'district' => 'required',
            'former' => 'required',
            'orgAddr' => 'required',
            'orgType' => 'required',
            'orgWard' => 'required',
            'appTitleOpt' => 'required',
            'appGenderOpt' => 'required',
            'dateInAD' => 'required',
            'authorizedPerson' => 'required'
            
        ];
    }
}
