<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditcountry']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of country is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmcountry']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of country is required"
            }
        });
	var btnAddcountry = $('.btnAddcountry');
	btnAddcountry.on('click', function(e){
		$('#countryModel').modal('show'); 
	});

	var btnEditcountry = $('.btnEditcountry');
	btnEditcountry.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.countryId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editcountryModel").modal('show');
			}
		})
	});

	var frmUpdatecountry = $('#frmUpdatecountry');
  frmUpdatecountry.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.countryId').val();
    var url  = "{{URL::to('/')}}" + "/setting/country/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editcountryModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editcountryModel').modal('hide');
});
});
</script>