<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\FiscalYear;
class fiscalYearController extends Controller
{
    protected $viewPath = 'admin.setting.fiscalYear';
    protected $fisYearTypeTbl = 'fiscal_years';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $page['page_title']       = 'Fiscal Year';
        $page['page_description'] = 'Fiscal Year Decription';

        $allFiscalYear = FiscalYear::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        return view($this->viewPath . '.index', compact(['page', 'allFiscalYear']));
    }


    public function store(Request $request)
    {
        $mySave = FiscalYear::create([
         'startDate' => $request->startDate,
         'endDate' => $request->endDate,
         'startShort' => $request->startShort,
         'endShort' => $request->endShort,
         'status'  => 0,
         'softDelete' => 0
         ]);
        if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage'));
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }
    public function edit($id)
    {
     $data = FiscalYear::findOrFail($id);
     if ($data) {
        return response()->json([
            'success' => true,
            'id'      => $data->id,
            'startDate' => $data->startDate,
            'endDate' => $data->endDate,
            'startShort' => $data->startShort,
            'endShort' => $data->endShort
            ]);
    }else{
        return response()->json([
            'success' => false,
            'message' => config('activityMessage.dataNotFound')
            ]);
    }
}
public function update(Request $request, $id)
{
    $update = FiscalYear::findOrFail($id);
    $update->startDate = $request->startDate;
    $update->endDate   = $request->endDate;
    $update->startShort  = $request->startShort; 
    $update->endShort  = $request->endShort;

    $myUpdate = $update->update();

    if ($myUpdate) {
        return response()->json([
            'success' => true,
            'message' => config('activityMessage.updateMessage')
            ]);
    }else{
        return response()->json([
            'success'  => false,
            'message'  => config('activityMessage.notUpdated')
            ]);
    }
}

public function changeStatus(Request $request){
   $id  = $request->id;
   $status = $request->status==1 ? 0 : 1;
   $change = mylogic::sysChangeStatus($tablename = $this->fisYearTypeTbl, $Id = $id, $status = $status);

   if ($change) {
      return MyLogic::successChangeStatus($code = 200);
  }else{
      return MyLogic::idNotFoundMessage($code = 301);
  }
}
}