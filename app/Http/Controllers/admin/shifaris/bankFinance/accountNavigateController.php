<?php

namespace App\Http\Controllers\admin\shifaris\bankFinance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\accountNavigate;
use App\model\shifaris\ApplicantUser;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\AccountNavigateVal;
use DB;
use Auth;

use App\model\mylogic;

class AccountNavigateController extends Controller
{
    protected $viewPath = 'admin.shifaris.bankFinance.accountNavigate';
    protected $shifarisTableName     = 'account_navigates';
    protected $appUsrTable     = 'applicant_users';
    protected $deginationsTbl = 'deginations';

    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixAccountNavigate');
        
    }

    public function index()
    {
        $page['page_title']       = 'Account Navigate';
        $page['page_description'] = 'Account Navigate';

        
        $propertyVal = accountNavigate::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        

    }

    public function create()
    {
        $page['page_title']       = 'Account Navigate : create';
        $page['page_description'] = 'Account Navigate details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);
        // dd($getUniqueCode);
        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationsTbl, $orderColumn = 'nameNep', $orderby = 'ASC');
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(AccountNavigateVal $request)
    {
        //
        
     DB::beginTransaction();
     try {

        //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


        $isShifarisExist = isset($request->shifarisId) ? accountNavigate::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? accountNavigate::findOrfail($isShifarisExist['id']) : new accountNavigate;

        $save->createdBy = Auth::user()->id;

        $save->refCode  = $request->refCode;

        $save->wards_id  = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";
        $save->issuedDate  = $request->issuedDate;
        $save->chalaniNum = $request->chalaniNum;
        $save->organizationName  = $request->organizationName;
        $save->organizationLocation  = $request->organizationLocation;
        $save->municipalityName  = $request->municipalityName;
        $save->wardNum  = $request->wardNum;
        $save->purpose  = $request->purpose;
        $save->accountNum  = $request->accountNum;
        $save->year  = $request->year;
        $save->interest  = $request->interest;
        
        $accountNavCount = count($request->name);
        $name = $request->name;
        $post = $request->post;
        $remark = $request->remarks;
        // dd($remark);
        $accountNavArray = [];

        for ($i=0; $i < $accountNavCount; $i++) {
            array_push($accountNavArray, [
                'name' => $name[$i],
                'post' => $post[$i],
                'remark' => $remark[$i] ?? 'N/A'
            ]);
        }
        $save->accountNavigateStatement = json_encode($accountNavArray);

        $save->authorizedPerson  = $request->authorizedPerson;
        $save->deginations_id  = $request->deginations_id;

        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

                       //end for login
        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

                    $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
    }
    
}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
