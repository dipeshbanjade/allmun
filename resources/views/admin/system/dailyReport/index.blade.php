@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-head">
            <header>@lang('commonField.extra.dailyReport')</header>
        </div>
        <div class="card-body ">
            <a href="{{ route('dailyReport.create') }}" type="button" class="btn btn-primary pull-right"><i class="fa fa-plus"></i><span> @lang('commonField.button.dailyReport')</span></a> 
            <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
                <!-- table start  -->
                <div class="mdl-tabs__panel is-active p-t-20" id="tab4-panel">
                    <div class="table-responsive">
                        @if(count($dailyReport) > 0)
                        <table class="table scrollable">
                            <tbody>
                                <tr>
                                    <th width="5%">@lang('commonField.extra.sn')</th>
                                    <th width="15%">@lang('commonField.extra.date')</th>
                                    <th width="15%">@lang('commonField.extra.time')</th>
                                    <th>@lang('commonField.extra.subject')</th>
                                    <th>@lang('commonField.extra.desc')</th>
                                    <th width="15%">@lang('commonField.extra.action')</th>
                                </tr>
                                <?php  $count = 1;     ?>
                                @foreach($dailyReport as $dlyRep)
                                <tr>
                                    <td>{{ $count ++ }}</td>
                                    <td>{{ $dlyRep->date }}</td>

                                    <td>
                                        {{ date('h:i a', strtotime($dlyRep->startTime)) }} - {{ date('h:i a', strtotime($dlyRep->endTime)) }}
                                    </td>
                                    <td>{{ $dlyRep->sub }}</td>

                                    <td title="{!! $dlyRep->desc !!}">
                                        {!! str_limit($dlyRep->desc , 100)
                                        !!}</td>
                                        <td>
                                            <a href="{{ route('dailyReport.edit', $dlyRep->id) }}">
                                              <i class="fa fa-pencil btn btn-success btn-xs"></i>
                                          </a>
                                          <a href="#" class="deleteMe" data-url="{{ route('dailyReport.delete', $dlyRep->id )}}" data-name="{{ $dlyRep->sub }}">
                                           <button class="btn btn-danger btn-xs">
                                             <i class="fa fa-trash-o "></i>
                                         </button>
                                     </a>
                                 </td>
                             </tr>
                             @endforeach
                         </tbody>
                     </table>
                     {{ $dailyReport->links() }}
                     @endif
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
{{-- Daily report Log Ends --}}
@endsection
