<?php

namespace App\Http\Controllers\admin\municipility;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\municipility\StaffMunicipility;
use App\model\admin\municipility\MunicipalityStaffLeave;
use App\Http\Requests\admin\municipality\leaveValidation;
use App\model\mylogic;
use Auth;
use DB;
use Validator;

class munStaffLeaveController extends Controller
{
    protected $viewPath        = 'admin.municipility.leave';
    protected $munLeaveTbl     = 'municipality_staff_leaves';
    protected $leaveTbl        = 'leave_types';
    protected $prefix;
    protected $munId;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.leavePrefix');
        $this->munId        = myLogic::getMunicipalityDetails()['id'];
    }

    public function index()
    {
        $page['page_title']       = ': leave';
        $page['page_description'] = 'staff leave';
        $currentId = Auth::user()->staffs_id;

        $myLeaveForm = MunicipalityStaffLeave::with('leaveTypesIds')->where(['softDelete' => 0, 'users_id' => Auth::user()->staffs_id])->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
            return view($this->viewPath . '.myLeaveForm', compact(['page', 'myLeaveForm']));
        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }

    public function adminViewLeaveDetails(){
     $page['page_title']       = ': leave';
     $page['page_description'] = 'staff leave';

     $viewAdmLeaveForm = MunicipalityStaffLeave::with('leaveTypesIds')->where(['softDelete' => 0])->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

     if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
         return view($this->viewPath . '.adminViewLeave', compact(['page', 'viewAdmLeaveForm']));

     }else{
         return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
     }
    }

    public function getStaffName($id){
       $staff = StaffMunicipility::select('firstNameEng', 'middleNameEng', 'lastNameEng', 'firstNameNep', 'middleNameNep', 'lastNameNep')->where(['id' => $id])->first();

       return $staffName = $staff['firstNameEng'] . $staff['middleNameEng'] . $staff['lastNameEng'];
    }

    public function create()
    {
        $page['page_title']       = ': Create leave';
        $page['page_description'] = 'staff leave';

        // $refCode  = mylogic::sysRefCode($tablename = $this->munLeaveTbl, $prefix = $this->prefix);
        
        $leaveType = mylogic::getDrowDownData($tablename = $this->leaveTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
             return view($this->viewPath . '.create', compact(['page', 'leaveType']));
        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }
    public function store(leaveValidation $request)
    {
        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
            $refCode = mylogic::sysRefCode($tablename = $this->munLeaveTbl, $prefix = $this->prefix);
            DB::beginTransaction();
            try {
                $save = new MunicipalityStaffLeave;
                $save->refCode            = $refCode;
                $save->users_id           = Auth::user()->staffs_id;
                $save->municipilities_id  = $this->munId;
                $save->leave_types_id     = $request->leave_types_id;
                $save->noOfDays           = $request->noOfDays;
                $save->shortNoteNep       = $request->shortNoteNep;
                $save->shortNoteEng       = $request->shortNoteEng;
                $save->startDate          = $request->startDate;
                $save->startDate          = $request->startDate;
                $save->endDate             = $request->endDate;
                $save->status              = 0;
                $save->softDelete          = 0;

                $mySave = $save->save();

                $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createLeaveForm'), $tableName = $this->munLeaveTbl, $tblId = $save->id);
                 if ($mySave && $log) {
                     DB::commit();
                    return redirect()->route('myLeaveProfile')->withMessage(config('activityMessage.saveMessage') .$refCode);
                }else{
                    return back()->withMessage(config('activityMessage.unSaveMessage'));
                }
            } catch (Exception $e) {
                DB::rollback();
                return back()->withMessage(config('activityMessage.dataNotInserted'));
            }
        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }

    public function edit($id)
    {
        $page['page_title']       = ':Edit Leave Form';
        $page['page_description'] = 'edit my leave form';
        $leaveType = mylogic::getDrowDownData($tablename = $this->leaveTbl, $orderColumn = 'nameEng', $orderby = 'ASC');

        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
            if ($id) {
                $data = MunicipalityStaffLeave::findOrFail($id);
                return view($this->viewPath . '.edit', compact(['page', 'data', 'leaveType']));
            }else{
                return redirect()->route('home')->withMessage(config('activityMessage.idNotFound'));
            }
        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }

    public function update(Request $request, $id)
    {
       if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
             $validator = Validator::make($request->all(), [
                'leave_types_id'   => 'required',
                'noOfDays'         => 'required',
                'shortNoteEng'     => 'required|min:7|max:3000',
                'startDate'        => 'required',
                'endDate'          => 'required'
              ]);
             if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
             }  // validation 

             DB::beginTransaction();
             try {
                 $update =MunicipalityStaffLeave::findOrFail($id);
                 $update->leave_types_id     = $request->leave_types_id;
                 $update->noOfDays           = $request->noOfDays;
                 $update->shortNoteNep       = $request->shortNoteNep;
                 $update->shortNoteEng       = $request->shortNoteEng;
                 $update->startDate          = $request->startDate;
                 $update->startDate          = $request->startDate;
                 $update->endDate             = $request->endDate;
                 $update->status              = 0;
                 $update->softDelete          = 0;

                 $update = $update->update();

                 $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') .'Leave', $tableName = $this->munLeaveTbl, $tblId = $request->refCode);

                  if ($update && $log) {
                      DB::commit();
                     return redirect()->route('myLeaveProfile')->withMessage(config('activityMessage.updateMessage') .$request->refCode);
                 }else{
                     return back()->withMessage(config('activityMessage.unSaveMessage'));
                 }
             } catch (Exception $e) {
                 DB::rollback();
                 return back()->withMessage(config('activityMessage.dataNotInserted'));
             }
         }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
         }

    }
    public function destroy($id)
    {
        $del = MunicipalityStaffLeave::findOrFail($id);
        $del->softDelete = 1;
        $myDel = $del->update();
        if ($myDel) {
          return MyLogic::successChangeStatus($code = 200);
        }else{
           return MyLogic::successChangeStatus($code = 301);
        }
    }

    public function changeStatus(Request $request){  // change status either present or absent
        $id        = $request->id;
        if ($request->status == 1) {
            $status = 0;
        }
        if ($request->status == 0) {
             $status = 1;
        }
        $approveBy = Auth::user()->id;
        $change = MunicipalityStaffLeave::findOrFail($id);
        $change->status  = $status;
        $change->approvedBy = $approveBy;
        $update = $change->update();
        
        if ($update) {
            return MyLogic::successChangeStatus($code = 200);
        }else{
            return MyLogic::idNotFoundMessage($code = 301);
        }
    }
}
