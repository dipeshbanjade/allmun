@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
	       <i class="fa fa-plus"></i>
	 </div>

	 <div class="card-head">
            <header>Register Report</header>
	           {{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	         </div>

	 {{ Form::open(['route' => 'admin.regReport.store', 'name' => 'frmWardregReport']) }}
          @include('admin.shifaris.report.regReport._form')
	 {{ Form::close() }}
	 </div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
	 @include('admin.shifaris.script')
@endsection