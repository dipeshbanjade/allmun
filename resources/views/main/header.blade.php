<div class="page-header navbar navbar-fixed-top">
  <div class="page-header-inner">
    <!-- logo start -->
    <div class="page-logo">
      <a href="{{ route('home') }}">
        <span class="logo-default">
         <?php     
         $urs = Auth::user()->userLevel;
         ?>
         {{ $urs == 'wrd' ? getWardDetails(Auth::user()->wards_id)['nameEng'] : getMunicipalityData()['nameEng']    }}
       </span>
     </a>
   </div>
   <!-- logo end -->
   <ul class="nav navbar-nav navbar-left in">
    <li>
      <a href="#" class="menu-toggler sidebar-toggler">
        <i class="icon-menu"></i>
      </a>
    </li>
  </ul>
    <!-- <form class="search-form-opened" action="#" method="GET">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search..." name="query">
          <span class="input-group-btn">
            <a href="javascript:;" class="btn submit">
              <i class="icon-magnifier"></i>
            </a>
          </span>
        </div>
      </form> -->
      <!-- start mobile menu -->
      <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        <span></span>
      </a>
      <!-- end mobile menu -->
      <!-- start header menu -->
      <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
          <li class="dropdown language-switch">
            @if(in_array('viewLogFile', $myPermission))
            <li>
              <a href="{{ route('viewLogFile') }}" title="view staff log files">
               <i class="fa fa-history" aria-hidden="true"></i>
             </a>
           </li>
           @endif

           @if(in_array('fieldVisit.index', $myPermission))
           <li>
             <a href="{{ route('fieldVisit.index') }}" title="Field Visit">
              <i class="fa fa-tripadvisor"></i>
            </a>
          </li>
          @endif
          
          @if(in_array('dailyReport.index', $myPermission))
          <li>
           <a href="{{ route('dailyReport.index') }}" title="Daily Report">
            <i class="fa fa-id-card-o"></i>
          </a>
        </li>
        @endif

        <li>
          <a href="" onclick="location.reload() " title="Daily Report">
            <i class="fa fa-refresh"></i>
          </a>
        </li>
        <li>
          <a href="{{ route('home') }}" title="Home">
            <i class="fa fa-home"></i>
          </a>
        </li>
        <li>
          <a class="changeLanguage dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  data-url="{{ route('changeLanguage', 'en') }}" data-val="en">
            <img src="{{ asset('lib/img/gb.png') }}" class="position-left" alt="english logo" title="English">
          </a>
        </li>
        <li class="dropdown language-switch">
          <a class="changeLanguage dropdown-toggle flag" data-toggle="dropdown" aria-expanded="false" data-url="{{ route('changeLanguage', 'np') }}" data-val="np">
            <img src="{{ asset('lib/img/flag.png') }}" class="position-left" alt="Nepal logo" title="Nepali">
          </a>
        </li>
        <li>
          <a href="javascript:;" class="fullscreen-btn" title="Full Screen">
            <i class="fa fa-arrows-alt"></i>
          </a>
        </li>
        <!-- end manage user dropdown -->

        <!--  megaMenu Starts -->
        <li class="nav-item dropdown">
          @if(Auth::user()->userLevel ==='dev')
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-cog"></i>
          </a>
          @endif
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <div class="container">
              <div class="row">
                <div class="col col-12 pt-2">
                  <span class="text-uppercase text-white">
                    <i class="fa fa-cog"></i>Settings
                  </span><br><hr>
                </div>

                <div class="col-md-4 margin-top-10" >

                  <ul class="nav flex-column ">
                    <li class="nav-item ">
                      <a class="nav-link active" href="{{ route('admin.province.index') }}">
                        <span class="title text-white" >
                          @lang('sidebarMenu.system_setting.province')
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('admin.degination.index') }}">
                        <span class="title text-white">
                          @lang('commonField.extra.degination')
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ route('admin.staffType.index') }}">
                        <span class="title text-white">
                         @lang('sidebarMenu.system_setting.staffType')
                       </span>
                     </a>
                   </li>
                   <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.leaveType.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.leaveType')
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('occupation.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.occupation')
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('houseType.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.houseType')
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('religion.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.religious')
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('jatjati.index') }} ">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.jatjati')
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
              <!-- /.col-md-4  -->
              <div class="col-md-4">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <a class="nav-link active" href="{{ route('admin.visitType.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.fieldVisit')
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('qualification.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.qualification')
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('citizenType.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.citizenShipType')
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('set-language.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.language')
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('nationality.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.nationality')
                      </span>
                    </a>
                  </li>  
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.fiscalYear.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.fiscalYear')
                      </span>
                    </a>
                  </li>   
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.department.index') }}">
                      <span class="title text-white">
                        @lang('sidebarMenu.system_setting.department')
                      </span>
                    </a>
                  </li>

                  <li class="nav-item">
                   <a class="nav-link" href="{{ route('taxType.index') }}">
                     <span class="title text-white">
                       @lang('sidebarMenu.system_setting.taxType')
                     </span>
                   </a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="{{ route('animal.index') }}">
                     <span class="title text-white">
                       @lang('sidebarMenu.system_setting.animal')
                     </span>
                   </a>
                 </li>
                 <!-- disablity -->
                 <li class="nav-item">
                   <a class="nav-link" href="{{ route('disable.index') }}">
                     <span class="title text-white">
                       Disablity
                     </span>
                   </a>
                 </li>


               </ul>
             </div>
             <!-- /.col-md-4  -->
             <div class="col-md-4">
              <ul class="nav flex-column">
               <li class="nav-item">
                 <a class="nav-link" href="{{ route('floor.index') }}">
                   <span class="title text-white">
                     @lang('sidebarMenu.system_setting.floor')
                   </span>
                 </a>
               </li>
               <li class="nav-item">
                 <a class="nav-link" href="{{ route('familyRelation.index') }}">
                   <span class="title text-white">
                     @lang('sidebarMenu.system_setting.familyRelation')
                   </span>
                 </a>
               </li>
               <li class="nav-item">
                 <a class="nav-link" href="{{ route('migrationReason.index') }}">
                   <span class="title text-white">
                     @lang('sidebarMenu.system_setting.migrationReason')
                   </span>
                 </a>
               </li>
             </ul>
           </div>
           <!-- /.col-md-4  -->
         </div>
       </div>
       <!--  /.container  -->
     </div>
   </li>

   <!-- end manage user dropdown -->
   <!-- household megaMenu starts here -->
   <li class="nav-item dropdown">
    @if(Auth::user()->userLevel ==='dev')
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fa fa-header"></i>
    </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
      <div class="container">
        <div class="row">
          <div class="col col-12 pt-2">
            <span class="text-uppercase text-white">
              <i class="fa fa-header"></i>&nbsp; Household Settings
            </span><br><hr>
          </div>

          <div class="col-md-3 margin-top-1" >

            <ul class="nav flex-column ">
              <li class="nav-item ">
                <a class="nav-link active" href="{{ route('houseWallType.index') }}">
                  <span class="title text-white" >
                    @lang('sidebarMenu.system_setting.houseWallType')
                  </span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('houseRoof.index') }}">
                  <span class="title text-white">
                   @lang('sidebarMenu.system_setting.houseRoof')

                 </span>
               </a>
             </li>
             <li class="nav-item">
              <a class="nav-link" href="{{ route('swamipto.index') }}">
                <span class="title text-white">
                 @lang('sidebarMenu.system_setting.swamipto')
               </span>
             </a>
           </li>
           <li class="nav-item">
            <a class="nav-link" href="{{ route('drinkingWater.index') }}">
              <span class="title text-white">
                @lang('sidebarMenu.system_setting.drinkingWater')
              </span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('chuloType.index') }}">
              <span class="title text-white">
                @lang('sidebarMenu.system_setting.chuloType')
              </span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('toiletType.index') }}">
              <span class="title text-white">
                @lang('sidebarMenu.system_setting.toiletType')
              </span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('phone.index') }}">
              <span class="title text-white">
                @lang('sidebarMenu.system_setting.phone')
              </span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('vaccineType.index') }}">
              <span class="title text-white">
                @lang('sidebarMenu.system_setting.vaccineType')
              </span>
            </a>
          </li>   
          <li class="nav-item">
            <a class="nav-link" href="{{ route('country.index') }}">
              <span class="title text-white">
                @lang('sidebarMenu.system_setting.country')
              </span>
            </a>
          </li>     
        </ul>
      </div>
      <!-- /.col-md-3  -->
      <div class="col-md-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="{{ route('road.index') }}">
              <span class="title text-white">
                @lang('sidebarMenu.system_setting.road')
              </span>
            </a>
          </li>
          <!-- familyType -->
          <li class="nav-item">
           <a class="nav-link active" href="{{ route('familyType.index') }}">
             <span class="title text-white">
               @lang('sidebarMenu.system_setting.familyType')
             </span>
           </a>
         </li>
         <!--  -->
         <li class="nav-item">
          <a class="nav-link" href="{{ route('vehicle.index') }}">
            <span class="title text-white">
              @lang('sidebarMenu.system_setting.vehicle')
            </span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{ route('fuel.index') }}">
            <span class="title text-white">
              @lang('sidebarMenu.system_setting.fuel')
            </span>
          </a>
        </li>
        <li class="nav-item">
         <a class="nav-link" href="{{ route('roomUseFor.index') }}">
           <span class="title text-white">
             @lang('sidebarMenu.system_setting.roomUseFor')
           </span>
         </a>
       </li>
       <li class="nav-item">
         <a class="nav-link" href="{{ route('roomFloorType.index') }}">
           <span class="title text-white">
             @lang('sidebarMenu.system_setting.roomFloorType')
           </span>
         </a>
       </li>
       <li class="nav-item">
         <a class="nav-link" href="{{ route('houseFoundation.index') }}">
           <span class="title text-white">
             @lang('sidebarMenu.system_setting.houseFoundation')
           </span>
         </a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="{{ route('handWashMethod.index') }}">
          <span class="title text-white">
            @lang('sidebarMenu.system_setting.handWashMethod')
          </span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('educationLevel.index') }}">
          <span class="title text-white">
            @lang('sidebarMenu.system_setting.educationLevel')
          </span>
        </a>
      </li>       
    </ul>
  </div>
  <!-- /.col-md-3  -->
  <div class="col-md-3">
    <ul class="nav flex-column">
      <li class="nav-item">
       <a class="nav-link" href="{{ route('wasteManagement.index') }}">
         <span class="title text-white">
           @lang('sidebarMenu.system_setting.wasteManagement')
         </span>
       </a>
     </li>

     <li class="nav-item">
      <a class="nav-link" href="{{ route('qualityWater.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.qualityWater')
        </span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('schoolClassLvl.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.schoolClassLvl')
        </span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('workType.index') }}" title="work type">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.workType')
        </span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('salaryType.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.salaryType')
        </span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('disease.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.disease')
        </span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('lightSource.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.lightSource')
        </span>
      </a>
    </li>       
    <li class="nav-item">
     <a class="nav-link" href="{{ route('livingFor.index') }}">
       <span class="title text-white">
         @lang('sidebarMenu.system_setting.livingFor')
       </span>
     </a>
   </li>

   <li class="nav-item">
     <a class="nav-link" href="{{ route('ovenType.index') }}">
       <span class="title text-white">
         @lang('sidebarMenu.system_setting.ovenType')
       </span>
     </a>
   </li>        

 </ul>
</div >
<div class="col-md-3">
  <ul class="nav flex-column">
    <li class="nav-item">
      <a class="nav-link" href="{{ route('electricity.index') }} ">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.electricity')
        </span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('filterProcess.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.filterProcess')
        </span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('drinkingWaterSource.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.drinkingWaterSource')
        </span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('areaType.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.areaType')
        </span>
      </a>
    </li>   
    <li class="nav-item">
      <a class="nav-link" href="{{ route('electricalDevice.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.electricalDevice')
        </span>
      </a>
    </li>     
    <li class="nav-item">
      <a class="nav-link" href="{{ route('houseUseFor.index') }}">
        <span class="title text-white">
          @lang('sidebarMenu.system_setting.houseUseFor')
        </span>
      </a>
    </li>
    <li class="nav-item">
     <a class="nav-link" href="{{ route('businessType.index') }}">
       <span class="title text-white">
         @lang('sidebarMenu.system_setting.businessType')
       </span>
     </a>
   </li>

   <li class="nav-item">
     <a class="nav-link" href="{{ route('waterAvailability.index') }}">
       <span class="title text-white">
         @lang('sidebarMenu.system_setting.waterAvailability')
       </span>
     </a> 
     <li class="nav-item">
       <a class="nav-link" href="{{ route('whyNot.index') }}">
         <span class="title text-white">
           @lang('sidebarMenu.system_setting.whyNot')
         </span>
       </a>
     </li> 

     <li class="nav-item">
       <a class="nav-link" href="{{ route('treatmentArea.index') }}">
         <span class="title text-white">
           @lang('sidebarMenu.system_setting.treatmentArea')
         </span>
       </a>
     </li>



     <li class="nav-item">
       <a class="nav-link" href="{{ route('ageGroup.index') }}">
         <span class="title text-white">
           @lang('sidebarMenu.system_setting.ageGroup')
         </span>
       </a>
     </li>    
   </ul>
 </div>
 <!-- /.col-md-4  -->
</div>
</div>
<!--  /.container  -->
</div>
</li>
<!-- houserhold mega menu ends here  -->

<li class="dropdown dropdown-user font-white">
  <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
    <span class="username username-hide-on-mobile"> 
      <strong>{!! ucfirst(Auth::user()->name) !!}</strong></span>
      <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-menu-default">
      <li>
        <a href="{{ route('myProfile') }}">
          <span>
            <i class="icon-user"></i>
            Profile

          </span>

        </a>
      </li>
      @if(Auth::user()->userLevel == 'mun')
      <li >
        <a href="{{ route('myLeaveProfile') }}"> 
          <span>
            <!-- @lang('sidebarMenu.extra.leave') -->
            <i class="icon-directions"></i>
            Leave Form
          </span>
        </a>
      </li>
      @endif
      @if(Auth::user()->userLevel == 'wrd')
      <li>
        <a href="{{ route('wardStaffLeave.index') }}" > 
          <span>
            <i class="icon-directions"></i>
            <!-- @lang('sidebarMenu.extra.leave') -->
            Leave Form
          </span>
        </a>
      </li>
      @endif

      <li>
        <a href="{{ route('changePassword') }}">
         <span>
          <i class="icon-directions"></i> 
          <!--  @lang('sidebarMenu.staff_setting.changePassword') -->
          Change Password
        </span>
      </a>
    </li>

                    <!-- <li>
                      <a href="#">
                        <span>
                        <i class="icon-directions"></i>
                          
                         Help
                        </span>
                   
                      </a>
                    </li> -->
                    <li class="divider"></li>
                    <li>
                      <a href="{{ route('system.logout') }}">
                        <span>
                          <i class="icon-logout"></i>
                          
                          Log Out 
                        </span>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>

