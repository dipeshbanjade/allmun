<?php

namespace App\model\admin\houseDetails\electronic;

use Illuminate\Database\Eloquent\Model;

class HouseElectronicDevices extends Model
{
    protected $table = 'house_electronic_devices';
}
