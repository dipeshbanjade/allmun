@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
    <div class="card-body ">
      <button id="btnAddDeg" class="btn btn-primary pull-right">  
       <i class="fa fa-plus"></i>&nbsp;@lang('commonField.extra.createProvision')
      </button>

      <div class="table-scrollable">
          @if(count($allDegination) > 0)
                      <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
                        <thead>
                          <tr>
                            <th width="5%">@lang('commonField.extra.sn')</th>
                            <th>@lang('commonField.extra.refCode')</th>
                            <th>@lang('commonField.personal_information.name') </th>
                            <th> @lang('commonField.extra.action') </th>
                          </tr>
                        </thead>
                        <tbody>
                            @php $count = 1  @endphp
                              @foreach($allDegination as $deg)
                                      <tr class="odd gradeX">
                                        <td>{{ $count ++ }}</td>
                                        <td>{{ $deg->refCode }}</td>
                                        <td>{{ $deg->nameNep }} - {{ $deg->nameEng }}</td>
                                        <td>
                                          <a href="#" class="btn btn-primary btn-xs" id="btnEditDeg"  data-url="{!! route('admin.degination.edit', $deg->id) !!}">
                                            <i class="fa fa-pencil"></i>
                                          </a>
                                            <a href="#" class="deleteMe btn btn-danger btn-xs" data-url="{!! route('admin.degination.delete', $deg->id ) !!}" title="delete degination" data-name="{{ $deg->refCode }}" data-id = "{{ $deg->id }}">
                                              
                                              <i class="fa fa-trash-o "></i>
                                            </a>
                                            </td>
                                      </tr>
                              @endforeach
                         </tbody>
                        </table>
                          {{ $allDegination->links() }}
                      @else
                      <h3 class="fontColorBlack">
                        @lang('commonField.extra.pleaseInsertData')
                        </h3>
          @endif 
        
          </div>
        </div>
      </div>
    </div>
    @endsection
@section('modelSection')
   <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.addDegination')</i></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        {{ Form::open(['route' => 'admin.degination.store', 'name' => 'frmDegination']) }}
        <div class="modal-body">
             @include('admin.setting.degination._form')
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
            <div class="col-lg-12 p-t-20 text-right"> 
            {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
            </div>
        </div>
        {{ Form::close() }}

      </div>
    </div>
   </div>

   <!-- update model -->
   <div class="modal" id="updateDeginationForm">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.editDegination')</i></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
        {{ Form::open(['name' => 'frmeditDegination', 'id' => 'frmUpdateDeg']) }}
             @include('admin.setting.degination._form')
               {{ Form::hidden('degId', null, ['class' => 'degId']) }}

            <div class="col-lg-12 p-t-20 text-right"> 
            {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
            </div>
        {{ Form::close() }}
        </div>
          <div class="modal-footer">

          </div>
      </div>
    </div>
   </div>
@endsection
@section('custom_script')
 @include('admin.setting.degination.script')
@endsection