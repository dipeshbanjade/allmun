<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditvehicle']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of fuel is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmfuel']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of fuel is required"
            }
        });
  var btnAddhouseFoundation = $('.btnAddhouseFoundation');
  btnAddhouseFoundation.on('click', function(e){
    $('#houseFoundationModel').modal('show'); 
  });

  var btnEdithouseFoundation = $('.btnEdithouseFoundation');
  btnEdithouseFoundation.on('click', function(e) {
    e.preventDefault();
    var url = $(this).data('url');
    $.ajax({
      'type': 'GET',
      'url': url,
      success: function (response) {
        $('.houseFoundationId').val(response.id);
        $('.txtRefCode').val(response.refCode).attr('disabled',true);
        $('.txtNameNep').val(response.foundationNameNep);
        $('.txtNameEng').val(response.foundationNameEng);
        $("#edithouseFoundationModel").modal('show');
      }
    })
  });

  var frmUpdatehouseFoundation = $('#frmUpdatehouseFoundation');
  frmUpdatehouseFoundation.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.houseFoundationId').val();
    var url  = "{{URL::to('/')}}" + "/setting/houseFoundation/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#edithouseFoundationModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#edithouseFoundationModel').modal('hide');
});
});
</script>