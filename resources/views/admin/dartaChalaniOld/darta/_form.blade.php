<style>
  .form-select-add{
    width: 88%; 
    height: 36px !important;
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .form-add-button{
    margin-top: -36px;
  }
  .box-body textarea{
    width: 90% !important;
    height: 35px !important;
    border-left: 0;
    border-top: 0;
    border-right: 0;
    border-radius: 0;
    border-bottom: 2px solid #ccc;
    resize: none;
  }
   textarea::-webkit-input-placeholder {
   text-align: center;
  }

  textarea:-moz-placeholder { /* Firefox 18- */
     text-align: center;  
  }

  textarea::-moz-placeholder {  /* Firefox 19+ */
     text-align: center;  
  }

  textarea:-ms-input-placeholder {  
     text-align: center; 
  }
  select{
    height: 40px !important;
  }
    
</style>
<div class="col">
    <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('dartaDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'current date', 'id' => 'dartaDate', 'title'=>'enter current date', 'required'=>true]) }}</p>
  </div>
  <div class="box-body" align="center">
      <div class="form-group">
        {{ Form::textarea('dartaSubject', null, ['class'=> 'form-control', 'placeholder'=>'Darta Subject', 'title'=>'enter Darta subject', 'required'=>true]) }}
      </div>
</div>
<div class="container" style="margin-top: 20px;">
  <div class="row">
    <div class="col-md-4 col-lg-4 col-sm-12">
      <div class="form-group">
        <label>@lang('commonField.dartaChalani.dartaNo')</label>    
        {{ Form::text('dartaNo', null, ['class'=> 'form-control col-sm-12 txtDartaNo', 'placeholder'=>'Darta number', 'title'=>'enter Darta Number']) }}
      </div>
      <div class="form-group" style="margin-top: 10px;">
        <label>पत्र  संख्या</label>
          {{ Form::text('dartaPage', null, ['class'=> 'form-control txtChlaniPage', 'placeholder'=>'Darta page', 'title'=>'enter Darta page']) }}
      </div>
      <div class="form-group" style="margin-top: 10px;">
        <label>पत्र मिति</label>
          {{ Form::text('dartaPageDate', null, ['class'=>'form-control dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  * Darta page date', 'id' => 'dartaPageDate', 'title'=>'darta page date']) }}
      </div>  
    </div>
    <div class="col-md-4 col-lg-4 col-sm-12">
      <div class="form-group">
        <label>प्राप्त मिति</label>
        {{ Form::text('dartaReceiveDate', null, ['class'=>'form-control dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'YYYY/mm/dd', 'id' => 'reciveDate', 'title'=>'darta receive date']) }}
      </div>
      <div class="form-group" style="margin-top: 10px;">
        <label>कार्यालय</label>
        <select class="form-control form-select-add" name="other_offices_id", title="select comapny name">
          <option value="">Select company name</option>
            @if($companyList)
               @foreach ($companyList as $cName)
                 <option value="{{$cName->id}}" {{ isset($data) && $data->other_offices_id == $cName->id ? 'selected' : '' }}>
                     {{ $lang == 'np' ? $cName->nameNep : $cName->nameEng }}
                 </option>
               @endforeach
            @endif
        </select>
        <button type="button" id="btnAddCompanyDetail"  class="btn btn-primary btn-md form-add-button pull-right"><i class="fa fa-plus"></i><span></span></button>
      </div>
      
      <div class="form-group">
        <label>विभाग</label>
        <select class="form-control cboDeptId" name="departments_id" title="select department name">
          <option value="">select department</option>
          @if($department)
             @foreach($department as $dept)
                    <option value="{{ $dept->id }}">
                        {{ $lang =='np' ? $dept->nameNep : $dept->nameEng }}
                    </option>
             @endforeach
          @endif
        </select>
          
      </div>
    </div> 

    <div class="col-md-4 col-lg-4 col-sm-12">
      <div class="form-group">
        <label>कसको लागि</label>
        <select class="form-control" name="dartaIsFor" id="dartaIsFor" title="letter is for">
          <option value="">select staff name</option>
        <!--   @if(isset($staff))
              @foreach($staff as $stff)
                  <?php    
                       $staffName = $lang == 'np' ? $stff->firstNameNep .''. $stff->middleNameNep .' '. $stff->lastNameNep : $stff->firstNameEng .''. $stff->middleNameEng .' '. $stff->lastNameEng
                  ?>
                  <option value="{{ $stff->id }}">
                       {{ $staffName  }}
                  </option>
              @endforeach
          @endif -->
        </select>
      </div>
      <div class="form-group" style="margin-top: 10px;">
        <label>bujhiline मिति</label>
        {{ Form::text('dartaReciverDate', null, ['class'=>'form-control dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'YYYY/mm/dd', 'id' => 'myDate', 'onfocus' => 'showNdpCalendarBox("reciveDate")']) }}
      </div>
      <div class="form-group">
        <!-- <label>@lang('commonField.dartaChalani.dartaChalani')</label> -->
        <label>@lang('commonField.dartaChalani.kaifiyat')</label>
        {{ Form::text('dartaKaifiyat', null, ['class'=>'form-control ', 'placeholder'=>'kaufiyat', 'rows'=>1, 'cols'=>3]) }}
      </div>

      <div class="form-group">
        <!-- <label>@lang('commonField.dartaChalani.dartaChalani')</label> -->
        <label>Storage palce</label>
        {{ Form::text('dartaStorePlace', null, ['class'=>'form-control ', 'placeholder'=>'storage Place', 'rows'=>3]) }}
      </div> 

      <div class="form-group">
        <!-- <label>@lang('commonField.dartaChalani.dartaChalani')</label> -->
        <label>Select image</label>
        <div class="form-group">
            <input type="file" name="dartaImage[]" multiple="multiple">
        </div>
      </div>

   </div>

  </div>

</div>
</div>