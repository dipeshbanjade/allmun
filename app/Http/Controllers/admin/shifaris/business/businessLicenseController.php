<?php

namespace App\Http\Controllers\admin\shifaris\business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\shifaris\ApplicantUser;
use App\model\shifaris\NirmanBebasaya;  // Business Building Licence
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\businessLicenseVal;
use DB;
use Auth;
// use App\model\admin\ward\union;

use App\model\mylogic;

class BusinessLicenseController extends Controller
{
    protected $viewPath = 'admin.shifaris.business.businessLicense';
    protected $shifarisTableName     = 'nirman_bebasayas';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixNirmanBebasaya');

    }

    public function index()
    {
        $page['page_title']       = 'Business Building License';
        $page['page_description'] = 'Business Building License';

        $nirman = NirmanBebasaya::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        

    }

    public function create()
    {
        $page['page_title']       = 'Business Building License : create';
        $page['page_description'] = 'Business Building License details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        

        //TODO  display municipility according to municipility 
        // return view($this->viewPath . '.create', compact(['page', 'getUniqueCode']));
    }

    public function store(businessLicenseVal $request)
    {
     
     DB::beginTransaction();
     try {
        //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

        $isShifarisExist = isset($request->shifarisId) ? NirmanBebasaya::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? NirmanBebasaya::findOrfail($isShifarisExist['id']) : new NirmanBebasaya;

        // $save = new NirmanBebasaya;

        $save->createdBy = Auth::user()->id;        
        $save->refCode  = $request->refCode;
        $save->wards_id  = Auth::user()->wards_id ?? NULL;
        $save->municipilities_id = getMunicipalityData()['id'] ?? "";
        
        $save->fiscalYear  = $request->fiscalYear;
        $save->chalaniNum = $request->chalaniNum;
        $save->address = $request->address;
        $save->companyName  = $request->companyName;
        $save->authorizedBy  = $request->authorizedBy;
        $save->deginations_id  = $request->deginations_id;
        $save->issuedDate  = $request->issuedDate;
        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
        $citizenShifarish = true;
        if(empty($isShifarisExist['id'])){
            $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
        }            
        if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }


}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
