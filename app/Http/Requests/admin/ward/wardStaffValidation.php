<?php

namespace App\Http\Requests\admin\ward;

use Illuminate\Foundation\Http\FormRequest;

class wardStaffValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'citizenNo'      => 'required|min:4|max:30|unique:ward_staffs',
            'firstNameEng'   => 'required|min:3|max:30',
            'lastNameEng'    => 'required',
            'phoneNumber'    => 'required|min:7|max:11|unique:ward_staffs',
            'email'          => 'required|unique:ward_staffs'
        ];
    }
}
