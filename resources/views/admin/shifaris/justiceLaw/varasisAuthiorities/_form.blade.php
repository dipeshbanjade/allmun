<?php $checker = getMunicipalityData(); ?>

<div class="row top-part">
  <div class="col-md-6 ps-cn">
    <p><b class="ps">प. सं.:</b>
      @if(isset($data->refCode))
      {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
      @else
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
      @endif
    </p>
    <p align="left" class="star">
      <b class="cn">च. नं.:</b>
      {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
    </p>
  </div>
  <div class="col-md-6">
    <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
  </div>
</div>

<div class="col-md-12">
  <p> लिखितम
   <b><?php echo isset($data->municipalityName) ? $data->municipalityName : $checker->nameNep; ?>  </b>
   महा/उप/नगर/गाउँ-पालिका वडा न. बस्ने वर्ष 
   {{ Form::text('successorAge', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'successorAge', 'required' => 'required']) }}
   को 
   {{ Form::text('successorName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'successorName', 'required' => 'required']) }}
   आगे 
   {{ Form::text('successorMunicipality', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'successorMunicipality', 'required' => 'required']) }}
   महा/उप/नगर/गाउँ-पालिका वडा न. 
   {{ Form::text('successorWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'successorWard', 'required' => 'required']) }}
   बस्ने 
   {{ Form::text('casePerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'casePerson', 'required' => 'required']) }}
   संगको 
   {{ Form::text('caseName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'caseName', 'required' => 'required']) }}
   मुद्दा 
   {{ Form::text('caseMunicipality', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'caseMunicipality', 'required' => 'required']) }}
   महा/उप/नगर/गाउँ-पालिकामा चलिरहेकोमा मेरो कार्यव्यस्तताको कारणले उक्त मुद्धामा 
   {{ Form::text('casereason', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'casereason', 'required' => 'required']) }}
   असमर्थ रहेकोले उक्त मुद्धामा   
   {{ Form::text('caseRunMunicipality', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'caseRunMunicipality', 'required' => 'required']) }}
   महा/उप/नगर/गाउँ-पालिका कार्यालयमा 
   {{ Form::text('agreedOn', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'agreedOn', 'required' => 'required']) }}
   गर्ने लगायतका कार्य गर्नलाई मेरो सट्टामा एेनले वारेस दिन हुने मेरो मानिस 
   {{ Form::text('manisMunicipality', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'manisMunicipality', 'required' => 'required']) }}
   वडा न. 
   {{ Form::text('manisWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'manisWard', 'required' => 'required']) }}
   बस्ने वर्ष 
   {{ Form::text('manisAge', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'manisAge', 'required' => 'required']) }}
   को 
   {{ Form::text('manisName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'manisName', 'required' => 'required']) }}
   लाई वारेस अख्तियारी गरी पठाएको छु।निज वारेसले 
   {{ Form::text('caseOnMunicipality', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'caseOnMunicipality', 'required' => 'required']) }}
   महा/उप/नगर/गाउँ-पालिका कार्यालयमा उपस्थित भई तारेखमा रही मुद्धा पुर्पक्ष् गर्दा मुद्धा जिते हारेमा समेत मेरो मन्जुरी छ।पछि मुद्धा किनारा हुँदा बखत मा आफैँ उपस्थित  भई जो लागेको बुझाँउला।अड्डा अदालतबाट लागेको दण्ड, जरिवाना, दशौद आदेशले बाँकी रहेको कोर्ट फि समेत तिर्न बुझाउन बाँकी छैन।नबुझाई बाँकी रहेको ठहेरे यो वारेसनामाको कागज बदर गरी मुद्दा कानुन बमोजिम होस् भनी मेरो मनोमान राजिखुशिका साथ किनाराका साक्षिको रोहबरमा 
   {{ Form::text('rohabar', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'rohabar', 'required' => 'required']) }}
   बसी यो वारेसनामाको कागज लेखी लेखाई सही छाप गरी 
   {{ Form::text('transfer', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'transfer', 'required' => 'required']) }}
   लाई दिएँ।ईति सम्बत् २०
   {{ Form::text('year', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'year', 'required' => 'required']) }}
   साल
   {{ Form::text('month', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'month', 'required' => 'required']) }}
   महिना 
   {{ Form::text('day', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'day', 'required' => 'required']) }} 
   गते रोज
   {{ Form::text('eachday', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'eachday', 'required' => 'required']) }}
   शुभम.......
 </p>
 <!--views for nibedak detail -->
 <div class="clearfix"></div>
 <hr>
 @include('admin.shifaris.nibedakCommonField')
 <!-- END -->
 <div class="col-md-12">
  <hr>
</div>
</div>
