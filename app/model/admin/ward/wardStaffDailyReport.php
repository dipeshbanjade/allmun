<?php

namespace App\model\admin\ward;

use Illuminate\Database\Eloquent\Model;

class wardStaffDailyReport extends Model
{
    protected $table = 'w_r_d_staff_daily_reports';


    public function wards_staff(){
    	return $this->belongsTo('App\model\admin\ward\WardStaff'::class);
    }
}
