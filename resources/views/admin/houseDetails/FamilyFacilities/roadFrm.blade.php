<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-6">{{-- 15 --}}
                <p>
                    १५. तपाईको घरसम्म पुग्ने बाटोको अवस्था कस्तो प्रकारको रहेको छ ?
                </p>
                <br>
                @if (isset($road) && count($road)>0)
                @foreach ($road as $roadType)
                <label class="radio-inline">
                    <input type="radio" class="rdoClickMe" title="double click for reset" name="roadTypeNam" value="{{$roadType->nameNep}}" {{ isset($hsRoadData) && $hsRoadData->roadTypeNam == $roadType->nameNep ? 'checked="checked"' : '' }}>
                    {{$roadType->nameNep}}
                </label> 
                <br />
                @endforeach
                @else
                <label>Insert data from setting</label>
                @endif
            </div>
            <div class="form-group col-md-6">
                {{-- 16 --}}
                <p>
                    १६. तपाईको घरसम्म पुग्ने बाटो कति फिट चौडाईको रहेको छ ? 
                </p>
                <br>
                {{ Form::text('roadWidth', isset($hsRoadData->roadWidth) ? $hsRoadData->roadWidth : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'others']) }}
                फिट
                <br>
            </div>

        </div>
        <div class="msgDisplay"></div>
    </div>
</div>