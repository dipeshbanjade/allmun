<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class HouseFoundationType extends Model
{
    //
    protected $table = 'house_foundation_types';
    protected $fillable = ['refCode','foundationNameEng','foundationNameNep','status','softDelete'];
}
