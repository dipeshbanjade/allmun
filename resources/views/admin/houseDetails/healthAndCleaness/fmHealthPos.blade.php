<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
      <div class="form-group col-md-12">{{-- 12 --}}
        <p>
          १. तपाईका परिवारको कुनै सदस्य विरामी भएमा उपचारका लागि सवै भन्दा पहिला कहाँ जानुहुन्छ ?                                
        </p>
        <br>
        @if (isset($treatmentArea)&& count($treatmentArea) >0)
        @foreach ($treatmentArea as $tArea)
        <label class="radio-inline">
         <input type="radio" class="rdoClickMe" title="double click for reset" name="treatmentArea" value="{{$tArea->nameNep}}" {{ isset($hsFHP->treatmentArea) && $hsFHP->treatmentArea == $tArea->nameNep ? 'checked' : '' }}>
         {{$tArea->nameNep}}     
       </label> 
       <br />
       @endforeach
       @else
        <label>Insert data from the setting</label>
       @endif
       <label class="radio-inline">

        <label class="">
          {{ Form::text('otherTreatmentArea', isset($hsFHP->otherTreatmentArea) ? $hsFHP->otherTreatmentArea : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य भए खुलाउने']) }}
        </label> 
        <br>
      </div>
      <div class="form-group col-md-12">{{-- 12 --}}
       <p>
         २. घरबाट नजिकको स्वास्थ्य संस्थामा पुग्न लाग्ने समय
       </p>
       <br>
       <label class="">
         क) उपस्वास्थ्य चौकी / हेल्थसेन्टर पुग्न लाग्ने समय
         {{ Form::text('timeToHealthCenter', isset($hsFHP->timeToHealthCenter) ? $hsFHP->timeToHealthCenter : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'समय']) }}
       </label>
       <br>
       <label class="">
        ख) अस्पताल पुग्न लाग्ने समय
        {{ Form::text('timeToHospital', isset($hsFHP->timeToHospital) ? $hsFHP->timeToHospital : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'समय']) }}
      </label>
    </div>
    
    <div class="form-group col-md-12">{{-- 12 --}}
      <p>
        ३. तपाईको परिवारको सदस्यलाई कुनै दीर्घ रोग लागेको छ ? यदि छ भने                        
      </p>
      <td>
        <label class="radio-inline">
          १. छ 
          <input type="radio" onchange="displayFDiseaseTbl(this)" name="haveLTDisease" value="1"  {{ isset($hsFHP->haveLTDisease) && $hsFHP->haveLTDisease == '1' ? 'checked' : '' }}>
        </label> 
        <label class="radio-inline">
          २. छैन्
          <input type="radio" onchange="displayFDiseaseTbl(this)" name="haveLTDisease" value="0"  {{ isset($hsFHP->haveLTDisease) && $hsFHP->haveLTDisease == '0' ? 'checked' : '' }}>
        </label> 
      </td>
      <br>
      <table border="1" class="familyDiseaseTbl" style="display: none;">
        <thead>
          <tr>
            <th>रोगको नाम</th>
            <th>महिला</th>
            <th>पुरुष</th>
            <th>तेश्रो लिङ्गी </th>
          </tr>
        </thead>
        <tbody>
          @if (isset($longTermDisease) && count($longTermDisease)>0)
          <?php 
          $count = 0;
          $healthPositionInfo = isset($hsFHP->hsLTDiseaseDetail) ? json_decode($hsFHP->hsLTDiseaseDetail) : '';
          ?>
          @foreach ($longTermDisease as $lTDisease)
          <?php 
          $count++; 
          $singleDiseaseInfo = moreHouseDetailsFormUpdate($lTDisease->id, $healthPositionInfo, $compareJsonKey = 'diseaseId');          
          ?>
          <tr>                            
            <td>
              {{$lTDisease->nameNep}}
              <input type="hidden" name="hsLTDiseaseDetail[<?php echo $count;  ?>][diseaseId]" value="{{ $lTDisease->id }}">
              <input type="hidden" name="hsLTDiseaseDetail[<?php echo $count;  ?>][diseaseName]" value="{{ $lTDisease->nameNep }}">
            </td>
            <td>
              {{ Form::text("hsLTDiseaseDetail[$count][maleNum]", isset($singleDiseaseInfo['maleNum']) ? $singleDiseaseInfo['maleNum'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '' ]) }}
            </td>
            <td>
              {{ Form::text("hsLTDiseaseDetail[$count][femaleNum]",  isset($singleDiseaseInfo['femaleNum']) ? $singleDiseaseInfo['femaleNum'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '' ]) }}
            </td>
            <td>
              {{ Form::text("hsLTDiseaseDetail[$count][otherNum]",  isset($singleDiseaseInfo['otherNum']) ? $singleDiseaseInfo['otherNum'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '' ]) }}
            </td>
          </tr>
          @endforeach
          @endif
          
        </tbody>
      </table>
    </div>
  </div>
  <div class="msgDisplay"></div>
</div>
</div>