<div class="card ">
    <div class="card-body">
        <div class="row" style="margin-top: 10px;">
        <!-- Note : getMyFamilyDetails come from houseDetails insertMoreDetails function  -->
            <?php
                 $dtMemUpdate = isset($familyDeathDetails->hsDeathReport) ? json_decode($familyDeathDetails->hsDeathReport) : '';
            ?>
            <table class="table table-striped table-responsive">
                <thead>
                    <tr>
                      <th>@lang('commonField.extra.sn').</th>
                      <th>मृत्यु भएको छ</th>
                      <th>सदस्यको नाम</th>
                      <th>उमेर</th>
                      <th>कारण</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                      $houseOwnerDetails = getParentsName($hsDetails->citizen_infos_id);
                      $abCnt = 1;

                      $fullName = $lan =='nep' ? $houseOwnerDetails->fnameNep .''. $houseOwnerDetails->mnameNep .' '.$houseOwnerDetails->lnameNep :  $houseOwnerDetails->fnameEng .''. $houseOwnerDetails->mnameEng .' '.$houseOwnerDetails->lnameEng;
                      $currAge = convertMyAge($houseOwnerDetails->dobAD);

                      $singleDeathMemUpdate = moreHouseDetailsFormUpdate($compareKey = $houseOwnerDetails->id, $jsonData = $dtMemUpdate, $compareJsonKey = 'citizenId');

                     // dd($familyDeathDetails);

                    ?>
                    <tr>
                      <td>{{ $abCnt }}</td>
                      <td>
                        <input type="checkbox" class="isDeath" onchange="deathCheck(this)" name="deathMem[<?php echo $abCnt ?>][isDeath]" value="1" {{ $singleDeathMemUpdate['isDeath'] ==1 ? 'checked' : '' }}>
                      </td>

                      <td>
                         <input type="text" name="deathMem[<?php echo $abCnt ?>][fullName]" value="{{ $fullName }}" readonly>
                      </td>

                      <td> 
                          {{  $currAge }} YRS
                         <input type="hidden" name="deathMem[<?php echo $abCnt ?>][age]" value="{{ $currAge }}">
                      </td>
                      <td>
                         {{ Form::text("deathMem[$abCnt][deathReason]", isset($singleDeathMemUpdate) ? $singleDeathMemUpdate['deathReason'] : '', ['class' =>'form-control deathReason', 'placeholder'=>'breif death reason']) }}
                     </td>
                     <input type="hidden" name="deathMem[{{ $abCnt }}][citizenId]" value="{{ $houseOwnerDetails->id }}">
                    </tr>
                    <!-- list of all memember -->
                    <?php 
                        $abCnt = 2;
                        $dataCount = 1;
                    ?>
                    @foreach($getMyFamilyDetails as $deathMember)
                    <?php  
                       $dataCount ++;
                       $deathMemName = getParentsName($deathMember->relation_id); 
                       $otherMemName =  $lan =='nep' ? $deathMemName->fnameNep .''. $deathMemName->mnameNep .' '.$deathMemName->lnameNep :  $deathMemName->fnameEng .''. $deathMemName->mnameEng .' '.$deathMemName->lnameEng; 

                       $deathAge = convertMyAge($deathMemName->dobAD);

                       $multipleDeathMemUpdate = moreHouseDetailsFormUpdate($compareKey = $deathMemName->id, $jsonData = $dtMemUpdate, $compareJsonKey = 'citizenId');

                       // dd($deathMemName);
                    ?>
                    <tr>
                      <td>{{ $abCnt ++ }}</td>
                      <td>
                        <input type="checkbox" class="isDeath" onchange="deathCheck(this)"  name="deathMem[<?php echo $abCnt ?>][isDeath]" value="1" {{ $multipleDeathMemUpdate['isDeath'] ==1 ? 'checked' : '' }}>
                        
                      </td>
                      <td>
                          <input type="text" name="deathMem[<?php echo $abCnt ?>][fullName]" value="{{ $otherMemName }}" readonly>
                      </td>
                      <td>
                        {{ $deathAge }} YRS
                         <input type="hidden" name="deathMem[<?php echo $abCnt ?>][age]" value="{{ $deathAge }}">
                      </td>
                      <td>
                        {{ Form::text("deathMem[$abCnt][deathReason]", isset($multipleDeathMemUpdate) ? $multipleDeathMemUpdate['deathReason'] : '', ['class' =>'form-control deathReason', 'placeholder'=>'breif death reason']) }}
                      </td>
                        <input type="hidden" name="deathMem[{{ $abCnt }}][citizenId]" value="{{ $deathMemName->id }}">
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="msgDisplay"></div>
</div>