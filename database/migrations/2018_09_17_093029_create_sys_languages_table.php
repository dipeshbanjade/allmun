<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysLanguagesTable extends Migration
{
    public function up()
    {
        Schema::create('sys_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode', 100)->unique();
            $table->string('langEng');
            $table->string('langNep');
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('sys_languages');
    }
}
