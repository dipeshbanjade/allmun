<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class RoomUseFor extends Model
{
    protected $table = 'room_use_fors';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];

}