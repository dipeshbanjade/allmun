@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="">
      <div class="">
        <div class="card-head">
          <header><i class="fa fa-eye"></i><span>Description</span></header>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="panel tab-border card-box">
                    <header class="panel-heading panel-heading-gray custom-tab tab-head">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a href="#citizen" data-toggle="tab">Citizen</a>
                            </li>
                            <li class="nav-item"><a href="#houseDetails" data-toggle="tab" class="active">Ghar</a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="houseDetails">
                                  <div class="">
                                    <div class="card-head">
                                      <header><i class="fa fa-eye"></i>&nbsp;@lang('commonField.links.createHouseDetails')</header>
                                    </div>
                                    <a href="{{ route('houseDetails.create') }}" id="btnAddDepartment" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
                                      <span> @lang('commonField.links.createHouseDetails')</span>
                                    </a>

                                    <div class="table-scrollable" style="color:black;">
                                      <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
                                        <thead>
                                          <tr>
                                             <th width="5%">@lang('commonField.extra.sn')</th>
                                             <th width="5%">@lang('commonField.personal_information.image')</th>
                                             <th width="5%">@lang('commonField.address_information.toleNam')</th>
                                             <th width="10%">@lang('commonField.personal_information.houseOwner')</th>
                                             <th width="5%">@lang('commonField.extra.houseNum')</th>
                                             <th width="5%">@lang('commonField.extra.familyMember')</th>
                                             <th width="10%">@lang('commonField.personal_information.phoneNumber')</th>
                                             <th width="12%">@lang('commonField.extra.date')</th>
                                            <th width="17%"> @lang('commonField.extra.action') </th>
                                          </tr>
                                        </thead>
                                          <tbody>
                                              @if($house)
                                                  <?php $count = 1;    ?>
                                                  @foreach($house as $hs)
                                                  <?php  $slug = $hs->identifier;   ?>
                                                  <tr>
                                                       <td>{{ $count ++ }}</td>
                                                       <td><img src="{{ asset($hs->hsImgPath) }}" width="50" height="50"></td>
                                                       <td>{{ $hs->toleName }}</td>
                                                       <td>
                                                          {{ $lang === 'np' ? $hs->citizen_infos->fnameNep .' '. $hs->citizen_infos->mnameNep . ' ' .$hs->citizen_infos->mnameNep : $hs->citizen_infos->fnameEng .' '. $hs->citizen_infos->mnameEng . ' ' .$hs->citizen_infos->mnameEng  }}
                                                       </td>
                                                       <td>{{ $hs->hsNum }}</td>
                                                       <td>{{ $hs->fmMem }}</td>
                                                       <td>{{ $hs->fmHeadPh }}</td>
                                                       <td>{{ $hs->hsEstd }}</td>
                                                       <td title="{!! $hs->surName !!} - {!! $hs->surPhone !!}">
                                                          <a href="{!! route('houseDetails.edit', $slug) !!}" data-url="" id="">
                                                            <button class="btn btn-success btn-xs">
                                                              <i class="fa fa-pencil"></i>
                                                             </button>
                                                         </a>
                                                          <a href="#" class="deleteMe" data-url="" data-name="">
                                                           <button class="btn btn-danger btn-xs">
                                                             <i class="fa fa-trash-o "></i>
                                                           </button>
                                                         </a>


                                                         <a href="{!! route('addHsMoreDetails', $slug) !!}"  title="add more details on house" target="_blank">
                                                           <button class="btn btn-info btn-xs">
                                                             <i class="fa fa-plus"></i>
                                                           </button>
                                                         </a>
                                                         
                                                         <a href="{!! route('changeGharMuli',$hs->citizen_infos->idEncrip) !!}"  title="change gharmuli" target="_blank">
                                                           <button class="btn btn-success btn-xs">
                                                             <i class="fa fa-user"></i>
                                                           </button>
                                                         </a>
                                                         
                                                       </td>
                                                  </tr>
                                                  @endforeach
                                              @endif
                                          </tbody>
                                          </table>
                                          {{ $house->links() }}
                                        </div>
                                    </div>
                            </div>
                            <div class="tab-pane" id="citizen">
                              Nagarik table
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
  </div>
</div>
@endsection
@section('custom_script')
@include('admin.houseDetails.script')
@endsection