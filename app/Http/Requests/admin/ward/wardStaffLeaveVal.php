<?php

namespace App\Http\Requests\admin\ward;

use Illuminate\Foundation\Http\FormRequest;

class wardStaffLeaveVal extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'noOfDays' => 'required|numeric',
            'startDate' => 'required|date_format:"Y-m-d"',
            'endDate'   => 'required|date_format:"Y-m-d"',
            'leave_types_id' => 'required',
            'shortNoteEng'   => 'required|min:6|max:60000'
        ];
    }
}
