<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class gharBatoVerfiyVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:gharbato_verifies',
            'chalaniNumber' => 'required|numeric|unique:gharbato_verifies',
            'accountHolderName' => 'required', 
            'accountHolderAddress' => 'required',
            'municipalityName' => 'required', 
            'wardNumber' => 'required|numeric', 
            'sabikAddress' => 'required',
            'shrestaName' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
