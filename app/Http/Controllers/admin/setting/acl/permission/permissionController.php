<?php

namespace App\Http\Controllers\admin\setting\acl\permission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\acl\Permission;
use App\Http\Requests\admin\setting\acl\permissionValidation;

class permissionController extends Controller
{
    protected $viewPath = 'admin.acl.permission';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $page['page_title']       = 'Dashboard Permission';
        $page['page_description'] = 'all permission define';

        $permissionData  = Permission::orderby('created_at', 'Desc')->where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

        return view($this->viewPath . '.index', compact(['page', 'permissionData']));


    }

    public function store(permissionValidation $request)
    {
        $mySave = Permission::create([
                   'perName'    => $request->perName,
                   'moduleName' => $request->moduleName,
                   'routeName'  => $request->routeName,
                   'desc'       => $request->desc ?? 'N/A',
                   'status'     => 0,
                   'softDelete' => 0
                 ]);
        if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage'));
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
