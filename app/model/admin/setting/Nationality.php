<?php

namespace App\model\admin\setting;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    protected $table = 'nationalities';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];

}
