<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/dashboard/home';


    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // protected function credentials(\Illuminate\Http\Request $request)  // check if the user is active or not
    // {
    //     return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status' =>0];
    // }


}
