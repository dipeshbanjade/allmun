@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
      <div class="card-head">
        <header>
          <h3><i class="fa fa-eye"></i>
           &nbsp;&nbsp;<span>@lang('commonField.extra.ward') &nbsp;@lang('commonField.extra.staff')</span>
           </h3>
         </header>
      </div>
      <div class="card-body">
         <a href="{{ route('wardStaff.create') }}" class="btn btn-primary pull-right">
            <i class="fa fa-plus"></i>
            @lang('commonField.button.createStaff')
         </a>
         <?php $lan = getLan();    ?>
      <div class="table-scrollable">
          @if(count($allStaff) > 0)
              <label class="pull-right"><i class="fa fa-user text-success"></i> : <strong>{{ count($allStaff) }}</strong></label>
              <div class="clearfix"></div>
              <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4" role="grid" aria-describedby="example4_info">
                <thead>
                  <tr>
                    <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 10px;">@lang('commonField.extra.sn')</th>


                    <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 60px;">@lang('commonField.personal_information.image')</th>

                    <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 150px;">@lang('commonField.personal_information.fullName') </th>

                    <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">@lang('commonField.extra.department')</th>

                    <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">@lang('commonField.address_information.phone')</th>

                    <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 30px;">@lang('commonField.employer.biomatricIdNo')</th>

                    <th> @lang('commonField.extra.action') </th>
                  </tr>
                </thead>
                <tbody>
                  @php  $count = 1   @endphp
                      @foreach($allStaff as $staff)
                          <tr class="odd gradeX">
                            <td>{{ $count ++ }}</td>
                            <td><img src="{{ asset($staff->profilePic) }}" width="60" height="60"></td>
                            <td>
                               <a href="{{ isset($staff->idEncrip) ? route('wardStaff.show', $staff->idEncrip) : ''  }}">
                                  {{ $lan == 'np' ? $staff->firstNameNep . ' '. $staff->middleNameNep .' '. $staff->lastNameNep : $staff->firstNameEng .' '. $staff->middleNameEng . ' '.$staff->lastNameEng }}
                               </a> 
                               
                            </td>
                            <td>
                                 <?php 
                                     $deptName = getDepartMent($staff->departments_id);
                                   ?>

                                   {{ $lan == 'np' && isset($deptName) ? $deptName->nameNep : isset($deptName->nameEng)  }}
                                
                            </td>

                            <td>{{ $staff->phoneNumber }}</td>

                            <td>
                                {{ $staff->pounchId }}
                            </td>

                            <td>
                              <a href="{{ isset($staff->idEncrip) ? route('wardStaff.edit', $staff->idEncrip) : '#' }}" class="btn btn-success btn-xs">
                                <i class="fa fa-edit"></i>
                              </a>

                              <a href="javascript:void(0)" class="btn btn-danger btn-xs">
                                <i class="fa fa-trash-o" data-url="{{ route('wardStaff.delete', $staff->id) }}" class="deleteMe"></i>
                              </a>

                              <a href="{{ route('adminChangePwd', $staff->idEncrip) }}" class="btn btn-primary btn-xs" title="change password">
                                  <i class="fa fa-lock"></i>
                              </a> 
                                </td>
                              </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {{ $allStaff->links() }}
             @else
             <h3 align="center" class="fontColorBlack">
                 @lang('commonField.extra.pleaseInsertData');
             </h3>
          @endif
         
            </div>
          </div>
  </div>
  
</div>
@endsection