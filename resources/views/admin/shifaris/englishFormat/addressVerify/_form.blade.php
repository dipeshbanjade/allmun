<div class="right_col eng" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x eng">Address Verification</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

          @include('admin.shifaris.municipalityDetail')

          <div class="row top-part">
            <div class="col-md-6 ps-cn">
              <p><b class="ps">प. सं.:</b>
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
                @endif
              </p>
              <p align="left" class="star">
                <b class="cn">च. नं.:</b>
                {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
              </p>
            </div>
            <div class="col-md-6">
              <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
             <h3 align="center"><b> Subject: Address Verification </b></h3>
             {{ Form::hidden('letterSub', 'Address Verification') }}
             <h4 align="center"><b>To Whom It May Concern</b></h4>
           </div>
         </div>
         <div class="row">
           <div class="col-md-12 content-para">
            <p align="left">This is to certify that according to the ministry level meeting of Nepal Government <b> {{ Form::text('prevVdcName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b> Ward No <b>{{ Form::text('prevWardNo', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>  Nepal has been changed to  <b> <?php echo getChecker()->nameNep; ?></b> Ward No <b> 
              {{ Form::text('wardNo', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            </b>
          .All of the addresses are same &amp; doesnot make any difference using any of them. </p>
        </div>
      </div>
      <div class="col-md-12">
        <div class="text-right btm-last">
          <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
          <p>   <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">
            <option value="">@lang('commonField.extra.deginations_id')</option>
            @if(count($deginationsId) > 0)  <!-- selecting leave type -->
            @foreach($deginationsId as $deg)
            <option value="{{ $deg->id }}" {{!empty($data) && $data->deginations_id == $deg->id ? 'selected' : '' }} >
              {{ $deg->nameNep }}
            </option>
            @endforeach
            @endif
          </select>
        </p>
      </div>
    </div>
  </div>
  <!--views for nibedak detail -->
  <div class="clearfix"></div>
  <hr>
  @include('admin.shifaris.nibedakCommonField')

  <!-- END -->
</div>
</div>

</div>
</div>
</div>
</div>
</div>