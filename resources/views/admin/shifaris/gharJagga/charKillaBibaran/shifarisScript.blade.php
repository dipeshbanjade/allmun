<script type="text/javascript">
	var addRowBtn = $('#addNewRowInTable');
	var clickCount = 1;
	
	addRowBtn.on('click', function(e){
		e.preventDefault();

		clickCount++;

		var no = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="" value="'+clickCount+'">';



		var ward = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="ward[]">';

		var seatNum = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="seatNum[]">';

		var roadStatus = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="roadStatus[]">';

		var east = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="east[]">';

		var west = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="west[]">';

		var north = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="north[]">';

		var south = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="south[]">';

		var kaifiyat = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="kaifiyat[]">';



		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertNewRow = "<tr class='tblRow'>"
		+"<td>"+no+"</td>"
		+"<td>"+ward+"</td>"
		+"<td>"+seatNum+"</td>"
		+"<td>"+roadStatus+"</td>"
		+"<td>"+east+"</td>"
		+"<td>"+west+"</td>"
		+"<td>"+north+"</td>"
		+"<td>"+south+"</td>"
		+"<td>"+kaifiyat+"</td>"		
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#tableBody').append(insertNewRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

		addListener();
	});

	$(function(){
		/*add buttoin*/
		$("form[name='formCharKillaBibaran']").validate({
			rules:{
				
				refCode: {
					required: true,
					minlength: 3,
					maxlength: 20
				},
				wards_id :{
					required: true,
					minlength:1,
					maxlength:3,
					number: true
				},

				municipilities_id :{
					required: true,
					minlength:1,
					number : true
				},

				issuedDate : {
					required : true,
					date : true
				},

				chalaniNumber: {
					required: true,
					minlength: 1,
					maxlength: 30
					
				},
				
				municipalityName :{
					required: true,
					minlength:3,
					maxlength:255
				},
				
				wardNumber :{
					required: true,
					minlength: 1,
					maxlength : 3,
					number : true
				},
				
				sabikAddress :{
					required : true,
					minlength : 3,
					maxlength : 255
				},
				
				shrestaName :{
					required : true,
					minlength : 3,
					maxlength : 255
				},

				authorizedPerson :{
					required : true,
					minlength : 3,
					maxlength : 255					
				}
				
			},
			messages: {
				refCode: "Patra Sankhya is required",
				wards_id : "Ward id is required ",
				municipilities_id : "Muncipality is required",
				issuedDate : "Issue Date is required",
				chalaniNumber: "Chalani Number should be numeric",
				municipalityName : "Muncipality name is required ",
				wardNumber : "Ward number is required",
				sabikAddress : " Sabik address is required ",
				shrestaName : "Shresta name is required ",
				authorizedPerson : "Authorized person name is required"
			}
		});
	});
</script>