@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Different English Spell Certified<small> फरक फरक अंग्रेजी हिज्जे प्रमाणित
		</small></header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>
	{!! Form::model($data, ['method' => 'POST','route' => ['admin.diffEngSpellCert.store', $data->id], 'files'=>true, 'name' => 'frmDiffEngSpellCertEdit', 'class'=>'sifarishSubmitForm']) !!}						
	{{ Form::hidden('shifarisId',  $data->id )}}
	
	@include('admin.shifaris.other.diffEngSpellCert._form')
	<!-- <p class="pull-right">
		<button type="submit" class="btn btn-success pull-right">CREATE</button>
		<button type="submit" class="btn btn-active pull-right" onclick="">PRINT</button>
	</p> -->
	@include('admin.shifaris.commonButton')
	{{ Form::close() }}
	<div class="msgDisplay" style="z-index: 999999 !important"></div>
	
</div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
@include('admin.shifaris.script')
@include('admin.shifaris.other.diffEngSpellCert.script')
@endsection