<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class MritakHakdarShifarisVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:mritak_hakdar_shifaris',
            'chalaniNumber' => 'required|numeric|unique:mritak_hakdar_shifaris',
            'issuedDate' => 'required',
            'municipalityName' => 'required', 
            'wardNumber' => 'required|numeric', 
            'sabikAddress' => 'required',
            'nibedakName' => 'required', 
            'municipalityNameTwo' => 'required', 
            'dateSince' => 'required',
            'registrationNumber' => 'required',
            'deadPerson' => 'required',
            'rightfulHolderNumber' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
