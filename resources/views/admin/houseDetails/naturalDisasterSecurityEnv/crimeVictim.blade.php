<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <?php
             $crimeVictim     = isset($crimeVictim) ? $crimeVictim : '';
             $jsonCrimeVictim = isset($crimeVictim->crimeName)  ? json_decode($crimeVictim->crimeName) : '';
           ?>
            <div class="row color_red">
                <div class="form-group col-md-12">
                    <!--  -->
                    <p>
                       ५ तपाईको परिवार विगत १ वर्षभित्र निम्न अपराधिक घटनाबाट पिडित हुनु भएको छ ? (बहुउत्तर सम्भव)
                    </p>
                    <label class="radio-inline">
                        १. छ 
                        <input type="radio" name="haveCrimeVictim" value="1" class="rdoClickMe" {{ isset($crimeVictim->haveCrimeVictim) && $crimeVictim->haveCrimeVictim== 1 ? 'checked' : ''  }}>
                    </label> 
                    <label class="radio-inline">
                        २. छैन्
                        <input type="radio" name="haveCrimeVictim" value="0" class="rdoClickMe" {{ isset($crimeVictim->haveCrimeVictim) && $crimeVictim->haveCrimeVictim== 0 ? 'checked' : ''  }}>
                    </label><br>
                    <div class="col-md-12">

                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="हत्या" {{ !empty($crimeVictim->crimeName) && in_array('हत्या', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            १. हत्या
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="बलात्कार" {{ !empty($crimeVictim->crimeName) && in_array('बलात्कार', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            २. बलात्कार
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="अपहरण" {{ !empty($crimeVictim->crimeName) && in_array('अपहरण', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            ३. अपहरण
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="डर, धम्की" {{ !empty($crimeVictim->crimeName) && in_array('डर, धम्की', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            ४. डर, धम्की
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="चोरी" {{ !empty($crimeVictim->crimeName) && in_array('चोरी', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            ५. चोरी
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="डकैती" {{ !empty($crimeVictim->crimeName) && in_array('डकैती', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            ६. डकैती
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="चेलीबेटी बेचबिखन" {{ !empty($crimeVictim->crimeName) && in_array('चेलीबेटी बेचबिखन', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            ७. चेलीबेटी बेचबिखन
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="अन्य" {{ !empty($crimeVictim->crimeName) && in_array('अन्य', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            ९.अन्य
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="crimeName[]" value="कुनै पनि घटनाबाट पिडित नभएको" {{ !empty($crimeVictim->crimeName) && in_array('कुनै पनि घटनाबाट पिडित नभएको', $jsonCrimeVictim) ? 'checked' : ''  }}>
                            १०. कुनै पनि घटनाबाट पिडित नभएको 
                        </label><br />
                        <label class="radio-inline">
                             {{ Form::text('otherCrimeName', $crimeVictim ? $crimeVictim->otherCrimeName : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
                        </label>
                    </div>
                </div> 
            </div>

            <div class="form-group col-md-12">
                <p>
                   ६. तपाईको विचारमा यस स्थानमा जातजाति, लिगं वा समुदाय विशेष (जस्तै महिला, दलित, आदि ...) भएकै आधारमा भेदभावको अनुभव गर्नु भएको छ ?
                </p>
                <div class="col-md-12">
                    <label class="radio-inline">
                        १. छ
                        <input type="radio" name="havediscrimination" value="1" {{ isset($crimeVicti->havediscrimination) && $crimeVicti->havediscrimination ==1  ? 'checked' : '' }}>
                    </label>
                    <label class="radio-inline">
                        २. छैन
                        <input type="radio" name="havediscrimination" value="0" {{ isset($crimeVicti->havediscrimination) && $crimeVicti->havediscrimination ==0  ? 'checked' : '' }} checked>
                    </label>
                </div>
            </div> 
            <div class="msgDisplay"></div>
    </div>
</div>

