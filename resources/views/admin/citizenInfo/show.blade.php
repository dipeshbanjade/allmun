@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
<div class="card-box">
<div class="card-body">
	<div class="card-head">
		<header><i class="fa fa-eye"></i>&nbsp;@lang('commonField.extra.citizen')</header>
	</div>
	@if(count($citizen) > 0)
   <?php 
      $lan = getLan();
   ?>
		    <div class="row p-3">
		       <div class="col col-6">
       	          <img src="{{ asset($citizen->profilePic) }}" width="200" height="200">
       	          <br>
       	          <label>
                  <div class="clearfix"></div>
       	   	       <i class="fa fa-user"></i>
       	   	       <span>
       	   	       	     {!! $lan == 'np' ? ucfirst($citizen->fnameNep) .'&nbsp;'. $citizen->mnameNep .'&nbsp;'. $citizen->lnameNep : ucfirst($citizen->fnameEng) .'&nbsp;'.  $citizen->mnameEng .'&nbsp;'. $citizen->lnameEng   !!}
       	   	       </span>
       	          </label><br>
       	          <label>
       	                <i class="fa fa-phone"></i><span>{!! $citizen->phoneNumber !!}</span><br>
       	          	     <i class="fa fa-map-marker"></i>
       	          	       {!! $lan == 'np' ? $citizen->villageNameNep : $citizen->villageNameEng    !!} <br>
       	          </label><br>

                  <label>
                    <i class="fa fa-envelope"></i>
                    <span>
                        {!! $citizen->email  !!}
                    </span>
                  </label>
		       </div>
		       <div class="col col-6">
		       	   @include('partials.qrCode.full.qrlib')
                   <?php 
                       $name         =  ucfirst($citizen->fnameNep) . ' ' . $citizen->mnameNep  . ' ' .  $citizen->lnameNep .'/'.
                                        ucfirst($citizen->fnameEng) . ' ' . $citizen->mnameEng  . ' ' .  $citizen->lnameEng; 
                       $phoneCell    = $citizen->phoneNumber; 
                       $email        = $citizen->email; 
                       // if not used - leave blank! 
                       $addressLabel     = 'Home'; 
                       $addressPobox     = ''; 
                       $addressExt       =  $citizen->villageNameEng; 
                       $addressStreet    = '7th Avenue'; 
                       $addressTown      = 'New York'; 
                       $addressRegion    = 'NY'; 
                       $addressPostCode  = '91921-1234'; 
                       $addressCountry   = 'USA'; 

                       // we building raw data 
                       $codeContents  = 'BEGIN:VCARD'."\n"; 
                       $codeContents .= 'VERSION:2.1'."\n"; 
                       $codeContents .= 'FN:'.$name."\n"; 

                       $codeContents .= 'TEL;TYPE=cell:'.$phoneCell."\n"; 

                       $codeContents .= 'ADR;TYPE=home;'. 
                           'LABEL="'.$addressLabel.'":' 
                           .$addressExt.';' 
                       ."\n"; 

                       $codeContents .= 'EMAIL:'.$email."\n"; 

                       $codeContents .= 'END:VCARD'; 

                       // generating 
                       $myQr = QRcode::svg($codeContents); 
                       echo $myQr;
                       ?>
		       </div>
		     </div>
		       <hr>

	                 <div class="row">
                   <table class="table table-striped">
                        <tr>
                          <th>@lang('commonField.extra.refCode')</th>
                          <td>{{ $citizen->refCode }}</td>
                        </tr>
                        <!--  -->

                        <tr>
                          <th>@lang('commonField.extra.houseNum')</th>
                          <td>{{ $citizen->houseNumber }}</td>
                        </tr>

                        <tr>
                          <th width="20%">@lang('commonField.extra.citizenType')</th>
                          <td>
                             @if(isset($citizen->citizen_types_id))
                               {{ $lan == 'np' && isset($citizen->citizen_types_id) ? $citizen->citizenTypes->nameNep : $citizen->citizenTypes->nameEng }} 
                             @endif
                          </td>
                        </tr>

                        <tr>
                          <th>@lang('commonField.personal_information.dob')</th>
                          <td>BS: {{ $citizen->dobBS }}  / AD : {{ $citizen->dobAD }}</td>
                        </tr> 
                             
                        <tr>
                          <th> @lang('commonField.extra.nationality')</th>
                          <td>{!! isset($citizen->nationalities_id) ? $citizen->nationalities->nameNep .' '.  $citizen->nationalities->nameEng : '' !!}</td>
                        </tr>

                         <tr>
                           <th> @lang('commonField.extra.language')</th>
                           <td>{!! isset($citizen->sys_languages_id) ? $citizen->sys_languages->langEng . ' ' .$citizen->sys_languages->langNep : ''  !!} <td>
                         </tr>

                         <tr>
                           <th> @lang('commonField.extra.religious')</th>
                           <td>{!! isset($citizen->religiouses_id) ? $citizen->religiouses->nameEng .' '. $citizen->religiouses->nameNep  : '' !!} </td>
                         </tr>

                         <tr>
                           <th> @lang('commonField.extra.jatjati')</th>
                           <td>{!! isset($citizen->jatjatis_id) ? $citizen->jatjatis->nameEng . ' '. $citizen->jatjatis->nameNep : ''  !!}</td>
                         </tr>

                          <tr>
                           <th> @lang('commonField.extra.birthPlace')</th>
                           <td>{!! $citizen->birthplace !!}  </td>
                         </tr>

                          <tr>
                           <th> @lang('commonField.extra.grandFatherName')</th>
                           <td>
                              @if(isset($grandFatherData) && $grandFatherData)
                                   {{ $lan =='np' &&  isset($grandFatherData) ? $grandFatherData->fnameNep . ' '. $grandFatherData->mnameNep .' '. $grandFatherData->lnameNep  : $grandFatherData->fnameEng . ' '. $grandFatherData->mnameEng .' '. $grandFatherData->lnameEng   }}
                              @endif
                              
                            </td>
                         </tr>

                          <tr>
                           <th> @lang('commonField.extra.fatherName')</th>
                           <td>
                             @if(isset($fatherData) && $fatherData)
                               {{ $lan =='np' &&  isset($fatherData) ? $fatherData->fnameNep . ' '. $fatherData->mnameNep .' '. $fatherData->lnameNep  : $fatherData->fnameEng . ' '. $fatherData->mnameEng .' '. $fatherData->lnameEng   }}
                             @endif
                            </td>
                         </tr>
                          
                          <tr>
                           <th> @lang('commonField.extra.motherName')</th>
                           <td>
                           @if(isset($motherData) && $motherData)
                              {{ $lan =='np' &&  isset($motherData) ? $motherData->fnameNep . ' '. $motherData->mnameNep .' '. $motherData->lnameNep  : $motherData->fnameEng . ' '. $motherData->mnameEng .' '. $motherData->lnameEng   }}
                           @endif
                              
                            </td>
                         </tr>
                         <!--  -->
                         <tr>
                          <th> @lang('commonField.extra.husbandWifeName')</th>
                          <td>
                             @if(!empty($marriageData))
                                {!!
                                   $lan == 'np' ? $marriageData->fnameNep .'&nbsp;'. $marriageData->mnameNep .'&nbsp;'. $marriageData->lnameEng : $marriageData->mnameEng .'&nbsp;'. $marriageData->lnameEng   
                                !!}
                             @endif
                          </td>
                        </tr>

                         <!--  -->

                         <tr>
                           <th> @lang('commonField.personal_information.citizenship')</th>
                           <td>
                                <a href="{{URL::to('/').'/'.$citizen->citizenImgPath}}" target="_blank"> Download citizenship</a>
                            </td>
                         </tr>


                   </table>
                    <p class="p-3">
                         <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> 
                              <span>@lang('commonField.button.back')</span> 
                         </button>
                         @if(isset($citizen->idEncrip))
                            <a href="{{ isset($citizen->idEncrip) ? route('citizenInfo.edit', $citizen->idEncrip) : '#' }}" class="btn btn-success pull-right">
                            <i class="fa fa-edit"></i><span>@lang('commonField.button.edit')</span></a>
                         @endif
                    </p>
              </div><!--  -->
	</div>
	@endif

</div>
</div>
</div>
@endsection