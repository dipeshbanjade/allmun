@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-head">
            <header>
            <i class="fa fa-eye">&nbsp;@lang('commonField.links.fieldVisit')</i></header>
        </div>
        <div class="card-body ">
            <a href="{{ route('fieldVisit.create') }}" type="button" class="btn btn-primary pull-right" title="create field visit"><i class="fa fa-plus"></i><span> @lang('commonField.links.fieldVisit')</span></a> 
            <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
                <div class="mdl-tabs__panel is-active p-t-20" id="tab4-panel">
                    <div class="table-responsive tableHeight250">
                        <table class="table scrollable">
                            <thead>
                                <tr>
                                    <th width="5%">@lang('commonField.extra.sn')</th>
                                    <th>@lang('commonField.links.fieldVisit')</th>
                                    <th>@lang('commonField.extra.startDate')</th>
                                    <th>@lang('commonField.extra.endDate')</th>
                                    <th>@lang('commonField.extra.desc')</th>
                                    <th>@lang('commonField.extra.approvedBy')</th>
                                    <th width="10%">@lang('commonField.extra.action') </th>
                                </tr>
                                </thead>
                                <?php  
                                   $count = 1; 
                                   $lang = getLan();
                                 ?>
                                @if(count($fieldVisit) > 0)
                                   @foreach($fieldVisit as $fieldVisit)
                                   <tbody>
                                       <tr>
                                           <td>{{ $count ++ }}</td>
                                           <td>
                                                 {{ $lang == 'np' ? $fieldVisit->visit_types->nameNep :  $fieldVisit->visit_types->nameEng }} 
                                            </td>
                                           <td>
                                              {{ $fieldVisit->startDate }} 
                                              | {{ $fieldVisit->startTime }}
                                           </td>

                                           <td>{{ $fieldVisit->endDate }} 
                                           - {{ $fieldVisit->endTime }}</td>

                                           <td title="{!! $fieldVisit->shortDesc !!}">
                                               {!! str_limit($fieldVisit->shortDesc , 100) !!}
                                           </td>

                                           <td>
                                               <button class="{{ $fieldVisit->approveBy ? 'btn btn-success' : 'btn-danger' }} btn-sm">
                                                  @if(isset($fieldVisit->approveBy))
                                                     <?php
                                                        $approvedBy = Auth::user()->userLevel = 'mun' ? getMunStaff($fieldVisit->approveBy) : getWardStaff($fieldVisit->approveBy);
                                                      ?>
                                                  @endif
                                                   {{ 

                                                       isset($fieldVisit->approvedBy) ? $lang == 'np' ? ucfirst($approvedBy->firstNameNep) . ' '. $approvedBy->middleNameNep . ' ' . $approvedBy->lastNameNep   : ucfirst($approvedBy->firstNameEng) . ' '. $approvedBy->middleNameEng . ' ' . $approvedBy->lastNameEng  : 'Pending' 
                                                   }}
                                               </button>
                                               {{ $fieldVisit->approveBy ? $fieldVisit->approveBy : ''  }}
                                           </td>

                                           <td>
                                             @if($fieldVisit->approveStatus == '')
                                               <a href="{{ route('fieldVisit.edit', $fieldVisit->id) }}">
                                                 <i class="fa fa-pencil btn btn-success btn-xs"></i>
                                               </a>

                                               <a href="#" class="deleteMe" data-url="{{ route('fieldVisit.delete', $fieldVisit->id )}}" data-name="{{ $fieldVisit->refCode }}">
                                                <button class="btn btn-danger btn-xs">
                                                  <i class="fa fa-trash-o "></i>
                                                </button>
                                                </a>
                                             @endif
                                           </td>
                                       </tr>
                                   </tbody>
                                   @endforeach
                                </tbody>
                            </table>
                    @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
