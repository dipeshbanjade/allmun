<script type="text/javascript">
	var btnAddTableRow = $('#addTableRow');
	btnAddTableRow.on('click', function(e){
		e.preventDefault();

		var bodartha = '<input type="text" class="form-control" name="bodartha[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+bodartha+"</td>"
		+"<td>"+cancelButton+"</td>"
		+"</tr>";

		$('#sabik_address_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='formUnmarriedVerifyNep']").validate({
			rules:{
				issuedDate : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				chalaniNumber : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				sabikAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				fatherName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				mothername : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				unmarriedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {

				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				municipalityName : "Municipality Nepal is required",
				wardNumber : "Ward Number is required",
				sabikAddress : "Sabik Address is required",
				fatherName : "Father Name is required",
				mothername : "Mother Name is required",
				unmarriedPerson : "Unmarried Person is required",
				authorizedPerson : "Authorized Person is required"
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='husbandFather']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	                  $("input[name='husbandSabikAddress']").val(response.villageNameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>