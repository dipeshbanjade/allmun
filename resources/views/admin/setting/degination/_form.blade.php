<div class="card-body row">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('refCode', isset($prefix) ? $prefix : '', ['class'=>'mdl-textfield__input txtrefCode', 'placeholder'=>'']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.extra.refCode')</label>
    </div>

    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('nameNep', null, ['class'=>'mdl-textfield__input UnicodeKeyboardTextarea txtnameNep', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.personal_information.nameNep')</label>
    </div>   
     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('nameEng', null, ['class'=>'mdl-textfield__input txtnameEng', 'placeholder'=>'', 'row'=>'4']) }}    
        <label class = "mdl-textfield__label">@lang('commonField.personal_information.nameEng')</label>
    </div>                                             
</div>