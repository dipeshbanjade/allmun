<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditroad']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of electricity is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmroad']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of electricity is required"
            }
        });
	var btnAddRoad = $('.btnAddRoad');
	btnAddRoad.on('click', function(e){
		$('#roadModel').modal('show'); 
	});

	var btnEditRoad = $('.btnEditRoad');
	btnEditRoad.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.roadId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editroadModel").modal('show');
			}
		})
	});

	var frmUpdateRoad = $('#frmUpdateRoad');
  frmUpdateRoad.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.roadId').val();
    var url  = "{{URL::to('/')}}" + "/setting/road/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editroadModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editroadModel').modal('hide');
});
});
</script>