<script type="text/javascript">
	var currencyType = $('#currency_type');
	var currencyRate = $('#exchange_rate');
	var addRowBtn = $('#addNewRowInTable');
	var clickCount = 1;
	var totalAnnualIncome = 0;
	var totalAnnualIncomeFCurrency = 0;
	var firstYearInput = document.querySelectorAll('.first_year_income');
	var firstYearInputLength = firstYearInput.length;

	addRowBtn.on('click', function(e){
		var firstYearInput = document.getElementsByClassName('first_year_income');
		var firstYearInputLength = firstYearInput.length + 1;

		e.preventDefault();

		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + firstYearInputLength + '">';

		var sourceIncome = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="incomeSource[]">';

		var firstYear = '<input type="text" class="dashed-input-field first_year_income" placeholder="   *" required="required" name="firstYear[]">';
		var secondYear = '<input type="text" class="dashed-input-field second_year_income" placeholder="   *" required="required" name="secondYear[]">';
		var thirdYear = '<input type="text" class="dashed-input-field third_year_income" placeholder="   *" required="required" name="thirdYear[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertNewRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+sourceIncome+"</td>"
		+"<td>"+firstYear+"</td>"
		+"<td>"+secondYear+"</td>"
		+"<td>"+thirdYear+"</td>"
		+"<td>"+cancelButton+"</td>"
		+"</tr>";

		$('#annualIncomeJapanTd').append(insertNewRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			calcAllTotalAmount();
			manageSerialNumber();
			return false;
		});

		addListener();
	});

	currencyRate.on('keyup', function(e){
		calcAllTotalAmount();
	});

	function manageSerialNumber(){
		var firstYearInput = document.getElementsByClassName('first_year_income');

		var firstYearInputLength = firstYearInput.length;
	}


	function addListener(){
		var firstYearInput = document.getElementsByClassName('first_year_income');
		var secondYearInput = document.getElementsByClassName('second_year_income');
		var thirdYearInput = document.getElementsByClassName('third_year_income');

		var rowNumber = firstYearInput.length - 1;

		firstYearInput[rowNumber].addEventListener('keyup', function(){
			calcTotalAmount('.first_year_income');
		});

		secondYearInput[rowNumber].addEventListener('keyup', function(){
			calcTotalAmount('.second_year_income');
		});

		thirdYearInput[rowNumber].addEventListener('keyup', function(){
			calcTotalAmount('.third_year_income');
		});
	}

	function calcTotalAmount(className){
		totalAnnualIncome = 0;
		var inputField = document.querySelectorAll(className);
		var inputFieldLength = inputField.length;

		for (var i = 0; i < inputFieldLength; i++) {
			if(inputField[i].value == '' || inputField[i].value == null){
				continue;
			}
			totalAnnualIncome += parseInt(inputField[i].value);
		}

		var totalNrsFieldToBeUpdate;
		var totalForeignFieldToBeUpdate;

		if(className == '.first_year_income'){
			totalFieldToBeUpdate = '#totalNrsInFirst';
			totalForeignFieldToBeUpdate = '#totalUsdInFirst';
		}else if(className == '.second_year_income'){
			totalFieldToBeUpdate = '#totalNrsInSecond';
			totalForeignFieldToBeUpdate = '#totalUsdInSecond';
		}else if(className == '.third_year_income'){
			totalFieldToBeUpdate = '#totalNrsInThird';
			totalForeignFieldToBeUpdate = '#totalUsdInThird';
		}

		addtotalAmmount(totalAnnualIncome, totalFieldToBeUpdate, totalForeignFieldToBeUpdate);
	}

	function amountInForeignCurrency(totalAmt, foreignCurrencyRate){
		return totalAmt/foreignCurrencyRate;
	}

	function addtotalAmmount(totalAnnualIncome, totalFieldToBeUpdate, totalForeignFieldToBeUpdate){

		$(totalFieldToBeUpdate).val(totalAnnualIncome);

		var foreignCurrencyRate = currencyRate.val();
		$(totalForeignFieldToBeUpdate).val(amountInForeignCurrency(totalAnnualIncome, foreignCurrencyRate));
		$('#totalNrs').val($('#totalNrsInThird').val());
		$('#total_usd').val($('#totalUsdInThird').val());
	}

	function initiateKeyup(){
		$('.first_year_income').first().on('keyup', function(){
			calcTotalAmount('.first_year_income');
		});

		$('.second_year_income').first().on('keyup', function(){
			calcTotalAmount('.second_year_income');
		});

		$('.third_year_income').first().on('keyup', function(){
			calcTotalAmount('.third_year_income');
		});
	}

	function calcAllTotalAmount(){
		calcTotalAmount('.first_year_income');
		calcTotalAmount('.second_year_income');
		calcTotalAmount('.third_year_income');
	}

	initiateKeyup();
	calcAllTotalAmount();

	currencyType.on('change', function(){
		var ctype = document.querySelectorAll('#ctype');
		var currency = ctype.length;
		for (var i = 0; i < currency; i++) {
			ctype[i].value = this.value;
		}
	});


	$(function(){
		/*add buttoin*/
		
		$("form[name='formMohiKitta']").validate({
			rules:{
				issuedDate : {
					required : true,
					date : true
				},

				chalaniNumber: {
					required : true,
					minlength : 1,
					maxlength : 30
				},

				officeAddress : {
					required : true,
					minlength :3,
					maxlength : 255
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				address:{
					required: true,
					minlength: 3,
					maxlength : 255
				},
				seatNumber : {
					required: true,
					minlength: 1,
					number: true
				},
				kittaNumber : {
					required: true,
					minlength: 1,
					number: true
				},

				area : {
					required: true,
					minlength: 1
				},

				mohiName : {
					required: true,
					minlength: 3,
					maxlength : 255
				},

				landOwnerName : {
					required: true,
					minlength: 3,
					maxlength : 255
				},

				sarjaminDate : {
					required : true,
					date : true				
				},

				authorizedPerson : {
					required: true,
					minlength: 3,
					maxlength : 255				
				}
			},

			messages: {
				issuedDate : "Issue Date is required",
				chalaniNumber: "Chalani number is required",
				officeAddress: "Office address is requireed",
				wardNumber : "Ward Number must be number",
				address : "Address is required",
				seatNumber : "Seat number is required",
				kittaNumber: "Kitta number is required",
				area : "Area is required",
				mohiName: "Mohi name is required",
				landOwnerName: "Land owner name is required",
				sarjaminDate: "Sajamin date is required",
				authorizedPerson: " Authorized person name is required",
				deginations_id : " Degination id is required",
				

			}
		});
	});

</script>