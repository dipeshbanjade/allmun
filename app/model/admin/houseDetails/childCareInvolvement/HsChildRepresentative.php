<?php

namespace App\model\admin\houseDetails\childCareInvolvement;

use Illuminate\Database\Eloquent\Model;

class HsChildRepresentative extends Model
{
    Protected $table = 'hs_child_representatives'; 
}
