<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='formHandicapId']").validate({
			rules:{
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 2,
					maxlength : 30
				},
				officeAddress : {
					required : true,
					minlength : 2,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number : true
				},
				sabikAddress : {
					required : true,
					minlength : 2,
					maxlength : 30
				},
				applicant : {
					required : true,
					minlength : 2,
					maxlength : 30
				},
				type : {
					required : true					
				},
				authorizedPerson : {
					required : true,
					minlength : 2,
					maxlength : 30
				}
			},
			messages: {

				issuedDate: "Issued Date is required",
				chalaniNumber: "Chalani Number is required",
				officeAddress: "Office Address is required",
				wardNumber: "Ward Number is required",
				sabikAddress: "Sabik Address is required",
				applicant: "Applicant Name is required",
				type: "Handicap type is required"

			}
		});
	});
	
	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='applicant']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>