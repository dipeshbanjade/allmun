<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class ClassAddRecommVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:class_add_recomms',
            'issuedDate' => 'required',
            'organizationChairman' => 'required',
            'organizationAddress' => 'required',
            'municipalityName' => 'required', 
            'wardNum' => 'required', 
            'schoolName' => 'required',
            'class' => 'required',
            'applicationReason' => 'required',
            'addRemove' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'


        ];
    }
}
