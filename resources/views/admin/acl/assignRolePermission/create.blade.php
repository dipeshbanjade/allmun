@extends('main.app')
@section('page_title')
@section('page_description')
@section('content')
   <div class="card-body row">
     <div class="col-sm-12">
       <div class="panel">
         <header class="panel-heading panel-heading-red">
           Assign Role Permission </header>
           <div class="panel-body card-box">
             <div class="row">
               <div class="col-sm-12 col-md-12">
                 <h2 style="color: black;">Role</h2>
                
                 {{ Form::open(['route' => 'system.assignRolePermission.store', 'name' => 'frmPerRole']) }}
                      @include('admin.acl.assignRolePermission._form')
                       {{ Form::submit('CREATE', ['class' => 'btn btn-success pull-right']) }}
		               <button type="button" class="btn btn-default pull-right" id="btn-clear-search">Clear</button>
		             {{ Form::close() }}
           
             </div>

           </div>
         </div>
       </div>
     </div>
     </div>
@endsection