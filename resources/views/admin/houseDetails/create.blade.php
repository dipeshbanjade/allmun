@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
    <div class="card-body">
    </div>
    <div class="card-head">
        <header>
            <i class="fa fa-eye">
                @lang('commonField.links.createHouseDetails')
            </i>
        </header>
    </div>
    {{ Form::open(['route' => 'houseDetails.store', 'name' => 'frmHouseDetails', 'files' => true, 'enctype' => 'multipart/form-data']) }}
    @include('admin.houseDetails._form')
    <!--  -->
    <p class="p-4">
        <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> 
        <span>@lang('commonField.button.back')</span> 
        </button>
         {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
    {{ Form::close() }}
    </p>
</div>
@endsection
@section('custom_css')
  <style>
    .gharBibaran p{
      color: #28a745;
    }
    p{
      color: green
    }
  </style>
@endsection

@section('custom_script')
@include('admin.houseDetails.script')
@include('admin.houseDetails.houseDetailScript')
@endsection

