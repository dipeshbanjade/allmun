@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12">
  <div class="card-box">
    <div class="card-head">
     <header>Role Permission</header>

     <a href="{{ route('system.assignRolePermission.create') }}" class="btn btn-primary btn-xs btn btn-default btn-lg m-b-10 pull-right"><i class="fa fa-plus"><span>Add</span></i></a>
   </div>
   <div class="card-body">
    <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
      <!-- table start  -->
      <div class="mdl-tabs__panel is-active p-t-20" id="tab4-panel">
        <div class="table-responsive">

         <table class="table">
           <tbody>
             <tr>
               <th width="5%">SN</th>
               <th width="20%">Role Name</th>
               <th>Permission Name</th>
               <th width="5%">Action</th>
             </tr>


             @if(count($myRole) > 0)
                 <?php 
                      $count = 1;
                  ?>
                 @foreach($myRole as $role)
                 <tr>
                      <td>{{ $count ++ }}</td>
                     <td>{{ $role->roleName }} -></td>
                     <td>
                     <?php $countPermission = 1;  ?>
                     @foreach($role->permissions as $permission)
                        <strong title="{{ $permission->routeName }}">{{ $countPermission ++ }}.{{ $permission->perName }}</strong> &nbsp;&nbsp;
                     @endforeach
                     </td>
                     <td>
                         <a href="{{ route('system.assignRolePermission.edit' , $role->id) }}">
                           <i class="fa fa-edit fa-lg"></i>
                         </a>
                     </td>

                   </tr>
                 @endforeach
             @endif
           </tbody>
         </table>
         <div class="text-center">
           <button class="btn btn-outline-primary btn-round btn-sm">Load
             More</button>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
@endsection