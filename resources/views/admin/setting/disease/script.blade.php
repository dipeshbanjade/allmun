<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditdisease']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 50
                }
            },
            messages: {
                nameEng : "The name of Waste Management is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmdisease']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 50
                }
            },
            messages: {
          
                nameEng : "The name of Waste Management is required"
            }
        });
	var btnAddDisease = $('.btnAddDisease');
	btnAddDisease.on('click', function(e){
		$('#diseaseModel').modal('show'); 
	});

	var btnEditdisease = $('.btnEditdisease');
	btnEditdisease.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
                console.log(response);
				$('.diseaseId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
				$('.txttag').val(response.tag).change();
				$("#editdiseaseModel").modal('show');
			}
		})
	});

	var frmUpdatedisease = $('#frmUpdatedisease');
  frmUpdatedisease.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.diseaseId').val();
    var url  = "{{URL::to('/')}}" + "/setting/disease/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editdiseaseModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editdiseaseModel').modal('hide');
});
});
</script>