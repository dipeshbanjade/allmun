<?php

namespace App\Http\Controllers\admin\shifaris\business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\CloseBusiness;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\closeVal;
use DB;
use Auth;

use App\model\mylogic;

class CloseBusinessController extends Controller
{
    protected $viewPath = 'admin.shifaris.business.closeBusiness';
    protected $shifarisTableName = 'close_businesses';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixTurnOffBusiness');

    }

    public function index()
    {
        $page['page_title']       = 'Turn Off Business';
        $page['page_description'] = 'Turn Off Business';

        //TODO accordkign to ward user dispaly recored
        // $wardStaff = WardStaff::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

        // return view($this->viewPath . '.index', compact(['page']));
        
        $nirman = CloseBusiness::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        

    }

    public function create()
    {
        $page['page_title']       = 'Turn Off Business : create';
        $page['page_description'] = 'Turn Off Business details';


        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);


        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
            // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));


    }

    public function store(closeVal $request)
    {

        DB::beginTransaction();
        try {
            $businessCloseCount = count($request->businessCategory);

            $businessCategory = $request->businessCategory;
            $businessNature = $request->businessNature;
            $ward = $request->ward;
            $regNo = $request->regNo;
            $tollNo = $request->tollNo;
            $bhatoNaam = $request->bhatoNaam;
            $gharNo = $request->gharNo;
            $businessCloseArray = [];

            for ($i=0; $i < $businessCloseCount; $i++) {
                array_push($businessCloseArray, [
                    'businessCategory' => $businessCategory[$i],
                    'businessNature' => $businessNature[$i],
                    'ward' => $ward[$i],
                    'regNo' => $regNo[$i],
                    'tollNo' => $tollNo[$i],
                    'bhatoNaam' => $bhatoNaam[$i],
                    'gharNo' => $gharNo[$i]
                ]);
            }
            $businessCloseJson = json_encode($businessCloseArray);


            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

            $isShifarisExist = isset($request->shifarisId) ? CloseBusiness::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? CloseBusiness::findOrfail($isShifarisExist['id']) : new CloseBusiness;


            $save->createdBy = Auth::user()->id;
            
            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";
            $save->issuedDate  = $request->issuedDate;            
            $save->chalaniNumber = $request->chalaniNumber;
            $save->municipality = $request->municipality;
            $save->wardNumber = $request->wardNumber;
            $save->organizationAddress = $request->organizationAddress;
            $save->organizationType = $request->organizationType;
            $save->organizationWard = $request->organizationWard;
            $save->title = $request->title;
            $save->name = $request->name;
            $save->closedDate = $request->closedDate;
            $save->viewDate = $request->viewDate;
            $save->businessCloseDetail = $businessCloseJson;

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;
            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
            $request->chalaniNum = $request->chalaniNumber;
            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
          DB::rollback();
          return back()->withMessage(config('activityMessage.dataNotInserted'));
      }

  }

  public function show($id)
  {
        //
  }

  public function edit($id)
  {
        //
  }

  public function update(Request $request, $id)
  {
        //
  }

  public function destroy($id)
  {
        //
  }

  public function changeStatus(){

  }
}
