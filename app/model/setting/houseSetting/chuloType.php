<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class chuloType extends Model
{
    protected $table = 'chulo_types';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
