<?php

namespace App\Http\Controllers\admin\setting\houseSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseSetting\WasteManagement;
use App\model\mylogic;
use App\Http\Requests\admin\setting\WasteManagement\WasteManagValidation;
use Validator;
class wasteManagementController extends Controller
{
 protected $viewPath  = 'admin.setting.wasteManagement';
 protected $wasteManagementTbl = 'waste_managements';
 protected $prefix;

 public function __construct()
 {
    $this->middleware('auth');
    $this->prefix = config('activityMessage.wasteManagementPrefix');
}
public function index()
{
    $page['page_title']       = 'Waste Management';
    $page['page_description'] = 'list of all Waste Management';

    $allWasteManagement = WasteManagement::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
    
    return view($this->viewPath . '.index', compact(['page', 'allWasteManagement']));
}

public function store(WasteManagValidation $request)
{
    $prefix   = myLogic::sysRefCode($tablename = $this->wasteManagementTbl, $prefix = $this->prefix);
       $mySave = WasteManagement::create([
         'refCode' => $prefix,
         'nameNep' => $request->nameNep ?? 'N/A',
         'nameEng' => $request->nameEng,
         'tag'     => $request->tag,
         'status'  => 0,
         'softDelete' => 0
         ]);
       if ($mySave) {
        return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
    }else{
        return back()->withMessage(config('activityMessage.unSaveMessage'));
    }
}

public function edit($id)
{
     $data = WasteManagement::findOrFail($id);
     if ($data) {
        return response()->json([
          'success' => true,
          'refCode' => $data->refCode,
          'id'      => $data->id,
          'nameNep' => $data->nameNep,
          'nameEng' => $data->nameEng,
          'tag'     => $data->tag
          ]);
    }else{
        return response()->json([
          'success' => false,
          'message' => config('activityMessage.dataNotFound')
          ]);
    }
}


public function update(Request $request, $id)
{
       $validator = Validator::make($request->all(), [
           'nameEng'  => 'required|min:4|max:50',
           'nameNep'  => 'min:4|max:50',
           'tag'      => 'required'
           ]);

       if ($validator->fails()) {
          return response()->json([
             'success' => false,
             'message' => $validator->errors()->all()
             ]);
      }
      $update          = WasteManagement::findOrFail($id);
      $update->nameNep = $request->nameNep ?? 'N/A';
      $update->nameEng = $request->nameEng;
      $update->tag = $request->tag;
      


      $myUpdate = $update->update();
      if ($myUpdate) {
        return response()->json([
          'success' => true,
          'message' => config('activityMessage.updateMessage')
          ]);
    }else{
        return response()->json([
          'success'  => false,
          'message'  => config('activityMessage.notUpdated')
          ]);
    }
}

public function destroy($id)
{
     $del = WasteManagement::findOrFail($id);
     $del->softDelete = 1;
     $myDel = $del->update();
     if ($myDel) {
         return MyLogic::successChangeStatus($code = 200);
     }else{
        return MyLogic::successChangeStatus($code = 301);
    }
}
}
