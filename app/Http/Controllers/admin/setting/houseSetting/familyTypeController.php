<?php
namespace App\Http\Controllers\admin\setting\houseSetting;
use Illuminate\Http\Request;
use App\model\setting\FamilyType;
use App\Http\Controllers\Controller;

use App\model\mylogic;
use App\Http\Requests\admin\setting\familyType\FamilyTypeValidation;
use Validator;

class familyTypeController extends Controller
{
    protected $viewPath  = 'admin.setting.familyType';
    protected $familyTypeTbl = 'family_types';
    protected $prefix;
    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.familyTypePrefix');
    }
    
    public function index()
    {
        $page['page_title']       = 'Family type';
        $page['page_description'] = 'list of family type';

        $familyType = FamilyType::where(['status' => 0, 'softDelete' => 0])->orderby('nameEng', 'ASC')->get();

        
        return view($this->viewPath  . '.index', compact(['page', 'familyType'])); 
    }
    public function store(familyTypeValidation $request)
    {
          $prefix   = myLogic::sysRefCode($tablename = $this->familyTypeTbl, $prefix = $this->prefix);
          $mySave = FamilyType::create([
          'refCode' => $prefix,
           'nameNep' => $request->nameNep,
           'nameEng' => $request->nameEng,
           'status'  => 0,
           'softDelete' => 0
           ]);
          if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function edit($id)
    {
         $data = FamilyType::findOrFail($id);
         if ($data) {
           return response()->json([
             'success' => true,
             'id'      => $data->id,
             'nameNep' => $data->nameNep,
             'nameEng' => $data->nameEng
             ]);
        }else{
           return response()->json([
             'success' => false,
             'message' => config('activityMessage.dataNotFound')
             ]);
        }
    }

    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
          'nameEng'  => 'required|min:4|max:50',
          'nameNep'  => 'min:4|max:50',
          ]);

         if ($validator->fails()) {
             return response()->json([
                'success' => false,
                'message' => $validator->errors()->all()
                ]);
         }
         $update          = FamilyType::findOrFail($id);
         $update->nameNep = $request->nameNep ?? 'N/A';
         $update->nameEng = $request->nameEng;

         $myUpdate = $update->update();
         if ($myUpdate) {
           return response()->json([
             'success' => true,
             'message' => config('activityMessage.updateMessage')
             ]);
       }else{
           return response()->json([
             'success'  => false,
             'message'  => config('activityMessage.notUpdated')
             ]);
       }
    }

    public function destroy($id)
    {
       $del = FamilyType::findOrFail($id);
       $del->softDelete = 1;
       $myDel = $del->update();
       if ($myDel) {
        return MyLogic::successChangeStatus($code = 200);
        }else{
         return MyLogic::successChangeStatus($code = 301);
       }
    }
}
