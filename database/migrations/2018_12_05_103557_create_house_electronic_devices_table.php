<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseElectronicDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_electronic_devices', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->integer('createdBy')->unsigned()->nullable();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('house_details_id')->nullable()->unsigned();
            $table->foreign('house_details_id')->references('id')->on('house_details')->onDelete('cascade');
             
            $table->text('hsDevices')->nullable();
            // $table->boolean('haveDevice')->default(0);
            // $table->string('deviceName')->nullable();
            // $table->string('deviceNumber')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_electronic_devices');
    }
}
