<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWardStaffLeavesTable extends Migration
{
    public function up()
    {
        Schema::create('ward_staff_leaves', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode', 100)->unique();

             $table->integer('users_id')->nullable()->unsigned();
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');


            $table->integer('wards_id')->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('leave_types_id')->unsigned();
            $table->foreign('leave_types_id')->references('id')->on('leave_types')->onDelete('cascade');

            $table->string('noOfDays');

            $table->integer('approvedBy')->nullable()->unsigned();
            $table->foreign('approvedBy')->references('id')->on('users')->onDelete('cascade');

            $table->text('shortNoteNep')->nullable();
            $table->text('shortNoteEng');

            $table->date('startDate')->nullable();
            $table->date('endDate')->nullable();

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);

            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('ward_staff_leaves');
    }
}
