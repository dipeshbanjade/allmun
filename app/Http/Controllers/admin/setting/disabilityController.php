<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\setting\Disability;
use App\model\mylogic;
use Validator;

class disabilityController extends Controller
{
    protected $viewPath  = 'admin.setting.disability';
    protected $disabilityTbl = 'disabilities';
    protected $prefix;

    public function __construct()
    {
       $this->middleware('auth');
       $this->prefix = config('activityMessage.phonePrefix');
   }
   public function index()
   {
       $page['page_title']       = 'Disability';
       $page['page_description'] = 'disability Details';

       $allDisable = Disability::where(['softDelete' => 0, 'status'=>0])->paginate(config('activityMessage.pagination'));
       $prefix   = myLogic::sysRefCode($tablename = $this->disabilityTbl, $prefix = $this->prefix);
       return view($this->viewPath . '.index', compact(['page', 'allDisable', 'prefix'])); 
   }

   public function store(Request $request)
   {
          $mySave = Disability::create([
            'refCode' => $request->refCode,
            'nameNep' => $request->nameNep,
            'nameEng' => $request->nameEng,
            'status'  => 0,
            'softDelete' => 0
            ]);
          if ($mySave) {
           return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
       }else{
           return back()->withMessage(config('activityMessage.unSaveMessage'));
       }
   }


   public function edit($id)
   {
        $data = Disability::findOrFail($id);
        if ($data) {
           return response()->json([
             'success' => true,
             'refCode' => $data->refCode,
             'id'      => $data->id,
             'nameNep' => $data->nameNep,
             'nameEng' => $data->nameEng
             ]);
       }else{
           return response()->json([
             'success' => false,
             'message' => config('activityMessage.dataNotFound')
             ]);
       }
   }

   public function update(Request $request, $id)
   {
          $validator = Validator::make($request->all(), [
              'nameEng'  => 'required|min:3|max:50',
              ]);

          if ($validator->fails()) {
             return response()->json([
                'success' => false,
                'message' => $validator->errors()->all()
                ]);
         }
         $update                 = Disability::findOrFail($id);
         $update->nameNep        = $request->nameNep;
         $update->nameEng        = $request->nameEng;

         $myUpdate = $update->update();
         if ($myUpdate) {
           return response()->json([
             'success' => true,
             'message' => config('activityMessage.updateMessage')
             ]);
       }else{
           return response()->json([
             'success'  => false,
             'message'  => config('activityMessage.notUpdated')
             ]);
       }
   }

   public function destroy($id)
   {
        $del = Disability::findOrFail($id);
        $del->softDelete = 1;
        $myDel = $del->update();
        if ($myDel) {
            return MyLogic::successChangeStatus($code = 200);
        }else{
           return MyLogic::successChangeStatus($code = 301);
       }
   }
}
