@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
   <div class="col-sm-12 col-md-12">
     <div class="card-box">
      <div class="card-head">
       	<header>
       		<i class="fa fa-edit">
            @lang('commonField.extra.edit')
            </i>
       	</header>
      </div>
       </div>
            {!! Form::model($data, ['method' => 'POST','route' => ['wrdStaffDailyReport.update', $data->id],  'name' => 'formUpdailyReport']) !!}
        <div class="card-body row formColor">
                 @include('admin.ward.wardDailyReport._form')
                 <p class="pull-right">
                 	   {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success pull-right']) }}
                 	  <button type="button" class="btn btn-danger pull-right" onclick="history.back()">
                 	      @lang('commonField.button.back')
                 	  </button>
                 </p>
            {{ Form::close() }}
       </div>
       </div>
       </div>
     </div>
@endsection