<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditroomUseFor']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of room use for is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmroomUseFor']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of room use for is required"
            }
        });
	var btnAddroomUseFor = $('.btnAddroomUseFor');
	btnAddroomUseFor.on('click', function(e){
		$('#roomUseForModel').modal('show'); 
	});

	var btnEditroomUseFor = $('.btnEditroomUseFor');
	btnEditroomUseFor.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.roomuseforId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editroomUseForModel").modal('show');
			}
		})
	});

	var frmUpdateroomUseFor = $('#frmUpdateroomUseFor');
  frmUpdateroomUseFor.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.roomUseForId').val();
    var url  = "{{URL::to('/')}}" + "/setting/roomUseFor/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editroomUseForModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editroomUseForModel').modal('hide');
});
});
</script>