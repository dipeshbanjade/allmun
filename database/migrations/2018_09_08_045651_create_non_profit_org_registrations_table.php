<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonProfitOrgRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_profit_org_registrations', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->string('refCode', 100)->unique();

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->date('issuedDate');
            $table->string('chalaniNumber');
            $table->string('dartaCode', 100)->unique();
            $table->date('dartaDate');
            $table->string('companyName');
            $table->string('companyAddress');
            $table->string('subjectArea');
            $table->date('businessStartDate');
            $table->string('companyEmail');
            $table->string('companyContact');
            $table->string('operatorName');
            $table->string('operatorAddress');
            $table->string('operatorEmail');
            $table->string('operatorContact');
            $table->string('authorizedPerson');

            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');
            
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('non_profit_org_registrations');
    }
}
