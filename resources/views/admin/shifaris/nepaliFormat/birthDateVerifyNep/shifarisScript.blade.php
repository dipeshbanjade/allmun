<script type="text/javascript">
	var btnAddTableRow = $('#addTableRow');
	btnAddTableRow.on('click', function(e){
		e.preventDefault();

		var bodartha = '<input type="text" class="form-control" name="bodartha[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+bodartha+"</td>"
		+"<td>"+cancelButton+"</td>"
		+"</tr>";

		$('#sabik_address_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='formBirthDateVerifyNep']").validate({
			rules:{
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				fatherName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				motherName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number : true
				},
				sabikAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				personName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				birthDate : {
					required : true,
					date : true
				},
				birthPlace : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				fatherName : "Father Name  is required",
				motherName : "Mother Name is required",
				municipalityName : "Municipality Name is required",
				wardNumber : "Ward Number is required",
				sabikAddress : "Sabik Address is required",
				personName : "Person Name is required",
				birthDate : "Birth Date is required",
				birthPlace : "Birth Place is required"
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='fatherName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});

</script>