<?php

namespace App\model\shifaris;

use Illuminate\Database\Eloquent\Model;

class BirthDateVerification extends Model
{
    protected $table = 'birth_date_verifications';
}
