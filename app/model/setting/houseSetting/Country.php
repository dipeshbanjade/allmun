<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
