<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    protected $table = 'leave_types';
    protected $fillable = ['refCode', 'nameNep', 'nameEng', 'daysLeave', 'status', 'softDelete'];
}
