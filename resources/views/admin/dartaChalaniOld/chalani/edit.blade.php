@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
    <div class="card-body">
    </div>
    <div class="card-head">
        <header>
            <i class="fa fa-edit">
                @lang('commonField.dartaChalani.editChalani')
            </i>
        </header>
    </div>
    {!! Form::model($data, ['method' => 'POST','route' => ['updateChallani', $data->id], 'files'=>true, 'name' => 'updateChalani']) !!}
    @include('admin.dartaChalani.chalani._form')
    <!--  -->
    <p class="p-4">
        <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> 
            <span>@lang('commonField.button.back')</span> 
        </button>
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success pull-right']) }}
        {{ Form::close() }}
    </p>
</div>
@endsection