<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualIncomeVerificationsTable extends Migration
{

    public function up()
    {
        Schema::create('annual_income_verifications', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('refCode', 100)->unique();
            $table->date('issuedDate');
            $table->string('title', 100);
            $table->string('personName', 100);
            $table->string('municipality',100);
            $table->string('ward',100);
            $table->string('district', 100);
            $table->string('former', 100);
            $table->string('orgAddr', 100);
            $table->string('orgType', 100);
            $table->string('orgWard', 100);
            $table->string('incomeSource', 100);
            $table->string('incomeHolderName', 100);
            $table->string('relationWithSeeker', 100);
            $table->string('annualIncome', 100);
            $table->string('remarks', 100);
            $table->string('valuationInNrs', 100);
            $table->string('perDollarNrs', 100);
            $table->string('totalValueInDollars', 100);
            $table->string('authorizedPerson', 100);

            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('applicant_users')->onDelete('cascade');
                       
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annual_income_verifications');
    }
}
