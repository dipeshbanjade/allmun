<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditoccupation']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 100
                }
            },
            messages: {
                nameEng : "The name of Occupation is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmoccupation']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 100
                }
            },
            messages: {
          
                nameEng : "The name of Occupation is required"
            }
        });
	var btnAddOccupation = $('.btnAddOccupation');
	btnAddOccupation.on('click', function(e){
		$('#occupationModel').modal('show'); 
	});

	var btnEditOccupation = $('.btnEditOccupation');
	btnEditOccupation.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.occupId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editOccupModel").modal('show');
			}
		})
	});

	var frmUpdateoccupation = $('#frmUpdateoccupation');
  frmUpdateoccupation.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.occupId').val();
    var url  = "{{URL::to('/')}}" + "/setting/occupation/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editOccupModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editOccupModel').modal('hide');
});
});
</script>