<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class HandWashMethod extends Model
{
	protected $table = 'hand_wash_methods';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
