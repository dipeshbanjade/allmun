<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class courtRecommVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:court_fees',
            'issuedDate' => 'required',
            'courtAddress' => 'required',
            'municipalityName' => 'required', 
            'municipalityWard' => 'required', 
            'organizationAddress' => 'required',
            'organizationType' => 'required',
            'organizationWard' => 'required',
            'firstPersonTitle' => 'required',
            'firstPersonName' => 'required',
            'firstAccusion' => 'required',
            'secondPersonTitle' => 'required',
            'secondPersonName' => 'required',
            'secondAccusion' => 'required',
            'districtCourt' => 'required',
            'case' => 'required',
            'caseDate' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'

        ];
    }
}
