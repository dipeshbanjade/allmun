@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
                <header>
                <h3>
                  <i class="fa fa-eye">&nbsp;<span>
                    @lang('commonField.dartaChalani.darta')
                  </span></i></h3></header>
            </div>

            <button id="" class="dartaFrm btn btn-primary pull-right"><i class="fa fa-plus"></i><span>@lang('commonField.dartaChalani.createDarta')</span></button>
            <p>
              <i class="fa fa-calculator"></i>
              <span>{{ count($dartaList) }}</span>
            </p>
            <div class="table-scrollable" id="dropLabel">
              <table id="example4" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                         <thead>
                          <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">
                              @lang('commonField.extra.sn')
                            </th>

                            <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">
                              @lang('commonField.personal_information.image')
                            </th>
                            
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 150px;">
                              @lang('commonField.dartaChalani.dartaNo')
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">
                              @lang('commonField.dartaChalani.date')
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">
                              @lang('commonField.dartaChalani.office')
                            </th> 
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">
                              @lang('commonField.dartaChalani.page')
                              
                            </th> 

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">
                              @lang('commonField.extra.department')
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">
                              @lang('commonField.dartaChalani.receiverPerson')
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">
                              @lang('commonField.extra.time')
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 350px;">
                              @lang('commonField.extra.action')
                            </th>

                        </tr>
                  </thead>
                  <tbody>
                    @if($dartaList)
                       <?php 
                          $count = 1;
                       ?>
                       @foreach($dartaList as $darta)
                            <?php 
                              $rPerson =  getDeginationNameById($darta->dartaIsFor);
                              if ($rPerson) {
                              $rPersonDeg = $lang == 'np' && isset($rPerson) ? $rPerson->nameNep : $rPerson->nameEng;
                              }
                             ?>
                            <tr>
                               <td title="{!! $darta->dartaNo !!}">{{ $count ++ }}</td>
                               <td>
                                 <i class="fa fa-image"></i>
                               <?php
                                   $dartaImg = json_decode($darta->imagePath);
                                   echo count($dartaImg);
                               ?>
                               </td>
                               <td>{{ $darta->dartaNo }}</td>
                               <td>{{ $darta->dartaDate }}</td>
                               <td>
                                 @if($darta->other_offices_id)
                                     {{ $lang == 'np' ? $darta->otherOffices->nameNep : $darta->otherOffices->nameEng  }}
                                 @endif
                               </td>
                               <td>{{ $darta->dartaPage }}</td>
                               <td>
                                 @if($darta->departments_id)
                                     {{ $lang == 'np' ? $darta->departments->nameNep : $darta->departments->nameEng  }}
                                 @endif
                               </td>
                               <td>{{ isset($rPersonDeg) ? $rPersonDeg : '' }}</td>
                               <td>
                                 {{ $darta->created_at->diffForHumans() }}
                               </td>

                               <td>
                                 <span>
                                   <a href="{{ route('dartaShow', $darta->idEncript) }}" class="txt-color-success" 
                                       title="view details details">
                                      <i class="fa fa-fw fa-lg fa-eye text-success"> </i> </a>
                                  </span>
                            
                                <span> 
                                  <a href="{{ route('dartaEdit', $darta->idEncript) }}"  title="Edit data">
                                    <i class="fa fa-fw fa-lg fa-pencil-square-o" data-toggle="modal" data-target="#updateUNit" data-id=""></i>
                                </a>
                                </span>

                                <span>
                                  <a href="" class="txt-color-red deleteMe" 
                                      title="delete darta row" data-name="{{ $darta->dartaNo }}" data-id = "{{ $darta->id  }}" data-url="{{ route('deleteDarta', $darta->id) }}">
                                     <i class="fa fa-fw fa-lg fa-trash-o deletable text-danger"> </i> </a>
                                 </span>

                               </td>
                            </tr>
                       @endforeach
                    @endif
                  </tbody>
                </table>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
        @endsection
        @section('modelSection')
         <div id="frmAddDarta" class="modal fade bd-example-modal-lg" role="dialog" style="z-index: 9996;">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-body">
                       {!! Form::open(['route'=>'dartaStore', 'name'=>'frmdarta', 'method'=>'post', 'id'=>'addDarta', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
                            @include('admin.dartaChalani.darta._form')    
                            <div class="row">
                              {!! Form::submit(__('commonField.button.create'), ['class'=>'btn btn-success']) !!}
                               <button class="btn btn-danger" onclick="window.history.go(0); return false;">@lang('commonField.button.close')</button>
                            </p>
                      {!! Form::close() !!}
                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>

              </div>
            </div>
            </div>
            <!-- --------------------------- -->

            <div id="frmAddcompanyModel" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">@lang('commonField.dartaChalani.addCompany')</h4>
                   </div>
                   <div class="modal-body">
                        {!! Form::open(['route'=>'company.store', 'name'=>'frmcompany', 'method'=>'post', 'id'=>'addcompany']) !!}
                        <!-- Requireee companu form -->
                             <p class="pull-right">
                                 {{-- {!! Form::submit(__('commonField.button.create'), ['class'=>'btn btn-success']) !!} --}}
                                 <input type="submit" name="" class="btn btn-success">
                                  @include('admin.company._form')
                                   <button type="button" class="btn btn-danger" id="closeCompanyModel">@lang('commonField.button.close')</button>
                           </p>
                       {!! Form::close() !!}
                   </div>
                   <div class="modal-footer">
                     
                   </div>
                 </div>

               </div>
             </div>
     @endsection
<!-- modal ended here -->
@section('custom_script')
<script type="text/javascript">
    $(function(){
      /*------------------------------------*/
      $('.dartaFrm').on('click', function(e){
        $('#addDarta')[0].reset();
         $('#frmAddDarta').modal('show');
      });  

      $('#btnAddCompanyDetail').on('click',function(){
        $('#frmAddcompanyModel').modal('show');
        // $('#addcompany')[0].reset();
      });
       $('#closeCompanyModel').on('click',function (){
        $('#frmAddcompanyModel').modal('hide');
      });
      /*--------------------------------------*/
      $("form[name='frmdarta']").validate({
       rules:{
        dartaDate:{
          required:true,
          date:true
        },
        dartaSubject:{
          required:true,
          minlength:5,
          maxlength:200,
          alphanumeric:true
        },
        dartaNo:{
          required:true,
          number:true
        },
        dartaPage:{
          required:true
        },
        dartaPageDate:{
          required:true
        },
        reciveDate:{
          required:true
        },
        department_id:{
          required:true
        },
        other_office_id:{
          required:true
        },
        leterFor:{
          required:true
        }
       },
       messages: {
        dartaDate :" Darta date is required ",
        dartaSubject :" Darta subject date is required ",
        dartaNo :" Darta number is required",
        dartaPage :" Darta page is required ",
        dartaPageDate :" Darta page date is required ",
        reciveDate: " Recived date is required",
        department_id :" Department is required",
        other_office_id :" Office is required",
        leterFor :" The letter for is required ",
       }
      });
        $("form[name='frmcompany']").validate({
         rules:{
          nameNep : {
            required: true,
            minlength : 6,
            alphanumeric: true,
            maxlength : 255
          },   
          nameEng : {
            required: true,
            minlength : 6,
            maxlength : 255
          },
          phoneNumber : {
            number:true,
            minlength : 10,
            maxlength : 10
          },
          landlineNumber : {
            minlength: 9,
            maxlength : 9
          },
          email : {
            email : true,
          }
         },
         messages: {
           nameNep     : "Please enter company name in nepali with more than 6 character", 
           nameEng     : "Please enter company name in english with more than 6 character",
           phoneNumber  : "enter correct number with 10 digit",
           addr    : "company full address",
           email   : "email address must be in correct format"
         }
        });
        /*-----------------------*/
      });

    // $('.cboDeptId').on('change', function(){   
    //      $('#dartaIsFor').empty();
    //      var deptId = $(this).val();
    //      if (deptId) {
    //         var url  = "{{URL::to('/')}}" + "/dashboard/getStaffByDept/"+deptId;
    //         $('#dartaIsFor').val('');
    //         $.ajax({
    //           'type' : 'GET',
    //           'url'  : url,
    //           success:function(response){
    //             if (response.success == true) {
    //                 $.each(response.data, function(index, element) {
    //                     $('#dartaIsFor').append($('<option>', {
    //                         value: element.id,
    //                         text: element.firstNameNep + ' '+ element.middleNameNep +' '+ element.lastNameNep  
    //                     }));
    //                 });
    //             }
                
    //           }
    //         }).fail(function (response){
    //           infoDiv.addClass('alert alert-danger').append(response.message);
    //         });
    //      }
    // });
  </script>
@endsection