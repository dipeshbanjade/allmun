@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
   <div class="card-box">
       <div class="card-body">
			<div class="card-head">
				<header><i class="fa fa-eye"></i>&nbsp;@lang('commonField.dartaChalani.darta')</header>
			</div>
            <!--  -->
            <div class="row">
	             <table class="table table-striped">
                       <tr>
                       	  <th>@lang('commonField.dartaChalani.dartaNo')</th>
                       	  <td>
                       	     2075/06/05 <br>
                       	     0012636521	   
                       	  <td>
                       </tr>
                       <tr>
                       	  <th></th>
                       	  <td></td>
                       </tr>
                       <tr>
                       	  <th></th>
                       	  <td></td>
                       </tr>
                       <tr>
                       	  <th></th>
                       	  <td></td>
                       </tr>
                       <tr>
                       	  <th></th>
                       	  <td></td>
                       </tr>
                       <tr>
                       	  <th></th>
                       	  <td></td>
                       </tr>
	             </table>
             </div>
		</div>
	</div>
</div>
@endsection