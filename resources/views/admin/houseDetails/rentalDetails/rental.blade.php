<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
      <div class="form-group col-md-12">{{-- 12 --}}
        <table  class="table table-striped table-responsive">
          <thead>
            <tr>
              <th width="3%">@lang('commonField.extra.sn')</th>
              <th>@lang('commonField.personal_information.fullName')</th>
              <th>@lang('commonField.address_information.address')</th>
              <th>@lang('commonField.houseHoldSetting.phone')</th>
              <th>AGE</th>
              <th width="7%">@lang('commonField.extra.qualification')</th>

              <th width="7%">@lang('commonField.extra.occupation')</th>
              <th width="5%">
                <div id="btnAddMoreRentail" class="btn btn-success btn-sm">
                  <i class="fa fa-plus"></i>
                </div>
              </th>
            </tr>
          </thead>
          <tbody class="hsRentailDetails">
            <?php
            $rent = isset($rentailDetails->rentailDetails) ? json_decode($rentailDetails->rentailDetails) : '';
            if ($rent) {
              $count = 0;
              foreach ($rent as $rentDetails) {?>
              <tr>
               <td>{{ $count ++ }}
               </td>
               <?php //dd($rentDetails); ?>
               <td> <input type="text" class="from-control" name="rentailDetails[<?php echo $count  ?>][fullName]" value="<?php echo $rentDetails->fullName; ?>"></td>
               <td>
                 <input type="text" class="from-control" name="rentailDetails[<?php echo $count  ?>][addr]" value="<?php echo $rentDetails->addr  ?>">
               </td>
               <td>
                 <input type="text" class="from-control" name="rentailDetails[<?php echo $count  ?>][phoneNo]" value="<?php echo $rentDetails->phoneNo ?>">
               </td>
               <td>
                <input type="text" class="from-control" name="rentailDetails[<?php echo $count  ?>][age]" value="<?php echo isset($rentDetails->age) ?? $rentDetails->age ?>">
               </td>
               <td>
                 <select name="rentailDetails[<?php echo $count ?>][qualification]">
                   <option value="">Select qualification</option>
                   <?php
                   if (isset($qualificationId)) {
                     foreach($qualificationId as $qfy) {?>
                     <option value="<?php echo $qfy->nameNep ?>" {{ !empty($rentDetails->qualification) && $rentDetails->qualification == $qfy->nameNep ? 'selected' : '' }} >
                       <?php echo $qfy->nameNep;  ?>
                     </option>
                     <?php
                   }
                 }
                 ?>
               </select>
             </td>
             <td>
               <select name="rentailDetails[<?php echo $count ?>][occupation]">
                 <option value="">Select Occupation</option>
                 <?php
                 if (isset($occupationId)) {
                   foreach ($occupationId as $occupation) {?>
                   <option value="<?php echo $occupation->nameNep ?>" {{ !empty($rentDetails->occupation) && $rentDetails->occupation == $occupation->nameNep ? 'selected' : '' }} >
                     <?php echo $occupation->nameNep;  ?>
                   </option>
                   <?php
                 }
               }
               ?>
             </select>
           </td>
           <td>
             <div class="btn btn-danger btnRemoveItem"><i class="fa fa-minus"></i></div>
           </td>
         </tr>
         <?php
       }
     }
     ?>
   </tbody>
 </table>
</div>
</div>
<div class="msgDisplay"></div>
</div>
</div>
<script type="text/javascript">
 var addRowBtn = document.getElementById('btnAddMoreRentail');
 var rentCounter = 2222;
 var rowCounter = 0;

 addRowBtn.addEventListener('click', function(){
   rentCounter++;

   var no = ++rowCounter;

   var fullName = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="full name" name="rentailDetails['+ rentCounter +'][fullName]">';

   var address = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="address" name="rentailDetails['+ rentCounter +'][addr]">';

   var phoneNumber = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="Phone number 10 digit" name="rentailDetails['+ rentCounter +'][phoneNo]">';

   var age = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="page" name="rentailDetails['+ rentCounter +'][age]">';

   var qualification = '<select name="rentailDetails['+ rentCounter +'][qualification]">'
   +'<option value="">Select occupation</option>'+
   <?php
   if (isset($qualificationId)) {
     foreach($qualificationId as $qfy){ ?>
       '<option value="<?php echo $qfy->nameNep; ?>">'+
       '<?php echo $qfy->nameNep;  ?>'+
       '1</option>'
       <?php
     }
   }
   ?>
   +'</select>';

   /*-----------------------------------------------*/

   var occupation = '<select name="rentailDetails['+ rentCounter +'][occupation]">'+
   '<option value="">Select occupation</option>'+
   <?php
   if (isset($occupationId)) {
     foreach ($occupationId as $occupation) {?>
       '<option value="<?php echo $occupation->nameNep   ?>">'+
       '<?php echo $occupation->nameNep;  ?>'+
       +'</option>'
       <?php
     }
   }
   ?>
   +'</select>';         

   var cancelButton = '<div class="btn btn-danger btnRemoveItem"><i class="fa fa-minus"></i></div>';

   var insertNewRow = "<tr class='tblRow'>"
   +"<td>"+no+"</td>"
   +"<td>"+fullName+"</td>"
   +"<td>"+address+"</td>"
   +"<td>"+phoneNumber+"</td>"
   +"<td>"+age+"</td>"
   +"<td>"+qualification+"</td>"
   +"<td>"+occupation+"</td>"
   +"<td>"+cancelButton+"</td>"
   +"</tr>";

   $('.hsRentailDetails').append(insertNewRow);                        
   $('.btnRemoveItem').on('click', function(e){
     $(this).parent().parent().remove();
     return false;
   });
 });

</script>