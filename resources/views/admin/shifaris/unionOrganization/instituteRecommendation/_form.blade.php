<div class="right_col nep" role="main" style="min-height: 747px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">संस्था नबिकरण सिफारिस</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

         @include('admin.shifaris.municipalityDetail')

         <div class="row top-part">
          <div class="col-md-6">
            <p align="left">
              <b>@lang('commonField.extra.refCode'):
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
                @endif
              </b>
            </p>
            <p align="left">
              <b class="cn">च. नं.:
                {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
              </b>
            </p>
          </div>
          <div class="col-md-6">
            <p align="right">
              <b class="mt">मिति :
                {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
              </b>
            </p>
          </div>
        </div>
        <div class="row title-left">
          <div class="col-md-12">
            <p>श्री  
              {{ Form::text('operatorName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'operatorName', 'required' => 'required']) }},
            </p>
            <p>
              {{ Form::text('companyName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyName', 'required' => 'required']) }}।</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p align="center" class="font-size-24"> </p>
              <h4 align="center"><b>विषय: सिफारिस सम्बन्धमा।</b> </h4>
              {{ Form::hidden('letter_subject', 'सिफारिस सम्बन्धमा', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 content-para">
              <p>उपरोक्त विषयमा <b>
              {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
              </b> वडा नं. 
              <b>
                {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
              </b> (साविक 
              <!--  -->
              {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationAddress', 'required' => 'required']) }}
              <!--  -->
              <b><select onchange="changeSelect(this)" name="organizationType">
                <option value="गा.वि.स." 
                 {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
                 <option value="नगरपालिका" 
                 {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
                 <option value="उप महानगरपालिका" 
                 {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
                 <option value="महानगरपालिका" 
                 {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
              </select>
            </b>, वडा नं.
            {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationWard', 'required' => 'required']) }}
            )
            स्थित रहेको 
            {{ Form::text('organizationName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationName', 'required' => 'required']) }}
            नामक संस्था नवीकरण गर्नु पर्ने भएकोले सोको लागि"सिफारिस गरी पाऊँ" भनी यस वडा कार्यालयमा पर्न आएको सो संस्थाको चलानी नं.
            {{ Form::text('instituteChalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'instituteChalaniNumber', 'required' => 'required']) }}
            को निवेदन सम्बन्धमा त्यस कार्यालयको नियमानुसार गरी दिनुहुन सिफारिस गरिन्छ।</p>
          </div>
          <div class="col-md-6">
          </div>
          </div>
          <div class="text-right btm-last">
            <p>
              {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }}
            </p>
            <p> <b>
              <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
                @if(count($deginationsId) > 0)
                @foreach($deginationsId as $deg)
                <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                  {{ $deg->nameNep }} 
                </option>
                @endforeach
                @endif
              </select>
            </b></p>
          </div>
          <!--views for nibedak detail -->
          <div class="clearfix"></div>
          <hr>
          @include('admin.shifaris.nibedakCommonField')
          <!-- END -->
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div></div></div></div>