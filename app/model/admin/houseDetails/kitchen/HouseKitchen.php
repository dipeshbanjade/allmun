<?php

namespace App\model\admin\houseDetails\kitchen;

use Illuminate\Database\Eloquent\Model;

class HouseKitchen extends Model
{
    protected $table = 'house_kitchens';
}
