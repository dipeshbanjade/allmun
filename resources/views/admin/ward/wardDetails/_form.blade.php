<div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('nameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'woda name in nepali', 'title'=>'woda name in nepali', 'required'=>true]) }}
        <label class = "mdl-textfield__label" > @lang('commonField.personal_information.nameNep')</label>
      </div>
    </div>
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('nameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'woda name in english', 'title'=>'woda name in english', 'required'=>true]) }}
        <label class = "mdl-textfield__label" > @lang('commonField.personal_information.nameEng') </label>
      </div>
    </div>
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('landLineNumber', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'landline number', 'title'=>'landline number', 'required'=>'true']) }}
        <label class = "mdl-textfield__label" > @lang('commonField.personal_information.landLineNumber') </label>
      </div>
    </div>
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('faxNumber', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'woda fax number', 'title'=>'woda fax number']) }}
        <label class = "mdl-textfield__label" > @lang('commonField.personal_information.faxNumber') </label>
      </div>
    </div> 
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('addrNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'woda address in nepali', 'title'=>'woda address in nepali']) }}
        <label class = "mdl-textfield__label" > @lang('commonField.personal_information.addrNep')  </label>
      </div>
    </div>
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('addrEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'woda address in english', 'title'=>'woda address in english', 'required'=>'true']) }}
        <label class = "mdl-textfield__label" > @lang('commonField.personal_information.addrEng')</label>
      </div>
    </div>
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label style="color: black;"> @lang('commonField.address_information.province')</label>
        <select name="provinces_id" class="form-control" title="select province" required>
          <option value="">@lang('commonField.extra.provisions_id')</option>
          @if(count($provincesId) > 0)
             @foreach($provincesId as $provinces)
                <option value="{{ $provinces->id }}" 
                  <?php if (isset($data) && $data->provinces_id== $provinces->id) {
                      echo 'selected';
                  }    ?>
                  >
                   {{ $provinces->name }}</option>
             @endforeach
          @endif
          
        </select>
      </div>
    </div>    

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label style="color: black;">@lang('commonField.address_information.district')</label>
        <select name="districts_id" class="form-control" title="select district name" required>
          <option value="">@lang('commonField.extra.districts_id')</option>
           @if(count($districtId) > 0)
              @foreach($districtId as $district)
                 <option value="{{ $district->id }}" {{ isset($data) && $data->districts_id ==  $district->id ? 'selected' : '' }}>
                   {{ $district->districtNameEng }} - {{ $district->districtNameNep }}</option>
              @endforeach
           @endif
        </select>
      </div>
    </div> 

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" style="margin-top: 20px;">
        {{ Form::text('muncipilityHead', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'municipality head name', 'title'=>'municipality head name']) }}
        <label class = "mdl-textfield__label" >@lang('commonField.extra.muncipilityHead') </label>
      </div>
    </div>
    
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" style="margin-top: 20px;">
        {{ Form::number('bioDeviceId', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Biomatric identity number', 'title'=>'biomatric identity number']) }}
        <label class = "mdl-textfield__label" > @lang('commonField.employer.biomatricIdNo') </label>
      </div>
    </div>

    <div class="col-lg-12 pull-right">
     <label class="control-label col-md-3" style="color: black;">@lang('commonField.extra.uploadLogo')
     </label>
      <p>
          <label>
           <br>
           <img src="{{ isset($data) ? asset($data->wrdLogo) : '' }}" width="120" height="80" class="img img-thumbnai" id="wardLogo"><br>
            <input name="wardLogo" type='file' onchange="displayImage(this, 'wardLogo');" title="select ward logo" />
          </label>
      </p>
   </div>
</div> 