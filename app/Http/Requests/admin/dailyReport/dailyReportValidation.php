<?php

namespace App\Http\Requests\admin\dailyReport;

use Illuminate\Foundation\Http\FormRequest;

class dailyReportValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'sub'       => 'required|min:3|max:100',
            'date'      => 'required',
            'startTime' => 'required',
            'endTime'   => 'required',
            'desc'      => 'required|min:4|max:60000'
        ];
    }
}
