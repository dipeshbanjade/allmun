  <?php $municipalityNa = getChecker()->nameNep; 
    // echo $municipalityNa;
  ?>
  <div class="row top-part">
    <div class="col-md-6">
     <p align="left">
       <b>@lang('commonField.extra.refCode'):</b>
       @if(isset($data->refCode))
       {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
       @else
       {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
       @endif
     </p>
     <p align="left">
       <b> च. नं.:
        {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
      </b>
    </p>
  </div>
  <div class="col-md-6">
    <p align="right">
      <b class="mt">मिति :
        {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
      </b>
    </p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    <h4 align="center">
      <b>विषय: खाता नवीकरण  गरिदिने सम्बन्धमा।</b>
    </h4>
    {{ Form::hidden('letter_subject', 'खाता नवीकरण गरिदिने सम्बन्धमा', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left"> 
      <b>श्री 
        {{ Form::text('organizationName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationName', 'required' => 'required']) }},
      </b>
    </p>
    <p align="left">
      <b>
       {{ Form::text('organizationLocation', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationLocation', 'required' => 'required']) }}।
     </b>
   </p>
 </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">
    <p>प्रस्तुत विषयमा 
      <b>
       {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : $municipalityNa, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
     </b>
     वडा नं. 
     <b>
       {{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNum', 'required' => 'required']) }}
     </b>
     द्धारा संचालन भएको 
     {{ Form::text('purpose', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'purpose', 'required' => 'required']) }}
     प्रयोजन को 
     {{ Form::text('accountNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountNum', 'required' => 'required']) }}
     खाता नवीकरण गर्नु पर्ने भएकाले निम्न उल्लेखित ब्यक्तिहरु को संयुक्त हस्ताक्षरमा 
     {{ Form::text('year', null, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => 'year', 'required' => 'required']) }}
     वर्षका लागि नवीकरण   गरी दिनुहुन अनुरोध गरिन्छ।साथै सो खाताबाट प्राप्त हुने ब्याज च. हि. नं. 
     {{ Form::text('interest', null, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => 'interest', 'required' => 'required']) }}
     भुक्तानी  गरी दिनुहुन अनुरोध गरिन्छ।
   </p> 
 </div>
 <h4 align="center"><b>खाता संचालन हुने ब्यक्तिहरुको  विवरण</b> </h4>
 <div class="col-md-12">
  <div class="table-responsive">
    <table class="table table-bordered" id="accountTable">
      <thead>
        <tr>
          <th width="2%" rowspan="2">क्र.स.</th>
          <th rowspan="2">नाम</th>
          <th rowspan="2">पद</th>
          <th rowspan="2">कैफियत</th>
        </tr>
        <tr> 
        </tr>
      </thead>
      <tbody>
        
        <?php 
          $count =1;
          $accountNavigateStatement = !empty($data)? json_decode($data->accountNavigateStatement,true) :[1];
        ?>
        @foreach ($accountNavigateStatement as $singleData)
          <tr>
            <td>{{$count++ }}</td>
            <td>{{ Form::text('name[]', !empty($singleData['name'])?$singleData['name'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'name', 'required' => 'required']) }} </td>
            <td>{{ Form::text('post[]', !empty($singleData['post'])? $singleData['post'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'post', 'required' => 'required']) }} </td>
            <td>{{ Form::text('remarks[]', !empty($singleData['remark'])? $singleData['remark'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'remarks', 'required' => 'required']) }} </td>

            @if ($count ==2)
              <td class="add-btns" style="border: 0px;"><a href="" id="add" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
            @else
              <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>              
            @endif
          </tr>

        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
<div class="text-right btm-last">
  <p>   
    {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} 
  </p>
  <p>
   <b>
    <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
      @if(count($deginationsId) > 0)
      @foreach($deginationsId as $deg)
      <option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $deg->id == $data->deginations_id ? 'selected' : '' }}>
        {{ $deg->nameNep }} 
      </option>
      @endforeach
      @endif
    </select>
  </b></p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>