<?php

namespace App\model\admin\houseDetails\childrenEducation;

use Illuminate\Database\Eloquent\Model;

class ChildMontessori extends Model
{
	protected $table = 'child_montessoris';
}
