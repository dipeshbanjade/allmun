<script type="text/javascript">
$(function(){
		/*add buttoin*/
		$("form[name='frmDepartment']").validate({
			rules:{
				nameNep : {
					required : true,
					minlength : 3,
					maxlength : 100
				},
				nameEng : {
					required : true,
					minlength : 3,
					maxlength : 100
				}
			},
			messages: {
				nameNep : "The name of degination in Nepali is required",
				nameEng : "The name of degination is required"
			}
		});
	var btnEdDep = $('.btnEditDep');
	btnEdDep.on('click', function(e){
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				console.log(response);
				$('.depId').val(response.id),
				$('.txtnameNep').val(response.nameNep);
				$('.txtnameEng').val(response.nameEng);
				$("#editDepModel").modal('show');
			}
		})
	});

	var btnAddDepartment = $('.btnAddDepartment');
	btnAddDepartment.on('click', function(e){
		$('#DepModel').modal('show'); 
	});


	var frmUpdateDept = $('#frmUpdateDept');
	frmUpdateDept.on('submit', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var id   = $('.depId').val();
		var url  = "{{URL::to('/')}}" + "/setting/department/"+id+"/update";
		$.ajax({
			'type' : 'POST',
			'url'  : url,
			'data'    : data,
			success : function(response){
				console.log(response);
				if (response.success==true) {
					$('.infoDiv').append(response.message).addClass('alert alert-success');
				}
				if (response.success==false) {
					$.each(response.message, function(key, value){
						$('.infoDiv').append(value).addClass('alert alert-danger');
					})
				}
			},complete:function(){
				$("#editDepModel").modal('hide');
				setTimeout(location.reload.bind(location), 1500);
			}
		})
		.fail(function (response) {
			alert('data already exists');
		});
		$('#editDepModel').modal('hide');
	});
});
</script>