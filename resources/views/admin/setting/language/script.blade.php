<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditLanguage']").validate({
            rules:{
                langEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                langEng : "The name of language is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmsetlanguage']").validate({
            rules:{

                langEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                langEng : "The name of language is required"
            }
        });
	var btnAddLanguage = $('.btnAddLanguage');
	btnAddLanguage.on('click', function(e){
		$('#languageModel').modal('show'); 
	});

	var btnEditlanguage = $('.btnEditlanguage');
	btnEditlanguage.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.langId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.langNep);
				$('.txtNameEng').val(response.langEng);
				$("#editLangModel").modal('show');
			}
		})
	});

	var frmUpdateLanguage = $('#frmUpdateLanguage');
  frmUpdateLanguage.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.langId').val();
    var url  = "{{URL::to('/')}}" + "/setting/set-language/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editLangModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editLangModel').modal('hide');
});
});
</script>