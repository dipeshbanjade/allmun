<?php

namespace App\Http\Middleware;
use Route as LaravelRoute;
use AppHelper;
use Closure;
use Auth;
use App\model\admin\acl\Permission;
use App\model\admin\acl\Role;
use Request;

class authorizedRoute
{
    public function handle($request, Closure $next)
    {
        $routePrefix       = $request->route()->getPrefix();
        $currenctLoginUser = Auth::user()->userLevel;
        $roleId            = Auth::user()->roles_id;
        $currentRoute      = Request::route()->getName();

        // dd($routePrefix);

        $munPrefix = ['/setting', '/dashboard', '/municipility', '/ward']; 
        $wrdPrefix = ['/ward', '/dashboard'];

        if($currenctLoginUser === 'dev'){   // super user
            return $next($request);
        }

        if ($routePrefix === '/houseDetail') {  // for house details
           $rolePer = Role::find($roleId)->permissions()->get();
           foreach ($rolePer as $per) {
             if ($per->routeName ==='houseDetails.index') {
              return $next($request);
             }
           }
        }
       if ($currenctLoginUser === 'mun' || $currenctLoginUser === 'wrd' || $currenctLoginUser === 'dev') {
         if ($routePrefix === '/shifaris') {  // for sifarish 
            $rolePer = Role::find($roleId)->permissions()->get();
            foreach ($rolePer as $perm) {
              if ($perm->routeName ==='allShifaris') {
               return $next($request);
              }
            }
         }
       }
       if ($currenctLoginUser === 'mun' || $currenctLoginUser === 'wrd' || $currenctLoginUser === 'dev') {
           if ($routePrefix === '/companyLeave') {  // for sifarish 
              $rolePer = Role::find($roleId)->permissions()->get();
              foreach ($rolePer as $perm) {
                if ($perm->routeName ==='setUpCompanyLeave') {
                 return $next($request);
                }
              }
           }
       }
       /*chalani*/
       if ($currenctLoginUser === 'mun' || $currenctLoginUser === 'wrd' || $currenctLoginUser === 'dev') {
         if ($routePrefix === '/challani') {  // for chalani 
            $rolePer = Role::find($roleId)->permissions()->get();
            foreach ($rolePer as $perm) {
              if ($perm->routeName ==='allChalani') {
               return $next($request);
              }
            }
         }
       }
       /*darta*/
       if ($currenctLoginUser === 'mun' || $currenctLoginUser === 'wrd' || $currenctLoginUser === 'dev') {
         if ($routePrefix === '/darta') {  // for darta 
            $rolePer = Role::find($roleId)->permissions()->get();
            foreach ($rolePer as $perm) {
              if ($perm->routeName ==='allDarta') {
               return $next($request);
              }
            }
         }
       }

        if ($currenctLoginUser === 'mun') {  // municipility user
            if (in_array($routePrefix, $munPrefix)) {
                $chck = $this->hasSystemRole($roleId, $currentRoute);
                if ($chck) {
                  return $next($request);
                }else{
                    $this->redirectHomePage();
                }
            }
        } 
        /**/
        if ($currenctLoginUser === 'wrd') {  // ward user
            if (in_array($routePrefix, $wrdPrefix)) {
                $check = $this->hasSystemRole($roleId, $currentRoute);
                if ($check) {
                  return $next($request);
                }else{
                    $this->redirectHomePage();
                }
            }
        }

        return redirect()->route('home')->withMessage(config('activityMessage.accessDenied'));
    }

    public function hasSystemRole($roleId, $currentRoute){   // login user role Id
      if($currentRoute === 'system.logout'){
        return true;
      }
      $rolePer = Role::find($roleId)->permissions()->get();
      foreach ($rolePer as $per) {
          if($per->routeName === $currentRoute){
             return true;
          }
      }
    }

    public function isHouseDetailsUser($routePrefix){
      $rolePer = Role::find($roleId)->permissions()->get();
      foreach ($rolePer as $per) {
        if ($per->routeName ==='houseDetails.index' && $routePrefix ==='/houseDetail') {
         return true;
        }
      }
    }
    /**/
    public function redirectHomePage(){
     return redirect()->route('home')->withMessage(config('activityMessage.accessDenied'));
    }
}
