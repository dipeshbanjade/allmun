<?php

namespace App\model\admin\otherOffice;

use Illuminate\Database\Eloquent\Model;

class otherOffice extends Model
{
    protected $table = 'other_offices';
}
