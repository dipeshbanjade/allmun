<?php

namespace App\model\admin\houseDetails\childCareInvolvement;

use Illuminate\Database\Eloquent\Model;

class HsFamilyLivingDetail extends Model
{
	protected $table = 'hs_family_living_details';
}
