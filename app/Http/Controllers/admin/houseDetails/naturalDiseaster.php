<?php

namespace App\Http\Controllers\admin\houseDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\mylogic;
use App\model\admin\houseDetails\diseaster\hsNaturalDiseaster;
use App\model\admin\houseDetails\diseaster\hsHaveNaturalDiseaster;
use App\model\admin\houseDetails\diseaster\hsHaveWildAnimalPro;
use App\model\admin\houseDetails\diseaster\hsCrimeVictim;
use App\model\admin\houseDetails\diseaster\hsEnvironmentProblem;

use Auth;
use DB;
use Validator;
class naturalDiseaster extends Controller
{

	public function __construct()
	{
	 $this->middleware('auth');
	}
    public function naturalDiseaster(Request $request){
      DB::beginTransaction();
      $jsonNaturalDiseasterName   = $request->diseasterName ? json_encode($request->diseasterName) : ''; 
      if ($request->myHouseId && $request->fm_member_id) {
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = hsNaturalDiseaster::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
         $naturalDiseaster  = !$isExistsHsNum['house_details_id'] ? new hsNaturalDiseaster : hsNaturalDiseaster::findOrfail($isExistsHsNum['id']);

         $naturalDiseaster->createdBy         = Auth::user()->id;
         $naturalDiseaster->municipilities_id = $ishouseExists['municipilities_id'];
         $naturalDiseaster->wards_id          = $ishouseExists['wards_id'];
         $naturalDiseaster->house_details_id  = $ishouseExists['id'];
         $naturalDiseaster->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
         
         $naturalDiseaster->haveNaturalDiseaster = $request->haveNaturalDiseaster;
         $naturalDiseaster->diseasterName      = $jsonNaturalDiseasterName;
         $naturalDiseaster->otherNaturalDiseasterName     = $request->otherNaturalDiseasterName;
        
         $naturalDiseaster->status            = 0;
         $naturalDiseaster->softDelete        = 0;
         $mySave = !$isExistsHsNum['house_details_id'] ? $naturalDiseaster->save() : $naturalDiseaster->update();

         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_natural_diseasters', $tblId = $naturalDiseaster->id);
         if ($mySave && $log) {
          DB::commit();
          return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
            ]);
          // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
          // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
        }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.dataNotInserted')
            ]);
         // return back()->withMessage(config('activityMessage.unSaveMessage'));
       }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
     }
    }else{   
     return back()->withMessage(config('activityMessage.idNotFound'));
    }
    }else{    
     return back()->withMessage('House number is missing please try to insert it from browser');
    }
    }

    public function haveGetDiseasterProblem(Request $request){
       DB::beginTransaction();
       $jsonDiseasterName   = $request->problemName ? json_encode($request->problemName) : ''; 
       if ($request->myHouseId && $request->fm_member_id) {
        $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
        $isExistsHsNum = hsHaveNaturalDiseaster::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
        if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
         try {
          $naturalDiseaster  = !$isExistsHsNum['house_details_id'] ? new hsHaveNaturalDiseaster : hsHaveNaturalDiseaster::findOrfail($isExistsHsNum['id']);

          $naturalDiseaster->createdBy         = Auth::user()->id;
          $naturalDiseaster->municipilities_id = $ishouseExists['municipilities_id'];
          $naturalDiseaster->wards_id          = $ishouseExists['wards_id'];
          $naturalDiseaster->house_details_id  = $ishouseExists['id'];
          $naturalDiseaster->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
          
          $naturalDiseaster->haveGetDiseasterProblem = $request->haveGetDiseasterProblem;
          $naturalDiseaster->problemName      = $jsonDiseasterName;
          $naturalDiseaster->otherProblemName     = $request->otherProblemName;
         
          $naturalDiseaster->status            = 0;
          $naturalDiseaster->softDelete        = 0;
          $mySave = !$isExistsHsNum['house_details_id'] ? $naturalDiseaster->save() : $naturalDiseaster->update();

          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_have_natural_diseasters', $tblId = $naturalDiseaster->id);
          if ($mySave && $log) {
           DB::commit();
           return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
            ]);
           // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
         }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.dataNotInserted')
            ]);

          // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
      } catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
      }
     }else{   
      return back()->withMessage(config('activityMessage.idNotFound'));
     }
     }else{    
      return back()->withMessage('House number is missing please try to insert it from browser');
     }
    }

    public function wildAnimalProblem(Request $request){
       DB::beginTransaction();
       $jsonProblemFace   = $request->problemFace ? json_encode($request->problemFace) : ''; 
       if ($request->myHouseId && $request->fm_member_id) {
        $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
        $isExistsHsNum = hsHaveWildAnimalPro::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
        if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
         try {
          $wildAnimalProblem  = !$isExistsHsNum['house_details_id'] ? new hsHaveWildAnimalPro : hsHaveWildAnimalPro::findOrfail($isExistsHsNum['id']);

          $wildAnimalProblem->createdBy         = Auth::user()->id;
          $wildAnimalProblem->municipilities_id = $ishouseExists['municipilities_id'];
          $wildAnimalProblem->wards_id          = $ishouseExists['wards_id'];
          $wildAnimalProblem->house_details_id  = $ishouseExists['id'];
          $wildAnimalProblem->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
          
          $wildAnimalProblem->haveWildAnimalPro = $request->haveWildAnimalPro;
          $wildAnimalProblem->problemFace       = $jsonProblemFace;
          $wildAnimalProblem->otherProblemFace  = $request->otherProblemFace;
         
          $wildAnimalProblem->status            = 0;
          $wildAnimalProblem->softDelete        = 0;
          $mySave = !$isExistsHsNum['house_details_id'] ? $wildAnimalProblem->save() : $wildAnimalProblem->update();

          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_have_wild_animal_pros', $tblId = $wildAnimalProblem->id);
          if ($mySave && $log) {
           DB::commit();
           return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
            ], 200);
           // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
         }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.dataNotInserted')
            ], 200);
          // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
      } catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
      }
     }else{   
      return back()->withMessage(config('activityMessage.idNotFound'));
     }
     }else{    
      return back()->withMessage('House number is missing please try to insert it from browser');
     }
    }

    public function crimeVictim(Request $request){ // crime victim
      DB::beginTransaction();
      // dd($request);
      $jsonCrimeName   = $request->crimeName ? json_encode($request->crimeName) : ''; 
      if ($request->myHouseId && $request->fm_member_id) {
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = hsCrimeVictim::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
         $crimeVictim  = !$isExistsHsNum['house_details_id'] ? new hsCrimeVictim : hsCrimeVictim::findOrfail($isExistsHsNum['id']);

         $crimeVictim->createdBy         = Auth::user()->id;
         $crimeVictim->municipilities_id = $ishouseExists['municipilities_id'];
         $crimeVictim->wards_id          = $ishouseExists['wards_id'];
         $crimeVictim->house_details_id  = $ishouseExists['id'];
         $crimeVictim->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
         
         $crimeVictim->haveCrimeVictim   = $request->haveCrimeVictim;
         $crimeVictim->crimeName         = $jsonCrimeName;
         $crimeVictim->otherCrimeName    = $request->otherCrimeName;
         $crimeVictim->haveDiscrimination = $request->havediscrimination;
        
         $crimeVictim->status            = 0;
         $crimeVictim->softDelete        = 0;
         $mySave = !$isExistsHsNum['house_details_id'] ? $crimeVictim->save() : $crimeVictim->update();

         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_crime_victims', $tblId = $crimeVictim->id);
         if ($mySave && $log) {
          DB::commit();
          return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
            ], 200);
          // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
          // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
        }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.unSaveMessage')
            ]);
         // return back()->withMessage(config('activityMessage.unSaveMessage'));
       }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
     }
    }else{   
     return back()->withMessage(config('activityMessage.idNotFound'));
    }
    }else{    
     return back()->withMessage('House number is missing please try to insert it from browser');
    }
    }

    public function environmentProblem(Request $request){
        DB::beginTransaction();
        $jsonEnvironmentProblem   = $request->environmentProblem ? json_encode($request->environmentProblem) : ''; 
        if ($request->myHouseId && $request->fm_member_id) {
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = hsEnvironmentProblem::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
          try {
           $envProblem  = !$isExistsHsNum['house_details_id'] ? new hsEnvironmentProblem : hsEnvironmentProblem::findOrfail($isExistsHsNum['id']);

           $envProblem->createdBy         = Auth::user()->id;
           $envProblem->municipilities_id = $ishouseExists['municipilities_id'];
           $envProblem->wards_id          = $ishouseExists['wards_id'];
           $envProblem->house_details_id  = $ishouseExists['id'];
           $envProblem->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
           
           $envProblem->environmentProblem      = $jsonEnvironmentProblem;
           $envProblem->otherEnvProblemName    = $request->otherEnvProblemName;
          
           $envProblem->status            = 0;
           $envProblem->softDelete        = 0;
           $mySave = !$isExistsHsNum['house_details_id'] ? $envProblem->save() : $envProblem->update();

           $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_environment_problems', $tblId = $envProblem->id);
           if ($mySave && $log) {
            DB::commit();
            return response()->json([
              'success' => true,
              'message' => config('activityMessage.saveMessage')
              ]);
            // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
            // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
          }else{
             return response()->json([
              'success' => false,
              'message' => config('activityMessage.unSaveMessage')
              ]);
           // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
       } catch (Exception $e) {
         DB::rollback();
         return back()->withMessage(config('activityMessage.dataNotInserted'));
       }
      }else{   
       return back()->withMessage(config('activityMessage.idNotFound'));
      }
      }else{    
       return back()->withMessage('House number is missing please try to insert it from browser');
      }
    }
}
