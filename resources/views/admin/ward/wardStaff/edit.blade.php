@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
   <div class="col-sm-12 col-md-12">
     <div class="card-box">
       <div class="card-head">
       	<header>
       		<i class="fa fa-edit">
       			@lang('commonField.extra.edit')
       			<h3>@lang('sidebarMenu.staff_setting.staff')</h3>
       		</i>
       	</header>
       </div>

       <div class="card-body">
            {!! Form::model($data, ['method' => 'POST','route' => ['wardStaff.update', $data->id], 'files'=>true, 'name' => 'frmWardStaff']) !!}
                 @include('admin.ward.wardStaff._form')

                 <p class="pull-right">
                 	   {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success pull-right']) }}
                 	  <button type="button" class="btn btn-danger pull-right" onclick="history.back()">
                 	      @lang('commonField.button.back')
                 	  </button>
                 </p>
            {{ Form::close() }}
       </div>
       </div>
     </div>
@endsection
@section('custom_script')
   @include('admin.ward.wardStaff.script')
@endsection