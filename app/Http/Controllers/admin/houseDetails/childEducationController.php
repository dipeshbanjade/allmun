<?php

namespace App\Http\Controllers\admin\houseDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\houseDetails\childrenEducation\ChildrenEducation;
use App\model\admin\houseDetails\childrenEducation\SchoolDistance;
use App\model\admin\houseDetails\childrenEducation\ChildMontessori;
use App\model\admin\houseDetails\childrenEducation\ChildClubInvolve;
use App\model\admin\houseDetails\childrenEducation\SchoolUnadmit;
use App\model\admin\houseDetails\childrenEducation\ChildHobby;
use App\model\admin\houseDetails\childrenEducation\EducationDrop;
use App\model\mylogic;
use Auth;
use DB;
use Validator;

class childEducationController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
     // insert house Toilet data 
    public function insertChildrenEducation(Request $request){  // add house kitchen
      // dd($request->all());
      $childrenJson = json_encode($request->childrenInfo);
      if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = ChildrenEducation::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
            DB::beginTransaction(); 
            try {
              $ce  = !$isExistsHsNum['house_details_id'] ? new ChildrenEducation : ChildrenEducation::findOrfail($isExistsHsNum['id']);   // get drinking water

              $ce->createdBy         = Auth::user()->id;
              $ce->municipilities_id = $ishouseExists['municipilities_id'];
              $ce->wards_id          = $ishouseExists['wards_id'];
              $ce->house_details_id  = $ishouseExists['id'];

              $ce->citizen_info_id   = $ishouseExists['citizen_infos_id'];

              $ce->haveChildEdu = $request->haveChildEdu;
              $ce->hsChildrenEducation = $childrenJson;

              $ce->status            = 0;
              $ce->softDelete        = 0;
              // $mySave = $ce->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $ce->save() : $ce->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'children_educations', $tblId = $ce->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                  ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
            }else{
              return response()->json([
                  'success' => false,
                  'message' => config('activityMessage.unSaveMessage')
                  ]);
             // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
          }else{   // house checking
             return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
           return back()->withMessage('House number is missing please try to insert it from browser');
       }
   }
    public function insertSchoolDistance(Request $request){  // add house kitchen
      // dd($request->all());

      $schoolDisJson = json_encode($request->disTime);
      // dd($schoolDisJson);
      if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = SchoolDistance::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
            DB::beginTransaction(); 
            try {
              $sd  = !$isExistsHsNum['house_details_id'] ? new SchoolDistance : SchoolDistance::findOrfail($isExistsHsNum['id']);   // get drinking water
              $sd->createdBy         = Auth::user()->id;
              $sd->municipilities_id = $ishouseExists['municipilities_id'];
              $sd->wards_id          = $ishouseExists['wards_id'];
              $sd->house_details_id  = $ishouseExists['id'];
              $sd->educationDistance = $schoolDisJson;
              $sd->status            = 0;
              $sd->softDelete        = 0;
              // $mySave = $ce->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $sd->save() : $sd->update();
              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'school_distances', $tblId = $sd->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                  ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
            }else{
              return response()->json([
                  'success' => false,
                  'message' => config('activityMessage.unSaveMessage')
                  ]);
             // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
          }else{   // house checking
             return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
           return back()->withMessage('House number is missing please try to insert it from browser');
       }
   } 


   public function insertChildMontessoris(Request $request){  

      if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = ChildMontessori::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
            DB::beginTransaction(); 
            try {
              $cm  = !$isExistsHsNum['house_details_id'] ? new ChildMontessori : ChildMontessori::findOrfail($isExistsHsNum['id']);   // get drinking water

              $cm->createdBy         = Auth::user()->id;
              $cm->municipilities_id = $ishouseExists['municipilities_id'];
              $cm->wards_id          = $ishouseExists['wards_id'];
              $cm->house_details_id  = $ishouseExists['id'];
              $cm->citizen_info_id   = $ishouseExists['citizen_infos_id'];

              $cm->maleChildNum = $request->maleChildNum;
              $cm->femaleChildNum = $request->femaleChildNum;            

              $cm->status            = 0;
              $cm->softDelete        = 0;
              // $mySave = $ce->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $cm->save() : $cm->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'child_montessoris', $tblId = $cm->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                  ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
            }else{
              return response()->json([
                  'success' => false,
                  'message' => config('activityMessage.unSaveMessage')
                  ]);
             // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
          }else{   // house checking
             return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
           return back()->withMessage('House number is missing please try to insert it from browser');
       }
   }    

   public function insertChildClubInvolve(Request $request){  
      $schoolRelated = json_encode($request->schoolRelated);
      $communityRelated = json_encode($request->communityRelated);
      $childClubRelated = json_encode($request->childClubRelated);

      if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = ChildClubInvolve::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
            DB::beginTransaction(); 
            try {
              $cci  = !$isExistsHsNum['house_details_id'] ? new ChildClubInvolve : ChildClubInvolve::findOrfail($isExistsHsNum['id']);   // get drinking water

              $cci->createdBy         = Auth::user()->id;
              $cci->municipilities_id = $ishouseExists['municipilities_id'];
              $cci->wards_id          = $ishouseExists['wards_id'];
              $cci->house_details_id  = $ishouseExists['id'];
              $cci->citizen_info_id   = $ishouseExists['citizen_infos_id'];

              $cci->haveChildInvolve = $request->haveChildInvolve;
              $cci->schoolRelated = $schoolRelated;
              $cci->communityRelated = $communityRelated;
              $cci->childClubRelated = $childClubRelated;

              $cci->status            = 0;
              $cci->softDelete        = 0;
              // $mySave = $ce->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $cci->save() : $cci->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'child_club_involves', $tblId = $cci->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                  ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
            }else{
              return response()->json([
                  'success' => false,
                  'message' => config('activityMessage.unSaveMessage')
                  ]);
             // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
          }else{   // house checking
             return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
           return back()->withMessage('House number is missing please try to insert it from browser');
       }
   }

   public function insertSchoolUnadmit(Request $request){        
      if ($request->myHouseId && $request->fm_member_id) {          
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = SchoolUnadmit::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
            DB::beginTransaction(); 
            try {
              $su  = !$isExistsHsNum['house_details_id'] ? new SchoolUnadmit : SchoolUnadmit::findOrfail($isExistsHsNum['id']);

              $su->createdBy         = Auth::user()->id;
              $su->municipilities_id = $ishouseExists['municipilities_id'];
              $su->wards_id          = $ishouseExists['wards_id'];
              $su->house_details_id  = $ishouseExists['id'];

              $su->citizen_info_id  = $ishouseExists['citizen_infos_id'];

              $su->primaryHealthyMale = $request->primaryHealthyMale;
              $su->primaryHealthyFemale = $request->primaryHealthyFemale;
              $su->primaryUnhealthyMale = $request->primaryUnhealthyMale;
              $su->primaryUnhealthyFemale = $request->primaryUnhealthyFemale;
              $su->secondaryHealthyMale = $request->secondaryHealthyMale;
              $su->secondaryHealthyFemale = $request->secondaryHealthyFemale;
              $su->secondaryUnhealthyMale = $request->secondaryUnhealthyMale;
              $su->secondaryUnhealthyFemale = $request->secondaryUnhealthyFemale;

              $su->status            = 0;
              $su->softDelete        = 0;
              // $mySave = $ce->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $su->save() : $su->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'school_unadmits', $tblId = $su->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                  ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
            }else{
              return response()->json([
                  'success' => false,
                  'message' => config('activityMessage.unSaveMessage')
                  ]);
             // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
          }else{   // house checking
             return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
           return back()->withMessage('House number is missing please try to insert it from browser');
       }
   }
   public function insertChildHobby(Request $request){        
      $childHobbyData = json_encode($request->childHobby);      
      if ($request->myHouseId && $request->fm_member_id) {          
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = ChildHobby::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
            DB::beginTransaction(); 
            try {
              $ch  = !$isExistsHsNum['house_details_id'] ? new ChildHobby : ChildHobby::findOrfail($isExistsHsNum['id']);

              $ch->createdBy         = Auth::user()->id;
              $ch->municipilities_id = $ishouseExists['municipilities_id'];
              $ch->wards_id          = $ishouseExists['wards_id'];
              $ch->house_details_id  = $ishouseExists['id'];

              $ch->citizen_info_id  = $ishouseExists['citizen_infos_id'];

              $ch->haveHobby = $request->haveHobby;
              $ch->childHobbyData = $childHobbyData;

              $ch->status            = 0;
              $ch->softDelete        = 0;
              // $mySave = $ce->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $ch->save() : $ch->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'child_hobbies', $tblId = $ch->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                  ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
            }else{
              return response()->json([
                  'success' => false,
                  'message' => config('activityMessage.unSaveMessage')
                  ]);
             // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
          }else{   // house checking
             return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
           return back()->withMessage('House number is missing please try to insert it from browser');
       }
   }
   public function insertEducationDrop(Request $request){        
      $trainingInfo = json_encode($request->trainingInfo);     
      if ($request->myHouseId && $request->fm_member_id) {          
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = EducationDrop::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
            DB::beginTransaction(); 
            try {
              $edud  = !$isExistsHsNum['house_details_id'] ? new EducationDrop : EducationDrop::findOrfail($isExistsHsNum['id']);

              $edud->createdBy         = Auth::user()->id;
              $edud->municipilities_id = $ishouseExists['municipilities_id'];
              $edud->wards_id          = $ishouseExists['wards_id'];
              $edud->house_details_id  = $ishouseExists['id'];

              $edud->citizen_info_id  = $ishouseExists['citizen_infos_id'];

              $edud->maleChildNum = $request->maleChildNum;
              $edud->maleReasonToLeave = $request->maleReasonToLeave;
              $edud->otherMReasonToLeave = $request->otherMReasonToLeave;
              $edud->femaleChildNum = $request->femaleChildNum;
              $edud->femaleReasonToLeave = $request->femaleReasonToLeave;
              $edud->otherFReasonToLeave = $request->otherFReasonToLeave;

              $edud->trainingInfo = $trainingInfo;

              $edud->status            = 0;
              $edud->softDelete        = 0;
              // $mySave = $ce->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $edud->save() : $edud->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'education_drops', $tblId = $edud->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                  'success' => true,
                  'message' => config('activityMessage.saveMessage')
                  ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
            }else{
              return response()->json([
                  'success' => false,
                  'message' => config('activityMessage.unSaveMessage')
                  ]);
             // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
          }else{   // house checking
             return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
           return back()->withMessage('House number is missing please try to insert it from browser');
       }
   }


}
