<?php

namespace App\Http\Controllers\admin\shifaris\business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\model\admin\ward\union;


use App\model\shifaris\ApplicantUser;
use App\model\shifaris\BusinessRegistrationCertificate;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\businessRegistrationVal;

use DB;
use Auth;

use App\model\mylogic;

class BusinessRegistrationController extends Controller
{
    protected $viewPath = 'admin.shifaris.business.businessRegistration';
    protected $shifarisTableName = 'business_registration_certificate';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixBusinessReg');

    }

    public function index()
    {
        $page['page_title']       = 'Business Registration Rate Form';
        $page['page_description'] = 'Business Registration Rate Form';

        
        $nirman = BusinessRegistrationCertificate::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        
    }
    public function create()
    {
        $page['page_title']       = 'Business Registration Rate Form : create';
        $page['page_description'] = 'Business Registration Rate Form details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
            // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(businessRegistrationVal $request)
    {
        //

        DB::beginTransaction();
        try {
            $businessRegCount = count($request->businessType);

            $businessType = $request->businessType;
            $businessNature = $request->businessNature;
            $wardNo = $request->wardNo;
            $registrationNo = $request->registrationNo;
            $tollName = $request->tollName;
            $streetName = $request->streetName;
            $houseNo = $request->houseNo;
            $businessRegArray = [];

            for ($i=0; $i < $businessRegCount; $i++) {
                array_push($businessRegArray, [
                    'businessType' => $businessType[$i],
                    'businessNature' => $businessNature[$i],
                    'wardNo' => $wardNo[$i],
                    'registrationNo' => $registrationNo[$i],
                    'tollName' => $tollName[$i],
                    'streetName' => $streetName[$i],
                    'houseNo' => $houseNo[$i]
                ]);
            }
            $businessRegJson = json_encode($businessRegArray);


            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);
            
            $isShifarisExist = isset($request->shifarisId) ? BusinessRegistrationCertificate::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? BusinessRegistrationCertificate::findOrfail($isShifarisExist['id']) : new BusinessRegistrationCertificate;

            $save->createdBy = Auth::user()->id;
            
            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";
            $save->issuedDate  = $request->issuedDate;            
            $save->registrationNumber  = $request->registrationNumber;            
            $save->registrationDate  = $request->registrationDate;            
            $save->chalaniNumber = $request->chalaniNumber;
            $save->businessFarmName = $request->businessFarmName;
            $save->businessOwnerName = $request->businessOwnerName;
            $save->houseOwnerName = $request->houseOwnerName;
            $save->businessLocation = $request->businessLocation;
            $save->businessStreetName = $request->businessStreetName;
            $save->businessHouseNumber = $request->businessHouseNumber;
            $save->businessEmail = $request->businessEmail;
            $save->businessPhoneNumber = $request->businessPhoneNumber;
            $save->businessPanNumber = $request->businessPanNumber;
            $save->organizationName = $request->organizationName;
            $save->organizationRegisterNumber = $request->organizationRegisterNumber;
            $save->organizationRegisterDate = $request->organizationRegisterDate;
            $save->courtName = $request->courtName;
            
            $save->tableData = $businessRegJson;

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;
            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
            $request->chalaniNum = $request->chalaniNumber;
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                ]);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
          DB::rollback();
          return back()->withMessage(config('activityMessage.dataNotInserted'));
      }

  }

  public function show($id)
  {
        //
  }

  public function edit($id)
  {
        //
  }

  public function update(Request $request, $id)
  {
        //
  }

  public function destroy($id)
  {
        //
  }

  public function changeStatus(){

  }
}
