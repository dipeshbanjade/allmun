.<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditFiscalYear']").validate({
            rules:{
                startDate : {
                    required : true,
                  date: true
                },
                endDate : {
                    required : true,
                  date: true
                }
            },
            messages: {
                startDate : "The startDate of fiscalYear is required",
                 endDate : "The endDate of fiscalYear is required"

            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmFiscalYear']").validate({
            rules:{

                startDate : {
                    required : true,
                  date: true
                },
                 endDate : {
                    required : true,
                  date: true
                }
            },
            messages: {
          
                startDate : "The startDate of fiscalYear is required",
                endDate : "The endDate of fiscalYear is required"

            }
        });
	var btnAddfiscalYear = $('.btnAddfiscalYear');
	btnAddfiscalYear.on('click', function(){
		$('#fiscalYearModal').modal('show');
	});

	var btnEditfisYear = $('.btnEditfisYear');
	btnEditfisYear.on('click', function(e){

		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				console.log(response);
				$('.fiscalId').val(response.id),
				$('.txtstartDate').val(response.startDate);
				$('.txtendDate').val(response.endDate);
				$('.txtstartShort').val(response.startShort);
				$('.txtendShort').val(response.endShort);
				$("#updateFiscalForm").modal('show');
			}
		})
	});

	var frmUpdateFiscalYear = $('#frmUpdateFiscalYear');
	frmUpdateFiscalYear.on('submit', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		var id   = $('.fiscalId').val();
		var url  = "{{URL::to('/')}}" + "/setting/fiscalYear/"+id+"/update";
		$.ajax({
			'type' : 'POST',
			'url'  : url,
			'data'    : data,
			success : function(response){
				console.log(response);
				if (response.success==true) {
					$('.infoDiv').append(response.message).addClass('alert alert-success');
				}
				if (response.success==false) {
					$.each(response.message, function(key, value){
						$('.infoDiv').append(value).addClass('alert alert-danger');
					})
				}
			},complete:function(){
				$("#updateFiscalForm").modal('hide');
				setTimeout(location.reload.bind(location), 1500);
			}
		})
		.fail(function (response) {
			alert('data already exists');
		});
		$('#updateFiscalForm').modal('hide');
	});
});
</script>