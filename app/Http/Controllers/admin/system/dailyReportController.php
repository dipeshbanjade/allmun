<?php

namespace App\Http\Controllers\admin\system;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\admin\municipility\MunStaffDailyReport;
use App\model\admin\ward\wardStaffDailyReport;

use App\Http\Requests\admin\dailyReport\dailyReportValidation;
use App\model\mylogic;

use DB;
use Auth;

class dailyReportController extends Controller
{
    protected $viewPath = 'admin.system.dailyReport';
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $page['page_title']       = 'Daily Report list';
        $page['page_description'] = 'Daily Report details';
        $authUsr  = Auth::user()->userLevel;

        if ($authUsr == 'dev') {
          return redirect()->route('home')->withMessage('Oops your are developer');
        }
        if ($authUsr == 'mun') {
            $dailyReport = MunStaffDailyReport::where(['status'=>0, 'softDelete'=> 0, 'staff_municipilities_id' => Auth::user()->staffs_id])->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));
        }

        if ($authUsr == 'wrd') {
            $dailyReport = wardStaffDailyReport::where(['status'=>0, 'softDelete'=> 0, 'wards_staff_id' => Auth::user()->staffs_id])->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));
        }
        return view($this->viewPath . '.index', compact(['page', 'dailyReport']));
    }
    /*admin view daily report*/
    public function adminView(){
      $page['page_title']       = 'AdminView Daily Report';
      $page['page_description'] = 'Monitor staff  daily report by admin';
      
      $authUsr          = Auth::user()->userLevel;
      if ($authUsr == 'mun') {
          $dailyReport = MunStaffDailyReport::where(['status'=>0, 'softDelete'=> 0])->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));
      }
      if ($authUsr == 'wrd') {
          $dailyReport = wardStaffDailyReport::where(['status'=>0, 'softDelete'=> 0, 'wards_id' => Auth::user()->wards_id])->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));
      }
      return view($this->viewPath . '.adminView', compact(['page', 'dailyReport']));
    }

    public function create()
    {
        $page['page_title']       = 'Create Daily Report';
        $page['page_description'] = 'Create new daily report';

        return view($this->viewPath . '.create', compact(['page']));
    }

    public function store(dailyReportValidation $request)
    {
        $tblDailyReport = Auth::user()->userLevel == 'mun' ? 'mun_staff_daily_reports' : 'w_r_d_staff_daily_reports';
        $authUsr          = Auth::user()->userLevel;
        DB::beginTransaction();
        try {
        $save  = $authUsr == 'mun' ? new MunStaffDailyReport : new wardStaffDailyReport;
        $save->sub        = $request->sub;
        $save->date       = $request->date;
        $save->startTime  = $request->startTime;
        $save->endTime    = $request->endTime;
        $save->desc       = $request->desc;
        $save->status     = 0;
        $save->softDelete = 0;
        if ($authUsr ==='mun') {
          $save->staff_municipilities_id = Auth::user()->staffs_id;
        }
        if ($authUsr ==='wrd') {
          $save->wards_staff_id = Auth::user()->staffs_id;
          $save->wards_id       = Auth::user()->wards_id;
        }
        $mySave = $save->save();

         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createDailyReport') . $request->sub, $tableName = $tblDailyReport, $tblId = $save->id);

         if ($mySave && $log) {
             DB::commit();
             return redirect()->route('dailyReport.index')->withMessage(config('activityMessage.saveMessage') .$request->refCode);
             }else{
                return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
        
    }

    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $page['page_title']       = 'Edit daily report';
        $page['page_description'] = 'edit my daily reports';
        $authUsr          = Auth::user()->userLevel;
        if ($authUsr == 'mun') {
            $data = MunStaffDailyReport::findOrFail($id);
        }
        if ($authUsr == 'wrd') {
            $data = wardStaffDailyReport::findOrFail($id);
        }
        return view($this->viewPath . '.edit', compact(['page', 'data']));
    }

    public function update(dailyReportValidation $request, $id)
    {
      $authUsr          = Auth::user()->userLevel;
      $tblDailyReport   = Auth::user()->userLevel == 'mun' ? 'mun_staff_daily_reports' : 'w_r_d_staff_daily_reports';

      DB::beginTransaction();
      try {
        switch ($authUsr) {
          case 'mun':
            $update = MunStaffDailyReport::findOrFail($id);
            $update->sub        = $request->sub;
            $update->date       = $request->date;
            $update->startTime  = $request->startTime;
            $update->endTime    = $request->endTime;
            $update->desc       = $request->desc;
            $update->status     = 0;
            $update->softDelete = 0;
            $update->save();
            break;

          case 'wrd':
            $update = wardStaffDailyReport::findOrFail($id);
            $update->sub        = $request->sub;
            $update->date       = $request->date;
            $update->startTime  = $request->startTime;
            $update->endTime    = $request->endTime;
            $update->desc       = $request->desc;
            $update->status     = 0;
            $update->softDelete = 0;
            $update->save();
            break;
          
          default:
            return back()->withMessage(config('activityMessage.idNotFound'));
            break;
        }

       $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createDailyReport') . $request->sub, $tableName = $tblDailyReport, $tblId = $update->id);

       if ($log) {
           DB::commit();
           return redirect()->route('dailyReport.index')->withMessage(config('activityMessage.updateMessage'));
           }else{
              return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
      }catch (Exception $e) {
          DB::rollback();
          return back()->withMessage(config('activityMessage.dataNotInserted'));
      }

    }
    public function destroy($id)
    {
      if ($id) {
        $authUsr          = Auth::user()->userLevel;
        $tblDailyReport   = Auth::user()->userLevel == 'mun' ? 'mun_staff_daily_reports' : 'w_r_d_staff_daily_reports';
         DB::beginTransaction();
        try {
          switch ($authUsr) {
            case 'mun':
              $del = MunStaffDailyReport::findOrFail($id);
              $del->delete();
              break;
            case 'wrd':
              $del = wardStaffDailyReport::findOrFail($id);
              $del->delete();
              break;

            case 'dev':
              return back()->withMessage('Oops your are developer');
              break;
            
            default:
              return back()->withMessage(config('activityMessage.idNotFound'));
              break;
          }
          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createDailyReport') . $id, $tableName = $tblDailyReport, $tblId = $id);

          if ($log) {
              DB::commit();
              return redirect()->route('dailyReport.index')->withMessage(config('activityMessage.deleteMessage'));
              }else{
                 return back()->withMessage(config('activityMessage.idNotFound'));
             }

        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
      }else{
        return back()->withMessage(config('activityMessage.idNotFound'));
      }
    }
}
