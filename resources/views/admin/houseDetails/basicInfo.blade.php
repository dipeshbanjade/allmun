<div class="card-body row">
        <div class="col-lg-6 p-t-20"> 
         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
          {{ Form::text('refCode', isset($data) ? $data->refCode : $refCode, ['class'=>'mdl-textfield__input txtHouseNumber', 'placeholder'=>'House Number']) }}
          <label class = "mdl-textfield__label">@lang('commonField.extra.houseNum')</label>
        </div>
      </div>
      <!--  -->

       <div class="col-lg-6 p-t-20"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('houseNumber', isset($data) ? $data->houseNumber : null, ['class'=>'mdl-textfield__input txtHouseNumber', 'placeholder'=>'House Number']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.houseNum')</label>
       </div>
     </div>
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('MunHouseNumber', isset($data) ? $data->MunHouseNumber : 00, ['class'=>'mdl-textfield__input txtMunHouseNumber', 'placeholder'=>'muncipality House number']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.munHouseNumber')
         </label>
       </div>
     </div>
     <!-- <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('familyHead', isset($data) ? $data->familyHead : null, ['class'=>'mdl-textfield__input txtfamilyHead', 'placeholder'=>'Family Head']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.familyHead')</label>
       </div>
     </div> -->
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('aabadhatolBikasSansthakoNaam', isset($data) ? $data->aabadhatolBikasSansthakoNaam : null, ['class'=>'mdl-textfield__input txtaabadhatolBikasSansthakoNaam', 'placeholder'=>'']) }}
         <label class = "mdl-textfield__label">@lang('commonField.address_information.aabadhatolBikasSansthakoNaam')</label>
       </div>
     </div>

     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
         <select name="house_types_id" class="form-control">
           <option value="">@lang('commonField.extra.houseType') </option>  
           @if($houseTypeId)
               @foreach($houseTypeId as $houseType)
                   <option value="{{ $houseType->id }}" {{ isset($data) && $data->house_types_id  == $houseType->id ? 'selected' : '' }}>
                        {{ $houseType->houseNep }} /  {{ $houseType->houseEng }}
                   </option>
               @endforeach
           @endif          
         </select>
       </div>
     </div>  
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
         <select name="provinces_id" class="form-control">
           <option value="">@lang('commonField.extra.provinceId')</option>
           @if($provincesId)
                @foreach($provincesId as $provision)
                    <option value="{{ $provision->id }}" {{ isset($data) && $data->provinces_id  == $provision->id ? 'selected' : '' }}>
                         {{ $provision->name }}
                    </option>
                @endforeach
           @endif            
         </select>
       </div>
     </div>
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">


         <select name="districts_id" class="form-control">
           <option value="">@lang('commonField.extra.districts_id')</option> 
           @if($districtId)
              @foreach($districtId as $district)
                 <option value="{{ $district->id }}" {{ isset($data) && $data->districts_id  == $district->id ? 'selected' : '' }}>
                     {{ $district->districtNameNep  }} / {{ $district->districtNameEng  }}
                 </option>
              @endforeach
           @endif           
         </select>
       </div>
     </div> 
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('address', isset($data) ? $data->address : null, ['class'=>'mdl-textfield__input txtaddress', 'placeholder'=>'Address']) }}
         <label class = "mdl-textfield__label">@lang('commonField.address_information.gautolBastiKoNaam')</label>
       </div>
     </div>  
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
         <select name="floors_id" class="form-control">
           <option value="">@lang('commonField.extra.floor') </option> 
              @if($floorId)
                   @foreach($floorId as $floor)
                     <option value="{{ $floor->id }}" {{ isset($data) && $data->floors_id  == $floor->id ? 'selected' : ''}}>
                          {{ $floor->nameNep }} / {{ $floor->nameEng }}
                     </option>
                   @endforeach
              @endif
         </select>
       </div>
     </div>
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('familyHeadPhoneNumber', isset($data) ? $data->familyHeadPhoneNumber : null, ['class'=>'mdl-textfield__input txtfamilyHeadPhoneNumber', 'placeholder'=>'Family head phone number']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.familyHeadPhoneNumber')</label>
       </div>
     </div>   
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('noOfRoom', isset($data) ? $data->noOfRoom : null, ['class'=>'mdl-textfield__input txtnoOfRoom', 'placeholder'=>'Number of Romm']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.noOfRooms')</label>
       </div>
     </div>    
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('roomUsageFor', isset($data) ? $data->roomUsageFor : null, ['class'=>'mdl-textfield__input txtroomUsageFor', 'placeholder'=>'Room uses for']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.roomUsageFor')</label>
       </div>
     </div>      
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('houseAge', isset($data) ? $data->houseAge : null, ['class'=>'mdl-textfield__input txthouseAge', 'placeholder'=>'House Age']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.houseAge')</label>
       </div>
     </div>   
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('wardNo', isset($data) ? $data->wardNo : null, ['class'=>'mdl-textfield__input txtwardNo', 'placeholder'=>'Ward number']) }}
         <label class = "mdl-textfield__label">@lang('commonField.address_information.wardNo')</label>
       </div>
     </div>   
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('wardKhanda', isset($data) ? $data->wardKhanda : null, ['class'=>'mdl-textfield__input txtwardKhanda', 'placeholder'=>'Ward khanda']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.wardKhanda')</label>
       </div>
     </div> 
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('streetName', isset($data) ? $data->streetName : null, ['class'=>'mdl-textfield__input txtstreetName', 'placeholder'=>'street name']) }}
         <label class = "mdl-textfield__label">@lang('commonField.address_information.streetName')</label>
       </div>
     </div>     
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('localAreaName', isset($data) ? $data->localAreaName : null, ['class'=>'mdl-textfield__input txtlocalAreaName', 'placeholder'=>'local area name']) }}
         <label class = "mdl-textfield__label">@lang('commonField.address_information.localAreaName')</label>
       </div>
     </div>       
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('familyMember', isset($data) ? $data->familyMember : null, ['class'=>'mdl-textfield__input txtfamilyMember', 'placeholder'=>'family member']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.familyMember')</label>
       </div>
     </div>   
     {!! Form::hidden('citizen_infos_id', $citizenId['id']) !!}    
      <!-- TO Do  gender -->
    <!--  <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         <label class = "mdl-textfield__label">@lang('commonField.extra.familyHeadGender')</label><br>
         <label>
            @lang('commonField.personal_information.male')
            {{ Form::radio('ishouseOwnByHead', 'M', isset($data) && $data->yes ? true : true) }}
         </label>
         <label>
            @lang('commonField.personal_information.female')
            {{ Form::radio('ishouseOwnByHead', 'F', isset($data) && $data->no ? true : '') }}
         </label>
         <label>
            @lang('commonField.personal_information.others')
            {{ Form::radio('ishouseOwnByHead', 'O', isset($data) && $data->no ? true : '') }}
         </label>
       </div>
     </div>   -->

     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('memberLiving', isset($data) ? $data->memberLiving : null, ['class'=>'mdl-textfield__input txtmemberLiving', 'placeholder'=>'']) }}
         <label class = "mdl-textfield__label">@lang('commonField.extra.memberLiving')</label>
       </div>
     </div>    

     <!-- To Do -->  
     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         <!-- {{ Form::text('ishouseOwnByHead', null, ['class'=>'mdl-textfield__input txtishouseOwnByHead', 'placeholder'=>'']) }} -->
         <label class = "mdl-textfield__label">@lang('commonField.extra.ishouseOwnByHead')</label><br>

         <label>
            @lang('commonField.extra.yesM')
            {{ Form::radio('ishouseOwnByHead', 'yes', isset($data) && $data->yes ? true : true) }}
         </label>
         <!--  -->
         <label>
            @lang('commonField.extra.noM')
            {{ Form::radio('ishouseOwnByHead', 'no', isset($data) && $data->no ? true : '') }}
         </label>

       </div>
     </div> 

     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('longitude', isset($data) ? $data->longitude : null, ['class'=>'mdl-textfield__input txtlongitude', 'placeholder'=>'longitude']) }}
         <label class = "mdl-textfield__label">@lang('commonField.distance.longitude')</label>
       </div>
     </div>

     <div class="col-lg-6 p-t-20"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
         {{ Form::text('latitude', isset($data) ? $data->latitude : null, ['class'=>'mdl-textfield__input txtlatitude', 'placeholder'=>'latitude']) }}
         <label class = "mdl-textfield__label">@lang('commonField.distance.latitude')</label>
       </div>
     </div> 

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
             <label> @lang('commonField.extra.house')</label>
             <div class="form-group imgHouseDetails">
               <p class="pull-right">
                   <label>
                    <br>
                    <img src="{{ isset($data->houseImage) ? asset($data->houseImage) : '' }}" width="120" height="80" class="img img-thumbnai" id="imgHouseDetails"><br>
                     <input name="houseImage" type='file' onchange="displayImage(this, 'imgHouseDetails');" title="select citizenship" />
                   </label>
               </p>
             </div>
         </div>
     </div>

     <p class="p-4">
         <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> <span>@lang('commonField.button.back')</span> 
         </button>
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
     </p>
</div>