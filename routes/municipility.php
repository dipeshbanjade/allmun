<?php
    Route::prefix('municipility')->group(function(){     	
      Route::get('/mun-set',             'admin\municipility\municipilityDetailsController@index')->name('admin.mun-set.index');
      Route::get('/mun-set/create', 'admin\municipility\municipilityDetailsController@create')->name('admin.mun-set.create');
      Route::post('/mun-set/store',      'admin\municipility\municipilityDetailsController@store')->name('admin.mun-set.store');
      Route::get('/mun-set/{id}/edit',   'admin\municipility\municipilityDetailsController@edit')->name('admin.mun-set.edit');
      Route::post('mun-set/{id}/update', 'admin\municipility\municipilityDetailsController@update')->name('admin.mun-set.update');
      
      // Route::get('/mun-set/{id}',        'admin\municipility\municipilityDetailsController@destroy')->name('admin.mun-set.delete');
      /*municipility User*/
      Route::get('/munStaff',             'admin\municipility\staffMunicipilityController@index')->name('admin.munStaff.index');
      Route::get('/munStaff/create', 'admin\municipility\staffMunicipilityController@create')->name('admin.munStaff.create');
      Route::post('/munStaff/store',      'admin\municipility\staffMunicipilityController@store')->name('admin.munStaff.store');

      Route::get('/munStaff/{slug}/edit',   'admin\municipility\staffMunicipilityController@edit')->name('admin.munStaff.edit');

      Route::get('/munStaff/{slug}',        'admin\municipility\staffMunicipilityController@show')->name('admin.munStaff.show');
      Route::post('munStaff/{id}/update', 'admin\municipility\staffMunicipilityController@update')->name('admin.munStaff.update');
      Route::get('/munStaffDel/{id}',        'admin\municipility\staffMunicipilityController@destroy')->name('admin.munStaff.delete');
      Route::get('munStaffChangeStatus', 'admin\municipility\staffMunicipilityController@changeStatus')->name('admin.munStaff.changeStatus');
        /*attendance*/
      /*municipility staff detaqils*/
      
      /*leave*/
      Route::get('/munStaffLeave',             'admin\municipility\munStaffLeaveController@index')->name('myLeaveProfile');

      Route::get('/adminLeaveView', 'admin\municipility\munStaffLeaveController@adminViewLeaveDetails')->name('adminViewLeave');
      
      Route::get('/munStaffLeave/create', 'admin\municipility\munStaffLeaveController@create')->name('admin.munStaffLeave.create');
      Route::post('/munStaffLeave/store',      'admin\municipility\munStaffLeaveController@store')->name('admin.munStaffLeave.store');
      Route::get('/munStaffLeave/{id}/edit',   'admin\municipility\munStaffLeaveController@edit')->name('admin.munStaffLeave.edit');
      Route::get('/munStaffLeave/{id}',        'admin\municipility\munStaffLeaveController@show')->name('admin.munStaffLeave.show');
      Route::post('/munStaffLeave/{id}/update', 'admin\municipility\munStaffLeaveController@update')->name('admin.munStaffLeave.update');


      Route::get('/munStaffLeaveDelete/{id}',        'admin\municipility\munStaffLeaveController@destroy')->name('admin.munStaffLeave.delete');

      Route::get('munStaffChangeStatus', 'admin\municipility\munStaffLeaveController@changeStatus')->name('admin.munStaffLeave.changeStatus');


      /*create ward and its admin*/
      Route::get('viewWardDetails/{slug}', 'admin\ward\wardDetailsController@getAllWardDetails')->name('viewWrdDetails');
      Route::get('/wardDetails',             'admin\ward\wardDetailsController@index')->name('wardDetails.index');
      Route::get('/wardDetails/create',      'admin\ward\wardDetailsController@create')->name('wardDetails.create');
      Route::post('/wardDetails/store',      'admin\ward\wardDetailsController@store')->name('wardDetails.store');
      Route::get('/wardDetails/{slug}/edit',   'admin\ward\wardDetailsController@edit')->name('wardDetails.edit');
      Route::post('/wardDetails/{id}/update', 'admin\ward\wardDetailsController@update')->name('wardDetails.update');
      Route::get('/wardDelete/{id}',        'admin\ward\wardDetailsController@destroy')->name('wardDetails.delete');
      Route::get('/wardDetailsChangeStatus',     'admin\ward\wardDetailsController@changeStatus')->name('wardDetails.changeStatus');
      /*ward admin*/
      Route::get('/wardAdmin', 'admin\ward\wardAdminController@index')->name('wardAdmin');
      Route::get('/createWardAdmin', 'admin\ward\wardAdminController@create')->name('createWardAdmin');
      Route::post('/storeWardAdmin', 'admin\ward\wardAdminController@store')->name('storeWardAdmin');

      Route::get('/editWardAdmin/{slug}', 'admin\ward\wardAdminController@edit')->name('editWardAdmin');
      Route::post('/updateWardAdmin/{id}/update', 'admin\ward\wardAdminController@update')->name('UpdateWardAdmin');
      Route::get('/deleteWardAdmin/{$id}', 'admin\ward\wardAdminController@delete')->name('deleteWardAdmin');


      /*---------------------daily report---------------------------------------*/
      Route::get('/munStaffDailyReport',             'admin\municipility\munStaffDailyReportController@indexStaff')->name('munStaffDailyReport');

       Route::get('/munStaffDailyReportAdmin',             'admin\municipility\munStaffDailyReportController@adminViewDailyReport')->name('adminViewDailyReport');

       Route::get('/munStaffDailyReport/create', 'admin\municipility\munStaffDailyReportController@create')->name('munStaffDailyReport.create');

       Route::post('/munStaffDailyReport/store',      'admin\municipility\munStaffDailyReportController@store')->name('munStaffDailyReport.store');

       Route::get('/munStaffDailyReport/{id}/edit',   'admin\municipility\munStaffDailyReportController@edit')->name('munStaffDailyReport.edit');

       Route::post('/munStaffDailyReport/{id}/update', 'admin\municipility\munStaffDailyReportController@update')->name('munStaffDailyReport.update');
       Route::get('/munStaffDailyReportDelete/{id}',        'admin\municipility\munStaffDailyReportController@destroy')->name('munStaffDailyReport.delete');
    });

?>