<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
        <div class="form-group col-md-12">{{-- 12 --}}
            <p>
            <strong>तपाईको परिवारका महिलाहरुमा निम्न रोगहरुको समस्या रहेको छ ?</strong>
            </p>
             <?php   
                // data come from controller to display added data in form
                $updateFemaleDis = isset($hsfemaleDisease->hsDiseaseDetails) ? json_decode($hsfemaleDisease->hsDiseaseDetails) : '';
             ?>
            <table border="1">
                <thead>
                    <tr>
                        <th>@lang('commonField.extra.sn')</th>
                        <th>रोगहर छ</th>
                        <th>महिलाको नाम</th>
                        <th>रोगको नाम</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $count = 1;
                        $upDateFDisease = moreHouseDetailsFormUpdate($compareKey = $hsDetails->citizen_infos->id, $jsonData = $updateFemaleDis, $compareJsonKey = 'femaleId');

                    ?>
                    @if($hsDetails->citizen_infos->gender == 'female')
                    <tr>
                         <td>1.</td>
                         <td>
                             <input type="checkbox" class="haveDisease"  name="hsFemaleDisease[{{ 1 }}][haveDisease]" value="1"
                             {{ $upDateFDisease['haveDisease'] ==1 ? 'checked' : '' }}>
                         </td>
                         <td>
                          <?php
                                 $gharmuliName = $lan =='nep' ? $hsDetails->citizen_infos->fnameNep .''. $hsDetails->citizen_infos->mnameNep .' '.$hsDetails->citizen_infos->lnameNep :  $hsDetails->citizen_infos->fnameEng .''. $hsDetails->citizen_infos->mnameEng .' '.$hsDetails->citizen_infos->lnameEng
                            ?>
                            {{ Form::text("hsFemaleDisease[$count][fmFemaleNam]", isset($gharmuliName) ? $gharmuliName : '', ['class'=>'mdl-textfield__input dashed-input-field', 'readonly']) }}
                            <input type="hidden" name="hsFemaleDisease[{{ $count }}][femaleId]" value="{{ $hsDetails->citizen_infos->id }}">
                         </td>
                         <td>
                           @if($allDisease)
                              @foreach($allDisease as $disease)
                               <label>
                                   <input type="checkbox" name="hsFemaleDisease[{{ 1  }}][diseaseName][]" value="{{ $disease->nameNep }}" <?php echo isset($upDateFDisease['diseaseName']) && in_array($disease->nameNep, $upDateFDisease['diseaseName']) ? 'checked' : ''    ?>>
                                   {{ $disease->nameNep }}
                               </label>
                              @endforeach
                           @endif
                         </td>
                    </tr>
                    @endif
                    @foreach($getMyFamilyDetails as $femaleDisease)
                    <?php
                      $fmFemaleDetail = getParentsName($femaleDisease->relation_id); 
                    ?>
                     @if($fmFemaleDetail->gender == 'female')
                     <?php
                        $count ++;
                        $fmFemaleDetailNam =  $lan =='nep' ? $fmFemaleDetail->fnameNep .''. $fmFemaleDetail->mnameNep .' '.$fmFemaleDetail->lnameNep :  $fmFemaleDetail->fnameEng .''. $fmFemaleDetail->mnameEng .' '.$fmFemaleDetail->lnameEng; 

                        $upDateFDisease = moreHouseDetailsFormUpdate($compareKey = $fmFemaleDetail->id, $jsonData = $updateFemaleDis, $compareJsonKey = 'femaleId');

                      ?>
                    <tr>
                        <td>{{ $count }}.</td>
                        <td>
                            <input type="checkbox" class="haveDisease"  name="hsFemaleDisease[{{ $count }}][haveDisease]" value="1"
                            {{ $upDateFDisease['haveDisease'] ==1 ? 'checked' : '' }}>
                        </td>
                        <td>
                            {{ Form::text("hsFemaleDisease[$count][fmFemaleNam]", isset($fmFemaleDetailNam) ? $fmFemaleDetailNam : '', ['class'=>'mdl-textfield__input dashed-input-field', 'readonly']) }}
                            <input type="hidden" name="hsFemaleDisease[{{ $count }}][femaleId]" value="{{ $fmFemaleDetail->id }}">
                        </td>
                        <td>
                           @if($allDisease)
                              @foreach($allDisease as $disease)
                               <label>
                                   <input type="checkbox" name="hsFemaleDisease[{{ $count  }}][diseaseName][]" value="{{ $disease->nameNep }}" <?php echo isset($upDateFDisease['diseaseName']) && in_array($disease->nameNep, $upDateFDisease['diseaseName']) ? 'checked' : ''    ?>>
                                   {{ $disease->nameNep }}
                               </label>
                              @endforeach
                           @endif
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="msgDisplay"></div>
  </div>
</div>