<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="@yield('page_description')" />
    <meta name="author" content="Malika incorporate" />
    <title>@yield('page_title')</title>
    @include('main.css')
    @yield('custom_css')
    <link rel="shortcut icon" href="{{ asset('lib/img/npicon.png') }}" /> 
 </head>
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-indigo">
    <div class="page-wrapper">
<!-- header  -->
<?php
    use App\model\admin\acl\Role;
    $roleUsr = Auth::user()->roles_id;
    $role    = Role::findOrFail($roleUsr);
    $permission = $role->permissions;
    $myPermission = array();
    foreach ($permission as $per) {
        array_push($myPermission, $per->routeName);
    }
    // dd($myPermission); 
?>
 @include('main.header')
 @yield('modelSection')
<!-- header -->
<div class="page-container">
        <!-- sidebar -->
     @include('main.sidebar')
    <!-- sidebar -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- @include('main.breadcrum') -->
				  	<div class="state-overview">
						<div class="row" >
                         <div class="clearfix"></div>
                         @if(Auth::user()->isPasswordChange == 0)
                             <div class="infoDiv col-sm-12 col-md-12" style="z-index: 999 !important;">
                                   <p class="alert alert-warning fashMessage" style="z-index: 99999 !important;">
                                     @lang('commonField.extra.changeYourPassword')
                                     <a class="close fontColorBlack" data-dismiss="alert" href="#">
                                         ×
                                     </a>
                                     <span class="pull-right"><a href="{{route('changePassword') }}">Change Password</a></span>
                                   </p>
                                </div>
                         @endif

                         <div class="infoDiv col-sm-12 col-md-12" style="z-index: 999 !important;">
                          @include('main.errorLog')
                            @if(Session::has('message'))
                               <p class="alert alert-success fashMessage" style="z-index: 99999 !important;">
                                 <a class="close fontColorBlack" data-dismiss="alert" href="#">
                                     ×
                                 </a>
                                 {{session::get('message')}}
                               </p>
                            @endif
                            
                            </div>
						            @yield('content')
						       </div>
						</div>
        </div>
    </div>
</div>
<!-- end page container -->
<!-- start footer -->
<!-- <div class="page-footer"> -->
   <!-- @include('main.footer') -->
 <!-- </div> -->
<!-- end footer -->
</div>
<!-- start js include path -->
@include('main.script')
@yield('custom_script')
<!-- end js include path -->
  </body>
</html>