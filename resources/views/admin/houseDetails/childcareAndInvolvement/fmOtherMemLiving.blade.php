<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १. तपाईको परिवारमा अरुको घरको वा आफन्त व्यक्ति बसोबास गरेका छन् ? 
                    </p>
                    <div>
                        <label class="radio-inline">
                            १. छन् 
                            <input type="radio" onchange="displayOhterMemTbl(this)" name="haveOtherLiving" value="1"  {{ isset($hsOML) && $hsOML->haveOtherLiving == 1 ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैनन्
                            <input type="radio" onchange="displayOhterMemTbl(this)" name="haveOtherLiving" value="0" {{ isset($hsOML) && $hsOML->haveOtherLiving == 0 ? 'checked' : '' }}>
                            यदि छन् भने, किन बसोबास गरेका छन् ?    
                        </label>
                    </div>
                </div>
                <br>
                @if (isset($hsOML) && $hsOML->haveOtherLiving == 1 )
                    <table class="table otherMemLivingTbl" style="display: block">
                @else
                   <table class="table otherMemLivingTbl"  style="display: none;">
                @endif
             
                    <tr>
                        <th rowspan="2">सि.न.</th>
                        <th rowspan="2">व्यक्तिको नाम</th>
                        <th rowspan="2">उमेर</th>
                        <th colspan="6">बसोबास गर्नुको प्रमुख कारण</th>
                    </tr>
                    <tr>
                        <td>१) संरक्षक नभएकोले</td>
                        <td>२) काममा सहयोग गर्न </td>
                        <td>३) ज्याला पाउने गरी काम गर्न</td>
                        <td>४) पढ्न</td>
                        <td>५) अन्य</td>                        
                    </tr>
                    <tbody id="otherMemTb1">
                        <?php 
                        $count = 0;
                        $otherMemLivingDetail = isset($hsOML->otherLivingDetailOne) ? json_decode($hsOML->otherLivingDetailOne) : ['0'];
                        //dd((array) $otherMemLivingDetail);
                        //dd($otherMemLivingDetail);
                        //dd($otherMemLivingDetail);
                        //Note $getMyFamilyDetails from houseDetailsController
                        
                        foreach($otherMemLivingDetail as $member){
                            $memberArr = (array) $member;
                            //dd($memberArr);
                            $count++;

                            //dd($memberInfo);
                            //dd($singleChildHealthCare);
                            ?>
                            <tr>
                                <td>
                                    {{ $count }}
                                </td>
                                <td>
                                    <?php 
                                        // var_dump((array) $member);
                                    ?>
                                    {{ Form::text("otherLivingDetailOne[$count][name]", isset($memberArr['name']) ? $memberArr['name']: '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}

                                </td>
                                <td>
                                    {{ Form::text("otherLivingDetailOne[$count][age]",  isset($memberArr['age']) ? $memberArr['age']: '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                                </td>
                                <td>
                                   <!--  {{ Form::text("otherLivingDetailOne[$count][noSafety]",  isset($memberArr['noSafety']) ? $memberArr['noSafety']: '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }} -->
                                    <input type="checkbox" name="otherLivingDetailOne[<?php echo $count; ?>][noSafety]" class="mdl-textfield__input dashed-input-field" value="1" {{ isset($memberArr['noSafety']) && $memberArr['noSafety'] == '1' ? 'checked' : '' }}>
                                </td>
                                <td>
                                   <!--  {{ Form::text("otherLivingDetailOne[$count][workHelp]",  isset($memberArr['workHelp']) ? $memberArr['workHelp']: '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }} -->

                                   <input type="checkbox" name="otherLivingDetailOne[<?php echo $count; ?>][workHelp]" class="mdl-textfield__input dashed-input-field" value="1" {{ isset($memberArr['workHelp']) && $memberArr['workHelp'] == '1' ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <!-- {{ Form::text("otherLivingDetailOne[$count][employment]",  isset($memberArr['employment']) ? $memberArr['employment']: '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }} -->

                                    <input type="checkbox" name="otherLivingDetailOne[<?php echo $count; ?>][employment]" class="mdl-textfield__input dashed-input-field" value="1" {{ isset($memberArr['employment']) && $memberArr['employment'] == '1' ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <!-- {{ Form::text("otherLivingDetailOne[$count][study]",  isset($memberArr['study']) ? $memberArr['study']: '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }} -->

                                    <input type="checkbox" name="otherLivingDetailOne[<?php echo $count; ?>][study]" class="mdl-textfield__input dashed-input-field" value="1" {{ isset($memberArr['study']) && $memberArr['study'] == '1' ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <!-- {{ Form::text("otherLivingDetailOne[$count][others]",  isset($memberArr['others']) ? $memberArr['others']: '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }} -->

                                    <input type="checkbox" name="otherLivingDetailOne[<?php echo $count; ?>][others]" class="mdl-textfield__input dashed-input-field" value="1" {{ isset($memberArr['others']) && $memberArr['others'] == '1' ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <?php 
                                    if($count == 1){
                                        ?>
                                        <div id="addNewRowInTableOne" class="btn btn-success" style="padding:2px;">
                                            <i class="fa fa-plus"></i>
                                        </div>
                                        <?php }else{ ?>
                                        <div class="btn btn-danger btnRemoveItem" style="padding:2px;">
                                            <i class="fa fa-minus"></i>
                                        </div>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <div class="form-group col-md-12">
                        <div>
                            <p>
                                २. यदि १८ वर्ष भन्दा कम उमेरका काममा सहयोग गर्न र काम गर्न बसेका भए, निम्न विवरण भर्नुहोस ।
                            </p>
                            <div>
                                <label class="radio-inline">
                                    १. छन् 
                                    <input type="radio" onchange="displayWorkingTbl(this)" name="haveOtherChildLiving" value="1"  {{ isset($hsOML) && $hsOML->haveOtherChildLiving == 1 ? 'checked' : '' }}>
                                </label> 
                                
                                <label class="radio-inline">
                                    २. छैनन्
                                    <input type="radio" onchange="displayWorkingTbl(this)" name="haveOtherChildLiving" value="0" {{ isset($hsOML) && $hsOML->haveOtherChildLiving == 0 ? 'checked' : '' }} checked="">  
                                </label>
                            </div>
                        </div>
                        <br>
                            <table class="table workingTbl">    
                            <tr>
                                <th>सि.न.</th>
                                <th>बालबालिकाको नाम</th>
                                <th>उमेर</th>
                                <th>काम शुरु गरेको मिति</th>
                                <th>दैनिक / महिनावाारी</th>
                                <th>कामको प्रकार</th>
                                <th>दैनिक कार्य घण्टा </th>
                                <th> गत ३० दिनमा पाएको पारिश्रमिक रु. (सुविधा समेत)</th>
                            </tr>
                            <tbody id="otherMemTb2">
                                <?php 
                                $count = 0;
                                $otherMemLivingDetailTwo = isset($hsOML->otherLivingDetailTwo) ? json_decode($hsOML->otherLivingDetailTwo) : ['0'];
                        //dd($otherMemLivingDetail);
                        //Note $getMyFamilyDetails from houseDetailsController                                
                                foreach($otherMemLivingDetailTwo as $member){
                                    $memberArr = (array) $member;
                                    $count++;
                            //dd($memberInfo);
                            //dd($singleChildHealthCare);
                                    ?>                        
                                    <tr>
                                        <td>
                                            {{ $count }}
                                        </td>
                                        <td>
                                            {{ Form::text("otherLivingDetailTwo[$count][childName]", isset($memberArr['childName']) ? $memberArr['childName'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Child Name', 'id' => '']) }}
                                        </td>
                                        <td>
                                            {{ Form::text("otherLivingDetailTwo[$count][childAge]",  isset($memberArr['childAge']) ? $memberArr['childAge'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'ChildAge', 'id' => '']) }}
                                        </td>
                                        <td>
                                            {{ Form::date("otherLivingDetailTwo[$count][startDate]",  isset($memberArr['startDate']) ? $memberArr['startDate'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Start date', 'id' => '']) }}
                                        </td>
                                        <td>
                                            {{ Form::text("otherLivingDetailTwo[$count][salaryBased]",  isset($memberArr['salaryBased']) ? $memberArr['salaryBased'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Salary Based', 'id' => '']) }}
                                        </td>
                                        <td>
                                            {{ Form::text("otherLivingDetailTwo[$count][workType]",  isset($memberArr['workType']) ? $memberArr['workType'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'WorkType', 'id' => '']) }}
                                        </td>
                                        <td>
                                            {{ Form::text("otherLivingDetailTwo[$count][dailyWorkHour]",  isset($memberArr['dailyWorkHour']) ? $memberArr['dailyWorkHour'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Daily Work Hour', 'id' => '']) }}
                                        </td>
                                        <td>
                                            {{ Form::text("otherLivingDetailTwo[$count][monthlySalary]",  isset($memberArr['monthlySalary']) ? $memberArr['monthlySalary'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Monthly Salary', 'id' => '']) }}
                                        </td>
                                        <td>
                                            <?php 
                                            if($count == 1){
                                                ?>
                                                <div id="addNewRowInTableTwo" class="btn btn-success" style="padding:2px;">
                                                    <i class="fa fa-plus"></i>
                                                </div>
                                                <?php }else{ ?>
                                                <div class="btn btn-danger btnRemoveItem" style="padding:2px;">
                                                    <i class="fa fa-minus"></i>
                                                </div>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <br>
                            </div>     
                        </div>
                        <div class="msgDisplay"></div>
                    </div>
                </div>

                <script type="text/javascript">
                    var addRowBtn = document.getElementById('addNewRowInTableOne');
                    var othMemCount = 2222;

                    addRowBtn.addEventListener('click', function(){
                        othMemCount++;
                        var no = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="">';

                        var name = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="otherLivingDetailOne['+ othMemCount +'][name]">';

                        var age = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="otherLivingDetailOne['+ othMemCount +'][age]">';

                        var noSafety = '<input type="checkbox" value="1" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="otherLivingDetailOne['+ othMemCount +'][noSafety]">';

                        var workHelp = '<input type="checkbox" value="1" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="otherLivingDetailOne['+ othMemCount +'][workHelp]">';

                        var employment = '<input type="checkbox" value="1" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="otherLivingDetailOne['+ othMemCount +'][employment]">';

                        var study = '<input type="checkbox" value="1" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="otherLivingDetailOne['+ othMemCount +'][study]">';

                        var others = '<input type="checkbox" value="1" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="otherLivingDetailOne['+ othMemCount +'][others]">';            

                        var cancelButton = '<div class="btn btn-danger btnRemoveItem"><i class="fa fa-minus"></i></div>';

                        var insertNewRow = "<tr class='tblRow'>"
                        +"<td>"+no+"</td>"
                        +"<td>"+name+"</td>"
                        +"<td>"+age+"</td>"
                        +"<td>"+noSafety+"</td>"
                        +"<td>"+workHelp+"</td>"
                        +"<td>"+employment+"</td>"
                        +"<td>"+study+"</td>"
                        +"<td>"+others+"</td>"
                        +"<td>"+cancelButton+"</td>"
                        +"</tr>";

                        $('#otherMemTb1').append(insertNewRow);                        
                        $('.btnRemoveItem').on('click', function(e){
                            $(this).parent().parent().remove();
                            return false;
                        });
                    });
                    
                    // $('.btnRemoveItem').on('click', function(e){
                    //     $(this).parent().parent().remove();
                    //     return false;
                    // });



                    var addRowBtn2 = document.getElementById('addNewRowInTableTwo');

                    addRowBtn2.addEventListener('click', function(){
                        othMemCount++;
                        var no = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="" name="">';

                        var childName = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="childName" name="otherLivingDetailTwo['+ othMemCount +'][childName]">';

                        var childAge = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="childAge" name="otherLivingDetailTwo['+ othMemCount +'][childAge]">';

                        var startDate = '<input type="date" class="mdl-textfield__input dashed-input-field" placeholder="startDate" name="otherLivingDetailTwo['+ othMemCount +'][startDate]">';

                        var salaryBased = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="salaryBased" name="otherLivingDetailTwo['+ othMemCount +'][salaryBased]">';

                        var workType = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="workType" name="otherLivingDetailTwo['+ othMemCount +'][workType]">';

                        var dailyWorkHour = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="dailyWorkHour" name="otherLivingDetailTwo['+ othMemCount +'][dailyWorkHour]">';

                        var monthlySalary = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="monthlySalary" name="otherLivingDetailTwo['+ othMemCount +'][monthlySalary]">';            

                        var cancelButton = '<div class="btn btn-danger btnRemoveItem"><i class="fa fa-minus"></i></div>';

                        var insertNewRow = "<tr class='tblRow'>"
                        +"<td>"+no+"</td>"
                        +"<td>"+childName+"</td>"
                        +"<td>"+childAge+"</td>"
                        +"<td>"+startDate+"</td>"
                        +"<td>"+salaryBased+"</td>"
                        +"<td>"+workType+"</td>"
                        +"<td>"+dailyWorkHour+"</td>"
                        +"<td>"+monthlySalary+"</td>"                        
                        +"<td>"+cancelButton+"</td>"
                        +"</tr>";

                        $('#otherMemTb2').append(insertNewRow);

                        $('.btnRemoveItem').on('click', function(e){
                            $(this).parent().parent().remove();
                            return false;
                        });

                    });

                </script>