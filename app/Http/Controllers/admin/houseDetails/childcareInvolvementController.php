<?php

namespace App\Http\Controllers\admin\houseDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\houseDetails\childCareInvolvement\HsChildSecurity;
use App\model\admin\houseDetails\childCareInvolvement\HsMarriageDetail;
use App\model\admin\houseDetails\childCareInvolvement\HsFamilyLivingDetail;
use App\model\admin\houseDetails\childCareInvolvement\HsFmMemWorkingDetail;
use App\model\admin\houseDetails\childCareInvolvement\HsOtherMemLivingTog;
use App\model\admin\houseDetails\childCareInvolvement\HsChildRepresentative;
use App\model\mylogic;
use Auth;
use DB;
use Validator;

class childcareInvolvementController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function insertChildSecurity(Request $request){

   if ($request->myHouseId && $request->fm_member_id) {
    $childSecurityDetails = json_encode($request->childSecurityDetails);

    $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
    $isExistsHsNum = HsChildSecurity::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

    if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
      try {
             $childSecurity  = !$isExistsHsNum['house_details_id'] ? new HsChildSecurity : HsChildSecurity::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $childSecurity->createdBy         = Auth::user()->id;
             $childSecurity->municipilities_id = $ishouseExists['municipilities_id'];
             $childSecurity->wards_id          = $ishouseExists['wards_id'];
             $childSecurity->house_details_id  = $ishouseExists['id'];

             $childSecurity->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $childSecurity->childSecurityDetails = $childSecurityDetails;

             $childSecurity->status            = 0;
             $childSecurity->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $childSecurity->save() : $childSecurity->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_child_securities', $tblId = $childSecurity->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
              ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
             }else{
              return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
              ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
          } catch (Exception $e) {
           DB::rollback();
           return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
       }else{ 
         return back()->withMessage(config('activityMessage.idNotFound'));
       }
     }else{ 
       return back()->withMessage('House number is missing please try to insert it from browser');
     }
   }
    //hs marriageDetail
   public function insertMarriageDetails(Request $request){ 
    $marriageDetailArr = $request->marriageDetail; 
    $marriageDetailSelectedArray = [];
    DB::beginTransaction();   
    foreach ($marriageDetailArr as $marriageDetail) {    
     if (isset($marriageDetail['selectMarriageDetail']) && $marriageDetail['selectMarriageDetail'] == 1 ) {
       $marriageDetailSelectedArray[] = $marriageDetail;
     }
   }
   $marriageDetailJson = json_encode($marriageDetailSelectedArray);

   if ($request->myHouseId && $request->fm_member_id) {
    $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
    $isExistsHsNum = HsMarriageDetail::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

    if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
      try {
             $hsMarriageDetail  = !$isExistsHsNum['house_details_id'] ? new HsMarriageDetail : HsMarriageDetail::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $hsMarriageDetail->createdBy         = Auth::user()->id;
             $hsMarriageDetail->municipilities_id = $ishouseExists['municipilities_id'];
             $hsMarriageDetail->wards_id          = $ishouseExists['wards_id'];
             $hsMarriageDetail->house_details_id  = $ishouseExists['id'];

             $hsMarriageDetail->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $hsMarriageDetail->haveMarriageInFive = $request->haveMarriageInFive;
             $hsMarriageDetail->marriageDetail = $marriageDetailJson;
             
             $hsMarriageDetail->status            = 0;
             $hsMarriageDetail->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $hsMarriageDetail->save() : $hsMarriageDetail->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_marriage_details', $tblId = $hsMarriageDetail->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
              ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
             }else{
              return response()->json([
                'success' => false,
                'message' => config('activityMessage.saveMessage')
              ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
          } catch (Exception $e) {
           DB::rollback();
           return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
       }else{ 
         return back()->withMessage(config('activityMessage.idNotFound'));
       }
     }else{ 
       return back()->withMessage('House number is missing please try to insert it from browser');
     }
   } 

   //hs marriageDetail
   public function insertFamilyLiving(Request $request){ 
    DB::beginTransaction();   
    $familyLivingDetailsJson = json_encode($request->familyLivingDetails);
    if ($request->myHouseId && $request->fm_member_id) {
      $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
      $isExistsHsNum = HsFamilyLivingDetail::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

      if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
             $hsFamilyLiving  = !$isExistsHsNum['house_details_id'] ? new HsFamilyLivingDetail : HsFamilyLivingDetail::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $hsFamilyLiving->createdBy         = Auth::user()->id;
             $hsFamilyLiving->municipilities_id = $ishouseExists['municipilities_id'];
             $hsFamilyLiving->wards_id          = $ishouseExists['wards_id'];
             $hsFamilyLiving->house_details_id  = $ishouseExists['id'];

             $hsFamilyLiving->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $hsFamilyLiving->haveChildLived = $request->haveChildLived;
             $hsFamilyLiving->familyLivingDetails = $familyLivingDetailsJson;
             
             $hsFamilyLiving->status            = 0;
             $hsFamilyLiving->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $hsFamilyLiving->save() : $hsFamilyLiving->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_marriage_details', $tblId = $hsFamilyLiving->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
              ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
             }else{
              return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
              ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
          } catch (Exception $e) {
           DB::rollback();
           return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
       }else{ 
         return back()->withMessage(config('activityMessage.idNotFound'));
       }
     }else{ 
       return back()->withMessage('House number is missing please try to insert it from browser');
     }
   }

    //hs marriageDetail
   public function insertFmMemWorking(Request $request){ 
    $fmMemWorkingArr = $request->familyWorkingDetails; 
    $fmMemWorkingSelectedArray = [];
    DB::beginTransaction();   
    foreach ($fmMemWorkingArr as $fmMemWorking) {    
     if (isset($fmMemWorking['isWorking']) && $fmMemWorking['isWorking'] == 1 ) {
       $fmMemWorkingSelectedArray[] = $fmMemWorking;
     }
   }
   $familyWorkingDetailsJson = json_encode($fmMemWorkingSelectedArray);
   if ($request->myHouseId && $request->fm_member_id) {
    $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
    $isExistsHsNum = HsFmMemWorkingDetail::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

    if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
      try {
             $hsFmMemWorking  = !$isExistsHsNum['house_details_id'] ? new HsFmMemWorkingDetail : HsFmMemWorkingDetail::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $hsFmMemWorking->createdBy         = Auth::user()->id;
             $hsFmMemWorking->municipilities_id = $ishouseExists['municipilities_id'];
             $hsFmMemWorking->wards_id          = $ishouseExists['wards_id'];
             $hsFmMemWorking->house_details_id  = $ishouseExists['id'];

             $hsFmMemWorking->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $hsFmMemWorking->haveWorking = $request->haveWorking;
             $hsFmMemWorking->familyWorkingDetails = $familyWorkingDetailsJson;             
             
             $hsFmMemWorking->status            = 0;
             $hsFmMemWorking->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $hsFmMemWorking->save() : $hsFmMemWorking->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_fm_mem_working_details', $tblId = $hsFmMemWorking->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
              ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
             }else{
              return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
              ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
          } catch (Exception $e) {
           DB::rollback();
           return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
       }else{ 
         return back()->withMessage(config('activityMessage.idNotFound'));
       }
     }else{ 
       return back()->withMessage('House number is missing please try to insert it from browser');
     }
   }
    //hs marriageDetail
   public function insertOtherMemLivingTog(Request $request){ 
    DB::beginTransaction();   
    $otherLivingDetailOne = json_encode($request->otherLivingDetailOne);
    //dd($otherLivingDetailOne);
    $otherLivingDetailTwo = json_encode($request->otherLivingDetailTwo);
   //dd($request->otherLivingDetailOne);
    if ($request->myHouseId && $request->fm_member_id) {
      $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
      $isExistsHsNum = HsOtherMemLivingTog::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

      if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
             $otherLiving  = !$isExistsHsNum['house_details_id'] ? new HsOtherMemLivingTog : HsOtherMemLivingTog::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $otherLiving->createdBy         = Auth::user()->id;
             $otherLiving->municipilities_id = $ishouseExists['municipilities_id'];
             $otherLiving->wards_id          = $ishouseExists['wards_id'];
             $otherLiving->house_details_id  = $ishouseExists['id'];

             $otherLiving->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $otherLiving->haveOtherLiving = $request->haveOtherLiving;
             $otherLiving->otherLivingDetailOne = $otherLivingDetailOne;
             $otherLiving->haveOtherChildLiving = $request->haveOtherChildLiving;
             $otherLiving->otherLivingDetailTwo = $otherLivingDetailTwo;           

             $otherLiving->status            = 0;
             $otherLiving->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $otherLiving->save() : $otherLiving->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_fm_mem_working_details', $tblId = $otherLiving->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
              ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
             }else{
              return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
              ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
          } catch (Exception $e) {
           DB::rollback();
           return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
       }else{ 
         return back()->withMessage(config('activityMessage.idNotFound'));
       }
     }else{ 
       return back()->withMessage('House number is missing please try to insert it from browser');
     }
   }  

     //hs marriageDetail
   public function insertFmChildRepresentative(Request $request){ 
    DB::beginTransaction();   
    //dd($request->all());
    // $otherLivingDetailOne = json_encode($request->otherLivingDetailOne);
    //dd($otherLivingDetailOne);
    // $otherLivingDetailTwo = json_encode($request->otherLivingDetailTwo);
   //dd($request->otherLivingDetailOne);

    $JSONDataChildRep = json_encode($request->childRepresentativeDetail);

    if ($request->myHouseId && $request->fm_member_id) {
      $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
      $isExistsHsNum = HsChildRepresentative::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

      if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
             $childRepreset  = !$isExistsHsNum['house_details_id'] ? new HsChildRepresentative : HsChildRepresentative::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $childRepreset->createdBy         = Auth::user()->id;
             $childRepreset->municipilities_id = $ishouseExists['municipilities_id'];
             $childRepreset->wards_id          = $ishouseExists['wards_id'];
             $childRepreset->house_details_id  = $ishouseExists['id'];

             $childRepreset->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID
             $childRepreset->childRepresentativeDetail = $JSONDataChildRep;
             $childRepreset->haveChildLeft = $request->haveChildLeft;

             
             $childRepreset->status            = 0;
             $childRepreset->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $childRepreset->save() : $childRepreset->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_fm_mem_working_details', $tblId = $childRepreset->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
              ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
             }else{
              return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
              ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
          } catch (Exception $e) {
           DB::rollback();
           return back()->withMessage(config('activityMessage.dataNotInserted'));
         }
       }else{ 
         return back()->withMessage(config('activityMessage.idNotFound'));
       }
     }else{ 
       return back()->withMessage('House number is missing please try to insert it from browser');
     }
   }
 }
