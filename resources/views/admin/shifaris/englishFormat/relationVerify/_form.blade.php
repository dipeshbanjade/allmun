<div class="right_col eng" role="main" style="min-height: 570px;">
  <div class="clearfix">

  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x eng">Relationship Verification</h3>
          <div class="clearfix">

          </div>
        </div>
        <div class="x_content" id="block">
          @include('admin.shifaris.municipalityDetail')
          
          <div class="row top-part">
            <div class="col-md-6 ps-cn">
              <p><b class="ps">प. सं.:</b>
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
                @endif
              </p>
              <p align="left" class="star">
                <b class="cn">च. नं.:</b>
                {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
              </p>
            </div>
            <div class="col-md-6">
              <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p align="center" class="font-size-24">
              </p><h3 align="center"><b>  Subject:Relationship Verification  </b></h3>
              {{ Form::hidden('letter_subject', 'Relationship Verification') }}
              <p></p>
              <p align="center" class="font-size-24">
              </p><h4 align="center"><b> To Whom It May Concern </b></h4>
              <p></p>
            </div>
            <div class="col-md-12 content-para">
              <p align="left">This is to certify that 
                <select onchange="changeSelect(this)" name="title">
                  <option value="Mr" {{(!empty($data) && $data->title == "Mr") ? 'selected' : ''}}>Mr</option>
                  <option value="Mrs"  {{(!empty($data) && $data->title == "Mrs") ? 'selected' : ''}}>Mrs</option>
                  <option value="Miss" {{(!empty($data) && $data->title == "Miss") ? 'selected' : ''}}>Miss</option>
                </select>
                {{ Form::text('name', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
                permanent resident of 
                <b>
                  {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                </b>, Ward No 
                <b>
                  {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
                </b>,
                <b>
                  {{ Form::text('districtName', isset($data->districtName) ? $data->districtName : getChecker()->districts->districtNameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                </b> Nepal(
                <select onchange="changeSelect(this)" name="former">
                  <option value="Former" {{(!empty($data) && $data->former == 'Former')? 'selected' :''}}>Former</option>
                  <option value="Previously designated as" {{(!empty($data) && $data->former == 'Previously designated as')? 'selected' :''}}>Previously designated as</option>
                </select>
                <b>
                  {{ Form::text('preVDCName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'preVDCName']) }}
                </b>
                <b>
                  <select onchange="changeSelect(this)" name="municipalityType">
                    <option value="V.D.C" {{(!empty($data) && $data->municipalityType == 'V.D.C') ? 'selected' :''}}>V.D.C</option>
                    <option value="Municipality"  {{(!empty($data) && $data->municipalityType == 'Municipality') ? 'selected' :''}}>Municipality</option>
                    <option value="Sub- Metropolitian" {{(!empty($data) && $data->municipalityType == 'Sub- Metropolitian') ? 'selected' :''}}>Sub- Metropolitian</option>
                    <option value="Metropolitian" {{(!empty($data) && $data->municipalityType == 'Metropolitian') ? 'selected' :''}}> Metropolitian</option>
                  </select>
                </b>, Ward No. 
                {{ Form::text('preWardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} )
                has submitted an application for the verification of relationship with
                <b>
                  <select onchange="changeSelect(this)" name="titleType">
                    <option value="his" {{(!empty($data) && $data->titleType == 'his')?'selected':''}}>his</option>
                    <option value="her" {{(!empty($data) && $data->titleType == 'her')?'selected':''}}>her</option>
                  </select>
                </b> family.
              </p>
              <p> 
                As per the inquiry and witnesses at ward level, this is hereby verified in accordance to the Local Governance Operation Act 2074 (2017), clause '12' sub clause 'e' that the following persons have following relationship as mentioned below. 
              </p>
            </div>
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-bordered" id="relation_verification_table">
                  <thead>
                    <tr>
                      <th> S.N. </th>
                      <th> Title </th>
                      <th> Relative's Name </th>
                      <th> Relation with Applicant </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      $count = 1;
                      $relationVerifyDetail = !empty($data)? json_decode($data->relationVerifyDetail,true) : [1];
                    ?>
                    @foreach ($relationVerifyDetail as $singleData)
                      <tr>
                        <td>
                          {{$count++}}
                        </td>
                        <td>
                          <select onchange="changeSelect(this)" name="relativeTitle[]">
                            <option value="Mr" {{(!empty($singleData['relativeTitle'] ) && $singleData['relativeTitle'] == 'Mr') ? 'selected' : ''}}>Mr</option>
                            <option value="Mrs" {{(!empty($singleData['relativeTitle'] ) && $singleData['relativeTitle'] == 'Mrs') ? 'selected' : ''}}>Mrs</option>
                            <option value="Miss" {{(!empty($singleData['relativeTitle'] ) && $singleData['relativeTitle'] == 'Miss') ? 'selected' : ''}}>Miss</option>
                          </select>
                        </td>
                        <td>
                          {{ Form::text('relativeName[]',!empty($singleData['relativeName']) ? $singleData['relativeName'] : NULL, ['class'=>'dashed-input-md-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                        </td>
                        <td>
                          {{ Form::text('relativeRelation[]', !empty($singleData['relativeRelation']) ? $singleData['relativeRelation'] : NULL, ['class'=>'dashed-input-md-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                        </td>
                        @if ($count==2)
                          <td class="add-btns" style="border: 0px;"><button type="button" id="addTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button></td>
                        @else
                          <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                        @endif

                      </tr>
                      {{-- expr --}}
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12">
          <div class="text-right btm-last">
            <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
            <p> 
              <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
                @if(count($deginationsId) > 0)
                @foreach($deginationsId as $deg)
                <option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                  {{ $deg->nameNep }} 
                </option>
                @endforeach
                @endif
              </select>
            </p>
          </div>
          <!--views for nibedak detail -->
          <div class="clearfix">

          </div>
          <hr>
          @include('admin.shifaris.nibedakCommonField')

          <!-- END -->
        </div>
      </div>

    </div>
  </div>
</div>
</div>


