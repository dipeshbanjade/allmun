@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Financial Treatment Suppport Recommendation<small> उपचारमा आर्थीक सहायता सिफारिस
</small></header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>
	

{{-- 	{{ Form::open(['route' => 'admin.arthritisSupport.store', 'name' => 'frmWardarthritisSupport', 'class'=>'sifarishSubmitForm']) }}
	@include('admin.shifaris.economic.arthritisSupport._form') --}}
	{!! Form::model($data, ['method' => 'POST','route' => ['admin.arthritisSupport.store', $data->id], 'files'=>true, 'name' => 'frmEditArthritisSupport', 'class'=>'sifarishSubmitForm']) !!}

	{{ Form::hidden('shifarisId',  $data->id )}}

	@include('admin.shifaris.economic.arthritisSupport._form')

	@include('admin.shifaris.commonButton')
	{{ Form::close() }}
</div>
<div class="msgDisplay" style="z-index: 999999 !important"></div>
@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
@include('admin.shifaris.script')
@include('admin.shifaris.economic.arthritisSupport.script')
@endsection