<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class IdentifyVerifyTwoValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
        'refCode' => 'required|min:3',
        'issuedDate' => 'required',
        'letterSub' =>'required|min:3|max:50',
        'appName' => 'required|min:3|max:50',
        'appTitle' => 'required',
        'municipality' =>'required|min:3|max:50',
        'ward' => 'required',
        'district' => 'required',
        'former' =>'required',
        'orgAddr' => 'required|min:3|max:40',
        'orgType' => 'required',
        'orgWard' =>'required',
        'citizenshipNo' => 'required',
        'passportNo' => 'required',
        'name' =>'required|min:3|max:50',
        'citizenshipDate' => 'required',
        'passportDate' => 'required',
        'marriageNo' =>'required',
        'marriageDate' => 'required',
        'FemaleName' => 'required|min:3|max:50',
        'actualName' =>'required|min:3|max:50', 
        'nameOpt' =>'required',
        'authorizedPerson' => 'required',
        'deginations_id' => 'required'
        ];
    }
}
