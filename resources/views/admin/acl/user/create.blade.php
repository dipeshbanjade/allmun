@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
	    <div class="card-body">
              <div class="card-head">
                 <i class="fa fa-plus"></i>
                <header>Create New User</header>
              </div>
              {{ Form::open(['route' => 'system.userDetails.store', 'name' => 'frmCreateUser', 'method' => 'post']) }}
                 <div class="col-sm-12" title="select role for">
                     <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                      <label id="dropLabel">Select Role for</label>
                      <select name="selectRoleFor" class="selectRoleFor form-control" required>
                             <option value="">Select role for</option>
                             <option value="mun">Create municipility users</option>
                             <option value="wrd">Role for ward users</option>
                       <option>
                       </option>
                     </select>
                   </div>
                 </div>
                 <!--  -->
                <div class="frmContain" style="display: none;">
                   @include('admin.acl.user._form')
                   <div class="col-sm-6">
                       <div class = "dropDownSelectBox mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">

                       </div>
                    </div>
                 <p class="pull-right">
                 	 <button type="submit" class="btn btn-success">
                 	  <i class="fa fa-plus"></i>CREATE</button>
                 	 <button class="btn btn-danger" onclick="history.go(-1)">
                 	   BACK
                 	   </button>
                 </p>
                </div>
              {{ Form::close() }}
	     </div>
   </div>
 </div>
@endsection
@section('custom_script')
   @include('admin.acl.user.script')
@endsection