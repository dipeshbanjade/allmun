<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
	protected $table = 'vehicles';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
