<script type="text/javascript">
$(function(){
	/*add buttoin*/
	$("form[name='frmWardmarriageVerify']").validate({
		rules:{

			issuedDate : {
				required : true,
				date : true
			},
			letterSub : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			brideName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			bridefatherName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			brideGroomName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			brideGroomfatherName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipality : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			ward : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			dateInBS : {
				required : true,
				date : true
			},
			dateInAD : {
				required : true,
				date : true
			},
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {

			issuedDate : "Issue Date is required",
			letterSub : "Letter subject is required",
			brideName : "Bride name is required",
			bridefatherName : "Bride Father Name is required",
			brideGroomName : "Bride Groom Name is required",
			brideGroomfatherName : "Bride Groom Father name is required",
			municipality : "Municipality is required",
			ward : "Ward is required",
			dateInBS : "Date in BS is required",
			dateInAD : "Date in AD is required",
			authorizedPerson : "Authorized Person is required",

		}
	});
});
$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                  $("input[name='brideName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
            
          },complete:function(){
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>