@include('admin.shifaris.script')

<script type="text/javascript">

	var addButton = $('#btnAddTableRow');
	var clickCount = 1;

	addButton.on('click', function(e){
		e.preventDefault();
		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var businessCategory = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="businessCategory[]">';

		var businessNature = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="businessNature[]">';

		var ward = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="ward[]">';

		var regNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="regNo[]">';

		var tollNo = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="tollNo[]">';

		var bhatoNaam = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="bhatoNaam[]">';
		
		var gharNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="gharNo[]">';


		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"

		+"<td>"+businessCategory+"</td>"
		+"<td>"+businessNature+"</td>"
		+"<td>"+ward+"</td>"
		+"<td>"+regNo+"</td>"
		+"<td>"+tollNo+"</td>"
		+"<td>"+bhatoNaam+"</td>"
		+"<td>"+gharNo+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#business_close_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmWardcloseBusiness']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 10
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 1,
					maxlength : 30					
				},
				municipality : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				organizationType : {
					required : true
				},
				organizationWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				title : {
					required : true
				},
				name : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				closedDate : {
					required : true,
					date : true
				},
				viewDate : {
					required : true,
					date : true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
			},
			messages: {
				refCode : "Reference Code is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number must be number",
				municipality : "Municipality is required",
				wardNumber : "Ward Number must be number",
				organizationAddress : "OrganizationAddress is required",
				organizationType : "Organization Type is required",
				organizationWard : "Organization Ward must be number",
				title : "Title is required",
				name : "Name is required",
				closedDate : "Close Date is required",
				viewDate : "View Date is required",				
				authorizedPerson : "Authorized Person is required"
			}
		});
	});

	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='name']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');

// code for gender determination and option change partial work

					// if(response.gender == 'female'){
					// 	$("select[name='title'] option[value=श्री]").attr('selected', true);
					// 	changeSelect($("select[name='title']"));
					// 	// $("select[name='title'] option[value=श्री]").trigger('change');
					// }else{
					// 	$("select[name='title'] option[value=सुश्री]").attr('selected', true);	
					// 	changeSelect($("select[name='title']"));
					// 	// $("select[name='title'] option[value=सुश्री]").trigger('change');				
					// 	// $("input[name='title']").val('सुश्री');
					// 	// $("input[name='name']")
					// }
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>