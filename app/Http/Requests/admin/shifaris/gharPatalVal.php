<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class gharPatalVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'issuedDate' => 'required|min:3|unique:consumer_committes',
            'chalaniNumber' => 'required',
            'accountHolderName' => 'required',
            'accountHolderAddress' => 'required',
            'municipalityName' => 'required',
            'wardNumber' => 'required',
            'houseOwnerName' => 'required',
            'kittaNumber' => 'required',
            'area' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
