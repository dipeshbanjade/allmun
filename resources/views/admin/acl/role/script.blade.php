<script type="text/javascript">
	var btnAddRole = $('#btnAddRole');
	btnAddRole.on('click', function(){
	  $('#frmRole').modal('show'); 
	});
	$(function(){
	     $("form[name='frmRole']").validate({
	      rules:{
	       roleName : {
	         required : true,
	         minlength : 3,
	         maxlength : 30
	       },
	       roleFor :{
	       	required :true
	       }
	      },
	      messages: {
	       roleName : "role name required with min with 3 character"
	      }
	     });
	   });
</script>