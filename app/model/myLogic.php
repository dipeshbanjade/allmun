<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use App\model\admin\municipility\MunicipilityDetails;
use App\model\admin\acl\Role;
use App\model\admin\ward\WardDetails;
use App\model\admin\ward\WardStaff;
use App\model\admin\citizen\CitizenInfo;
use App\model\admin\ward\WardStaffLeave;
use App\model\admin\setLeave\leaveisFor;


// use App\model\admin\houseDetails\houseDetails;
use App\model\admin\houseDetails\HouseDetails;
use AppHelper;
use File;
use Auth;
use DB;
use app\User;
use App\model\shifaris\ApplicantUser;
use App\model\setting\Department;
use App\model\admin\municipility\StaffMunicipility;
use Illuminate\Support\Facades\Hash;
use App\model\admin\citizen\CitizenRelation;
use App\model\setting\occupation;
use App\model\admin\biomatric\bioMatric;
use Carbon\Carbon;


class mylogic extends Model
{
    public function __construct()
    {
        $this->middleware('auth');
    }

   /*getting data for drop down cannot do paginationn */
   public static function getDrowDownData($tablename = null, $orderColumn = null, $orderby = null){
     return  DB::table($tablename)->orderby($orderColumn, $orderby)->where(['status'=> 0, 'softDelete' => 0])->get();
   }

   public static function drowDownDataByTagFilter($tablename = null,$filterCol = null,$filterVal = null, $orderColumn = null, $orderby = null){
     return  DB::table($tablename)->orderby($orderColumn, $orderby)->where(['status'=> 0, 'softDelete' => 0, $filterCol => $filterVal])->get();
   }
   /*crop image*/
   public static function cropImage($file, $sizeWidth, $sizeHeight, $path, $imageName){
     $upload = Image::make( $file )->resize( $sizeWidth, $sizeHeight )->save($path . $imageName );
     if (!$upload) {
         return false;
     }
     return true;
  }

  public static function getMunicipalityDetails(){ //GETTING MUNICIPALITY DETAILS
    return MunicipilityDetails::select('id', 'uniqueCode', 'nameNep', 'nameEng', 'bioDeviceId')->first();
  }

  public static function getAllWardDetails(){ //GETTING WARD DETAILS
    return WardDetails::select('id', 'uniqueCode', 'nameNep', 'nameEng')->orderby('nameEng', 'ASC')->get();
  }

   public static function successSaveMessage($code){
    return response()->json([
          'success'  => true,
          'message'  => config('activityMessage.saveMessage')
        ], $code);
   }

   public static function successUpdateMessage($code = null){
    return response()->json([
          'success'  => true,
          'message'  => config('activityMessage.updateMessage')
        ], $code);
   }

   public static function unSeccessUpdateMessage($code = null){
    return response()->json([
          'success'  => false,
          'message'  => config('activityMessage.notUpdated')
        ], $code);
   }

   public static function successDeleteMessage($code = null){
    return response()->json(['success' => true, 'message' =>config('activityMessage.deleteMessage')], $code);
   }

   public static function unSuccessDeleteMessage($code = null){
    return response()->json(['success' => false, 'message' =>config('activityMessage.unDeleteMessage')], $code);
   }

   public static function idNotFoundMessage($code = null){
    return response()->json(['success' => false, 'message' => config('activityMessage.idNotFound')], $code);
   }

   public static function successChangeStatus(){
    return response()->json(['success' => true, 'message' => config('activityMessage.EnableMessage')]);
   }

   /*reference code for all*/
  public static function sysRefCode($tablename = null, $prefix = null){
    $latestId = DB::table($tablename)->select('id')->latest()->first();
    return $latestId ? $prefix.'_'. $latestId->id +=1 : $prefix .'_'.'1';   
  }

  /*change status*/
  public static function sysChangeStatus($tablename = null , $Id = null, $status = null){
     $changeStatus = DB::table($tablename)->where('id', $Id)->update(['status' => $status]);
     if ($changeStatus) {
       return true;
     }else{
      return false;
     }
  }
  /*-----------*/

  public static function changeLeaveStatus($tablename = null , $Id = null, $status = null, $approveBy = null){
    $changeStatus = DB::table($tablename)->where('id', $Id)->update(['status' => $status, 'approvedBy' => $approveBy]);
    if ($changeStatus) {
      return true;
    }else{
     return false;
    }
  }

  public static function getRandNumber(){
    return uniqid();
  }

  /*insert in log file*/
  public static function insertInLogFile($userId, $action, $tableName, $tblId){
      $log = DB::table('log_files')->insert(
                array('users_id' => $userId,
                      'action'   => $action,
                      'tblName'  => $tableName,
                      'tblId'    => $tblId,
                      'status'   => 0,
                      'softDelete'=> 0,
                      "created_at" =>  \Carbon\Carbon::now(), # \Datetime()
                      "updated_at" => \Carbon\Carbon::now()
                      )
                 );
      return $log ? true : false;
  }

  public static function getRoleBy($id){
   return Role::select('roleName')->where('id', $id)->first();
  }
  
  /*getting role as per municipility and ward*/
  public static function getSysRole($roleFor){
    if ($roleFor =='dev') {   // if developer
      return Role::with('permissions')->where(['softDelete' => 0, 'status' => 0])->orderby('roleName', 'ASC')->get();
    }
     return Role::with('permissions')->where(['softDelete' => 0, 'status' => 0, 'roleFor' => $roleFor])->orderby('roleName', 'ASC')->get();
  }

  public static function getUserDetailsById($staffId, $selectId, $selectIdName){
    return User::where(['status' => 0, 'softDelete' => 0, 'staffs' => $id])->first();
  }

  public static function getUserDetails(){
    $id = Auth::user()->id;
    return User::findOrFail($id);
  }

  public static function isWardStaffExists(){
    return WardStaff::where(['status' => 0, 'softDelete' => 0])->get();
  }

  /*citizenInfo*/
  public static function getCitizenDetailsById($citizenId){
    return CitizenInfo::where(['status' => 0, 'softDelete' => 0])->get();
  }

  public static function getHouseNumberByCitizenId($citizenId){   // getting house Number 
    return houseDetails::select('houseNumber', 'MunHouseNumber')->where(['citizen_infos_id' => $citizenId,  'status'=> 0, 'softDelete' => 0])->first();
  }

  public static function applicantUser($userId, $action, $tableName, $tblId, $applicantName, $applicantAddress, $citizenshipNumber, $phoneNumber, $email, $dob, $fatherName, $profilePic, $citizenshipImgPath){

    $citizenshipData = ApplicantUser::where(['citizenshipNumber' => $citizenshipNumber])->first();

    if($citizenshipData){
      return $citizenshipData->id;
    }

    $save = new ApplicantUser;
    $save->applicantName  = $applicantName;
    $save->applicantAddress  = $applicantAddress;
    $save->citizenshipNumber = $citizenshipNumber;
    $save->phoneNumber  = $phoneNumber;
    $save->email  = $email;
    $save->dob  = $dob;
    $save->fatherName  = $fatherName;
    $save->citizenshipImgPath  = $citizenshipImgPath;             

    $mySave = $save->save();
    return $mySave ? $save->id : false;
  }

  // public static function addHouseNumberToCitizen($tablename, $citizenId, $houseNumber){  
  //  return DB::table($tablename)->where('id', $citizenId)->update(['status' => $status, 'approvedBy' => $approveBy]);
  // }

  public static function getDepartmentName($id){
    return Department::select('nameNep', 'nameEng')->where(['id'=>$id,'status' => 0, 'softDelete' => 0])->first();
  }

  public static function idEncription($id){  
    return \Crypt::encrypt($id);
  }

  public static function getMyFamilyDetails($citizenId){
    return CitizenRelation::where(['citizens_id'=>$citizenId])->orderby('orderColumn', 'ASC')->get();
  }

   public static function getIndividualHouseDetails($id){   // getting indivudaul house details
      return HouseDetails::findOrfail($id, ['hsNum','wards_id', 'municipilities_id', 'id', 'citizen_infos_id']);
    }

  public static function getBirthList(){   // birth list
    $usr = Auth::user()->userLevel;
    switch ($usr) {
      case 'mun':
         $dobStaff = StaffMunicipility::with('departments')->select('firstNameEng', 'middleNameEng', 'lastNameEng', 'firstNameNep', 'middleNameNep', 'lastNameNep', 'dob','dobEng', 'departments_id')->whereNotNull('dobEng')->get();
         return $dobStaff;
        break;

      case 'wrd':
        $dobStaff = WardStaff::with('departments')->select('firstNameEng', 'middleNameEng', 'lastNameEng', 'firstNameNep', 'middleNameNep', 'lastNameNep', 'dob', 'dobEng', 'departments_id')->whereNotNull('dobEng')->get();
        return $dobStaff;
        break;
      
      default:
        return back()->withMessage(config('activityMessage.unAuthorized'));
        break;
    }
  }

  public static function getWardStaffOnLeave(){  // staff on leave
   $allLeave = WardStaffLeave::with(['leaveTypes', 'users'])->where(['softDelete' => 0, 'wards_id' => Auth::user()->wards_id])->paginate(config('activityMessage.pagination'));
   return $allLeave;
  }

  public static function getWardList(){
    return WardDetails::select('nameNep', 'nameEng', 'id')->where(['status'=> 0, 'softDelete'=> 0])->get();
  }

  public static function houseCount($wardId=null){
    return  HouseDetails::where(['status'=>0, 'softDelete'=>0, 'wards_id' => $wardId])->count();
  }

  public static function getTotalCitizenNo($wardId=null){
    return CitizenInfo::select('id')->where(['status'=>0, 'softDelete'=>0, 'wards_id' => $wardId])->count();
  }

  public static function getWardStaffNo($wardId){
   return WardStaff::where(['status' => 0, 'softDelete' => 0, 'wards_id'=> $wardId])->count();
  }

  public static function getWardDetailsById(){
    return WardDetails::where(['status'=>0, 'softDelete'=>0, 'id'=>Auth::user()->wards_id])->first();
  }

  public static function getGenderCount($gender){
    if (Auth::user()->userLevel == 'wrd') {
       return CitizenInfo::where(['status'=>0, 'softDelete'=>0, 'gender'=>$gender, 'wards_id' => Auth::user()->wards_id])->count();
    }else{
       return CitizenInfo::where(['status'=>0, 'softDelete'=>0, 'gender'=>$gender])->count();
    }
  }

  public static function getCitizenNameById($citizenId){
     return CitizenInfo::where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id, 'citizenNo' => $citizenId])->first();
   }
  
  public static function citizenWithCitizenCount(){
    if (Auth::user()->userLevel == 'wrd') {
       return CitizenInfo::select('citizenNo')->where(['softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->where('citizenNo', '<>', '')->count();
    }else{
       return CitizenInfo::select('citizenNo')->where(['softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->where('citizenNo', '<>', '')->count();
    }
  }
  public static function citizenWithoutCitizenCount(){
    if (Auth::user()->userLevel == 'wrd') {
       return CitizenInfo::select('citizenNo')->where(['softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->whereNull('citizenNo')->count();
    }else{
      return CitizenInfo::select('citizenNo')->where(['softDelete' => 0, 'status' => 0, 'wards_id'=>Auth::user()->wards_id])->whereNull('citizenNo')->count();
    }
  }

  public static function getDisableCitizen(){
    if (Auth::user()->userLevel == 'wrd') {
       return CitizenInfo::select('citizenNo', 'isdisabled')->where(['softDelete' => 0, 'status'=>0, 'wards_id'=>Auth::user()->wards_id, 'isdisabled' => 1])->count();
    }else{
      return CitizenInfo::select('citizenNo', 'isdisabled')->where(['softDelete' => 0,  'status'=>0, 'isdisabled' => 1, 'wards_id'=>Auth::user()->wards_id])->count();
    }
  }

  public static function getOccupationWiseCount($slug){
    $occId = occupation::select('id')->where(['status' => 0, 'softDelete'=> 0, 'nameEng' => $slug])->first();

    if ($occId) {
       return CitizenInfo::select('id')->where(['status' => 0, 'softDelete'=> 0, 'occupations_id' => $occId->id, 'wards_id'=>Auth::user()->wards_id])->count();
    } 
    return 0;
  }

  public static function getCountJobHolder($gender){
      $jobHolderCount =  CitizenInfo::select('gender', 'id')->where(['gender'=>$gender, 'status'=>0, 'softDelete'=>0, 'wards_id' => Auth::user()->wards_id])->count();
      if ($jobHolderCount > 0) {
        return $jobHolderCount;
      }
      return 0;
  }

  public static function getOccupationIdByName($slug){
     return occupation::select('id')->where(['status' => 0, 'softDelete'=> 0, 'nameEng' => $slug])->first();
  }

  public static function getCitizenByIdEncript($idEncript){
   return CitizenInfo::select('id')->where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id, 'idEncrip' => $idEncript])->first();
  }

  public static function getListOfWard(){
    return WardDetails::select('nameNep', 'nameEng','landLineNumber', 'muncipilityHead', 'id', 'identifier')->where(['status'=>0, 'softDelete'=>0])->get();
  }
  /**/
  public static function getCurrentAtten(){
    $today = date("Y-m-d");
    if (Auth::user()->userLevel == 'mun') {
      $atten =  DB::select(
          'select staff_municipilities.firstNameEng, staff_municipilities.middleNameEng, staff_municipilities.lastNameEng, staff_municipilities.firstNameNep, staff_municipilities.middleNameNep, staff_municipilities.departments_id, staff_municipilities.lastNameNep, staff_municipilities.pounchId, bio_matrics.punchId, bio_matrics.mode, bio_matrics.created_at, staff_municipilities.departments_id, departments.nameNep AS departmentNameNep, departments.nameEng AS departmentNameEng FROM staff_municipilities JOIN bio_matrics, departments WHERE staff_municipilities.pounchId = bio_matrics.punchId AND bio_matrics.created_at LIKE "%'.$today.'%" AND departments.id = staff_municipilities.departments_id'  
        );
      return $atten;
    }else{

    }
  }

  public static function getStaffDetails(){
    if (Auth::user()->userLevel == 'mun' || Auth::user()->userLevel == 'dev') {
      return $staff = StaffMunicipility::with('departments')->select('id', 'firstNameEng', 'middleNameEng', 'lastNameEng', 'firstNameNep', 'middleNameNep', 'lastNameNep', 'departments_id', 'pounchId')->where(['status'=>0, 'softDelete'=>0])->get();
    }else if (Auth::user()->userLevel == 'wrd') {
      return WardStaff::with('departments')->select('id', 'firstNameEng', 'middleNameEng', 'lastNameEng', 'firstNameNep', 'middleNameNep', 'lastNameNep', 'departments_id', 'pounchId')->where(['wards_id'=>Auth::user()->wards_id])->get();
    }else{
      return true;
    }
  }

  public static function getOfficialLeave(){
    $currDate = date('Y');
    if (Auth::user()->userLevel == 'mun' || Auth::user()->userLevel == 'dev') {
       return leaveisFor::select('dateFromEng', 'dateFromNep', 'leaveFor')->where(['status'=>0, 'softDelete'=>0])->where('dateFromEng', 'like', $currDate . '%')->whereNull('wards_id')->get();
    }else{
       return leaveisFor::select('dateFromEng', 'dateFromNep', 'leaveFor')->where(['status'=>0, 'softDelete'=>0, 'wards_id'=>Auth::user()->wards_id])->where('dateFromEng', 'LIKE', $currDate.'%')->get();
    }
  }

}