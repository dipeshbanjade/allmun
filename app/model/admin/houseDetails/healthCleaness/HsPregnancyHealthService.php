<?php

namespace App\model\admin\houseDetails\healthCleaness;

use Illuminate\Database\Eloquent\Model;

class HsPregnancyHealthService extends Model
{
    protected $table = 'hs_pregnancy_health_services';
}
