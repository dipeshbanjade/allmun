<?php

namespace App\Http\Requests\admin\municipality;

use Illuminate\Foundation\Http\FormRequest;

class municipalityStaffVal extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'citizenNo'      => 'required|min:4|max:30|unique:staff_municipilities',
            'firstNameEng'   => 'required|min:3|max:30',
            'lastNameEng'    => 'required',
            'phoneNumber'    => 'required|min:3|max:30|unique:staff_municipilities',
            'joingDateNep'   => 'required',
            'email'          => 'required|email|unique:staff_municipilities',
            'departments_id' => 'required',
            'pounchId'       => 'unique:staff_municipilities,pounchId,'
        ];
    }
}
