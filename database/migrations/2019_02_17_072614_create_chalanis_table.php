<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChalanisTable extends Migration
{
    public function up()
    {
        Schema::create('chalanis', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('users_id')->nullable()->unsigned();
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('idEncript');

            $table->string('chalaniNo')->nullable();
            $table->string('chalaniDate')->nullable();
            $table->string('chalaniPage')->nullable();
            $table->string('chalaniPageDate')->nullable();
            $table->string('chalaniSubject')->nullable();

            $table->integer('other_offices_id')->nullable()->unsigned();
            $table->foreign('other_offices_id')->references('id')->on('other_offices')->onDelete('cascade');
            $table->string('chalaniTicket')->nullable();
            $table->string('chalaniKaifiyat')->nullable();
            $table->string('chalaniStorePlace')->nullable();

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('chalanis');
    }
}
