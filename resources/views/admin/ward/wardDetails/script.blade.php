<script type="text/javascript">
	$(function(){
		$("form[name='creatWardFrm']").validate({
			rules:{
				nameNep : {
					minlength : 3,
					maxlength : 30
				},
				nameEng : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				landLineNumber : {
					required : true,
					minlength: 7,
					maxlength : 9,
					number : true
				},
				addrEng : {
					required :true,
					minlength : 3,
					maxlength :30
				},
				addrNep : {
					minlength : 3,
					maxlength : 30
				},
				provinces_id:{
					required: true
				},
				districts_id: {
					required: true
				},
				muncipilityHead: {
					required: true,
					minlength: 3,
					maxlength: 255
				}
			},
			messages: {
				nameEng : "The name of municipality is required",
				landLineNumber : {
					required: "Land Line Number is required",
					minlength: "Land Line Number should not be less than 7 digits",
					maxlength: "Land Line Number should not exceed 9 digits"
				},
				addrEng : "The address is required ",
				provinces_id: "Province should be selected",
				districts_id: "District should be selected",
				muncipilityHead : {
					required: "Municipality Head Name is required"
				},
				// wardLogo: "File format must be 'JPG', 'JPEG', 'PNG'"
			}
		});
	});
</script>