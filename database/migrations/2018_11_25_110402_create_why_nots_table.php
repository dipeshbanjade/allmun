<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhyNotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('why_nots', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('refCode',100)->unique();
            $table->string('nameNep')->nullable();
            $table->string('nameEng')->nullable();
            $table->string('tag')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);

            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('why_nots');
    }
}
