<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use App\model\admin\acl\Permission;
use App\model\admin\acl\Role;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];
    public function boot(GateContract $gate)
    {    //defining guard 
        $this->registerPolicies();
        //  foreach ($this->getPermission() as $permission) {
        //     Gate::define($permission->perName, function ($user) use ($permission){
        //            $role = Role::findOrFail($user->roles_id);
        //            $access = $this->sysAccess($role, $permission);
        //            return $access ? true : false;
        //         });
        // }

        
    }

    protected function getPermission(){
        return Permission::with('roles')->get();
    }
    protected function sysAccess($role, $permission){
       return $role->permissions;
    }
}