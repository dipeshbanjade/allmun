<script type="text/javascript">
$(function(){

	$("form[name='frmeditphone']").validate({
		rules:{
			name : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			mobileTag : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			name : "The name of phone is required",
			mobileTag : "The name of mobile brand is required",

		}
	});
});
$(function(){

	$("form[name='frmphone']").validate({
		rules:{

			name : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			mobileTag : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {

			name : "The name of phone is required",
			mobileTag : "The name of mobile brand is required",
			
		}
	});
	  var btnAddPhone = $('.btnAddPhone');
	  btnAddPhone.on('click', function(e){
	    $('#phoneModel').modal('show'); 
	});

	  var btnEditphone = $('.btnEditphone');
	  btnEditphone.on('click', function(e) {
	  
	      e.preventDefault();
	      var url = $(this).data('url');
	      $.ajax({
	       'type': 'GET',
	       'url': url,
	       success: function (response) {
	          $('.phoneId').val(response.id);
	          $('.txtRefCode').val(response.refCode).attr('disabled',true);
	          $('.txtName').val(response.name);
	          $('.txtMobileTag').val(response.mobileTag);
	          $("#editphoneModel").modal('show');
	      }
	  })
	  });

	  var frmUpdatephone = $('#frmUpdatephone');
	  frmUpdatephone.on('submit', function(e){
	      e.preventDefault();
	      var data = $(this).serialize();
	      var id   = $('.phoneId').val();
	      var url  = "{{URL::to('/')}}" + "/setting/phone/"+id+"/update";
	      $.ajax({
	          'type' : 'POST',
	          'url'  : url,
	          'data'    : data,
	          success : function(response){
	              console.log(response);
	              if (response.success==true) {
	                  $('.infoDiv').append(response.message).addClass('alert alert-success');
	              }
	              if (response.success==false) {
	                  $.each(response.message, function(key, value){
	                      $('.infoDiv').append(value).addClass('alert alert-danger');
	                  })
	              }
	          },complete:function(){
	              $("#editphoneModel").modal('hide');
	              setTimeout(location.reload.bind(location), 1500);
	          }
	      })
	      .fail(function (response) {
	          alert('data already exists');
	      });
	      $('#editphoneModel').modal('hide');
	  });
	  });

</script>