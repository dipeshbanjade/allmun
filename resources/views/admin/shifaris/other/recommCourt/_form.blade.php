<div class="row top-part">
  <div class="col-md-6">
    <p align="left">
     <b>@lang('commonField.extra.refCode'):</b>
     @if(isset($data->refCode))
     {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
     @else
     {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
     @endif
   </p>
   <p align="left">
     <b> च. नं.:
       {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
     </b>
   </p>
 </div>
 <div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :  
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
    </b>
  </p>
</div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">श्री जिल्ला अदालत,</p>
    <p align="left">
      {{ Form::text('courtAddress', isset($data->district) ? $data->district : getChecker()->districts->districtNameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'courtAddress', 'required' => 'required']) }}।
    </p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24">
    </p><h4 align="center"><b>विषय: सिफारिस सम्बन्धमा।</b> </h4>
    {{ Form::hidden('letter_subject', 'सिफारिस सम्बन्धमा।', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
    <p></p>
  </div>
</div>
<div class="row content-para">
  <div class="col-md-12">
    <p align="left"> 
      उपरोक्त सम्बन्धमा
      <b>
       {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
     </b> वडा नं. 
     <b>
      {{ Form::text('municipalityWard', Auth::user()->wards_id, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'municipalityWard', 'required' => 'required']) }}
    </b> 
    (साविक
    <!--SABIK ADDRESS START -->
    {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'organizationAddress', 'required' => 'required']) }}
    <!-- SABIK ADDRESS END -->
    <select onchange="changeSelect(this)" name="organizationType">
      <option value="गा.वि.स." 
      {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
      <option value="नगरपालिका" 
      {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
      <option value="उप महानगरपालिका" 
      {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
      <option value="महानगरपालिका" 
      {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
    </select>
    , वडा नं. 
    {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field star', 'placeholder'=>'    *', 'id' => 'organizationWard', 'required' => 'required']) }}
    )
    बस्ने  
    <b>
      <select onchange="changeSelect(this)" name="firstPersonTitle">
        <option value="श्री"
        {{ !empty($data->firstPersonTitle) && $data->firstPersonTitle == 'श्री' ? 'selected' : '' }}>श्री</option>
        <option value="सुश्री"
        {{ !empty($data->firstPersonTitle) && $data->firstPersonTitle == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
        <option value="श्रीमती"
        {{ !empty($data->firstPersonTitle) && $data->firstPersonTitle == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
      </select>
    </b>
    {{ Form::text('firstPersonName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'firstPersonName', 'required' => 'required']) }}
    <b>
      <select onchange="changeSelect(this)" name="firstAccusion">
        <option value="बादी" {{ !empty($data->firstAccusion) && $data->firstAccusion == 'बादी' ? 'selected' : '' }}>बादी</option>
        <option value="प्रतिवादी" {{ !empty($data->firstAccusion) && $data->firstAccusion == 'प्रतिवादी' ? 'selected' : '' }}>प्रतिवादी</option>
      </select>
    </b> ले पति/पत्नी 
    <b>
      <select onchange="changeSelect(this)" name="secondPersonTitle">
        <option value="श्री"
        {{ !empty($data->secondPersonTitle) && $data->secondPersonTitle == 'श्री' ? 'selected' : '' }}>श्री</option>
        <option value="सुश्री"
        {{ !empty($data->secondPersonTitle) && $data->secondPersonTitle == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
        <option value="श्रीमती"
        {{ !empty($data->secondPersonTitle) && $data->secondPersonTitle == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
      </select>
    </b> 
    {{ Form::text('secondPersonName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'secondPersonName', 'required' => 'required']) }} 
    <b>
      <select onchange="changeSelect(this)" name="secondAccusion">
        <option value="बादी" {{ !empty($data->secondAccusion) && $data->secondAccusion == 'बादी' ? 'selected' : '' }}>बादी</option>
        <option value="प्रतिवादी" {{ !empty($data->secondAccusion) && $data->secondAccusion == 'प्रतिवादी' ? 'selected' : '' }}>प्रतिवादी</option>
      </select>
    </b> संग श्री 
    {{ Form::text('districtCourt', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'districtCourt', 'required' => 'required']) }} 
    जिल्ला अदालतमा 
    {{ Form::text('case', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'case', 'required' => 'required']) }}
    मुद्दा चलिरहेकोमा आयश्रोत केही नभई आर्थिक अवस्था कमजोर भई कोर्ट-फी राख्न असमर्थ भएकोले तत्कालको लागि कोर्ट-फी नराखी पछि मुद्दा फैसला भएपछि उक्त कोर्ट-फी लिने गरी आवश्यक कारवाहीको लागि "सिफारिस गरी पाऊँ" भनी यस वडा कार्यालयमा निवेदन दिनुभएको हुँदा सो सम्बन्धमा 
    {{ Form::text('caseDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'caseDate',  'onfocus' => 'showNdpCalendarBox("caseDate")']) }}
  मा गरिएको सर्जमिन अनुसार व्यहोरा मनासिब बुझिएकोले त्यहाँको नियमानुसार गरीदिनुहुन सिफारिस गरिन्छ।</p>
</div>
</div>
<div class="text-right btm-last">
  <p>
    {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }}
  </p>
  <p> 
    <b>
      <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
        @if(count($deginationsId) > 0)
        @foreach($deginationsId as $deg)
        <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
          {{ $deg->nameNep }} 
        </option>
        @endforeach
        @endif
      </select>
    </b>
  </p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>