<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseUseForsTable extends Migration
{
    
    public function up()
    {
        Schema::create('house_use_fors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode',100)->unique();
            $table->string('nameNep')->nullable();
            $table->string('nameEng')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);   
            $table->timestamps();
        });
    }

  
    public function down()
    {
        Schema::dropIfExists('house_use_fors');
    }
}
