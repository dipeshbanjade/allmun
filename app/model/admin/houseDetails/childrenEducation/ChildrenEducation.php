<?php

namespace App\model\admin\houseDetails\childrenEducation;

use Illuminate\Database\Eloquent\Model;

class ChildrenEducation extends Model
{
    protected $table = 'children_educations';
}
