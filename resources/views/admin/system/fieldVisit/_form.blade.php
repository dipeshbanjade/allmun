<div class="row">
      <div class="col-md-4 col-sm-12">
          <div class="form-group">
              <label>@lang('commonField.extra.startTime')</label>
              {{ Form::time('startTime', null, ['class' => 'form-control', 'placeholder' => 'start time', 'title'=>'task start date', 'required'=>true]) }}
          </div>
          <div class="form-group">
              <label>@lang('commonField.extra.endTime')</label>
              {{ Form::time('endTime', null, ['class' => 'form-control', 'placeholder' => 'Enter End Time...', 'title'=>'task end date', 'required'=>true]) }}

          </div>
          <div class="form-group">
              <label>@lang('commonField.extra.startDate')</label>
              {{ Form::text('startDate', null, ['class' => 'form-control', 'placeholder' => '2075-07-04', 'id'=>'currNepDate', 'title'=>'task start date', 'required'=>true]) }}
          </div>
          <div class="form-group">
              <label>@lang('commonField.extra.endDate')</label>
              {{ Form::text('endDate', null, ['class' => 'form-control', 'placeholder' => '2075-07-04', 'id'=>'nDate', 'title'=>'task end date', 'required'=>true]) }}
          </div>
      </div>
      <div class="col-md-8  col-sm-12">
        <!-- summernote -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card-box">
                    <div class="card-head">
                       <Header>@lang('commonField.extra.desc')</Header>
                    </div>
                    <div class="card-body ">
                        <div class="mail-list">
                               <div class="compose-mail">
                                   <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <select name="visit_types_id" class="form-control" title="select title" required>
                                              <option value=""> select fieldvisit title </option>
                                              @if(count($fieldVisitId) > 0)
                                                 @foreach($fieldVisitId as $fieldVisit)
                                                     <option value="{{ $fieldVisit->id }}" {{ isset($data) && $data->visit_types_id == $fieldVisit->id ? 'selected' : ''}}>
                                                           {{ $fieldVisit->nameNep }} | {{ $fieldVisit->nameEng }}
                                                     </option>
                                                 @endforeach
                                              @endif
                                         </select>
                                       <label class = "mdl-textfield__label" for = "text7">@lang('commonField.extra.desc')</label>
                                   </div>

                                   <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                      {{ Form::textarea('shortDesc', null, ['class'=>'summernote mdl-textfield__input', 'placeholder'=>'short description', 'rows' => '3', 'required'=>true]) }}
                                       <label class = "mdl-textfield__label" for = "text7">@lang('commonField.extra.desc')</label>
                                   </div>
                               </div>
                           </div>
                    </div>
                </div>
        </div>
      </div>
</div>