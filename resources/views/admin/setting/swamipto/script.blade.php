<script type="text/javascript">
$(function(){
	/*add buttoin*/
	$("form[name='frmeditswamipto']").validate({
		rules:{
			nameEng : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			nameEng : "The name of swamipta is required"
		}
	});
});
$(function(){
	/*add buttoin*/
	$("form[name='frmswamipto']").validate({
		rules:{

			nameEng : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {

			nameEng : "The name of swamipta is required"
		}
	});
	  var btnAddSwamipto = $('.btnAddSwamipto');
	  btnAddSwamipto.on('click', function(e){
	    $('#swamiptoModel').modal('show'); 
	});

	  var btnEditswamipto = $('.btnEditswamipto');
	  btnEditswamipto.on('click', function(e) {
	  
	      e.preventDefault();
	      var url = $(this).data('url');
	      $.ajax({
	       'type': 'GET',
	       'url': url,
	       success: function (response) {
	          $('.swamiptoId').val(response.id);
	          $('.txtRefCode').val(response.refCode).attr('disabled',true);
	          $('.txtNameNep').val(response.nameNep);
	          $('.txtNameEng').val(response.nameEng);
	          $('.txttagName').val(response.tagName).change();
	          $("#editswamiptoModel").modal('show');
	      }
	  })
	  });

	  var frmUpdateswamipto = $('#frmUpdateswamipto');
	  frmUpdateswamipto.on('submit', function(e){
	      e.preventDefault();
	      var data = $(this).serialize();
	      var id   = $('.swamiptoId').val();
	      var url  = "{{URL::to('/')}}" + "/setting/swamipto/"+id+"/update";
	      $.ajax({
	          'type' : 'POST',
	          'url'  : url,
	          'data'    : data,
	          success : function(response){
	              console.log(response);
	              if (response.success==true) {
	                  $('.infoDiv').append(response.message).addClass('alert alert-success');
	              }
	              if (response.success==false) {
	                  $.each(response.message, function(key, value){
	                      $('.infoDiv').append(value).addClass('alert alert-danger');
	                  })
	              }
	          },complete:function(){
	              $("#editswamiptoModel").modal('hide');
	              setTimeout(location.reload.bind(location), 1500);
	          }
	      })
	      .fail(function (response) {
	          alert('data already exists');
	      });
	      $('#editswamiptoModel').modal('hide');
	  });
	  });

</script>