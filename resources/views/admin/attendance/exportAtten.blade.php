<h3>{{ isset($staffName) ? $staffName : '' }}</h3>
<p>Date From : {{ isset($nepStartDate) ? $nepStartDate : ''  }}</p>
<p>Date To : {{ isset($nepEndDate) ? $nepEndDate : ''  }}</p>
<table>
	<thead>
		<tr>
			<th>sn</th>
			<th>Date</th>
			<th>checkIn</th>
			<th>checkOut</th>
			<th>total working hour</th>
			<th>status</th>
		</tr>
	</thead>
	<tbody>
	    @if(isset($attendance) && $attendance > 0)
           <?php
               $startDate  = new DateTime($startDate);  //nepStartDate
               $endDate    = new DateTime($endDate);    //nepEndDate
               $nepDate    = new DateTime($nepStartDate);
               $count = 1;
               for ($i=$startDate; $i <=$endDate; $i->modify('+1 day')) { 
                  $sysDate    = $i->format("Y-m-d");
                  $myDay      = date('D',strtotime($i->format("Y-m-d")));
                  $attenTime  = checkAttendance($attendance, $sysDate);
                  $myNepDate    = $nepDate->modify('+1 day');
                  $displayNepDate = $myNepDate->format("Y-m-d"); 
                 ?>
                 <tr>
                   <td>{{ $count ++ }}</td>
                   <td>
                      {{ $myDay }} --
                      {{ $displayNepDate }} ---  &nbsp;
                      {{ $sysDate }}
                   </td>
                   <td>
                      {{ isset($attenTime) ? $attenTime['minTime'] : '' }}
                   </td>
                   <td>
                     {{ isset($attenTime) ? $attenTime['maxTime'] : '' }}
                    </td>

                   <td>
                     {{ isset($attenTime) ? round($attenTime['totalWorking'], 2) . 'HRS' : '' }} 
                   </td>
                   <td>
                     @if($attenTime)
                       <button class="btn btn-success btn-sm">
                         @lang('commonField.personal_information.present')
                       </button>
                        @else
                        <button class="btn btn-danger btn-sm">
                          @if($myDay == 'Sat')
                              <label>saturday</label>
                              @else
                              @lang('commonField.personal_information.absent')
                          @endif
                        </button>
                     @endif
                   </td>
                 </tr>
                <?php 
              } 
           ?>
       @endif
	</tbody>
</table>
