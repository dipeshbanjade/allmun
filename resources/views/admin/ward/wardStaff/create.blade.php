@extends('main.app')
@section('page_title')
@section('page_description')
@section('content')
<div class="col-sm-12 col-md-12">
	<div class="card-box">
		<div class="card-body">
		      <div class="card-head">
	           <header>
	           <h3>
	              <i class="fa fa-plus"></i>
	                 <span>@lang('sidebarMenu.staff_setting.createStaff')</span>
	             </h3>
	             </header>
	         </div>

			 {{ Form::open(['route' => 'wardStaff.store', 'files' => true, 'name' => 'frmWardStaff']) }}
		          @include('admin.ward.wardStaff._form')
		          <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"><span>@lang('commonField.button.back')</span>
		          </button>
		           {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
			 {{ Form::close() }}
		</div>
	</div>	
</div>

@endsection
@section('custom_script')
   @include('admin.ward.wardStaff.script')
@endsection