<div class="right_col eng" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x eng">Verification of Annual Income Japan</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
          @include('admin.shifaris.municipalityDetail')
          
          <div class="row top-part">
            <div class="col-md-6 ps-cn">
              <p><b class="ps">प. सं.:</b>
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
                @endif
              </p>
              <p align="left" class="star">
                <b class="cn">च. नं.:</b>
                {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
              </p>
            </div>
            <div class="col-md-6">
              <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
            </div>
          </div>
          <div class="row top-part">
            <div class="col-md-12">
             <h3 align="center"><b> Subject: Annual Income Verification </b> </h3>
             {{ Form::hidden('letter_subject', 'Annual Income Verification') }}
             <h4 align="center"><b> To Whom It May Concern </b></h4>
           </div>
         </div>
         <div class="row">
          <div class="col-md-12 content-para">
            <p align="left">This is to certify that <b><select onchange="changeSelect(this)" name="title">
              <option value="Mr" {{(!empty($data) && $data->title == "Mr") ? 'selected' : ''}}>Mr</option>
              <option value="Mrs"  {{(!empty($data) && $data->title == "Mrs") ? 'selected' : ''}}>Mrs</option>
              <option value="Miss" {{(!empty($data) && $data->title == "Miss") ? 'selected' : ''}}>Miss</option>
            </select>
          </b>{{ Form::text('personName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }},<b>
            <select onchange="changeSelect(this)" name="relationWithSeeker">
              <option value="son" {{(!empty($data) && $data->relationWithSeeker == 'son')? 'selected' :''}}>son</option>
              <option value="daughter" {{(!empty($data) && $data->relationWithSeeker == 'daughter')? 'selected' :''}}>daughter</option>
            </select>
          </b> of Mr.
          {{ Form::text('fatherName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} permanent resident of <b>{{ Form::text('municipality', isset($data->municipality) ? $data->municipality : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b>, Ward No. <b>{{ Form::text('ward', Auth::user()->wards_id , ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>,<b>{{ Form::text('district', isset($data->district) ? $data->district : getChecker()->districts->districtNameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b> Nepal
          (
          <select onchange="changeSelect(this)" name="former">
            <option value="Former" {{ (!empty($data) && $data->former == 'Former') ? 'selected' : ''}}>Former</option>
            <option value="Previously designated as" {{ (!empty($data) && $data->former == 'Previously designated as') ? 'selected' : ''}}>Previously designated as</option>
          </select>
          <b>
            <!--  -->
            {{ Form::text('orgAddr', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'orgAddr']) }}
            <!--  -->
          </b><b> 
            <select onchange="changeSelect(this)" name="orgType">
              <option value="V.D.C" {{(!empty($data) && $data->orgType == "V.D.C")? 'selected' :''}}>V.D.C</option>
              <option value="Municipality" {{(!empty($data) && $data->orgType == "Municipality")? 'selected' :''}}>Municipality</option>
              <option value="Sub- Metropolitian" {{(!empty($data) && $data->orgType == "Sub- Metropolitian")? 'selected' :''}}>Sub- Metropolitian</option>
              <option value="Metropolitian" {{(!empty($data) && $data->orgType == "Metropolitian")? 'selected' :''}}> Metropolitian</option>
            </select>
          </b>, Ward No. {{ Form::text('orgWard', Auth::user()->wards_id , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} ) has been earning following annual income from different sources as given below:</p>
        </div>
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-bordered" id="annual_income_verification_table">
              <thead>
                <tr>
                  <th rowspan="2"><b>S.N.</b></th>
                  <th rowspan="2" style="text-align:center"><b>Source of Income</b></th>
                  <th colspan="3" style="text-align:center"><b>Annual Income in NRs.</b></th>
                </tr>

                <tr>
                  <th><b>Fiscal Year<br>2016/2017 AD</b></th>
                  <th><b>Fiscal Year<br>2017/2018 AD</b></th>
                  <th><b>Fiscal Year<br>2018/2019 AD</b></th>
                </tr>
              </thead>
              <tbody id="annualIncomeJapanTd">
                <?php 
                  $count= 1;
                  $japanAnnualIncomeDetail = !empty($data) ? json_decode($data->japanAnnualIncomeDetail,true) : [1];
                ?>
                @foreach ($japanAnnualIncomeDetail as $singleData)
                  <tr>
                    <td>
                      {{$count++}}
                    </td>
                    <td>
                      {{ Form::text('incomeSource[]', !empty($singleData['incomeSource']) ? $singleData['incomeSource'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} 
                    </td>
                    <td>
                      {{ Form::text('firstYear[]', !empty($singleData['firstYear']) ? $singleData['firstYear'] : NULL , ['class'=>'dashed-input-field first_year_income', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('secondYear[]', !empty($singleData['secondYear']) ? $singleData['secondYear'] : NULL , ['class'=>'dashed-input-field second_year_income', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('thirdYear[]', !empty($singleData['thirdYear']) ? $singleData['thirdYear'] : NULL , ['class'=>'dashed-input-field third_year_income', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    @if ($count == 2)
                      <td class="add-btns" style="border: 0px;">
                        <a href="" id="addNewRowInTable" type="button" class="btn btn-success"><span class="fa fa-plus"></span></a>
                      </td>
                    @else
                      <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                    @endif

                  </tr>
                  {{-- expr --}}
              </tbody>
              <tfoot>

                <tr id="total_row">
                  <td> </td>
                  <td>Total Annual Income (NRs.) </td>
                  <td>
                    {{ Form::text('totalNrsInFirst', !empty($singleData['totalNrsInFirst']) ? $singleData['totalNrsInFirst'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalNrsInFirst']) }}
                  </td>
                  <td>
                    {{ Form::text('totalNrsInSecond', !empty($singleData['totalNrsInSecond']) ? $singleData['totalNrsInSecond'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalNrsInSecond']) }}
                  </td>
                  <td>
                    {{ Form::text('totalNrsInThird', !empty($singleData['totalNrsInThird']) ? $singleData['totalNrsInThird'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalNrsInThird']) }}
                  </td>
                </tr>
                <tr>
                  <td> </td>
                  <td>Total Annual Income 
                    <select onchange="changeSelect(this)" name="currency_type" id="currency_type">
                      <option value="USD($)" {{(!empty($singleData['currency_type']) && $singleData['currency_type'] == 'USD($)' ) ? 'selected' :""}}>USD($)</option>
                      <option value="AUD($)" {{(!empty($singleData['currency_type']) && $singleData['currency_type'] == 'AUD($)' ) ? 'selected' :""}}>AUD($)</option>
                      <option value="Pound(£)" {{(!empty($singleData['currency_type']) && $singleData['currency_type'] == 'Pound(£)' ) ? 'selected' :""}}>Pound(£)</option>
                      <option value="Euro(€)" {{(!empty($singleData['currency_type']) && $singleData['currency_type'] == 'Euro(€)' ) ? 'selected' :""}}>Euro(€)</option>
                    </select>
                  </td>
                  <td>
                    {{ Form::text('totalUsdInFirst', !empty($singleData['totalUsdInFirst']) ? $singleData['totalUsdInFirst'] : NULL  , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalUsdInFirst']) }}
                  </td>
                  <td>
                    {{ Form::text('totalUsdInSecond', !empty($singleData['totalUsdInSecond']) ? $singleData['totalUsdInSecond'] : NULL  , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalUsdInSecond']) }} 
                  </td>
                  <td>
                    {{ Form::text('totalUsdInThird', !empty($singleData['totalUsdInThird']) ? $singleData['totalUsdInThird'] : NULL  , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalUsdInThird']) }}
                  </td>
                </tr>  
                @endforeach
              </tfoot>
            </table>
          </div>
        </div>
        <div class="col-md-12">
          <p> Total annual income of Fiscal Year 2018/2019 AD (NRs) = {{ Form::text('total_nrs', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalNrs']) }}
            <br> 
            Note: Today's Exchange Rate 
            {{ Form::text('ctype', 'USD($)' , ['class'=>'dashed-input-md-field', 'id' => 'ctype', 'readonly' => 'readonly']) }}
            1 = NRs. {{ Form::text('exchangeRate', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'exchange_rate']) }}  <br>
            which is equivalent to   {{ Form::text('ctype', 'USD($)' , ['class'=>'dashed-input-md-field', 'id' => 'ctype', 'readonly' => 'readonly']) }}
            {{ Form::text('totalUsd', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'total_usd']) }}
          </p>
          <p> (Source : Nepal Rastra Bank)</p>
          <div class="text-right btm-last">
            <p>
              {{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
            </p>
            <p>  
             <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">
              <option value="">@lang('commonField.extra.deginations_id')</option>
              @if(count($deginationsId) > 0)  <!-- selecting leave type -->
              @foreach($deginationsId as $deg)
              <option value="{{ $deg->id }}" {{ (!empty($data->deginations_id) && $data->deginations_id == $deg->id) ? 'selected' :''}}>
                {{ $deg->nameNep }}
              </option>
              @endforeach
              @endif
            </select>
          </p>
        </div>
        <!--views for nibedak detail -->
        <div class="clearfix"></div>
        <hr>
        @include('admin.shifaris.nibedakCommonField')

        <!-- END -->
      </div>
    </div>

  </div>
</div>
</div>
</div>
</div>