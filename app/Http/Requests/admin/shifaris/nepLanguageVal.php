<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class nepLanguageVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:nep_languages',
            'chalaniNum' => 'required|unique:nep_languages',
            'issuedDate' => 'required',
            'organizationName' => 'required',
            'organizationAddress' => 'required',
            'orgAddressSec' => 'required',
            'shifarishSubject' => 'required',
            'note' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
