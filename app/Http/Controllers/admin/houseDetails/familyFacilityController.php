<?php

namespace App\Http\Controllers\admin\houseDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\houseDetails\drinkingWater\DrinkingWater;
use App\model\admin\houseDetails\kitchen\HouseKitchen;
use App\model\admin\houseDetails\houseLight\HouseLight;
use App\model\admin\houseDetails\houseToilet\HouseToilet;
use App\model\admin\houseDetails\houseWaste\HouseWaste;
use App\model\admin\houseDetails\houseRoad\HouseRoad;
use App\model\admin\houseDetails\electronic\HouseElectronicDevices;
use App\model\mylogic;
use Auth;
use DB;
use Validator;

class familyFacilityController extends Controller
{
    public function __construct()
    {
     $this->middleware('auth');
    }
    public function insertDrinkingWater(Request $request){
      if ($request->myHouseId && $request->fm_member_id) {
         // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
         $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
         $isExistsHsNum = DrinkingWater::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
         // dd($isExistsHsNum['id']);
         if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
          // if (!$isExistsHsNum['house_details_id']) {
           DB::beginTransaction(); 
           try {
            $dk  = !$isExistsHsNum['house_details_id'] ? new DrinkingWater : DrinkingWater::findOrfail($isExistsHsNum['id']);   // get drinking water
            // $dk  = new DrinkingWater;
            $dk->createdBy         = Auth::user()->id;
            $dk->municipilities_id = $ishouseExists['municipilities_id'];
            $dk->wards_id          = $ishouseExists['wards_id'];
            $dk->house_details_id  = $ishouseExists['id'];
            $dk->sourceNam         = $request->sourceNam;

            $dk->ownershipNam      = $request->ownershipNam;
            $dk->otherOwnershipNam      = $request->otherOwnershipNam;

            $dk->whyNotsNam        = $request->whyNotsNam;
            $dk->otherWhyNotsNam        = $request->otherWhyNotsNam;

            $dk->availability      = $request->availability;
            $dk->waterQty          = $request->waterQty;
            $dk->distToSrcTime     = $request->distToSrcTime;
            $dk->status            = 0;
            $dk->softDelete        = 0;
            // $mySave = $dk->save();

            $mySave = !$isExistsHsNum['house_details_id'] ? $dk->save() : $dk->update();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'house_drinking_waters', $tblId = $dk->id);

            if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => ''])->with(['mainTab'=>'', 'helpTab' => '']);
           }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
            // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    } catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }
        // }else{
        //   return back()->withMessage(config('activityMessage.dataAlreadyExists'));
        // }
         }else{   // house checking
          return back()->withMessage(config('activityMessage.idNotFound'));
      }
      }else{    // checking hosue id pass from browser
        return back()->withMessage('House number is missing please try to insert it from browser');
    }
}

    // insert house kitchen data 
    public function insertHouseKitchen(Request $request){  // add house kitchen
      // dd($request->all());
     if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']);// houseDetails
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = HouseKitchen::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
           // if (!$isExistsHsNum['house_details_id']) {
        DB::beginTransaction(); 
        try {
              $kd  = !$isExistsHsNum['house_details_id'] ? new HouseKitchen : HouseKitchen::findOrfail($isExistsHsNum['id']);   // get drinking water

              // $kd  = new HouseKitchen;
              $kd->createdBy         = Auth::user()->id;
              $kd->municipilities_id = $ishouseExists['municipilities_id'];
              $kd->wards_id          = $ishouseExists['wards_id'];
              $kd->house_details_id  = $ishouseExists['id'];

              $kd->cookingFuelNam    = $request->cookingFuelNam;
              $kd->otherCookingFuelNam =  $request->otherCookingFuelNam;
              $kd->ovenTypeNam       = $request->ovenTypeNam;
              $kd->otherOvenTypeNam  = $request->otherOvenTypeNam;

              $kd->status            = 0;
              $kd->softDelete        = 0;
              // $mySave = $kd->save();

              $mySave = !$isExistsHsNum['house_details_id'] ? $kd->save() : $kd->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'house_kitchens', $tblId = $kd->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
              }else{
                return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
               // return back()->withMessage(config('activityMessage.unSaveMessage'));
             }
           } catch (Exception $e) {
             DB::rollback();
             return back()->withMessage(config('activityMessage.dataNotInserted'));
           }
        //  }else{
        //   return back()->withMessage(config('activityMessage.dataAlreadyExists'));
        // }
          }else{   // house checking
           return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
         return back()->withMessage('House number is missing please try to insert it from browser');
       }
     }

    // insert house LIght data 
    public function insertHouseLights(Request $request){  // add house kitchen
      // dd($request->all());
     if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = HouseLight::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
           // if (!$isExistsHsNum['house_details_id']) {
        DB::beginTransaction(); 
        try {
              $hl  = !$isExistsHsNum['house_details_id'] ? new HouseLight : HouseLight::findOrfail($isExistsHsNum['id']);   // get drinking water

              // $hl  = new HouseLight;
              $hl->createdBy         = Auth::user()->id;
              $hl->municipilities_id = $ishouseExists['municipilities_id'];
              $hl->wards_id          = $ishouseExists['wards_id'];
              $hl->house_details_id  = $ishouseExists['id'];

              $hl->lightSrc          = $request->lightSrc ?? '';
              $hl->whyNotReasonNam    = $request->whyNotReasonNam ?? '';
              $hl->otherWhyNotReasonNam = $request->otherWhyNotReasonNam ?? '';          

              $hl->status            = 0;
              $hl->softDelete        = 0;
              // $mySave = $hl->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $hl->save() : $hl->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'house_lights', $tblId = $hl->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
              }else{
                return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
               // return back()->withMessage(config('activityMessage.unSaveMessage'));
             }
           } catch (Exception $e) {
             DB::rollback();
             return back()->withMessage(config('activityMessage.dataNotInserted'));
           }
        //  }else{
        //   return back()->withMessage(config('activityMessage.dataAlreadyExists'));
        // }
          }else{   // house checking
           return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
         return back()->withMessage('House number is missing please try to insert it from browser');
       }
     }

    // insert house Toilet data 
    public function insertHouseToilet(Request $request){  // add house kitchen
      // dd($request->all());
     if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = HouseToilet::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
           // if (!$isExistsHsNum['house_details_id']) {
        DB::beginTransaction(); 
        try {
              $ht  = !$isExistsHsNum['house_details_id'] ? new HouseToilet : HouseToilet::findOrfail($isExistsHsNum['id']);   // get drinking water
              // $ht  = new HouseToilet;
              $ht->createdBy         = Auth::user()->id;
              $ht->municipilities_id = $ishouseExists['municipilities_id'];
              $ht->wards_id          = $ishouseExists['wards_id'];
              $ht->house_details_id  = $ishouseExists['id'];

              $ht->toiletTypeNam = $request->toiletTypeNam;
              $ht->haveShareToilet = $request->haveShareToilet;

              $ht->status            = 0;
              $ht->softDelete        = 0;
              // $mySave = $ht->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $ht->save() : $ht->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'house_toilets', $tblId = $ht->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
              }else{
                return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
               // return back()->withMessage(config('activityMessage.unSaveMessage'));
             }
           } catch (Exception $e) {
             DB::rollback();
             return back()->withMessage(config('activityMessage.dataNotInserted'));
           }
        //  }else{
        //   return back()->withMessage(config('activityMessage.dataAlreadyExists'));
        // }
          }else{   // house checking
           return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
         return back()->withMessage('House number is missing please try to insert it from browser');
       }
     }

    // insert house Toilet data 
    public function insertHouseWaste(Request $request){  // add house kitchen
      // dd($request->all());
     if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = HouseWaste::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        DB::beginTransaction(); 
        try {
              $hw  = !$isExistsHsNum['house_details_id'] ? new HouseWaste : HouseWaste::findOrfail($isExistsHsNum['id']);   // get drinking water

              // $hw  = new HouseWaste;
              $hw->createdBy         = Auth::user()->id;
              $hw->municipilities_id = $ishouseExists['municipilities_id'];
              $hw->wards_id          = $ishouseExists['wards_id'];
              $hw->house_details_id  = $ishouseExists['id'];

              $hw->solidWasteMntNam = $request->solidWasteMntNam;
              $hw->contWtrMntNam = $request->contWtrMntNam;
              $hw->otherSolidWasteMntNam = $request->otherSolidWasteMntNam;
              $hw->otherContWtrMntNam = $request->otherContWtrMntNam;
              $hw->status            = 0;
              $hw->softDelete        = 0;
              // $mySave = $hw->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $hw->save() : $hw->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'house_kitchens', $tblId = $hw->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
              }else{
                return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
               // return back()->withMessage(config('activityMessage.unSaveMessage'));
             }
           } catch (Exception $e) {
             DB::rollback();
             return back()->withMessage(config('activityMessage.dataNotInserted'));
           }
          }else{   // house checking
           return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
         return back()->withMessage('House number is missing please try to insert it from browser');
       }
     }

    // insert house Toilet data 
    public function insertHouseRoad(Request $request){  // add house kitchen
      // dd($request->all());
     if ($request->myHouseId && $request->fm_member_id) {
          // $ishouseExists = HouseDetails::findOrfail($request->myHouseId, ['hsNum','wards_id', 'municipilities_id', 'id']); // houseDetails
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = HouseRoad::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        DB::beginTransaction(); 
        try {
              $hr  = !$isExistsHsNum['house_details_id'] ? new HouseRoad : HouseRoad::findOrfail($isExistsHsNum['id']);   // get drinking water

              // $hr  = new HouseRoad;
              $hr->createdBy         = Auth::user()->id;
              $hr->municipilities_id = $ishouseExists['municipilities_id'];
              $hr->wards_id          = $ishouseExists['wards_id'];
              $hr->house_details_id  = $ishouseExists['id'];

              $hr->roadTypeNam = $request->roadTypeNam;
              $hr->roadWidth = $request->roadWidth;

              $hr->status            = 0;
              $hr->softDelete        = 0;
              // $mySave = $hr->save();
              $mySave = !$isExistsHsNum['house_details_id'] ? $hr->save() : $hr->update();

              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'house_roads', $tblId = $hr->id);
              if ($mySave && $log) {
                DB::commit();
                return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
              }else{
                return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
               // return back()->withMessage(config('activityMessage.unSaveMessage'));
             }
           } catch (Exception $e) {
             DB::rollback();
             return back()->withMessage(config('activityMessage.dataNotInserted'));
           }
          }else{   // house checking
           return back()->withMessage(config('activityMessage.idNotFound'));
         }
       }else{    // checking hosue id pass from browser
         return back()->withMessage('House number is missing please try to insert it from browser');
       }
     }

     //inserting electronic devices
     public function insertElectronicDevices(Request $request){   // insert electronic devices
      $deviceJson = json_encode($request->deviceName);
      if ($request->myHouseId && $request->fm_member_id) {
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = HouseElectronicDevices::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        DB::beginTransaction(); 
        try {
                 $ed  = !$isExistsHsNum['house_details_id'] ? new HouseElectronicDevices : HouseElectronicDevices::findOrfail($isExistsHsNum['id']);   // get houseElectronic devices

                 $ed->createdBy         = Auth::user()->id;
                 $ed->municipilities_id = $ishouseExists['municipilities_id'];
                 $ed->wards_id          = $ishouseExists['wards_id'];
                 $ed->house_details_id  = $ishouseExists['id'];
                 $ed->hsDevices = $deviceJson;
                 $ed->status            = 0;
                 $ed->softDelete        = 0;
                 $mySave = !$isExistsHsNum['house_details_id'] ? $ed->save() : $ed->update();

                 $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'house_electronic_devices', $tblId = $ed->id);
                 if ($mySave && $log) {
                   DB::commit();
                   return response()->json([
                       'success' => true,
                       'message' => config('activityMessage.saveMessage')
                       ]);
                   // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'', 'helpTab' => '']);
                 }else{
                  return response()->json([
                      'success' => false,
                      'message' => config('activityMessage.unSaveMessage')
                      ]);
                  // return back()->withMessage(config('activityMessage.unSaveMessage'));
                }
              } catch (Exception $e) {
               DB::rollback();
               return back()->withMessage(config('activityMessage.dataNotInserted'));
             }
            }else{   // house checking
             return back()->withMessage(config('activityMessage.idNotFound'));
           }
         }else{    // checking hosue id pass from browser
           return back()->withMessage('House number is missing please try to insert it from browser');
         }
     } //
}
