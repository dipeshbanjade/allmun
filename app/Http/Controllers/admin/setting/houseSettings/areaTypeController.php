<?php

namespace App\Http\Controllers\admin\setting\houseSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseSetting\AreaType;
use App\model\mylogic;
use App\Http\Requests\admin\setting\areaType\AreaTypeVal;
use Validator;



class areaTypeController extends Controller
{
   protected $viewPath  = 'admin.setting.areaType';
   protected $area_type = 'area_types';
   protected $prefix;

   public function __construct()
   {
    $this->middleware('auth');
    $this->prefix = config('activityMessage.areaTypePrefix');
   }
    public function index()
    {
     $page['page_title']       = 'Area Type';
    $page['page_description'] = 'list of all Area Type';

    $allAreaType = AreaType::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
    return view($this->viewPath . '.index', compact(['page', 'allAreaType', 'prefix'])); 
    }



    public function store(AreaTypeVal $request)
    {
        $prefix   = myLogic::sysRefCode($tablename = $this->area_type, $prefix = $this->prefix);
        $mySave = AreaType::create([
           'refCode' => $prefix,
           'nameNep' => $request->nameNep ?? 'N/A',
           'nameEng' => $request->nameEng,
           'status'  => 0,
           'softDelete' => 0
       ]);
         if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function edit($id)
    {
         $data = AreaType::findOrFail($id);
         if ($data) {
            return response()->json([
              'success' => true,
              'refCode' => $data->refCode,
              'id'      => $data->id,
              'nameNep' => $data->nameNep,
              'nameEng' => $data->nameEng
              ]);
        }else{
            return response()->json([
              'success' => false,
              'message' => config('activityMessage.dataNotFound')
              ]);
        }
    }

  
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
         'nameEng'  => 'required|min:4|max:50',
         'nameNep'  => 'min:4|max:50',
        ]);

        if ($validator->fails()) {
          return response()->json([
           'success' => false,
           'message' => $validator->errors()->all()
            ]);
        }
      $update          = AreaType::findOrFail($id);
      $update->nameNep = $request->nameNep ?? 'N/A';
      $update->nameEng = $request->nameEng;

      $myUpdate = $update->update();
      if ($myUpdate) {
        return response()->json([
          'success' => true,
          'message' => config('activityMessage.updateMessage')
        ]);
        }else{
            return response()->json([
              'success'  => false,
              'message'  => config('activityMessage.notUpdated')
          ]);
        }
    }

    public function destroy($id)
    {
        $del = AreaType::findOrFail($id);
        $del->softDelete = 1;
        $myDel = $del->update();
        if ($myDel) {
            return MyLogic::successChangeStatus($code = 200);
        }else{
           return MyLogic::successChangeStatus($code = 301);
        }

    }
}
