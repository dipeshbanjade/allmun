@extends('main.app')
@section('content')
<style>
.attendanceData{
    padding: 10px;
    background: #fff;
}
.table thead tr th{
    padding: 10px !important;
}
.table tbody tr td{
    padding: 10px !important;
    color: #000;
}
</style>
    <div class="container">
        <div class="card">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">@lang('commonField.employer.singleSearch')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">@lang('commonField.employer.department')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                                @lang('commonField.employer.all')
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content p-3" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <!-- <form class="form-inline" method=""> -->
                            {{ Form::open(['route' => 'displayStaffAttendance', 'name' => 'frmDisplayStaffAtten']) }}
                                <div class="form-group">
                                    <input type="checkbox" name="tag" value="withFile">
                                    <label for="tagName">with Download</label>
                                  </div>
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                        <label for="dateFrom">@lang('commonField.extra.startDate')</label>
                                        <input class="form-control" id="nepDate" name="startDateNep" required>
                                        <input type="hidden" name="startDate" id="lvEng">
                                    </div>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                        <label for="dateTo">@lang('commonField.extra.endDate')</label>
                                        <input class="form-control" id="nepDate2" name="endDateNep" required>
                                        <input type="hidden" name="endDate" id="lvEng2">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <label for="staffName">@lang('commonField.extra.staff')</label>
                                        <select class="form-control" title="select staff name" name="pounchId" required><!-- sysSelect2 -->
                                            <option value="">@lang('commonField.extra.staff')</option>
                                            @if($staff)
                                                @foreach($staff as $sd)
                                                    <option value="{{ $sd->pounchId }}">
                                                         {{ $lang == 'np' ? $sd->firstNameNep .''. $sd->middleNameNep .' '. $sd->lastNameNep : $sd->firstNameEng .''. $sd->middleNameEng .' '. $sd->lastNameEng     }} 
                                                         @if($lang == 'np')
                                                          <small>
                                                             {{ $sd->firstNameEng .''. $sd->middleNameEng .' '. $sd->lastNameEng  }}    
                                                          </small>
                                                          @endif
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        
                                    </div>
                                       
                                    <div class="form-group col-lg-2 col-md-2 col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Search</button>
                                    </div>            
                                </div>
                                <div class="row">
                                  
                                </div>
                            {{ Form::close() }}
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <!-- <form class="form-inline">
                                <div class="row">
                                    <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                        <label for="dateFrom">Date From</label>
                                        <input type="date" class="form-control" id="dateFrom">
                                    </div>
                                    <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                        <label for="dateTo">Date To</label>
                                        <input type="date" class="form-control" id="dateTo">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <label for="departmentName">Department Name</label>
                                           <select name="departments_id" class="form-control">
                                                 <option value="">@lang('commonField.extra.department')</option>
                                                  @if($dept)
                                                      @foreach($dept as $dpt)
                                                          <option value="{{ $dpt->id }}">
                                                              {{ $lang == 'np' ? $dpt->nameNep : $dpt->nameEng }}
                                                          </option>
                                                      @endforeach
                                                  @endif
                                           </select>
                                    </div>
                                    <div class="form-group col-lg-2 col-md-2 col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Search</button>
                                    </div>            
                                </div>
                            </form> -->
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                           <!--  <form class="form-inline">
                                <div class="row">
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <label for="dateFrom">Date From</label>
                                        <input type="date" class="form-control" id="dateFrom">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <label for="dateTo">Date To</label>
                                        <input type="date" class="form-control" id="dateTo">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Search</button>
                                    </div>            
                                </div>
                            </form> -->
                        </div>
                    </div>
                </div>
                <!--  -->
        <div class="attendanceData">
           @if(isset($attendance) && count($attendance) > 1)
                {{ Form::open(['route' => 'downloadAtten', 'name' => 'download']) }}
                    {{ Form::hidden('startDate', null, ['id'=>'hidStartDate']) }}
                    {{ Form::hidden('endDate', null, ['id'=>'hidEndDate']) }}
                    {{ Form::hidden('pounchId', null, ['id'=>'hidPounchId']) }}
                    <button class="btn btn-success btn-sm pull-right" type="submit">
                    <i class="fa fa-calendar">Export</i></button>
                {{ Form::close() }}
            @endif

           <p>
               @if(isset($attendance))
               <label class="alert alert-success col-sm-12">
                    <i class="fa fa-calendar">
                        {{ isset($attendance) ? count($attendance) : 'no record' }} days<br>
                        {{ isset($nepStartDate) ? $nepStartDate : '' }} -- {{ isset($nepEndDate) ? $nepEndDate : '' }}<br>
                        {{ isset($staffNameLan) ? $staffNameLan : 'no record' }}
                    </i>
               </label>
               @else
               <p align="center" class="alert alert-danger">
                   Oops data not found
               </p>
               @endif
           </p>
            <table class="table table-striped">
                <thead style="background: #6673FC">
                    <tr>
                        <th scope="col" width="10%">@lang('commonField.extra.sn')</th>
                        <th scope="col">@lang('commonField.extra.day')</th>
                        <th scope="col">@lang('commonField.employer.checkIn')</th>
                        <th scope="col">@lang('commonField.employer.checkOut')</th>
                        <th scope="col">@lang('commonField.employer.workingHour')</th>
                        <th>@lang('commonField.extra.status')</th>
                    </tr>
                </thead>
                <tbody>
                          @if(isset($attendance) && $attendance > 0)
                               <?php
                                   $startDate  = new DateTime($startDate);  //nepStartDate
                                   $endDate    = new DateTime($endDate);    //nepEndDate
                                   $nepDate    = new DateTime($nepStartDate);
                                   $count = 1;
                                   for ($i=$startDate; $i <=$endDate; $i->modify('+1 day')) { 
                                      $sysDate    = $i->format("Y-m-d");
                                      $myDay      = date('D',strtotime($i->format("Y-m-d")));
                                      $attenTime  = checkAttendance($attendance, $sysDate);
                                      $myNepDate    = $nepDate->modify('+1 day');
                                      $displayNepDate = $myNepDate->format("Y-m-d"); 
                                     ?>
                                     <tr>
                                       <td>{{ $count ++ }}</td>
                                       <td>
                                          {{ $myDay }} --
                                          {{ $displayNepDate }} ---  &nbsp;
                                          {{ $sysDate }}
                                       </td>
                                       <td>
                                          {{ isset($attenTime) ? $attenTime['minTime'] : '' }}
                                       </td>
                                       <td>
                                         {{ isset($attenTime) ? $attenTime['maxTime'] : '' }}
                                        </td>

                                       <td>
                                         {{ isset($attenTime) ? round($attenTime['totalWorking'], 2) . 'HRS' : '' }} 
                                       </td>
                                       <td>
                                         @if($attenTime)
                                           <button class="btn btn-success btn-sm">
                                             @lang('commonField.personal_information.present')
                                           </button>
                                            @else
                                            <button class="btn btn-danger btn-sm">
                                              @if($myDay == 'Sat')
                                                  <label>saturday</label>
                                                  @else
                                                  @lang('commonField.personal_information.absent')
                                              @endif
                                            </button>
                                         @endif
                                       </td>
                                     </tr>
                                    <?php 
                                  } 
                               ?>
                           @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('custom_script')

@endsection