<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunStaffDailyReportsTable extends Migration
{
    public function up()
    {
        Schema::create('mun_staff_daily_reports', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('sub');
            $table->date('date');
            $table->string('startTime');
            $table->string('endTime');
            $table->text('desc');
            $table->integer('staff_municipilities_id')->unsigned();
            $table->foreign('staff_municipilities_id')->references('id')->on('staff_municipilities')->onDelete('cascade');

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('mun_staff_daily_reports');
    }
}
