<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १.बाल विकास केन्द्र वा पूर्व प्रा.वि. (किन्डर गार्डेन, मन्टेश्वरी) जाने ३ देखि ४ बर्षसम्मका बालबालिकाको संख्या
                    </p>
                </div>
                <br>
                <table border="1">
                    <thead>
                        <tr>
                            <th>बालकको संख्या</th>
                            <th>बालिकाको संख्या</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        <tr>   
                            <td>
                                {{ Form::text('maleChildNum', isset($hsCMData) && isset($hsCMData->maleChildNum) ? $hsCMData->maleChildNum : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text('femaleChildNum', isset($hsCMData) && isset($hsCMData->femaleChildNum) ? $hsCMData->femaleChildNum : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                        </tr>                        
                    </tbody>
                </table>
                <br>
            </div> 
        </div>
            <div class="msgDisplay"></div>
        
    </div>
</div>