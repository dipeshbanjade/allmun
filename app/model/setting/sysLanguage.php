<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class sysLanguage extends Model
{
    protected $table = 'sys_languages';
    protected $fillable = ['refCode','langEng','langNep','status','softDelete'];
}
