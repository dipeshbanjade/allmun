<div class="row top-part">
	<div class="col-md-6">
		<p align="left">
			<b class="ps">प. सं.:
				@if(isset($data->refCode))
				{{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
				@else
				{{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
				@endif
			</b>
		</p>
		<p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNum']) }}</b></p>
	</div>
	<div class="col-md-6">
		<p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<p align="center" class="font-size-24">
		</p><h4 align="center"><b>विषय: प्रमाणित सिफारिस गरिएको बारे।</b> </h4>
		{{ Form::hidden('letter_subject', 'प्रमाणित सिफारिस गरिएको बारे') }}
		<p></p>
	</div>
</div>
<div class="row title-left">
	<div class="col-md-12">
		<p>श्री 
			{{ Form::text('officeChairman', 'घरेलु तथा साना उद्योग कार्यालय', ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
		</p>
		<p> {{ Form::text('officeAddress', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}।</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12 content-para">
		<p align="justify">
			उपरोक्त सम्बन्धमा तहाँ कार्यालयको मिति {{ Form::text('receivedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'receivedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("receivedDate")']) }} च.नं. {{ Form::text('receivedNum', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} को प्राप्त पत्र अनुसार सम्बन्धित स्थानमा गई गाउँ सर्जमिन बुझी मुचुल्का यसै पत्र साथ राखी पठाएको र निज निवेदक {{ Form::text('estPerson', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} को नाममा दर्ता रहेको साविक {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} हाल {{ Form::text('address', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} को जग्गा विवरण {{ Form::text('areaDetails', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} को जग्गामा {{ Form::text('objective', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} उदेश्यको  श्री {{ Form::text('occupationName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} संचालनका लागि दर्ता हुने फर्मले चौतर्फी सधियार र छरछिमेकका  साथसाथै सार्बजनिक महत्वको स्थान लगायत कुनै कुराहरुलाई असर नगर्ने भएको हुँदा निवेदकको फर्म नियम अनुसार दर्ता गरी दिनुहुन किटानी सिफारिस गरिएको व्यहोरा अनुरोध छ।
		</p> 
	</div>
	<div class="col-md-12">
	</div>
	<div class="col-md-12">
		<div class="text-right btm-last">
			<p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
			<p> <b>
				<select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
					@if(count($deginationsId) > 0)
					@foreach($deginationsId as $deg)
					<option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
						{{ $deg->nameNep }} 
					</option>
					@endforeach
					@endif
				</select>
			</b></p>
		</div>
		<!--views for nibedak detail -->
		<div class="clearfix"></div>
		<hr>
		@include('admin.shifaris.nibedakCommonField')
		<!-- END -->
	</div>
	<div class="col-md-12">
		<hr>
	</div>
</div>
</div>
</div>