<?php

namespace App\Http\Controllers\admin\ward;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\ward\WardDetails;
use App\model\admin\ward\WardStaff;

use App\model\admin\municipility\MunicipilityDetails;

use App\model\mylogic;
use App\Http\Requests\admin\ward\wardDetailsValidation;
use App\model\admin\acl\Role;
use Validator;
use Auth;
use DB;


class wardDetailsController extends Controller
{
    protected $viewPath     = 'admin.ward.wardDetails';
    protected $provincesTbl = 'provinces';
    protected $districtTbl  = 'districts';
    protected $wardTbl      = 'wards';
    protected $prefix;
    protected $wardLogo;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.wardPrefix');
        $this->wardLogo    = config('activityMessage.wardLogo');
    }

    public function index()
    {
        $page['page_title']       = 'Wards';
        $page['page_description'] = 'Wards Details';

        //TODO accordkign to ward user dispaly recored
        $wardDetails = WardDetails::orderBy('created_at', 'DESC')->where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

        if (Auth::user()->userLevel === 'dev'  || Auth::user()->userLevel === 'mun') {   //TODO NEEDTO CHECK THIRD TIRE
            return view($this->viewPath . '.index', compact(['page', 'wardDetails']));
        }else{
          $wardDetails = WardDetails::where(['softDelete' => 0, 'id' => Auth::user()->wards_id])->first();
          return view($this->viewPath . '.singleWard', compact(['page', 'wardDetails']));
          // return back()->withMessage(config('activityMessage.unAuthorized'));
        }
    }

    public function create()
    {
        $page['page_title']       = 'Wards : create';
        $page['page_description'] = 'Wards details';

        $provincesId = myLogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');

        $districtId = myLogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');
        if(Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun'){

          $checkCount = myLogic::getMunicipalityDetails();
          if (!count($checkCount) > 0) {
            return back()->withMessage(config('activityMessage.createMunicipilityFirst'));
          }
          return view($this->viewPath . '.create', compact(['page', 'getUniqueCode', 'provincesId', 'districtId']));

        }else{
           return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }

    public function store(wardDetailsValidation $request)
    {
       if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
             DB::beginTransaction();
             $refcode = mylogic::sysRefCode($tablename = $this->wardTbl, $prefix = $this->prefix);
             try {
                $save = new WardDetails;
                $save->uniqueCode             = mylogic::sysRefCode($tablename = $this->wardTbl, $prefix = $this->prefix);
                $save->municipilities_id      = myLogic::getMunicipalityDetails()['id'];
                // $save->identifier             = mylogic::getRandNumber();
                $save->nameNep                = $request->nameNep;
                $save->nameEng                = $request->nameEng;
                $save->landLineNumber         = $request->landLineNumber;
                $save->faxNumber              = $request->faxNumber ?? 'N/A';
                $save->addrNep                = $request->addrNep;
                $save->addrEng                = $request->addrEng;
                $save->provinces_id           = $request->provinces_id;
                $save->districts_id           = $request->districts_id;
                $save->muncipilityHead         = $request->muncipilityHead ?? 'N/A';
                $save->bioDeviceId            = $request->bioDeviceId;

                 if ($request->hasFile('wardLogo')) {
                  $logoWidth  = config('activityMessage.logoWidth');   // logo width
                  $logoHeight = config('activityMessage.logoHeight');  // logo Height

                   $imgFile = $request->file('wardLogo');
                   $filename = str_replace(' ', '', $request->nameEng). '.' . $imgFile->getClientOriginalExtension();

                   $imgPath = mylogic::cropImage($imgFile, $logoWidth, $logoHeight, $this->wardLogo, $filename);
                   $save->wrdLogo =  $this->wardLogo . $filename;
                }
                $mySave = $save->save();

                $idEncription  = myLogic::idEncription($save->id);
                  
                $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.wardCreated'), $tableName = $this->wardTbl, $tblId = $save->id);

                $save->identifier  = $idEncription;
                $save->save();  // making encription ID

                if ($mySave && $log) {
                     DB::commit();
                    return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
                }else{
                    return back()->withMessage(config('activityMessage.unSaveMessage'));
                }
             }
             catch (Exception $e) {
               DB::rollback();
               return back()->withMessage(config('activityMessage.dataNotInserted'));
             }
           }
    }

    public function edit($slug)
    {
        $page['page_title']       = 'Wards :edit Details';   //TODO change lana
        $page['page_description'] = 'Wards Details';

        if (!$slug) {
           return back()->withMessage(config('activityMessage.idNotFound'));
        }

        $provincesId = myLogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
        $districtId = myLogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');

        $data = WardDetails::where(['identifier'=>$slug])->first();

        return view($this->viewPath . '.edit', compact(['page', 'data', 'provincesId', 'districtId']));
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
           'nameNep'        => 'required|min:3|max:60',
           'nameEng'        => 'required|min:3|max:60',
           'landLineNumber'  => 'required|min:7|max:10',
           'provinces_id'    => 'required',
            'districts_id'   => 'required'
         ]);

        if ($validator->fails()) {
           return back()->withErrors($validator)->withInput();
        }  //validation check

        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun' || Auth::user()->userLevel === 'wrd') {
              DB::beginTransaction();
              try {
                 $update = WardDetails::findOrFail($id);
                 $update->nameNep  = $request->nameNep;
                 $update->nameEng  = $request->nameEng;
                 $update->landLineNumber  = $request->landLineNumber;
                 $update->faxNumber  = $request->faxNumber ?? 'N/A';
                 $update->addrNep  = $request->addrNep;
                 $update->addrEng  = $request->addrEng;
                 $update->provinces_id  = $request->provinces_id;
                 $update->districts_id  = $request->districts_id;
                 $update->muncipilityHead  = $request->muncipilityHead ?? 'N/A';
                 $update->bioDeviceId            = $request->bioDeviceId;


                  if ($request->hasFile('wardLogo')) {
                    $imgFile = $request->file('wardLogo');
                    $filename = str_replace(' ', '', $request->nameEng). '.' . $imgFile->getClientOriginalExtension();

                    $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->wardLogo, $filename);
                    $update->wrdLogo =  $this->wardLogo.$filename;
                 }

                 $myUpdate = $update->update();

                 $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Ward Details', $tableName = $this->wardTbl, $tblId = $update->id);

                 if ($myUpdate && $log) {
                     DB::commit();
                     return redirect()->route('wardDetails.index')->withMessage(config('activityMessage.updateMessage'));
                     // return back()->withMessage(config('activityMessage.updateMessage'));
                 }else{
                     return back()->withMessage(config('activityMessage.notUpdated'));
                 }

              }catch (Exception $e) {
               DB::rollback();
               return back()->withMessage(config('activityMessage.dataNotInserted'));
             }
           }
    }

    public function destroy($id)
    {
       $delId = WardDetails::findOrFail($id);
       $delId->softDelete = 1;
          $myDel = $delId->update();
       if ($myDel) {
           return mylogic::successDeleteMessage($code = 200);
        }else{
             return mylogic::unSuccessDeleteMessage($code = 401);
        }
    }

    public function changeStatus(){
        $id  = $request->id;
        $status = $request->status==1 ? 0 : 1;

       $changeStatus = myLogic::sysChangeStatus($tablename = $this->wardTbl , $Id = $id, $status = $status);

       if ($changeStatus) {
           return back()->withMessage(config('a'));
       }
    }

    protected function usrDetails(){
      $usr = myLogic::getUserDetails();
      return $usr;
    }

    public function wardAdmin(){
      $page['page_title']       = 'List of ward admin';   //TODO change lana
      $page['page_description'] = 'all ward admin';

      $checkCount = myLogic::getMunicipalityDetails();
      if (!$checkCount > 0) {
          return back()->withMessage(config('activityMessage.createMunicipilityFirst'));
      }

      $wardAdminList = WardStaff::where(['isAdmin'=>1, 'softDelete' => 0])->paginate(config('activityMessage.pagination'));
      return view($this->viewPath . '.wardAdminList', compact(['page', 'wardAdminList']));
    }

    public function getAllWardDetails($slug){
      $page['page_title']        ='Ward details';
      $page['page_description']  ='view all ward details';
      $wardDetails  = WardDetails::where(['status'=> 0, 'softDelete'=> 0, 'identifier'=>$slug])->first();

      $totalHouse   = mylogic::houseCount($wardId = $wardDetails->id);
      $totalCitizen = mylogic::getTotalCitizenNo($wardId = $wardDetails->id);
      $wardStaffNo    = mylogic::getWardStaffNo($wardDetails->id);

      return view($this->viewPath . '.viewAllWardDetails', compact(['wardDetails', 'page', 'totalHouse', 'totalCitizen', 'wardStaffNo']));
    }

    /*------------------*/
}
