<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaritialStatusesTable extends Migration
{
    public function up()
    {
        Schema::create('maritial_statuses', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('refCode',100)->unique();
            $table->string('nameNep')->nullable();
            $table->string('nameEng')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0); 
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('maritial_statuses');
    }
}
