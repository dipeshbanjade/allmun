<?php

namespace App\Http\Controllers\admin\shifaris\defineRelation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\MritakHakdarShifaris;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\MritakHakdarShifarisVal;
use DB;
use Auth;

use App\model\mylogic;

class mritakHakdarShifarisController extends Controller
{
    protected $viewPath = 'admin.shifaris.defineRelation.mritakHakdarShifaris';
    protected $shifarisTableName     = 'mritak_hakdar_shifaris';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixMritakHakdarShifaris');

    }

    public function index()
    {
        $page['page_title']       = 'Mritak Hakdar Shifaris';
        $page['page_description'] = 'Mritak Hakdar Shifaris';

        
        $instRecomd = MritakHakdarShifaris::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        
    }

    
    public function create()
    {
        $page['page_title']       = 'Mritak Hakdar Shifaris : create';
        $page['page_description'] = 'Mritak Hakdar Shifaris details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(MritakHakdarShifarisVal $request)
    {
        //

        DB::beginTransaction();
        try {
            $dataCount = count($request->rightfulHolderName);

            $rightfulHolderName = $request->rightfulHolderName;
            $relation = $request->relation;
            $sonHusbandName = $request->sonHusbandName;
            $nagarikata = $request->nagarikata;
            $houseNo = $request->houseNo;
            $kittaNo = $request->kittaNo;
            $streetName = $request->streetName;

            $dataArray = [];

            for ($i=0; $i < $dataCount; $i++) {
                array_push($dataArray, [
                    'rightfulHolderName' => $rightfulHolderName[$i],
                    'relation' => $relation[$i],
                    'sonHusbandName' => $sonHusbandName[$i],
                    'nagarikata' => $nagarikata[$i],
                    'houseNo' => $houseNo[$i],
                    'kittaNo' => $kittaNo[$i],
                    'streetName' => $streetName[$i],
                ]);
            }
            $dataJson = json_encode($dataArray);


                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

            $isShifarisExist = isset($request->shifarisId) ? MritakHakdarShifaris::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? MritakHakdarShifaris::findOrfail($isShifarisExist['id']) : new MritakHakdarShifaris;

            $save->createdBy = Auth::user()->id;

            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";

            $save->issuedDate  = $request->issuedDate;            
            $save->chalaniNumber = $request->chalaniNumber;
            $save->municipalityName = $request->municipalityName;
            $save->wardNumber = $request->wardNumber;
            $save->nibedakName = $request->nibedakName;
            $save->municipalityNameTwo = $request->municipalityNameTwo;
            $save->sabikAddress = $request->sabikAddress;
            $save->dateSince = $request->dateSince;
            $save->registrationNumber = $request->registrationNumber;
            $save->deadPerson = $request->deadPerson;
            $save->rightfulHolderNumber = $request->rightfulHolderNumber;

            $save->tableData = $dataJson;                

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;
            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
            $request->chalaniNum = $request->chalaniNumber;
            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
             DB::commit();
             return response()->json([
                 'success' => true,
                 'message' => config('activityMessage.saveMessage')
             ]);
                   // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
         }else{
             return response()->json([
                 'success' => false,
                 'message' => config('activityMessage.unSaveMessage')
             ]);
                   // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     }catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
    }


}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
