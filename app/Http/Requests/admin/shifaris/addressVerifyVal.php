<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class addressVerifyVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:address_verifies',
            'issuedDate' => 'required',
            'prevVdcName' => 'required',
            'prevWardNo' => 'required', 
            'wardNo' => 'required|max:3',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
