<script type="text/javascript">
	var addRowBtn = $('#addNewRowInTable');
	var clickCount = 1;
	

	addRowBtn.on('click', function(e){

		e.preventDefault();

		clickCount++;

		var no = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="" value="'+clickCount+'">';

		var name = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="nameCaste[]">';

		var relation = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="relation[]">';

		var citizenBirthNo = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="citizenBirthNo[]">';

		var houseNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="houseNo[]">';

		var streetName = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="streetName[]">';

		var age = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="age[]">';
		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertNewRow = "<tr class='tblRow'>"
		+"<td>"+no+"</td>"
		+"<td>"+name+"</td>"
		+"<td>"+relation+"</td>"
		+"<td>"+citizenBirthNo+"</td>"
		+"<td>"+houseNo+"</td>"
		+"<td>"+streetName+"</td>"
		+"<td>"+age+"</td>"		
		+"<td class='add-btns' style='border: 0px;'>"+cancelButton+"</td>"

		+"</tr>";

		$('#tableInternalMigration').append(insertNewRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

		addListener();
	});
	$(function(){
		/*add buttoin*/

		$("form[name='formInternalMigration']").validate({
			rules:{
				issuedDate : {
					required: true,
					date: true
				},

				chalaniNumber : {
					required: true,
					minlength:1,
					maxlength:30
				},

				accountHolderName : {
					required: true,
					minlength: 3,
					maxlength: 255
				},

				accountHolderAddress : {
					required: true,
					minlength: 3,
					maxlength: 255
				},

				applicant : {
					required: true,
					minlength: 3,
					maxlength: 255
				},

				moveDate : {
					required: true,
					date: true
				},

				district : {
					required: true,
					minlength: 3,
					maxlength: 255
				},

				municipalityName : {
					required: true,
					minlength: 3,
					maxlength: 255
				},

				wardFrom : {
					required: true,
					minlength:1,
					maxlength:3,
					number: true
				},

				municipalityName2 : {
					required: true,
					minlength: 3,
					maxlength: 255
				},

				ward2 : {
					required: true,
					minlength:1,
					maxlength:3,
					number: true			
				},

				address : {
					required: true,
					minlength: 3,
					maxlength: 255
				}

			},
			messages: {

				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani number is required ",
				accountHolderName : "Account holder name is required ",
				accountHolderAddress : "Account holder address is required ",
				applicant : "Applicant name is required ",
				moveDate : "Date is required ",
				district : "District name is required ",
				municipalityName : "Muncipality name is required ",
				wardFrom : "Ward number is required ",
				municipalityName2 : "Munclipality name is required ",
				ward2 : "Ward number is required ",
				address : "Address is required "
			}
		});
	});
</script>