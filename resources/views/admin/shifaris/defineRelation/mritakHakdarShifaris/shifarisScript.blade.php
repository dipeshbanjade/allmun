<script type="text/javascript">

	var addButton = $('#btnAddTableRow');
	var clickCount = 1;

	addButton.on('click', function(e){
		e.preventDefault();
		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var rightfulHolderName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="rightfulHolderName[]">';

		var relation = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="relation[]">';

		var sonHusbandName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="sonHusbandName[]">';

		var nagarikata = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="nagarikata[]">';

		var houseNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="houseNo[]">';

		var kittaNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="kittaNo[]">';

		var streetName = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="streetName[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+rightfulHolderName+"</td>"
		+"<td>"+relation+"</td>"
		+"<td>"+sonHusbandName+"</td>"
		+"<td>"+nagarikata+"</td>"
		+"<td>"+houseNo+"</td>"
		+"<td>"+kittaNo+"</td>"
		+"<td>"+streetName+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#mritakTable').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmMritakHakdarShifaris']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 10
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number: true
				},
				nibedakName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				sabikAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				municipalityNameTwo : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				dateSince : {
					required : true,
					date: true
				},
				registrationNumber : {
					required : true,
					minlength : 1,
					maxlength : 255
				},
				deadPerson : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				rightfulHolderNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
			},
			messages: {
				refCode : "Reference Code is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number must be number",
				nibedakName : "Applicant Name must be number",
				municipalityName : "Municipality is required",
				wardNumber : "Ward Number must be number",
				sabikAddress : "Municipality is required",
				municipalityNameTwo : "Municipality Name is required",
				dateSince : "Date is required",
				registrationNumber : "Registration Number is required",
				deadPerson : "Dead Person Name is required",
				rightfulHolderNumber : "Rightful Holder Number is required",
				authorizedPerson : "Authorized Person is required"
			}
		});
	});
	
	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='nibedakName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>