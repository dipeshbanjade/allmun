<?php

namespace App\Http\Requests\admin\setting\familyType;

use Illuminate\Foundation\Http\FormRequest;

class FamilyTypeValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'nameEng' => 'required|min:3|max:30'
        ];
    }
}
