<div class="right_col eng" role="main" style="min-height: 521px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x eng">Open Format</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

          @include('admin.shifaris.municipalityDetail')

          <div class="row top-part">
            <div class="col-md-6">
              <p align="left"><b>Ref No. :
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
                @endif
              </b></p>
              <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNum']) }}</b></p>
            </div>
            <div class="col-md-6">
              <p align="right"><b>Date :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b> </p>
            </div>
          </div>
          <div class="row title-left">
           <div class="col-md-12">
            <p align="left"> {{ Form::text('nameOfOffice', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'nameOfOffice', 'title' => 'Office Name']) }}, </p>
            <p align="left">{{ Form::text('officeAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'officeAddress', 'title' => 'Office Address']) }}</p>
            <p align="left">{{ Form::text('officeAddress2', null, ['class'=>'dashed-input-field','title' => 'Office Address']) }}|</p>
          </div>
        </div>
        <div class="row top-part">
          <div class="col-md-12">
           <h3 align="center"><b>Subject: {{ Form::text('subject', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'subject']) }}</b></h3>
         </div>
       </div>
       <div class="row">
        <div class="col-md-12 content-para">
          {!! Form::textarea('letterDesc', null, ['class'=>'summernote', 'rows' => '10', 'cols' => '30']) !!}
        </div>
      </div>
      <div class="col-md-12">
       <h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bondage&nbsp;  </font></font></h2>
       <table class="table table-bordered" id="sabik_address_table">
        <tbody>
          <?php
            $count=1;
            $bordthaData= !empty($data)?json_decode($data->accountOpeningDetail,true):[1];
          ?>
          @foreach ($bordthaData as $singleData)
          <?php $count++ ?>
           <tr>
            <td>{{ Form::text('bodartha[]', !empty($singleData['bodartha'])? $singleData['bodartha']:NULL, ['class'=>'form-control']) }}</td>
            @if ($count == 2)
              <td class="add-btns" style="border: 0px;"><button type="button" id="addTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button></td>
            @else
              <td class="add-btns" style="border: 0px;"><button type="button" id="addTableRow" class="btn btn-danger btnRemoveItem"><span class="fa fa-plus"></span></button></td>
            @endif
          </tr>
          @endforeach
      </tbody>
    </table>
    <div class="text-right btm-last">
      <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
      <p>
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </p>
    </div>
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr>
    @include('admin.shifaris.nibedakCommonField')
    <!-- END -->
  </div>
  <div class="col-md-12">
    <hr>
  </div>
</div>
</div>
</div>