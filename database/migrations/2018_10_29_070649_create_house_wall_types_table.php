<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseWallTypesTable extends Migration
{

    public function up()
    {
        Schema::create('house_wall_types', function (Blueprint $table) {
            $table->increments('id',100);
            $table->string('refCode',100)->unique();
            $table->string('nameEng');
            $table->string('nameNep')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_wall_types');
    }
}
