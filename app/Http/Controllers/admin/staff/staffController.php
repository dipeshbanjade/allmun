<?php

namespace App\Http\Controllers\admin\staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\municipility\StaffMunicipility;
use App\model\admin\ward\WardStaff;
use App\model\myLogic;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use DB;
use Auth;
class staffController extends Controller
{
    protected $viewPath = 'admin.staff';
    protected $usrTbl   = 'users';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getMyProfile(){  // getting individual staff profile
    $page['page_title'] = 'Profile';
    $page['page_description']  = 'view my profile profile';

    $profile = myLogic::getUserDetails();
    $checkUserLevel = $profile->userLevel;
        switch ($profile) {
            case $profile->userLevel === 'dev':
                return redirect()->route('home')->withMessage('your are developer');
                break;
                
            case  $profile->userLevel === 'mun':
                   $data = StaffMunicipility::findOrFail($profile['staffs_id']);
                   $data->roles_id = $profile->roles_id;
                   return view($this->viewPath . '.profile', compact(['data', 'page', 'profile']));
                   break;  

            case  $profile->userLevel === 'wrd':
                   $data = WardStaff::findOrFail($profile['staffs_id']);
                   $data->roles_id = $profile->roles_id;
                   return view($this->viewPath . '.profile', compact(['data', 'page', 'profile']));
                   break;  
            default:
                return redirect()->route('home')->withMessage('Oops user level is not found');
                break;
        }
    }

    public function updateMyProfile(Request $request){

    }

    public function storePassword(Request $request){
       /* $validatedData = $request->validate([
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
        ]);*/
        $dbuser     = myLogic::getUserDetails();
        $inboxPwd  = Hash::make($request->oldPassword);

        if(Hash::check($request->oldPassword, $dbuser['password'])) {
        DB::beginTransaction();
        try {
            $usr = User::findOrFail($dbuser['id']);
            $usr->password = Hash::make($request->password);
            $usr->isPasswordChange = 1;
            $changePwd = $usr->update();

            $log = myLogic::insertInLogFile($userId = $dbuser['id'], $action = config('activityMessage.changePassword'), $tableName = $this->usrTbl, $tblId = $dbuser['id']);

            if ($changePwd && $log) {
                 DB::commit();
                 Auth::logout();
                 return redirect()->route('/')->withMessage(config('activityMessage.passwordChange'));
                // return back()->withMessage(config('activityMessage.saveMessage'));
            }else{
                return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        } catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
        }else{
            return back()->withMessage('Your old password doesnot match');
        }

    }

    public function changePassword(Request $request){
    $page['page_title'] = 'Change Password';
    $page['page_description']  = 'change staff password';
    return view($this->viewPath . '.changePassword', compact(['page']));
    }

    /*--------------------*/
    public function getMyLeaveList(){
        
    }

    /*---------------------------------------------*/
    public function adminStaffChangePwdView($slug){
       $page['page_title']        = 'Change Staff Password';
       $page['page_description']  = 'change staff password';
       
       if (!$slug) {
           return back()->withMessage(config('activityMessage.idNotFound'));
       }
       $usrDetails = User::where(['idEncrip' => $slug, 'status' => 0, 'softDelete' => 0])->first();
       // dd($usrDetails);
       
       return view($this->viewPath . '.adminstaffChangePassword', compact(['page', 'usrDetails']));
    }

    public function staffPasswordStore(Request $request){

        $request->validate([
               'password'=>'required|min:6|max:16',
               'confirmPassword'=>'same:password'
               ]);

        if (!$request->userId) {
           return back()->withMessage(config('activityMessage.idNotFound'));
        }
        try {
            DB::beginTransaction();
            $chnPwd = User::findOrFail($request->userId);
            $chnPwd->password = Hash::make($request->password);
            $chnPwd->isPasswordChange = 0;

            $upd = $chnPwd->update();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.changeStaffPassword'), $tableName = $this->usrTbl, $tblId = $chnPwd->id);

            if ($upd && $log) {
                DB::commit();
                return back()->withMessage('successfully change password '. $chnPwd->email);
            }
        } catch (Exception $e) {
           DB::rollback();
           return back()->withMessage(config('activityMessage.dataNotInserted')); 
        }
    }

    public function getStaffDetails($id){
        $page['page_title']        = 'Update staff';
        $page['page_description']  = 'Update staff';

       if (!$id) {
           return back()->withMessage(config('activityMessage.idNotFound'));
       }
    
       if (Auth::user()->userLevel == 'dev') {
           $roles = myLogic::getSysRole($roleFor = 'dev');
       }else{
          $roles = Auth::user()->userLevel =='mun' ?  myLogic::getSysRole($roleFor = 'mun') : myLogic::getSysRole($roleFor = 'wrd');
       }
       $updateUsr = User::where(['staffs_id' => $id, 'status' => 0, 'softDelete' => 0])->first();
       return view($this->viewPath . '.adminUpdateStaff', compact(['page', 'roles', 'updateUsr']));
    }

    public function updateStaffDetails(Request $request){
      if (!$request->userId) {
           return back()->withMessage(config('activityMessage.idNotFound'));
        }
      $request->validate([
             'email'=>'required|email|unique:users,email,'.$request->userId,
             'name'=>'required|min:3|max:30',
             'roles_id' => 'required',
             'userId'   => 'required'
             ]);
      try {
          DB::beginTransaction();
          $usr = User::findOrFail($request->userId);
          $usr->name = $request->name;
          $usr->email  = $request->email;
          $usr->roles_id = $request->roles_id;
          $upd = $usr->update();

          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateStaffLoginDetails'), $tableName = $this->usrTbl, $tblId = $usr->id);

          if ($upd && $log) {
              DB::commit();
              return back()->withMessage('successfully updated staff login details'. $usr->email);
          }
      } catch (Exception $e) {
         DB::rollback();
         return back()->withMessage(config('activityMessage.dataNotInserted')); 
      }
    }
}
