<script type="text/javascript">

	var addButton = $('#btnAddTableRow');
	var clickCount = 1;

	addButton.on('click', function(e){
		e.preventDefault();
		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var businessType = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="businessType[]">';

		var businessNature = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="businessNature[]">';

		var wardNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="wardNo[]">';

		var registrationNo = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="registrationNo[]">';

		var tollName = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="tollName[]">';

		var streetName = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="streetName[]">';

		var houseNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="houseNo[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+businessType+"</td>"
		+"<td>"+businessNature+"</td>"
		+"<td>"+wardNo+"</td>"
		+"<td>"+registrationNo+"</td>"
		+"<td>"+tollName+"</td>"
		+"<td>"+streetName+"</td>"
		+"<td>"+houseNo+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#business_close_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmWardbusinessRegistration']").validate({
			rules:{
				refCode: {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number: true
				},
				registrationNumber : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number: true
				},
				registrationDate : {
					required : true,
					date: true
				},
				businessFarmName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				businessOwnerName : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				houseOwnerName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				businessLocation : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				businessStreetName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				businessHouseNumber : {
					required : true,
					minlength : 3,
					maxlength : 15
				},
				businessEmail : {
					required : true,
					minlength : 5,
					maxlength : 30,
					email: true
				},
				businessPhoneNumber : {
					required : true,
					minlength : 3,
					maxlength : 15
				},
				businessPanNumber : {
					required : true,
					minlength: 5,
					maxlength: 20
				},
				organizationName : {
					required : true,
					minlength : 5,
					maxlength : 255
				},
				organizationRegisterNumber : {
					required : true,
					minlength : 1,
					maxlength : 255
				},
				organizationRegisterDate : {
					required : true,
					date: true
				},
				courtName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				refCode: "Patra Sankha is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				registrationNumber : "Registration Number is required",
				registrationDate : "Registration Date is required",
				businessFarmName : "Business Name is required",
				businessOwnerName : "Business Owner Name is required",
				houseOwnerName : "House Owner Name is required",
				businessLocation : "Business Location is required",
				businessStreetName : "Business Located street name is required",
				businessHouseNumber : "Business Located House Number is required",
				businessEmail : "Business Email is required",
				businessPhoneNumber : "Business Phone Number is required",
				businessPanNumber : "Business PAN Number is required",
				organizationName : "Organization Name is required",
				organizationRegisterDate : "Organization Registration Date is required",
				organizationRegisterNumber : "Organization Registration Number is required",
				courtName : "Law is required",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});


	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='houseOwnerName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>