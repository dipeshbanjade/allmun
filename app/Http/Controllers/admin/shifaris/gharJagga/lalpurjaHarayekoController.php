<?php

namespace App\Http\Controllers\admin\shifaris\gharJagga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\LalpurjaHarayeko;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\LalpurjaHarayekoVal;
use DB;
use Auth;

use App\model\mylogic;

class lalpurjaHarayekoController extends Controller
{

    protected $viewPath = 'admin.shifaris.gharJagga.lalpurjaHarayeko';
    protected $shifarisTableName     = 'lalpurja_harayekos';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;
    protected $refCode;

    public function __construct(){
        $this->middleware('auth');
        $this->prefix   = config('activityMessage.prefixLalpurjaHarayeko');
        $this->refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['page_title']       = 'Lalpurja Harayeko Shifarish';
        $page['page_description'] = 'Lalpurja Harayeko Shifarish';


        $instRecomd = LalpurjaHarayeko::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $page['page_title']       = 'Lalpurja Harayeko Shifarish';
        $page['page_description'] = 'Lalpurja Harayeko Shifarish';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 

        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LalpurjaHarayekoVal $request)
    {
        
        DB::beginTransaction();
        try {
            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $this->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


            $isShifarisExist = isset($request->shifarisId) ? LalpurjaHarayeko::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? LalpurjaHarayeko::findOrfail($isShifarisExist['id']) : new LalpurjaHarayeko;

            $save->createdBy = Auth::user()->id;

            $save->refCode = $request->refCode;

            $save->wards_id = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";


            $save->issuedDate = $request->issuedDate;
            $save->chalaniNumber = $request->chalaniNumber;
            $save->accountHolderName = $request->accountHolderName;
            $save->accountHolderAddress = $request->accountHolderAddress;
            $save->applicantName1 = $request->applicantName1;
            $save->municipalityName = $request->municipalityName;
            $save->wardNumber1 = $request->wardNumber1;
            $save->address1 = $request->address1;
            $save->kittaNumber = $request->kittaNumber;
            $save->area = $request->area;
            $save->wardNumber2 = $request->wardNumber2;
            $save->address2 = $request->address2;
            $save->applicantName2 = $request->applicantName2;
            $save->applicant = $request->applicant;
            $save->citizenshipNo = $request->citizenshipNo;
            $save->issuedDate2 = $request->issuedDate2;
            $save->fatherName = $request->fatherName;
            $save->grandFatherName = $request->grandFatherName;
            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;

            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
$request->chalaniNum = $request->chalaniNumber;
                        $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
