@extends('main.app')
@section('page_title')
@section('page_description')
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
	       <i class="fa fa-plus"></i>
	 </div>

	      <div class="card-head">
           <header>@lang('commonField.links.createWardAdmin')</header>
         </div>

		 
	          <button type="button" class="btn btn-danger pull-right" onclick="history.back()"><span>@lang('sidebarMenu.button.back')</span> 
              </button>
              <input class="btn btn-success pull-right" type="submit" value="सिर्जना">
 </div>
@endsection
@section('custom_script')
   @include('admin.ward.wardStaff.script')
@endsection