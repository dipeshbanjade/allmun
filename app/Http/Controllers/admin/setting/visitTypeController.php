<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\setting\VisitType;
use App\model\myLogic;
use App\Http\Requests\admin\setting\visitType\visitTypeValidation;
use Validator;

class visitTypeController extends Controller
{
       protected $viewPath = 'admin.setting.visitType';
       protected $visitTypeTbl = 'visit_types';
       protected $prefix;

       public function __construct()
       {
           $this->middleware('auth');
           $this->prefix = config('activityMessage.visitTypePrefix');
       }

       public function index()
       {
           $page['page_title']       = 'Visit Type';
           $page['page_description'] = 'list of all Visit Type';

           $allVisitType = VisitType::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));


           $prefix   = myLogic::sysRefCode($tablename = $this->visitTypeTbl, $prefix = $this->prefix);
           return view($this->viewPath . '.index', compact(['page', 'allVisitType', 'prefix']));
       }

       public function store(visitTypeValidation $request )
       {
           $mySave = VisitType::create([
                      'refCode' => $request->refCode,
                      'nameNep' => $request->nameNep ?? 'N/A',
                      'nameEng' => $request->nameEng,
                      'desc'    => $request->desc,
                      'status'  => 0,
                      'softDelete' => 0
                    ]);
           if ($mySave) {
               return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
           }else{
               return back()->withMessage(config('activityMessage.unSaveMessage'));
           }
       }

       public function edit($id)
       {

           $data = VisitType::findOrFail($id);
           if ($data) {
               return response()->json([
                       'success' => true,
                       'refCode' => $data->refCode,
                       'id'      => $data->id,
                       'nameNep' => $data->nameNep,
                       'nameEng' => $data->nameEng,
                       'desc'    => $data->desc
                      ]);
           }else{
               return response()->json([
                   'success' => false,
                   'message' => config('activityMessage.dataNotFound')
                   ]);
           }
       }

       public function update(Request $request, $id)
       {
           // $validator = Validator::make($request->all(), [
           //      'refCode'      => 'required|unique:visit_types,refCode,'.$request->id,
           //      'nameEng'  => 'required|min:4|max:20',
           //      'nameNep'  => 'min:4|max:20',
           //      'desc'      => 'required|min:6|max:6000'
           //  ]);

           // if ($validator->fails()) {
           //     return response()->json([
           //            'success' => false,
           //            'message' => $validator->errors()->all()
           //         ]);
           // }

           $update          = VisitType::findOrFail($id);
           $update->refCode = $request->refCode;
           $update->nameNep = $request->nameNep ?? 'N/A';
           $update->nameEng = $request->nameEng;
           $update->desc    = $request->desc;

           $myUpdate = $update->update();
           if ($myUpdate) {
               return response()->json([
                   'success' => true,
                   'message' => config('activityMessage.updateMessage')
                   ]);
           }else{
               return response()->json([
                   'success'  => false,
                   'message'  => config('activityMessage.notUpdated')
                   ]);
           }

       }

       public function destroy($id)
       {
           $del = VisitType::findOrFail($id);
           $del->softDelete = 1;
           $myDel = $del->update();
           if ($myDel) {
             return MyLogic::successChangeStatus($code = 200);
           }else{
              return MyLogic::successChangeStatus($code = 301);
           }
       }

       public function changeStatus(Request $request){
          $id  = $request->id;
          $status = $request->status==1 ? 0 : 1;
          $change = mylogic::sysChangeStatus($tablename = $this->visitTypeTbl, $Id = $id, $status = $status);

          if ($change) {
              return MyLogic::successChangeStatus($code = 200);
          }else{
              return MyLogic::idNotFoundMessage($code = 301);
          }
       }
}
