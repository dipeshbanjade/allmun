<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class AreaType extends Model
{
    protected $table = 'area_types';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
