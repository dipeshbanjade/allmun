@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Char Killa Pramanit <small> चार किला विवरण</small></header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>
	<div class="right_col nep" role="main" style="min-height: 882px;">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h3 class="title-x">सिफारिस सम्बन्धमा  </h3>
						<div class="clearfix"></div>
					</div>
					<div class="x_content" id="block">
						@include('admin.shifaris.municipalityDetail')
						
						{!! Form::model($data, ['method' => 'POST','route' => ['charKillaBibaran.store', $data->id], 'files'=>true, 'name' => 'frmCharKillaBibaran', 'class'=>'sifarishSubmitForm']) !!}						
						{{ Form::hidden('shifarisId',  $data->id )}}
						@include('admin.shifaris.gharJagga.charKillaBibaran._form')
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- <p class="pull-right">
					<button type="submit" class="btn btn-success">CREATE</button>
					<button type="submit" class="btn btn-active" onclick="">PRINT</button>
				</p> -->
				@include('admin.shifaris.commonButton')
				{{ Form::close() }}
				<div class="msgDisplay" style="z-index: 999999 !important"></div>

			</div>
		</div>
	</div>
</div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
@include('admin.shifaris.script')
@include('admin.shifaris.gharJagga.charKillaBibaran.shifarisScript')
@endsection
