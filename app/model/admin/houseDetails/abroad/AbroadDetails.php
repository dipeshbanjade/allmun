<?php

namespace App\model\admin\houseDetails\abroad;

use Illuminate\Database\Eloquent\Model;

class AbroadDetails extends Model
{
    protected $table = 'abroad_details';
}
