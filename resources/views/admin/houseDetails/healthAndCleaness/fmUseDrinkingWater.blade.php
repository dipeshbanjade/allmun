<style type="text/css">
table th, table td{
    border: 1px solid black;
    text-align: center;
}
</style>
<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <p>
                    १.तपाईको परिवारका सदस्यहरुले खानेपानी पिउदा प्रमुख रुपमा तलका कुन तरिका अपनाउनु हुन्छ ?
                </p>        
                <br>
                <table class="table">
                    <tr>
                        <th rowspan="2">सि.न.</th>
                        <th rowspan="2">पानीको स्रोत</th>
                        <th rowspan="2">पानीको स्रोत</th>
                        <th colspan="6">पानी पिउने तरिका ( कोड लेख्दा अंक लेख्ने जस्तै : फिल्टर गरेर भन्नेमा : १) </th>
                    </tr>
                    <tr>
                        <td>कोड</td>
                        <td>१.फिल्टर गरेर</td>
                        <td>२.उमालेर</td>
                        <td>३. पियुष प्रयोग गरेर</td>
                        <td>४. सोडिस (घाममा तताएर)</td>
                        <td>५. सिधै</td>
                    </tr>
                    <?php 
                    $waterSourceArr = [
                        'पाइप धाराको पानी ',
                        'पाइप धाराको पानी ',
                        'कुवाको पानी',
                        'मुलको पानी',
                        'जारको पानी ',
                        'अन्य'
                    ];                    
                    $fmUseDrinkingWaterDetail = isset($hsFUDW->hsFmUsDrinkWaterDetails) ? json_decode($hsFUDW->hsFmUsDrinkWaterDetails) : '';

                    for($count = 1; $count <= count($waterSourceArr); $count++ ){
                        $useDrinkingWaterSingle = moreHouseDetailsFormUpdate($count, $fmUseDrinkingWaterDetail, $compareJsonKey = 'waterSrcRef');
                        ?>
                        <tr>
                            <td>
                                {{ $count }}
                            </td>
                            <td>
                                <input type="hidden" name="hsFmUsDrinkWaterDetails[<?php echo $count;  ?>][waterSrcRef]" value="{{ $count }}">

                                <input type="checkbox" class="requireFiltrationType" name="hsFmUsDrinkWaterDetails[<?php echo $count ?>][sourceUsed]" value="1" {{ isset($useDrinkingWaterSingle['sourceUsed']) ? 'checked' : '' }} >
                            </td>
                            <td>
                                {{ $waterSourceArr[$count - 1] }}
                            </td>
                            <td>
                                {{ Form::text("hsFmUsDrinkWaterDetails[$count][code]", isset($useDrinkingWaterSingle['code']) ? $useDrinkingWaterSingle['code'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Code', 'id' => '']) }}
                            </td>
                            <td>
                                <input type="checkbox" name="hsFmUsDrinkWaterDetails[<?php echo $count ?>][isFilter]" value="1" {{ isset($useDrinkingWaterSingle['isFilter']) ? 'checked' : '' }} >                                
                            </td>
                            <td>
                                <input type="checkbox" name="hsFmUsDrinkWaterDetails[<?php echo $count ?>][isBoiled]" value="1" {{ isset($useDrinkingWaterSingle['isBoiled']) ? 'checked' : '' }} >                                
                            </td>
                            <td>
                                <input type="checkbox" name="hsFmUsDrinkWaterDetails[<?php echo $count ?>][isPiyusUsed]" value="1" {{ isset($useDrinkingWaterSingle['isPiyusUsed']) ? 'checked' : '' }} >                                
                            </td>
                            <td>
                                <input type="checkbox" name="hsFmUsDrinkWaterDetails[<?php echo $count ?>][isSunUVFilter]" value="1" {{ isset($useDrinkingWaterSingle['isSunUVFilter']) ? 'checked' : '' }} >                                
                            </td>
                            <td>
                                <input type="checkbox" name="hsFmUsDrinkWaterDetails[<?php echo $count ?>][isDirectConsumption]" value="1" {{ isset($useDrinkingWaterSingle['isDirectConsumption']) ? 'checked' : '' }} >                                
                            </td>
                        </tr>
                        <?php
                    }
                    ?>                        
                </table>
                <br>
            </div>     
            <div class="msgDisplay"></div>
        </div>
    </div>
</div>