<script type="text/javascript">

	$('#nepDate').focusout(function(){
		getdate();
	});

	function getdate() {
		var tt = document.getElementById('nepDate').value;

		var newdate = new Date(tt);

		var noOfDays = document.getElementById('leaveDays').value;

		console.log(tt + ' | New Date: ' + newdate + ' | No. Of Days: ' + noOfDays)

		newdate.setDate(newdate.getDate() + parseInt(noOfDays));

		var dd = newdate.getDate();
		var mm = newdate.getMonth() + 1;
		var y = newdate.getFullYear();

		if (mm.length < 2) mm = '0' + mm;
		if (dd.length < 2) dd = '0' + dd;

		var someFormattedDate = y + '-' + mm + '-' +dd;
		document.getElementById('nDate').value = someFormattedDate;
	}
	
	$(function(){
		     $("form[name='frmMunLeave']").validate({
		      rules:{
		       refCode : {
		         required : true,
		         minlength : 3
		       },
		       leave_types_id : {
		       	required : true
		       },
		       noOfDays : {
		         required : true,
		         number   : true
		       },
		       shortNoteEng : {
		         required : true,
		         minlength : 3,
		         maxlength :1000
		       },
		       startDate : {
		         required :true,
		         minlength : 3,
		         maxlength : 30
		       },
		       endDate : {
		         required : true,
		         minlength : 8,
		         maxlength : 30
		       }
		     },
		     messages: {
		       refCode : "refCode is required with min 3 character",
		       leave_types_id : "select leave type",
		       noOfDays : "enter number of leave",
		       startDate : "required start date",
		       endDate : "required end date"
		     }
		   });
	       /*----------------------*/
	 $('#nepDate').nepaliDatePicker({
          disableDaysBefore: '1',
          onChange:function(){
          calculateLeaveDate();
      }
    });
	 /*----------------------------*/
    });
    $('#leaveDays').on('change', function(){
    		if ($(this).val() === '') {
    			$('#nepDate').val('');
    			$('#endDate').val('');
    		}else{
    			calculateLeaveDate();
    		}
    	});

    	function calculateLeaveDate(){
    		var leaveDays=$('#leaveDays').val();
    		if(leaveDays.length > 0){
    			var frmEndDate = $('#endDate');

    			var startNepDate= BS2AD($('#nepDate').val());
    			startNepDate = new Date(startNepDate);
    			if(startNepDate >0){
    				endDate= startNepDate.setDate(startNepDate.getDate()+ parseInt(leaveDays));
    				endDate= new Date(endDate);


    				var month=endDate.getMonth()+1;
    	    	    $('#engDate').val(endDate.getFullYear()+'-'+ month +'-'+endDate.getDate()); // Hidden Fiel
    	    	    console.log("Eng Date " + $('#engDate').val());

    	    	    var nepEndDate= AD2BS($('#engDate').val());
    	    	    console.log("NepaliDate: "+ nepEndDate);

    	    	    frmEndDate.val(nepEndDate);

    	    	}
    	    }else{
    	    	$('#nepDate').val('');
    	    	frmEndDate.val('');
    	    }
    	}
    // $('#lvEng').on('change', function(){
    // 	alert(this.val());
    //    $('#myEngDate').text(this.val());
    // });
</script>