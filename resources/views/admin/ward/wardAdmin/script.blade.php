<script type="text/javascript">
	$(function(){
	     $("form[name='frmWardStaff']").validate({
	      rules:{
	      	refCode:{
	      		required: true,
	      		minlength: 3
	      	},
	       firstNameNep : {
	         minlength : 3,
	         maxlength : 30
	       },
	       firstNameEng : {
	         required : true,
	         minlength : 3,
	         maxlength : 30
	       },
	       lastNameNep : {
	         minlength : 3,
	         maxlength :30
	       },
	       lastNameEng : {
	         required :true,
	         minlength : 3,
	         maxlength : 30
	       },
	       password : {
	         required : true,
	         minlength : 8,
	         maxlength : 30
	       },
	       citizenNo : {
	         required : true,
	         minlength : 3,
	         maxlength: 15
	       },
	       joingDateNep : {
	         required : true,
	         date: true
	       },
	       wardNo : {
	         required : true,
	         number: true
	       },
	       villageNep : {
	         minlength : 3,
	         maxlength : 30
	       },
	       villageEng : {
	         required : true,
	         minlength : 3,
	         maxlength : 30
	       },
	       phoneNumber : {
	         required : true,
	         minlength : 10,
	         maxlength: 10,
	         number: true
	       },
	       // profilePic : {
	       // 	 required: true,
        //      extension: "jpg|png|jpeg"
	       // },
	       // citizenImagePath : {
	       //  required: true,
        //      extension: "jpg|png|jpeg"
	       // },
	       wards_id : {
	       	required : true
	       },
	       email:{
	       	email: true
	       },
	       departments_id:{
	       	required: true
	       },
	       roles_id:{
	       	required: true
	       },
	       staff_types_id:{
	       	required: true
	       },
	       deginations_id:{
	       	required: true
	       },
	       provinces_id:{
	       	required: true
	       },
	       districts_id:{
	       	required: true
	       },
	       dob:{
	       	required: true,
	       	date: true
	       },
	       joingDateNep:{
	       	required : true
	       }

	     },
	     messages: {
	     	refCode: "Code is required",
	       firstnameEng : "The First name is required",
	       lastNameEng : "The Last Name  is required",
	       password : "Enter a password with minimun 8 character long ",
	       citizenNo : "The Citizen No field is required",
	       joingDateNep : "Joining Date is required",
	       wardNo : "Ward Number field is required",
	       villageEng : "Village Name field is required with minimun 3 character and maximum 30 character",
	       phoneNumber : "Phone Number field is required with minimun 10 character and maximum 10 character",
	       // profilePic  : "file extension is not match",
	       // citizenImagePath : "select correct file format required",
	       wards_id : 'please select ward',
	       email: "Email is required",
	       departments_id: "Please select Department",
	       roles_id: "Please select roles",
	       staff_types_id: "Please select staff type",
	       deginations_id: "Please select deignation",
	       provinces_id: "Please select province",
	       districts_id: "Please select district",
	       dob: "Please select Date of Birth",
	       joingDateNep : 'joining Date is required'
	     }
	   });
       /*----------------------*/
	   });

</script>