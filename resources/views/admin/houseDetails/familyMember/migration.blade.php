<div class="row">
        <div class="form-group col-md-12">
                <div>
                    <strong>
                        १.तपाईको परिवार यस स्थानमा वसाई सरी आउनु भएको हो ? 
                        हो भने कहाँबाट आउनु भएको हो ?
                    </strong>
                    <div>
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" name="haveMigration" value="1" onchange="checkMigrationReason(this)" {{ isset($migrationDetails) && $migrationDetails->haveMigration == 1 ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" name="haveMigration" value="0" onchange="checkMigrationReason(this)">
                        </label> 
                    </div>
                    <br>
                    @if (isset($migrationDetails) && $migrationDetails->haveMigration==1)
                        <div class="migrationReasonDiv" style="display: block"> 
                    @else
                        <div class="migrationReasonDiv" style="display: none"> 
                    @endif
                                      
                        <p>
                          २.हो भने कहाँबाट आउनु भएको हो ?
                        </p>
                        <div>
                            <!-- <label class="radio-inline">
                                (१) कास्की जिल्ला भित्रबाट  
                                <input type="radio" name="radio" value="">
                            </label> 
                            <label class="radio-inline">
                                (२) अन्य जिल्लाबाट 
                                <input type="radio" name="radio" value="">
                            </label> 
                            <label class="radio-inline">
                                 (३) विदेशबाट
                                <input type="radio" name="radio" value="">
                            </label> --> 
                           <!--  {{ Form::text('migrationPlace', isset($migrationDetails) ? $migrationDetails->migrationPlace : '', ['class'=>'form-control migrationReason', 'placeholder'=>'Migration place']) }} -->
                            <select class="form-control" name="migrationPlace">
                                  <option value="">Select migration place</option>
                                  <option value="कास्की जिल्ला भित्रबाट" {{ !empty($migrationDetails->migrationPlace) && $migrationDetails->migrationPlace == 'कास्की जिल्ला भित्रबाट' ? 'selected' : ''}}>
                                      कास्की जिल्ला भित्रबाट
                                  </option>

                                  <option value="अन्य जिल्लाबाट" 
                                  {{ !empty($migrationDetails->migrationPlace) && $migrationDetails->migrationPlace == 'अन्य जिल्लाबाट' ? 'selected' : ''}}>अन्य जिल्लाबाट</option>
                                  <option value="विदेशबाट" {{ !empty($migrationDetails->migrationPlace) && $migrationDetails->migrationPlace == 'विदेशबाट' ? 'selected' : ''}}>विदेशबाट</option>
                            </select>
                        </div>
                        <br>
                        <div>
                            <label>
                                (१) बसाई सरी आएको वर्ष ः
                            {{ Form::text('migrationDate',isset($migrationDetails) ? $migrationDetails->migrationDate : '', ['class'=>'form-control migrationReason', 'placeholder'=>'वर्ष', 'id'=>'myDate2']) }}
                            </label>
                        </div>
                        <div>
                            <label>
                                (२) वसाई सरी आउनुको प्रमुख कारण (तत्कालिन अवस्थामा) ः 
                            {{ Form::textarea('migrationReason',isset($migrationDetails) ? $migrationDetails->migrationReason : '', ['class'=>'form-control migrationReason', 'placeholder'=>'प्रमुख कारण ']) }}
                            </label>
                        </div>
                    </div>
                </div>
                <br>
                <br>
        </div>  
        <div class="msgDisplay"></div>   
</div>
       