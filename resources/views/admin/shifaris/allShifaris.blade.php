@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')


<style type="text/css">
.panel-heading-custom{
	background: #6673fc;
}
</style>

<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<header>
			<h3>
				<i class="fa fa-plus"></i>
				All Shifaris
			</h3>
		</header>
	</div>

	<div class="card-head">
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>
	<br>
	<div class="col-lg-12">
		<div class="row">
			<!-- Academic  -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Academic
						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">

										<a href="{{ route('admin.scholarshipRecomm.create') }}" class="list-group-item">Scholarship Recommendation<small> छात्रवृत्ति सिफारिस</small></a>

										<a href="{{ route('admin.widespreadRecomm.create') }}" class="list-group-item">Widespread Recommendation<small> विपन्नता सिफारिस</small></a>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Economic  -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Economic					
						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">

										<a href="{{ route('admin.arthritisSupport.create') }}" class="list-group-item">Financial Treatment Suppport Recommendation <small> उपचारमा आर्थीक सहायता सिफारिस</small></a>

										<a href="{{ route('admin.yearlyIncomeCert.create') }}" class="list-group-item">Yearly Income Certified<small>बार्षिक आम्दानी</small></a>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Open format -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Open Format
						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">



										<a href="{{ route('admin.nepLanguage.create') }}" class="list-group-item">In Nepali Language</a>


										<a href="{{ route('admin.engLanguage.create') }}" class="list-group-item">In English Format</a>



									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Union Organization -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Union Organization
						</header>
						<div class="panel-body">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">
										<a href="{{ route('admin.nonProfitOrgRegistration.create') }}" class="list-group-item">Non Profit Organization Register<small> गैर नाफामुलुक संस्था दर्ता</small></a>
										<a href="{{ route('admin.instituteRegistration.create') }}" class="list-group-item">Institution Registration Recommendation<small> संस्था दर्ता सिफारिस</small></a>
										<a href="{{ route('admin.instituteRecommendation.create') }}" class="list-group-item">Institutionalization Recommendation<small> संस्था नबिकरण सिफारिस</small></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>

			<!-- justice Law  -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Justice / Law
						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">


										<a href="{{ route('admin.reconciliation.create') }}" class="list-group-item">Reconciliation Recommendation<small>मिलापत्र सिफारिस
										</small></a>


										<a href="{{ route('admin.commissionerDetail.create') }}" class="list-group-item">Commissioner Detail Recommendation<small>सयुक्त दरखास्त सिफारिस
										</small></a>


										<a href="{{ route('admin.varasisAuthiorities.create') }}" class="list-group-item">Varasis Authorities Recommendation<small>वारेस अख्तियारिनामा सिफारिस
										</small></a>


									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Bank Finiancial Institution -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Bank / Financial Institution
						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">


										<a href="{{ route('admin.accountOpening.create') }}" class="list-group-item">Account Opening Recommendation<small>खाता खोल्ने सिफारिस
										</small></a>


										<a href="{{ route('admin.accountClose.create') }}" class="list-group-item">Account Closed Recommendation<small> खाता बन्द सिफारिस
										</small></a>


										<a href="{{ route('admin.accountNavigate.create') }}" class="list-group-item">Account Navigation Recommendation<small> खाता नबिकरण सिफारिस
										</small></a>


										<a href="{{ route('admin.moneyTransfer.create') }}" class="list-group-item">Money Transfer Recommendation<small> रकम ट्रान्सफर सिफारिस
										</small></a>


									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Business and accounting -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Business & Accounting
						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">

										<a href="{{ route('admin.businessLicense.create') }}" class="list-group-item" title="Business Building License">Business Building License <small> निर्माण व्यवसाय इजाजत पत्र</small></a>

										<a href="{{ route('admin.businessRegistration.create') }}" class="list-group-item">Business Registration Rate Form</a>

										<a href="{{ route('admin.closeBusiness.create') }}" class="list-group-item">Close Business <small>व्यवसाय बन्द</small></a>

										<a href="{{ route('admin.permAccounting.create') }}" class="list-group-item">New Business PAN Number</a>

										<a href="{{ route('admin.businessAccounting.create') }}" class="list-group-item">  Business Extra PAN Number <small>कारोबार थप पान नं</small></a>

										<a href="{{ route('admin.industryRegistrationRecomm.create') }}" class="list-group-item">Industry Registration Recommendation<small>उद्योग दर्ता सिफारिस
										</small></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--  English format -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							English Format
						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">

										<a href="{{ route('admin.verifyAnnualIncome.create') }}" class="list-group-item">Verification Of Annual Income</a>

										<a href="{{ route('admin.verifyAnnualIncomeJpn.create') }}" class="list-group-item">Verification Of Annual Income For Japan</a>

										<a href="{{ route('admin.propertyVal.create') }}" class="list-group-item">Property Valuation</a>

										<a href="{{ route('admin.taxClearCert.create') }}" class="list-group-item">Tax Clearance Certificate</a>

										<a href="{{ route('admin.relationVerify.create') }}" class="list-group-item">Relationship Verification</a>

										<a href="{{ route('admin.birthVerify.create') }}" class="list-group-item">Birth Date Verification</a>

										<a href="{{ route('admin.unmarriedVerify.create') }}" class="list-group-item">Unmarried Verification</a>

										<a href="{{ route('admin.marriageVerify.create') }}" class="list-group-item">Marrige Verification</a>

										<a href="{{ route('admin.addressVerify.create') }}" class="list-group-item">Address Verification</a>

										<a href="{{ route('admin.occupationVerify.create') }}" class="list-group-item">Occupation Verification</a>

										<a href="{{ route('admin.identityVerificationOne.create') }}" class="list-group-item">Identity Verification 1</a>

										<a href="{{ route('admin.identityVerificationTwo.create') }}" class="list-group-item">Identity Verification 2</a>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Other -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Other
						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">

										<a href="{{ route('admin.diffIndividualCert.create') }}" class="list-group-item">Different Individuals Certified<small> फरक फरक व्यहोरा प्रमाणित
										</small></a>


										<a href="{{ route('admin.diffNameCert.create') }}" class="list-group-item">Different Names are Certified<small> फरक फरक नाम थर प्रमाणित
										</small></a>


										<a href="{{ route('admin.diffBDCert.create') }}" class="list-group-item">Different Birth Date Certified<small> फरक फरक जन्म मिति प्रमाणित
										</small></a>


										<a href="{{ route('admin.diffEngSpellCert.create') }}" class="list-group-item">Different English Spell Certified<small> फरक फरक अंग्रेजी हिज्जे प्रमाणित
										</small></a>


										<a href="{{ route('admin.recommSent.create') }}" class="list-group-item">Recommendation Sent<small> खुलाई पठाएको
										</small></a>


										<a href="{{ route('admin.jetMachineRecomm.create') }}" class="list-group-item">Jet Machine Recommendation<small>जेट मेशिन सिफारिस</small></a>


										<a href="{{ route('admin.roomCleaningRecomm.create') }}" class="list-group-item">Room Cleaning Recommendation<small> कोठा खाली गर्ने सिफारिस</small></a>


										<a href="{{ route('admin.recommCourt.create') }}" class="list-group-item">Feeless to Court Recommendation<small> कोर्ट फि नराख्ने सिफारिस</small></a>


										<a href="{{ route('admin.classAddRecomm.create') }}" class="list-group-item">Recommendation of Class Additional Approval<small> कक्षा थप स्वीकृतिको सिफारिस</small></a>


										{{-- <a href="{{ route('admin.attendanceCert.create') }}" class="list-group-item">Attendance Certified</a> --}}


										<a href="{{ route('admin.consumerCommitte.create') }}" class="list-group-item">Consumer Committee Constituted<small> उपभोक्ता समिति गठन</small></a>


										<a href="{{ route('admin.woodProviderRecomm.create') }}" class="list-group-item">Provider Wood Recommendation<small> सहुलियत काठ सिफारिस</small></a>


										<a href="{{ route('admin.naturalCalamities.create') }}" class="list-group-item">Natural Calamities Recommendation</a>
										
										<a href="{{ route('handicapId.create') }}" class="list-group-item">Handicap ID Card Recommendation</a>										
										
										<a href="{{ route('khulaiPathayeko.create') }}" class="list-group-item">Khulai Pathayeko Shifarish</a>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>		
			<!-- Ghar Jagga Shifaris  -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Ghar Jagga

						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">


										<a href="{{ route('batoKayam.create') }}" class="list-group-item">Bato Kayam Shifaris <small> बाटो कायम</small></a>

										<a href="{{ route('lalpurjaHarayeko.create') }}" class="list-group-item">Lalpurja Harayeko Shifaris <small> लाल पुर्जा हराएको</small></a>

										<a href="{{ route('mohiKitta.create') }}" class="list-group-item">Mohi Kitta Shifaris <small> मोही कट्टा</small></a>

										<a href="{{ route('gharPatal.create') }}" class="list-group-item">Ghar Patal Shifaris <small> धर भत्काएको</small></a>

										{{-- <a href="{{ route('kittaKatShifaris.create') }}" class="list-group-item">Kitta Kat Shifaris <small> कित्ता काट सिफारिस</small></a> --}}

										<a href="{{ route('charKillaBibaran.create') }}" class="list-group-item">Char Killa Bibaran <small> चार किला विवरण</small></a>

										<a href="{{ route('propertyNameTransfer.create') }}" class="list-group-item">Property Name Transfer Shifaris <small> </small></a>

										<a href="{{ route('addHomePropertyPaper.create') }}" class="list-group-item">Add Home Property Paper <small>पुर्जामा घर कायम गर्न सिफारिस</small></a>

										<a href="{{ route('internalMigration.create') }}" class="list-group-item">Internal Migration <small> </small></a>


										<a href="{{ route('gharbatoVerify.create') }}" class="list-group-item">Ghar Bato Verify <small> घरबाटो प्रमाणित </small></a>


										<a href="{{ route('gharKayam.create') }}" class="list-group-item">Ghar Kayam <small> घर कायम </small></a>


										<a href="{{ route('BijuliJadanShifaris.create') }}" class="list-group-item">Bijuli Jadan <small> बिजुली जडान </small></a>


										<a href="{{ route('DharaJadan.create') }}" class="list-group-item">Dhara Jadan <small> धारा जडान </small></a>

										
										<a href="{{ route('permanentResidentProposal.create') }}" class="list-group-item">Permanent Resident Proposal<small> स्थायी बसोबास सिफारिस </small></a>
										

										<a href="{{ route('temporaryResidentProposal.create') }}" class="list-group-item">Temporary Resident Proposal<small> अस्थायी बसोबास सिफारिस </small></a>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			

			<!-- Relation Define Shifaris  -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Relation Define

						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">


										<a href="{{ route('relationIdentify.create') }}" class="list-group-item">Relation Identify <small> नता प्रमाणित</small></a>

										<a href="{{ route('mritakHakdarShifaris.create') }}" class="list-group-item">Mritak Hakdar <small> मृतकका हकदार</small></a>

										<a href="{{ route('familyHierarchy.create') }}" class="list-group-item">Family Hierarchy <small> तीन पुस्ते</small></a>

										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<!-- Relation Define Shifaris  -->
			<div class="col-lg-4">
				<div class="col-sm-12">
					<div class="panel">
						<header class="panel-heading panel-heading-custom all-shifaris-headings">
							Nepali Format

						</header>
						<div class="panel-body ">
							<div class="row">
								<div class="col col-12">
									<div class="list-group">


										<a href="{{ route('unmarriedVerifyNep.create') }}" class="nav-link">Unmarried Verify <small> अविवाहित प्रमाणित</small></a>

										<a href="{{ route('marriedVerifyNep.create') }}" class="nav-link">Married Verify <small> विवाहित प्रमाणित</small></a>

										<a href="{{ route('birthDateVerifyNep.create') }}" class="nav-link">Birth Date Verify Nepali <small> जन्म मिति प्रमाणित</small></a>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
