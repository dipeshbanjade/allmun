@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12">
    <div class="card-box">
          <button type="button" class="btn btn-primary   pull-right" id="btnAddRole">
              <i class="fa fa-plus">&nbsp; <span>Add</span></i>
          </button>
        <div class="card-head">
            <header>Role</header>
          {{ App::getLocale() }}
        </div>
        <div class="card-body">
        <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
           <div class="mdl-tabs__panel is-active p-t-20" id="tab4-panel">
           <div class="table-scrollable">
                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
                    <tbody>
                        <tr>
                            <th>SN</th>
                            <th>Role For</th>
                            <th>Role Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                         @php  $count = 1  @endphp
                         @if(count($roleData) > 0)
                         @foreach($roleData as $role)
                             <tr>
                                 <td>{{ $count ++ }}</td>
                                 <td>{{ $role->roleFor }}</td>
                                 <td>{{ $role->roleName }}</td>
                                 <td>{{ $role->desc ?? 'N/A' }}</td>
                                 <td>
                                 <a class="btn btn-primary btn-xs">
                                     <i class="fa fa-fw fa-lg fa-pencil-square-o btnUpdateSysAccount" data-toggle="modal" data-target="#updateAccount" data-id="{{ $role->id }}" data-url="{!! route('system.role.edit', $role->id) !!}"></i>
                                 </a>
                                 <a class="txt-color-red deleteMe" 
                                        data-url="{!! route('system.role.delete', $role->id ) !!}" title="delete user role" data-name="{{ $role->roleName }}" data-id = "{{ $role->id }}">
                                     <i class="fa fa-trash-o"></i>
                                </a>
                                 
                             </td>
                             </tr>
                         @endforeach
                        @endif
                    </tbody>
                </table>
                {{ $roleData->links() }}
            </div>
            <div class="text-center">
                <button class="btn btn-outline-primary btn-round btn-sm">Load
                    More</button>
            </div>
           </div>
        </div>
        
        </div>
    </div>
   
</div>
@section('modelSection')
<div class="modal fade" id="frmRole">
  <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <div class="card-head">
         <header><i class="fa fa-plus">&nbsp;Add Permission</i></header>
       </div>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
       {{ Form::open(['route' => 'system.role.store', 'name'=>'frmRole']) }}
           @include('admin.acl.role._form')
           <div class="col-lg-12 p-t-20 text-right"> 
           {{ Form::submit('CREATE', ['class' => 'btn btn-success']) }}
           <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
           </div>
       {{ Form::close() }}
     </div>
     <div class="modal-footer">
     </div>
   </div>
 </div>
</div>
@endsection

@endsection
@section('custom_script')
@include('admin.acl.role.script')
@endsection