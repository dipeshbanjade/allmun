 <script type="text/javascript">
	$(function(){
		$("form[name='frmHouseDetails']").validate({ 
			rules:{
				citizen_infos_id:{
					required:true
				},
				toleName:{
					required:true,
					minlength:3,
					maxlength:255
				},
                isHsMadeByRule : {
                   required : true
                },
				streetName:{
					required:true,
					minlength:3,
					maxlength:255
				},
				hsNum:{
					required:true,
					minlength:3,
					maxlength:50
				},	

				munHsNum:{
					maxlength:50
				},
				localAreaName:{
					required:true,
					minlength:3,
					maxlength:255
				},
				fmTypeName:{
					required:true
				},
				fmHeadPh:{
					required:true,
					number: true,
					minlength:9,
					maxlength:10
				},
				hsTypeName:{
					required:true
				},
				noOfRom:{
					required:true,
					number:true,
					maxlength : 3
				},
				romUsageForName:{
					required:true
				},
				fmMember:{
					required:true
				},
				memLiving:{
					required:true,
					number:true,
					maxlength:5
				},
				houseSamipto:{
					required:true
				},
				landSamipto:{
					required:true
				},
				numOfHs:{
					required:true,
					number:true
				},
				florNum:{
					required:true,
					number:true
				},				
				romNum:{
					required:true,
					number:true
				},
				haveSameKitchen:{
					required:true
				},
				hsUseFor:{
					required:true				
				},
				areaOfHs:{
					required:true
				},
				hsEstd:{
					required:true,
					date: true
				},
				hsFndName:{
					required:true
				},
				romFloorName:{
					required:true
				},
				houseWallName:{
					required:true
				},
				houseRoofName:{
					required:true
				},
				ishouseMadeByRole:{
					required:true
				},
				eqRegistance:{
					required:true
				},
				haveParking:{
					required:true
				},
				havePlantation:{
					required:true
				},
				haveGarden:{
					required:true
				},
				isHsOwnByHead:{
					required:true
				}
			},

			messages: {
				citizen_infos_id: " Citizen info is required ",
				toleName: " Tole name is required ",
				streetName: " Street name is required",
				hsNum: " House number is required with 3 digit",
				munHsNum: " Muncipality house number is required ",
				wardKhanda: " Ward category is required ",
				localAreaName: " Local area name is required ",
				fmTypeName: " Family type name is required ",
				fmHeadPh: " Family head phone number is required",
				hsTypeName: " House type name is required ",
				noOfRom: " Number of room is required ",
				romUsageForName: " Room use for is required ",
				fmMember: " Family memeber is required",
				memLiving: " Member living for is required",
				houseSamipto: " Houese samipto is required ",
				landSamipto: " Land samipto is required",
				noOfHs: " Number of house is required",
				florNum: " Floor Number is required",
				romNum: " Room Numer is required",
				haveSameKitchen: " The same Kitchen filed cannot be placed empty",
				hsUseFor: " House user for is required",
				areaOfHs: " Area of house is required",
				hsEstd: " House established date is required",
				hsFndName: " House foundation name is requireds",
				romFloorName: " Room floor name is required ",
				houseWallName: " House wall structure name is required",
				houseRoofName: " House roof name is required ",
				ishouseMadeByRole: " House made by role is required",
				eqRegistance: " The earthquake resistance filed cannot be left empty",
				haveParking: " The have parking field is required",
				havePlantation: " The plantation field is required",
				haveGarden: " The Garden filed is required",
				isHsOwnByHead: " Is house owned by head is required",
				isHsMadeByRule: "house made by rule field required"
			}
		});
	});
	var noOfRom;
	var romNum;
	var fmMem;
	var memLiving;
	$('.noOfRom').on('change',function(){
		noOfRom=parseInt($('.noOfRom').val());
	});
	$('.romNum').on('change',function(){
		romNum= parseInt($('.romNum').val());
		if(noOfRom >0 && romNum > noOfRom){
			$('.noOfRom').focus();
			alert('Used room number is greater than room number');
		}
	});

	$('.fmMem').on('change',function(){
		fmMem=parseInt($('.fmMem').val());
	});

	$('.memLiving').on('change',function(){
		memLiving= parseInt($('.memLiving').val());
		if(fmMem >0 && memLiving > fmMem){
			$('.fmMem').focus();
			alert('Member living is greate than total number of family member');
		}
	});
</script>