<?php

namespace App\Http\Requests\admin\setting\areaType;

use Illuminate\Foundation\Http\FormRequest;

class AreaTypeVal extends FormRequest
{
 
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nameEng' => 'required|min:4:min:50'
        ];
    }
}
