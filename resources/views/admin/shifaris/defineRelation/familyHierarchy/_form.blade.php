<div class="right_col nep" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">तीन पुस्ते</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
         @include('admin.shifaris.municipalityDetail')
         <div class="row top-part">
          <div class="col-md-6">
           <p align="left"><b class="ps">प. सं.:
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
            @endif
          </b></p>
          <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNum']) }}
          </b></p>
        </div>
        <div class="col-md-6">
          <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h4 align="center"><b>बिषय: <u>तीन पुस्ते प्रमाणित ।</u></b></h4>
          <p></p>
        </div>
      </div>
      <div class="row content-para">
        <div class="col-md-12">
          <p align="left">
            <b>
              {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
            </b> महा/उपमाहा/नगरपालिका/गाउँपालिका वडा नं.  
            <b>
              {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            </b>
            (साबिकको ठेगाना
            {{ Form::text('sabikAddress', null, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            ) निवासी श्री/सुश्री/श्रीमती 
            {{ Form::text('applicantName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
            को तीन पुस्ते तपशिलमा उल्लेख भए अनुसार रहेको व्यहोरा प्रमाणित साथ अनुरोध गरीन्छ। साथै निजको नाममा मालपोत कार्यालयमा दर्ता रहेको जग्गाको विवरण तपशिल बमोजिम रहेको व्यहोरा समेत अनुरोध गरीन्छ।  
          </p>
        </div>
      </div> 
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <p align="center"><b>जग्गाको विवरण</b></p>
            <table class="table table-bordered" id="jaggaBibaran">
              <thead>
                <tr><th>क्र.स.</th>
                  <th>कित्ता नं.</th>
                  <th>सिट नं.</th>
                  <th>क्षेत्रफल</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $count = 1;
                $tableData = !empty($data) ? json_decode($data->jaggaBibaran, true) : [1];
          //dd($tableData);
                foreach($tableData as $singleData){         
                  ?>
                  <tr>
                    <td>
                      {{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('kittaNo[]', !empty($singleData['kittaNo']) ? $singleData['kittaNo'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('seatNo[]', !empty($singleData['seatNo']) ? $singleData['seatNo'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('area[]', !empty($singleData['area']) ? $singleData['area'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>

                    @if($count == 2)
                    <td class="add-btns" style="border: 0px;">
                      <button type="button" id="jaggaBibaranAdd" class="btn btn-success"><span class="fa fa-plus"></span></button>
                    </td>
                    @else
                    <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                    @endif


                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <p align="center"><b>तिन पुस्ते विवरण</b></p>
              <table class="table table-bordered" id="tinPuste">
                <thead>
                  <tr><th>क्र.स.</th>
                    <th>नाम</th>
                    <th>नाता</th>
                    <th>नागरिकता नं.</th>
                    <th>जारी मिति</th>
                    <th>जिल्ला</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $count = 1;
                  $tableData = !empty($data) ? json_decode($data->hierarcyTable, true) : [1];
          //dd($tableData);
                  foreach($tableData as $singleData){         
                    ?>
                    <tr>
                      <td>
                        {{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                      </td>
                      <td>
                        {{ Form::text('name[]', !empty($singleData['name']) ? $singleData['name'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                      </td>
                      <td>
                        {{ Form::text('relation[]', !empty($singleData['relation']) ? $singleData['relation'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                      </td>
                      <td>
                        {{ Form::text('nagarikata[]', !empty($singleData['nagarikata']) ? $singleData['nagarikata'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                      </td>
                      <td>
                        {{ Form::text('dateOfIssue[]', !empty($singleData['dateOfIssue']) ? $singleData['dateOfIssue'] : NULL, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'dateOfIssue', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("dateOfIssue")']) }}
                      </td>
                      <td>
                        {{ Form::text('district[]', !empty($singleData['district']) ? $singleData['district'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                      </td>

                      @if($count == 2)
                      <td class="cancel-btns" style="border: 0px;">
                        <button type="button" id="tinPusteAdd" class="btn btn-success"><span class="fa fa-plus"></span></button>
                      </td>
                      @else
                      <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                      @endif

                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="text-right btm-last">
            <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
            <p> <b>       
             <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
              @if(count($deginationsId) > 0)
              @foreach($deginationsId as $deg)
              <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                {{ $deg->nameNep }} 
              </option>
              @endforeach
              @endif
            </select>
          </b> </p>
        </div>
        <!--views for nibedak detail -->
        <div class="clearfix"></div>
        <hr>
        @include('admin.shifaris.nibedakCommonField')
        <!-- END -->
      </div>
      <div class="col-md-12">
        <hr>
      </div>
    </div>
  </div>
</div>