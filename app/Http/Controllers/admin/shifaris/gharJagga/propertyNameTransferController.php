<?php

namespace App\Http\Controllers\admin\shifaris\gharJagga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\PropertyNameTransfer;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\propertyNameTransferVal;
use DB;
use Auth;
use App\model\mylogic;


class propertyNameTransferController extends Controller
{
    protected $viewPath = 'admin.shifaris.gharJagga.propertyNameTransfer';
    protected $shifarisTableName     = 'property_name_transfers';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixPropertyNameTransfer');

    }

    public function index()
    {
        $page['page_title']       = 'Property Name Transfer';
        $page['page_description'] = 'Property Name Transfer';

            $instRecomd = PropertyNameTransfer::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
  
    }

    public function create()
    {
        $page['page_title']       = 'Property Name Transfer : create';
        $page['page_description'] = 'Property Name Transfer details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
            $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
            return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));

    }

    public function store(propertyNameTransferVal $request)
    {
     
       DB::beginTransaction();
       try {
// Data table 2
        $dataTableOneCount = count($request->hakdarName);

        $hakdarName = $request->hakdarName;
        $nataRelation = $request->nataRelation;
        $fathHubName = $request->fathHubName;
        $citizenNo = $request->citizenNo;
        $houseNo = $request->houseNo;
        $kittaNo = $request->kittaNo;
        $batoName = $request->batoName;

        $dataTableOneArray = [];

        for ($i=0; $i < $dataTableOneCount; $i++) {
            array_push($dataTableOneArray, [
                'hakdarName' => $hakdarName[$i],
                'nataRelation' => $nataRelation[$i],
                'fathHubName' => $fathHubName[$i],
                'citizenNo' => $citizenNo[$i],
                'houseNo' => $houseNo[$i],
                'kittaNo' => $kittaNo[$i],
                'batoName' => $batoName[$i]
            ]);
        }
        $dataTableOneJson = json_encode($dataTableOneArray);

// Data table 2

        $dataTableTwoCount = count($request->wardNo);

        $wardNo = $request->wardNo;
        $seatNo = $request->seatNo;
        $kitta = $request->kitta;
        $area = $request->area;
        $houseNo2 = $request->houseNo2;
        $kittaNo2 = $request->kittaNo2;
        $streetType = $request->streetType;
        $kaifiyat = $request->kaifiyat;

        dd($request);

        $dataTableTwoArray = [];

        for ($i=0; $i < $dataTableTwoCount; $i++) {
            array_push($dataTableTwoArray, [
                'wardNo' => $wardNo[$i],
                'seatNo' => $seatNo[$i],
                'kitta' => $kitta[$i],
                'area' => $area[$i],
                'houseNo2' => $houseNo2[$i],
                'kittaNo2' => $kittaNo2[$i],
                'streetType' => $streetType[$i],
                'kaifiyat' => $kaifiyat[$i]
            ]);
        }
        $dataTableTwoJson = json_encode($dataTableTwoArray);


        //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


        $isShifarisExist = isset($request->shifarisId) ? PropertyNameTransfer::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? PropertyNameTransfer::findOrfail($isShifarisExist['id']) : new PropertyNameTransfer;

        $save->createdBy = Auth::user()->id;

        $save->refCode  = $request->refCode;

        $save->wards_id  = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";

        $save->issuedDate = $request->issuedDate;
        $save->chalaniNumber = $request->chalaniNumber;
        $save->accountHolderName = $request->accountHolderName;
        $save->accountHolderAddress = $request->accountHolderAddress;
        $save->ownerName = $request->ownerName;
        $save->nata = $request->nata;
        $save->natedarName = $request->natedarName;
        $save->date = $request->date;
        $save->applicant = $request->applicant;

        $save->tableData1 = $dataTableOneJson;
        $save->tableData2 = $dataTableTwoJson;


        $save->authorizedPerson = $request->authorizedPerson;
        $save->deginations_id  = $request->deginations_id;
        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
$request->chalaniNum = $request->chalaniNumber;
                    $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
            ]);
            // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
        }else{
            return response()->json([
            'success' => false,
            'message' => config('activityMessage.unSaveMessage')
            ]);
            // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
   }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }


}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
