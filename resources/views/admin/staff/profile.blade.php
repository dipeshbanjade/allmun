@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-md-12">
            <div class="card-head col-md-12">
                <header><i class="fa fa-eye">&nbsp;@lang('commonField.extra.muncipility')</i></header>
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar">
                        <div class="card card-topline-aqua">
                            <div class="card-body no-padding height-9">
                                <div class="row">
                                    <div class="profile-userpic">
                                        <img src="{{ asset($data->profilePic) }}" width="150" height="150" class="img-circle user-img-circle" alt="User Image" />
                                        </div>
                                </div>
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name">
                                          {!! $profile->email !!}
                                    </div>
                                    <div class="profile-usertitle-job">
                                       Role : {{ getRoleName($profile->roles_id)['roleName'] }}<br>
                                       Citizen Number : {{ $data->citizenNo }}
                                    </div>
                                    <div class="profile-usertitle-job">
                                      <label for="">{{ $data->refCode }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                <!-- BEGIN PROFILE CONTENT -->
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-head card-topline-aqua">
                                                    <header>Description</header>
                                                </div>
                                                <table class="table table-striped">
                                                    <tr>
                                                      <th>@lang('commonField.personal_information.name')</th>
                                                      <td>
                                                         {!! $data->firstNameEng . '&nbsp;'.$data->middleNameEng . '&nbsp;' . $data->lastNamaeEng  !!} <br>
                                                          
                                                          {!! $data->firstNameNep . '&nbsp;'.$data->middleNameNep . '&nbsp;' . $data->lastNamaeNep  !!}
                                                       </td>
                                                    </tr>
                                
                                                    <tr>
                                                      <th>@lang('commonField.personal_information.address')</th>
                                                      <td>{!! $profile->provinces_id . '&nbsp;' . $profile->districts_id . '&nbsp;' . $profile->villageEng !!}</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <th>@lang('commonField.extra.joiningDate')</th>
                                                        <td>{{ $data->joingDateNep }}</td>
                                                    </tr>
                              
                                                  </table>
                                            </div>
                                           <!--  <p class="pull-right profile-btn">
                                                <button type="button" class="btn btn-danger pull-right" onclick="history.back()"><i class="fa fa-arrow-left"><span>Back</span> </i>
                                                </button>
                                                <a href="" type="button" class="btn btn-success pull-right">
                                                    <i class="fa fa-edit"><span>Edit</span> </i>
                                                </a>
                                                </p> -->
                                        </div>
                                    </div>
                                    <!-- END PROFILE CONTENT -->
                                </div>
                                
                                <!-- END SIDEBAR USER TITLE -->
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>    
</div>

@endsection
@section('custom_script')
 @include('admin.setting.degination.script')
@endsection