@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
<div class="card-box">
<div class="card-body ">
	<div class="card-head">
		<header><i class="fa fa-eye"></i>&nbsp;@lang('commonField.extra.personalinfo')</header>
	</div>
	@if(count($details) > 0)
		    <div class="row p-3">
		       <div class="col col-6">
       	          <label>
       	   	       <i class="fa fa-user"></i>
       	   	       <span>
                    {!! $lang == 'np' ? $details->firstNameNep . $details->middleNameNep . $details->lastNameNep : $details->firstNameEng . $details->middleNameEng . $details->lastNameEng  !!}
       	   	       </span>
       	          </label><br>
       	          <label>
       	                <i class="fa fa-phone"></i><span>{!! $details->phoneNumber !!}</span><br>
       	          	     <i class="fa fa-map-marker"></i>
                              {!! isset($details->provinces_id) ? $details->provinces->name : ''  !!} &nbsp;
                              @if(isset($details->districts_id))
                                  {{ $lang == 'np' ? $details->districts->districtNameNep : $details->districts->districtNameEng  }}
                              @endif
       	          	       {!! $lang == 'np' ? $details->villageNep : $details->villageEng  !!} <br>  
       	          </label><br>
                  <label>
                    <i class="fa fa-envelope"></i>
                    <span>
                        {!! $details->email !!}
                    </span>
                  </label>

		       </div>
		       <div class="col col-6">
                  <img src="{{ asset($details->profilePic) }}">
		       </div>
		     </div>
		       <hr>

	        <div class="row">
		  	   <table class="table table-striped">
		  	   	    <tr>
		  	   	    	<th>@lang('commonField.extra.refCode')</th>
		  	   	    	<td>{{ $details->refCode }}</td>
		  	   	    </tr>
		  	   	    <!--  -->
                <tr>
                  <th>@lang('commonField.personal_information.citizenship')</th>
                  <td>{{ $details->citizenNo }}</td>
                </tr>

                 <tr>
                  <th>@lang('commonField.extra.department')</th>                 

                  <td>
                      @if(isset($details->departments_id))
                        {{  $lang === 'np' ? $details->departments->nameNep : $details->departments->nameEng }} 
                      @endif
                  </td>
                </tr>

                <tr>
                  <th>@lang('commonField.extra.role')</th>                 
                  <td>{{  $details->roles_id }}</td>
                </tr>

                <tr>
                  <th>@lang('commonField.extra.joiningDate')</th>
                  <td>{{ $details->joingDateNep }} / {{ $details->joingDateEng }}</td>
                </tr>

		  	   	    <tr>
		  	   	    	<th>@lang('commonField.personal_information.dob')</th>
		  	   	    	<td>BS: {{ $details->dob }}</td>
		  	   	    </tr> 

		  	   </table>
           <p class="p-3">
                <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> 
                     <span>@lang('commonField.button.back')</span> 
                </button>
                <a href="{{ route('admin.munStaff.edit', $details->idEncrip) }}" class="btn btn-success pull-right">
                <i class="fa fa-edit"></i><span>@lang('commonField.button.edit')</span></a>

           </p>
		  </div>
	</div>
	@endif

</div>
</div>
</div>
@endsection