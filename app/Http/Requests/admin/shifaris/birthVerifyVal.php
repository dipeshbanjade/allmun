<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class birthVerifyVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'refCode' => 'required|min:3|unique:birth_date_verifications',
            'issuedDate'=> 'required',
            'title'=> 'required',
            'Name'=> 'required',
            'appRelation'=> 'required',
            'fatherName'=> 'required',
            'motherName'=> 'required',
            'municipality'=> 'required',
            'ward'=> 'required',
            'district'=> 'required',
            'former'=> 'required',
            'orgAddr'=> 'required',
            'orgType'=> 'required',
            'orgWard'=> 'required|min:1',
            'appNameOpt'=> 'required',
            'citizenIssuedDistrict'=> 'required',
            'appTitleOption'=> 'required',
            'dateInBS'=> 'required',
            'dateInAD'=> 'required',
            'authorizedPerson'=> 'required'
        ];
    }
}
