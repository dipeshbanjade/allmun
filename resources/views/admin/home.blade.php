@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')

<?php
    use Carbon\Carbon;

    $today = Carbon::now()->format('m.d');  // dob 
    $todYear = Carbon::now()->format('y');
    $todMonth = Carbon::now()->format('m');
    $todays = Carbon::now()->format('d');
    $d = cal_days_in_month(CAL_GREGORIAN,$todMonth,$todYear);
    $tod = str_replace(".","",$today);

     $lan = getLan();  
 ?>
<!--  -->
<div class="col-sm-12">
  <div class="card-box">
        <div class="card-head">
            <header style="width: 100%;">
              Username : {!! Auth::user()->email !!}<br>
              Role : {!! Auth::user()->roles->roleName !!}<hr>
            </header>
        </div>
  </div>
</div>
<div class="infoDiv"></div>
   <div class="col-md-6 col-sm-12 about_card">
@if(Auth::user()->userLevel == 'mun')
           <div class="card-box">
                   <div class="card-head">
                       <header>@lang('sidebarMenu.system_setting.municipility')</header>
                   </div>
                   <div class="card-body no-padding height-9">

                       <ul class="list-group list-group-unbordered">
                           <li class="list-group-item">
                               <b>@lang('commonField.personal_information.name')</b>
                               <div class="pull-right">{!! Auth::user()->municipilities->nameEng !!}</div>
                           </li>
                           <li class="list-group-item">
                               <b>@lang('commonField.personal_information.phoneNumber')</b>
                               <div class="pull-right">{!! Auth::user()->municipilities->landLineNumber !!}</div>
                           </li>
                            <li class="list-group-item">
                               <b>@lang('commonField.personal_information.address')</b>
                               <div class="pull-right">{!! Auth::user()->municipilities->addrEng !!}</div>
                           </li>
                           <li class="list-group-item">
                               <b>@lang('commonField.extra.departmentHead')</b>
                               <div class="pull-right">{!! Auth::user()->municipilities->muncipilityHead !!}</div>
                           </li>
                            <!--  -->
                       </ul>
                   </div>
           </div>
@endif

           <!--  -->
           @if(Auth::user()->userLevel == 'wrd' || Auth::user()->userLevel == 'mun')
           <div class="card card-box">
             <div class="card-head">
                 <header>@lang('commonField.personal_information.birthDay')</header>
                 <div class="tools">
                   <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                </div>
             </div>
                <div class="card-body no-padding height-9">
                   <div class="row">
                       <table class="table table-striped">
                           <thead>
                             <tr>
                               <th>@lang('commonField.extra.sn')</th>
                               <th>@lang('commonField.personal_information.fullName')</th>
                               <th>@lang('commonField.extra.department')</th>
                               <th width="5%">@lang('commonField.extra.day')</th>
                             </tr>
                           </thead>
                           <tbody>
                           @php  
                               $count = 1;
                           @endphp
                               
                           @foreach($getDobStaff as $birthDay)
                               @php 
                                   $bDate = strtotime($birthDay->dobEng);
                                   $t = date('m.d',$bDate); 
                                   $month = date('m',$bDate);
                                   $day = date('d',$bDate);
                                   if($month<=$todMonth){ 
                                     $total = str_replace(".","",$t);
                                     $remain = $total - $tod;
                                   }else{
                                     if($month-$todMonth == 1){
                                      $remain = $d-$todays+$day; 
                                      }
                                   }  
                               @endphp
                               <!-- display if birthday of staff is 7 days to go-->
                               @if(isset($remain) &&  $remain>= 0 && $remain<= 7 )
                                   <tr>
                                       <td>{{ $count ++ }}</td>
                                       <td>
                                           <?php 
                                                   echo $lan == 'np' ? $birthDay->firstNameNep . ' ' . $birthDay->middleNameNep . ' ' . $birthDay->lastNameNep : $birthDay->firstNameEng . ' ' . $birthDay->middleNameEng . ' ' . $birthDay->lastNameEng     ?>
                                       </td>
                                       <td>
                                      @if(isset($birthDay->departments_id))
                                           @if(Auth::user()->userLevel == 'mun')
                                               {{ $lang == 'np' ? $birthDay->departments->nameNep : $birthDay->departments->nameEng  }}
                                               @endif
                                               @else
                                            {{ $lang == 'np' ? $birthDay->departments->nameNep : $birthDay->departments->nameEng  }}
                                        @endif
                                       </td>
                                       @if($remain == 0)
                                           <td>
                                               <h5 class="text-success"><b>Happy Birthday</b></h5>
                                           </td>
                                       @else
                                           <td class="text-primary">
                                               {{ $remain . 'days to go' }}
                                           </td>
                                       @endif
                                   </tr>
                               @endif  
                           @endforeach
                           </tbody>
                       </table>
                   </div>
                 </div>
   </div>
   @endif
   </div>
<!--  -->
 
@if(Auth::user()->userLevel == 'mun' && Auth::user()->roles_id ==2)
   <div class="col-md-6 col-sm-12 about_card">
           <div class="card-box">
                   <div class="card-head">
                       <header>@lang('sidebarMenu.system_setting.ward')</header>
                   </div>
                   <div class="card-body no-padding height-9">
                      <ul class="list-group">
                        @if($wardList)
                         @foreach($wardList as $wrdList)
                           <?php $wardStaffNo = getWardStaffNumber($wrdList->id);     ?>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              <a href="{{ route('viewWrdDetails', $wrdList->identifier) }}" target="_blank" title="Ward Details">
                                <span class="wardName">{{ $lang =='np' ? $wrdList->nameNep : $wrdList->nameEng }}</span>
                              </a>
                               <span class="wardPhone">{{ $wrdList->landLineNumber }}</span>
                               <span class="wardHead">{{ $wrdList->muncipilityHead }}</span>

                              <small>@lang('commonField.extra.staff')&nbsp;{{ count($wardStaffNo) }}</small>
                            </li>
                           @endforeach
                       @endif
                      </ul>
                   </div>
           </div>
   </div>
@endif

<div class="col-md-6 col-sm-12 about_card">
        <div class="card-box">
                <div class="card-head">
                    <header>@lang('commonField.leaveIsFor.leaveTitle')</header>
                </div>
                <div class="card-body no-padding height-9">
                   <ul class="list-group">
                     @if($getOfficialLeave)
                      @foreach($getOfficialLeave as $officialLeave)
                         <li class="list-group-item d-flex justify-content-between align-items-center">
                           
                             <span class="wardName">{{ $officialLeave->leaveFor }}</span>
                            <span class="wardPhone">{{ $officialLeave->dateFromNep }} - {{ $officialLeave->dateFromEng }}</span>
                         </li>
                        @endforeach
                    @endif
                   </ul>
                </div>
        </div>
</div>
<!--  -->
@if(Auth::user()->userLevel == 'mun' && Auth::user()->roles_id ==2)
   <div class="col-md-12 col-sm-12 about_card" height="500" style="overflow-x: scroll;">
           <div class="card-box">
                   <div class="card-head">
                       <header>@lang('commonField.extra.dailyAttendance')</header>
                   </div>
                @if($todayAtten)
                <table class="table table-striped">
                    <thead>
                      <tr>
                          <th width="5%">@lang('commonField.extra.sn')</th>
                          <th>@lang('commonField.personal_information.fullName')</th>
                          <th>@lang('commonField.extra.department')</th>
                          <th>@lang('commonField.employer.checkIn')</th>
                          <th>@lang('commonField.employer.checkOut')</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php  $count = 1;    ?>
                        @foreach($todayAtten as $att)
                        <tr>
                          <td>{{ $count ++ }}</td>
                          <td>{{ $lang == 'np' ? $att->firstNameNep .' ' . $att->middleNameNep . ' ' . $att->lastNameNep : $att->firstNameEng .' ' . $att->middleNameEng . ' ' . $att->lastNameEng   }}</td>
                          <td>{{ $lang == 'np' ? $att->departmentNameNep : $att->departmentNameNep  }}</td>
                          <td>{{ $att->created_at }}</td>
                          <td>5</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                 @else
                  <h3>staffs is not available</h3>
                @endif
            </div>
    </div>
@endif
<!-- ward attendance -->
@if(Auth::user()->userLevel == 'wrd' && Auth::user()->roles_id ==3)
     <div class="col-md-12 col-sm-12 about_card" height="500" style="overflow-x: scroll;">
             <div class="card-box">
                     <div class="card-head">
                         <header>@lang('commonField.extra.dailyAttendance')</header>
                     </div>
                  @if($todayAtten)
                  <table class="table table-striped">
                      <thead>
                        <tr>
                            <th width="5%">@lang('commonField.extra.sn')</th>
                            <th>@lang('commonField.personal_information.fullName')</th>
                            <th>@lang('commonField.extra.department')</th>
                            <th>@lang('commonField.employer.checkIn')</th>
                            <th>@lang('commonField.employer.checkOut')</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php  $count = 1;    ?>
                          @foreach($todayAtten as $att)
                          <tr>
                            <td>{{ $count ++ }}</td>
                            <td>{{ $lang == 'np' ? $att->firstNameNep .' ' . $att->middleNameNep . ' ' . $att->lastNameNep : $att->firstNameEng .' ' . $att->middleNameEng . ' ' . $att->lastNameEng   }}</td>
                            <td>{{ $lang == 'np' ? $att->departmentNameNep : $att->departmentNameNep  }}</td>
                            <td>{{ $att->created_at }}</td>
                            <td>5</td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
                   @else
                    <h3>staffs is not available</h3>
                  @endif
              </div>
      </div>
@endif
<!-- ward attendance -->
<!--  -->
@if(Auth::user()->userLevel == 'wrd')
     <div class="col-md-6 col-sm-12 about_card">
             <div class="card-box">
                     <div class="card-head">
                         <header>@lang('sidebarMenu.system_setting.ward')</header>
                     </div>
                     <div class="card-body no-padding height-9">

                         <ul class="list-group list-group-unbordered">
                             <li class="list-group-item">
                                 <b>@lang('commonField.personal_information.name')</b>
                                 <div class="pull-right">{!! Auth::user()->wards->nameEng !!}</div>
                             </li>
                             <li class="list-group-item">
                                 <b>@lang('commonField.personal_information.phoneNumber')</b>
                                 <div class="pull-right">{!! Auth::user()->wards->landLineNumber !!}</div>
                             </li>
                              <li class="list-group-item">
                                 <b>@lang('commonField.personal_information.address')</b>
                                 <div class="pull-right">{!! Auth::user()->wards->addrEng !!}</div>
                             </li>
                             <li class="list-group-item">
                                 <b>@lang('commonField.extra.departmentHead')</b>
                                 <div class="pull-right">{!! Auth::user()->wards->muncipilityHead !!}</div>
                             </li>

                             <!-- <li class="list-group-item">
                                 <b>@lang('commonField.extra.role')</b>
                                 <div class="pull-right">{!! Auth::user()->role !!}</div>
                             </li> -->
                         </ul>
                     </div>
             </div>
     </div>
     <!--  -->
     <div class="col-md-6 col-sm-12 about_card">
                <div class="card-box">
                        <div class="card-head">
                            <header>Description</header>
                        </div>
                        <div class="card-head">
                         <div class="pull-right">{{ isset($citizenCount) ? $citizenCount : ''  }}</div>
                          <header class="pull-right">@lang('commonField.extra.NoOfCitizen')</header>
                      </div>

                        <div class="card-body no-padding height-9">
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                <i class="fa fa-home"></i>
                                    <b>@lang('commonField.extra.NoOfHouse')</b>
                                    <div class="pull-right">{{ isset($hsCount) ? $hsCount : '' }}</div>
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-male"></i>
                                    <b>@lang('commonField.extra.NoOfMale')</b>
                                    <div class="pull-right">{{ isset($maleCount) ? $maleCount : '' }}</div>
                                </li>
                                 <li class="list-group-item">
                                    <i class="fa fa-female"></i>
                                    <b>@lang('commonField.extra.NoOfFemale')</b>
                                    <div class="pull-right">
                                      {{ isset($femaleCount) ? $femaleCount : '' }}
                                    </div>
                                </li>
                                <!--  -->
                                <li class="list-group-item">
                                    <i class="fa fa-check-square text-success"></i>

                                    <b>@lang('commonField.extra.withCitizen')</b>
                                    <div class="pull-right">{{ isset($withCitizen) ? $withCitizen : '' }}</div>
                                </li>
                                <!--  -->
                                <li class="list-group-item">
                                    <i class="fa fa-window-close text-danger"></i>
                                    <b>@lang('commonField.extra.withoutCitizen')</b>
                                    <div class="pull-right">
                                      {{ isset($withOutCitizen) ? $withOutCitizen : '' }}
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <i class="fa fa-male text-danger"></i>
                                    <b>@lang('commonField.personal_information.disabledCitizen')</b>
                                    <div class="pull-right">{{ isset($disableCitizen) ? $disableCitizen : '' }}</div>
                                </li>

                                <li class="list-group-item">
                                    <i class="fa fa-male text-danger"></i>
                                    <b>@lang('commonField.personal_information.farmer')</b>
                                    <div class="pull-right">{{ isset($occCount) ? $occCount : '' }}</div>
                                </li>
                                <!--  -->
                                <li class="list-group-item">
                                    <i class="fa fa-male"></i>
                                    <b>
                                       
                                       @lang('commonField.houseHoldSetting.maleJobHolder')
                                    </b>
                                    <div class="pull-right">{{ isset($maleJobHoder) ? $maleJobHoder : '' }}</div>
                                </li>
                                <!--  -->
                                <li class="list-group-item">
                                    <i class="fa fa-male"></i>
                                    <b>
                                       @lang('commonField.houseHoldSetting.femaleJobHolder')
                                    </b>
                                    <div class="pull-right">{{ isset($femaleJobHolder) ? $femaleJobHolder : '' }}</div>
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-male"></i>
                                    <b>
                                       @lang('commonField.houseHoldSetting.otherJobHolder')
                                    </b>
                                    <div class="pull-right">{{ isset($otherJobHolder) ? $otherJobHolder : '' }}</div>
                                </li>

                            </ul>
                        </div>
                </div>
        </div>
@endif
@endsection
@section('custom_script')
   <script type="text/javascript">
        $(document).ready(
                    function() {
                        setInterval(function() {
                           
                        }, 3000);
                    });
   </script>
@endsection