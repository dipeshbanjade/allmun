@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
	<div class="card-box">
		<div class="card-body">
		<div class="card-head">
			<header>
			   <h3><i class="fa fa-eye"></i>&nbsp;@lang('commonField.extra.ward')</h3>
			</header>

		</div>
			<a href="{{ route('wardDetails.create') }}" type="button" class="btn btn-primary pull-right"><i class="fa fa-plus"></i><span> @lang('commonField.extra.createWard')</span></a> 
			<div class="table-scrollable" id="dropLabel">
			  @if(count($wardDetails) > 0)
                @lang('commonField.address_information.wardNo') : <span>{{ count($wardDetails) }}</span>
				<table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
					<thead>
						<tr>
							<th>@lang('commonField.extra.sn')</th>
							<th>@lang('commonField.extra.ward')</th>
							<th>@lang('commonField.personal_information.landLineNumber')</th>
							<th>@lang('commonField.personal_information.address')</th>
							<th>@lang('commonField.extra.muncipilityHead') </th>
							<th>@lang('commonField.extra.action') </th>
						</tr>
					</thead>
					<tbody>
                        @php $count = 1   @endphp
                        @foreach($wardDetails as $ward)
						<tr class="odd gradeX">
							<td width="3%">{{ $count ++ }}</td>
							<td>{{ $lang =='np' ? $ward->nameNep : $ward->nameEng  }}</td>
							<td>{{ $ward->landLineNumber }}</td>
							<td>{{ $ward->addrNep }} - {{ $ward->addrNep }}</td>
							<td>{{ $ward->muncipilityHead }}</td>
							 <td>
								<a href="{{ route('wardDetails.edit' , $ward->identifier) }}" class="btn btn-primary btn-xs" title="edit ward details">
									<i class="fa fa-pencil"></i>
								</a>
								<!--  -->
								<a href="" class="delete_data" id="">
									<button class="btn btn-danger btn-xs">
										<i class="fa fa-trash-o "></i></button>
									</a>
						    </td>
						</tr>
                        @endforeach
						</tbody>
					</table>
					{{ $wardDetails->links() }}
			  @endif
				</div>
			</div>
		</div>
	</div>
@endsection
@section('custom_script')
   @include('admin.ward.wardDetails.script')
@endsection