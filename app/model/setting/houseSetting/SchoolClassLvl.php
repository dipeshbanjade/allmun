<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class SchoolClassLvl extends Model
{
    protected $table = 'school_class_lvls';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
