<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php
          $envProblem = isset($envProblem) ? $envProblem : '';
          $jsonEnv    = isset($envProblem->environmentProblem) ? json_decode($envProblem->environmentProblem) : '';

        ?>
            <div class="row color_red">
                <div class="form-group col-md-12">
                    <p>
                       ७. तपाईको परिवार बसोवास गरेको स्थान वरीपरिको मुख्य वातावरणीय समस्या के रहेको छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline">
                            <input type="checkbox" name="environmentProblem[]" value="प्रदुषित पानी" {{ !empty($envProblem->environmentProblem) && in_array('प्रदुषित पानी', $jsonEnv) ? 'checked' : '' }}>
                            १. प्रदुषित पानी
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="environmentProblem[]" value="अव्यवस्थित ठोस फोहोर" {{ !empty($envProblem->environmentProblem) && in_array('अव्यवस्थित ठोस फोहोर', $jsonEnv) ? 'checked' : '' }}>
                            २. अव्यवस्थित ठोस फोहोर
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="environmentProblem[]" value="अव्यवस्थित ढल निकास" {{ !empty($envProblem->environmentProblem) && in_array('अव्यवस्थित ढल निकास', $jsonEnv) ? 'checked' : '' }}>
                            ३. अव्यवस्थित ढल निकास
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="environmentProblem[]" value="धुलो धुवाँ" {{ !empty($envProblem->environmentProblem) && in_array('धुलो धुवाँ', $jsonEnv) ? 'checked' : '' }}>
                            ४. धुलो धुवाँ
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="environmentProblem[]" value="ध्वनी प्रदुषण" {{ !empty($envProblem->environmentProblem) && in_array('ध्वनी प्रदुषण', $jsonEnv) ? 'checked' : '' }}>
                            ५. ध्वनी प्रदुषण
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="environmentProblem[]" value="जथाभावी पार्किङ" {{ !empty($envProblem->environmentProblem) && in_array('जथाभावी पार्किङ', $jsonEnv) ? 'checked' : '' }}>
                            ६. जथाभावी पार्किङ
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="environmentProblem[]" value="भिडभाड" {{ !empty($envProblem->environmentProblem) && in_array('भिडभाड', $jsonEnv) ? 'checked' : '' }}>
                            ७. भिडभाड 
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="environmentProblem[]" value="कुनै समस्या नभएको" {{ !empty($envProblem->environmentProblem) && in_array('कुनै समस्या नभएको', $jsonEnv) ? 'checked' : '' }}>
                           ८. कुनै समस्या नभएको
                        </label><br />

                        <label class="radio-inline">
                             {{ Form::text('otherEnvProblemName', isset($envProblem->otherEnvProblemName) ? $envProblem->otherEnvProblemName : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
                        </label>
                        <div class="msgDisplay"></div>
                    </div>
                </div> 
            </div>
    </div>
</div>