@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
	       <i class="fa fa-plus"></i>
	 </div>

	 <div class="card-head">
            <header>Daily Report</header>
	           {{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	         </div>

     
             {{-- 
                
<div class="right_col dash-pg" role="main" style="min-height: 900px;">
  <div class="clearfix"></div>
  <div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">दैनिक रिपोर्ट</h3>
          
          <div class="clearfix"></div>
        </div>

        <div class="x_content" id="block">

         <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">

            <li role="presentation" class="active">
              <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"> पालिका</a>
            </li>




            <li role="presentation" class="">
              <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">वार्ड</a>
            </li>

          </ul>

          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

              <form class="form-inline" id="palika_daily_report_filter_form">
                <fieldset>
                  <legend></legend>

                <div class="form-group mx-sm-3 mb-1">
                  <label for="text">मिति &nbsp;</label>
                  <input type="text" class="form-control ndp-nepali-calendar date-input-field" id="from_date" name="from_date" onfocus="showNdpCalendarBox('from_date')"> 
                </div>
                <div class="form-group mx-sm-3 mb-1">
                  <label for="text">देखि &nbsp;</label>

                  <input type="text" class="form-control ndp-nepali-calendar date-input-field" id="to_date" name="to_date" onfocus="showNdpCalendarBox('to_date')"> 
                  <label for="text">सम्म &nbsp;</label>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                  <button type="submit" class="  btn btn-primary bttn"><span class="glyphicon glyphicon-search"></span> खोज्नुहोस</button>
                </div>
              </fieldset>
            </form>

            <hr>
            <div id="palika_daily_report_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dt-buttons btn-group"><a class="btn btn-default buttons-copy buttons-html5" tabindex="0" aria-controls="palika_daily_report" href="#"><span>Copy</span></a><a class="btn btn-default buttons-csv buttons-html5" tabindex="0" aria-controls="palika_daily_report" href="#"><span>CSV</span></a><a class="btn btn-default buttons-excel buttons-html5" tabindex="0" aria-controls="palika_daily_report" href="#"><span>Excel</span></a><a class="btn btn-default buttons-print" tabindex="0" aria-controls="palika_daily_report" href="#"><span>Print</span></a></div><div id="palika_daily_report_processing" class="dataTables_processing panel panel-default" style="display: none;">Processing...</div><table class="table table-bordered table-striped dataTable no-footer" id="palika_daily_report" role="grid">
              <caption id="palika_caption">ईलाम नगरपालिका को आर्थिक वर्ष २०७५/०७६ मा  मिति२०७५-०५-२१ देखि२०७५-०५-२१ सम्मको जारी सिफारिस रिपोर्ट</caption>

              <thead>
                <tr role="row"><th rowspan="2" class="sorting" tabindex="0" aria-controls="palika_daily_report" colspan="1" aria-label=" मिति : activate to sort column ascending"> मिति </th><th colspan="2" rowspan="1">सिफारिस</th><th colspan="2" rowspan="1">प्रमाणित</th><th colspan="2" rowspan="1">दर्ता</th><th colspan="2" rowspan="1">नविकरण</th><th colspan="2" rowspan="1">अन्य</th><th colspan="2" rowspan="1">जम्मा</th></tr>
                <tr role="row"><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="संख्या">संख्या</th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="दस्तुर">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="palika_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th></tr>
              </thead>
              <tbody>

              <tr class="odd"><td valign="top" colspan="13" class="dataTables_empty">No data available in table</td></tr></tbody>
            </table></div>

          </div>



          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
           <form class="form-inline" id="ward_daily_report_filter_form">
            <fieldset>
              <legend> </legend>         


             <div class="form-group mx-sm-3 mb-2" id="ward_no_section">
              <label for="text">वार्ड &nbsp;</label>


              <select onchange="changeSelect(this)" name="ward_no" class="form-control" id="ward_no">
<option value="" selected="selected">---सबै---</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
</select>
            </div>
            <div class="form-group mx-sm-3 mb-1">
              <label for="text">मिति &nbsp;</label>
              <input type="text" class="form-control ndp-nepali-calendar date-input-field" id="ward_from_date" name="from_date" onfocus="showNdpCalendarBox('ward_from_date')"> 
            </div>
            <div class="form-group mx-sm-3 mb-1">
              <label for="text">देखि &nbsp;</label>

              <input type="text" class="form-control ndp-nepali-calendar date-input-field" id="ward_to_date" name="to_date" onfocus="showNdpCalendarBox('ward_to_date')"> 
              <label for="text">सम्म &nbsp;</label>
            </div>



            <div class="form-group mx-sm-3 mb-2">
              <button type="submit" class="  btn btn-primary bttn"><span class="glyphicon glyphicon-search"></span> खोज्नुहोस</button>
            </div>
          </fieldset>
        </form>
        <hr>



        <div id="ward_daily_report_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dt-buttons btn-group"><a class="btn btn-default buttons-copy buttons-html5" tabindex="0" aria-controls="ward_daily_report" href="#"><span>Copy</span></a><a class="btn btn-default buttons-csv buttons-html5" tabindex="0" aria-controls="ward_daily_report" href="#"><span>CSV</span></a><a class="btn btn-default buttons-excel buttons-html5" tabindex="0" aria-controls="ward_daily_report" href="#"><span>Excel</span></a><a class="btn btn-default buttons-print" tabindex="0" aria-controls="ward_daily_report" href="#"><span>Print</span></a></div><div id="ward_daily_report_processing" class="dataTables_processing panel panel-default" style="display: none;">Processing...</div><table class="table table-bordered table-striped dataTable no-footer" id="ward_daily_report" role="grid">
          <caption id="caption">ईलाम नगरपालिका सबै वडा मा आर्थिक वर्ष २०७५/०७६ मा  मिति२०७५-०५-२१ देखि२०७५-०५-२१ सम्मको जारी सिफारिस रिपोर्ट</caption>
          <thead>
            <tr role="row"><th rowspan="2" id="table_title_ward" class="sorting" tabindex="0" aria-controls="ward_daily_report" colspan="1" aria-label="वार्ड: activate to sort column ascending">वार्ड</th><th colspan="2" rowspan="1">सिफारिस</th><th colspan="2" rowspan="1">प्रमाणित</th><th colspan="2" rowspan="1">दर्ता</th><th colspan="2" rowspan="1">नविकरण</th><th colspan="2" rowspan="1">अन्य</th><th colspan="2" rowspan="1">जम्मा</th></tr>
            <tr role="row"><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="संख्या">संख्या</th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="दस्तुर">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="संख्या: activate to sort column ascending">संख्या</th><th class="sorting" tabindex="0" aria-controls="ward_daily_report" rowspan="1" colspan="1" aria-label="दस्तुर: activate to sort column ascending">दस्तुर</th></tr>
          </thead>
          <tbody>

          <tr role="row" class="odd"><td>१</td><td>३</td><td></td><td>३</td><td>३००</td><td></td><td></td><td></td><td></td><td></td><td></td><td>६</td><td>३००</td></tr><tr role="row" class="even"><td>२</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="odd"><td>३</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="even"><td>४</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="odd"><td>५</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="even"><td>६</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="odd"><td>७</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="even"><td>८</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="odd"><td>९</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="even"><td>१०</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="odd"><td>११</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="even"><td>१२</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr role="row" class="odd"><td>जम्मा</td><td>३</td><td></td><td>३</td><td>३००</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody>
        </table></div>
      </div>


    </div>
  </div>






</div>
</div>
</div>
</div>
</div>

                
                --}}


	 </div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
	 @include('admin.shifaris.script')
@endsection