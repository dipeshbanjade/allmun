@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
	       <i class="fa fa-plus"></i>
	 </div>

	 <div class="card-head">
            <header>Register Report</header>
	           {{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	         </div>


{{--
    
    <div class="right_col dash-pg" role="main" style="min-height: 900px;">
        <div class="clearfix"></div>
        <div class="row">
      
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
      
              <div class="x_title">
                <h3 class="title-x">दर्ता रिपोर्ट</h3>
                
                <div class="clearfix"></div>
              </div>
              <div class="x_content" id="block">
                <form action="" id="darta_report_form" method="post">
                  <div class="row">
      
                     <div class="col-md-2">
                     <label>पालिका/वार्ड </label>
                     <select onchange="changeSelect(this)" name="palika_ward" id="palika_ward" class="form-control">
                       <option value="palika">पालिका</option>
                       <option value="ward">वार्ड</option>
                       
                     </select>
                   </div>
      
      
                   <div id="ward_no_section" style="display: none;">
                     <div class="col-md-2">
                       <label>वार्ड नं.:</label>
                       <select onchange="changeSelect(this)" name="palika_ward_no" class="form-control" id="palika_ward_no">
      <option value="1">१</option>
      <option value="2">२</option>
      <option value="3">३</option>
      <option value="4">४</option>
      <option value="5">५</option>
      <option value="6">६</option>
      <option value="7">७</option>
      <option value="8">८</option>
      <option value="9">९</option>
      <option value="10">१०</option>
      <option value="11">११</option>
      <option value="12">१२</option>
      </select>
                    </div>
                  </div>
      
      
                    <div class="col-md-2">
                     <label>मिति(देखि)</label>
      
                     <input type="text" name="date_from" id="date_from" class="form-control ndp-nepali-calendar date-input-field" onfocus="showNdpCalendarBox('date_from')">
      
                   </div>
      
                   <div class="col-md-2">
                     <label>मिति(सम्म)</label>
      
                     <input type="text" name="date_to" id="date_to" class="form-control ndp-nepali-calendar date-input-field" onfocus="showNdpCalendarBox('date_to')">
      
                   </div>
      
                  
                  <div class="col-md-2">
                    <label style="display: block">&nbsp;</label>
                    <button class="btn bttn btn-primary pull-right" name="get_printed_sifaris"> <span class="glyphicon glyphicon-search"></span> खोज्नुहोस</button>
                  </div>
                </div><!--row-->
      
      
              </form>
              <div id="darta_report_table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="dt-buttons btn-group"><a class="btn btn-default buttons-copy buttons-html5" tabindex="0" aria-controls="darta_report_table" href="#"><span>Copy</span></a><a class="btn btn-default buttons-csv buttons-html5" tabindex="0" aria-controls="darta_report_table" href="#"><span>CSV</span></a><a class="btn btn-default buttons-excel buttons-html5" tabindex="0" aria-controls="darta_report_table" href="#"><span>Excel</span></a><a class="btn btn-default buttons-print" tabindex="0" aria-controls="darta_report_table" href="#"><span>Print</span></a></div><div id="darta_report_table_processing" class="dataTables_processing panel panel-default" style="display: none;">Processing...</div><table class="table table-striped content-para table-bordered dataTable no-footer" id="darta_report_table" role="grid">
                <caption id="caption">ईलाम नगरपालिका को आर्थिक वर्ष २०७५/०७६ मा  मिति २०७५-०५-२१ देखि मिति २०७५-०५-२१ सम्म को दर्ता रिपोर्ट</caption>
                <thead>
                  <tr role="row"><th class="sorting" tabindex="0" aria-controls="darta_report_table" rowspan="1" colspan="1" aria-label="क्र.स.: activate to sort column ascending">क्र.स.</th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="मिति ">मिति </th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="दर्ता भएको पत्र संख्या ">दर्ता भएको पत्र संख्या </th></tr></thead>
      
                <tbody>
                <tr class="odd"><td valign="top" colspan="3" class="dataTables_empty">No data available in table</td></tr></tbody>
              </table></div>
            </div>
          </div>
      
        </div>
      </div>
      </div>
      
      
      --}}

    </div>

    @endsection
    @section('custom_script')
         @include('admin.shifaris.script')
    @endsection