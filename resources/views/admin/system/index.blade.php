@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
 <div class="col-sm-12">
  <div class="card-box">
    <div class="card-head">
     <header>Activity Log Files</header>
   </div>
   <div class="card-body">
    <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
 
      <div class="table-scrollable">
         <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example4">
           <thead>
             <tr>
               <th>SN</th>
               <th>user</th>
               <th>Activity</th>
               <th>Perform On</th>
               <th>Action</th>
             </tr>
           </thead>
           <tbody>
             @php  $count = 1  @endphp
             @if(count($logData) > 0)
                @foreach($logData as $log)
                  <tr>
                    <td>{{ $count ++ }}</td>
                    <td>{!! $log->users_id ? $log->users->name : '' !!}</td>
                    <td>{{ $log->action }}</td>
                    <td>{{ $log->created_at->diffForHumans() }}</td>
                    <td>
                        <a href="javascript.void(0)" class="deleteMe" data-url="{!! route('deleteLogFile', $log->id ) !!}" title="delete log activitiy" data-name="{{ $log->action }}" data-id = "{{ $log->id }}"> 

                           <i class="fa fa-trash-o btn-danger"></i>
                      </a>
                    </td>
                    </tr>
                @endforeach
             @endif
             
             </tbody>
           </table>
           {{ $logData->links() }}

           </div>
         
       </div>
     </div>
   </div>
 </div>
@endsection


@section('custom_script')
<script type="text/javascript">
 
</script>
@endsection