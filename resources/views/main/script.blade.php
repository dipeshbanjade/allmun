<script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('lib/popper/popper.js') }}"></script>
<script src="{{ asset('lib/jquery-blockui/jquery.blockui.min.js') }}"></script>
<script src="{{ asset('lib/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<!-- bootstrap -->
<script src="{{ asset('lib/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('lib/js/bootstrap-switch.min.js') }}"></script>
<script src="{{ asset('lib/sparkline/jquery.sparkline.js') }}"></script>
<script src="{{ asset('lib/sparkline/sparkline-data.js') }}"></script>
<!-- Common js-->
<script src="{{ asset('lib/js/app.js') }}"></script>
<script src="{{ asset('lib/js/layout.js') }}"></script>
<script src="{{ asset('lib/js/theme-color.js') }}"></script>
<!-- material -->
<script src="{{ asset('lib/material/material.min.js') }}"></script>
<!-- chart js -->
<script src="{{ asset('lib/chart-js/Chart.bundle.js') }}"></script>
<script src="{{ asset('lib/chart-js/utils.js') }}"></script>
<script src="{{ asset('lib/chart-js/home-data.js') }}"></script>
<script src="{{ asset('lib/chart-js/home-data2.js') }}"></script>
<!-- summernote -->
<script src="{{ asset('lib/summernote/summernote.js') }}"></script>
<script src="{{ asset('lib/summernote/summernote-data.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ asset('nepalidatepicker/nepali.datepicker.v2.2.min.js') }}"></script>
<script src="{{ asset('lib/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('lib/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('lib/datatables/table_data.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>

<script type="text/javascript">
	$('#example4').DataTable({
		dom: 'frt',
		"bPaginate": false,
	});
	$('#withOutcitizen').DataTable({
		dom: 'frt',
		"bPaginate": false,
	});
    $( function() {  // changing color on click in tab le i
      $('tr').click( function() {
          if($(this).attr('style'))
        $(this).removeAttr('style');
          else
        $(this).css('background', '#6673FC')
      } );
    } );

	$('.sysSelect2').select2();
	$('.select2Tbl').select2();
	/*customer search */
	var infoDiv   = $('.infoDiv');
	var changeStatus = $('.changeStatus');
	changeStatus.on('click', function(e){
		e.preventDefault();
		var status = $(this).data('status');
		var url    = $(this).data('url');
		var id     = $(this).data('id');
		var data = {id:id, status:status};
		if (confirm("Are you sure you want to change status")) {
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				'data' : data,
				success:function(response){
					infoDiv.addClass('alert alert-success').append(response.message);
					setTimeout(location.reload.bind(location), 1500);
				}
			}).fail(function (response){
				infoDiv.addClass('alert alert-danger').append(response.message);
			});
		}
		return false;
	});
	/*deleting cde*/
	$(".deleteMe").click(function(e) {
		var url = $(this).data('url');
		alert(url);
		var name = $(this).data('name');
		var infoDiv   = $('.infoDiv');
		if (confirm("Are you sure your want to  delete  " + name +'?')) {
			$.ajax({
				'type' : "GET",
				'url'  : url,
				success:function(response){
					console.log(response.message);
					if (response.success == true) {
						infoDiv.addClass('alert alert-success').append(response.message);
					}else{
						infoDiv.addClass('alert alert-danger').append(response.message);
					}
					setTimeout(location.reload.bind(location), 1500);
				}
			});
		}
		return false;
		e.preventDefault();
	});

	$('.client-info').fadeOut(30000);

	/*chnageLanguage*/
	var changeLanguage = $('.changeLanguage');
	changeLanguage.on('click', function(){
		var url = $(this).data('url');
		var val = $(this).data('val');

		if (confirm("Are you sure your want to change language")) {
			$.ajax({
				'type' : "GET",
				'url'  : url,
				success:function(response){
					console.log(response.message);
					if (response.success == true) {
						infoDiv.addClass('alert alert-success').append(response.message);
					}else{
						infoDiv.addClass('alert alert-danger').append(response.message);
					}
					setTimeout(location.reload.bind(location), 3000);
				}
			});
		}
		return false;
	});

	/*summer note*/
	$('.summernote').summernote({
		height: 200,
		toolbar: [
		['style', ['style']],
		['font', ['bold', 'italic', 'underline', 'clear']],
		['fontname', ['fontname']],
		['color', ['color']],
		['para', ['ul', 'ol', 'paragraph']],
		['height', ['height']],
		['table', ['table']],
		['insert', ['link', 'picture', 'hr']],
		['view', ['fullscreen', 'codeview', 'help']]

		]
	});

	/*summer note*/

	$('#sdob').nepaliDatePicker({
		ndpEnglishInput: 'sdobEng',
		npdMonth: true,
		npdYear: true
		// disableBefore: '2075'
	});

	/*------------*/
	$('#wrdStaffDob').nepaliDatePicker({
		ndpEnglishInput: 'wrdStaffEng',
		npdMonth: true,
		npdYear: true
		// disableBefore: '2075'
	});

	$('#myDate').nepaliDatePicker({
		ndpEnglishInput: 'myEngDate',
		npdMonth: true,
		npdYear: true
		// disableBefore: '12/08/2075'
	});

	$('#myDate2').nepaliDatePicker({
		npdMonth: true,
		npdYear: true
		// disableBefore: '12/08/2075'
	});

	$('#myDate3').nepaliDatePicker({
		npdMonth: true,
		npdYear: true
		// disableBefore: '12/08/2075'
	});

	$('#myDate4').nepaliDatePicker({
		npdMonth: true,
		npdYear: true
		// disableBefore: '12/08/2075'
	});
	$('#myDate5').nepaliDatePicker({
		npdMonth: true,
		npdYear: true
		// disableBefore: '12/08/2075'
	});

	$('#myDate6').nepaliDatePicker({
		npdMonth: true,
		npdYear: true
		// disableBefore: '12/08/2075'
	});

	$('#myDate7').nepaliDatePicker({
		npdMonth: true,
		npdYear: true
		// disableBefore: '12/08/2075'
	});

	// $('#nepDate').nepaliDatePicker();
    $('#nepDate').nepaliDatePicker({
		ndpEnglishInput: 'lvEng'
		// disableBefore: '12/08/2075'
	});
    $('#nepDate2').nepaliDatePicker({
		ndpEnglishInput: 'lvEng2'
		// disableBefore: '12/08/2075'
	});

	$('#currNepDate').nepaliDatePicker({
		disableDaysBefore: '1'
	});

	$('.infoDiv').fadeOut(30000);


	$('#nDate').nepaliDatePicker();

	/*image updatte*/
	function displayImage(input, divId){
		var divId = divId;
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#'+divId)
				.attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	function printDiv(divName) {
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
	}


	$(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready

// breakpoint and up  
$(window).resize(function(){
	if ($(window).width() >= 980){	

      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
      	$(this).parent().toggleClass("show");
      	$(this).parent().find(".dropdown-menu").toggleClass("show"); 
      });

        // hide the menu when the mouse leaves the dropdown
        $( ".navbar .dropdown-menu" ).mouseleave(function() {
        	$(this).removeClass("show");  
        });

		// do something here
	}	
});  
// document ready  
});

$('.calAge').nepaliDatePicker({
    npdMonth:true,
    npdYear:true,
  onChange: function(){
    var calAge= BS2AD($('.calAge').val());
    var dob= new Date(calAge)
    var ageDiff=Date.now()- dob.getTime();
    var ageDate= new Date(ageDiff);
    return Math.abs(ageDate.getUTCFullYear()-1970);
  }
});

function getMyDob($myAge){
   var calAge= $myAge;
   var dob= new Date(calAge)
    var ageDiff=Date.now()- dob.getTime();
    var ageDate= new Date(ageDiff);
    // alert(Math.abs(ageDate.getUTCFullYear()-1970));
    return Math.abs(ageDate.getUTCFullYear()-1970);
}

$(document).ready(function(){  //open tab when it is save
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
         $('#IDOFTAB a[href="' + activeTab + '"]').tab('show');
    }
});
</script>