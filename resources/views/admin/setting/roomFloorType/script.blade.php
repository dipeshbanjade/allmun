<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditroomFloorType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of Room Floor Type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmroomFloorType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of Room Floor Type is required"
            }
        });
  var btnAddRoomFloorType = $('.btnAddRoomFloorType');
  btnAddRoomFloorType.on('click', function(e){
      $('#roomFloorTypeModel').modal('show'); 
  });

  var btnEditroomFloorType = $('.btnEditroomFloorType');
  btnEditroomFloorType.on('click', function(e) {
      e.preventDefault();
      var url = $(this).data('url');
      $.ajax({
        'type': 'GET',
        'url': url,
        success: function (response) {
            $('.roomFloorTypeId').val(response.id);
            $('.txtRefCode').val(response.refCode).attr('disabled',true);
            $('.txtNameNep').val(response.nameNep);
            $('.txtNameEng').val(response.nameEng);
            $("#editRoomFloorTypeModel").modal('show');
        }
    })
  });


  var frmUpdateroomFloorType = $('#frmUpdateroomFloorType');
  frmUpdateroomFloorType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.roomFloorTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/roomFloorType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editRoomFloorTypeModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editRoomFloorTypeModel').modal('hide');
});
});


</script>