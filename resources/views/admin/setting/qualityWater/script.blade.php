<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditqualityWater']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of Quality Water is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmqualityWater']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of Quality Water is required"
            }
        });
	var btnAddqualityWater = $('.btnAddqualityWater');
	btnAddqualityWater.on('click', function(e){
		$('#qualityWaterModel').modal('show'); 
	});

	var btnEditqualityWater = $('.btnEditqualityWater');
	btnEditqualityWater.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.qualityWaterId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editqualityWaterModel").modal('show');
			}
		})
	});

	var frmUpdatequalityWater = $('#frmUpdatequalityWater');
  frmUpdatequalityWater.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.qualityWaterId').val();
    var url  = "{{URL::to('/')}}" + "/setting/qualityWater/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editqualityWaterModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editqualityWaterModel').modal('hide');
});
});
</script>