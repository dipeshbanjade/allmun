<script type="text/javascript">
	$(function(){
		$("form[name='frmEditStaffType']").validate({
			rules:{
				nameEng : {
					required: true,
					minlength : 3,
					maxlength : 60
				},
				refCode : {
					required : true
				}
			},
			messages: {
				nameEng  : "visit type is required with minimum 3 character and maximum 60 character",
				refCode  : "refCode required"
			}
		});
	});

	/*validation */
	$(function(){
		$("form[name='frmAddStaffType']").validate({
			rules:{
				nameEng : {
					required: true,
					minlength : 3,
					maxlength : 60
				},
				refCode : {
					required : true
				}
			},
			messages: {
				nameEng  : "visit type is required",
				refCode  : "refCode required"
			}
		});

		var btnStaffType = $('#btnStaffType');
		btnStaffType.on('click', function(e){
			e.preventDefault();
			$('#modelAddStaffType').modal('show');
		});


		var btnEditStaffType = $('.btnEditStaffType');
		btnEditStaffType.on('click', function(e){
			e.preventDefault();
			var url = $(this).data('url');
			$.ajax({
				'type': 'GET',
				'url': url,
				success: function (response) {
					console.log(response);
					$('.staffTypeId').val(response.id),
					$('.txtnameNep').val(response.nameNep);
					$('.txtnameEng').val(response.nameEng);
					$("#modelUpdateStaffType").modal('show');
				}
			})
		});

		var frmUpdateStaffType = $('#frmUpdateStaffType');
		frmUpdateStaffType.on('submit', function(e){
			e.preventDefault();
			var data = $(this).serialize();
			var id   = $('.staffTypeId').val();
			var url  = "{{URL::to('/')}}" + "/setting/staffType/"+id+"/update";
			$.ajax({
				'type' : 'POST',
				'url'  : url,
				'data'    : data,
				success : function(response){
					console.log(response);
					if (response.success==true) {
						$('.infoDiv').append(response.message).addClass('alert alert-success');
					}
					if (response.success==false) {
						$.each(response.message, function(key, value){
							$('.infoDiv').append(value).addClass('alert alert-danger');
						})
					}
				},complete:function(){
					$("#modelUpdateStaffType").modal('hide');
					setTimeout(location.reload.bind(location), 1500);
				}
			})
			.fail(function (response) {
				alert('data already exists');
			});
			$('#modelUpdateStaffType').modal('hide');
		});
	});
</script>