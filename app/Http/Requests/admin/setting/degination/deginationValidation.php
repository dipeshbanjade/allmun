<?php

namespace App\Http\Requests\admin\setting\degination;

use Illuminate\Foundation\Http\FormRequest;

class deginationValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'refCode'  => 'required|min:4|max:30|unique:deginations',
            'nameEng'  => 'required|min:4|max:100',
            'nameNep'  => 'min:4|max:100'
        ];
    }
}
