<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldVisitsTable extends Migration
{
    public function up()
    {
        Schema::create('field_visits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode', 100)->unique();
            $table->string('startDate');
            $table->string('startTime')->nullable();
            $table->string('endDate');
            $table->string('endTime')->nullable();
            $table->integer('visit_types_id')->nullable()->unsigned();
            $table->foreign('visit_types_id')->references('id')->on('visit_types')->onDelete('cascade');

            $table->integer('staff_municipilities_id')->unsigned()->nullable();
            $table->foreign('staff_municipilities_id')->references('id')->on('staff_municipilities')->onDelete('cascade');

            $table->integer('wards_staff_id')->unsigned()->nullable();
            $table->foreign('wards_staff_id')->references('id')->on('ward_staffs')->onDelete('cascade'); 

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('createBy')->nullable()->unsigned();
            $table->foreign('createBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('approveBy')->nullable()->unsigned();
            $table->foreign('approveBy')->references('id')->on('users')->onDelete('cascade');

            $table->text('shortDesc');
            $table->boolean('approveStatus')->default(0);
            $table->boolean('status')->default(0); 
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('field_visits');
    }
}
