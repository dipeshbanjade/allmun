<?php

namespace App\model\admin\log;

use Illuminate\Database\Eloquent\Model;

class LogFile extends Model
{
    protected $table = 'log_files';

    public function users(){
	   return $this->belongsTo('App\User'::class);
    }
}
