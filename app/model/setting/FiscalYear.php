<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class FiscalYear extends Model
{
    protected $table = 'fiscal_years';
    protected $fillable = ['startDate', 'endDate', 'startShort', 'endShort', 'status', 'softDelete'];
}
