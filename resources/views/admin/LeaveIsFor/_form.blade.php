<fieldset>
    <div class="row alert alert-default">
        <div class="col-md-4 col-lg-4 col-sm-12">
            <label>
                 @lang('commonField.leaveIsFor.nepDate')
            </label>
             {{ Form::text('dateFromNep', null, ['class'=>'mdl-textfield__input','autocomplete' => 'off','placeholder'=>'holiday start from', 'id' => 'myDate', 'required', 'title'=>'holiday date']) }}
        </div>
        <div class="col-md-4 col-lg-4 col-sm-12">
             <label>
                 @lang('commonField.leaveIsFor.engDate')
             </label>
             {{ Form::text('dateFromEng', null, ['class'=>'mdl-textfield__input','autocomplete' => 'off', 'placeholder'=>'date in english', 'id'=> 'myEngDate', 'readonly','required', 'title'=>'date in english']) }}
        </div>
        <div class="col-md-4 col-lg-4 col-sm-12">
             <label>
                 @lang('commonField.leaveIsFor.leaveTitle')
             </label>
             {{ Form::text('leaveFor', null, ['class'=>'mdl-textfield__input','placeholder'=> 'leave title', 'id'=>'leaveTitle', 'title'=>'leave title', 'required']) }}

             {{ Form::hidden('hiddenId', null, ['class'=>'hiddenId']) }}
        </div>
    </div>
</fieldset>
