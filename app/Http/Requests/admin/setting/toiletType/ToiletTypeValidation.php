<?php

namespace App\Http\Requests\admin\setting\toiletType;

use Illuminate\Foundation\Http\FormRequest;

class ToiletTypeValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:toilet_types',
            'nameEng' => 'required|min:4|max:50'
        ];
    }
}
