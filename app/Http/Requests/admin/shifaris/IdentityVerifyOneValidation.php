<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class IdentityVerifyOneValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
           'refCode' => 'required|min:3',
           'issuedDate' => 'required',
           'letterSub' =>'required|min:3|max:50',
           'appName' => 'required|min:3|max:50',
           'appTitle' => 'required',
           'municipality' =>'required|min:3|max:50',
           'ward' => 'required',
           'district' => 'required',
           'former' =>'required',
           'orgAddr' => 'required|min:3|max:40',
           'orgType' => 'required',
           'orgWard' =>'required',
           'docName' => 'required|min:3|max:50',
           'docNo' => 'required',
           'userName' =>'required',
           'userNameTwo' => 'required',
           'docNameTwo' => 'required',
           'docNoTwo' =>'required',
           'docDate' => 'required',
           'genderType' => 'required',
           'genderTypeTwo' =>'required',
           'authorizedPerson' => 'required',
           'deginations_id' => 'required'
       ];
   }
}
