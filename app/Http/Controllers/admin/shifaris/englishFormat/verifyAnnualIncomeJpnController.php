<?php

namespace App\Http\Controllers\admin\shifaris\englishFormat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\AnnualIncomeVerifyJapan;
use App\model\shifaris\ApplicantUser;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\verifyAnnualIncomeJpnVal;

use App\model\mylogic;
use DB;
use Auth;


class VerifyAnnualIncomeJpnController extends Controller
{
    protected $viewPath = 'admin.shifaris.englishFormat.verifyAnnualIncomeJpn';
    protected $shifarisTableName     = 'annual_income_verify_japans';
    protected $appUsrTable     = 'applicant_users';
    protected $deginationsTbl = 'deginations';
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.annualIncomeJapanPrefix');

    }

    public function index()
    {
        $page['page_title']       = 'Annual Income Verification for Japan';
        $page['page_description'] = 'Annual Income Verification for Japan';


        
        $annualIncome = ApplicantUser::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

    }

    public function create()
    {
        $page['page_title']       = 'Annual Income Verification for Japan : create';
        $page['page_description'] = 'Annual Income Verification for Japan details';

        $getUniqueCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);
        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationsTbl, $orderColumn = 'nameNep', $orderby = 'ASC');
        return view($this->viewPath . '.create', compact(['page', 'getUniqueCode', 'deginationsId']));

    }

    public function store(Request $request)
    {
        
       DB::beginTransaction();
       try {

        $annualIncomeJpnCount = count($request->incomeSource);

        $incomeSource = $request->incomeSource;
        $firstYear = $request->firstYear;
        $secondYear = $request->secondYear;
        $thirdYear = $request->thirdYear;
        $currency_type = $request->currency_type;
        // dd($request);

        $annualIncomeJpnArray = [];

        for ($i=0; $i < $annualIncomeJpnCount; $i++) {
            array_push($annualIncomeJpnArray, [
                'incomeSource' => $incomeSource[$i],
                'firstYear' => $firstYear[$i],
                'secondYear' => $secondYear[$i],
                'thirdYear' => $thirdYear[$i],
                'currency_type' => $currency_type,
            ]);
        }
        $annualIncomeJpnJson = json_encode($annualIncomeJpnArray);

        //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


        $isShifarisExist = isset($request->shifarisId) ? AnnualIncomeVerifyJapan::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? AnnualIncomeVerifyJapan::findOrfail($isShifarisExist['id']) : new AnnualIncomeVerifyJapan;

        $save->createdBy = Auth::user()->id;
        
        $save->refCode  = $request->refCode;

        $save->wards_id  = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";
        $save->chalaniNum  = $request->chalaniNum;
        $save->issuedDate  = $request->issuedDate;
        $save->title = $request->title;
        $save->personName  = $request->personName;
        $save->relationWithSeeker  = $request->relationWithSeeker;
        $save->fatherName  = $request->fatherName;
        
        $save->municipality  = $request->municipality;
        $save->ward  = $request->ward;
        $save->district  = $request->district;
        $save->former  = $request->former;
        $save->orgAddr  = $request->orgAddr;
        $save->orgType  = $request->orgType;
        $save->orgWard  = $request->orgWard;
        
        $save->japanAnnualIncomeDetail  = $annualIncomeJpnJson;

        // $save->incomeSource  = $request->incomeSource;
        // $save->firstYear  = $request->firstYear;
        // $save->secondYear  = $request->secondYear;
        // $save->thirdYear  = $request->thirdYear;
        
        $save->totalNrsInFirst  = $request->totalNrsInFirst;
        $save->totalNrsInSecond  = $request->totalNrsInSecond; 
        $save->totalNrsInThird  = $request->totalNrsInThird;
        $save->totalUsdInFirst  = $request->totalUsdInFirst;
        $save->totalUsdInSecond  = $request->totalUsdInSecond;
        $save->totalUsdInThird  = $request->totalUsdInThird;
        $save->total_nrs  = $request->total_nrs;
        $save->ctype  = $request->ctype;
        $save->totalUsd  = $request->totalUsd;  
        $save->exchangeRate  = $request->exchangeRate; 
        $save->authorizedPerson  = $request->authorizedPerson;
        $save->deginations_id  = $request->deginations_id;

        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

               //end for login
        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

        // $appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $save->id, $applicantName, $applicantAddress, $citizenshipNumber, $phoneNumber, $email, $dob, $fatherName, $profilePic, $citizenshipImgPath);


                    $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }


}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
