<div class="card-body row">
   <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('refCode', isset($prefix) ? $prefix : '', ['class'=>'mdl-textfield__input txtRefCode', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">@lang('commonField.extra.refCode')</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('nameNep', null, ['class'=>'UnicodeKeyboardTextarea mdl-textfield__input txtNameNep', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">@lang('commonField.personal_information.nameNep')
        </label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('nameEng', null, ['class'=>'mdl-textfield__input txtNameEng', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">@lang('commonField.personal_information.nameEng')</label>
    </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::textarea('desc', null, ['class'=>'mdl-textfield__input txtDesc', 'placeholder'=>'', 'rows'=> '3']) }}
        <label class = "mdl-textfield__label">@lang('commonField.extra.desc')</label>
    </div>
</div>