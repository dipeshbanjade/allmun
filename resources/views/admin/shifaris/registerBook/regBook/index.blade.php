@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
	       <i class="fa fa-plus"></i>
	 </div>

	 <div class="card-head">
            <header>Register Book</header>
	           {{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
             </div>
             

<div class="right_col dash-pg nep" role="main" style="min-height: 882px;">
    <div class="clearfix"></div>
    <div class="row">
  
  
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
  
          <div class="x_title">
            <h3 class="title-x">दर्ता किताब </h3>
            
            <div class="clearfix"></div>
          </div>
  
  
          <div class="x_content" id="block">
          <a href="{{ route('admin.regBook.create') }}" class="btn bttn btn-primary pull-right rgt-btm"> <span class="glyphicon glyphicon-plus"></span> नया रेकर्ड थप्नुहोस   </a>
   
   
              
            
  
  
          <div>
  <div class="clearfix"></div>
            <form class="form-inline " id="darta_book_search_form">
              <fieldset class="fieldseet">
              <legend>दर्ता किताब</legend>    
                <div class="row  ">
                  <div class="form-group col-md-2">
                    <label for="staticEmail2" class="sr-only">दर्ता नं.</label>
                    <input type="text" class="form-control" id="registration_no" name="registration_no" placeholder="दर्ता नं.">
                  </div>
                  <div class="form-group mx-sm-3 col-md-2 mb-2">
                    <label for="text" class="sr-only">दर्ता मिति</label>
                    <input type="text" class="form-control ndp-nepali-calendar date-input-field" id="registration_date" name="registration_date" placeholder="दर्ता मिति" onfocus="showNdpCalendarBox('registration_date')">
                  </div>
                  <div class="form-group mx-sm-3 col-md-3 mb-2">
                    <label for="text" class="sr-only">पत्र संख्या </label>
                    <input type="text" class="form-control" id="patra_sankya" name="patra_sankya" placeholder="पत्र संख्या">
                  </div>
                  <div class="form-group mx-sm-3 mb-2 col-md-3">
                    <label for="text" class="sr-only">पठाउने अफिसको नाम </label>
                    <input type="text" class="form-control" id="sender_office_name" name="sender_office_name" placeholder="पठाउने अफिसको नाम">
                  </div>
                  <div class="form-group mx-sm-3 mb-2 col-md-2">
                    <button type="submit" class="btn bttn btn-primary"><span class="glyphicon glyphicon-search"></span> खोज्नुहोस</button>
                  </div>
                </div>
              </fieldset>
            </form>
       
  
  <div class="row">
            <div class="col-md-12">
  
              <div class="table  responsive">
                <div id="darta-book-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table class="table table-bordered table-striped content-para dataTable no-footer" id="darta-book-table" role="grid" aria-describedby="darta-book-table_info">
  
                  <thead>
                    <tr role="row"><th rowspan="2" class="sorting" tabindex="0" aria-controls="darta-book-table" colspan="1">दर्ता नं. </th><th rowspan="2" class="sorting_disabled" colspan="1">दर्ता मिति </th><th colspan="2" rowspan="1">प्राप्त भएको </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="darta-book-table" colspan="1">पठाउने अफिसको नाम</th><th rowspan="2" class="sorting" tabindex="0" aria-controls="darta-book-table" colspan="1">विषय </th><th colspan="3" rowspan="1">बुझिलिने फाटवाला </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="darta-book-table" colspan="1">कैफियत </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="darta-book-table" colspan="1">दर्ता फाइलको फोटो </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="darta-book-table" colspan="1"> </th></tr>
                    <tr role="row"><th class="sorting" tabindex="0" aria-controls="darta-book-table" rowspan="1" colspan="1">पत्र संख्या </th><th class="sorting_disabled" rowspan="1" colspan="1">पत्रको मिति </th><th class="sorting" tabindex="0" aria-controls="darta-book-table" rowspan="1" colspan="1">नाम </th><th class="sorting" tabindex="0" aria-controls="darta-book-table" rowspan="1" colspan="1">सही </th><th class="sorting" tabindex="0" aria-controls="darta-book-table" rowspan="1" colspan="1">मिति </th></tr>
                  </thead>
                  <tbody>
  
  
  
                  </tbody>
                </table><div id="darta-book-table_processing" class="dataTables_processing panel panel-default" style="display: none;">Processing...</div></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="darta-book-table_info" role="status" aria-live="polite"></div></div><div class="col-sm-7"></div></div></div>
              </div>
            </div>
  
          </div>
  </div>
  
  
        </div>
      </div>
    </div>
  </div>
  
  </div>


{{-- Load layout --}}
</div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
	 @include('admin.shifaris.script')
@endsection