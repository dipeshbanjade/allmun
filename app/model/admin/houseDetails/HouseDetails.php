<?php

namespace App\model\admin\houseDetails;

use Illuminate\Database\Eloquent\Model;

class HouseDetails extends Model
{
    protected $table = 'house_details';

    public function citizen_infos(){
    	return $this->belongsTo('App\model\admin\citizen\CitizenInfo'::class);
    }
}