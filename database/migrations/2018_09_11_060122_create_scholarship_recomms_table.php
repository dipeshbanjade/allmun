<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScholarshipRecommsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scholarship_recomms', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->string('refCode', 100)->unique();
            
            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->date('issuedDate');
            $table->string('chalaniNum');
            $table->string('municipalityName');
            $table->string('organizationAddress');
            $table->string('organizationType');
            $table->string('wardNum');
            $table->string('organizationWard');
            $table->string('addressType');
            $table->string('fatherName');
            $table->string('motherName');
            $table->enum('economyStatus', ['कमजोर', 'न्युन']);
            $table->enum('childrenGender', ['छोरा', 'छोरी']);
            $table->enum('childrenTitle', ['श्री', 'सुश्री', 'श्रीमती']);
            $table->string('nameOfChildren');
            $table->text('tableData');

            $table->string('authorizedPerson');
            
            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade'); 

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');             
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scholarship_recomms');
    }
}
