<?php

namespace App\Http\Controllers\admin\ward\shifaris\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\model\admin\ward\union;

use App\model\mylogic;

class RegReportController extends Controller
{
    protected $viewPath = 'admin.ward.shifaris.report.regReport';
    // protected $wardTbl     = 'wards';
    // protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        // $this->prefix      = config('activityMessage.wardStaffPrefix');
 
    }

    public function index()
    {
        $page['page_title']       = 'Register Report';
        $page['page_description'] = 'Register Report';

        //TODO accordkign to ward user dispaly recored
        // $wardStaff = WardStaff::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

        return view($this->viewPath . '.index', compact(['page']));

    }

    public function create()
    {
        $page['page_title']       = 'Register Report : create';
        $page['page_description'] = 'Register Report details';

        // $getUniqueCode  = mylogic::sysRefCode($tablename = $this->wardTbl, $prefix = $this->prefix);

        //TODO  display municipility according to municipility 
        // return view($this->viewPath . '.create', compact(['page', 'getUniqueCode']));
        return view($this->viewPath . '.create', compact(['page']));
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function changeStatus(){

    }
}
