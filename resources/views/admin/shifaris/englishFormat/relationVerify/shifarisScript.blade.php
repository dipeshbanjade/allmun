<script type="text/javascript">
var btnAddTableRow = $('#addTableRow');
var clickCount = 1;
btnAddTableRow.on('click', function(e){
	e.preventDefault();

	clickCount++;

	var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

	var relativeTitle = '<select onchange="changeSelect(this)" name="relativeTitle[]"><option value="Mr">Mr</option><option value="Mrs">Mrs</option><option value="Miss">Miss</option></select>';
	
	var relativeName = '<input class="dashed-input-field" placeholder="   *" type="text" required="required" name="relativeName[]">';
	var relativeRelation = '<input class="dashed-input-field" placeholder="   *" type="text" required="required" name="relativeRelation[]">';

	var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

	var insertRow = "<tr class='tblRow'>"
	+"<td>"+serialNumber+"</td>"
	+"<td>"+relativeTitle+"</td>"
	+"<td>"+relativeName+"</td>"
	+"<td>"+relativeRelation+"</td>"
	+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
	+"</tr>";

	$('#relation_verification_table').append(insertRow);

	$('.btnRemoveItem').on('click', function(e){
		$(this).parent().parent().remove();
		return false;
	});

});
$(function(){
		/*add buttoin*/
		$("form[name='frmWardrelationVerify']").validate({
			rules:{
			refCode : {
				required : true,
				minlength : 3,
				maxlength : 10
			},
			issuedDate : {
				required : true,
				date : true
			},
			title : {
				required : true
			},
			name : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipalityName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			wardNumber : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			districtName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			former : {
				required : true
			},
			preVDCName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipalityType : {
				required : true
			},
			preWardNumber : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			titleType: {
				required : true
			},
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			refCode : " Ref code is required",
			issuedDate : "Issue Date is required",
			title : "Title is required",
			name : "Person name is required",
			municipalityName : " Municipality is required",
			wardNumber : " Ward is required",
			districtName : "District is required",
			former : "Former is required",
			preVDCName : "OrgAddress is required",
			municipalityType : "Org Type is required",
			preWardNumber : "Org Ward is required",
			titleType: "Person Title is required",
			authorizedPerson : "Authorized Person is required"
		}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='name']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>