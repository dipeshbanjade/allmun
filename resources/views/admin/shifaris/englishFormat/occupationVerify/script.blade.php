<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardoccupationVerify']").validate({
			rules:{
				refCode:{
					required: true,
					minlength: 3,
					maxlength: 10
				},
				issuedDate : {
					required : true,
					date : true
				},
				letterSub : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				appName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				fatherName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				motherName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipality : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				ward : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				district : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				former : {
					required : true
				},
				orgAddr : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				orgType : {
					required : true
				},
				orgWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				businessName : {
					required : true,
					minlength : 3,
					maxlength : 50
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				refCode: "Reference Code is required",
				issuedDate : "Issue Date is required",
				letterSub : "Letter subject is required",
				appName : "Name is required",
				fatherName : "Father Name is required",
				motherName : "Mother Name is required",
				municipality : "Municipality is required",
				ward : "Ward is required",
				district : "District name is required",
				former : "Former Type is required",
				orgAddr : "Organization Address is required",
				orgType : "Organization Type is required",
				orgWard : "Organization Ward is required",
				businessName : "Business Name is required",
				authorizedPerson : "Authorized Person is required",

			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='appName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>