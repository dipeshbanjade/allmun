@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
	<div class="card-box">
		<div class="card-body ">
	<div class="card-head">
		<header><i class="fa fa-eye"></i>&nbsp; @lang('commonField.extra.fiscalYear')</header>
	</div>
			<button id="" class="btnAddfiscalYear btn btn-primary pull-right">  
			 <i class="fa fa-plus"></i><span>&nbsp;@lang('commonField.extra.createFiscalYear') </span> 
			</button>

			<div class="table-scrollable fontColorBlack">
					@if(count($allFiscalYear) > 0)
		                  <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
		                  	<thead>
		                  		<tr>
		                  			<th width="5%">@lang('commonField.extra.sn')</th>
		                  			<th>@lang('commonField.extra.startDate')</th>
		                  			<th>@lang('commonField.extra.endDate') </th>
		                  			<th>@lang('commonField.extra.startShort')</th>
		                  			<th>@lang('commonField.extra.endShort') </th>
		                  			<th>@lang('commonField.extra.action') </th>
		                  		</tr>
		                  	</thead>
		                  	<tbody>
		                  	    @php $count = 1  @endphp
			                  	    @foreach($allFiscalYear as $fisYear)
                                      <tr class="odd gradeX">
                                      	<td>{{ $count ++ }}</td>
                                      	<td>{{ $fisYear->startDate }}</td>
                                      	<td>{{ $fisYear->endDate }}</td>
                                      	<td>{{ $fisYear->startShort }}</td>
                                      	<td>{{ $fisYear->endShort }}</td>
                                      	<td>
                                      		<a href="#" class="btnEditfisYear btn btn-primary btn-xs" id=""  data-url="{!! route('admin.fiscalYear.edit', $fisYear->id) !!}">
                                      			<i class="fa fa-pencil"></i>
                                      		</a>
                                      		    <a href="#" class="delete_data" id="" data-url="{!! route('admin.fiscalYear.changeStatus', $fisYear->id) !!}">
                                                     <button class="btn btn-danger btn-xs">
                                                         <i class="fa fa-eye-slash"></i>
                                                     </button>
                                                   </a>
                                      			</td>
                                      </tr>
			                  	    @endforeach
		                  	 </tbody>
		                  	</table>
		                  		{{ $allFiscalYear->links() }}
		                  @else
		                  <h3 class="fontColorBlack">
		                    @lang('commonField.extra.pleaseInsertData')
		                    </h3>
					@endif 
				
					</div>
				</div>
			</div>
		</div>
		@endsection

		@section('modelSection')
   <div class="modal" id="fiscalYearModal">
   	<div class="modal-dialog">
   		<div class="modal-content">
   			<!-- Modal Header -->
   			<div class="modal-header">
   				<h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.addFiscalYear')</i></h4>
   				<button type="button" class="close" data-dismiss="modal">&times;</button>
   			</div>

   			<!-- Modal body -->
   			{{ Form::open(['route' => 'admin.fiscalYear.store', 'name' => 'frmFiscalYear']) }}
   			<div class="modal-body">
   			     @include('admin.setting.fiscalYear._form')
   			</div>
   			<!-- Modal footer -->
   			<div class="modal-footer">
   			    <div class="col-lg-12 p-t-20 text-right"> 
   			    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
   				<button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
   			    </div>
   			</div>
   			{{ Form::close() }}

   		</div>
   	</div>
   </div>

   <!-- update model -->
   <div class="modal" id="updateFiscalForm">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.editDegination')</i></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
        {{ Form::open(['name' => 'frmeditFiscalYear', 'id' => 'frmUpdateFiscalYear']) }}
             @include('admin.setting.fiscalYear._form')
               {{ Form::hidden('fiscalId', null, ['class' => 'fiscalId']) }}

            <div class="col-lg-12 p-t-20 text-right"> 
            {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
            </div>
        {{ Form::close() }}
        </div>
          <div class="modal-footer">

          </div>
      </div>
    </div>
   </div>
@endsection
@section('custom_script')
 @include('admin.setting.fiscalYear.script')
@endsection