<div class="right_col nep" role="main" style="min-height: 521px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">फरक फरक अंग्रेजी हिज्जे प्रमाणित</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

         @include('admin.shifaris.municipalityDetail')

         <div class="row top-part">
          <div class="col-md-6">
            <p align="left">
             <b>@lang('commonField.extra.refCode'):</b>
             @if(isset($data->refCode))
             {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
             @else
             {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
             @endif
           </p>
           <p align="left">
             <b> च. नं.:
               {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
             </b>
           </p>
         </div>
         <div class="col-md-6">
          <p align="right">
            <b class="mt">मिति :
              {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
            </b>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
          </p><h4 align="center">
            <b>विषय: फरक फरक अंग्रेजी हिज्जे प्रमाणित।</b>
          </h4>
          {{ Form::hidden('letter_subject', 'फरक फरक अंग्रेजी हिज्जे प्रमाणित।', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
        </div>
      </div>
      <div class="row content-para">
        <div class="col-md-12">
          <p align="left">
           <b>
             {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }} 
           </b> महा/उपमाहा/नगरपालिका/गाउँपालिका  वडा नं. <b>
             {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
           </b>
           (साविक 
           <!--SABIK ADDRESS START -->
           {{ Form::text('municipalityAddress', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'municipalityAddress', 'required' => 'required']) }}
           <!-- SABIK ADDRESS END -->
           <b><select onchange="changeSelect(this)" name="municipalityType">
            <option value="गा.वि.स." 
            {{ !empty($data->municipalityType) && $data->municipalityType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
            <option value="नगरपालिका" 
            {{ !empty($data->municipalityType) && $data->municipalityType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
            <option value="उप महानगरपालिका" 
            {{ !empty($data->municipalityType) && $data->municipalityType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
            <option value="महानगरपालिका" 
            {{ !empty($data->municipalityType) && $data->municipalityType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
          </select>
        </b>, वडा नं.
        {{ Form::text('municipalityWard', Auth::user()->wards_id, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'municipalityWard', 'required' => 'required']) }}
        )
        निवासी 
        <b><select onchange="changeSelect(this)" name="personPrefix">
          <option value="श्री"
          {{ !empty($data->personPrefix) && $data->personPrefix == 'श्री' ? 'selected' : '' }}>श्री</option>
          <option value="सुश्री"
          {{ !empty($data->personPrefix) && $data->personPrefix == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
          <option value="श्रीमती"
          {{ !empty($data->personPrefix) && $data->personPrefix == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
        </select>
      </b>
      {{ Form::text('personName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'personName', 'required' => 'required']) }}
      को  नाम थर मा अंग्रेजी हिज्जे 
      {{ Form::text('trueDocument', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'trueDocument', 'required' => 'required']) }}
    हुनुपर्नेमा निजको तल उल्लेखित कागजातमा निजको अंग्रेजी हिज्जे फरक हुन गएको हुनाले सो फरक अंग्रेजी हिज्जे भएको व्येक्ति एकै भएको प्रमाणित गरि पाउँ भनि निवेदन गरेकोमा निजले पेश गरेको प्रमाण कागजातका आधारमा व्यहोरा मनासिव भएको खुल्न आएकोले सो फरक, फरक अंग्रेजी हिज्जे भएको व्यक्ति एकै भएको प्रमाणित गरिन्छ। </p>
  </div>
  <p align="center"><b>फरक अंग्रेजी हिज्जे र कागजात विवरण </b></p>
  <div class="col-md-12">
    <div class="table-responsive">
      <table class="table table-bordered" id="diff_hijje_table">
        <thead>
          <tr>
            <th>फरक अंग्रेजी हिज्जे</th>
            <th>फरक परेको कागजात </th>
          </tr></thead>
          <tbody>
            <?php 
            $count = 1;
            $tableData = !empty($data) ? json_decode($data->dataTable, true) : [1];
          //dd($tableData);
            foreach($tableData as $singleData){  
              $count++;       
              ?>
              <tr>
                <td>
                  {{ Form::text('diffHijje[]', !empty($singleData['diffHijje']) ? $singleData['diffHijje'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                </td>
                <td>
                  {{ Form::text('diffPaper[]', !empty($singleData['diffPaper']) ? $singleData['diffPaper'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                </td>

                @if($count == 2)
                <td class="add-btns" style="border: 0px;">
                  <button type="button" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button>
                </td>
                @else
                <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                @endif                
                <!-- <td><a href="" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></a></td> -->
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="text-right btm-last">
      <p>
        {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} 
      </p>
      <p> <b> 
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b> </p>
    </div>
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr>
    @include('admin.shifaris.nibedakCommonField')
    <!-- END -->

    <div class="col-md-12">
      <hr>
    </div>
  </div>
</div>
</div>
</div>
</div>
