<?php

namespace App\Http\Controllers\admin\houseDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\mylogic;
use App\model\admin\houseDetails\economicDetails\hsAgricultureDetails;
use App\model\admin\houseDetails\economicDetails\hsAgricultureProduction;
use App\model\admin\houseDetails\economicDetails\hsAgriAnimalProduction;
use App\model\admin\houseDetails\economicDetails\hsOtherIncomeSource;
use App\model\admin\houseDetails\economicDetails\hsAgriProSurvial;
use App\model\admin\houseDetails\economicDetails\hsOrgTaxPay;
use App\model\admin\houseDetails\economicDetails\hsOrgDetails;
use App\model\admin\houseDetails\economicDetails\IndustryBusinessDetail;
use App\model\admin\houseDetails\economicDetails\hsLoanDetails;
use App\model\admin\houseDetails\economicDetails\hsOrgInvolvement;
use App\model\admin\houseDetails\economicDetails\hsIncomeExpensDetails;

use App\model\admin\houseDetails\diseaster\hsNaturalDiseaster;
use App\model\admin\houseDetails\rentail\hsRentailDetails;
use App\model\admin\houseDetails\memberTraining\hsMemberTraining;
use Auth;
use DB;
use Validator;
class economicDetails extends Controller
{    
  public function __construct()
  {
   $this->middleware('auth');
 }

 /*Agriculture Details*/
 public function agricultureDetails(Request $request){
  // dd($request);
  $agricultureDetailData = $request->haveAgriArea; // cacheing array data

  //$hsAgriDetails = json_encode($request->haveAgriArea);

  $selectAgrDetailJson = []; 
  DB::beginTransaction(); 

  if (isset($agricultureDetailData['haveFamilyArea']) && $agricultureDetailData['haveFamilyArea'] == 1) {
    $selectAgrDetailJson['haveFamilyArea'] = 1;
    $selectAgrDetailJson['familyArea'] = $agricultureDetailData['familyArea'];
  }
  if(isset($agricultureDetailData['haveOtherArea']) && $agricultureDetailData['haveOtherArea'] == 1){
    $selectAgrDetailJson['haveOtherArea'] = 1;
    $selectAgrDetailJson['otherArea'] = $agricultureDetailData['otherArea'];
  }

  if(isset($agricultureDetailData['haveOtherOwner']) && $agricultureDetailData['haveOtherOwner'] == 1){
    $selectAgrDetailJson['haveOtherOwner'] = 1;
    $selectAgrDetailJson['otherOwner'] = $agricultureDetailData['otherOwner'];
  }

  $hsAgrDetailsJson = json_encode($selectAgrDetailJson);

  if ($request->myHouseId && $request->fm_member_id) {
   $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

   $isExistsHsNum = hsAgricultureDetails::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
   if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
    try {
     $agriDetails  = !$isExistsHsNum['house_details_id'] ? new hsAgricultureDetails : hsAgricultureDetails::findOrfail($isExistsHsNum['id']);   


     $agriDetails->createdBy         = Auth::user()->id;
     $agriDetails->municipilities_id = $ishouseExists['municipilities_id'];
     $agriDetails->wards_id          = $ishouseExists['wards_id'];
     $agriDetails->house_details_id  = $ishouseExists['id'];
     $agriDetails->citizen_infos_id  = $ishouseExists['citizen_infos_id']; 

     $agriDetails->haveAgriculture   = $request->haveAgriculture;
     $agriDetails->hsAgriDetails = $hsAgrDetailsJson;

     $agriDetails->status            = 0;
     $agriDetails->softDelete        = 0;
     $mySave = !$isExistsHsNum['house_details_id'] ? $agriDetails->save() : $agriDetails->update();

     $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_agriculture_details', $tblId = $agriDetails->id);
     if ($mySave && $log) {
      DB::commit();
      return response()->json([
        'success' => true,
        'message' => config('activityMessage.saveMessage')
      ]);
                  // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
            // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
    }else{
      return response()->json([
        'success' => false,
        'message' => config('activityMessage.unSaveMessage')
      ]);
           // return back()->withMessage(config('activityMessage.unSaveMessage'));
    }
  } catch (Exception $e) {
   DB::rollback();
   return back()->withMessage(config('activityMessage.dataNotInserted'));
 }
}else{   
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{    
 return back()->withMessage('House number is missing please try to insert it from browser');
}
}
/*agriculture production details*/
public function agricultureProductionDetails(Request $request){
      $agricultureProd = $request->hsAgrProductionDetails; // cacheing array data
      $agricultureProductArray = []; 
      DB::beginTransaction(); 
      foreach ($agricultureProd as $agrProd => $value) {                  
       if (isset($value['selectStatus']) && $value['selectStatus'] == 1) {
         $agricultureProductArray[$agrProd] = $value;
       }
     }

     //dd($agricultureProductArray);
     $hsAgrProductionDetailsJson = json_encode($agricultureProductArray);
       //$hsAgrProductionDetailsJson = json_encode($request->hsAgrProductionDetails);
     if ($request->myHouseId && $request->fm_member_id) {
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

       $isExistsHsNum = hsAgricultureProduction::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
                 $agrProduction  = !$isExistsHsNum['house_details_id'] ? new hsAgricultureProduction : hsAgricultureProduction::findOrfail($isExistsHsNum['id']);   // insert only who is death

                 $agrProduction->createdBy         = Auth::user()->id;
                 $agrProduction->municipilities_id = $ishouseExists['municipilities_id'];
                 $agrProduction->wards_id          = $ishouseExists['wards_id'];
                 $agrProduction->house_details_id  = $ishouseExists['id'];

                 $agrProduction->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID
                 $agrProduction->hsAgrProductionDetails = $hsAgrProductionDetailsJson;
                 
                 $agrProduction->status            = 0;
                 $agrProduction->softDelete        = 0;
                 $mySave = !$isExistsHsNum['house_details_id'] ? $agrProduction->save() : $agrProduction->update();

                 $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_agriculture_productions', $tblId = $agrProduction->id);
                 if ($mySave && $log) {
                   DB::commit();
                   return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                  ]);
                     // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'familyMember', 'helpTab' => 'familyDeath']);
                   // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
                 }else{
                  return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                  ]);
                  // return back()->withMessage(config('activityMessage.unSaveMessage'));
                }
              } catch (Exception $e) {
               DB::rollback();
               return back()->withMessage(config('activityMessage.dataNotInserted'));
             }
           }else{ 
             return back()->withMessage(config('activityMessage.idNotFound'));
           }
         }else{ 
           return back()->withMessage('House number is missing please try to insert it from browser');
         }
       }

    public function agricultureAnimalProduction(Request $request){  // Animal production details     
     DB::beginTransaction(); 
     $hsAnimalProDetailsJson = json_encode($request->hsAnimalProDetails);

     if ($request->myHouseId && $request->fm_member_id) {
      $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

      $isExistsHsNum = hsAgriAnimalProduction::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
      if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
       try {
                $animalProduct  = !$isExistsHsNum['house_details_id'] ? new hsAgriAnimalProduction : hsAgriAnimalProduction::findOrfail($isExistsHsNum['id']);   // insert only who is death

                $animalProduct->createdBy         = Auth::user()->id;
                $animalProduct->municipilities_id = $ishouseExists['municipilities_id'];
                $animalProduct->wards_id          = $ishouseExists['wards_id'];
                $animalProduct->house_details_id  = $ishouseExists['id'];

                $animalProduct->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID
                
                $animalProduct->haveHusbandry     = $request->haveHusbandry;
                $animalProduct->hsAnimalProDetails= $hsAnimalProDetailsJson;
                

                $animalProduct->status            = 0;
                $animalProduct->softDelete        = 0;
                $mySave = !$isExistsHsNum['house_details_id'] ? $animalProduct->save() : $animalProduct->update();

                $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_agri_animal_productions', $tblId = $animalProduct->id);
                if ($mySave && $log) {
                  DB::commit();
                  return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                  ]);
                    // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'familyMember', 'helpTab' => 'familyDeath']);
                  // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
                }else{
                  return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                  ]);
                 // return back()->withMessage(config('activityMessage.unSaveMessage'));
                }
              } catch (Exception $e) {
                DB::rollback();
                return back()->withMessage(config('activityMessage.dataNotInserted'));
              }
            }else{ 
              return back()->withMessage(config('activityMessage.idNotFound'));
            }
          }else{ 
            return back()->withMessage('House number is missing please try to insert it from browser');
          }
        }
    public function otherIncomeSource(Request $request){  // other income macha and mauri palan
      DB::beginTransaction();
      $jsonMachaPal = json_encode($request->machaPal);
      $jsonMauriPal = json_encode($request->mauriPal);
      $jsonFulKheti = json_encode($request->fulKheti);

      if ($request->myHouseId && $request->fm_member_id) {
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

       $isExistsHsNum = hsOtherIncomeSource::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
         $agriOtherIncome  = !$isExistsHsNum['house_details_id'] ? new hsOtherIncomeSource : hsOtherIncomeSource::findOrfail($isExistsHsNum['id']);

         $agriOtherIncome->createdBy         = Auth::user()->id;
         $agriOtherIncome->municipilities_id = $ishouseExists['municipilities_id'];
         $agriOtherIncome->wards_id          = $ishouseExists['wards_id'];
         $agriOtherIncome->house_details_id  = $ishouseExists['id'];
         $agriOtherIncome->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
         
         $agriOtherIncome->machaPal           = $jsonMachaPal;
         $agriOtherIncome->mauriPal           = $jsonMauriPal;
         $agriOtherIncome->fulKheti           = $jsonFulKheti;
         
         $agriOtherIncome->status            = 0;
         $agriOtherIncome->softDelete        = 0;
         $mySave = !$isExistsHsNum['house_details_id'] ? $agriOtherIncome->save() : $agriOtherIncome->update();

         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_other_income_sources', $tblId = $agriOtherIncome->id);
         if ($mySave && $log) {
          DB::commit();
          return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
          ]);
          // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
          // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
        }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.unSaveMessage')
          ]);
         // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
     }
   }else{   
     return back()->withMessage(config('activityMessage.idNotFound'));
   }
 }else{    
   return back()->withMessage('House number is missing please try to insert it from browser');
 }
}

public function agricultureSurvival(Request $request){
  DB::beginTransaction();
  if ($request->myHouseId && $request->fm_member_id) {
   $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
   $isExistsHsNum = hsAgriProSurvial::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
   if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
    try {
     $agriSurvival  = !$isExistsHsNum['house_details_id'] ? new hsAgriProSurvial : hsAgriProSurvial::findOrfail($isExistsHsNum['id']);

     $agriSurvival->createdBy         = Auth::user()->id;
     $agriSurvival->municipilities_id = $ishouseExists['municipilities_id'];
     $agriSurvival->wards_id          = $ishouseExists['wards_id'];
     $agriSurvival->house_details_id  = $ishouseExists['id'];
         $agriSurvival->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
         
         $agriSurvival->survivalTime      = $request->yearlySustain;
         
         $agriSurvival->status            = 0;
         $agriSurvival->softDelete        = 0;
         $mySave = !$isExistsHsNum['house_details_id'] ? $agriSurvival->save() : $agriSurvival->update();

         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_agri_pro_survials', $tblId = $agriSurvival->id);
         if ($mySave && $log) {
          DB::commit();
          return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
          ]);
          // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
          // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
        }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.unSaveMessage')
          ]);
         // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
     }
   }else{   
     return back()->withMessage(config('activityMessage.idNotFound'));
   }
 }else{    
   return back()->withMessage('House number is missing please try to insert it from browser');
 }
}

    public function taxPayDetails(Request $request){   // tax payment details
     DB::beginTransaction();
     $jsonTaxPayName = $request->payTaxName ? json_encode($request->payTaxName) : ''; 
     if ($request->myHouseId && $request->fm_member_id) {
      $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
      $isExistsHsNum = hsOrgTaxPay::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
      if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
       try {
        $taxPayDetails  = !$isExistsHsNum['house_details_id'] ? new hsOrgTaxPay : hsOrgTaxPay::findOrfail($isExistsHsNum['id']);

        $taxPayDetails->createdBy         = Auth::user()->id;
        $taxPayDetails->municipilities_id = $ishouseExists['municipilities_id'];
        $taxPayDetails->wards_id          = $ishouseExists['wards_id'];
        $taxPayDetails->house_details_id  = $ishouseExists['id'];
          $taxPayDetails->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
          
          $taxPayDetails->havePaidTax       = $request->havePaidTax;
          $taxPayDetails->payTaxName        = $jsonTaxPayName;
          
          $taxPayDetails->status            = 0;
          $taxPayDetails->softDelete        = 0;
          $mySave = !$isExistsHsNum['house_details_id'] ? $taxPayDetails->save() : $taxPayDetails->update();

          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_org_tax_pays', $tblId = $taxPayDetails->id);
          if ($mySave && $log) {
           DB::commit();
           return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
          ]);
           // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
         }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.unSaveMessage')
          ]);
          // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
      } catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
      }
    }else{   
      return back()->withMessage(config('activityMessage.idNotFound'));
    }
  }else{    
    return back()->withMessage('House number is missing please try to insert it from browser');
  }
}

 public function industryBusinessDetail(Request $request){   // tax payment details
   DB::beginTransaction();
   $govOfficeRegDetailJson = json_encode($request->govOfficeRegDetail); 
   
   if ($request->myHouseId && $request->fm_member_id) {
    $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
    $isExistsHsNum = IndustryBusinessDetail::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
    if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
     try {
      $industryBusiness  = !$isExistsHsNum['house_details_id'] ? new IndustryBusinessDetail : IndustryBusinessDetail::findOrfail($isExistsHsNum['id']);

      $industryBusiness->createdBy         = Auth::user()->id;
      $industryBusiness->municipilities_id = $ishouseExists['municipilities_id'];
      $industryBusiness->wards_id          = $ishouseExists['wards_id'];
      $industryBusiness->house_details_id  = $ishouseExists['id'];
      $industryBusiness->citizen_infos_id  = $ishouseExists['citizen_infos_id']; 
           // house ower id                  

      $industryBusiness->haveIndustryBusiness = $request->haveIndustryBusiness;
      $industryBusiness->industryBusinessNum = $request->industryBusinessNum;
      $industryBusiness->haveRegistered = $request->haveRegistered;
      $industryBusiness->registeredNum = $request->registeredNum;
      $industryBusiness->nonRegisteredNum = $request->nonRegisteredNum;
      $industryBusiness->haveAccountNum = $request->haveAccountNum;

      $industryBusiness->govOfficeRegDetail = $govOfficeRegDetailJson;

      $industryBusiness->otherGovOfficeReg = $request->otherGovOfficeReg;
      $industryBusiness->haveAccountRecord = $request->haveAccountRecord;

      $industryBusiness->status            = 0;
      $industryBusiness->softDelete        = 0;
      $mySave = !$isExistsHsNum['house_details_id'] ? $industryBusiness->save() : $industryBusiness->update();

      $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'industry_business_details', $tblId = $industryBusiness->id);
      if ($mySave && $log) {
       DB::commit();
       return response()->json([
        'success' => true,
        'message' => config('activityMessage.saveMessage')
      ]);
           // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
     }else{
      return response()->json([
        'success' => false,
        'message' => config('activityMessage.unSaveMessage')
      ]);
          // return back()->withMessage(config('activityMessage.unSaveMessage'));
    }
  } catch (Exception $e) {
    DB::rollback();
    return back()->withMessage(config('activityMessage.dataNotInserted'));
  }
}else{   
  return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{    
  return back()->withMessage('House number is missing please try to insert it from browser');
}
}

/*org Details*/
public function orgDetails(Request $request){      
 DB::beginTransaction();
 $jsonOrgDetails = $request->hsOrgDetails ? json_encode($request->hsOrgDetails) : ''; 
 if ($request->myHouseId && $request->fm_member_id) {
  $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
  $isExistsHsNum = hsOrgDetails::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
  if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
   try {
    $orgDetails  = !$isExistsHsNum['house_details_id'] ? new hsOrgDetails : hsOrgDetails::findOrfail($isExistsHsNum['id']);

    $orgDetails->createdBy         = Auth::user()->id;
    $orgDetails->municipilities_id = $ishouseExists['municipilities_id'];
    $orgDetails->wards_id          = $ishouseExists['wards_id'];
    $orgDetails->house_details_id  = $ishouseExists['id'];
          $orgDetails->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id

          $orgDetails->hsOrgDetails  = $jsonOrgDetails;  
          $orgDetails->status            = 0;
          $orgDetails->softDelete        = 0;
          $mySave = !$isExistsHsNum['house_details_id'] ? $orgDetails->save() : $orgDetails->update();

          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_org_details', $tblId = $orgDetails->id);
          if ($mySave && $log) {
           DB::commit();
           return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
          ]);
         }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.unSaveMessage')
          ]);
        }
      } catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
      }
    }else{   
      return back()->withMessage(config('activityMessage.idNotFound'));
    }
  }else{    
    return back()->withMessage('House number is missing please try to insert it from browser');
  }
}

/*org Details*/

    public function loanDetails(Request $request){  // loan details
      DB::beginTransaction();
      $jsonloanTakenFrom = $request->loanTakenFrom ? json_encode($request->loanTakenFrom) : ''; 
      $jsonLoanPurpose   = $request->loanPurpose ? json_encode($request->loanPurpose) : ''; 
      if ($request->myHouseId && $request->fm_member_id) {
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
       $isExistsHsNum = hsLoanDetails::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
         $loanDetails  = !$isExistsHsNum['house_details_id'] ? new hsLoanDetails : hsLoanDetails::findOrfail($isExistsHsNum['id']);

         $loanDetails->createdBy         = Auth::user()->id;
         $loanDetails->municipilities_id = $ishouseExists['municipilities_id'];
         $loanDetails->wards_id          = $ishouseExists['wards_id'];
         $loanDetails->house_details_id  = $ishouseExists['id'];
           $loanDetails->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
           
           $loanDetails->haveLoan          = $request->haveLoan;
           $loanDetails->loanTakenFrom     = $jsonloanTakenFrom;
           $loanDetails->loanPurpose       = $jsonLoanPurpose;
           $loanDetails->otherLoanTakenFrom = $request->otherLoanTakenFrom;
           $loanDetails->otherLoanPurpose   = $request->otherLoanPurpose;
           
           $loanDetails->status            = 0;
           $loanDetails->softDelete        = 0;
           $mySave = !$isExistsHsNum['house_details_id'] ? $loanDetails->save() : $loanDetails->update();

           $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_loan_details', $tblId = $loanDetails->id);
           if ($mySave && $log) {
            DB::commit();
            return response()->json([
              'success' => true,
              'message' => config('activityMessage.saveMessage')
            ]);
            // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
            // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
          }else{
            return response()->json([
              'success' => false,
              'message' => config('activityMessage.unSaveMessage')
            ]);
           // return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
        } catch (Exception $e) {
         DB::rollback();
         return back()->withMessage(config('activityMessage.dataNotInserted'));
       }
     }else{   
       return back()->withMessage(config('activityMessage.idNotFound'));
     }
   }else{    
     return back()->withMessage('House number is missing please try to insert it from browser');
   }
 }

 public function orgInvolvement(Request $request){
  DB::beginTransaction();
  $jsonOrgInvName   = $request->orgInvName ? json_encode($request->orgInvName) : ''; 
  if ($request->myHouseId && $request->fm_member_id) {
   $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
   $isExistsHsNum = hsOrgInvolvement::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
   if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
    try {
     $orgInv  = !$isExistsHsNum['house_details_id'] ? new hsOrgInvolvement : hsOrgInvolvement::findOrfail($isExistsHsNum['id']);

     $orgInv->createdBy         = Auth::user()->id;
     $orgInv->municipilities_id = $ishouseExists['municipilities_id'];
     $orgInv->wards_id          = $ishouseExists['wards_id'];
     $orgInv->house_details_id  = $ishouseExists['id'];
         $orgInv->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
         
         $orgInv->haveOrgInv          = $request->haveOrgInv;
         $orgInv->orgInvName          = $jsonOrgInvName;
         
         $orgInv->status            = 0;
         $orgInv->softDelete        = 0;
         $mySave = !$isExistsHsNum['house_details_id'] ? $orgInv->save() : $orgInv->update();

         $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_org_involvements', $tblId = $orgInv->id);
         if ($mySave && $log) {
          DB::commit();
          return response()->json([
            'success' => true,
            'message' => config('activityMessage.saveMessage')
          ]);
        }else{
          return response()->json([
            'success' => false,
            'message' => config('activityMessage.unSaveMessage')
          ]);
        }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
     }
   }else{   
     return back()->withMessage(config('activityMessage.idNotFound'));
   }
 }else{    
   return back()->withMessage('House number is missing please try to insert it from browser');
 }
}
public function memberTraining(Request $request){
 DB::beginTransaction();
 $memberDataArray = $request->training ? $request->training : null; 
 $selectedData = null; 
 foreach ($memberDataArray as $memberData => $value) {                  
   if (isset($value['status']) && $value['status'] == 1) {
     $selectedData[$memberData] = $value;
   }
 }
 $jsonOrgInvName = json_encode($selectedData); 

 if ($request->myHouseId && $request->fm_member_id) {
  $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
  $isExistsHsNum = hsMemberTraining::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
  if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
   try {
    $memberTraining  = !$isExistsHsNum['house_details_id'] ? new hsMemberTraining : hsMemberTraining::findOrfail($isExistsHsNum['id']);

    $memberTraining->createdBy         = Auth::user()->id;
    $memberTraining->municipilities_id = $ishouseExists['municipilities_id'];
    $memberTraining->wards_id          = $ishouseExists['wards_id'];
    $memberTraining->house_details_id  = $ishouseExists['id'];
    $memberTraining->citizen_infos_id  = $ishouseExists['citizen_infos_id'];
            // house ower id

    $memberTraining->haveMemberTraining= $request->haveMemberTraining;
    $memberTraining->hsMemberTraining  = $jsonOrgInvName;

    $memberTraining->status            = 0;
    $memberTraining->softDelete        = 0;
    $mySave = !$isExistsHsNum['house_details_id'] ? $memberTraining->save() : $memberTraining->update();


    $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_org_involvements', $tblId = $memberTraining->id);
    if ($mySave && $log) {
     DB::commit();
     return response()->json([
       'success' => true,
       'message' => config('activityMessage.saveMessage')
     ]);
   }else{
     return response()->json([
       'success' => false,
       'message' => config('activityMessage.unSaveMessage')
     ]);
   }
 } catch (Exception $e) {
  DB::rollback();
  return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}else{   
  return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{    
  return back()->withMessage('House number is missing please try to insert it from browser');
}

}

/*rentails details*/
public function rentailDetails(Request $request){
 DB::beginTransaction();
 $jsonRentailDetails   = $request->rentailDetails ? json_encode($request->rentailDetails) : ''; 
 if ($request->myHouseId && $request->fm_member_id) {
  $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
  $isExistsHsNum = hsRentailDetails::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
  if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
   try {
    $rentailDetails  = !$isExistsHsNum['house_details_id'] ? new hsRentailDetails : hsRentailDetails::findOrfail($isExistsHsNum['id']);

    $rentailDetails->createdBy         = Auth::user()->id;
    $rentailDetails->municipilities_id = $ishouseExists['municipilities_id'];
    $rentailDetails->wards_id          = $ishouseExists['wards_id'];
    $rentailDetails->house_details_id  = $ishouseExists['id'];
          $rentailDetails->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // house ower id
          $rentailDetails->rentailDetails    = $jsonRentailDetails;
          $rentailDetails->status            = 0;
          $rentailDetails->softDelete        = 0;
          $mySave = !$isExistsHsNum['house_details_id'] ? $rentailDetails->save() : $rentailDetails->update();

          $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_rentail_details', $tblId = $rentailDetails->id);
          if ($mySave && $log) {
           DB::commit();
           return response()->json([
             'success' => true,
             'message' => config('activityMessage.saveMessage')
           ]);
         }else{
           return response()->json([
             'success' => false,
             'message' => config('activityMessage.unSaveMessage')
           ]);
         }
       } catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
      }
    }else{   
      return back()->withMessage(config('activityMessage.idNotFound'));
    }
  }else{    
    return back()->withMessage('House number is missing please try to insert it from browser');
  }
}

/*Income and Expenses Details*/
public function incomeAndExpensesDetail(Request $request){
      $incomeDetailsArray = json_encode($request->incomeDetails); // cacheing array data
      $expensesDetailsArray = json_encode($request->expensesDetails); // cacheing array data

      $agricultureProductArray = []; 
      DB::beginTransaction(); 
      
      if ($request->myHouseId && $request->fm_member_id) {
       $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

       $isExistsHsNum = hsIncomeExpensDetails::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
       if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
                 $incomeExpenses  = !$isExistsHsNum['house_details_id'] ? new hsIncomeExpensDetails : hsIncomeExpensDetails::findOrfail($isExistsHsNum['id']);   // insert only who is death

                 $incomeExpenses->createdBy         = Auth::user()->id;
                 $incomeExpenses->municipilities_id = $ishouseExists['municipilities_id'];
                 $incomeExpenses->wards_id          = $ishouseExists['wards_id'];
                 $incomeExpenses->house_details_id  = $ishouseExists['id'];

                 $incomeExpenses->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID
                 $incomeExpenses->incomeDetails = $incomeDetailsArray;
                 $incomeExpenses->totalIncome = $request->totalIncome;

                 $incomeExpenses->expensesDetails = $expensesDetailsArray;
                 $incomeExpenses->totalExpense = $request->totalExpense;

                 $incomeExpenses->haveAccountInBank = $request->haveAccountInBank;     
                 
                 $incomeExpenses->status            = 0;
                 $incomeExpenses->softDelete        = 0;
                 $mySave = !$isExistsHsNum['house_details_id'] ? $incomeExpenses->save() : $incomeExpenses->update();

                 $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_income_expens_details', $tblId = $incomeExpenses->id);
                 if ($mySave && $log) {
                   DB::commit();
                   return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                  ]);
                     // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'familyMember', 'helpTab' => 'familyDeath']);
                   // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
                 }else{
                  return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                  ]);
                  // return back()->withMessage(config('activityMessage.unSaveMessage'));
                }
              } catch (Exception $e) {
               DB::rollback();
               return back()->withMessage(config('activityMessage.dataNotInserted'));
             }
           }else{ 
             return back()->withMessage(config('activityMessage.idNotFound'));
           }
         }else{ 
           return back()->withMessage('House number is missing please try to insert it from browser');
         }
       }
     }
