@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
 <div class="card-box">
  <div class="card-body">
    <div class="card-head">
      <header><i class="fa fa-eye">
       &nbsp;@lang('commonField.houseHoldSetting.houseUseFor')</i>
     </header>
   </div>
   <button id="" class="btnAddhouseUseFor btn btn-primary pull-right">
     <i class="fa fa-plus"></i>
     <span> @lang('commonField.houseHoldSetting.createHouseUseFor')</span>
   </button>

   @if(count($allhouseUseFor) > 0)
   <div class="table-scrollable" id="dropLabel">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
     <thead>
      <tr>
       <th>@lang('commonField.extra.sn')</th>
       <th>@lang('commonField.extra.refCode')</th>
       <th>@lang('commonField.houseHoldSetting.houseUseFor')</th>
       <th>@lang('commonField.extra.action') </th>
     </tr>
   </thead>
   <tbody>
    @php $count = 1  @endphp
    @foreach($allhouseUseFor as $houseUseFor)
    <tr class="odd gradeX">
     <td>{{ $count ++ }}</td>
     <td>{{ $houseUseFor->refCode }}</td>
     <td >{{ $houseUseFor->nameNep ?? 'N/A' }} - {{ $houseUseFor->nameEng }} </td>

     <td class="left" >
       @if(Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun')
       <a href="#" data-url="{{ route('houseUseFor.edit', $houseUseFor->id) }}" class="btnEdithouseUseFor btn btn-primary btn-xs" id="" >
         <i class="fa fa-pencil"></i>
       </a>
       <a href="#" class="deleteMe" data-url="{{ route('houseUseFor.delete', $houseUseFor->id )}}" data-name="{{ $houseUseFor->nameEng }}">
        <button class="btn btn-danger btn-xs">
          <i class="fa fa-trash-o "></i>
          </button>
        </a>
        @else
        <small>@lang('commonField.extra.limited')</small>
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endif
</div>
</div>
</div>
@endsection
  @section('modelSection')
<div class="modal" id="houseUseForModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus" >&nbsp;@lang('commonField.houseHoldSetting.addHouseUseFor')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ Form::open(['route' => 'houseUseFor.store', 'name'=>'frmhouseUseFor']) }}
        @include('admin.setting.houseUseFor._form')
        <div class="col-lg-12 p-t-20 text-right"> 
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
        </div>
        {{ Form::close() }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

      </div>

    </div>
  </div>
</div>


<!-- edit model for houseUseFor starts here -->

<div class="modal" id="edithouseUseForModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.houseHoldSetting.editHouseUseFor')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       {{ Form::open(['name'=>'frmedithouseUseFor', 'id' => 'frmUpdatehouseUseFor']) }}
       @include('admin.setting.houseUseFor._form')
       {{ Form::hidden('houseUseForId', null, ['class' => 'houseUseForId']) }}
       <div class="col-lg-12 p-t-20 text-right"> 
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
      </div>
      {{ Form::close() }}
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">

    </div>

  </div>
</div>
</div>

<!-- edit model for houseUseFor ends here -->
@endsection

@section('custom_script')
@include('admin.setting.houseUseFor.script')
@endsection

