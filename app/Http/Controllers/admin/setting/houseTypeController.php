<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseType;
use App\model\mylogic;;
use App\Http\Requests\admin\setting\HouseType\HouseTypeValidation;
use Validator;

class houseTypeController extends Controller
{
    protected $viewPath  = 'admin.setting.houseType';
    protected $houseTypeTbl = 'house_types';
    protected $prefix;

    public function __construct()
    {
       $this->middleware('auth');
       $this->prefix = config('activityMessage.houseTypePrefix');
   }
   public function index()
   {
    $page['page_title']       = 'House Type';
    $page['page_description'] = 'list of all House Type';

    $allHouseType = houseType::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
    $prefix   = myLogic::sysRefCode($tablename = $this->houseTypeTbl, $prefix = $this->prefix);
    return view($this->viewPath . '.index', compact(['page', 'allHouseType', 'prefix'])); 
}


public function store(HouseTypeValidation $request)
{
   $mySave = houseType::create([
     'refCode' => $request->refCode,
     'houseNep' => $request->houseNep ?? 'N/A',
     'houseEng' => $request->houseEng,
     'status'  => 0,
     'softDelete' => 0
     ]);
   if ($mySave) {
    return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
}else{
    return back()->withMessage(config('activityMessage.unSaveMessage'));
}
}


public function edit($id)
{
 $data = houseType::findOrFail($id);
 if ($data) {
    return response()->json([
      'success' => true,
      'refCode' => $data->refCode,
      'id'      => $data->id,
      'houseNep' => $data->houseNep,
      'houseEng' => $data->houseEng
      ]);
}else{
    return response()->json([
      'success' => false,
      'message' => config('activityMessage.dataNotFound')
      ]);
}
}

public function update(Request $request, $id)
{
  $validator = Validator::make($request->all(), [
   'houseEng'  => 'required|min:4|max:100',
   'houseNep'  => 'min:4|max:100',
   ]);

  if ($validator->fails()) {
      return response()->json([
         'success' => false,
         'message' => $validator->errors()->all()
         ]);
  }
  $update          = houseType::findOrFail($id);
  $update->houseNep = $request->houseNep ?? 'N/A';
  $update->houseEng = $request->houseEng;

  $myUpdate = $update->update();
  if ($myUpdate) {
    return response()->json([
      'success' => true,
      'message' => config('activityMessage.updateMessage')
      ]);
}else{
    return response()->json([
      'success'  => false,
      'message'  => config('activityMessage.notUpdated')
      ]);
}
}


public function destroy($id)
{
    $del = houseType::findOrFail($id);
    $del->softDelete = 1;
    $myDel = $del->update();
    if ($myDel) {
     return MyLogic::successChangeStatus($code = 200);
 }else{
  return MyLogic::successChangeStatus($code = 301);
}
}
}
