<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>

  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
    <div class="row">
      <div class="col-sm-12">
        <div class="row pull-right">
        <div class="col-sm-6"><div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
            {{ Form::text('joingDateNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'joining date in nepali', 'id' => 'myDate', 'required']) }}

            <label class = "mdl-textfield__label" >@lang('commonField.extra.joiningDateNep')</label>
          </div></div>
          <div class="col-sm-6"><div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('joingDateEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'joining date in english', 'id'=> 'myEngDate', 'readonly']) }}

            <label class = "mdl-textfield__label" >@lang('commonField.extra.joiningDateEng')</label>
          </div></div>
        </div>
      </div>
      <div class="col-lg-12"> 
        <!-- profile pic -->
          <p>
              <label>
               <br>
               <img src="{{ isset($data) ? asset($data->profilePic) : '' }}" width="120" height="80" class="img img-thumbnai" id="profilePic"><br>
                <input name="profilePic" type='file' onchange="displayImage(this, 'profilePic');" title="select citizenship" />
              </label>
          </p>
      </div>

       <div class="col-lg-12"> 
         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
          {{ Form::text('email', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'login email address', 'required'=>'true']) }}
          <label class = "mdl-textfield__label" >@lang('commonField.personal_information.email')</label>
        </div>
      </div>

      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
         {{ Form::text('firstNameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'first name in english', 'title'=>'first name in napali', 'required'=>true]) }}
         <label class = "mdl-textfield__label" >@lang('commonField.personal_information.firstNameNep')</label>
       </div>
     </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('middleNameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'middle name in nepali', 'title'=>'middle name in nepali']) }}

        <label class = "mdl-textfield__label" >@lang('commonField.personal_information.middleNameNep')</label>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('lastNameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'last name in nepali', 'title'=>'last name in nepali', 'required'=>true]) }}

        <label class = "mdl-textfield__label" >@lang('commonField.personal_information.lastNameNep')</label>
      </div>
    </div>

     <div class="col-lg-4"> 
      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('firstNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'first name in english', 'title'=>'first name in english', 'required'=>true]) }}
        <label class = "mdl-textfield__label">@lang('commonField.personal_information.firstNameEng')</label>
      </div>
    </div>
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('middleNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'middle name in english', 'title'=>'middle name in english']) }}

        <label class = "mdl-textfield__label" >@lang('commonField.personal_information.middleNameEng')</label>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('lastNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'last name in english', 'title'=>'lastname in english', 'required'=>true]) }}

        <label class = "mdl-textfield__label" >@lang('commonField.personal_information.lastNameEng')</label>
      </div>
    </div>
   <!--  -->
   <div class="col-lg-4"> 
     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
       <label id="dropLabel">@lang('commonField.extra.department')</label>
       <select name="departments_id" class="form-control" title="select department" required>
         <option value="">@lang('commonField.extra.department')</option>
         @if(count($deptId) > 0)
            @foreach($deptId as $dept)
               <option value="{{ $dept->id }}" {{ isset($data) && $data->departments_id == $dept->id ? 'selected' : '' }}>
                   {{ $dept->nameNep }}  - {{ $dept->nameEng }} 
               </option>
            @endforeach
         @endif
       </select>
     </div>
   </div>
   <!--  -->

   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
          <select name="roles_id" class="form-control" title="select role name" required="true">
              <option value="">@lang('commonField.extra.selectRole')</option>
              @if(count($roles) > 0)
                  @foreach($roles as $role)
                       <option value="{{ $role->id }}" {{ isset($data->roles_id) &&  $data->roles_id == $role->id ? 'selected' : ''  }}> <!-- needed role id -->
                            {{ $role->roleName }}
                       </option>
                  @endforeach
              @endif
          </select>
     </div>
   </div>

   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
          <select name="wards_id" class="form-control" title="select ward name" required="true">
              <option value="">@lang('sidebarMenu.system_setting.ward')</option>
              @if(count($getWard) > 0)
                  @foreach($getWard as $ward)
                       <option value="{{ $ward->id }}" {{ isset($data) && $data->wards_id == $ward->id ? 'selected' : '' }}>
                            {{ $ward->nameEng }} - {{ $ward->nameNep }}
                       </option>
                  @endforeach
              @endif
          </select>
     </div>
   </div>

   <div class="col-lg-4"> 
     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width"  style="margin-top: 20px;">
       {{ Form::text('citizenNo', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'citizenship number','title'=>'citizenship number']) }}

       <label class = "mdl-textfield__label" >@lang('commonField.extra.citizenNo')</label>
     </div>
   </div>
    <div class="col-lg-4">
     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
       {{ Form::text('phoneNumber', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'phone number', 'title'=>'phone number with 10 digit', 'required'=>'true']) }}
       
       <label class = "mdl-textfield__label" for = "text5">@lang('commonField.personal_information.phoneNumber')</label>
     </div>                         
   </div>

   @if(!isset($data))
   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
       {{ Form::text('password', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'password','title'=>'login password', 'required'=>'true']) }}
       <label class = "mdl-textfield__label" >@lang('commonField.personal_information.password')</label>
     </div>
   </div>
   @endif
   
</div> 
</div>

</div>