<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १.तपाईको परिवारका बालबालिका हाल विद्यालयमा अध्ययन गरिरहेका छन् ? 
                    </p>
                    <td>
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" onchange="checkChildEdu(this)" class="rdoClickMe" title="double click for reset" name="haveChildEdu" value="1"  {{ isset($hsCEData) && $hsCEData->haveChildEdu == '1' ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" onchange="checkChildEdu(this)" class="rdoClickMe" title="double click for reset" name="haveChildEdu" value="0" {{ isset($hsCEData) && $hsCEData->haveChildEdu == '0' ? 'checked' : '' }} >
                        </label> 
                    </td>
                </div>
                <br>
                @if (isset($hsCEData) && $hsCEData->haveChildEdu == '1')
                  <table border="1" class="childEduTbl" style="display: block;">
                @else
                   <table border="1" class="childEduTbl" style="display: none;">
                @endif
                
                    <thead>
                        <tr>
                            <th width="50%">अध्ययनरत तह (कक्षा)	</th>
                            <th>बालक संख्या	</th>
                            <th>बालिका संख्या</th>
                            <th>जम्मा</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php                                        
                        $hsChildrenEducation = isset($hsCEData->hsChildrenEducation) ? json_decode($hsCEData->hsChildrenEducation) : '';
                        ?>                        
                        <?php $count = 0; ?>

                        @if (isset($educationLevel) && count($educationLevel)>0)
                        @foreach ($educationLevel as $eLevel)
                        <?php $count++; ?>
                        <tr>
                            <td>
                                {{$eLevel->nameNep}}
                                <?php                                 

                                $childrenEduData = moreHouseDetailsFormUpdate($eLevel->id, $hsChildrenEducation, $compareJsonKey = 'levelId');        

                                ?>
                            </td>
                            <td> 
                                <input type="hidden" name="childrenInfo[<?php echo $count;  ?>][levelId]" value="{{ $eLevel->id }}">                                
                                <input type="hidden" name="childrenInfo[<?php echo $count;  ?>][levelName]" value="{{ $eLevel->nameNep }}">                          
                                {{ Form::text('childrenInfo['.$count.'][maleChildNum]', isset($childrenEduData['maleChildNum']) ? $childrenEduData['maleChildNum'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}

                            </td>
                            <td>
                                {{ Form::text('childrenInfo['.$count.'][femaleChildNum]', isset($childrenEduData['femaleChildNum']) ? $childrenEduData['femaleChildNum'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text('childrenInfo['.$count.'][childSum]', isset($childrenEduData['childSum']) ? $childrenEduData['childSum'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <label>Please insert data from setting</label>
                        @endif

                    </tbody>
                </table>
                <br>
            </div>     
       
        </div>
            <div class="msgDisplay"></div>
        
    </div>
</div>
