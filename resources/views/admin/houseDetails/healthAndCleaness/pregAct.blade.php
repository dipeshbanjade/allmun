<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
        <div class="form-group col-md-12">{{-- 12 --}}
            <p>
                १.विगत ५ वर्ष भित्र तपाईको परिवारमा महिला सदस्य गर्भवती हुँदा निम्न क्रियाकलाप भए ?                 
            </p>
            <br>
            <table border="1">
                <thead>
                    <tr>
                        <th>सि.न.</th>
                        <th>५ वर्ष भित्र गर्भवती</th>
                        <th>गर्भवती महिलाको नाम</th>
                        <th>उमेर</th>
                        <th>जुकाको औषधी सेवन</th>
                        <th>गर्भको परीक्षण</th>
                        <th>स्वास्थ्य परीक्षण पटक</th>
                        <th>टिटानस सुई पटक</th>
                        <th>आइरन चक्की संख्या</th>
                        <th>अन्य रोग उपचार</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $pregActionInfo = isset($hsPA->hsPregActionDetails) ? json_decode($hsPA->hsPregActionDetails) : '';
                        $count = 1;

                         $upDatePregAct = moreHouseDetailsFormUpdate($compareKey = $hsDetails->citizen_infos->id, $jsonData = $pregActionInfo, $compareJsonKey = 'womenId');
                    ?>
                    @if($hsDetails->citizen_infos->gender == 'female')
                    <?php
                           $gharmuliName = $lan =='nep' ? $hsDetails->citizen_infos->fnameNep .''. $hsDetails->citizen_infos->mnameNep .' '.$hsDetails->citizen_infos->lnameNep :  $hsDetails->citizen_infos->fnameEng .''. $hsDetails->citizen_infos->mnameEng .' '.$hsDetails->citizen_infos->lnameEng;
                             $muliAge = getAge($hsDetails->citizen_infos->dobAD);                        

                      ?>
                    <tr>
                        <td>
                            1.
                        </td>
                        <td>
                            <input type="hidden" name="hsPregActionDetails[{{ 1 }}][womenId]" value="{{ $hsDetails->citizen_infos->id }}">
                            <input type="checkbox" class="hsPregAction" name="hsPregActionDetails[{{ 1 }}][isPregWithinFive]" value="1" {{ isset($upDatePregAct['womenId']) ? 'checked' : '' }} >
                        </td>
                        <td>                                    
                            {{ Form::text("hsPregActionDetails[1][pregWomenNam]", $gharmuliName, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                        </td>
                        <td>
                            {{ Form::text("hsPregActionDetails[1][pregWomenAge]", $muliAge, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>' ', 'id' => '']) }}
                        </td>
                        <td>
                        {{ Form::number("hsPregActionDetails[1][havHookWormMed]", isset($upDatePregAct['havHookWormMed']) ? $upDatePregAct['havHookWormMed'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}

                        </td>
                        <td>
                        {{ Form::number("hsPregActionDetails[1][havePregTreatment]", isset($upDatePregAct['havePregTreatment']) ? $upDatePregAct['havePregTreatment'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                        
                        </td>
                        <td>
                            {{ Form::number("hsPregActionDetails[1][treatmentCount]", isset($upDatePregAct['treatmentCount']) ? $upDatePregAct['treatmentCount'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                        </td>
                        <td>
                            {{ Form::number("hsPregActionDetails[1][tetanusInjCount]", isset($upDatePregAct['tetanusInjCount']) ? $upDatePregAct['tetanusInjCount'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                        </td>
                        <td>
                            {{ Form::number("hsPregActionDetails[1][ironTabletCount]", isset($upDatePregAct['ironTabletCount']) ? $upDatePregAct['ironTabletCount'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                        </td>
                        <td>
                            {{ Form::text("hsPregActionDetails[1][otherTreatment]", isset($upDatePregAct['otherTreatment']) ? $upDatePregAct['otherTreatment'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                        </td>
                    </tr>
                     @endif
                    <?php
                    //Note $getMyFamilyDetails from houseDetailsController
                    $count = 1;
                    foreach($getMyFamilyDetails as $member){
                        $memberInfo = getParentsName($member->relation_id);
                        $memberAge = getAge($memberInfo['dobAD']);                        
                        if( isset($memberInfo) && $memberInfo['gender'] == 'female'){                        
                            $count++; 
                            $singlePregAction = moreHouseDetailsFormUpdate($member->relation_id, $pregActionInfo, $compareJsonKey = 'womenId');
                            //dd($singleEducationDropper);
                            ?>
                            <tr>
                                <td>
                                    {{ $count }}
                                </td>
                                <td>
                                    <input type="hidden" name="hsPregActionDetails[<?php echo $count;  ?>][womenId]" value="{{ $member->relation_id }}">
                                    <input type="checkbox" class="hsPregAction" name="hsPregActionDetails[<?php echo $count ?>][isPregWithinFive]" value="1" {{ isset($singlePregAction['womenId']) ? 'checked' : '' }} >
                                </td>
                                <td>                                    
                                    {{ Form::text("hsPregActionDetails[$count][pregWomenNam]", $memberInfo['fnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text("hsPregActionDetails[$count][pregWomenAge]", $memberAge, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>' ', 'id' => '']) }}
                                </td>
                                <td>
                                {{ Form::number("hsPregActionDetails[$count][havHookWormMed]", isset($singlePregAction['havHookWormMed']) ? $singlePregAction['havHookWormMed'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}

                                </td>
                                <td>
                                {{ Form::number("hsPregActionDetails[$count][havePregTreatment]", isset($singlePregAction['havePregTreatment']) ? $singlePregAction['havePregTreatment'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                
                                </td>
                                <td>
                                    {{ Form::number("hsPregActionDetails[$count][treatmentCount]", isset($singlePregAction['treatmentCount']) ? $singlePregAction['treatmentCount'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::number("hsPregActionDetails[$count][tetanusInjCount]", isset($singlePregAction['tetanusInjCount']) ? $singlePregAction['tetanusInjCount'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::number("hsPregActionDetails[$count][ironTabletCount]", isset($singlePregAction['ironTabletCount']) ? $singlePregAction['ironTabletCount'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::text("hsPregActionDetails[$count][otherTreatment]", isset($singlePregAction['otherTreatment']) ? $singlePregAction['otherTreatment'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                </td>
                            </tr>
                            <?php }} ?>                                     
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="msgDisplay"></div>
        </div>
    </div>