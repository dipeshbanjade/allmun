@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
	       <i class="fa fa-plus"></i>
	 </div>

	 <div class="card-head">
            <header>Register Book</header>
	           {{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
             </div>
             
{{-- <div class="right_col dash-pg nep" role="main" style="min-height: 747px;">
<div class="clearfix"></div>
<div class="row">



<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
<div class="x_title">
<h3 class="title-x">चलानी किताब</h3>          
<div class="clearfix"></div>
</div>
<div class="x_content" id="block">

<div class="" role="tabpanel" data-example-id="togglable-tabs">
<ul id="myTab" class="nav nav-tabs bar_tabs tabss" role="tablist">

<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">नयाँ पत्रहरुको चलानी </a>
</li>

<li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">सिस्टम बाट जारी भएको सिफारिसको चलानी</a>
</li>
</ul>
<div id="myTabContent" class="tab-content">
<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

<div style="border:solid 1px #ccc; padding:30px  15px 10px;">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">


<a href="{{ route('admin.rollBook.create') }}" class="btn bttn btn-primary rgt-btm pull-right"> <span class="glyphicon glyphicon-plus"></span> नया रेकर्ड थप्नुहोस </a>


</div>
</div>
<form class="form-inline" id="chalani_book_search_form">
<fieldset class="fieldseet">
<legend>चलानी खोज्नुहोस</legend>
<div class="row  ">

<div class="form-group col-md-2">
<label for="chalani_no" class="sr-only">चलानी नं: *</label>
<input type="text" class="form-control" id="chalani_no" name="chalani_no" placeholder="चलानी नं.">
</div>
<div class="form-group mx-sm-3 mb-2 col-md-2">
<label for="chalani_date" class="sr-only">चलानी मिति:*</label>
<input type="text" class="form-control ndp-nepali-calendar date-input-field" id="chalani_date" name="chalani_date" placeholder="चलानी मिति" onfocus="showNdpCalendarBox('chalani_date')">
</div>
<div class="form-group mx-sm-2 col-md-3">
<label for="patra_sankya" class="sr-only">पत्र संख्या:* </label>
<input type="text" class="form-control" id="patra_sankya" name="patra_sankya" placeholder="पत्र संख्या">
</div>

<div class="form-group mx-sm-3 mb-2 col-md-3">
<label for="patra_chalan_office" class="sr-only">पत्र चलान गर्ने अफिसको नाम:* </label>
<input type="text" class="form-control" id="patra_chalan_office" name="patra_chalan_office" placeholder="पत्र चलान गर्ने अफिसको नाम">

<datalist id="patra_chalan_office">
<option value="चाँगुनारायण नगरपालिका">

</option>

<option value="office1">

</option>

<option value="Office 1 ">

</option>

<option value="divash office ">

</option>

<option value="raj office ">

</option>

<option value="Office by Nabaraj ">

</option>

<option value="सुर्यबिनायक नगरपालीका">

</option>

<option value="नापी कार्यलय">

</option>

<option value="जिल्ला अायोजना कार्यान्वयन इकाइ (भवन)">

</option>

<option value="अार्थिक मामिला तथा योजना मन्त्रालय">

</option>

<option value="संघीय मामिला तथा सामान्य प्रशासन मन्त्रालय">

</option>

<option value="१ नं वडा कार्यालय">

</option>

<option value="२ नं वडा कार्यालय">

</option>

<option value="३ नं वडा कार्यालय">

</option>

<option value="४ नं वडा कार्यालय">

</option>

<option value="५ नं वडा कार्यालय">

</option>

<option value="६ नं वडा कार्यालय">

</option>

<option value="७ नं वडा कार्यालय">

</option>

</datalist>
</div>
<div class="col-md-2">
<button type="submit" class="btn bttn btn-primary"><span class="glyphicon glyphicon-search"></span> खोज्नुहोस</button>
</div>
</div>
</fieldset>
</form>






<div class="row">
<div class="col-md-12">

<div class="table responsive ">
<div id="chalani-book-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table class="table table-bordered content-para table-striped dataTable no-footer" id="chalani-book-table" role="grid" aria-describedby="chalani-book-table_info">

<thead>
<tr role="row"><th colspan="2" rowspan="1">चलानी </th><th colspan="2" rowspan="1">चलानी हुने पत्रको </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="chalani-book-table" colspan="1">विषय </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="chalani-book-table" colspan="1">पत्र चलान गर्ने अफिसको नाम</th><th rowspan="2" class="sorting" tabindex="0" aria-controls="chalani-book-table" colspan="1">टिकट </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="chalani-book-table" colspan="1">कैफियत </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="chalani-book-table" colspan="1">फाइलको फोटो </th><th rowspan="2" class="sorting" tabindex="0" aria-controls="chalani-book-table" colspan="1"> </th></tr>
<tr role="row"><th class="sorting" tabindex="0" aria-controls="chalani-book-table" rowspan="1" colspan="1">नम्बर</th><th class="sorting_disabled" rowspan="1" colspan="1">मिति </th><th class="sorting" tabindex="0" aria-controls="chalani-book-table" rowspan="1" colspan="1">पत्र संख्या</th><th class="sorting_disabled" rowspan="1" colspan="1">पत्रको मिति </th></tr>
</thead>
<tbody>



</tbody>
</table><div id="chalani-book-table_processing" class="dataTables_processing panel panel-default" style="display: none;">Processing...</div></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="chalani-book-table_info" role="status" aria-live="polite"></div></div><div class="col-sm-7"></div></div></div>
</div>
</div>
</div>
</div>
</div>

<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
<div style="border:solid 1px #ccc; padding:  15px;">  
<div class="x_content" id="block">
<fieldset class="fieldseet">
<legend>चलानी खोज्नुहोस</legend>
<form id="chalani_sifaris_search_form" method="post">
<div class="col-md-3">
<label>मुख्य सिफारिस. : </label>
<select onchange="changeSelect(this)" name="main_sifaris" class="form-control" id="main_sifaris">
<option value="" selected="selected">---मुख्य छान्नुहोस---</option>
<option value="1">घर / जग्गा जमिन</option>
<option value="2">संघ सस्था</option>
<option value="3">ब्यबसाय / ब्यबसाय</option>
<option value="4">English Format</option>
<option value="5">शैक्षिक</option>
<option value="6">आर्थिक</option>
<option value="7">समाजिक / पारिवारिक</option>
<option value="8">भौतिक निर्माण</option>
<option value="9">सूचना तथा संचार</option>
<option value="10">न्याय / कानुन</option>
<option value="11">अन्य</option>
<option value="12">राष्ट्रिय पञ्जीकरण</option>
<option value="13">बैंक / वित्तीय संस्था </option>
<option value="15">खुल्ला ढाँचा</option>
</select>
</div>
<div class="col-md-3">
<label>सिफारिस  : </label>
<select onchange="changeSelect(this)" name="sifaris" id="sifaris" class="form-control">
<option value="">---मुख्य सिफारिस छान्नुहोस--- </option>
</select>
</div>

<div class="col-md-3">
<label>मिति: </label>
<input type="text" name="entry_date" id="entry_date" class="form-control ndp-nepali-calendar date-input-field" onfocus="showNdpCalendarBox('entry_date')">
</div>

<div class="col-md-3">
<label>बिषय: </label>
<input type="text" name="letter_subject" id="letter_subject" class="form-control">
</div>

<div class="col-md-3">
<label>अफिसको नाम: </label>
<input type="text" name="letter_office_name" id="letter_office_name" class="form-control">
</div>
<div class="col-md-3">
<label style="display: block">&nbsp;</label>
<button class="btn bttn btn-primary pull-right" name="get_printed_sifaris"> <span class="glyphicon glyphicon-search"></span> खोज्नुहोस</button>
</div>
</form>
</fieldset>
</div><!--row-->


<div id="sifaris_chalani_list_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table class="table table-striped content-para table-bordered dataTable no-footer" id="sifaris_chalani_list" role="grid" aria-describedby="sifaris_chalani_list_info">
<thead>

<tr role="row"><th class="sorting" tabindex="0" aria-controls="sifaris_chalani_list" rowspan="1" colspan="1">चलानी नं.</th><th class="sorting_disabled" rowspan="1" colspan="1">मुख्य सिफारिस </th><th class="sorting" tabindex="0" aria-controls="sifaris_chalani_list" rowspan="1" colspan="1">सिफारिसको नाम </th><th class="sorting_disabled" rowspan="1" colspan="1">दाखिला भएको मिति </th><th class="sorting" tabindex="0" aria-controls="sifaris_chalani_list" rowspan="1" colspan="1">बिषय </th><th class="sorting" tabindex="0" aria-controls="sifaris_chalani_list" rowspan="1" colspan="1">आफिसको नाम </th><th class="sorting" tabindex="0" aria-controls="sifaris_chalani_list" rowspan="1" colspan="1"></th></tr></thead>
<tbody>
</tbody>
</table><div id="sifaris_chalani_list_processing" class="dataTables_processing panel panel-default" style="display: none;">Processing...</div></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="sifaris_chalani_list_info" role="status" aria-live="polite"></div></div><div class="col-sm-7"><div class="dataTables_paginate paging_full_numbers" id="sifaris_chalani_list_paginate"></div></div></div></div>
</div>
</div>

</div>

</div>
</div>

</div>
</div>
</div>
</div> --}}



{{-- Load layout --}}
</div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
	 @include('admin.shifaris.script')
@endsection