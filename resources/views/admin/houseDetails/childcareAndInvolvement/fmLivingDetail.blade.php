<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १. तपाईको परिवारमा रहेका बालबालिका (१८ वर्ष वा सो भन्दा कम उमेरका) को संग बसोबास गर्दछन् ? (संख्या) 
                    </p>
                    <div>
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" onchange="displayFmLivingTbl(this)" name="haveChildLived" value="1" {{ isset($hsFLD) && $hsFLD->haveChildLived == 1 ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" onchange="displayFmLivingTbl(this)" name="haveChildLived" value="0" {{ isset($hsFLD) && $hsFLD->haveChildLived == 0 ? 'checked' : '' }}>
                        </label>
                    </div>
                </div>
                <br>
                @if (isset($hsFLD) && $hsFLD->haveChildLived == 1)
                    <table class="table fmLivingTbl"  style="display: block;">
                @else
                    <table class="table fmLivingTbl"  style="display: none;">
                @endif
                    <tr>
                        <th>बालक वा बालिकाको नाम</th>
                        <th>लिङ्ग</th>
                        <th>आमाबाबु</th>
                        <th>आमा</th>
                        <th>बाबु</th>
                        <th>बाबु र सौतेनी आमा</th>
                        <th>आमा र सौतेनी बाबु</th>
                        <th>अरु नातेदार</th>
                        <th>काम लगाउने व्यक्ति</th>
                        <th>अन्य</th>
                    </tr>
                    <?php 

                    $familyLivingDetail = isset($hsFLD->familyLivingDetails) ? json_decode($hsFLD->familyLivingDetails) : '';
                        //Note $getMyFamilyDetails from houseDetailsController
                    foreach($getMyFamilyDetails as $member){
                        $memberInfo = getParentsName($member->relation_id);
                        $memberAge = getAge($memberInfo['dobAD']);

                        if( isset($memberAge) && $memberAge < 18){                        
                            $count++;
                            $singleFmLivingInfo = moreHouseDetailsFormUpdate($member->relation_id, $familyLivingDetail, $compareJsonKey = 'childId');
                            //dd($memberInfo);
                            //dd($singleChildHealthCare);
                            ?>
                            <tr>
                                <td>
                                    <input type="hidden" name="familyLivingDetails[<?php echo $count ?>][childId]" value="{{ $member->relation_id }}">

                                    {{ Form::text("familyLivingDetails[$count][childName]", $memberInfo['fnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'First Name', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::text("familyLivingDetails[$count][gender]", $memberInfo['gender'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Gender', 'id' => '']) }}
                                </td>
                                <td>
                                    <input type="checkbox" name="familyLivingDetails[<?php echo $count ?>][isWithParent]" value="1" {{ isset($singleFmLivingInfo['isWithParent']) ? 'checked' : '' }} >
                                </td>
                                <td>
                                    <input type="checkbox" name="familyLivingDetails[<?php echo $count ?>][isWithMother]" value="1" {{ isset($singleFmLivingInfo['isWithMother']) ? 'checked' : '' }} >
                                </td>                        
                                <td>
                                    <input type="checkbox" name="familyLivingDetails[<?php echo $count ?>][isWithFater]" value="1" {{ isset($singleFmLivingInfo['isWithFater']) ? 'checked' : '' }} >
                                </td>
                                <td>
                                    <input type="checkbox" name="familyLivingDetails[<?php echo $count ?>][isWithStepMother]" value="1" {{ isset($singleFmLivingInfo['isWithStepMother']) ? 'checked' : '' }} >
                                </td>
                                <td>
                                    <input type="checkbox" name="familyLivingDetails[<?php echo $count ?>][isWithStepFather]" value="1" {{ isset($singleFmLivingInfo['isWithStepFather']) ? 'checked' : '' }} >
                                </td>
                                <td>
                                    <input type="checkbox" name="familyLivingDetails[<?php echo $count ?>][isWithOtherRelative]" value="1" {{ isset($singleFmLivingInfo['isWithOtherRelative']) ? 'checked' : '' }} >
                                </td>
                                <td>
                                    <input type="checkbox" name="familyLivingDetails[<?php echo $count ?>][isWithEmployer]" value="1" {{ isset($singleFmLivingInfo['isWithEmployer']) ? 'checked' : '' }} >
                                </td>
                                <td>
                                    <input type="checkbox" name="familyLivingDetails[<?php echo $count ?>][isWithOthers]" value="1" {{ isset($singleFmLivingInfo['isWithOthers']) ? 'checked' : '' }} >
                                </td>
                            </tr>
                            <?php }} ?>                                     
                        </table>
                        <br>
                    </div>     
                </div>
            </div>
            <div class="msgDisplay"></div>
        </div>