@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
   <div class="col-sm-12 col-md-12">
     <div class="card-box">
       <div class="card-body">
       <div class="card-head">
       	<header>
            <h3 class="fontColorBlack"> 
       		<i class="fa fa-edit">
            @lang('sidebarMenu.system_setting.ward')
            @lang('commonField.extra.edit') &nbsp;
            </i>
            </h3>
       	</header>
       </div>
            {!! Form::model($data, ['method' => 'POST','route' => ['wardStaffLeave.update', $data->id], 'name' => 'frmWardLeave']) !!}
        <div class="card-body row ">
                 @include('admin.ward.wardStaffLeave._form')
                 <p class="pull-right">
                    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
                        @lang('commonField.button.back')
                    </button>
                 	   {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success pull-right']) }}
                 </p>
            {{ Form::close() }}
       </div>
       </div>
       </div>
     </div>
@endsection
@section('custom_script')
   @include('admin.ward.wardStaffLeave.script')
@endsection