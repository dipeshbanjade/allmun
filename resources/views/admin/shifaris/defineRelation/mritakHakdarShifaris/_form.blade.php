<div class="right_col nep" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">मृतकका हकदार</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
         @include('admin.shifaris.municipalityDetail')
         <div class="row top-part">
          <div class="col-md-6">
           <p align="left"><b class="ps">प. सं.:
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
            @endif
          </b></p>
          <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNumber']) }}
          </b></p>
        </div>
        <div class="col-md-6">
          <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h4 align="center"><b>विषय: <u>सिफारिस सम्बन्धमा ।</u></b></h4>
          <p align="center" class="font-size-24">
            <b><u>जो - जससँग सम्बन्ध छ ।</u></b>
          </p>
          <p></p>
        </div>
      </div>
      <div class="row content-para">
        <div class="col-md-12">
          <p align="left">
            उपरोक्त सम्बन्धमा
            <b>
              {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
            </b> महा/उपमाहा/नगरपालिका/गाउँपालिका वडा नं.  
            <b>
              {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            </b>
            (साबिकको ठेगाना
            {{ Form::text('sabikAddress', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            ) बस्ने श्री/सुश्री/श्रीमती 
            {{ Form::text('nibedakName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            ले हकदार प्रमाणित गरी पाँऊ भनि यस वडा कार्यालय निवेदन दिनुभएको हुँदा सो सम्बन्धमा 
            {{ Form::text('municipalityNameTwo', isset($data->municipalityNameTwo) ? $data->municipalityNameTwo : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
            महा/उपमाहा/नगरपालिका/गाउँपालिका बाट मिति 
            {{ Form::text('dateSince', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'dateSince', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("dateSince")']) }}
            मा गरिएको द.नं. 
            {{ Form::text('registrationNumber', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            को नाता प्रमाणित प्रमाणपत्र अनुसार मृतक श्री/सुश्री/श्रीमती 
            {{ Form::text('deadPerson', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            का हकदारहरु देहाय बमोजिम उल्लेखित 
            {{ Form::text('rightfulHolderNumber', null, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            जना मात्र भएको व्यहोराको सिफारिस गरिन्छ ।
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <p align="center"><b>मृतकका हकदारको विवरण</b></p>
            <table class="table table-bordered" id="mritakTable">
              <thead>
                <tr><th>क्र.स.</th>
                  <th>हकदारहरुको नाम</th>
                  <th>नाता</th>
                  <th>बाबु/पति को नाम</th>
                  <th>नागरिकता नं</th>
                  <th>घर नं</th>
                  <th>किता नं</th>
                  <th>बाटोको नाम</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $count = 1;
                $tableData = !empty($data) ? json_decode($data->tableData, true) : [1];
          //dd($tableData);
                foreach($tableData as $singleData){         
                  ?>
                  <tr>
                    <td>
                      {{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('rightfulHolderName[]', !empty($singleData['rightfulHolderName']) ? $singleData['rightfulHolderName'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('relation[]', !empty($singleData['relation']) ? $singleData['relation'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('sonHusbandName[]', !empty($singleData['sonHusbandName']) ? $singleData['sonHusbandName'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('nagarikata[]', !empty($singleData['nagarikata']) ? $singleData['nagarikata'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('houseNo[]', !empty($singleData['houseNo']) ? $singleData['houseNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('kittaNo[]', !empty($singleData['kittaNo']) ? $singleData['kittaNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('streetName[]', !empty($singleData['streetName']) ? $singleData['streetName'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>

                    @if($count == 2)
                    <td class="add-btns" style="border: 0px;">
                      <button type="button" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button>
                    </td>
                    @else
                    <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                    @endif                                      
                    <!-- <td><a href="" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></a></td> -->
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="text-right btm-last">
              <p>
                {{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
              </p>
              <p> 
                <b>       
                  <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
                    @if(count($deginationsId) > 0)
                    @foreach($deginationsId as $deg)
                    <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                      {{ $deg->nameNep }} 
                    </option>
                    @endforeach
                    @endif
                  </select>
                </b> 
              </p>
            </div>
          </div>
          <!--views for nibedak detail -->
          <div class="clearfix"></div>
          <hr>
          <!-- END -->
        </div>
        <div class="col-md-12">
          @include('admin.shifaris.nibedakCommonField')
          <hr>
        </div>
      </div>
    </div>
  </div>