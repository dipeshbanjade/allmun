<?php

namespace App\Http\Requests\admin\setting\houseFoundation;

use Illuminate\Foundation\Http\FormRequest;

class houseFoundationValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'foundationNameNep' => 'required|min:4',
            'foundationNameEng' => 'required|min:4'
        ];
    }
}
