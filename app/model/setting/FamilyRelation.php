<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class FamilyRelation extends Model
{
	protected $table = 'family_relations';
	protected $fillable = ['refCode','nameEng','nameNep','orderColumn','status','softDelete'];
}
