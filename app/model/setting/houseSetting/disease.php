<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class disease extends Model
{
 protected $table = 'diseases';
 protected $fillable = ['refCode','nameEng','nameNep','tag','status','softDelete'];
}
