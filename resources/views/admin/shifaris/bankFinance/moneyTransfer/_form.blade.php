<div class="right_col nep" role="main" style="min-height: 504px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x"> रकम ट्रान्सफर सिफारिस </h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
          @include('admin.shifaris.municipalityDetail')
          
          <div class="row top-part">
            <div class="col-md-6">
              <p align="left">
               <b>@lang('commonField.extra.refCode'):</b>
               @if(isset($data->refCode))
               {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
               @else
               {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
               @endif
             </p>
             <p align="left">
               <b> च. नं.:
                 {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
               </b>
             </p>
           </div>
           <div class="col-md-6">
            <p align="right">
              <b class="mt">मिति :
                {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
              </b>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <p align="center" class="font-size-24"></p>
            <h4 align="center">
              <b>विषय:रकम ट्रान्सफर  सिफारिस।</b>
            </h4>
            {{ Form::hidden('letter_subject', 'रकम ट्रान्सफर  सिफारिस', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}

          </div>
        </div>
        <div class="row title-left">
          <div class="col-md-12">
            <p align="left">
             <b>श्री 
               {{ Form::text('nameOfOffice', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'nameOfOffice', 'required' => 'required']) }}
             </b>
             ,</p>
             <p align="left">
               <b>
                 {{ Form::text('districtOfOffice', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'districtOfOffice', 'required' => 'required']) }}।
               </b>
             </p>
           </div>
         </div>
         <div class="row">
          <div class="col-md-12 content-para">
            <p>उपरोक्त विषयमा यस  कार्यालयको नाममा तहाँ स्थित मूल खाता नं. 
              <b> 
                {{ Form::text('mainAccountNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'mainAccountNumber', 'required' => 'required']) }}
              </b> 
              खाताबाट यसै कार्यालयका निम्न चालु खाता नं. हरुमा उल्लेखित  रकम ट्रान्सफर गरी दिनुहुन अनुरोध छ | 
            </p> 
          </div>
          <div class="col-md-12">
          </div>
          <h4 align="center">
            <b>रकम ट्रान्सफर गर्नुपर्ने खाताको  विवरण</b>
          </h4>
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered" id="house_kayem_table">
                <thead>
           

                  <tr><th width="2%">क्र.स.</th>
                    <th>चालु खाता नं. </th>
                    <th>जम्मा गर्नुपर्ने रकम </th>
                    <th>जम्मा गर्नुपर्ने रकम (अक्ष्ररमा) </th>
                  </tr></thead>
                  <tbody>
                  <?php
                    $count=1;
                    $moneyTransferDetail = !empty($data) ? json_decode($data->moneyTransferDetail,true): [1];
                  ?>
                  @foreach ($moneyTransferDetail as $singleData)
                    <tr>
                      <td>{{$count++}} </td>
                      <td>{{ Form::text('runningAccNum[]', !empty($singleData['runningAccNum'])? $singleData['runningAccNum'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'runningAccNum', 'required' => 'required']) }} </td>
                      <td>{{ Form::text('amount[]', !empty($singleData['amount'])? $singleData['amount'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'amount', 'required' => 'required']) }} </td>
                      <td>{{ Form::text('amountInWords[]', !empty($singleData['amountInWords'])? $singleData['amountInWords'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'amountInWords', 'required' => 'required']) }} </td>

                      @if ($count == 2)
                        <td class="add-btns" style="border: 0px;"><button type="button" id="addTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button></td>
                        {{-- expr --}}
                      @else
                        <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                      @endif
                    </tr>
                    {{-- expr --}}
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-md-6">
              </div>
            </div>
            <div class="text-right btm-last">
              <p>   
                  {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} 
              </p>
              <p> 
                <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
                  @if(count($deginationsId) > 0)
                  @foreach($deginationsId as $deg)
                  <option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $deg->id == $data->deginations_id ? 'selected' : '' }}>
                    {{ $deg->nameNep }} 
                  </option>
                  @endforeach
                  @endif
                </select>
              </p>
            </div>
            <!--views for nibedak detail -->
            <div class="clearfix"></div>
            <hr>
            <hr>
            @include('admin.shifaris.nibedakCommonField')
            <!-- END -->
          </div>
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>