<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
      <div class="form-group col-md-6">{{-- 11 --}}
        <p>
          ११. परिवारले प्रयोग गर्ने सौचालय कस्तो प्रकारको छ ?
        </p> <br>
        @if (isset($toiletType) && count($toiletType)>0)
        @foreach ($toiletType as $tType)
        <label class="radio-inline">
          <input type="radio" class="rdoClickMe" title="double click for reset" name="toiletTypeNam" value="{{$tType->nameNep}}" {{ isset($hsToiletData) && $hsToiletData->toiletTypeNam == $tType->nameNep ? 'checked="checked"' : '' }}>
          {{$tType->nameNep}}  
        </label> 
        <br />
        @endforeach
        @else
        <label>Insert data from the setting</label>
        @endif
      </div>

      <div class="form-group col-md-6">{{-- 12 --}}
        <p>
          १२. के तपाईको परिवारले अरु परिवारसंग साझेदारी रुपमा चर्पी प्रयोग गर्दछ ?                                  
        </p>
        <br>
        <label class="radio-inline">
          १. छ 
          <input type="radio" class="rdoClickMe" title="double click for reset" name="haveShareToilet" value="1" {{ isset($hsToiletData) && $hsToiletData->haveShareToilet == 1 ? 'checked="checked"' : '' }}>
        </label> 
        <label class="radio-inline">
          २. छैन्
          <input type="radio" class="rdoClickMe" title="double click for reset" name="haveShareToilet" value="0" {{ isset($hsToiletData) && $hsToiletData->haveShareToilet == 0 ? 'checked="checked"' : '' }}>
        </label> 
        <br>
      </div>

    </div>
    <div class="msgDisplay"></div>
  </div>
</div>