@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header><i class="fa fa-plus"></i><span>
			@lang('commonField.button.dailyReport')
		</span></header>
	</div>

	{{ Form::open(['route' => 'dailyReport.store', 'name' => 'frmDailyReport']) }}
	      @include('admin.system.dailyReport._form')
	{{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
	dsgfdsfgfdg
	<button type="button" class="btn btn-danger pull-right" onclick="history.back()">BACK
	</button>
	{{ Form::close() }}
</div>
@endsection
@section('custom_script')
   @include('admin.system.dailyReport.script')
@endsection