@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
<div class="card-box ">
    <div class="card-body">
    <div class="card-head">
          <header><i class="fa fa-plus">&nbsp;@lang('commonField.extra.createLeave')</i></header>
        </div>
         {{ Form::open(['route' => 'wardStaffLeave.store', 'name' => 'frmWardLeave']) }}
            @include('admin.ward.wardStaffLeave._form')
            <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">@lang('commonField.button.back')
            </button>
             {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
         {{ Form::close() }}
    </div>
</div>
</div>
@endsection
@section('custom_script')
    @include('admin.ward.wardStaffLeave.script')
@endsection