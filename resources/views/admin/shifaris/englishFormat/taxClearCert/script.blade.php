<script type="text/javascript">
$(function(){
	/*add buttoin*/
	$("form[name='frmWardtaxClearCert']").validate({
		rules:{
			refCode : {
				required : true,
				minlength : 3,
				maxlength : 10
			},
			issuedDate : {
				required : true,
				date : true
			},
			title : {
				required : true
			},
			personName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			fatherTitle : {
				required : true
			},
			fatherName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			grandFatherTitle : {
				required : true
			},
			grandFatherName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipality : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			ward : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			district : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			former : {
				required : true
			},
			orgAddr : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			orgType : {
				required : true
			},
			orgWard : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			refCode : " Ref code is required",
			issuedDate : "Issue Date is required",
			title : "Title is required",
			personName : "Person name is required",
			fatherTitle : " Father Title is required",
			fatherName : "Father name is required",
			grandFatherTitle : "Grand Father Title is required",
			grandFatherName : "Grand Father Name is required",
			municipality : " Municipality is required",
			ward : " Ward is required",
			district : "District is required",
			former : "Former is required",
			orgAddr : "OrgAddress is required",
			orgType : "Org Type is required",
			orgWard : "Org Ward is required",
			authorizedPerson : "Authorized Person is required"
		}
	});
});

$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                  $("input[name='personName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
            
          },complete:function(){
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>