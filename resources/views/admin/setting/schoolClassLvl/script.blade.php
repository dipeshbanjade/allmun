<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditschoolClassLvl']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of School Class Level is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmschoolClassLvl']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of School Class Level is required"
            }
        });
	var btnAddSchoolClassLvl = $('.btnAddSchoolClassLvl');
	btnAddSchoolClassLvl.on('click', function(e){
		$('#schoolClassLvlModel').modal('show'); 
	});

	var btnEditschoolClassLvl = $('.btnEditschoolClassLvl');
	btnEditschoolClassLvl.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.schoolClassLvlId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editschoolClassLvlModel").modal('show');
			}
		})
	});

	var frmUpdateschoolClassLvl = $('#frmUpdateschoolClassLvl');
  frmUpdateschoolClassLvl.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.schoolClassLvlId').val();
    var url  = "{{URL::to('/')}}" + "/setting/schoolClassLvl/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editschoolClassLvlModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editschoolClassLvlModel').modal('hide');
});
});
</script>
