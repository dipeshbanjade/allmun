<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditsalaryType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of Salary Type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmsalaryType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of Salary Type is required"
            }
        });
    var btnAddSalaryType = $('.btnAddSalaryType');
    btnAddSalaryType.on('click', function(e){
        $('#salaryTypeModel').modal('show'); 
    });

    var btnEditsalaryType = $('.btnEditsalaryType');
    btnEditsalaryType.on('click', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        $.ajax({
            'type': 'GET',
            'url': url,
            success: function (response) {
                $('.salaryTypeId').val(response.id);
                $('.txtRefCode').val(response.refCode).attr('disabled',true);
                $('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
                $("#editsalaryTypeModel").modal('show');
            }
        })
    });

    var frmUpdatesalaryType = $('#frmUpdatesalaryType');
  frmUpdatesalaryType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.salaryTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/salaryType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editsalaryTypeModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editsalaryTypeModel').modal('hide');
});
});
</script>
