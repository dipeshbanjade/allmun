<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class WhyNot extends Model
{
   protected $table = 'why_nots';
	protected $fillable = ['refCode','nameEng','nameNep','tag','status','softDelete']; 
}
