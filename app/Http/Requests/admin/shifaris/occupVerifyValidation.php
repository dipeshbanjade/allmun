<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class occupVerifyValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'refCode' => 'required',
            'issuedDate' => 'required|',
            'letterSub' =>'required|',
            'appName'   => 'required|min:4|max:30',
            'fatherName' => 'required|min:4|max:30',
            'motherName' => 'required|min:4|max:30',
            'municipality' => 'required|min:4|max:30',
            'ward'      => 'required',
            'district'  => 'required|min:4|max:80',
            'former'    => 'required|min:4|max:50',
            'orgAddr'   => 'required|min:4|max:80',
            'orgType'   => 'required|min:4|max:80',
            'orgWard'   => 'required',
            'businessName' => 'required|min:4|max:50',
            'authorizedPerson' => 'required|min:4|max:50'

        ];
    }
}
