<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourtFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('court_fees', function (Blueprint $table) {
            $table->increments('id', 100);

                        $table->string('refCode', 100)->unique();

            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->date('issuedDate');
            $table->string('chalaniNum');
            $table->string('courtAddress');
            $table->string('municipalityName');
            $table->string('municipalityWard');
            $table->string('organizationAddress');
            $table->enum('organizationType', ['गा.वि.स.', 'नगरपालिका', 'उप महानगरपालिका', 'महानगरपालिका']);
            $table->string('organizationWard');
            $table->enum('firstPersonTitle', ['श्री', 'सुश्री', 'श्रीमती']);
            $table->string('firstPersonName');
            $table->enum('firstAccusion', ['बादी', 'प्रतिवादी']);
            $table->enum('secondPersonTitle', ['श्री', 'सुश्री', 'श्रीमती']);
            $table->string('secondPersonName');
            $table->enum('secondAccusion', ['बादी', 'प्रतिवादी']);
            $table->string('districtCourt');
            $table->string('case');
            $table->date('caseDate');

            $table->string('authorizedPerson');
            
            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade'); 

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');             
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('court_fees');
    }
}
