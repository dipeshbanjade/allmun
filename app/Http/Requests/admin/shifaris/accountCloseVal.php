<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class AccountCloseVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:account_closings',
            'chalaniNum' => 'required|unique:account_closings',
            'issuedDate' => 'required',
            'accountHolderName' => 'required', 
            'accountHolderLocation' => 'required',
            'municipalityName' => 'required', 
            'wardNum' => 'required|numeric', 
            'organizationAddress' => 'required',
            'organizationType' => 'required',
            'organizationWard' => 'required|numeric',
            'workName' => 'required',
            'accountNum' => 'required|numeric',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
