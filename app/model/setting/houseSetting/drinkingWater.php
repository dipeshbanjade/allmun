<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class drinkingWater extends Model
{
    protected $table = 'drinking_waters';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
