<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class moneyTransferVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:money_transfers',
            'chalaniNum' => 'required|unique:money_transfers',
            'issuedDate' => 'required',
            'nameOfOffice' => 'required', 
            'districtOfOffice' => 'required',
            'mainAccountNumber' => 'required|numeric',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
