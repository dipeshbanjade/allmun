<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditwhyNot']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 200
                }
            },
            messages: {
                nameEng : "The name of why not is required"
            }
        });
    });

$(function(){
        /*add buttoin*/
        $("form[name='frmwhyNot']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 200
                }
            },
            messages: {
                nameEng : "The name of why not is required"
            }
        });

	var btnAddwhyNot = $('.btnAddwhyNot');
	btnAddwhyNot.on('click', function(e){
		$('#whyNotModel').modal('show'); 
	});

	var btnEditwhyNot = $('.btnEditwhyNot');
	btnEditwhyNot.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
                console.log(response);
				$('.whyNotId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
				$('.txttag').val(response.tag).change();
                
				$("#editwhyNotModel").modal('show');
			}
		})
	});

	var frmUpdatewhyNot = $('#frmUpdatewhyNot');
  frmUpdatewhyNot.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.whyNotId').val();
    var url  = "{{URL::to('/')}}" + "/setting/whyNot/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editwhyNotModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editwhyNotModel').modal('hide');
});
});
</script>
