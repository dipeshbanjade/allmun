<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १.तपाइको परिवारका बालबालिका बालक्लव⁄संगठनमा आवद्ध  छन् ?  
                    </p>
                    <td>
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" onchange="displayChildClubTbl(this)" name="haveChildInvolve" value="1" {{ isset($hsCCIData) && $hsCCIData->haveChildInvolve == '1' ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" onchange="displayChildClubTbl(this)" name="haveChildInvolve" value="0" {{ isset($hsCCIData) && $hsCCIData->haveChildInvolve == '0' ? 'checked' : '' }} checked="">
                        </label> 
                    </td>
                </div>
                <br>
                @if ( isset($hsCCIData) && $hsCCIData->haveChildInvolve == '1')
                    <table border="1" class="childClubTbl" style="display: block;">
                @else
                    <table border="1" class="childClubTbl" style="display: none;">            
                @endif
                
                    <thead>
                        <tr>
                            <th>लिङ्ग</th>
                            <th>विद्यालयमा आधारित</th>
                            <th>टोलमा आधारित</th>
                            <th>श्रमिक बालक्लब</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($hsCCIData)){
                            $schoolRelatedData = (array) json_decode($hsCCIData->schoolRelated);
                            $communityRelatedData = (array) json_decode($hsCCIData->communityRelated);
                            $childClubRelatedData = (array) json_decode($hsCCIData->childClubRelated);  
                        }                        
                        ?>
                        <tr>                                      
                            <td>बालक</td>
                            <td>
                                {{ Form::text('schoolRelated[male]',  isset($hsCCIData) ? $schoolRelatedData['male'] : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Type here', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text('communityRelated[male]',  isset($hsCCIData) ? $communityRelatedData['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Type here', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text('childClubRelated[male]', isset($hsCCIData) ? $childClubRelatedData['male'] : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Type here', 'id' => '']) }}
                            </td>
                        </tr>                                
                        <tr>                                      
                            <td>बालिका</td>
                            <td>
                                {{ Form::text('schoolRelated[female]', isset($hsCCIData) ? $schoolRelatedData['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Type here', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text('communityRelated[female]',  isset($hsCCIData) ? $communityRelatedData['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Type here', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text('childClubRelated[female]', isset($hsCCIData) ? $childClubRelatedData['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Type here', 'id' => '']) }}
                            </td>
                        </tr>                                
                    </tbody>
                </table>
                <br>
            </div>     
        </div>
            <div class="msgDisplay"></div>
        
    </div>
</div>