<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardarthritisSupport']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 15
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 1,
					maxlength : 5,
					number: true
				},
				officeName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				officeAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityName : {
					required : true,
					minlength : 5,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				organizationType : {
					required : true
				},
				organizationWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				patientPrefix : {
					required : true
				},
				patientName : {
					required : true,
					minlength : 5,
					maxlength : 30
				},
				disease : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				hospital : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				officeName : "Office Name is required",
				officeAddress : "office Address is required",
				municipalityName : "Municipality name is required",
				chalaniNumber : "Chalani Number must be number",
				wardNumber : "Ward Number is required",
				organizationWard : "Organization Ward must be number",
				organizationAddress : "Organization Address is required",
				organizationType : "Organization Type is required",
				patientPrefix : "Patient Prefix is required",
				patientName : "Patient Name is required",
				disease : "Disease is required",
				hospital : "Hospital is required",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});
</script>