<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnmarriedVerificationsTable extends Migration
{

    public function up()
    {
        Schema::create('unmarried_verifications', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->string('refCode', 100)->unique();


            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->date('issuedDate');
            $table->text('chalaniNum');
            $table->string('letterSub');
            $table->string('appName');
            $table->string('appRelation');
            $table->string('fatherName');
            $table->string('motherName');
            $table->string('citizenNo', 100);
            $table->string('municipality');      
            $table->string('ward');
            $table->string('district');
            $table->string('former');
            $table->string('orgAddr');
            $table->string('orgType');
            $table->string('orgWard');
            $table->string('appTitleOpt');
            $table->string('appGenderOpt');
            $table->date('dateInAD');
            $table->string('authorizedPerson');

            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');

            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('unmarried_verifications');
    }
}
