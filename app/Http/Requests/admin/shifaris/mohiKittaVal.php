<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class mohiKittaVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:mohi_kittas',
            'issuedDate' => 'required',
            'chalaniNumber' => 'required|unique:mohi_kittas',
            'officeAddress' => 'required',
            'wardNumber'  => 'required',
            'address'  => 'required',
            'seatNumber'  => 'required',
            'kittaNumber'  => 'required',
            'area'  => 'required',
            'mohiName'  => 'required',
            'landOwnerName'  => 'required',
            'sarjaminDate'  => 'required',
            'authorizedPerson'  => 'required',
            'deginations_id'  => 'required'
        ];
    }
}
