<script type="text/javascript">
	{{-- auto width of input --}}
	$.fn.textWidth = function(text, font) {

		if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);

		$.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));

		return $.fn.textWidth.fakeEl.width();
	};

	$('.dashed-input-field').on('input', function() {
		var inputWidth = $(this).textWidth() + 30 + 'px';
		$(this).css({
			width: inputWidth
		})
	}).trigger('input');


	function inputWidth(elem, minW, maxW) {
		elem = $(this);
		console.log(elem)
	}

	var targetElem = $('.dashed-input-field');

	inputWidth(targetElem);


	function changeSelect(object){
		for(var i = 0; i < object.length; i++ ){
			object.options[i].removeAttribute('selected');
		}
		object.options[object.selectedIndex].setAttribute('selected', 'true');
		//'var s = document.getElementsByTag('select')[0];
		// s.options[s.selectedIndex].setAttribute('selected', 'true');
	}

	
	submitMe = $('.sifarishSubmitForm');
	submitMe.on('submit', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		console.log(data);
		var url     = $(this).attr('action');
		var msgDisplay = $('.msgDisplay');
		$.ajax({
			'type' : 'POST',
			'url'  : url,
			'data' : data,
			success : function(response){
				if (response.success==true) {
					msgDisplay.toggle().addClass('alert alert-success');
					msgDisplay.fadeIn().html(response.message);
					printContent('block');
				}
				if (response.success==false) {
					$.each(response.message, function(key, value){
						msgDisplay.append(value).addClass('alert alert-danger');
					})
				}
				setTimeout(function() {
					msgDisplay.fadeOut("slow");
				}, 2000);
			}
		})
		.fail(function (response) {
			alert('Oops your data is not save. some of the field is missing');
		});
	});
	
// $('#saveMe').on('click', function(e){
// 	e.preventDefault();

// 	var myUrl = $(this).attr('action');
// 	var data  = $(this).serialize();

// 	alert(myUrl);

// });
$('input').attr('autocomplete','off');

$('#citizenshipNumber').on('keyup', function(e){

	var citizenId = $(this).val();
		// alert(citizenId);
		makeNebedakEmpty();
		if(citizenId.length >= 4){
			// alert(citizenId);
			var url = "{{URL::to('/')}}" + "/dashboard/nibedakDetail/"+citizenId;
			// alert(url);
			$.ajax({
				'type': 'GET',
				'url': url,
				success: function (response) {
					if(response.success){
						$('#citizenshipNumber').val(response.citizenshipNumber);
						$('#applicantName').val(response.applicantName).prop( "disabled", true );
						$('#applicantAddress').val(response.applicantAddress).prop( "disabled", true );
						$('#id').val(response.id).prop( "disabled", true );
					}else{
						makeNebedakEmpty();
					}
				}
			});
		}
	});

function makeNebedakEmpty(){
	$('#applicantName').val('').prop( "disabled", false );
	$('#applicantAddress').val('').prop( "disabled", false );
	$('#id').val('').prop( "disabled", false );
}

function printContent(elem)
{

		// alert('printing');
		// var mywindow = window.open('', 'PRINT');
		// var style = '{{ asset("lib/css/sifarish.css") }}';
		// var bootstrap = '{{ asset("lib/css/bootstrap.min.css") }}';
		// mywindow.document.write('<html><head>');
		// mywindow.document.write('<link rel="stylesheet" type="text/css" href="'+ bootstrap +'" media="print, screen">');
		// mywindow.document.write('<link rel="stylesheet" type="text/css" href="'+ style +'" media="print, screen">');
		// mywindow.document.write('</head><body><div class="container">');
		// mywindow.document.write(document.getElementsByClassName(elem)[0].outerHTML);
		// mywindow.document.write('</div></body></html>');
	 //    mywindow.focus(); 
	 //    setTimeout(function(){ 
	 //    	mywindow.print();
	 //    	mywindow.close();
	 //    }, 3000);

	 //    return true;

         // var contents = $("#"+elem).html();

         // if ($("#printDiv").length == 0)
         // {
         // var printDiv = null;
         // printDiv = document.createElement('div');
         // printDiv.setAttribute('id','printDiv');
         // printDiv.setAttribute('class','printable');
         // $(printDiv).appendTo('body');
         // }

         // $("#printDiv").html(contents);

         // window.print();

         // $("#printDiv").remove();
         // 
         
         // document.getElementById(el).style = 'font-weight:normal';
         
         var restorepage = $('body').html();
         var printcontent = $('#' + elem).clone();

         // var style = '{{ asset("lib/css/sifarish.css") }}';
         // var bootstrap = '{{ asset("lib/css/bootstrap.min.css") }}';

         $('body').empty().html(printcontent);

         // window.document.write('<link rel="stylesheet" type="text/css" href="'+ bootstrap +'" media="print, screen">');
         // window.document.write('<link rel="stylesheet" type="text/css" href="'+ style +'" media="print, screen">');
         // window.document.write('<link rel="stylesheet" type="text/css" href="'+ style +'" media="print, screen">');

         window.print();
         $('body').html(restorepage);
         location.reload(true);
     }

 //     $('#deginations_id').on('change', function(e){
 //     	// var option = '<option>'+ $(this).val() +'</option>';
 //     	var option = $(this).val();
 //     	$("#deginations_id option:selected").text();
 //     	$('#selectId').val('newValue');
 //     	$(this).selected(option);

 //     	// var option = $(this).val();

 //     	// $(this).find('option:first').remove();
 //     	// $(this).append(option);
	// });
	



</script>

