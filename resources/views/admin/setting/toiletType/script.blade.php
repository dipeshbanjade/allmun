<script type="text/javascript">
$(function(){

	$("form[name='frmedittoiletType']").validate({
		rules:{
			nameEng : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			nameEng : "The name of Toilet Type is required"
		}
	});
});
$(function(){

	$("form[name='frmtoiletType']").validate({
		rules:{

			nameEng : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {

			nameEng : "The name of Toilet Type is required"
		}
	});
	  var btnAddToiletType = $('.btnAddToiletType');
	  btnAddToiletType.on('click', function(e){
	    $('#toiletTypeModel').modal('show'); 
	});

	  var btnEdittoiletType = $('.btnEdittoiletType');
	  btnEdittoiletType.on('click', function(e) {
	  
	      e.preventDefault();
	      var url = $(this).data('url');
	      $.ajax({
	       'type': 'GET',
	       'url': url,
	       success: function (response) {
	          $('.toiletTypeId').val(response.id);
	          $('.txtRefCode').val(response.refCode).attr('disabled',true);
	          $('.txtNameNep').val(response.nameNep);
	          $('.txtNameEng').val(response.nameEng);
	          $("#edittoiletTypeModel").modal('show');
	      }
	  })
	  });

	  var frmUpdatetoiletType = $('#frmUpdatetoiletType');
	  frmUpdatetoiletType.on('submit', function(e){
	      e.preventDefault();
	      var data = $(this).serialize();
	      var id   = $('.toiletTypeId').val();
	      var url  = "{{URL::to('/')}}" + "/setting/toiletType/"+id+"/update";
	      $.ajax({
	          'type' : 'POST',
	          'url'  : url,
	          'data'    : data,
	          success : function(response){
	              console.log(response);
	              if (response.success==true) {
	                  $('.infoDiv').append(response.message).addClass('alert alert-success');
	              }
	              if (response.success==false) {
	                  $.each(response.message, function(key, value){
	                      $('.infoDiv').append(value).addClass('alert alert-danger');
	                  })
	              }
	          },complete:function(){
	              $("#edittoiletTypeModel").modal('hide');
	              setTimeout(location.reload.bind(location), 1500);
	          }
	      })
	      .fail(function (response) {
	          alert('data already exists');
	      });
	      $('#edittoiletTypeModel').modal('hide');
	  });
	  });

</script>