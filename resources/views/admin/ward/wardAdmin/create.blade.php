@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
  	<div class="card-body">
          <div class="card-head">
             <header>
                <h3>
                    <i class="fa fa-plus"></i>
                    @lang('commonField.links.createWardAdmin') <br>
                </h3>
              </header>
           </div>
               {{ Form::open(['route' => 'storeWardAdmin', 'files' => true, 'name' => 'frmWardStaff']) }}
                    @include('admin.ward.wardAdmin._form')
                  <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"><span>@lang('commonField.button.back')</span> 
                  </button>
                  <input class="btn btn-success pull-right" type="submit" value="सिर्जना">
               {{ Form::close() }}
       
	  </div>
  </div>
  </div>
@endsection
@section('custom_script')
   @include('admin.ward.wardStaff.script')
@endsection