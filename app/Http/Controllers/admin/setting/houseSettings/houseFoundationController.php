<?php

namespace App\Http\Controllers\admin\setting\houseSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseSetting\HouseFoundationType;
use App\model\mylogic;
use App\Http\Requests\admin\setting\houseFoundation\houseFoundationValidation;
use Validator;

class houseFoundationController extends Controller
{
    protected $viewPath  = 'admin.setting.houseFoundation';
    protected $houseFoundationTbl = 'house_foundation_types';
    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.HouseFoundationPrefix');
    }
    public function index()
    {
        $page['page_title']       = 'House Foundation';
        $page['page_description'] = 'House Foundation: Details';

        $allhouseFoundationType = HouseFoundationType::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        
        return view($this->viewPath . '.index', compact(['page', 'allhouseFoundationType'])); 
    }

    public function store(Request $request)
    {

      $prefix   = myLogic::sysRefCode($tablename = $this->houseFoundationTbl, $prefix = $this->prefix);
     
           $mySave = HouseFoundationType::create([
             'refCode' => $prefix,
             'foundationNameNep' => $request->foundationNameNep ?? 'N/A',
             'foundationNameEng' => $request->foundationNameEng,
             'status'  => 0,
             'softDelete' => 0
             ]);
           if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function edit($id)
    {
         $data = HouseFoundationType::findOrFail($id);
         if ($data) {
            return response()->json([
              'success' => true,
              'refCode' => $data->refCode,
              'id'      => $data->id,
              'foundationNameNep' => $data->foundationNameNep,
              'foundationNameEng' => $data->foundationNameEng
              ]);
        }else{
            return response()->json([
              'success' => false,
              'message' => config('activityMessage.dataNotFound')
              ]);
        }
    }


    public function update(Request $request, $id)
    {
          $validator = Validator::make($request->all(), [
           'foundationNameEng'  => 'required'
           ]);

          if ($validator->fails()) {
              return response()->json([
                 'success' => false,
                 'message' => $validator->errors()->all()
                 ]);
          }
          $update          = HouseFoundationType::findOrFail($id);
          $update->foundationNameNep = $request->foundationNameNep ?? 'N/A';
          $update->foundationNameEng = $request->foundationNameEng;

          $myUpdate = $update->update();
          if ($myUpdate) {
            return response()->json([
              'success' => true,
              'message' => config('activityMessage.updateMessage')
              ]);
        }else{
            return response()->json([
              'success'  => false,
              'message'  => config('activityMessage.notUpdated')
              ]);
        }
    }


    public function destroy($id)
    {
            $del = HouseFoundationType::findOrFail($id);
            $del->softDelete = 1;
            $myDel = $del->update();
            if ($myDel) {
             return MyLogic::successChangeStatus($code = 200);
         }else{
          return MyLogic::successChangeStatus($code = 301);
        }
    }
}
