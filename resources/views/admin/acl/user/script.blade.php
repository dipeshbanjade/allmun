<script type="text/javascript">
	/*-------------------------------------------*/
	$('.selectRoleFor').on('change', function(e){
		e.preventDefault();
		var selectVal = $(this).val();
		if (selectVal == 'mun') {
			$('.frmContain').toggle();
			$('.dropDownSelectBox').hide();
			$('.dropDownSelectBox').append('');
		}
		if (selectVal == 'wrd') {
			//ward 
			var wards    = "<select name='wards_id' class='form-control sysSelect2' required>"+
			                        "<option value=''>select ward</option>"+
			                        @if(isset($wards))
				                         @if(count($wards) > 0)
				                            @foreach($wards as $ward)
				                                "<option value='{{ $ward->id }}'>{{ $ward->nameNep }} -  {{ $ward->nameEng }}</option>"+
				                            @endforeach
				                         @endif

				                    @endif    
			                       "</select>";
			 $('.frmContain').toggle();
			 $('.dropDownSelectBox').show();
		     $('.dropDownSelectBox').html(wards);
		     $('.dropDownSelectBox').append('');
		}
		selectVal = '';
	});
     /*form validation */
     $(function(){
          $("form[name='frmCreateUser']").validate({
           rules:{
            selectRoleFor : {
              required : true
            },
            refCode :{
                required :true,
                 minlength : 3,
                 maxlength : 16
            },
            citizenNo : {
            	required : true,
            	minlength : 3
            },
            name : {
            	required : true,
            	minlength : 3,
            	maxlength : 30
            },
            email : {
            	required : true,
            	email : true
            },
            roles_id : {
            	required : true
            },
            departments_id : {
            	required : true
            },
            staff_types_id : {
            	required : true
            },
            deginations_id : {
            	required : true
            },
            provinces_id : {
            	required : true
            }, 
            districts_id : {
            	required : true
            },
            phoneNumber : {
            	required : true,
            	number   : true,
            	maxlength : 10
            },
            password : {
            	required : true,
            	minlength : 6,
            	password_confirmation : {
            		equalTo : "#password"
            	}
            }
           
           },
           messages: {
            roleName : "role name required with min with 3 character"
           }
          });
        });
</script>