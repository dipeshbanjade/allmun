<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualIncomeVerifyJapansTable extends Migration
{
    public function up()
    {
        Schema::create('annual_income_verify_japans', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('refCode', 100)->unique();
            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');
            
            $table->date('issuedDate');
            $table->text('chalaniNum');
            $table->string('title');
            $table->string('personName');
            $table->string('relationWithSeeker');
            $table->string('fatherName');
            $table->string('municipality');      
            $table->string('ward');
            $table->string('district');
            $table->string('former');
            $table->string('orgAddr');
            $table->string('orgType');
            $table->string('orgWard');

            $table->text('japanAnnualIncomeDetail');

            // $table->string('incomeSource');
            // $table->string('firstYear');
            // $table->string('secondYear');
            // $table->string('thirdYear');

            $table->string('totalNrsInFirst');
            $table->string('totalNrsInSecond');
            $table->string('totalNrsInThird');
            $table->string('totalUsdInFirst');
            $table->string('totalUsdInSecond');
            $table->string('totalUsdInThird');
            $table->string('total_nrs');
            $table->string('ctype')->nullable();
            $table->string('totalUsd');
            $table->string('authorizedPerson');
            $table->string('exchangeRate');


            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annual_income_verify_japans');
    }
}
