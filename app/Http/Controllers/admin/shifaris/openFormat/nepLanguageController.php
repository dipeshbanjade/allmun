<?php

namespace App\Http\Controllers\admin\shifaris\openFormat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\model\admin\ward\union;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\NepLanguage;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\nepLanguageVal;
use DB;
use Auth;

use App\model\mylogic;

class NepLanguageController extends Controller
{
    protected $viewPath = 'admin.shifaris.openFormat.nepLanguage';
    protected $shifarisTableName = 'nep_languages';
    protected $appUsrTable = 'applicant_users';
    protected $deg = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.prefixNepLanguage');
        
    }

    public function index()
    {
        $page['page_title']       = 'Nepali Language';
        $page['page_description'] = 'Nepali Language';

        
        $nirman = NepLanguage::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        

    }

    public function create()
    {
        $page['page_title']       = 'Nepali Language : create';
        $page['page_description'] = 'Nepali Language details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
                // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(nepLanguageVal $request)
    {
        //
        
        DB::beginTransaction();
        try {

            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

            $isShifarisExist = isset($request->shifarisId) ? NepLanguage::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? NepLanguage::findOrfail($isShifarisExist['id']) : new NepLanguage;

            $save->createdBy = Auth::user()->id;
            
            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";
            $save->chalaniNum  = $request->chalaniNum;
            $save->issuedDate  = $request->issuedDate;
            $save->organizationName = $request->organizationName;
            $save->organizationAddress = $request->organizationAddress;
            $save->orgAddressSec = $request->orgAddressSec ?? 'N/A';
            $save->shifarishSubject = $request->shifarishSubject;
            $save->note = $request->note;

            $bodarthaCount = count($request->bodartha);
            $bodartha = $request->bodartha;
            $bodarthaArray = [];

            for ($i=0; $i < $bodarthaCount; $i++) {
                array_push($bodarthaArray, [
                    'bodartha' => $bodartha[$i]
                ]);
            }
            $save->bodarthaStatement = json_encode($bodarthaArray);

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id = $request->deginations_id;
                // $bodarthaCopyJson = json_encode($request->bodarthaCopy);
                // $save->bodarthaCopy = $bodarthaCopyJson;

            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

                    //end for login
            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
                        $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }

        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }

        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function changeStatus(){

    }
}
