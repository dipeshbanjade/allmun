<style>
.card-head{
  background: #D5D5D5;
}
</style>
<div class="card-box">
 <div class="card-body row">
   <div class="col-lg-12">
    <div class="col-lg-6">

    </div>
    <div class="col-lg-6 pull-right">
      <div class="form-group">
        {{ Form::text('surName', null, ['class' =>'form-control', 'placeholder' => 'serveyer name'])  }}
      </div>
      <div class="form-group">
        {{ Form::text('surPhone', null, ['class' =>'form-control', 'placeholder' => 'serveyer phone number'])  }}
      </div><br><hr>
    </div><div class="clearfix"></div>

    <label>@lang('commonField.extra.citizen')
    </label><br>

    <span>
     <?php  $lan = getLan(); ?>
     <select name="citizen_infos_id" class="sysSelect2" required>
       <option value="">@lang('commonField.extra.citizen')</option>
       @if(count($citizenInfo))
       @foreach($citizenInfo as $citizen)
       <option value="{{ $citizen->id }}" {{ isset($data->citizen_infos_id)  && $data->citizen_infos_id == $citizen->id ? 'selected' : ''}} {{(old('citizen')==$citizen->id)? 'selected':''}}>
         <label><b>CitizenNo : {{ $citizen->citizenNo }}</b> </label>
         <label>
           <b>Name : {{ $lan=="np" ? $citizen->fnameNep . $citizen->mnameNep . $citizen->lnameNep : $citizen->fnameEng . $citizen->mnameEng . $citizen->lnameEng }}</b> 
         </label>
       </option> 
       @endforeach
       @endif
     </select><br><hr>
   </span>

 </div>
 <!--  -->
     <!-- <div class="col-lg-3"> 
       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('toleName', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Tole Name', 'readonly']) }}
        <label class = "mdl-textfield__label">@lang('commonField.address_information.wardNo')</label>
      </div>
    </div> -->
    <!--  -->
    <div class="col-lg-3"> 
     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      {{ Form::text('toleName', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Tole Name']) }}
      <label class = "mdl-textfield__label">@lang('commonField.address_information.gautolBastiKoNaam')</label>
    </div>
  </div>

  <div class="col-lg-3"> 
   <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
     {{ Form::text('streetName',null, ['class'=>'mdl-textfield__input txtMunHouseNumber', 'placeholder'=>'Street Name']) }}
     <label class = "mdl-textfield__label">@lang('commonField.address_information.streetName')
     </label>
   </div>
 </div>

 <div class="col-lg-3"> 
   <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
     {{ Form::text('locAreaNam', isset($data) ? $data->localAreaName : null, ['class'=>'mdl-textfield__input txtlocalAreaName', 'placeholder'=>'local area name']) }}
     <label class = "mdl-textfield__label">@lang('commonField.address_information.localAreaName')</label>
   </div>
 </div> 

 <div class="col-lg-3"> 
   <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
     {{ Form::text('hsNum', null, ['class'=>'mdl-textfield__input txtMunHouseNumber', 'placeholder'=>'House Number']) }}
     <label class = "mdl-textfield__label">@lang('commonField.extra.houseNum')
     </label>
   </div>
 </div>
 <div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('munHsNum', null, ['class'=>'mdl-textfield__input txtaabadhatolBikasSansthakoNaam', 'placeholder'=>'Municipality House Number']) }}
    <label class = "mdl-textfield__label">@lang('commonField.extra.munHouseNumber')</label>
  </div>
</div> 

<div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('fmMem', isset($data) ? $data->fmMember : null, ['class'=>'fmMem mdl-textfield__input txtfamilyMember', 'placeholder'=>'family member']) }}
    <label class = "mdl-textfield__label">@lang('commonField.extra.familyMember')</label>
  </div>
</div> 

<div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('fmHeadPh', null, ['class'=>'mdl-textfield__input txtaddress', 'placeholder'=>'Family Head Phone Number']) }}
    <label class = "mdl-textfield__label">@lang('commonField.extra.familyHeadPhoneNumber')</label>
  </div>
</div>  
<div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('hsLandLineNum', null, ['class'=>'mdl-textfield__input txtaddress', 'placeholder'=>'House Land line number']) }}
    <label class = "mdl-textfield__label">@lang('commonField.personal_information.landLineNumber')</label>
  </div>
</div> 

<div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
    <select name="fmTypeNam" class="form-control">
      <option value="">@lang('commonField.extra.familyType')</option> 
      @if($familyTypeId)
      @foreach($familyTypeId as $familyType)
      <option value="{{ $familyType->nameNep }}" {{ isset($data->fmTypeNam) && $familyType->nameNep == $data->fmTypeNam ? 'selected' : '' }}>
       {{ $familyType->nameNep }}
     </option>
     @endforeach
     @endif        
   </select>
 </div>
</div> 

<!-- <div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('wrdKhanda', isset($data) ? $data->wardKhanda : null, ['class'=>'mdl-textfield__input txtwardKhanda', 'placeholder'=>'Ward khanda']) }}
    <label class = "mdl-textfield__label">@lang('commonField.extra.wardKhanda')</label>
  </div>
</div> --> 

<!-- <div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    <label class = "mdl-textfield__label">@lang('commonField.extra.houseType')</label>
    <select name="hsTypeNam" class="form-control">
      <option value="">@lang('commonField.extra.houseType')</option> 
      @if(count($houseTypeId)>0)
      @foreach($houseTypeId as $houseTypeName)
      <option value="{{ $houseTypeName->houseNep }}" {{ isset($data->hsTypeNam) && $houseTypeName->houseNep == $data->hsTypeNam ? 'selected' : '' }}>
       {{ $houseTypeName->houseNep }}
     </option>
     @endforeach
     @endif        
   </select>
 </div>
</div>  -->     
<!-- <div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    <label class = "mdl-textfield__label">@lang('commonField.extra.roomUsageFor')</label>        
    <select name="romUsgForNam" class="form-control">
      <option value="">@lang('commonField.extra.roomUsageFor')</option> 
      @if($houseTypeId)
      @foreach($roomUseForId as $romUse)
      <option value="{{ $romUse->nameNep }}" {{ isset($data->romUsgForNam)  && $data->romUsgForNam == $romUse->nameNep ? 'selected' : ''}} {{(old('romUse')==$romUse->nameNep)? 'selected':''}}>
       {{ $romUse->nameNep }}
     </option>
     @endforeach
     @endif        
   </select>
 </div>
</div>  -->     

<!-- <div class="col-lg-3"> 
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('memLiving', isset($data) ? $data->memLiving : null, ['class'=>'memLiving mdl-textfield__input txtmemberLiving', 'placeholder'=>' Number of Member Living']) }}
    <label class = "mdl-textfield__label">@lang('commonField.extra.memberLiving')</label>
  </div>
</div>  --> 

</div> 
</div>
<div class="card-head">
  <header><i class='fa fa-plus'></i>&nbsp;Add More Details</header>

</div> 
<div class="card-box">

  <div class="card-body">

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row">
          <div class="form-group col-md-6">
            <p>
              १. तपाईको परिवार हाल बसोबास गरेको घरको स्वामित्व कस्तो प्रकारको हो ?  (सहि उत्तरमा एकमा मात्र ठीक लगाउनु होस)  
            </p><br>
            @if($samiptoHsId)
            @foreach($samiptoHsId as $samiptoHs)
            <label class="radio-inline">
              <input type="radio" name="hsSamipto" class="rdoClickMe" value="{{ $samiptoHs->nameNep }}" {{ isset($data->hsSamipto) && $data->hsSamipto == $samiptoHs->nameNep ? 'checked="checked"' : '' }}>
              {{ $samiptoHs->nameNep }}
            </label>
            <br />
            @endforeach
            @endif
            <label class="radio-inline">
              {{ Form::text('otherHsSamipto', null, ['class'=>'mdl-textfield__input txtlatitude', 'placeholder'=>'other house swamipto name']) }}
            </label> 
          </div>
          <div class="form-group col-md-6">
            <p>
              २. तपाईको परिवारले प्रयोग गरेको घर रहेको जग्गाको स्वामित्व कस्तो हो ? 
            </p>
            <br>
            @if($samiptoLdId)
            @foreach($samiptoLdId as $samiptoLd)
            <label class="radio-inline">
              <input type="radio" name="landSamipto" class="rdoClickMe" value="{{ $samiptoLd->nameNep }}" {{ isset($data->landSamipto) && $data->landSamipto == $samiptoLd->nameNep ? 'checked="checked"' : '' }}>
              {{ $samiptoLd->nameNep }}
            </label> 
            <br />
            @endforeach
            @endif
            <label class="radio-inline">

              {{ Form::text('otherLandSamipto', null, ['class'=>'mdl-textfield__input txtlatitude', 'placeholder'=>'अन्य (खुलाउने)']) }}
            </label> 

          </div>
          <div class="form-group col-md-6">
            <p>
              ३.यदि तपाइको नगरपालिका भित्र आफ्नो परिवारको घर छ भने कति वटा घर रहेका छन् ? 
            </p>
            <label>
              {{ Form::text('numOfHs',null, ['class'=>'mdl-textfield__input txtlatitude', 'placeholder'=>'Number of House']) }}

            </label><br>
          </div>
          <div class="form-group col-md-6">
            <p>
              ४.तपाईको परिवारले प्रयोग गरेको घरको तला संख्या र कोठा संख्या कति छन ? (शौचालय बाहेक)<br>
            </p>
            <label>
              १. तला संख्या :        
              {{ Form::text('florNum',  null, ['class'=>'mdl-textfield__input txtlatitude', 'placeholder'=>'Number of Floor']) }}
             
              {{ Form::text('romNum',  null, ['class'=>'romNum mdl-textfield__input txtlatitude', 'placeholder'=>'Number of Room']) }}
            </label><br>
          </div>
          <div class="form-group col-md-6">
            <p>
              ५.तपाईको परिवारले कति वटा कोठा प्रयोग गरेको छ? (शौचालय बाहेक)<br>  
            </p>
            <label>
              {{ Form::text('noOfRom', isset($data) ? $data->noOfRoom : null, ['class'=>'noOfRom mdl-textfield__input txtnoOfRoom', 'placeholder'=>'Number of Room']) }}
              <label class = "mdl-textfield__label">@lang('commonField.extra.noOfRooms')</label>
            </label><br>
          </div>
          <div class="form-group col-md-6">
            <p>
              ६. तपाईको परिवारमा भान्साकोठा र सुत्नेकोठा अलग्गै छ ?
            </p><br>
            <label class="radio-inline">
              १. छ  
              <input type="radio" class="rdoClickMe" title="double click for reset" name="haveSameKitchen", value="1" {{ isset($data)  && $data->haveSameKitchen ==1  ? 'checked' : ''  }}>
            </label> 
            <label class="radio-inline">
              २. छैन  
              <input type="radio" class="rdoClickMe" title="double click for reset" name="haveSameKitchen" value="0" {{ isset($data)  && $data->haveSameKitchen ==0  ? 'checked' : ''  }}>
            </label> 

          </div>
          <div class="form-group col-md-6">
            <p>
              ७. तपाईको परिवारको घर के को रुपमा प्रयोग गर्नु भएको छ ?
            </p> <br>
            @if($houseUseForId)
            @foreach($houseUseForId as $houseUse)
            <label>
              <input type="radio" name="hsUsgFor" class="rdoClickMe" title="double click for reset"  value="{{ $houseUse->nameNep }}" {{ isset($data->hsUsgFor) && $data->hsUsgFor == $houseUse->nameNep ? 'checked="checked"' : '' }}>
              {{ $houseUse->nameNep }}
            </label>
            <br />
            @endforeach
            @endif
            <br>
          </div>
          <div class="form-group col-md-6">
            <p>
              ८. हाल तपाई बसोबास गरेको घरको अनुमानित क्षेत्रफल कति रहेको छ ? <br>
            </p>
            <label>
              {{ Form::text('areaOfHs',null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'Area of house']) }}
              वर्ग मिटर/वर्ग फिट   
            </label>
            <br />
          </div>
          <div class="form-group col-md-6">
            <p>
              ९. तपाईको परिवार हाल बसोबास गरेको घर कति सालमा निर्माण भएको हो ?
            </p><br>
            <label>
              वि.सं  {{ Form::text('hsEstd',null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'House Construction Year', 'id'=>'myDate']) }} 
            </label>
            <br />
          </div>
          <div class="form-group col-md-6">
            <p>
              १०. तपाईको परिवार बसोबास गरेको घरको जग कस्तो प्रकारको छ? 
            </p><br>
            @if($houseFoundationId)
            @foreach($houseFoundationId as $fnd)
            <label>
              <input type="radio" name="hsFndNam" class="rdoClickMe" title="double click for reset" value="{{ $fnd->foundationNameNep }}" {{ isset($data->hsFndNam) && $data->hsFndNam == $fnd->foundationNameNep ? 'checked="checked"' : '' }} >
              {{ $fnd->foundationNameNep }}
            </label>
            <br />
            @endforeach
            @endif
            <label>
              {{ Form::text('otherHsFndName',null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'अन्य (खुलाउने)']) }}
            </label><br>
          </div>
          <div class="form-group col-md-6">
            <p>
              ११. तपाइको परिवार बसोबास गरेको घरका कोठाहरुको भुई के ले बनेको छ ?
            </p> <br>
            @if($roomFloorTypeId)
            @foreach($roomFloorTypeId as $romFlr)
            <label>
              <input type="radio" name="romFlorNam" class="rdoClickMe" title="double click for reset" value="{{ $romFlr->nameNep }}" {{ isset($data->romFlorNam) && $data->romFlorNam == $romFlr->nameNep ? 'checked="checked"' : '' }}>
              {{ $romFlr->nameNep }}
              </label>
          <br />
          @endforeach()
          @endif
          <label>
            {{ Form::text('otherRomFloorNam',null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'अन्य (खुलाउने)']) }}
          </label><br>
        </div>
        <!--  -->
        <div class="form-group col-md-6">
          <p>
            १२. तपाइको परिवार बसोबास गरेको घरको बाहिरी गारो के ले बनेको छ ? 
          </p>
          <br>
          @if($hsWallTypeId)
          <label>
            @foreach($hsWallTypeId as $hsWall)
            <input type="radio" name="hsWallNam" class="rdoClickMe" title="double click for reset" value="{{ $hsWall->nameNep }}" {{ isset($data->hsWallNam) && $data->hsWallNam == $hsWall->nameNep ? 'checked="checked"' : '' }}>
            {{ $hsWall->nameNep }}
            <br />
            @endforeach
            @endif
            {{ Form::text('otherHsWallNam',null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'अन्य (खुलाउने)']) }}
          </label><br>
        </div>

        <!--  -->
        <div class="form-group col-md-6">
          <p>
            १३. बसोबास गर्ने घरको छानाको प्रकार (एकभन्दा वढी घर भएमा मूल घरको विवरणका आधारमा  एकमा मात्र चिन्ह लगाउने)
          </p>
          <br>
          <label>
            @if($hsRoof)
            @foreach($hsRoof as $roof)
            <input type="radio" name="hsRofNam" class="rdoClickMe" title="double click for reset" value="{{ $roof->nameNep }}" checked>
            {{ $roof->nameNep }}
            <br />
            @endforeach
            @endif

            {{ Form::text('otherHsRofNam',null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'अन्य (उल्लेख गर्ने) ']) }}
          </label><br>
        </div>

        <div class="form-group col-md-6">
          <p>
            १४. तपाई बसोबास गरेको घर भवन मापदण्ड अनुसार बनेको छ ? 
          </p><br>
          <label>
            १. छ  
            <input type="radio" class="rdoClickMe" title="double click for reset" name="isHsMadeByRule" value="छ" {{ isset($data->isHsMadeByRule) && $data->isHsMadeByRule == 'छ' ? 'checked="checked"' : '' }}>
            २. छैन
            <input type="radio" name="isHsMadeByRule" class="rdoClickMe" title="double click for reset" value="छैन" {{ isset($data->isHsMadeByRule) && $data->isHsMadeByRule == 'छैन' ? 'checked="checked"' : '' }}>  
            ३. थाहा छैन
            <input type="radio" name="isHsMadeByRule" value="थाहा छैन" {{ isset($data->isHsMadeByRule) && $data->isHsMadeByRule == 'थाहा छैन' ? 'checked="checked"' : '' }}>
          </label><br>
        </div>

        <div class="form-group col-md-6">
          <p>

            १५. तपाई बसोबास गरेकोे घर भूकम्प प्रतिरोधी छ ? 
          </p>
          <br>
          <label>
            १. छ  
            <input type="radio" name="isHsEqResis" class="rdoClickMe" title="double click for reset" value="छ" {{ isset($data->isHsEqResis) && $data->isHsEqResis == 'छ' ? 'checked="checked"' : '' }}>
            २. छैन
            <input type="radio" name="isHsEqResis" class="rdoClickMe" title="double click for reset"  value="छैन" {{ isset($data->isHsEqResis) && $data->isHsEqResis == 'छैन' ? 'checked="checked"' : '' }}>  
            ३. थाहा छैन
            <input type="radio" name="isHsEqResis" value="थाहा छैन" {{ isset($data->isHsEqResis) && $data->isHsEqResis == 'थाहा छैन' ? 'checked="checked"' : '' }}>
          </label><br>
        </div>
        <div class="form-group col-md-6">
          <p>
            १६. तपाई बसोबास गरेको घरमा चारपाङ्ग्रे सवारी साधन राख्ने ठाउँ छ ?
          </p>
          <br>
          <label>
            १. छ  
            <input type="radio" name="haveParking" class="rdoClickMe" title="double click for reset" value="छ" {{ isset($data->haveParking) && $data->haveParking == 'छ' ? 'checked="checked"' : '' }}>
            २. छैन
            <input type="radio" name="haveParking" value="छैन" {{ isset($data->haveParking) && $data->haveParking == 'छैन' ? 'checked="checked"' : '' }}>  
          </label><br>
        </div>

        <div class="form-group col-md-6">
          <p>
            १७. तपाई बसोबास गरेको घर कम्पाउण्ड भित्र हरिया रुख विरुवा रहेका छन् ?
          </p>
          <br>
          <label>
            १. छ  
            <input type="radio" name="havePlantation" class="rdoClickMe" title="double click for reset" value="छ" {{ isset($data->havePlantation) && $data->havePlantation == 'छ' ? 'checked="checked"' : '' }}>
            २. छैन
            <input type="radio" name="havePlantation" value="छैन" {{ isset($data->havePlantation) && $data->havePlantation == 'छैन' ? 'checked="checked"' : '' }}>  
          </label><br>
        </div>
        <div class="form-group col-md-6">
          <p>
            १८. तपाई बसोबास गरेको घरमा फूलका विरुवा, बगैचा वा गमलामा फूल रोपेको छ ?
          </p>
          <br>
          <label>
            १. छ  
            <input type="radio" name="haveGarden" class="rdoClickMe" title="double click for reset" value="छ" {{ isset($data->haveGarden) && $data->haveGarden == 'छ' ? 'checked="checked"' : '' }}>
            २. छैन
            <input type="radio" name="haveGarden" value="छैन" {{ isset($data->haveGarden) && $data->haveGarden == 'छैन' ? 'checked="checked"' : '' }}>  
          </label><br>
        </div>
                   <!--  <div class="form-group col-md-6"> 
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <label class = "mdl-textfield__label"><p> 19.@lang('commonField.extra.ishouseOwnByHead')</p></label><br>
                        <label>
                          @lang('commonField.extra.yesM')
                          <input type="radio" name="isHsOwnByHead" class="rdoClickMe" title="double click for reset" value="छ" {{ isset($data->isHsOwnByHead) && $data->isHsOwnByHead == 'छ' ? 'checked="checked"' : '' }} required>
                        </label>
                        <label>
                          @lang('commonField.extra.noM')
                          <input type="radio" name="isHsOwnByHead" class="rdoClickMe" title="double click for reset" value="छैन" {{ isset($data->isHsOwnByHead) && $data->isHsOwnByHead == 'छैन' ? 'checked="checked"' : '' }}>          
                        </label>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
              <!-- To Do -->  
              <div class="row">
                <div class="col-lg-3"> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                    {{ Form::text('longi', isset($data) ? $data->longitude : null, ['class'=>'mdl-textfield__input txtlongitude', 'placeholder'=>'longitude']) }}
                    <label class = "mdl-textfield__label">@lang('commonField.distance.longitude')</label>
                  </div>
                </div>

                <div class="col-lg-3"> 
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                    {{ Form::text('lati', isset($data) ? $data->latitude : null, ['class'=>'mdl-textfield__input txtlatitude', 'placeholder'=>'latitude']) }}
                    <label class = "mdl-textfield__label">@lang('commonField.distance.latitude')</label>
                  </div>
                </div> 

                <div class="col-lg-6"> 
                  <div class="form-group">
                    <label> @lang('commonField.extra.house')</label>
                    <div class="form-group imgHouseDetails">
                      <p class="pull-right">
                        <label>
                          <br>
                          <img src="{{ isset($data->hsImgPath) ? asset($data->hsImgPath) : '' }}" width="120" height="80" class="img img-thumbnai" id="imgHouseDetails"><br>

                          <input name="hsImgPath" type='file' onchange="displayImage(this, 'imgHouseDetails');" title="select house image image" />
                        </label>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>