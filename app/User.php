<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\model\admin\acl\Role;
use App\model\admin\acl\Permission;

class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsTo(Role::class);
    }

    public function hasRole($role){
        // $name = Route::currentRouteName();

        if (is_string($role)) {
            return $this->roles->contains('role_id', $role);
        }

        foreach ($role as $myRole) {
                if($this->roles($myRole->role_id)){
                   return true;
                }
            }
        return false;

        // foreach ($role as $myRole) {
        //     if($this->roles($myRole->roleName)){
        //        return true;
        //     }
        // }
        // return false;
        // return !! $role->intersect($this->role)->count();
    }

    public function wards(){
        return $this->belongsTo('App\model\admin\ward\WardDetails'::class);
    }

    public function municipilities(){
       return $this->belongsTo('App\model\admin\municipility\MunicipilityDetails'::class);
    }
}
