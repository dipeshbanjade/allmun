<div class="row top-part">
  <div class="col-md-6">
    <p align="left">
      <b>पत्र संख्य
        @if(isset($data->refCode))
        {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
        @else
        {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
        @endif
      </b>
    </p>
    <p align="left">
      <b> च. नं.:
       {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
     </b>
   </p>
 </div>
 <div class="col-md-6">
  <p align="right">
    <b>
      मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    
    <h4 align="center">
      <b>विषय: चार किल्ला प्रमाणित।</b>
    </h4>
  </div>
</div>

<div class="row">
  <div class="col-md-12 content-para">
    <p>
      <b>
       {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
     </b> 
     महा/उपमाहा/नगरपालिका/गाउँपालिका वडा नं.
     
     {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
     
     (साबिकको ठेगाना  
     
     {{ Form::text('sabikAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'sabikAddress', 'required' => 'required']) }}
     
     अन्तर्गत श्री/सुश्री/श्रीमती 
     
     {{ Form::text('shrestaName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'shrestaName', 'required' => 'required']) }}
     
     को नाममा मालपोत कार्यालयमा श्रेस्ता दर्ता कायम रहेको जग्गाको चार किल्ला निम्न विवरण अनुसार भएको प्रमाणित गरिन्छ ।
   </p> 
   <p align="center">
    <b>
      चर किल्ला विवरण
    </b>
  </p>
  <table class="table table-bordered" id="business_close_table">
    <thead>
      <tr>
        <th width="2%">क्र.स.</th>
        <th width="2%">वार्ड नं.</th>
        <th width="3%">सिट नं/कि.नं/क्षेत्रफल</th>
        <th width="15%">बाटो भएको/नभएको</th>
        <th width="15%">पूर्व</th>
        <th width="15%">पश्चिम</th>
        <th width="15%">उत्तर</th>
        <th width="15%">दक्षिण</th>
        <th width="15%">कैफियत</th>
      </tr>
    </thead>
    <tbody id="tableBody">
      <?php
        $count= 1;
        $tableData= !empty($data) ? json_decode($data->tableData,true) : [1];
      ?>
      @foreach ($tableData as $singleData)
        <tr>
          <td>            
            {{$count++}}
          </td>
          <td>
            {{ Form::text('ward[]', !empty($singleData['ward']) ? $singleData['ward'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </td>
          <td>
            {{ Form::text('seatNum[]', !empty($singleData['seatNum']) ? $singleData['seatNum'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </td>
          <td>
            {{ Form::text('roadStatus[]', !empty($singleData['roadStatus']) ? $singleData['roadStatus'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </td>
          <td>
            {{ Form::text('east[]', !empty($singleData['east']) ? $singleData['east'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </td>
          <td>
            {{ Form::text('west[]', !empty($singleData['west']) ? $singleData['west'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </td>
          <td>
            {{ Form::text('north[]', !empty($singleData['north']) ? $singleData['north'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </td>
          <td>
            {{ Form::text('south[]', !empty($singleData['south']) ? $singleData['south'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </td>
          <td>
            {{ Form::text('kaifiyat[]', !empty($singleData['kaifiyat']) ? $singleData['kaifiyat'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </td>
          @if ($count==2)
            <td class="add-btns" style="border: 0px;">
              <button type="button" id="addNewRowInTable" class="btn btn-success"><span class="fa fa-plus"></span></button>
            </td>
          @else
            <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
          @endif
          <!-- <td><a href="" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></a></td> -->
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
<div class="text-right btm-last">
  <p>
    <input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required" value="{{!empty($data)? $data->authorizedPerson :'' }}">
  </p>
  <p> 
    <b>
      <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
        @if(count($deginationsId) > 0)
        @foreach($deginationsId as $deg)
        <option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
          {{ $deg->nameNep }} 
        </option>
        @endforeach
        @endif
      </select>
    </b>
  </p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>