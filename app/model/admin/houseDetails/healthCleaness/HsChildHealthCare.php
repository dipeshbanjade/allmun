<?php

namespace App\model\admin\houseDetails\healthCleaness;

use Illuminate\Database\Eloquent\Model;

class HsChildHealthCare extends Model
{
    protected $table = 'hs_child_health_cares';
}
