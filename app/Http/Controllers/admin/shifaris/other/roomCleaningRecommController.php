<?php

namespace App\Http\Controllers\admin\shifaris\other;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\model\admin\ward\union;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\RoomCleaningRecomm;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\RoomCleaningRecommVal;
use DB;
use Auth;

use App\model\mylogic;

class RoomCleaningRecommController extends Controller
{
    protected $viewPath = 'admin.shifaris.other.roomCleaningRecomm';
    protected $shifarisTableName = 'room_cleaning_recomms';
    protected $appUsrTable = 'applicant_users';
    protected $deg = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.prefixRoomCleaningRecomm');

    }

    public function index()
    {
        $page['page_title']       = 'Room Cleaning Recommendation';
        $page['page_description'] = 'Room Cleaning Recommendation';

        
        $nirman = RoomCleaningRecomm::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

    }

    public function create()
    {
        $page['page_title']       = 'Room Cleaning Recommendation : create';
        $page['page_description'] = 'Room Cleaning Recommendation details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
                // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(RoomCleaningRecommVal $request)
    {
        //
        DB::beginTransaction();
        try {

                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);
            $isShifarisExist = isset($request->shifarisId) ? RoomCleaningRecomm::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? RoomCleaningRecomm::findOrfail($isShifarisExist['id']) : new RoomCleaningRecomm;

            $save->createdBy = Auth::user()->id;

            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";
            $save->issuedDate  = $request->issuedDate;
            $save->chalaniNum  = $request->chalaniNum;
            $save->organizationOfficer  = $request->organizationOfficer;
            $save->officerName = $request->officerName;
            $save->municipalityName = $request->municipalityName;
            $save->municipalityWard = $request->municipalityWard;
            $save->organizationAddress = $request->organizationAddress;
            $save->organizationType = $request->organizationType;
            $save->organizationWard = $request->organizationWard;
            $save->title = $request->title;
            $save->nameOfPerson = $request->nameOfPerson;

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id = $request->deginations_id;

            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

                    //end for login
            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

                // $appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $save->id, $applicantName, $applicantAddress, $citizenshipNumber, $phoneNumber, $email, $dob, $fatherName, $profilePic, $citizenshipImgPath);


            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                ]);
                    // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                ]);
                    // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }

        }catch (Exception $e) {
            DB::rollback();
            return back()->withMessage(config('activityMessage.dataNotInserted'));
        }

        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function changeStatus(){

    }
}
