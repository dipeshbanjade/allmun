@extends('main.app')
@section('custom_css')
    @include('admin.LeaveIsFor.style')
@endsection
@section('content')
    <div class="container">
        <div class="wrap row">
            <div class="col-md-6 col-lg-6 col-sm-12">
                <h4>Create Official Leave</h1>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-12">
                <a href="/leaveList" class="btn btn-primary create-button pull-right" >Holiday's List</a>
            </div>
        </div>
        <div class="clear"></div>
            {{ Form::open(['route' => 'LeaveForStore', 'name' => 'LeaveFor', 'files' => true]) }}
                @include('admin.LeaveIsFor._form')    
                {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right','style'=>'margin-top: -50px; margin-right: 10px;' ]) }}
                
            {{ Form::close() }}
    </div>
    
@endsection
@section('custom_script')
    @include('admin.LeaveIsFor.script')
@endsection