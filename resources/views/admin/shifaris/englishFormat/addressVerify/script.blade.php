<script type="text/javascript">
$(function(){
	/*add buttoin*/
	$("form[name='frmWardaddressVerify']").validate({
		rules:{
			refCode : {
				required : true,
				minlength: 3,
				maxlength: 10
			},
			issuedDate : {
				required : true,
				date : true
			},
			prevVdcName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			prevWardNo : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			wardNo : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			}
		},
		messages: {
			refCode : "refCode is required",
			issuedDate : "Issue Date is required",
			prevVdcName : "VDC name Required",
			prevWardNo : "Ward Number Required",
			wardNo : "Ward Number must be numeric"
		}
	});
});
</script>