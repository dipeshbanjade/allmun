<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class AgeGroup extends Model
{
    protected $table = 'age_groups';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
