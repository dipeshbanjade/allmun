@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
    <div class="card-body">
    <div class="munCreate">
          @if(!count($munDetails) > 0)
              <button class="btn btn-primary pull-right" onclick="myFunction()">
              <i class="fa fa-plus"></i>&nbsp;@lang('commonField.extra.createMunicipility')</button> 
          <h1 class="fa fa-plus fontColorBlack">
            @lang('commonField.extra.createMunicipility')
          </h1>
             @else
                  <div class="card-head">
                      <header><i class="fa fa-eye">&nbsp;@lang('commonField.extra.muncipility')</i></header>

                      <div class="col-md-12">
                          <!-- BEGIN PROFILE SIDEBAR -->
                          <div class="profile-sidebar">
                              <div class="card card-topline-aqua">
                                  <div class="card-body no-padding height-9">
                                      <div class="row"><!-- isset($munDetails) ? asset($munDetails->munLogo) : asset('image/defaultUser.png') -->
                                          <div class="profile-userpic">
                                              <img src="{{ asset($munDetails->munLogo) }}" width="150" height="150" class="img-circle user-img-circle" alt="Municipality Logo" />
                                              </div>
                                      </div>
                                      <div class="profile-usertitle">
                                          <div class="profile-usertitle-name">
                                                {{ $munDetails->nameNep }} - {{ $munDetails->nameEng }} 
                                          </div>
                                          <div class="profile-usertitle-job">
                                             Ref Code : {{ $munDetails->uniqueCode }}
                                          </div>
                                          <div class="profile-usertitle-job">
                                            <label for="">No. of Staff: {{ isset($staffNum) ? $staffNum : 'N/A' }}</label>
                                          </div>

                                          <div class="profile-usertitle-job">
                                            <label for="">No. of Wards: {{ isset($wardNum) ? $wardNum : 'N/A' }}</label>
                                          </div>
                                      </div>
                                      
                                      <!-- END SIDEBAR USER TITLE -->
                                  </div>
                              </div>
                          </div>
                          
                          <!-- BEGIN PROFILE CONTENT -->
                          <div class="profile-content">
                              <div class="row">
                                  <div class="col-md-12">
                                      <div class="card">
                                          <div class="card-head card-topline-aqua">
                                              <header>Description</header>
                                          </div>
                                          <table class="table table-striped">
                                              <tr>
                                                <th>@lang('sidebarMenu.system_setting.municipility')</th>
                                                <td>{{ $munDetails->nameNep }} - {{ $munDetails->nameEng }}</td>
                                              </tr>
                          
                                              <tr>
                                                <th>@lang('commonField.address_information.phone')</th>
                                                <td>{{ $munDetails->landLineNumber }} &nbsp; @lang('commonField.address_information.faxNumber') : {{ $munDetails->faxNumber }}</td>
                                              </tr>
                                              
                                              <tr>
                                                  <th>@lang('commonField.address_information.province')</th>
                                                  <td>{{ $munDetails->provinces->name }}</td>
                                              </tr>
                          
                                              <tr>
                                                  <th>@lang('commonField.address_information.district')</th>
                                                  <td>{{ $munDetails->districts->districtNameNep}}</td>
                                              </tr>
                          
                          
                                              <tr>
                                                <th>@lang('commonField.address_information.address')</th>
                                                <td>
                                                  {{ $munDetails->addrNep }}   - {{ $munDetails->addrEng }}  
                                                </td>
                                              </tr>
                                              
                                              <tr>
                                                <th>@lang('commonField.extra.muncipilityHead')</th>
                                                <td>
                                                  {{ $munDetails->muncipilityHead }} 
                                                </td>
                                              </tr>
                          
                                            </table>
                                      </div>
                                      <p class="pull-right profile-btn">
                                          <button type="button" class="btn btn-danger pull-right" onclick="history.back()">
                                          <i class="fa fa-arrow-left"><span>@lang('commonField.button.back')</span> </i>
                                          </button>
                                          <a href="{{ route('admin.mun-set.edit', $munDetails->id) }}" type="button" class="btn btn-success pull-right">
                                              <i class="fa fa-edit"><span>@lang('commonField.button.edit')</span> </i>
                                          </a>
                                          </p>
                                  </div>
                              </div>
                              <!-- END PROFILE CONTENT -->
                          </div>
                      </div>
                  </div>
             @endif
    </div>
          
         
        </div>
      </div>
    </div>
    
    
    <div class="col-sm-12 col-md-12" id="myDIV" style="display: none;">
      <div class="card-box">
        <div class="card-body ">

         <div class="card-head">
          <header>@lang('commonField.extra.createMunicipility')</header>
        </div>
          {{  Form::open(['route' => 'admin.mun-set.store', 'name' => 'createMunFrm', 'files'=>true]) }}
              @include('admin.municipility.details._form')
              <div class="clearfix"></div>
              <div class="modal-footer">
                  <div class="col-lg-12 p-t-20 text-right"> 
                        {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
                       <button type="button" class="btn btn-danger pull-right" onclick="history.back()">@lang('commonField.button.back')
                       </button>
                  </div>
              </div>
      {{ Form::close() }}
  </div>                           
</div>
</div>
</div>
@endsection
@section('custom_script')
  @include('admin.municipility.details.script')
  <script type="text/javascript">
     function myFunction() {
        var btnCreateMunicipility = document.getElementById("myDIV");
        if (btnCreateMunicipility.style.display === "none") {
            btnCreateMunicipility.style.display = "block";
        } else {
            btnCreateMunicipility.style.display = "none";
        }
    }
  </script>
@endsection