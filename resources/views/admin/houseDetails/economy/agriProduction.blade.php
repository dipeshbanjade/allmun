<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <p>
                 ३. तपाईको परिवारको वार्षिक बाली उत्पादन तथा बिक्रि परिमाण कति रहेको छ ? (गत कृषि वर्ष भित्र )
             </p>
             <div class="    col-md-12">
                <?php                                 
                $agrProdArr = isset($agriProdDetails->hsAgrProductionDetails) ? json_decode($agriProdDetails->hsAgrProductionDetails, true) : '';
                //dd($agrProdArr);
                ?> 
                <table class="table">
                    <tr>
                      <th>बाली</th>
                      <th>उत्पादन (क्विन्टल)</th>
                      <th>खेती गरिएको जग्गाकोक्षेत्रफल, रोपनी</th>
                      <th>विक्री परिमाण (क्विन्टल)</th>
                  </tr>
                  <!-- First row for annaBali starts from here -->
                  <tr>
                      <th>(क) अन्नबाली </th>
                      <th colspan="3"></th>
                  </tr>
                  <tr>
                    <td>
                        <?php //dd($agrProdArr); ?>
                        <input type="checkbox" name="hsAgrProductionDetails[अन्नबालीधान][selectStatus]" value="1" {{ isset($agrProdArr['अन्नबालीधान']['selectStatus']) && $agrProdArr['अन्नबालीधान']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                      धान          
                  </td>
                  <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीधान][production]", isset($agrProdArr['अन्नबालीधान']['production']) ? $agrProdArr['अन्नबालीधान']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीधान][area]", isset($agrProdArr['अन्नबालीधान']['area']) ? $agrProdArr['अन्नबालीधान']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीधान][sale]", isset($agrProdArr['अन्नबालीधान']['sale']) ? $agrProdArr['अन्नबालीधान']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="hsAgrProductionDetails[अन्नबालीमकै][selectStatus]" value="1" {{ isset($agrProdArr['अन्नबालीमकै']['selectStatus']) && $agrProdArr['अन्नबालीमकै']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                    मकै   
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीमकै][production]", isset($agrProdArr['अन्नबालीमकै']['production']) ? $agrProdArr['अन्नबालीमकै']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीमकै][area]", isset($agrProdArr['अन्नबालीमकै']['area']) ? $agrProdArr['अन्नबालीमकै']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीमकै][sale]", isset($agrProdArr['अन्नबालीमकै']['sale']) ? $agrProdArr['अन्नबालीमकै']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="hsAgrProductionDetails[अन्नबालीगहुँ][selectStatus]" value="1" {{ isset($agrProdArr['अन्नबालीगहुँ']['selectStatus']) && $agrProdArr['अन्नबालीगहुँ']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                    गहुँ          
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीगहुँ][production]", isset($agrProdArr['अन्नबालीगहुँ']['production']) ? $agrProdArr['अन्नबालीगहुँ']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीगहुँ][area]", isset($agrProdArr['अन्नबालीगहुँ']['area']) ? $agrProdArr['अन्नबालीगहुँ']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीगहुँ][sale]", isset($agrProdArr['अन्नबालीगहुँ']['sale']) ? $agrProdArr['अन्नबालीगहुँ']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="hsAgrProductionDetails[अन्नबालीकोदो][selectStatus]" value="1" {{ isset($agrProdArr['अन्नबालीकोदो']['selectStatus']) && $agrProdArr['अन्नबालीकोदो']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                    कोदो                                    
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीकोदो][production]", isset($agrProdArr['अन्नबालीकोदो']['production']) ? $agrProdArr['अन्नबालीकोदो']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीकोदो][area]", isset($agrProdArr['अन्नबालीकोदो']['area']) ? $agrProdArr['अन्नबालीकोदो']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्नबालीकोदो][sale]", isset($agrProdArr['अन्नबालीकोदो']['sale']) ? $agrProdArr['अन्नबालीकोदो']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="hsAgrProductionDetails[अन्यअन्नबाली][selectStatus]" value="1" {{ isset($agrProdArr['अन्यअन्नबाली']['selectStatus']) && $agrProdArr['अन्यअन्नबाली']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                    अन्य          
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्यअन्नबाली][production]", isset($agrProdArr['अन्यअन्नबाली']['production']) ? $agrProdArr['अन्यअन्नबाली']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्यअन्नबाली][area]", isset($agrProdArr['अन्यअन्नबाली']['area']) ? $agrProdArr['अन्यअन्नबाली']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
                <td>
                    {{ Form::text("hsAgrProductionDetails[अन्यअन्नबाली][sale]", isset($agrProdArr['अन्यअन्नबाली']['sale']) ? $agrProdArr['अन्यअन्नबाली']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                </td>
            </tr>
            <!-- First row for annaBali Ends here -->

            <!-- Second row  starts from here -->
            <tr>
              <th>(ख) दलहन बाली</th>
              <th colspan="3"></th>
          </tr>
          <tr>
            <td>
                <input type="checkbox" name="hsAgrProductionDetails[दलहन बालीमास][selectStatus]" value="1" {{ isset($agrProdArr['दलहन बालीमास']['selectStatus']) && $agrProdArr['दलहन बालीमास']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                मास                    
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमास][production]", isset($agrProdArr['दलहन बालीमास']['production']) ? $agrProdArr['दलहन बालीमास']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमास][area]", isset($agrProdArr['दलहन बालीमास']['area']) ? $agrProdArr['दलहन बालीमास']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमास][sale]", isset($agrProdArr['दलहन बालीमास']['sale']) ? $agrProdArr['दलहन बालीमास']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="hsAgrProductionDetails[दलहन बालीमकै][selectStatus]" value="1" {{ isset($agrProdArr['दलहन बालीमकै']['selectStatus']) && $agrProdArr['दलहन बालीमकै']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                मकै   
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमकै][production]", isset($agrProdArr['दलहन बालीमकै']['production']) ? $agrProdArr['दलहन बालीमकै']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमकै][area]", isset($agrProdArr['दलहन बालीमकै']['area']) ? $agrProdArr['दलहन बालीमकै']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमकै][sale]", isset($agrProdArr['दलहन बालीमकै']['sale']) ? $agrProdArr['दलहन बालीमकै']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="hsAgrProductionDetails[दलहन बालीरहर][selectStatus]" value="1" {{ isset($agrProdArr['दलहन बालीरहर']['selectStatus']) && $agrProdArr['दलहन बालीरहर']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                रहर            
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीरहर][production]", isset($agrProdArr['दलहन बालीरहर']['production']) ? $agrProdArr['दलहन बालीरहर']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीरहर][area]", isset($agrProdArr['दलहन बालीरहर']['area']) ? $agrProdArr['दलहन बालीरहर']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीरहर][sale]", isset($agrProdArr['दलहन बालीरहर']['sale']) ? $agrProdArr['दलहन बालीरहर']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="hsAgrProductionDetails[दलहन बालीमुसरो][selectStatus]" value="1" {{ isset($agrProdArr['दलहन बालीमुसरो']['selectStatus']) && $agrProdArr['दलहन बालीमुसरो']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                मुसरो                                                
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमुसरो][production]", isset($agrProdArr['दलहन बालीमुसरो']['production']) ? $agrProdArr['दलहन बालीमुसरो']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमुसरो][area]", isset($agrProdArr['दलहन बालीमुसरो']['area']) ? $agrProdArr['दलहन बालीमुसरो']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीमुसरो][sale]", isset($agrProdArr['दलहन बालीमुसरो']['sale']) ? $agrProdArr['दलहन बालीमुसरो']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="hsAgrProductionDetails[दलहन बालीचना][selectStatus]" value="1" {{ isset($agrProdArr['दलहन बालीचना']['selectStatus']) && $agrProdArr['दलहन बालीचना']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                चना                    
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीचना][production]", isset($agrProdArr['दलहन बालीचना']['production']) ? $agrProdArr['दलहन बालीचना']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीचना][area]", isset($agrProdArr['दलहन बालीचना']['area']) ? $agrProdArr['दलहन बालीचना']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीचना][sale]", isset($agrProdArr['दलहन बालीचना']['sale']) ? $agrProdArr['दलहन बालीचना']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="hsAgrProductionDetails[दलहन बालीसिमी][selectStatus]" value="1" {{ isset($agrProdArr['दलहन बालीसिमी']['selectStatus']) && $agrProdArr['दलहन बालीसिमी']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                सिमी                             
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीसिमी][production]", isset($agrProdArr['दलहन बालीसिमी']['production']) ? $agrProdArr['दलहन बालीसिमी']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीसिमी][area]", isset($agrProdArr['दलहन बालीसिमी']['area']) ? $agrProdArr['दलहन बालीसिमी']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीसिमी][sale]", isset($agrProdArr['दलहन बालीसिमी']['sale']) ? $agrProdArr['दलहन बालीसिमी']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="hsAgrProductionDetails[दलहन बालीभट्टमास][selectStatus]" value="1" {{ isset($agrProdArr['दलहन बालीभट्टमास']['selectStatus']) && $agrProdArr['दलहन बालीभट्टमास']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                भट्टमास                              
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीभट्टमास][production]", isset($agrProdArr['दलहन बालीभट्टमास']['production']) ? $agrProdArr['दलहन बालीभट्टमास']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीभट्टमास][area]", isset($agrProdArr['दलहन बालीभट्टमास']['area']) ? $agrProdArr['दलहन बालीभट्टमास']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीभट्टमास][sale]", isset($agrProdArr['दलहन बालीभट्टमास']['sale']) ? $agrProdArr['दलहन बालीभट्टमास']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox" name="hsAgrProductionDetails[दलहन बालीअन्य][selectStatus]" value="1" {{ isset($agrProdArr['दलहन बालीअन्य']['selectStatus']) && $agrProdArr['दलहन बालीअन्य']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
                अन्य                             
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीअन्य][production]", isset($agrProdArr['दलहन बालीअन्य']['production']) ? $agrProdArr['दलहन बालीअन्य']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीअन्य][area]", isset($agrProdArr['दलहन बालीअन्य']['area']) ? $agrProdArr['दलहन बालीअन्य']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
            <td>
                {{ Form::text("hsAgrProductionDetails[दलहन बालीअन्य][sale]", isset($agrProdArr['दलहन बालीअन्य']['sale']) ? $agrProdArr['दलहन बालीअन्य']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
            </td>
        </tr>
        <!-- Second row  Ends here -->

        <!-- Third row  starts from here -->
        <tr>
          <th>(ग) तेलहन बाली</th>
          <th colspan="3"></th>
      </tr>
      <tr>
        <td>
            <input type="checkbox" name="hsAgrProductionDetails[तेलहन बालीतोरी/सस्र्यु][selectStatus]" value="1" {{ isset($agrProdArr['तेलहन बालीतोरी/सस्र्यु']['selectStatus']) && $agrProdArr['तेलहन बालीतोरी/सस्र्यु']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
            तोरी/सस्र्यु          
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीतोरी/सस्र्यु][production]", isset($agrProdArr['तेलहन बालीतोरी/सस्र्यु']['production']) ? $agrProdArr['तेलहन बालीतोरी/सस्र्यु']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीतोरी/सस्र्यु][area]", isset($agrProdArr['तेलहन बालीतोरी/सस्र्यु']['area']) ? $agrProdArr['तेलहन बालीतोरी/सस्र्यु']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीतोरी/सस्र्यु][sale]", isset($agrProdArr['तेलहन बालीतोरी/सस्र्यु']['sale']) ? $agrProdArr['तेलहन बालीतोरी/सस्र्यु']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" name="hsAgrProductionDetails[तेलहन बालीआलस/तिल][selectStatus]" value="1" {{ isset($agrProdArr['तेलहन बालीआलस/तिल']['selectStatus']) && $agrProdArr['तेलहन बालीआलस/तिल']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
            आलस/तिल   
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीआलस/तिल][production]", isset($agrProdArr['तेलहन बालीआलस/तिल']['production']) ? $agrProdArr['तेलहन बालीआलस/तिल']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीआलस/तिल][area]", isset($agrProdArr['तेलहन बालीआलस/तिल']['area']) ? $agrProdArr['तेलहन बालीआलस/तिल']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीआलस/तिल][sale]", isset($agrProdArr['तेलहन बालीआलस/तिल']['sale']) ? $agrProdArr['तेलहन बालीआलस/तिल']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" name="hsAgrProductionDetails[तेलहन बालीसूर्यमुखी][selectStatus]" value="1" {{ isset($agrProdArr['तेलहन बालीसूर्यमुखी']['selectStatus']) && $agrProdArr['तेलहन बालीसूर्यमुखी']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
            सूर्यमुखी                      
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीसूर्यमुखी][production]", isset($agrProdArr['तेलहन बालीसूर्यमुखी']['production']) ? $agrProdArr['तेलहन बालीसूर्यमुखी']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीसूर्यमुखी][area]", isset($agrProdArr['तेलहन बालीसूर्यमुखी']['area']) ? $agrProdArr['तेलहन बालीसूर्यमुखी']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीसूर्यमुखी][sale]", isset($agrProdArr['तेलहन बालीसूर्यमुखी']['sale']) ? $agrProdArr['तेलहन बालीसूर्यमुखी']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
    </tr>
    <tr>
        <td>
            <input type="checkbox" name="hsAgrProductionDetails[तेलहन बालीअन्य][selectStatus]" value="1" {{ isset($agrProdArr['तेलहन बालीअन्य']['selectStatus']) && $agrProdArr['तेलहन बालीअन्य']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
            अन्य                                     
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीअन्य][production]", isset($agrProdArr['तेलहन बालीअन्य']['production']) ? $agrProdArr['तेलहन बालीअन्य']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीअन्य][area]", isset($agrProdArr['तेलहन बालीअन्य']['area']) ? $agrProdArr['तेलहन बालीअन्य']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
        <td>
            {{ Form::text("hsAgrProductionDetails[तेलहन बालीअन्य][sale]", isset($agrProdArr['तेलहन बालीअन्य']['sale']) ? $agrProdArr['तेलहन बालीअन्य']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
        </td>
    </tr>
    <!-- Third row  Ends here -->

    <!-- Fourth row  starts from here -->
    <tr>
      <th>(घ) तरकारी बाली</th>
      <th colspan="3"></th>
  </tr>
  <tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[तरकारी बालीआलु][selectStatus]" value="1" {{ isset($agrProdArr['तरकारी बालीआलु']['selectStatus']) && $agrProdArr['तरकारी बालीआलु']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        आलु                    
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीआलु][production]", isset($agrProdArr['तरकारी बालीआलु']['production']) ? $agrProdArr['तरकारी बालीआलु']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीआलु][area]", isset($agrProdArr['तरकारी बालीआलु']['area']) ? $agrProdArr['तरकारी बालीआलु']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीआलु][sale]", isset($agrProdArr['तरकारी बालीआलु']['sale']) ? $agrProdArr['तरकारी बालीआलु']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[तरकारी बालीबन्दागोभी/काउली÷रायो][selectStatus]" value="1" {{ isset($agrProdArr['तरकारी बालीबन्दागोभी/काउली÷रायो']['selectStatus']) && $agrProdArr['तरकारी बालीबन्दागोभी/काउली÷रायो']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        बन्दागोभी/काउली÷रायो   
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीबन्दागोभी/काउली÷रायो][production]", isset($agrProdArr['तरकारी बालीबन्दागोभी/काउली÷रायो']['production']) ? $agrProdArr['तरकारी बालीबन्दागोभी/काउली÷रायो']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीबन्दागोभी/काउली÷रायो][area]", isset($agrProdArr['तरकारी बालीबन्दागोभी/काउली÷रायो']['area']) ? $agrProdArr['तरकारी बालीबन्दागोभी/काउली÷रायो']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीबन्दागोभी/काउली÷रायो][sale]", isset($agrProdArr['तरकारी बालीबन्दागोभी/काउली÷रायो']['sale']) ? $agrProdArr['तरकारी बालीबन्दागोभी/काउली÷रायो']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[तरकारी बालीबोडी][selectStatus]" value="1" {{ isset($agrProdArr['तरकारी बालीबोडी']['selectStatus']) && $agrProdArr['तरकारी बालीबोडी']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        बोडी                               
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीबोडी][production]", isset($agrProdArr['तरकारी बालीबोडी']['production']) ? $agrProdArr['तरकारी बालीबोडी']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीबोडी][area]", isset($agrProdArr['तरकारी बालीबोडी']['area']) ? $agrProdArr['तरकारी बालीबोडी']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीबोडी][sale]", isset($agrProdArr['तरकारी बालीबोडी']['sale']) ? $agrProdArr['तरकारी बालीबोडी']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[तरकारी बालीगोलभेंडा][selectStatus]" value="1" {{ isset($agrProdArr['तरकारी बालीगोलभेंडा']['selectStatus']) && $agrProdArr['तरकारी बालीगोलभेंडा']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        गोलभेंडा                                              
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीगोलभेंडा][production]", isset($agrProdArr['तरकारी बालीगोलभेंडा']['production']) ? $agrProdArr['तरकारी बालीगोलभेंडा']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीगोलभेंडा][area]", isset($agrProdArr['तरकारी बालीगोलभेंडा']['area']) ? $agrProdArr['तरकारी बालीगोलभेंडा']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीगोलभेंडा][sale]", isset($agrProdArr['तरकारी बालीगोलभेंडा']['sale']) ? $agrProdArr['तरकारी बालीगोलभेंडा']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[तरकारी बाली काँक्रो, लौका,फर्सी][selectStatus]" value="1" {{ isset($agrProdArr['तरकारी बाली काँक्रो, लौका,फर्सी']['selectStatus']) && $agrProdArr['तरकारी बाली काँक्रो, लौका,फर्सी']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        काँक्रो, लौका,फर्सी                                              
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बाली काँक्रो, लौका,फर्सी][production]", isset($agrProdArr['तरकारी बाली काँक्रो, लौका,फर्सी']['production']) ? $agrProdArr['तरकारी बाली काँक्रो, लौका,फर्सी']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बाली काँक्रो, लौका,फर्सी][area]", isset($agrProdArr['तरकारी बाली काँक्रो, लौका,फर्सी']['area']) ? $agrProdArr['तरकारी बाली काँक्रो, लौका,फर्सी']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बाली काँक्रो, लौका,फर्सी][sale]", isset($agrProdArr['तरकारी बाली काँक्रो, लौका,फर्सी']['sale']) ? $agrProdArr['तरकारी बाली काँक्रो, लौका,फर्सी']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[तरकारी बालीच्याउ][selectStatus]" value="1" {{ isset($agrProdArr['तरकारी बालीच्याउ']['selectStatus']) && $agrProdArr['तरकारी बालीच्याउ']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        च्याउ 
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीच्याउ][production]", isset($agrProdArr['तरकारी बालीच्याउ']['production']) ? $agrProdArr['तरकारी बालीच्याउ']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीच्याउ][area]", isset($agrProdArr['तरकारी बालीच्याउ']['area']) ? $agrProdArr['तरकारी बालीच्याउ']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीच्याउ][sale]", isset($agrProdArr['तरकारी बालीच्याउ']['sale']) ? $agrProdArr['तरकारी बालीच्याउ']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[तरकारी बालीकरेला, घिरौला, चिचिण्डो][selectStatus]" value="1" {{ isset($agrProdArr['तरकारी बालीकरेला, घिरौला, चिचिण्डो']['selectStatus']) && $agrProdArr['तरकारी बालीकरेला, घिरौला, चिचिण्डो']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        करेला, घिरौला, चिचिण्डो 
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीकरेला, घिरौला, चिचिण्डो][production]", isset($agrProdArr['तरकारी बालीकरेला, घिरौला, चिचिण्डो']['production']) ? $agrProdArr['तरकारी बालीकरेला, घिरौला, चिचिण्डो']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीकरेला, घिरौला, चिचिण्डो][area]", isset($agrProdArr['तरकारी बालीकरेला, घिरौला, चिचिण्डो']['area']) ? $agrProdArr['तरकारी बालीकरेला, घिरौला, चिचिण्डो']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीकरेला, घिरौला, चिचिण्डो][sale]", isset($agrProdArr['तरकारी बालीकरेला, घिरौला, चिचिण्डो']['sale']) ? $agrProdArr['तरकारी बालीकरेला, घिरौला, चिचिण्डो']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[तरकारी बालीअन्य][selectStatus]" value="1" {{ isset($agrProdArr['तरकारी बालीअन्य']['selectStatus']) && $agrProdArr['तरकारी बालीअन्य']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        अन्य 
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीअन्य][production]", isset($agrProdArr['तरकारी बालीअन्य']['production']) ? $agrProdArr['तरकारी बालीअन्य']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीअन्य][area]", isset($agrProdArr['तरकारी बालीअन्य']['area']) ? $agrProdArr['तरकारी बालीअन्य']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[तरकारी बालीअन्य][sale]", isset($agrProdArr['तरकारी बालीअन्य']['sale']) ? $agrProdArr['तरकारी बालीअन्य']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<!-- Fourth row  Ends here -->

<!-- Fifth row  starts from here -->
<tr>
  <th>(ङ) मसला बाली</th>
  <th colspan="3"></th>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[मसला बालीअदुवा/वेसार][selectStatus]" value="1" {{ isset($agrProdArr['मसला बालीअदुवा/वेसार']['selectStatus']) && $agrProdArr['मसला बालीअदुवा/वेसार']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        अदुवा/वेसार                    
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीअदुवा/वेसार][production]", isset($agrProdArr['मसला बालीअदुवा/वेसार']['production']) ? $agrProdArr['मसला बालीअदुवा/वेसार']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीअदुवा/वेसार][area]", isset($agrProdArr['मसला बालीअदुवा/वेसार']['area']) ? $agrProdArr['मसला बालीअदुवा/वेसार']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीअदुवा/वेसार][sale]", isset($agrProdArr['मसला बालीअदुवा/वेसार']['sale']) ? $agrProdArr['मसला बालीअदुवा/वेसार']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[मसला बालीप्याज/लसुन][selectStatus]" value="1" {{ isset($agrProdArr['मसला बालीप्याज/लसुन']['selectStatus']) && $agrProdArr['मसला बालीप्याज/लसुन']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        प्याज/लसुन   
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीप्याज/लसुन][production]", isset($agrProdArr['मसला बालीप्याज/लसुन']['production']) ? $agrProdArr['मसला बालीप्याज/लसुन']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीप्याज/लसुन][area]", isset($agrProdArr['मसला बालीप्याज/लसुन']['area']) ? $agrProdArr['मसला बालीप्याज/लसुन']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीप्याज/लसुन][sale]", isset($agrProdArr['मसला बालीप्याज/लसुन']['sale']) ? $agrProdArr['मसला बालीप्याज/लसुन']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[मसला बालीखुर्सानी][selectStatus]" value="1" {{ isset($agrProdArr['मसला बालीखुर्सानी']['selectStatus']) && $agrProdArr['मसला बालीखुर्सानी']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        खुर्सानी                                        
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीखुर्सानी][production]", isset($agrProdArr['मसला बालीखुर्सानी']['production']) ? $agrProdArr['मसला बालीखुर्सानी']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीखुर्सानी][area]", isset($agrProdArr['मसला बालीखुर्सानी']['area']) ? $agrProdArr['मसला बालीखुर्सानी']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीखुर्सानी][sale]", isset($agrProdArr['मसला बालीखुर्सानी']['sale']) ? $agrProdArr['मसला बालीखुर्सानी']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[मसला बालीअन्य][selectStatus]" value="1" {{ isset($agrProdArr['मसला बालीअन्य']['selectStatus']) && $agrProdArr['मसला बालीअन्य']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        अन्य                                                       
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीअन्य][production]", isset($agrProdArr['मसला बालीअन्य']['production']) ? $agrProdArr['मसला बालीअन्य']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीअन्य][area]", isset($agrProdArr['मसला बालीअन्य']['area']) ? $agrProdArr['मसला बालीअन्य']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[मसला बालीअन्य][sale]", isset($agrProdArr['मसला बालीअन्य']['sale']) ? $agrProdArr['मसला बालीअन्य']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>

<!-- Fifth row  Ends here -->

<!-- Sixth row  starts from here -->
<tr>
  <th>(च) फलफूल</th>
  <th colspan="3"></th>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलआँप][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलआँप']['selectStatus']) && $agrProdArr['फलफूलआँप']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        आँप                              
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलआँप][production]", isset($agrProdArr['फलफूलआँप']['production']) ? $agrProdArr['फलफूलआँप']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलआँप][area]", isset($agrProdArr['फलफूलआँप']['area']) ? $agrProdArr['फलफूलआँप']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलआँप][sale]", isset($agrProdArr['फलफूलआँप']['sale']) ? $agrProdArr['फलफूलआँप']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूललिची][selectStatus]" value="1" {{ isset($agrProdArr['फलफूललिची']['selectStatus']) && $agrProdArr['फलफूललिची']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        लिची           
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूललिची][production]", isset($agrProdArr['फलफूललिची']['production']) ? $agrProdArr['फलफूललिची']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूललिची][area]", isset($agrProdArr['फलफूललिची']['area']) ? $agrProdArr['फलफूललिची']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूललिची][sale]", isset($agrProdArr['फलफूललिची']['sale']) ? $agrProdArr['फलफूललिची']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलकेरा][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलकेरा']['selectStatus']) && $agrProdArr['फलफूलकेरा']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        केरा                                                 
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकेरा][production]", isset($agrProdArr['फलफूलकेरा']['production']) ? $agrProdArr['फलफूलकेरा']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकेरा][area]", isset($agrProdArr['फलफूलकेरा']['area']) ? $agrProdArr['फलफूलकेरा']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकेरा][sale]", isset($agrProdArr['फलफूलकेरा']['sale']) ? $agrProdArr['फलफूलकेरा']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलमेवा][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलमेवा']['selectStatus']) && $agrProdArr['फलफूलमेवा']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        मेवा                                                                
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलमेवा][production]", isset($agrProdArr['फलफूलमेवा']['production']) ? $agrProdArr['फलफूलमेवा']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलमेवा][area]", isset($agrProdArr['फलफूलमेवा']['area']) ? $agrProdArr['फलफूलमेवा']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलमेवा][sale]", isset($agrProdArr['फलफूलमेवा']['sale']) ? $agrProdArr['फलफूलमेवा']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलअमिलोेजात][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलअमिलोेजात']['selectStatus']) && $agrProdArr['फलफूलअमिलोेजात']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        सुन्तला/कागती/निवुवा (अमिलोेजात)                                                                 
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअमिलोेजात][production]", isset($agrProdArr['फलफूलअमिलोेजात']['production']) ? $agrProdArr['फलफूलअमिलोेजात']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअमिलोेजात][area]", isset($agrProdArr['फलफूलअमिलोेजात']['area']) ? $agrProdArr['फलफूलअमिलोेजात']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअमिलोेजात][sale]", isset($agrProdArr['फलफूलअमिलोेजात']['sale']) ? $agrProdArr['फलफूलअमिलोेजात']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलनासपति/आरुवखडा][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलनासपति/आरुवखडा']['selectStatus']) && $agrProdArr['फलफूलनासपति/आरुवखडा']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        नासपति/आरुवखडा                                                                   
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलनासपति/आरुवखडा][production]", isset($agrProdArr['फलफूलनासपति/आरुवखडा']['production']) ? $agrProdArr['फलफूलनासपति/आरुवखडा']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलनासपति/आरुवखडा][area]", isset($agrProdArr['फलफूलनासपति/आरुवखडा']['area']) ? $agrProdArr['फलफूलनासपति/आरुवखडा']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलनासपति/आरुवखडा][sale]", isset($agrProdArr['फलफूलनासपति/आरुवखडा']['sale']) ? $agrProdArr['फलफूलनासपति/आरुवखडा']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलरुख कटहर][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलरुख कटहर']['selectStatus']) && $agrProdArr['फलफूलरुख कटहर']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        रुख कटहर                                                                
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलरुख कटहर][production]", isset($agrProdArr['फलफूलरुख कटहर']['production']) ? $agrProdArr['फलफूलरुख कटहर']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलरुख कटहर][area]", isset($agrProdArr['फलफूलरुख कटहर']['area']) ? $agrProdArr['फलफूलरुख कटहर']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलरुख कटहर][sale]", isset($agrProdArr['फलफूलरुख कटहर']['sale']) ? $agrProdArr['फलफूलरुख कटहर']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलभुई कटहर][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलभुई कटहर']['selectStatus']) && $agrProdArr['फलफूलभुई कटहर']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        भुई कटहर                                                                
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलभुई कटहर][production]", isset($agrProdArr['फलफूलभुई कटहर']['production']) ? $agrProdArr['फलफूलभुई कटहर']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलभुई कटहर][area]", isset($agrProdArr['फलफूलभुई कटहर']['area']) ? $agrProdArr['फलफूलभुई कटहर']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलभुई कटहर][sale]", isset($agrProdArr['फलफूलभुई कटहर']['sale']) ? $agrProdArr['फलफूलभुई कटहर']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलअम्बा][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलअम्बा']['selectStatus']) && $agrProdArr['फलफूलअम्बा']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        अम्बा                                                                            
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअम्बा][production]", isset($agrProdArr['फलफूलअम्बा']['production']) ? $agrProdArr['फलफूलअम्बा']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअम्बा][area]", isset($agrProdArr['फलफूलअम्बा']['area']) ? $agrProdArr['फलफूलअम्बा']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअम्बा][sale]", isset($agrProdArr['फलफूलअम्बा']['sale']) ? $agrProdArr['फलफूलअम्बा']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलकिवी][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलकिवी']['selectStatus']) && $agrProdArr['फलफूलकिवी']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        किवी                                                                         
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकिवी][production]", isset($agrProdArr['फलफूलकिवी']['production']) ? $agrProdArr['फलफूलकिवी']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकिवी][area]", isset($agrProdArr['फलफूलकिवी']['area']) ? $agrProdArr['फलफूलकिवी']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकिवी][sale]", isset($agrProdArr['फलफूलकिवी']['sale']) ? $agrProdArr['फलफूलकिवी']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलकफी][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलकफी']['selectStatus']) && $agrProdArr['फलफूलकफी']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        कफी                                                                          
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकफी][production]", isset($agrProdArr['फलफूलकफी']['production']) ? $agrProdArr['फलफूलकफी']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकफी][area]", isset($agrProdArr['फलफूलकफी']['area']) ? $agrProdArr['फलफूलकफी']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलकफी][sale]", isset($agrProdArr['फलफूलकफी']['sale']) ? $agrProdArr['फलफूलकफी']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलअलैची][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलअलैची']['selectStatus']) && $agrProdArr['फलफूलअलैची']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        अलैची                                                                            
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअलैची][production]", isset($agrProdArr['फलफूलअलैची']['production']) ? $agrProdArr['फलफूलअलैची']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअलैची][area]", isset($agrProdArr['फलफूलअलैची']['area']) ? $agrProdArr['फलफूलअलैची']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलअलैची][sale]", isset($agrProdArr['फलफूलअलैची']['sale']) ? $agrProdArr['फलफूलअलैची']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>
<tr>
    <td>
        <input type="checkbox" name="hsAgrProductionDetails[फलफूलउखु][selectStatus]" value="1" {{ isset($agrProdArr['फलफूलउखु']['selectStatus']) && $agrProdArr['फलफूलउखु']['selectStatus'] == '1' ? 'checked' : '' }}> &nbsp;
        उखु                                                                                      
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलउखु][production]", isset($agrProdArr['फलफूलउखु']['production']) ? $agrProdArr['फलफूलउखु']['production'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलउखु][area]", isset($agrProdArr['फलफूलउखु']['area']) ? $agrProdArr['फलफूलउखु']['area'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
    <td>
        {{ Form::text("hsAgrProductionDetails[फलफूलउखु][sale]", isset($agrProdArr['फलफूलउखु']['sale']) ? $agrProdArr['फलफूलउखु']['sale'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
    </td>
</tr>

<!-- Sixth row  Ends here -->
</table>
</div>
</div>
</div>
<div class="msgDisplay"></div>
</div>
</div>