<?php

namespace App\Http\Controllers\admin\setting\houseSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseSetting\RoomUseFor;
use App\model\mylogic;
use App\Http\Requests\admin\setting\RoomUseFor\RoomUseForValidation;
use Validator;


class roomUseForController extends Controller
{
  protected $viewPath  = 'admin.setting.roomUseFor';
  protected $roomUseFor = 'room_use_fors';
  protected $prefix;

  public function __construct()
  {
   $this->middleware('auth');
   $this->prefix = config('activityMessage.roomUseForPrefix');
  }

   public function index()
   {
    $page['page_title']       = 'Room Use For';
   $page['page_description'] = 'list of all Room Use For';

   $allRoomUseFor = RoomUseFor::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
   $prefix   = myLogic::sysRefCode($tablename = $this->roomUseFor, $prefix = $this->prefix);
   return view($this->viewPath . '.index', compact(['page', 'allRoomUseFor', 'prefix']));
   }

   public function store(RoomUseForValidation $request)
   {
          $mySave = RoomUseFor::create([
            'refCode' => $request->refCode,
            'nameNep' => $request->nameNep ?? 'N/A',
            'nameEng' => $request->nameEng,
            'status'  => 0,
            'softDelete' => 0
            ]);
          if ($mySave) {
           return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
       }else{
           return back()->withMessage(config('activityMessage.unSaveMessage'));
       }
   }


   public function edit($id)
   {
        $data = RoomUseFor::findOrFail($id);
        if ($data) {
           return response()->json([
             'success' => true,
             'refCode' => $data->refCode,
             'id'      => $data->id,
             'nameNep' => $data->nameNep,
             'nameEng' => $data->nameEng
             ]);
       }else{
           return response()->json([
             'success' => false,
             'message' => config('activityMessage.dataNotFound')
             ]);
       }
   }


   public function update(Request $request, $id)
   {
        $validator = Validator::make($request->all(), [
          'nameEng'  => 'required|min:4|max:50',
          'nameNep'  => 'min:4|max:50',
          ]);

      if ($validator->fails()) {
         return response()->json([
            'success' => false,
            'message' => $validator->errors()->all()
            ]);
     }
     $update          = RoomUseFor::findOrFail($id);
     $update->nameNep = $request->nameNep ?? 'N/A';
     $update->nameEng = $request->nameEng;

     $myUpdate = $update->update();
     if ($myUpdate) {
       return response()->json([
         'success' => true,
         'message' => config('activityMessage.updateMessage')
         ]);
   }else{
       return response()->json([
         'success'  => false,
         'message'  => config('activityMessage.notUpdated')
         ]);
   }
   }

   public function destroy($id)
   {
        $del = RoomUseFor::findOrFail($id);
        $del->softDelete = 1;
        $myDel = $del->update();
        if ($myDel) {
            return MyLogic::successChangeStatus($code = 200);
        }else{
           return MyLogic::successChangeStatus($code = 301);
        }

   }
}