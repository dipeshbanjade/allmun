<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditwasteManagement']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 50
                }
            },
            messages: {
                nameEng : "The name of Waste Management is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmwasteManagement']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 50
                }
            },
            messages: {
          
                nameEng : "The name of Waste Management is required"
            }
        });
	var btnAddWasteManagement = $('.btnAddWasteManagement');
	btnAddWasteManagement.on('click', function(e){
		$('#wasteManagementModel').modal('show'); 
	});

	var btnEditwasteManagement = $('.btnEditwasteManagement');
	btnEditwasteManagement.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
                console.log(response);
				$('.wasteManagementId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
				$('.txttag').val(response.tag).change();
				$("#editwasteManagementModel").modal('show');
			}
		})
	});

	var frmUpdatewasteManagement = $('#frmUpdatewasteManagement');
  frmUpdatewasteManagement.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.wasteManagementId').val();
    var url  = "{{URL::to('/')}}" + "/setting/wasteManagement/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editwasteManagementModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editwasteManagementModel').modal('hide');
});
});
</script>