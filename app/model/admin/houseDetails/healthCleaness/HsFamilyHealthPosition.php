<?php

namespace App\model\admin\houseDetails\healthCleaness;

use Illuminate\Database\Eloquent\Model;

class HsFamilyHealthPosition extends Model
{
	protected $table ='hs_family_health_positions';
}
