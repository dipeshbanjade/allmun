<?php

namespace App\Http\Requests\admin\citizenInfo;

use Illuminate\Foundation\Http\FormRequest;

class citizenInfoVal extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'gender'                    => 'required',
            'fnameEng'                  => 'required|min:3|max:30',
            'lnameEng'                  => 'required|min:3|max:30'
        ];
    }
}
