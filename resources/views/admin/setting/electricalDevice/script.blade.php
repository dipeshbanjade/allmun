<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditelectricalDevice']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of electrical device is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmelectricalDevice']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of electrical device is required"
            }
        });
	var btnAddelectricalDevice = $('.btnAddelectricalDevice');
	btnAddelectricalDevice.on('click', function(e){
		$('#electricalDeviceModel').modal('show'); 
	});

	var btnEditelectricalDevice = $('.btnEditelectricalDevice');
	btnEditelectricalDevice.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.electricalDeviceId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editelectricalDeviceModel").modal('show');
			}
		})
	});

	var frmUpdateelectricalDevice = $('#frmUpdateelectricalDevice');
  frmUpdateelectricalDevice.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.electricalDeviceId').val();
    var url  = "{{URL::to('/')}}" + "/setting/electricalDevice/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editelectricalDeviceModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editelectricalDeviceModel').modal('hide');
});
});
</script>