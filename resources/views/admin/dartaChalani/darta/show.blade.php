@extends('main.app')
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
               <section class="landing" id="printMe">
                <header>
                <h3>
                <span>
                <i class="fa fa-eye">&nbsp;@lang('commonField.dartaChalani.darta')</i>
                </span>: 
                {{ $data->dartaNo }}</h3>
                </header><br>

                <i class="fa fa-user"></i>
                <?php     
                  $dartafor =getDeginationNameById($data->dartaIsFor);
                  if ($dartafor) {
                  $rPerson    = $lang == 'np' ? $dartafor->nameNep : $dartafor->nameEng;
                  }
                 ?>
                <span>
                
                {{ isset($rPerson) && !empty($rPerson) ? $rPerson : '' }}</span><br>
                <i class="fa fa-home"></i>
                <span title="{{ $data->otherOffices->phoneNumber }}">
                  @if(isset($data->other_offices_id))
                     {{ $lang == 'np' ? $data->otherOffices->nameNep : $data->otherOffices->nameEng }} 
                  @endif
                </span>
                <span>
                  <i class="fa fa-file"></i>
                  {{ $data->dartaPage }}
                </span>
                <span>
                  <i class="fa fa-calendar"></i>
                  {{ $data->dartaPageDate }}
                </span>
            </div>
                <div class="container">
                <dl class="row card-header">
                  <dd class="col-sm-12 text-center">
                      
                </dl><hr>
                <!--  -->
                <dl class="row card-header">
                  <dd class="col-sm-12 text-center">
                      <span>@lang('commonField.dartaChalani.subject'): 
                      &nbsp;&nbsp;{{ $data->dartaSubject }}</span>
                      <span><br>
                      <span>
                        {{ $data->dartaKaifiyat }}
                      </span>
                      <p class="pull-right">
                        <i class="fa fa-calendar"></i>
                        {{ $data->dartaReceiveDate }}
                      </p>
                  </dd>
                </dl><hr>
              </section>
                <!--  -->
                  <dl class="row">
                    <dt class="col-sm-3">@lang('commonField.personal_information.image')</dt>
                    <dd class="col-sm-9">
                      @if($data->imagePath)
                           <?php   
                              $jsonDecode = json_decode($data->imagePath);
                           ?>
                           @if($jsonDecode)
                              @foreach($jsonDecode as $img)
                                 <img src="{{ asset($img) }}" width="600" height="500"> <br>
                              @endforeach
                           @endif
                      @endif
                    </dd>
                  </dl>
                  <dl class="row">
                    <dd class="col-sm-12 text-right">
                    <span> 
                      <a href="{{ route('dartaEdit', $data->idEncript) }}"  title="View & Edit darta details" class="btn btn-info">
                        <i class="fa fa-fw fa-lg fa-pencil-square-o" data-toggle="modal" data-target="#updateUNit" data-id=""></i>
                    </a>
                    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> 
                        <span>@lang('commonField.button.back')</span> 
                    </button>

                     <button class="btn btn-info" onclick="printDiv('printMe')">
                      <i class="fa fa-print"></i>
                    </button>
                    </span>
                    </dd>
                  </dl>


                </div>
            </div>
        </div>
</div>
@endsection