<script type="text/javascript">
	var btnAddTableRow = $('#addTableRow');
	var clickCount = 1;
	btnAddTableRow.on('click', function(e){
		e.preventDefault();

		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var poltNo = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="plotNo[]"> ';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+poltNo+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#house_kayem_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	var btnAddBodartha = $('#add_bodartha');
	btnAddBodartha.on('click', function(e){
		e.preventDefault();		
		var bodarthaInput = '<input type="text" class="form-control" name="bodarthaCopy[]">';
		var cancelButton = '<a href="" class="btn btn-danger removeBodartha"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+bodarthaInput+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#bodartha').append(insertRow);

		$('.removeBodartha').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});
	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmWardBijuliJadanShifaris']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 15
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required: true,
					minlength: 1,
					maxlength:10,
					number: true
				},
				accountHolderName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				accountHolderAddress : {
					required : true,
					minlength: 5,
					maxlength: 255
				},
				municipalityName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				sabikAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				ownerName : {
					required : true,
					minlength: 3,
					maxlength : 255
				},
				kittaNo : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				buildDate : {
					required : true,
					date : true
				},
				authorizedPerson : {
					required : true,
					minlength: 3,
					maxlength: 20
				},
				ampere : {
					required : true,
					minlength: 3,
					maxlength : 255
				},
				category : {
					required : true,
					minlength: 3,
					maxlength : 255
				},
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Num must be numeric",
				accountHolderName : "Applicant name is required",
				accountHolderAddress : "Applicant address is required",
				municipalityName : "Municipality name is required",
				wardNumber : "Ward Number should be numeric",
				sabikAddress : "Sabik Address is required",
				ownerName : "Owner Name is required",
				kittaNo : "Kitta Number is required",
				buildDate : "Build Date is required",
				ampere : "Ampere value is required",
				category : "Category is required",
				authorizedPerson : "Authorizing person is required"
			}
		});
	});

	
	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='ownerName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>