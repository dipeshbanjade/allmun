<script type="text/javascript">
$(function(){
    $("form[name='createMunFrm']").validate({
        rules:{
            uniqueCode: {
                required: true
            },
            nameNep : {
                minlength : 3,
                maxlength : 30
            },
            nameEng : {
                required : true,
                minlength : 3,
                maxlength : 30
            },
            landLineNumber : {
                required : true,
                minlength : 7,
                maxlength: 9,
                number : true
            },
            provinces_id : {
               required  : true
            },
            addrEng : {
                required :true,
                minlength : 3,
                maxlength :30
            },
            addrNep : {
                minlength : 3,
                maxlength : 30
            },
            districts_id : {
                required : true
            },
            faxNumber : {
                maxlength : 9,
                number : true
            },
            // munLogo: {
            //     extension: "jpg|png|jpeg"
            // }
        },
        messages: {
            uniqueCode: "Unique Code is required",
            nameNep : "The name of municipality is required",
            nameEng : "The name of municipality is required",
            landLineNumber : "LandLineNumber is required (Length: 7-9)",
            addrNep : "The address in nepali is required",
            addrEng : "The address is required ",
            provinces_id : 'Please select provision first',
            districts_id  : "Select district name first",
            faxNumber     : "Fax number must be 9 digit ",
            munLogo : "Image file supporting: Jpg, Jpeg, Png",
        }
    });
});

/*form toggle*/
function myFunction() {
    $('#dropLabel').toggle(); 
    $('.munCreate').addClass('hide');
}
</script>