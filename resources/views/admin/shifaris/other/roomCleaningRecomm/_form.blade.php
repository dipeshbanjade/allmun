        <div class="row top-part">
          <div class="col-md-6">
           <p align="left">
            <b>@lang('commonField.extra.refCode'):</b>
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
            @endif
          </p>
          <p align="left">
            <b> च. नं.:
              {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNum']) }}
            </b>
          </p>
        </div>
        <div class="col-md-6">
          <p align="right">
            <b class="mt">मिति :
              {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
            </b>
          </p>
        </div>
      </div>
      <div class="row title-left">
        <div class="col-md-12">
          <p>श्री   
            {{ Form::text('organizationOfficer', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'organizationOfficer', 'required' => 'required']) }},
          </p>
          <p>  
            {{ Form::text('officerName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'officerName', 'required' => 'required']) }}।
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
          </p><h4 align="center"><b>विषय: सिफारिस सम्बन्धमा।</b> </h4>
          {{ Form::hidden('letter_subject', 'सिफारिस सम्बन्धमा।', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}

        </div>
      </div>
      <div class="row content-para">
        <div class="col-md-12">
          <p align="left">
            उपरोक्त सम्बन्धमा 
            <b>
              {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }} 
            </b>
            वडा नं.
            <b>
             {{ Form::text('municipalityWard', Auth::user()->wards_id, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'municipalityWard', 'required' => 'required']) }}
           </b>
           (साविक 
           <!--SABIK ADDRESS START -->
           {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'organizationAddress', 'required' => 'required']) }}
           <!-- SABIK ADDRESS END -->
           <b><select onchange="changeSelect(this)" name="organizationType">
            <option value="गा.वि.स." 
            {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
            <option value="नगरपालिका" 
            {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
            <option value="उप महानगरपालिका" 
            {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
            <option value="महानगरपालिका" 
            {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
          </select>
        </b>, वडा नं. 
        {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field star', 'placeholder'=>'    *', 'id' => 'organizationWard', 'required' => 'required']) }}
        )
        बस्ने <b><select onchange="changeSelect(this)" name="title">
          <option value="श्री"
          {{ !empty($data->title) && $data->title == 'श्री' ? 'selected' : '' }}>श्री</option>
          <option value="सुश्री"
          {{ !empty($data->title) && $data->title == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
          <option value="श्रीमती"
          {{ !empty($data->title) && $data->title == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
        </select>
      </b>
      {{ Form::text('nameOfPerson', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'nameOfPerson', 'required' => 'required']) }}
      ले"कोठा खाली गर्न ३५ दिने सूचना जारी गरी पाऊँ" भनी यस वडा कार्यालयमा निवेदन दिनुभएकोमा सो सम्बन्धमा ३५ दिने सूचना जारी गरी आवश्यक कारवाहीको लागि यसै साथ संलग्न गरी सिफारिस गरी पठाइएको व्यहोरा अनुरोध गरिन्छ।
    </p>
  </div>
</div>
<div class="text-right btm-last">
  <p>
   {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }}
 </p>
 <p> 
  <b>
    <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
      @if(count($deginationsId) > 0)
      @foreach($deginationsId as $deg)
      <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
        {{ $deg->nameNep }} 
      </option>
      @endforeach
      @endif
    </select>
  </b></p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>