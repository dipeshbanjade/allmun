@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
      <div class="card-body ">
          <div class="card-head">
              <header>
              <h3><span>
              <i class="fa fa-eye">&nbsp; @lang('commonField.leaveIsFor.leaveList')</i></span></h3></header>
          </div>

          <button id="btnAddLeaveFor" class="chalaniFrm btn btn-primary pull-right">
              <i class="fa fa-plus"></i>
              <span>
                  @lang('commonField.button.create')
              </span>
          </button>
          <div class="clearfix"></div>
           <div class="col-sm-12">
               {{ Form::open(['route' => 'LeaveForStore', 'name' => 'LeaveFor', 'files' => true]) }}
                   @include('admin.LeaveIsFor._form')                    
                   {{ Form::submit( __('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
                {{ Form::close() }}
           </div>
           <div class="clearfix"></div>
          <div class="table-scrollable" id="dropLabel">
            <table id="example4" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example4_info">
                  <thead>
                        <tr role="row">
                          <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">
                            @lang('commonField.extra.sn')
                          </th>
                          
                          <th class="sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">
                            @lang('commonField.leaveIsFor.dateFrom')
                          </th>

                          <th class="sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">
                          @lang('commonField.leaveIsFor.leaveTitle')
                          </th> 
                        <th style="width: 250px;">@lang('commonField.extra.action')</th>
                      </tr>
                </thead>
                 <tbody>
                     @if($leave)
                        <?php
                            $count = 1;
                        ?>
                        @foreach($leave as $myLeave)
                            <tr>
                                <td>{{ $count ++ }}</td>
                                <td>{{ $myLeave->dateFromNep }} &nbsp; {{ $myLeave->dateFromEng }} </td>
                                <td>{{ $myLeave->leaveFor }} </td>

                                <td>
                                    <span> 
                                      <a href="#"  title="Edit">
                                        <i class="fa fa-fw fa-lg fa-pencil-square-o btnUpdatemyLeave" data-toggle="modal" data-target="#updateUNit" data-id="{{ $myLeave->id }}" data-url="{!! route('LeaveForEdit', $myLeave->id) !!}"></i>
                                    </a>
                                    </span>

                                    <span>
                                    <a href="#" class="deleteMe" data-url="{{ route('LeaveForDelete', $myLeave->id) }}" data-name="{{ $myLeave->leaveFor }}">
                                     <button class="btn btn-danger btn-xs">
                                       <i class="fa fa-trash-o "></i>
                                     </button>
                                     </a>
                                </td>
                            </tr>
                        @endforeach
                     @endif
                 </tbody>
              </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom_script')
  <script type="text/javascript">
       $('.btnUpdatemyLeave').on('click', function(e){
            e.preventDefault();
           var url = $(this).data('url');
           var btnEdDep = $('.btnEditDep');
            var url = $(this).data('url');
            $.ajax({
              'type': 'GET',
              'url': url,
              success: function (response) {
                console.log(response);
                $('.hiddenId').val(response.id),
                $('#myDate').val(response.dateFromNep);
                $('#myEngDate').val(response.dateFromEng);
                $("#leaveTitle").val(response.leaveFor);
              }
            });
       });
  </script>
@endsection