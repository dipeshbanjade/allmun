<script type="text/javascript">
var btnAddBodartha = $('#add_more_property');
var clickCount = 1;
var currencyRate = $('#per_dollar_price_npr');

btnAddBodartha.on('click', function(e){
	e.preventDefault();		
	clickCount++;
	var no = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="" value="'+clickCount+'">';
	var property = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="property[]">';
	var owner = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="owner[]">';
	var location = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="location[]">';
	var plotNo = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="plotNo[]">';
	var area = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="area[]">';
	var totalValue = '<input type="text" class="dashed-input-field total_value" placeholder="   *" required="required" id="totalValue" name="totalValue[]">';

	var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

	var insertRow = "<tr class='tblRow'>"
	+"<td>"+no+"</td>"
	+"<td>"+property+"</td>"
	+"<td>"+owner+"</td>"
	+"<td>"+location+"</td>"
	+"<td>"+plotNo+"</td>"
	+"<td>"+area+"</td>"
	+"<td>"+totalValue+"</td>"
	+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
	+"</tr>";

	$('#property_table').append(insertRow);

	$('.btnRemoveItem').on('click', function(e){
		$(this).parent().parent().remove();
		calcTotalAmount('.total_value');
		return false;
	});
	addListener();

});

$('#totalValue').on('keyup', function(){
	calcTotalAmount('.total_value');
});

currencyRate.on('keyup', function(e){
	calcTotalAmount('.annual_income');
});


function addListener(){
	var annualIncomeObj = document.getElementsByClassName('total_value');

	var rowNumber = annualIncomeObj.length - 1;

	annualIncomeObj[rowNumber].addEventListener('keyup', function(){
		calcTotalAmount('.total_value');
	});
}

function calcTotalAmount(className){
	currencyRate = $('#per_dollar_price_npr');
	totalAnnualIncome = 0;
	var inputField = document.querySelectorAll(className);
	var inputFieldLength = inputField.length;

	for (var i = 0; i < inputFieldLength; i++) {
		if(inputField[i].value == '' || inputField[i].value == null){
			continue;
		}
		totalAnnualIncome += parseInt(inputField[i].value);
	}

	var totalNrsFieldToBeUpdate;
	var totalForeignFieldToBeUpdate;

	totalFieldToBeUpdate = '#total_property_value_nrs';
	totalForeignFieldToBeUpdate = '#total_property_value_dollar';

	addtotalAmmount(totalAnnualIncome, totalFieldToBeUpdate, totalForeignFieldToBeUpdate);
}

function addtotalAmmount(totalAnnualIncome, totalFieldToBeUpdate, totalForeignFieldToBeUpdate){

	$(totalFieldToBeUpdate).val(totalAnnualIncome);

	var foreignCurrencyRate = currencyRate.val();
	$(totalForeignFieldToBeUpdate).val(amountInForeignCurrency(totalAnnualIncome, foreignCurrencyRate));
}
function amountInForeignCurrency(totalAmt, foreignCurrencyRate){
	return totalAmt/foreignCurrencyRate;
}


$(function(){
	/*add buttoin*/
	$("form[name='frmWardpropertyVal']").validate({
		rules:{

			refCode : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			issuedDate : {
				required : true,
				date:true
			},
			title : {
				required : true
			},
			personName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipality : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			ward : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			district : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			former : {
				required : true
			},
			orgAddr : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			orgType : {
				required : true
			},
			orgWard : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			propertyStatement : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			totalPropertyValueInNrs : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			perDollarPriceNpr : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			ctype : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			currencyType : {
				required : true
			},
			totalPropertyValueInDollar : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {

			refCode : "Ref code  is required",
			issuedDate : "Issue Date is required",
			title : "Title is required",
			personName : "Person name is required",
			municipality : "Municipality is required",
			ward : "Ward is required",
			district : "District is required",
			former : "Former is required",
			orgAddr : "Org Address is required",
			orgType : "Org Type  is required",
			orgWard : "Org Ward is required",
			propertyStatement : "Property Statement is required",
			totalPropertyValueInNrs : "Total propertyValuation is required",
			perDollarPriceNpr : "Per Doller price nrp is required",
			ctype : "Ctype is required",
			currencyType : "Currency type is required",
			totalPropertyValueInDollar : "Total Property value in dollor is required",
			authorizedPerson : " Authorized person is required",

		}
	});
});

$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                  $("input[name='personName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
            
          },complete:function(){
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>