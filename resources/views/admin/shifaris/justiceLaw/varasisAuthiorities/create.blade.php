@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Varasis Authiorities Recommendation<small>वारेस अख्तियारिनामा सिफारिस
		</small></header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>

	<div class="x_panel">
		<div class="x_title">
			<h3 class="title-x">वारेस अख्तियारिनामा सिफारिस</h3>
			<div class="clearfix"></div>
		</div>					
		<div class="x_content" id="block">
			@include('admin.shifaris.municipalityDetail')
			{{ Form::open(['route' => 'admin.varasisAuthiorities.store', 'name' => 'frmWardvarasisAuthiorities', 'class'=>'sifarishSubmitForm']) }}
			@include('admin.shifaris.justiceLaw.varasisAuthiorities._form')
		</div>
	</div>
	@include('admin.shifaris.commonButton')

	{{ Form::close() }}
	<div class="msgDisplay" style="z-index: 999999 !important"></div>

</div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
@include('admin.shifaris.script')
@include('admin.shifaris.justiceLaw.varasisAuthiorities.script')
@endsection