<?php

namespace App\Http\Controllers\admin\shifaris\justiceLaw;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\VarasisAuthiorities;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\varasisAuthioritiesVal;
use DB;
use Auth;

use App\model\mylogic;

class VarasisAuthioritiesController extends Controller
{
    protected $viewPath = 'admin.shifaris.justiceLaw.varasisAuthiorities';

    protected $shifarisTableName     = 'varasis_authiorities';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;
    protected $refCode;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixVarasisAuthiorities');
        $this->refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

    }

    public function index()
    {
        $page['page_title']       = 'Varasis Authorities Recommendation';
        $page['page_description'] = 'Varasis Authorities Recommendation';

        $instRecomd = VarasisAuthiorities::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
    }

    public function create()
    {
        $page['page_title']       = 'Varasis Authorities Recommendation : create';
        $page['page_description'] = 'Varasis Authorities Recommendation details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);
        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));


    }

    public function store(varasisAuthioritiesVal $request)
    {

     DB::beginTransaction();
     try {
            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $this->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


        $isShifarisExist = isset($request->shifarisId) ? VarasisAuthiorities::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? VarasisAuthiorities::findOrfail($isShifarisExist['id']) : new VarasisAuthiorities;

        $save->createdBy = Auth::user()->id;

        $save->refCode = $this->refCode;

        $save->wards_id = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";

        $save->issuedDate = $request->issuedDate;
        $save->chalaniNum = $request->chalaniNum;
        $save->successorAge  = $request->successorAge;
        $save->successorName  = $request->successorName;            
        $save->successorMunicipality = $request->successorMunicipality;
        $save->successorWard = $request->successorWard;
        $save->casePerson = $request->casePerson;
        $save->caseName = $request->caseName;
        $save->caseMunicipality = $request->caseMunicipality;
        $save->casereason = $request->casereason;
        $save->caseRunMunicipality = $request->caseRunMunicipality;
        $save->agreedOn = $request->agreedOn;
        $save->manisMunicipality = $request->manisMunicipality;
        $save->manisWard = $request->manisWard;
        $save->manisAge = $request->manisAge;            
        $save->manisName = $request->manisName;
        $save->caseOnMunicipality  = $request->caseOnMunicipality;
        $save->rohabar  = $request->rohabar;
        $save->transfer  = $request->transfer;
        $save->year  = $request->year;
        $save->month  = $request->month;
        $save->day  = $request->day;
        $save->eachday  = $request->eachday;
        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
$request->chalaniNum = $request->chalaniNumber;
        $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
        if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }

  
}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
