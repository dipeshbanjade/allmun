<?php
Route::get('/','HomeController@getIndexPage')->name('/');
Auth::routes();
// Route::get('/biomatric', 'HomeController@biomatric')->name('biomatric');

Route::get('/myMigrate', function(){
    Artisan::call('migrate');
});

Route::group(['middleware' => 'authorizedRoute'], function () {
  Route::prefix('setting')->group(function(){
   /*fiscal year*/
   Route::get('fiscalYear',             'admin\setting\fiscalYearController@index')->name('admin.fiscalYear.index');
   Route::post('fiscalYear/store',      'admin\setting\fiscalYearController@store')->name('admin.fiscalYear.store');
   Route::get('fiscalYear/{id}/edit',   'admin\setting\fiscalYearController@edit')->name('admin.fiscalYear.edit');
   Route::post('fiscalYear/{id}/update', 'admin\setting\fiscalYearController@update')->name('admin.fiscalYear.update');
   Route::get('fiscalYearChangeStatus', 'admin\setting\fiscalYearController@changeStatus')->name('admin.fiscalYear.changeStatus');
   /*endfiscal year*/
   /*disability conditation*/
    Route::get('disable',             'admin\setting\disabilityController@index')->name('disable.index');

    Route::post('disable/store',      'admin\setting\disabilityController@store')->name('disable.store');

    Route::get('disable/{id}/edit',   'admin\setting\disabilityController@edit')->name('disable.edit');

    Route::post('disable/{id}/update', 'admin\setting\disabilityController@update')->name('disable.update');

    Route::get('disable/{id}',        'admin\setting\disabilityController@destroy')->name('disable.delete');

    Route::get('disableChangeStatus', 'admin\setting\disabilityController@changeStatus')->name('disable.changeStatus');
   /*disablity conditation*/

    Route::get('maritial',             'admin\setting\maritialcontroller@index')->name('maritial.index');

    Route::post('maritial/store',      'admin\setting\maritialcontroller@store')->name('maritial.store');

    Route::get('maritial/{id}/edit',   'admin\setting\maritialcontroller@edit')->name('maritial.edit');

    Route::post('maritial/{id}/update', 'admin\setting\maritialcontroller@update')->name('maritial.update');

    Route::get('maritial/{id}',        'admin\setting\maritialcontroller@destroy')->name('maritial.delete');

    Route::get('maritialChangeStatus', 'admin\setting\maritialcontroller@changeStatus')->name('maritial.changeStatus');
   /*maritial status*/
   
   Route::get('/department',             'admin\setting\departmentController@index')->name('admin.department.index');
   Route::get('/department/create', 'admin\setting\departmentController@create')->name('admin.department.create');
   Route::post('/department/store',      'admin\setting\departmentController@store')->name('admin.department.store');
   Route::get('/department/{id}/edit',   'admin\setting\departmentController@edit')->name('admin.department.edit');
   Route::get('/department/show',        'admin\setting\departmentController@show')->name('admin.department.show');
   Route::post('department/{id}/update', 'admin\setting\departmentController@update')->name('admin.department.update');
   Route::get('/department/{id}',        'admin\setting\departmentController@destroy')->name('admin.department.delete');
   Route::get('departmentChangeStatus', 'admin\setting\departmentController@changeStatus')->name('admin.department.changeStatus');

   /*-------province -----------------*/
   Route::get('/province',             'admin\setting\provincesController@index')->name('admin.province.index');

   Route::post('/province/store',      'admin\setting\provincesController@store')->name('admin.province.store');
   
   Route::get('/province/{id}/edit',   'admin\setting\provincesController@edit')->name('admin.province.edit');

   Route::post('province/{id}/update', 'admin\setting\provincesController@update')->name('admin.province.update');
   
   Route::get('/province/{id}',        'admin\setting\provincesController@destroy')->name('admin.province.delete');

   Route::get('provinceChangeStatus', 'admin\setting\provincesController@changeStatus')->name('admin.province.changeStatus');
   /*-------province -----------------*/
   
   /*-----------degination----------------------------------------------------------------*/
   Route::get('/degination',             'admin\setting\deginationController@index')->name('admin.degination.index');
   Route::post('/degination/store',      'admin\setting\deginationController@store')->name('admin.degination.store');
   Route::get('/degination/{id}/edit',   'admin\setting\deginationController@edit')->name('admin.degination.edit');
   Route::post('degination/{id}/update', 'admin\setting\deginationController@update')->name('admin.degination.update');
   Route::get('/degination/{id}',        'admin\setting\deginationController@destroy')->name('admin.degination.delete');
   Route::get('deginationChangeStatus', 'admin\setting\deginationController@changeStatus')->name('admin.degination.changeStatus');


   /*-------------staff Type---------------------*/
   Route::get('/staffType',             'admin\setting\staffTypeController@index')->name('admin.staffType.index');
   Route::post('/staffType/store',      'admin\setting\staffTypeController@store')->name('admin.staffType.store');
   Route::get('/staffType/{id}/edit',   'admin\setting\staffTypeController@edit')->name('admin.staffType.edit');
   Route::post('staffType/{id}/update', 'admin\setting\staffTypeController@update')->name('admin.staffType.update');
   Route::get('/staffType/{id}',        'admin\setting\staffTypeController@destroy')->name('admin.staffType.delete');
   Route::get('staffTypeChangeStatus', 'admin\setting\staffTypeController@changeStatus')->name('admin.staffType.changeStatus');
   /*---------------------------------------------------*/

   /*leave type*/
   Route::get('/leaveType',             'admin\setting\leaveTypeController@index')->name('admin.leaveType.index');
   Route::post('/leaveType/store',      'admin\setting\leaveTypeController@store')->name('admin.leaveType.store');
   Route::get('/leaveType/{id}/edit',   'admin\setting\leaveTypeController@edit')->name('admin.leaveType.edit');
   Route::post('leaveType/{id}/update', 'admin\setting\leaveTypeController@update')->name('admin.leaveType.update');
   Route::get('/leaveType/{id}',        'admin\setting\leaveTypeController@destroy')->name('admin.leaveType.delete');
   Route::get('leaveTypeChangeStatus', 'admin\setting\leaveTypeController@changeStatus')->name('admin.leaveType.changeStatus');

   /*--------------------vist Type---------------------------*/
   Route::get('visitType',             'admin\setting\visitTypeController@index')->name('admin.visitType.index');
   Route::post('visitType/store',      'admin\setting\visitTypeController@store')->name('admin.visitType.store');
   Route::get('visitType/{id}/edit',   'admin\setting\visitTypeController@edit')->name('admin.visitType.edit');
   Route::post('visitType/{id}/update', 'admin\setting\visitTypeController@update')->name('admin.visitType.update');
   Route::get('/visitType/{id}',        'admin\setting\visitTypeController@destroy')->name('admin.visitType.delete');
   Route::get('visitTypeChangeStatus', 'admin\setting\visitTypeController@changeStatus')->name('admin.visitType.changeStatus');

   /*qualification */
   Route::get('qualification',             'admin\setting\qualificationController@index')->name('qualification.index');
   Route::post('qualification/store',      'admin\setting\qualificationController@store')->name('qualification.store');
   Route::get('qualification/{id}/edit',   'admin\setting\qualificationController@edit')->name('qualification.edit');
   Route::post('qualification/{id}/update', 'admin\setting\qualificationController@update')->name('qualification.update');
   Route::get('/qualification/{id}',        'admin\setting\qualificationController@destroy')->name('qualification.delete');
   Route::get('qualificationChangeStatus', 'admin\setting\qualificationController@changeStatus')->name('qualification.changeStatus');

   /*occcupation*/

   Route::get('occupation',             'admin\setting\occupationController@index')->name('occupation.index');
   Route::post('occupation/store',      'admin\setting\occupationController@store')->name('occupation.store');
   Route::get('occupation/{id}/edit',   'admin\setting\occupationController@edit')->name('occupation.edit');
   Route::post('occupation/{id}/update', 'admin\setting\occupationController@update')->name('occupation.update');
   Route::get('/occupation/{id}',        'admin\setting\occupationController@destroy')->name('occupation.delete');
   Route::get('occupationChangeStatus', 'admin\setting\occupationController@changeStatus')->name('occupation.changeStatus'); 

   /*JatJati*/

   Route::get('jatjati',             'admin\setting\jatJatiController@index')->name('jatjati.index');
   Route::post('jatjati/store',      'admin\setting\jatJatiController@store')->name('jatjati.store');
   Route::get('jatjati/{id}/edit',   'admin\setting\jatJatiController@edit')->name('jatjati.edit');
   Route::post('jatjati/{id}/update', 'admin\setting\jatJatiController@update')->name('jatjati.update');
   Route::get('/jatjati/{id}',        'admin\setting\jatJatiController@destroy')->name('jatjati.delete');
   Route::get('jatjatiChangeStatus', 'admin\setting\jatJatiController@changeStatus')->name('jatjati.changeStatus');

   /*Nationality*/
   Route::get('nationality',             'admin\setting\nationalityController@index')->name('nationality.index');

   Route::post('nationality/store',      'admin\setting\nationalityController@store')->name('nationality.store');
   Route::get('nationality/{id}/edit',   'admin\setting\nationalityController@edit')->name('nationality.edit');
   Route::post('nationality/{id}/update', 'admin\setting\nationalityController@update')->name('nationality.update');
   Route::get('/nationality/{id}',        'admin\setting\nationalityController@destroy')->name('nationality.delete');
   Route::get('nationalityChangeStatus', 'admin\setting\nationalityController@changeStatus')->name('nationality.changeStatus');

   /*house type*/

   Route::get('houseType',             'admin\setting\houseTypeController@index')->name('houseType.index');
   Route::post('houseType/store',      'admin\setting\houseTypeController@store')->name('houseType.store');
   Route::get('houseType/{id}/edit',   'admin\setting\houseTypeController@edit')->name('houseType.edit');
   Route::post('houseType/{id}/update', 'admin\setting\houseTypeController@update')->name('houseType.update');
   Route::get('/houseType/{id}',        'admin\setting\houseTypeController@destroy')->name('houseType.delete');
   Route::get('houseTypeChangeStatus', 'admin\setting\houseTypeController@changeStatus')->name('houseType.changeStatus');

   /*citizen type*/

   Route::get('citizenType',             'admin\setting\citizenTypeController@index')->name('citizenType.index');
   Route::post('citizenType/store',      'admin\setting\citizenTypeController@store')->name('citizenType.store');
   Route::get('citizenType/{id}/edit',   'admin\setting\citizenTypeController@edit')->name('citizenType.edit');
   Route::post('citizenType/{id}/update', 'admin\setting\citizenTypeController@update')->name('citizenType.update');
   Route::get('/citizenType/{id}',        'admin\setting\citizenTypeController@destroy')->name('citizenType.delete');
   Route::get('citizenTypeChangeStatus', 'admin\setting\citizenTypeController@changeStatus')->name('citizenType.changeStatus');

   /*language*/
   Route::get('set-language',             'admin\setting\sysLanguageController@index')->name('set-language.index');
   Route::post('set-language/store',      'admin\setting\sysLanguageController@store')->name('set-language.store');
   Route::get('set-language/{id}/edit',   'admin\setting\sysLanguageController@edit')->name('set-language.edit');
   Route::post('set-language/{id}/update', 'admin\setting\sysLanguageController@update')->name('set-language.update');
   Route::get('/set-language/{id}',        'admin\setting\sysLanguageController@destroy')->name('set-language.delete');
   Route::get('set-languageChangeStatus', 'admin\setting\sysLanguageController@changeStatus')->name('set-language.changeStatus');

   /*religioun*/
   Route::get('religion',             'admin\setting\religionController@index')->name('religion.index');
   Route::post('religion/store',      'admin\setting\religionController@store')->name('religion.store');
   Route::get('religion/{id}/edit',   'admin\setting\religionController@edit')->name('religion.edit');
   Route::post('religion/{id}/update', 'admin\setting\religionController@update')->name('religion.update');
   Route::get('/religion/{id}',        'admin\setting\religionController@destroy')->name('religion.delete');
   Route::get('religionChangeStatus', 'admin\setting\religionController@changeStatus')->name('religion.changeStatus');

   /*Tax Type*/
   Route::get('taxType',             'admin\setting\taxTypeController@index')->name('taxType.index');
   Route::post('taxType/store',      'admin\setting\taxTypeController@store')->name('taxType.store');
   Route::get('taxType/{id}/edit',   'admin\setting\taxTypeController@edit')->name('taxType.edit');
   Route::post('taxType/{id}/update', 'admin\setting\taxTypeController@update')->name('taxType.update');
   Route::get('/taxType/{id}',        'admin\setting\taxTypeController@destroy')->name('taxType.delete');
   Route::get('taxTypeChangeStatus', 'admin\setting\taxTypeController@changeStatus')->name('taxType.changeStatus');

   /*animal*/
   Route::get('animal',             'admin\setting\animalController@index')->name('animal.index');
   Route::post('animal/store',      'admin\setting\animalController@store')->name('animal.store');
   Route::get('animal/{id}/edit',   'admin\setting\animalController@edit')->name('animal.edit');
   Route::post('animal/{id}/update', 'admin\setting\animalController@update')->name('animal.update');
   Route::get('/animal/{id}',        'admin\setting\animalController@destroy')->name('animal.delete');
   Route::get('animalChangeStatus', 'admin\setting\animalController@changeStatus')->name('animal.changeStatus');

   /*floor*/
   Route::get('floor',             'admin\setting\floorController@index')->name('floor.index');
   Route::post('floor/store',      'admin\setting\floorController@store')->name('floor.store');
   Route::get('floor/{id}/edit',   'admin\setting\floorController@edit')->name('floor.edit');
   Route::post('floor/{id}/update', 'admin\setting\floorController@update')->name('floor.update');
   Route::get('/floor/{id}',        'admin\setting\floorController@destroy')->name('floor.delete');
   Route::get('floorChangeStatus', 'admin\setting\floorController@changeStatus')->name('floor.changeStatus');

   /*Family Relation*/
   Route::get('familyRelation',             'admin\setting\familyRelationController@index')->name('familyRelation.index');
   Route::post('familyRelation/store',      'admin\setting\familyRelationController@store')->name('familyRelation.store');
   Route::get('familyRelation/{id}/edit',   'admin\setting\familyRelationController@edit')->name('familyRelation.edit');
   Route::post('familyRelation/{id}/update', 'admin\setting\familyRelationController@update')->name('familyRelation.update');
   Route::get('/familyRelation/{id}',        'admin\setting\familyRelationController@destroy')->name('familyRelation.delete');
   Route::get('familyRelationChangeStatus', 'admin\setting\familyRelationController@changeStatus')->name('familyRelation.changeStatus');
   /* Migration Reason*/
   Route::get('migrationReason',             'admin\setting\migrationReasonController@index')->name('migrationReason.index');
   Route::post('migrationReason/store',      'admin\setting\migrationReasonController@store')->name('migrationReason.store');
   Route::get('migrationReason/{id}/edit',   'admin\setting\migrationReasonController@edit')->name('migrationReason.edit');
   Route::post('migrationReason/{id}/update', 'admin\setting\migrationReasonController@update')->name('migrationReason.update');
   Route::get('/migrationReason/{id}',        'admin\setting\migrationReasonController@destroy')->name('migrationReason.delete');
   Route::get('migrationReasonChangeStatus', 'admin\setting\migrationReasonController@changeStatus')->name('migrationReason.changeStatus');

   /*houseWall type*/
   Route::get('houseWall',             'admin\setting\houseSettings\houseWallController@index')->name('houseWallType.index');
   Route::post('houseWall/store',      'admin\setting\houseSettings\houseWallController@store')->name('houseWallType.store');
   Route::get('houseWall/{id}/edit',   'admin\setting\houseSettings\houseWallController@edit')->name('houseWallType.edit');
   Route::post('houseWall/{id}/update', 'admin\setting\houseSettings\houseWallController@update')->name('houseWallType.update');
   Route::get('/houseWall/{id}',        'admin\setting\houseSettings\houseWallController@destroy')->name('houseWallType.delete');
   Route::get('houseWallChangeStatus', 'admin\setting\houseSettings\houseWallController@changeStatus')->name('houseWallType.changeStatus');

   /*house roof*/
   Route::get('houseRoof',             'admin\setting\houseSettings\houseRoofController@index')->name('houseRoof.index');
   Route::post('houseRoof/store',      'admin\setting\houseSettings\houseRoofController@store')->name('houseRoof.store');
   Route::get('houseRoof/{id}/edit',   'admin\setting\houseSettings\houseRoofController@edit')->name('houseRoof.edit');
   Route::post('houseRoof/{id}/update', 'admin\setting\houseSettings\houseRoofController@update')->name('houseRoof.update');
   Route::get('/houseRoof/{id}',        'admin\setting\houseSettings\houseRoofController@destroy')->name('houseRoof.delete');
   Route::get('houseRoofChangeStatus', 'admin\setting\houseSettings\houseRoofController@changeStatus')->name('houseRoof.changeStatus');

   /*swamipto*/
   Route::get('swamipto',             'admin\setting\houseSettings\swamiptoController@index')->name('swamipto.index');
   Route::post('swamipto/store',      'admin\setting\houseSettings\swamiptoController@store')->name('swamipto.store');
   Route::get('swamipto/{id}/edit',   'admin\setting\houseSettings\swamiptoController@edit')->name('swamipto.edit');
   Route::post('swamipto/{id}/update', 'admin\setting\houseSettings\swamiptoController@update')->name('swamipto.update');
   Route::get('/swamipto/{id}',        'admin\setting\houseSettings\swamiptoController@destroy')->name('swamipto.delete');
   Route::get('swamiptoChangeStatus', 'admin\setting\houseSettings\swamiptoController@changeStatus')->name('swamipto.changeStatus');

   /*drinking Water*/
   Route::get('drinkingWater',             'admin\setting\houseSettings\drinkingWaterController@index')->name('drinkingWater.index');
   Route::post('drinkingWater/store',      'admin\setting\houseSettings\drinkingWaterController@store')->name('drinkingWater.store');
   Route::get('drinkingWater/{id}/edit',   'admin\setting\houseSettings\drinkingWaterController@edit')->name('drinkingWater.edit');
   Route::post('drinkingWater/{id}/update', 'admin\setting\houseSettings\drinkingWaterController@update')->name('drinkingWater.update');
   Route::get('/drinkingWater/{id}',        'admin\setting\houseSettings\drinkingWaterController@destroy')->name('drinkingWater.delete');
   Route::get('drinkingWaterChangeStatus', 'admin\setting\houseSettings\drinkingWaterController@changeStatus')->name('drinkingWater.changeStatus');

   /*Resource*/
   Route::get('resource',             'admin\setting\houseSettings\resourceController@index')->name('resource.index');
   Route::post('resource/store',      'admin\setting\houseSettings\resourceController@store')->name('resource.store');
   Route::get('resource/{id}/edit',   'admin\setting\houseSettings\resourceController@edit')->name('resource.edit');
   Route::post('resource/{id}/update', 'admin\setting\houseSettings\resourceController@update')->name('resource.update');
   Route::get('/resource/{id}',        'admin\setting\houseSettings\resourceController@destroy')->name('resource.delete');
   Route::get('resourceChangeStatus', 'admin\setting\houseSettings\resourceController@changeStatus')->name('resource.changeStatus');

   /*Chulo Type*/
   Route::get('chuloType',             'admin\setting\houseSettings\chuloTypeController@index')->name('chuloType.index');
   Route::post('chuloType/store',      'admin\setting\houseSettings\chuloTypeController@store')->name('chuloType.store');
   Route::get('chuloType/{id}/edit',   'admin\setting\houseSettings\chuloTypeController@edit')->name('chuloType.edit');
   Route::post('chuloType/{id}/update', 'admin\setting\houseSettings\chuloTypeController@update')->name('chuloType.update');
   Route::get('/chuloType/{id}',        'admin\setting\houseSettings\chuloTypeController@destroy')->name('chuloType.delete');
   Route::get('chuloTypeChangeStatus', 'admin\setting\houseSettings\chuloTypeController@changeStatus')->name('chuloType.changeStatus');

   /*toiletType*/
   Route::get('toiletType',             'admin\setting\houseSettings\toiletTypeController@index')->name('toiletType.index');
   Route::post('toiletType/store',      'admin\setting\houseSettings\toiletTypeController@store')->name('toiletType.store');
   Route::get('toiletType/{id}/edit',   'admin\setting\houseSettings\toiletTypeController@edit')->name('toiletType.edit');
   Route::post('toiletType/{id}/update', 'admin\setting\houseSettings\toiletTypeController@update')->name('toiletType.update');
   Route::get('/toiletType/{id}',        'admin\setting\houseSettings\toiletTypeController@destroy')->name('toiletType.delete');
   Route::get('toiletTypeChangeStatus', 'admin\setting\houseSettings\toiletTypeController@changeStatus')->name('toiletType.changeStatus');

   /*phone*/
   Route::get('phone',             'admin\setting\houseSettings\phoneController@index')->name('phone.index');
   Route::post('phone/store',      'admin\setting\houseSettings\phoneController@store')->name('phone.store');
   Route::get('phone/{id}/edit',   'admin\setting\houseSettings\phoneController@edit')->name('phone.edit');
   Route::post('phone/{id}/update', 'admin\setting\houseSettings\phoneController@update')->name('phone.update');
   Route::get('/phone/{id}',        'admin\setting\houseSettings\phoneController@destroy')->name('phone.delete');
   Route::get('phoneChangeStatus', 'admin\setting\houseSettings\phoneController@changeStatus')->name('phone.changeStatus');

   /* Electricity*/
   Route::get('electricity',             'admin\setting\houseSettings\ElectricityController@index')->name('electricity.index');
   Route::post('electricity/store',      'admin\setting\houseSettings\ElectricityController@store')->name('electricity.store');
   Route::get('electricity/{id}/edit',   'admin\setting\houseSettings\ElectricityController@edit')->name('electricity.edit');
   Route::post('electricity/{id}/update', 'admin\setting\houseSettings\ElectricityController@update')->name('electricity.update');
   Route::get('/electricity/{id}',        'admin\setting\houseSettings\ElectricityController@destroy')->name('electricity.delete');
   Route::get('electricityChangeStatus', 'admin\setting\houseSettings\ElectricityController@changeStatus')->name('electricity.changeStatus');

   /* Road */
   Route::get('road',             'admin\setting\houseSettings\roadController@index')->name('road.index');
   Route::post('road/store',      'admin\setting\houseSettings\roadController@store')->name('road.store');
   Route::get('road/{id}/edit',   'admin\setting\houseSettings\roadController@edit')->name('road.edit');
   Route::post('road/{id}/update', 'admin\setting\houseSettings\roadController@update')->name('road.update');
   Route::get('/road/{id}',        'admin\setting\houseSettings\roadController@destroy')->name('road.delete');
   Route::get('roadChangeStatus', 'admin\setting\houseSettings\roadController@changeStatus')->name('road.changeStatus');

   /* vehicle */
   Route::get('vehicle',             'admin\setting\houseSettings\vehicleController@index')->name('vehicle.index');
   Route::post('vehicle/store',      'admin\setting\houseSettings\vehicleController@store')->name('vehicle.store');
   Route::get('vehicle/{id}/edit',   'admin\setting\houseSettings\vehicleController@edit')->name('vehicle.edit');
   Route::post('vehicle/{id}/update', 'admin\setting\houseSettings\vehicleController@update')->name('vehicle.update');
   Route::get('/vehicle/{id}',        'admin\setting\houseSettings\vehicleController@destroy')->name('vehicle.delete');
   Route::get('vehicleChangeStatus', 'admin\setting\houseSettings\vehicleController@changeStatus')->name('vehicle.changeStatus');

   /* fuel */
   Route::get('fuel',             'admin\setting\houseSettings\fuelController@index')->name('fuel.index');
   Route::post('fuel/store',      'admin\setting\houseSettings\fuelController@store')->name('fuel.store');
   Route::get('fuel/{id}/edit',   'admin\setting\houseSettings\fuelController@edit')->name('fuel.edit');
   Route::post('fuel/{id}/update', 'admin\setting\houseSettings\fuelController@update')->name('fuel.update');
   Route::get('/fuel/{id}',        'admin\setting\houseSettings\fuelController@destroy')->name('fuel.delete');
   Route::get('fuelChangeStatus', 'admin\setting\houseSettings\fuelController@changeStatus')->name('fuel.changeStatus');

   /*-------family type----------*/
   Route::get('familyType',             'admin\setting\houseSetting\familyTypeController@index')->name('familyType.index');
   Route::post('familyType/store',      'admin\setting\houseSetting\familyTypeController@store')->name('familyType.store');
   Route::get('familyType/{id}/edit',   'admin\setting\houseSetting\familyTypeController@edit')->name('familyType.edit');
   Route::post('familyType/{id}/update', 'admin\setting\houseSetting\familyTypeController@update')->name('familyType.update');
   Route::get('/familyType/{id}',        'admin\setting\houseSetting\familyTypeController@destroy')->name('familyType.delete');
   Route::get('familyTypeChangeStatus', 'admin\setting\houseSetting\familyTypeController@changeStatus')->name('familyType.changeStatus');

   /* roomFloorType */
   Route::get('roomFloorType',             'admin\setting\houseSettings\roomFloorTypeController@index')->name('roomFloorType.index');
   Route::post('roomFloorType/store',      'admin\setting\houseSettings\roomFloorTypeController@store')->name('roomFloorType.store');
   Route::get('roomFloorType/{id}/edit',   'admin\setting\houseSettings\roomFloorTypeController@edit')->name('roomFloorType.edit');
   Route::post('roomFloorType/{id}/update', 'admin\setting\houseSettings\roomFloorTypeController@update')->name('roomFloorType.update');
   Route::get('/roomFloorType/{id}',        'admin\setting\houseSettings\roomFloorTypeController@destroy')->name('roomFloorType.delete');
   Route::get('roomFloorTypeChangeStatus', 'admin\setting\houseSettings\roomFloorTypeController@changeStatus')->name('roomFloorType.changeStatus');

   /*room use for*/
   Route::get('roomUseFor',             'admin\setting\houseSettings\roomUseForController@index')->name('roomUseFor.index');
   Route::post('roomUseFor/store',      'admin\setting\houseSettings\roomUseForController@store')->name('roomUseFor.store');
   Route::get('roomUseFor/{id}/edit',   'admin\setting\houseSettings\roomUseForController@edit')->name('roomUseFor.edit');
   Route::post('roomUseFor/{id}/update', 'admin\setting\houseSettings\roomUseForController@update')->name('roomUseFor.update');
   Route::get('/roomUseFor/{id}',        'admin\setting\houseSettings\roomUseForController@destroy')->name('roomUseFor.delete');
   Route::get('roomUseForChangeStatus', 'admin\setting\houseSettings\roomUseForController@changeStatus')->name('roomUseFor.changeStatus');

   /* houseFoundationType */
   Route::get('houseFoundation',             'admin\setting\houseSettings\houseFoundationController@index')->name('houseFoundation.index');
   Route::post('houseFoundation/store',      'admin\setting\houseSettings\houseFoundationController@store')->name('houseFoundation.store');
   Route::get('houseFoundation/{id}/edit',   'admin\setting\houseSettings\houseFoundationController@edit')->name('houseFoundation.edit');
   Route::post('houseFoundation/{id}/update', 'admin\setting\houseSettings\houseFoundationController@update')->name('houseFoundation.update');
   Route::get('/houseFoundation/{id}',        'admin\setting\houseSettings\houseFoundationController@destroy')->name('houseFoundation.delete');
   Route::get('houseFoundationChangeStatus', 'admin\setting\houseSettings\houseFoundationController@changeStatus')->name('houseFoundation.changeStatus');

   /* Area type */
   Route::get('areaType',             'admin\setting\houseSettings\areaTypeController@index')->name('areaType.index');
   Route::post('areaType/store',      'admin\setting\houseSettings\areaTypeController@store')->name('areaType.store');
   Route::get('areaType/{id}/edit',   'admin\setting\houseSettings\areaTypeController@edit')->name('areaType.edit');
   Route::post('areaType/{id}/update', 'admin\setting\houseSettings\areaTypeController@update')->name('areaType.update');
   Route::get('/areaType/{id}',        'admin\setting\houseSettings\areaTypeController@destroy')->name('areaType.delete');
   Route::get('areaTypeChangeStatus', 'admin\setting\houseSettings\areaTypeController@changeStatus')->name('areaType.changeStatus');

   /* Electrical Device */
   Route::get('electricalDevice',             'admin\setting\houseSettings\electricalDeviceController@index')->name('electricalDevice.index');
   Route::post('electricalDevice/store',      'admin\setting\houseSettings\electricalDeviceController@store')->name('electricalDevice.store');
   Route::get('electricalDevice/{id}/edit',   'admin\setting\houseSettings\electricalDeviceController@edit')->name('electricalDevice.edit');
   Route::post('electricalDevice/{id}/update', 'admin\setting\houseSettings\electricalDeviceController@update')->name('electricalDevice.update');
   Route::get('/electricalDevice/{id}',        'admin\setting\houseSettings\electricalDeviceController@destroy')->name('electricalDevice.delete');
   Route::get('electricalDeviceChangeStatus', 'admin\setting\houseSettings\electricalDeviceController@changeStatus')->name('electricalDevice.changeStatus');

   /* Electrical Device */
   Route::get('houseUseFor',             'admin\setting\houseSettings\houseUseForController@index')->name('houseUseFor.index');
   Route::post('houseUseFor/store',      'admin\setting\houseSettings\houseUseForController@store')->name('houseUseFor.store');
   Route::get('houseUseFor/{id}/edit',   'admin\setting\houseSettings\houseUseForController@edit')->name('houseUseFor.edit');
   Route::post('houseUseFor/{id}/update', 'admin\setting\houseSettings\houseUseForController@update')->name('houseUseFor.update');
   Route::get('/houseUseFor/{id}',        'admin\setting\houseSettings\houseUseForController@destroy')->name('houseUseFor.delete');
   Route::get('houseUseForChangeStatus', 'admin\setting\houseSettings\houseUseForController@changeStatus')->name('houseUseFor.changeStatus');

   /*Business Type*/
   Route::get('businessType',             'admin\setting\houseSettings\businessTypeController@index')->name('businessType.index');
   Route::post('businessType/store',      'admin\setting\houseSettings\businessTypeController@store')->name('businessType.store');
   Route::get('businessType/{id}/edit',   'admin\setting\houseSettings\businessTypeController@edit')->name('businessType.edit');
   Route::post('businessType/{id}/update', 'admin\setting\houseSettings\businessTypeController@update')->name('businessType.update');
   Route::get('/businessType/{id}',        'admin\setting\houseSettings\businessTypeController@destroy')->name('businessType.delete');
   Route::get('businessTypeChangeStatus', 'admin\setting\houseSettings\businessTypeController@changeStatus')->name('businessType.changeStatus');

   /*Living For*/
   Route::get('livingFor',             'admin\setting\houseSettings\livingForController@index')->name('livingFor.index');
   Route::post('livingFor/store',      'admin\setting\houseSettings\livingForController@store')->name('livingFor.store');
   Route::get('livingFor/{id}/edit',   'admin\setting\houseSettings\livingForController@edit')->name('livingFor.edit');
   Route::post('livingFor/{id}/update', 'admin\setting\houseSettings\livingForController@update')->name('livingFor.update');
   Route::get('/livingFor/{id}',        'admin\setting\houseSettings\livingForController@destroy')->name('livingFor.delete');
   Route::get('livingForChangeStatus', 'admin\setting\houseSettings\livingForController@changeStatus')->name('livingFor.changeStatus');

   /*Waste Management*/
   Route::get('wasteManagement',             'admin\setting\houseSettings\wasteManagementController@index')->name('wasteManagement.index');
   Route::post('wasteManagement/store',      'admin\setting\houseSettings\wasteManagementController@store')->name('wasteManagement.store');
   Route::get('wasteManagement/{id}/edit',   'admin\setting\houseSettings\wasteManagementController@edit')->name('wasteManagement.edit');
   Route::post('wasteManagement/{id}/update', 'admin\setting\houseSettings\wasteManagementController@update')->name('wasteManagement.update');
   Route::get('/wasteManagement/{id}',        'admin\setting\houseSettings\wasteManagementController@destroy')->name('wasteManagement.delete');
   Route::get('wasteManagementChangeStatus', 'admin\setting\houseSettings\wasteManagementController@changeStatus')->name('wasteManagement.changeStatus');

   /*Quality Water*/
   Route::get('qualityWater',             'admin\setting\houseSettings\qualityWaterController@index')->name('qualityWater.index');
   Route::post('qualityWater/store',      'admin\setting\houseSettings\qualityWaterController@store')->name('qualityWater.store');
   Route::get('qualityWater/{id}/edit',   'admin\setting\houseSettings\qualityWaterController@edit')->name('qualityWater.edit');
   Route::post('qualityWater/{id}/update', 'admin\setting\houseSettings\qualityWaterController@update')->name('qualityWater.update');
   Route::get('/qualityWater/{id}',        'admin\setting\houseSettings\qualityWaterController@destroy')->name('qualityWater.delete');
   Route::get('qualityWaterChangeStatus', 'admin\setting\houseSettings\qualityWaterController@changeStatus')->name('qualityWater.changeStatus');

   /*School Class Level*/
   Route::get('schoolClassLvl',             'admin\setting\houseSettings\schoolClassLvlController@index')->name('schoolClassLvl.index');
   Route::post('schoolClassLvl/store',      'admin\setting\houseSettings\schoolClassLvlController@store')->name('schoolClassLvl.store');
   Route::get('schoolClassLvl/{id}/edit',   'admin\setting\houseSettings\schoolClassLvlController@edit')->name('schoolClassLvl.edit');
   Route::post('schoolClassLvl/{id}/update', 'admin\setting\houseSettings\schoolClassLvlController@update')->name('schoolClassLvl.update');
   Route::get('/schoolClassLvl/{id}',        'admin\setting\houseSettings\schoolClassLvlController@destroy')->name('schoolClassLvl.delete');
   Route::get('schoolClassLvlChangeStatus', 'admin\setting\houseSettings\schoolClassLvlController@changeStatus')->name('schoolClassLvl.changeStatus');

   /*Work Type*/
   Route::get('workType',             'admin\setting\houseSettings\workTypeController@index')->name('workType.index');
   Route::post('workType/store',      'admin\setting\houseSettings\workTypeController@store')->name('workType.store');
   Route::get('workType/{id}/edit',   'admin\setting\houseSettings\workTypeController@edit')->name('workType.edit');
   Route::post('workType/{id}/update', 'admin\setting\houseSettings\workTypeController@update')->name('workType.update');
   Route::get('/workType/{id}',        'admin\setting\houseSettings\workTypeController@destroy')->name('workType.delete');
   Route::get('workTypeChangeStatus', 'admin\setting\houseSettings\workTypeController@changeStatus')->name('workType.changeStatus');

   /*Salary Type*/
   Route::get('salaryType',             'admin\setting\houseSettings\salaryTypeController@index')->name('salaryType.index');
   Route::post('salaryType/store',      'admin\setting\houseSettings\salaryTypeController@store')->name('salaryType.store');
   Route::get('salaryType/{id}/edit',   'admin\setting\houseSettings\salaryTypeController@edit')->name('salaryType.edit');
   Route::post('salaryType/{id}/update', 'admin\setting\houseSettings\salaryTypeController@update')->name('salaryType.update');
   Route::get('/salaryType/{id}',        'admin\setting\houseSettings\salaryTypeController@destroy')->name('salaryType.delete');
   Route::get('salaryTypeChangeStatus', 'admin\setting\houseSettings\salaryTypeController@changeStatus')->name('salaryType.changeStatus'); 

   /*Disease*/
   Route::get('disease',             'admin\setting\houseSettings\diseaseController@index')->name('disease.index');
   Route::post('disease/store',      'admin\setting\houseSettings\diseaseController@store')->name('disease.store');
   Route::get('disease/{id}/edit',   'admin\setting\houseSettings\diseaseController@edit')->name('disease.edit');
   Route::post('disease/{id}/update', 'admin\setting\houseSettings\diseaseController@update')->name('disease.update');
   Route::get('/disease/{id}',        'admin\setting\houseSettings\diseaseController@destroy')->name('disease.delete');
   Route::get('diseaseChangeStatus', 'admin\setting\houseSettings\diseaseController@changeStatus')->name('disease.changeStatus');

   /* Ligth Source*/
   Route::get('lightSource',             'admin\setting\houseSettings\lightSourceController@index')->name('lightSource.index');
   Route::post('lightSource/store',      'admin\setting\houseSettings\lightSourceController@store')->name('lightSource.store');
   Route::get('lightSource/{id}/edit',   'admin\setting\houseSettings\lightSourceController@edit')->name('lightSource.edit');
   Route::post('lightSource/{id}/update', 'admin\setting\houseSettings\lightSourceController@update')->name('lightSource.update');
   Route::get('/lightSource/{id}',        'admin\setting\houseSettings\lightSourceController@destroy')->name('lightSource.delete');
   Route::get('lightSourceChangeStatus', 'admin\setting\houseSettings\lightSourceController@changeStatus')->name('lightSource.changeStatus');

   /* Oven Type */
   Route::get('ovenType',             'admin\setting\houseSettings\ovenTypeController@index')->name('ovenType.index');
   Route::post('ovenType/store',      'admin\setting\houseSettings\ovenTypeController@store')->name('ovenType.store');
   Route::get('ovenType/{id}/edit',   'admin\setting\houseSettings\ovenTypeController@edit')->name('ovenType.edit');
   Route::post('ovenType/{id}/update', 'admin\setting\houseSettings\ovenTypeController@update')->name('ovenType.update');
   Route::get('/ovenType/{id}',        'admin\setting\houseSettings\ovenTypeController@destroy')->name('ovenType.delete');
   Route::get('ovenTypeChangeStatus', 'admin\setting\houseSettings\ovenTypeController@changeStatus')->name('ovenType.changeStatus');

   /* HandWash Method */
   Route::get('handWashMethod',             'admin\setting\houseSettings\handWashMethodController@index')->name('handWashMethod.index');
   Route::post('handWashMethod/store',      'admin\setting\houseSettings\handWashMethodController@store')->name('handWashMethod.store');
   Route::get('handWashMethod/{id}/edit',   'admin\setting\houseSettings\handWashMethodController@edit')->name('handWashMethod.edit');
   Route::post('handWashMethod/{id}/update', 'admin\setting\houseSettings\handWashMethodController@update')->name('handWashMethod.update');
   Route::get('/handWashMethod/{id}',        'admin\setting\houseSettings\handWashMethodController@destroy')->name('handWashMethod.delete');
   Route::get('handWashMethodChangeStatus', 'admin\setting\houseSettings\handWashMethodController@changeStatus')->name('handWashMethod.changeStatus');

   /* Education Level */
   Route::get('educationLevel',             'admin\setting\houseSettings\educationLevelController@index')->name('educationLevel.index');
   Route::post('educationLevel/store',      'admin\setting\houseSettings\educationLevelController@store')->name('educationLevel.store');
   Route::get('educationLevel/{id}/edit',   'admin\setting\houseSettings\educationLevelController@edit')->name('educationLevel.edit');
   Route::post('educationLevel/{id}/update', 'admin\setting\houseSettings\educationLevelController@update')->name('educationLevel.update');
   Route::get('/educationLevel/{id}',        'admin\setting\houseSettings\educationLevelController@destroy')->name('educationLevel.delete');
   Route::get('educationLevelChangeStatus', 'admin\setting\houseSettings\educationLevelController@changeStatus')->name('educationLevel.changeStatus');             

   /* Vaccine Type */
   Route::get('vaccineType',             'admin\setting\houseSettings\vaccineTypeController@index')->name('vaccineType.index');
   Route::post('vaccineType/store',      'admin\setting\houseSettings\vaccineTypeController@store')->name('vaccineType.store');
   Route::get('vaccineType/{id}/edit',   'admin\setting\houseSettings\vaccineTypeController@edit')->name('vaccineType.edit');
   Route::post('vaccineType/{id}/update', 'admin\setting\houseSettings\vaccineTypeController@update')->name('vaccineType.update');
   Route::get('/vaccineType/{id}',        'admin\setting\houseSettings\vaccineTypeController@destroy')->name('vaccineType.delete');
   Route::get('vaccineTypeChangeStatus', 'admin\setting\houseSettings\vaccineTypeController@changeStatus')->name('vaccineType.changeStatus');             

   /* Country */
   Route::get('country',             'admin\setting\houseSettings\countryController@index')->name('country.index');
   Route::post('country/store',      'admin\setting\houseSettings\countryController@store')->name('country.store');
   Route::get('country/{id}/edit',   'admin\setting\houseSettings\countryController@edit')->name('country.edit');
   Route::post('country/{id}/update', 'admin\setting\houseSettings\countryController@update')->name('country.update');
   Route::get('/country/{id}',        'admin\setting\houseSettings\countryController@destroy')->name('country.delete');
   Route::get('countryChangeStatus', 'admin\setting\houseSettings\countryController@changeStatus')->name('country.changeStatus');

   /* Age Group */
   Route::get('ageGroup',             'admin\setting\houseSettings\ageGroupController@index')->name('ageGroup.index');
   Route::post('ageGroup/store',      'admin\setting\houseSettings\ageGroupController@store')->name('ageGroup.store');
   Route::get('ageGroup/{id}/edit',   'admin\setting\houseSettings\ageGroupController@edit')->name('ageGroup.edit');
   Route::post('ageGroup/{id}/update', 'admin\setting\houseSettings\ageGroupController@update')->name('ageGroup.update');
   Route::get('/ageGroup/{id}',        'admin\setting\houseSettings\ageGroupController@destroy')->name('ageGroup.delete');
   Route::get('ageGroupChangeStatus', 'admin\setting\houseSettings\ageGroupController@changeStatus')->name('ageGroup.changeStatus');

   /* Filter Process */
   Route::get('filterProcess',             'admin\setting\houseSettings\filterProcessController@index')->name('filterProcess.index');
   Route::post('filterProcess/store',      'admin\setting\houseSettings\filterProcessController@store')->name('filterProcess.store');
   Route::get('filterProcess/{id}/edit',   'admin\setting\houseSettings\filterProcessController@edit')->name('filterProcess.edit');
   Route::post('filterProcess/{id}/update', 'admin\setting\houseSettings\filterProcessController@update')->name('filterProcess.update');
   Route::get('/filterProcess/{id}',        'admin\setting\houseSettings\filterProcessController@destroy')->name('filterProcess.delete');
   Route::get('filterProcessChangeStatus', 'admin\setting\houseSettings\filterProcessController@changeStatus')->name('filterProcess.changeStatus');          

   /* Drinking Water Source */
   Route::get('drinkingWaterSource',             'admin\setting\houseSettings\drinkingWaterSourceController@index')->name('drinkingWaterSource.index');
   Route::post('drinkingWaterSource/store',      'admin\setting\houseSettings\drinkingWaterSourceController@store')->name('drinkingWaterSource.store');
   Route::get('drinkingWaterSource/{id}/edit',   'admin\setting\houseSettings\drinkingWaterSourceController@edit')->name('drinkingWaterSource.edit');
   Route::post('drinkingWaterSource/{id}/update', 'admin\setting\houseSettings\drinkingWaterSourceController@update')->name('drinkingWaterSource.update');
   Route::get('/drinkingWaterSource/{id}',        'admin\setting\houseSettings\drinkingWaterSourceController@destroy')->name('drinkingWaterSource.delete');
   Route::get('drinkingWaterSourceChangeStatus', 'admin\setting\houseSettings\drinkingWaterSourceController@changeStatus')->name('drinkingWaterSource.changeStatus');

   /* Water Availability */
   Route::get('waterAvailability',             'admin\setting\houseSettings\waterAvailabilityController@index')->name('waterAvailability.index');
   Route::post('waterAvailability/store',      'admin\setting\houseSettings\waterAvailabilityController@store')->name('waterAvailability.store');
   Route::get('waterAvailability/{id}/edit',   'admin\setting\houseSettings\waterAvailabilityController@edit')->name('waterAvailability.edit');
   Route::post('waterAvailability/{id}/update', 'admin\setting\houseSettings\waterAvailabilityController@update')->name('waterAvailability.update');
   Route::get('/waterAvailability/{id}',        'admin\setting\houseSettings\waterAvailabilityController@destroy')->name('waterAvailability.delete');
   Route::get('waterAvailabilityChangeStatus', 'admin\setting\houseSettings\waterAvailabilityController@changeStatus')->name('waterAvailability.changeStatus');

   /* Why not */
   Route::get('whyNot',             'admin\setting\houseSettings\whyNotController@index')->name('whyNot.index');
   Route::post('whyNot/store',      'admin\setting\houseSettings\whyNotController@store')->name('whyNot.store');
   Route::get('whyNot/{id}/edit',   'admin\setting\houseSettings\whyNotController@edit')->name('whyNot.edit');
   Route::post('whyNot/{id}/update', 'admin\setting\houseSettings\whyNotController@update')->name('whyNot.update');
   Route::get('/whyNot/{id}',        'admin\setting\houseSettings\whyNotController@destroy')->name('whyNot.delete');
   Route::get('whyNotChangeStatus', 'admin\setting\houseSettings\whyNotController@changeStatus')->name('whyNot.changeStatus');

   /* Treatment Area */
   Route::get('treatmentArea',             'admin\setting\houseSettings\treatmentAreaController@index')->name('treatmentArea.index');
   Route::post('treatmentArea/store',      'admin\setting\houseSettings\treatmentAreaController@store')->name('treatmentArea.store');
   Route::get('treatmentArea/{id}/edit',   'admin\setting\houseSettings\treatmentAreaController@edit')->name('treatmentArea.edit');
   Route::post('treatmentArea/{id}/update', 'admin\setting\houseSettings\treatmentAreaController@update')->name('treatmentArea.update');
   Route::get('/treatmentArea/{id}',        'admin\setting\houseSettings\treatmentAreaController@destroy')->name('treatmentArea.delete');
   Route::get('treatmentAreaChangeStatus', 'admin\setting\houseSettings\treatmentAreaController@changeStatus')->name('treatmentArea.changeStatus');

 });

Route::prefix('dashboard')->group(function(){
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/currentAtten', 'HomeController@getCurrentAttendance')->name('currAttend');

  Route::get('/changeLanguage/{lan}', 'HomeController@switchLanguage')->name('changeLanguage');

  Route::get('/myProfile', 'admin\staff\staffController@getMyProfile')->name('myProfile');
  Route::get('/changePassword', 'admin\staff\staffController@changePassword')->name('changePassword');
  Route::post('/change-password', 'admin\staff\staffController@storePassword')->name('changePassword.save');

    // all shifaris Route

  Route::get('/nibedakDetail/{id}', 'HomeController@getNibedakDetail')->name('nibedakDetail');

    // Route::get('/accessPermission', 'HomeController@accessPermission')->name('systemAccess');
  Route::get('/logout', 'HomeController@adminlogout')->name('system.logout');
  Route::get('/staffChangePassword/{slug}', 'admin\staff\staffController@adminStaffChangePwdView')->name('adminChangePwd');
  Route::post('/storeChangePassword',      'admin\staff\staffController@staffPasswordStore')->name('storeStaffPwd');

  /*----------edit staff--------------*/
  Route::get('/adminGetStaffDetails/{id}',     'admin\staff\staffController@getStaffDetails')->name('adminGetStaffDetails');
  Route::post('/adminUpdateStaffDetails',      'admin\staff\staffController@updateStaffDetails')->name('adminUpdateStaffDetails');

  Route::get('/citizenInfo',             'admin\citizenInfo\citizenInfoController@index')->name('citizenInfo.index');
  Route::get('/citizenInfo/create',      'admin\citizenInfo\citizenInfoController@create')->name('citizenInfo.create');
  Route::get('createParents/{tag}', 'admin\citizenInfo\citizenInfoController@create')->name('createParents');

  Route::post('citizenInfo/store',      'admin\citizenInfo\citizenInfoController@store')->name('citizenInfo.store');
  Route::get('/citizenInfo/{slug}',        'admin\citizenInfo\citizenInfoController@show')->name('citizenInfo.show');
  Route::get('citizenInfo/{slug}/edit',   'admin\citizenInfo\citizenInfoController@edit')->name('citizenInfo.edit');
  Route::post('citizenInfo/{slug}/update', 'admin\citizenInfo\citizenInfoController@update')->name('citizenInfo.update');
  Route::get('/citizenInfoDelete/{slug}',   'admin\citizenInfo\citizenInfoController@destroy')->name('citizenInfo.delete');
  Route::get('citizen-print-card/{slug}', 'admin\citizenInfo\citizenInfoController@printCard')->name('citizenPrintCard');
  Route::get('/getCitizenRelationData', 'admin\citizenInfo\citizenInfoController@getRelationData')->name('getParentDetails');
  Route::post('/insertCitizenFormHouse', 'admin\citizenInfo\citizenInfoController@insertCitizenFromHouse')->name('insertCitizenFromHome');


  Route::post('/insertAbroadCitizen', 'admin\citizenInfo\citizenInfoController@insertAbroadGoneMember')->name('abroadGoneMember');
  /*view log file*/
  Route::get('logFiles', 'admin\system\logFileController@index')->name('viewLogFile');
  Route::get('logFiles/{id}', 'admin\system\logFileController@delete')->name('deleteLogFile');

  /*-----------------daily report-------------------------------------------------------------*/
  Route::get('/dailyReport',             'admin\system\dailyReportController@index')->name('dailyReport.index');

  Route::get('/dailyReport/create',      'admin\system\dailyReportController@create')->name('dailyReport.create');

  Route::post('dailyReport/store',      'admin\system\dailyReportController@store')->name('dailyReport.store');

  Route::get('/dailyReport/{id}',        'admin\system\dailyReportController@show')->name('dailyReport.show');

  Route::get('dailyReport/{id}/edit',   'admin\system\dailyReportController@edit')->name('dailyReport.edit');

  Route::post('dailyReport/{id}/update', 'admin\system\dailyReportController@update')->name('dailyReport.update');

  Route::get('/dailyReportDelete/{id}',   'admin\system\dailyReportController@destroy')->name('dailyReport.delete');
  
  Route::get('/adminViewDailyReport',      'admin\system\dailyReportController@adminView')->name('dailyReport.adminView');

  /*-----------------------------------------------------------*/
  Route::get('/fieldVisit',             'admin\system\fieldVisitController@index')->name('fieldVisit.index');

  Route::get('/fieldVisit/create',      'admin\system\fieldVisitController@create')->name('fieldVisit.create');

  Route::post('fieldVisit/store',      'admin\system\fieldVisitController@store')->name('fieldVisit.store');

  Route::get('/fieldVisit/{id}',        'admin\system\fieldVisitController@show')->name('fieldVisit.show');

  Route::get('fieldVisit/{id}/edit',   'admin\system\fieldVisitController@edit')->name('fieldVisit.edit');

  Route::post('fieldVisit/{id}/update', 'admin\system\fieldVisitController@update')->name('fieldVisit.update');

  Route::get('/fieldVisitDelete/{id}',   'admin\system\fieldVisitController@destroy')->name('fieldVisit.delete');

  Route::get('/adminViewFieldVisit',      'admin\system\fieldVisitController@adminViewFieldVisit')->name('fieldVisit.adminView');

  Route::get('/approveFieldVisit', 'admin\system\fieldVisitController@approveFieldVisit')->name('approveFieldVisit');
  
  /* Company */
   Route::get('company',             'admin\company\companyController@index')->name('company.index');
   Route::post('company/store',      'admin\company\companyController@store')->name('company.store');
   Route::get('company/{id}/edit',   'admin\company\companyController@edit')->name('company.edit');
   Route::post('company/{id}/update', 'admin\company\companyController@update')->name('company.update');
   Route::get('/company/{id}',        'admin\company\companyController@destroy')->name('company.delete');

   Route::get('/staffAttendance',             'admin\attendance\attendanceController@index')->name('admin.munAtten.index');
   Route::post('/getDisplayStaffAtten',       'admin\attendance\attendanceController@getStaffAttendance')->name('displayStaffAttendance');
   Route::get('/downloadAttendance',          'admin\attendance\attendanceController@exportAttendance')->name('downloadAtten');

});

Route::prefix('companyLeave')->group(function(){
   Route::get('/leaveisfor','admin\municipility\LeaveIsForController@index')->name('LeaveForShow');
   Route::get('/leaveisfor/create','admin\municipility\LeaveIsForController@create')->name('LeaveForCreate');
   Route::post('/leaveisfor/store','admin\municipility\LeaveIsForController@store')->name('LeaveForStore');
   Route::get('/leaveisfor/{id}/edit','admin\municipility\LeaveIsForController@edit')->name('LeaveForEdit');
   Route::post('/leaveisfor/{id}/update','admin\municipility\LeaveIsForController@update')->name('LeaveForUpdate');
   Route::get('/deleteLeaveFor/{id}','admin\municipility\LeaveIsForController@destroy')->name('LeaveForDelete');
});

require_once('houseDetail.php');
require_once 'acl.php';
require_once 'municipility.php';  // all municipilities route goes here
require_once 'ward.php';   // all ward route goes here
require_once('shifaris.php');
require_once('dartachalani.php');
 });
