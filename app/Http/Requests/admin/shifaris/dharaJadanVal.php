<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class dharaJadanVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:bijuli_jadan_shifaris',
            'chalaniNumber' => 'required',
            'accountHolderName' => 'required', 
            'accountHolderAddress' => 'required',
            'municipalityName' => 'required', 
            'wardNumber' => 'required|numeric', 
            'sabikAddress' => 'required',
            'ownerName' => 'required',
            'buildDate' =>'required',
            'category' =>'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
