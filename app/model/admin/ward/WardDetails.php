<?php

namespace App\model\admin\ward;

use Illuminate\Database\Eloquent\Model;

class WardDetails extends Model
{
    protected $table = 'wards';
}
