<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class relationIdentityVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:relation_identifies',
            'chalaniNum' => 'required|numeric|unique:relation_identifies',
            'issuedDate' => 'required',
            'name' => 'required', 
            'municipalityName' => 'required', 
            'wardNumber' => 'required|numeric', 
            'municipality' => 'required',
            'ward' => 'required|numeric',
            'providedDate' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
