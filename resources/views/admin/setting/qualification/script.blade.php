<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditQualification']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of qualification is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmQualification']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of qualification is required"
            }
        });
  var btnAddQualification = $('.btnAddQualification');
  btnAddQualification.on('click', function(e){
      $('#quaModel').modal('show'); 
  });

  var btnEditQua = $('.btnEditQua');
  btnEditQua.on('click', function(e) {
      e.preventDefault();
      var url = $(this).data('url');
      $.ajax({
        'type': 'GET',
        'url': url,
        success: function (response) {
            $('.quaId').val(response.id);
            $('.txtRefCode').val(response.refCode).attr('disabled',true);
            $('.txtNameNep').val(response.nameNep);
            $('.txtNameEng').val(response.nameEng);
            $("#editQuaModel").modal('show');
        }
    })
  });


  var frmUpdateQualify = $('#frmUpdateQualify');
  frmUpdateQualify.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.quaId').val();
    var url  = "{{URL::to('/')}}" + "/setting/qualification/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editQuaModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editQuaModel').modal('hide');
});
});


</script>