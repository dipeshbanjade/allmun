<?php

namespace App\Http\Requests\admin\setting\country;

use Illuminate\Foundation\Http\FormRequest;

class CountryVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

     public function rules()
    {
        return [
            'nameEng' => 'required|min:4:min:50'
        ];
    }
}
