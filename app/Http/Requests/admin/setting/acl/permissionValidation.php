<?php

namespace App\Http\Requests\admin\setting\acl;

use Illuminate\Foundation\Http\FormRequest;

class permissionValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'perName'    => 'required|min:3|max:100',
            'moduleName' => 'required|min:3|max:100',
            'routeName' => 'required|min:3|max:100'
        ];
    }
}
