<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १.  तपाइको परिवारको सदस्य घरायसी कामको लागि पोखरा लेखनाथ महानगरपालिका भित्र काम गर्न गएका छन् ? 
                    </p>
                    <div>
                        <label class="radio-inline">
                            १. छन् 
                            <input type="radio" onchange="displayFmWorkingTbl(this)" name="haveWorking" value="1"  {{ isset($hsFMWD) && $hsFMWD->haveWorking == 1 ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैनन्
                            <input type="radio" onchange="displayFmWorkingTbl(this)" name="haveWorking" value="0" {{ isset($hsFMWD) && $hsFMWD->haveWorking == 0 ? 'checked' : '' }}>
                            यदि छन् भने तलको विवरण भर्नुहोस् ।
                        </label>
                    </div>
                </div>
                <br>
                @if (isset($hsFMWD) && $hsFMWD->haveWorking == 1)
                    <table class="table fmWorkingDetailTbl" style="display: block;">
                @else
                    <table class="table fmWorkingDetailTbl" style="display: none;">

                @endif
                    <tr>
                        <th>सि.न</th>
                        <th>काम गर्न गएको</th>
                        <th>काम गर्न गएको सदस्यको नाम</th>
                        <th>उमेर</th>
                        <th>काम शुरु गरेको मिति</th>
                        <th>दैनिक ⁄ महिनाबारी</th>
                        <th>कामको प्रकार </th>
                        <th>गत ३० दिनमा पाएको (सुविधा समेत) पारिश्रमिक रु.</th>
                        <th>संस्था वा परिवार मुलीको नाम</th>
                        <th>वडा नं</th>
                    </tr>
                    <?php 
                    $count = 0;
                    $familyWorkingDetail = isset($hsFMWD->familyWorkingDetails) ? json_decode($hsFMWD->familyWorkingDetails) : '';
                        //Note $getMyFamilyDetails from houseDetailsController
                    foreach($getMyFamilyDetails as $member){
                        $memberInfo = getParentsName($member->relation_id);
                        $memberAge = getAge($memberInfo['dobAD']);

                        $count++;
                        $singleFmWorkingInfo = moreHouseDetailsFormUpdate($member->relation_id, $familyWorkingDetail, $compareJsonKey = 'memberId');
                            //dd($memberInfo);
                            //dd($singleChildHealthCare);
                        ?>
                        <tr>                        
                            <td>{{ $count }}</td>
                            <td>
                                <input type="hidden"  name="familyWorkingDetails[<?php echo $count ?>][memberId]" value="{{ $member->relation_id }}">

                                <input type="checkbox" onchange="requireWorkingDetail(this)" name="familyWorkingDetails[<?php echo $count ?>][isWorking]" value="1" {{ isset($singleFmWorkingInfo['isWorking']) ? 'checked' : '' }} >
                            </td>
                            <td>
                                {{ Form::text("familyWorkingDetails[$count][memName]", $memberInfo['fnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("familyWorkingDetails[$count][age]", $memberAge, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Age', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::date("familyWorkingDetails[$count][jobStartDate]", isset($singleFmWorkingInfo['jobStartDate']) ? $singleFmWorkingInfo['jobStartDate'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("familyWorkingDetails[$count][salaryBased]",  isset($singleFmWorkingInfo['salaryBased']) ? $singleFmWorkingInfo['salaryBased'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("familyWorkingDetails[$count][workType]",  isset($singleFmWorkingInfo['workType']) ? $singleFmWorkingInfo['workType'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Work Type', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("familyWorkingDetails[$count][monthlySalary]",  isset($singleFmWorkingInfo['monthlySalary']) ? $singleFmWorkingInfo['monthlySalary'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("familyWorkingDetails[$count][muliName]",  isset($singleFmWorkingInfo['muliName']) ? $singleFmWorkingInfo['muliName'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("familyWorkingDetails[$count][wardNo]",  isset($singleFmWorkingInfo['wardNo']) ? $singleFmWorkingInfo['wardNo'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'wardNo', 'id' => '']) }}
                            </td>
                        </tr>
                        <?php } ?>                                     
                    </table>
                    <br>
                </div>                             
            </div>                    
        </div>
            <div class="msgDisplay"></div>
        
    </div>