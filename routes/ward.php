<?php
   Route::prefix('ward')->group(function(){
       /*ward Details route*/
       Route::get('/wardStaff',             'admin\ward\wardStaffController@index')->name('wardStaff.index');
       Route::get('/wardStaff/create', 'admin\ward\wardStaffController@create')->name('wardStaff.create');
       Route::post('/wardStaff/store',      'admin\ward\wardStaffController@store')->name('wardStaff.store');
       
       Route::get('/wardStaff/{slug}/edit',   'admin\ward\wardStaffController@edit')->name('wardStaff.edit');
       Route::get('/wardStaff/{slug}',        'admin\ward\wardStaffController@show')->name('wardStaff.show');

       Route::post('/wardStaff/{id}/update', 'admin\ward\wardStaffController@update')->name('wardStaff.update');
       Route::get('/wardStaffDel/{id}',        'admin\ward\wardStaffController@destroy')->name('wardStaff.delete');
       Route::get('/wardStaffChangeStatus', 'admin\ward\wardStaffController@changeStatus')->name('wardStaff.changeStatus');
       /*-----------------------------------------------*/
       
        Route::get('/wardStaffLeave',              'admin\ward\wardStaffLeaveController@index')->name('wardStaffLeave.index');
        Route::get('/allLeaveForm',              'admin\ward\wardStaffLeaveController@getAllStaffLeave')->name('AdminListLeaveForm');
        Route::get('/wardStaffLeave/create',      'admin\ward\wardStaffLeaveController@create')->name('wardStaffLeave.create');
        Route::post('/wardStaffLeave/store',      'admin\ward\wardStaffLeaveController@store')->name('wardStaffLeave.store');
        Route::get('/wardStaffLeave/{id}/edit',   'admin\ward\wardStaffLeaveController@edit')->name('wardStaffLeave.edit');
        Route::post('wardStaffLeave/{id}/update', 'admin\ward\wardStaffLeaveController@update')->name('wardStaffLeave.update');
        Route::get('/wardStaffLeave/{id}',        'admin\ward\wardStaffLeaveController@destroy')->name('wardStaffLeave.delete');
        Route::get('wardStaffLeaveChangeStatus',  'admin\ward\wardStaffLeaveController@changeStatus')->name('wardStaffLeave.changeStatus');

   });
?>