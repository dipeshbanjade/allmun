@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
    <div class="col-sm-12 col-md-12" id="myDIV" >
      <div class="card-box">
        <div class="card-body">
         <div class="card-head">
          <header>@lang('commonField.extra.muncipility')&nbsp;@lang('commonField.extra.createLeave')</header>
        </div>
    <div class="table-scrollable" style="color:black;">
      <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle"  id="example4">
        <thead>
          <tr>
            <th width="3%">@lang('commonField.extra.sn')</th>
            <!-- <th>@lang('commonField.extra.refCode')</th> -->
            <th>@lang('commonField.extra.staff')</th>
            <th>@lang('commonField.extra.department')</th>
            <!-- <th>@lang('commonField.extra.department')</th> -->
            <th width="10%">@lang('commonField.extra.leaveType')</th>
            <th width="10%">@lang('commonField.extra.noOfDays')</th>
            <th>@lang('commonField.extra.startDate')</th>
            <th width="7%">@lang('commonField.extra.appliedOn')</th>
            <th>@lang('commonField.extra.action')</th>
            <th>@lang('commonField.extra.approvedBy')</th>
          </tr>
        </thead>
        <tbody>
           <?php  
               $count = 1; 
               $lan   = getLan(); 
            ?>
           @if(count($viewAdmLeaveForm))
                @foreach($viewAdmLeaveForm as $leave)
                     <?php
                        $leaveType = getleaveType($leave->leave_types_id);
                        $leaveName = $lang == 'np' ? $leaveType['nameNep'] : $leaveType['nameEng'];
                        $stafName  = getMunStaff($leave->users_id); 
                     ?>
                    <tr class="odd gradeX">
                      <td>
                           {{ $count ++ }}
                      </td>
                      <td>
                        {{ $lan == 'np' ? $stafName->firstNameNep .' '.  $stafName->middleNameNep .' '. $stafName->lastNameNep :  $stafName->firstNameEng .' '. $stafName->middleNameEng .' '. $stafName->lastNameEng  }}

                      </td>
                      <td>
                        {{ 
                           $lan == 'np' ? $stafName->departmentNameNep : $stafName->departmentNameEng 
                        }}
                      </td>
                      <!-- <td>{{ $leave->refCode }}</td> -->
                      <!-- <td></td> -->
                      <td>{{ $leaveName }}</td>  
                      <td >{{ $leave->noOfDays }} days</td>
                      <td>{{ $leave->startDate }} - {{ $leave->endDate }}</td>
                      <td>{{ $leave->created_at->diffForHumans() }}</td>
                      <td title="{!! $leave->shortNoteEng !!}">
                          {!! str_limit($leave->shortNoteEng , 100) !!}
                      </td>
                      <td>
                          <span>
                             <a href="#" data-url="{!! route('admin.munStaffLeave.changeStatus') !!}"  data-status = "{{ $leave->status }}" data-id = "{{ $leave->id }}" class="changeStatus">
                             <button class="<?php echo !$leave->approvedBy=='' ? 'btn btn-success btn-sm': 'btn btn-danger btn-sm' ?>">
                               <?php 
                               echo  !$leave->approvedBy== '' ? 'Approve' : 'Pending';    
                               ?>
                            </button>
                              </a>
                          </span>
                      </td>
                      </tr>
                @endforeach
           @endif
          </tbody>
        </table>
        {{ $viewAdmLeaveForm->links() }}
      </div>
    </div>
  </div>
  </div>
  @endsection