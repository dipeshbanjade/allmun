<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipilitiesTable extends Migration
{
    public function up()
    {
        Schema::create('municipilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('munLogo')->nullable();
            $table->string('uniqueCode', 100)->unique();
            $table->string('identifier', 100)->unique();
            $table->string('nameNep');
            $table->string('nameEng')->nullable();
            $table->string('landLineNumber', 10);

            $table->string('faxNumber', 10)->nullable();
            $table->string('addrNep', 60);
            $table->string('addrEng', 60)->nullable();

            $table->integer('provinces_id')->unsigned();
            $table->foreign('provinces_id')->references('id')->on('provinces')->onDelete('cascade');

            $table->integer('districts_id')->unsigned();
            $table->foreign('districts_id')->references('id')->on('districts')->onDelete('cascade');

            $table->string('muncipilityHead')->nullable();
            $table->integer('bioDeviceId')->nullable();

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
        /*-------------------*/
        Schema::create('wards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wardLogo')->nullable();
            $table->string('uniqueCode', 100)->unique();
            $table->string('identifier', 100)->unique();
            $table->string('nameNep');
            $table->string('nameEng')->nullable();
            $table->string('landLineNumber', 10);

            $table->string('faxNumber', 10)->nullable();
            $table->string('addrNep', 60);
            $table->string('addrEng', 60)->nullable();

            $table->integer('provinces_id')->unsigned();
            $table->foreign('provinces_id')->references('id')->on('provinces')->onDelete('cascade');

            $table->integer('districts_id')->unsigned();
            $table->foreign('districts_id')->references('id')->on('districts')->onDelete('cascade');

            $table->integer('municipilities_id')->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            $table->string('muncipilityHead')->nullable();
            $table->integer('bioDeviceId')->nullable();
            

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });

        /*staff*/
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('municipilities_id')->unsigned();
           $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

           $table->integer('wards_id')->nullable()->unsigned();
           $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

           $table->string('refCode', 100)->unique();
           $table->string('imagePath')->nullable();


           $table->string('citizenNo', 100)->unique()->nullable();


           $table->integer('staff_types_id')->unsigned();
           $table->foreign('staff_types_id')->references('id')->on('staff_types')->onDelete('cascade');


           $table->integer('deginations_id')->unsigned();
           $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

           $table->string('firstNameEng', 100);
           $table->string('firstNameNep', 100);

           $table->string('middleNameEng', 100)->nullable();
           $table->string('middleNameNep', 100)->nullable();

           $table->string('lastNameEng', 100);
           $table->string('lastNameNep', 100);

           $table->integer('provinces_id')->unsigned();
           $table->foreign('provinces_id')->references('id')->on('provinces')->onDelete('cascade');


           $table->integer('districts_id')->unsigned();
           $table->foreign('districts_id')->references('id')->on('districts')->onDelete('cascade');

           $table->date('joingDateNep');
           $table->date('joingDateEng');


           $table->string('citizenImagePath')->nullable();

           $table->string('wordNo');
           $table->string('villageEng')->nullable();
           $table->string('villageNep');
           $table->string('phoneNumber', 100);
           $table->string('email', 100)->nullable();
           $table->boolean('status')->default(0);
           $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
        /*-----------------*/
        Schema::create('leaves', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('municipilities_id')->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

           $table->integer('wards_id')->nullable()->unsigned();
           $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('staff_id')->unsigned();
            $table->foreign('staff_id')->references('id')->on('staff')->onDelete('cascade');

            $table->integer('leave_types_id')->unsigned();
            $table->foreign('leave_types_id')->references('id')->on('leave_types')->onDelete('cascade');

            $table->string('identifier', 100)->unique();
            $table->string('leaveRefCode', 100)->unique();

            $table->string('noOfDays');

            $table->string('shortNoteNep');
            $table->string('shortNoteEng')->nullable();

            $table->date('startDate');
            $table->date('endDate');

            $table->integer('approvedBy')->nullable()->unsigned();
            $table->foreign('approvedBy')->references('id')->on('staff')->onDelete('cascade');

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });

        Schema::create('attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('municipilities_id')->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

           $table->integer('wards_id')->nullable()->unsigned();
           $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('staff_id')->unsigned();
            $table->foreign('staff_id')->references('id')->on('staff')->onDelete('cascade');

            $table->text('attendance')->nullable();   // biomateric case

            $table->date('checkInTime');
            $table->date('checkOutTime');

            $table->boolean('adminApprove')->default(0);

            $table->boolean('status')->default(0);
            $table->timestamps();
        });

    }
    public function down()
    {
        Schema::dropIfExists('municipilities');
        Schema::dropIfExists('wards');
        Schema::dropIfExists('staff');
        Schema::dropIfExists('leaves');
        Schema::dropIfExists('attendances');
    }
}
