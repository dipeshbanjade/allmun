<div class="right_col nep" role="main" style="min-height: 882px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">सहुलियत काठ सिफारिस</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

         @include('admin.shifaris.municipalityDetail')

         <div class="row top-part">
          <div class="col-md-6">
            <p align="left">
             <b>@lang('commonField.extra.refCode'):</b>
             @if(isset($data->refCode))
             {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
             @else
             {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
             @endif
           </p>
           <p align="left">
             <b> च. नं.:
               {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
             </b>
           </p>
         </div>
         <div class="col-md-6">
           <p align="right"><b class="mt">मिति :
             {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
           </b>
         </p>
       </div>
     </div>
     <div class="row">
      <div class="col-md-12">
       <p align="center" class="font-size-24">
       </p><h4 align="center"><b>विषय: सिफारिस गरिएको बारे।</b> </h4>
       {{ Form::hidden('letter_subject', 'सिफारिस गरिएको बारे।', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
       <p></p>
     </div>
   </div>
   <div class="row title-left">
    <div class="col-md-12">
     <p>श्री 
      {{ Form::text('companyHead', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyHead', 'required' => 'required']) }}
    </p>
    <p> {{ Form::text('companyAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyAddress', 'required' => 'required']) }}।</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">
   <p>
    उपरोक्त विषयमा  {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
    {{ Form::text('personWardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'personWardNumber', 'required' => 'required']) }} मा बस्ने 
    {{ Form::text('personName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'personName', 'required' => 'required']) }} ले 
    {{ Form::text('purpose', 'घर निर्माण', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'purpose', 'required' => 'required']) }} प्रयोजनको लागि  लकडीको सिफारिस पाऊँ भनि
    {{ Form::text('appealDetail', 'लकडीको सिफारिस पाऊँ भनि', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'appealDetail', 'required' => 'required']) }}
    दिएको निवेदन अनुसार निजलाई <span id="p_copy">घर निर्माण</span> प्रयोजनको लागि 
    {{ Form::text('requiredKath', 'मसला जातको लकडी', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'requiredKath', 'required' => 'required']) }}
    आवश्यक भएको हुँदा नियम अनुसार 
    {{ Form::text('kibiInfo', '८५ कि.बि. लकडी', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'kibiInfo', 'required' => 'required']) }}
    उपलब्ध गराई दिनहुन सिफारिस गरिएको व्येहोरा अनुरोध छ।
  </p> 
</div>
<div class="col-md-12">
</div>
<div class="col-md-12">
  <div class="text-right btm-last">
   <p>{{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} </p>
   <p> <b>
    <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
      @if(count($deginationsId) > 0)
      @foreach($deginationsId as $deg)
      <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
        {{ $deg->nameNep }} 
      </option>
      @endforeach
      @endif
    </select>
  </b> </p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>
</div>
</div>
</div>
</div>
</div>
</div>