<script type="text/javascript">

	var addButton = $('#btnAddTableRow');
	var clickCount = 1;
	var counter = 0;

	addButton.on('click', function(e){
		e.preventDefault();
		clickCount++;

		var actualDate = '<input class="dashed-input-field ndp-nepali-calendar date-input-field" placeholder="    *" required="required" id="actual_date' + (counter += 1) + '" onfocus=\'showNdpCalendarBox("actual_date'+ (counter) +'")\' name="actualDate[]" type="text" autocomplete="off">';

		var diffDate = '<input class="dashed-input-field ndp-nepali-calendar date-input-field" placeholder="    *" required="required" id="diff_date' + (counter += 1) + '" onfocus=\'showNdpCalendarBox("diff_date'+ (counter) +'")\' name="diffDate[]" type="text" autocomplete="off">';

		var diffPaper = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="diffPaper[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+actualDate+"</td>"
		+"<td>"+diffDate+"</td>"
		+"<td>"+diffPaper+"</td>"

		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#diff_date_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmWarddiffBDCert']").validate({
			rules:{

				refCode : {
					required : true,
					minlength : 3,
					maxlength : 10
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number: true
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				municipalityAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityType : {
					required : true				
				},
				municipalityWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				personPrefix : {
					required : true
				},
				personName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {

				refCode : "Ref code is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				municipalityName : "Municipality Name is required",
				wardNumber : "Ward Number is required",
				municipalityAddress : "Municipality Address is required",
				municipalityType : "Municipality Type is required",
				municipalityWard : "Municipality Ward is required",
				personPrefix : "Person Prefix is required",
				personName : "Person Name is required",
				authorizedPerson : "Authorized Person is required"
			}
		});
	});

	
	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='personName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>