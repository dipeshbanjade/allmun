<script type="text/javascript">
	  $(function(){
	  $("form[name='frmChangePassword']").validate({
	   rules:{
	    oldPassword : {
	      required : true
	    },
	    password : {
	      required : true,
	      minlength : 6,
	      maxlength : 16
	    },
	    confirmPassword : {
	      required : true,
	      minlength : 6,
	      maxlength :16,
	      equalTo : '[name="password"]'
	    }
	  },
	  messages: {
	    oldPassword : "Please enter old password",
	    password : "Please enter new password min 6 maximum 16 character",
	    confirmPassword : "confirmPassword does not match"
	  }
	});
	});
</script>