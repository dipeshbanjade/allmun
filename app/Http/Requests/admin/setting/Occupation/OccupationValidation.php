<?php

namespace App\Http\Requests\admin\setting\Occupation;

use Illuminate\Foundation\Http\FormRequest;

class OccupationValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:30|unique:occupations',
            'nameEng' => 'required|min:3|max:100'
        ];
    }
}
