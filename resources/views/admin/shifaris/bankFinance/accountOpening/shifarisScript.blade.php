<script type="text/javascript">
	var btnAddTableRow = $('#addTableRow');
	var clickCount = 1;
	btnAddTableRow.on('click', function(e){
		e.preventDefault();

		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var personName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="personName[]"> ';
		var personPost = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="personPost[]">';
		var remarks = '<input type="text" class="dashed-input-field" name="remarks[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+personName+"</td>"
		+"<td>"+personPost+"</td>"
		+"<td>"+remarks+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#house_kayem_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardaccountOpening']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 15
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum : {
					required: true,
					minlength: 1					
				},
				nameOfOffice : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				locationOfOffice : {
					required : true,
					minlength: 5,
					maxlength: 255
				},
				municipalityName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				wardNum : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				organizationWard : {
					required : true,
					minlength: 1,
					maxlength: 3,
					number:true
				},
				organizationType : {
					required : true
				},
				workName : {
					required : true,
					minlength : 3,
					maxlength : 50
				},
				personName : {
					required : true,
					minlength : 5,
					maxlength : 255
				},
				personPost: {
					required : true,
					minlength : 5,
					maxlength : 255
				},
				authorizedPerson : {
					required : true,
					minlength: 3,
					maxlength: 20
				}
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNum : "Chalani Number is required",
				nameOfOffice : "Name of Office is required",
				locationOfOffice : "Office Location is required",
				municipalityName : "Municipality name is required",
				wardNum : "Ward Number should be numeric",
				organizationType : "Organization Type is required",
				organizationAddress : "Organization Address is required",
				organizationWard : "Organization Ward must be numeric",
				workName : "Work Name is required",
				personName : "Person Name is required",
				personPost : "Person Post is required",
				authorizedPerson : "Authorizing person is required"
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='personName[]']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>