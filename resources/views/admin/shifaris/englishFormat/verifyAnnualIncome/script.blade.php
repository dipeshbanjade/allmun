<script type="text/javascript">

var btnAddBodartha = $('#add_more_income_source');
// var clickCount = 1;
var currencyRate = $('#per_dollar_nrs');


btnAddBodartha.on('click', function(e){
	var annualIncome = document.getElementsByClassName('annual_income');
	var annualIncomeLength = annualIncome.length + 1;



	e.preventDefault();		
	// clickCount++;
	
	var no = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + annualIncomeLength + '">';
	var incomeSource = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="incomeSource[]">';
	var incomeHolderName = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="incomeHolderName[]">';
	var relationWithSeeker = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="relationWithSeeker[]">';
	var annualIncome = '<input type="text" class="dashed-input-field annual_income" placeholder="   *" required="required" id="annual_income" name="annualIncome[]">';
	var remarks = '<input type="text" class="dashed-input-field" name="remarks[]">';

	var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

	var insertRow = "<tr class='tblRow'>"
	+"<td>"+no+"</td>"
	+"<td>"+incomeSource+"</td>"
	+"<td>"+incomeHolderName+"</td>"
	+"<td>"+relationWithSeeker+"</td>"
	+"<td>"+annualIncome+"</td>"
	+"<td>"+remarks+"</td>"
	+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
	+"</tr>";

	$('#annual_income_verification_table').append(insertRow);

	$('.btnRemoveItem').on('click', function(e){
		$(this).parent().parent().remove();
		calcTotalAmount('.annual_income');
		manageSerialNumber();
		return false;
	});

	addListener();

});

$('#annual_income').on('keyup', function(){
	calcTotalAmount('.annual_income');
});

currencyRate.on('keyup', function(e){
	calcTotalAmount('.annual_income');
});

function manageSerialNumber(){

}

function addListener(){
	var annualIncomeObj = document.getElementsByClassName('annual_income');

	var rowNumber = annualIncomeObj.length - 1;

	annualIncomeObj[rowNumber].addEventListener('keyup', function(){
		calcTotalAmount('.annual_income');
	});
}

function calcTotalAmount(className){
	currencyRate = $('#per_dollar_nrs');
	totalAnnualIncome = 0;
	var inputField = document.querySelectorAll(className);
	var inputFieldLength = inputField.length;

	for (var i = 0; i < inputFieldLength; i++) {
		if(inputField[i].value == '' || inputField[i].value == null){
			continue;
		}
		totalAnnualIncome += parseInt(inputField[i].value);
	}

	var totalNrsFieldToBeUpdate;
	var totalForeignFieldToBeUpdate;

	totalFieldToBeUpdate = '#valuationInNrs';
	totalForeignFieldToBeUpdate = '#total_valuation_dollar';

	addtotalAmmount(totalAnnualIncome, totalFieldToBeUpdate, totalForeignFieldToBeUpdate);
}

function addtotalAmmount(totalAnnualIncome, totalFieldToBeUpdate, totalForeignFieldToBeUpdate){

	$(totalFieldToBeUpdate).val(totalAnnualIncome);

	var foreignCurrencyRate = currencyRate.val();
	$(totalForeignFieldToBeUpdate).val(amountInForeignCurrency(totalAnnualIncome, foreignCurrencyRate));
}

function amountInForeignCurrency(totalAmt, foreignCurrencyRate){
	return totalAmt/foreignCurrencyRate;
}




$(function(){
	/*add buttoin*/
	$("form[name='frmWardverifyAnnualIncome']").validate({
		rules:{

			refCode : {
				required : true,
				minlength : 3,
				maxlength : 10
			},
			issuedDate : {
				required : true,
				date : true
			},
			title : {
				required : true
			},
			personName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipality : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			ward : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			district : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			former : {
				required : true
			},
			orgAddr : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			orgType : {
				required : true
			},
			orgWard : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			valuationInNrs : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			perDollarNrs : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			currency_type : {
				required : true
			},
			totalValueInDollars : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			refCode : "Refcode is required",
			issuedDate : "Issue Date is required",
			title : "Title  is required",
			personName : "Person name is required",
			municipality : "Municipality is required",
			ward : " Ward is required",
			district : " Distrct is required",
			former : " Former is required",
			orgAddr : " OrgAddr is required",
			orgType : " OrgType is required",
			orgWard : " OrgWard is required",
			valuationInNrs : " Valution is required",
			perDollarNrs : " Per DollerNrs is required",
			currency_type : " Currency type is required",
			totalValueInDollars : " Total Valuation in dollor is required",
			authorizedPerson : " Authorized Person is required"
		}
	});
});

$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                  $("input[name='personName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
            
          },complete:function(){
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>