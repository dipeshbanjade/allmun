<?php

namespace App\model\admin\houseDetails\childCareInvolvement;

use Illuminate\Database\Eloquent\Model;

class HsOtherMemLivingTog extends Model
{
    protected $table = 'hs_other_mem_living_togs';
}
