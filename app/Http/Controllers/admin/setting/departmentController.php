<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\Department;
use App\model\mylogic;


class departmentController extends Controller
{
    protected $viewPath  = 'admin.setting.department';
      protected $departmentTbl = 'departments';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
         $page['page_title']       = 'Department';
       $page['page_description'] = 'list of all Department';
       $allDepartments = Department::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        return view($this->viewPath . '.index', compact(['page', 'allDepartments']));
    }

    public function store(Request $request)
    {
        $mySave = Department::create([
                  'nameNep' => $request->nameNep ?? 'N/A',
                  'nameEng' => $request->nameEng,
                  'status'  => 0,
                  'softDelete' => 0
                ]);
       if ($mySave) {
           return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
       }else{
           return back()->withMessage(config('activityMessage.unSaveMessage'));
       }
    }

    public function edit($id)
    {
        $data = Department::findOrFail($id);
       if ($data) {
           return response()->json([
                   'success' => true,
                   'id'      => $data->id,
                   'nameNep' => $data->nameNep,
                   'nameEng' => $data->nameEng
                  ]);
       }else{
           return response()->json([
               'success' => false,
               'message' => config('activityMessage.dataNotFound')
               ]);
       }
    }

    public function update(Request $request, $id)
    {
        $update = Department::findOrFail($id);
        $update->nameNep   = $request->nameNep;
        $update->nameEng  = $request->nameEng;

        $myUpdate = $update->update();

        if ($myUpdate) {
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.updateMessage')
                ]);
        }else{
            return response()->json([
                'success'  => false,
                'message'  => config('activityMessage.notUpdated')
                ]);
        }
    }

    public function destroy($id)
    {
        $del = Department::findOrFail($id);
       $del->softDelete = 1;
       $myDel = $del->update();
       if ($myDel) {
         return MyLogic::successChangeStatus($code = 200);
       }else{
          return MyLogic::successChangeStatus($code = 301);
       }
    }
}

