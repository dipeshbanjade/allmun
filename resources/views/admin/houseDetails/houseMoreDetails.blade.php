@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
  <div class="card-head">
    <header>
      <i class="fa fa-eye">
        @lang('commonField.links.createHouseDetails')
        <!-- Note : getMyFamilyDetails come from houseDetails insertMoreDetails function  -->
      </i>
      <?php  $lan = getLan();    ?>
      <p class="pull-right">
        <i class="fa fa-user"></i>
        <label>
         @lang('commonField.extra.citizen')
         <span>
          <?php

          $citizenNameNep = isset($hsDetails->citizen_infos) ? $hsDetails->citizen_infos->fnameNep .' '.$hsDetails->citizen_infos->mnameNep.'  '. $hsDetails->citizen_infos->lnameNep   : 'N/A';

          $citizenNameEng = isset($hsDetails->citizen_infos) ? $hsDetails->citizen_infos->fnameEng .' '.$hsDetails->citizen_infos->mnameEng.'  '. $hsDetails->citizen_infos->lnameEng   : 'N/A';

          $mainTab = Session::get('mainTab');
          $helpTab = Session::get('helpTab');

          ?>

          <a href="{{  !empty($hsDetails->citizen_infos->idEncrip) ? route('citizenInfo.show', $hsDetails->citizen_infos->idEncrip) : '#' }}">
           &nbsp;&nbsp;{{ $lan =='nep' ? ucfirst($citizenNameNep) : ucfirst($citizenNameEng) }}
         </a>
       </span>
     </label>
     <br>
     <label>
      <i class="fa fa-home"></i>
      @lang('commonField.extra.houseNum')
      {{ $hsDetails->hsNum }}

    </label>
  </p>
</header>
</div>
<div class="col-md-12 col-sm-12 fontColorBlack">
  <div class="card">
    <div class="card-head">
      <header>
        @lang('commonField.button.create')
      </header>
    </div>
    <div class="card-body " id="bar-parent">
      <div class="row">
        <div class="col-md-2 col-sm-2 col-2">
          <ul class="nav nav-tabs tabs-left">
           <li class="nav-item active">
            <p align="right">भाग ४</p>
            <a href="#tab_1" data-toggle="tab" <?php echo $mainTab =='familyMember' ?  'class=active show' : ''    ?>>
              जनसंख्या सम्बन्धी विवरण
              <small>Family Members<br>
                <strong>भाग ४</strong></small>
              </a>
            </li>

            <li class="nav-item">
              <a href="#tab_2" data-toggle="tab">
                पारिवारिक सुविधा सम्बन्धी विवरण
                <small>Family Facilities
                  <strong>भाग ३</strong></small>
                </a>
              </li>
              <li class="nav-item">
                <a href="#tab_3" data-toggle="tab">
                  बालबालिकाको शिक्षा सम्बन्धी विवरण
                  <small>Children Education
                    <strong>भाग ५</strong></small>

                  </a>
                </li>
                <li class="nav-item">
                  <?php  $showHealth = 'active'.' '. 'show'; ?>
                  <a href="#tab_4" data-toggle="tab" <?php echo $mainTab =='HealthNCleaness' ?  'class="$showHealth"' : ''    ?>>
                   स्वास्थ्य तथा सरसफाई सम्बन्धी विवरण
                   <small>Health and Cleaness {{ $showHealth }}
                    <strong>भाग ६</strong></small>

                  </a>
                </li>
                <li class="nav-item">
                  <a href="#tab_5" data-toggle="tab">
                    बालसंरक्षण र बाल सहभागिता सम्बन्धी विवरण
                    <small>Childcare and Involvement
                      <strong>भाग ७</strong></small>

                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#tab_6" data-toggle="tab">
                      आर्थिक अवस्था सम्बन्धी विवरण
                      <small>Economic Condition
                        <strong>भाग ८</strong></small>

                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="#tab_7" data-toggle="tab">
                        प्राकृतिक प्रकोप, सुरक्षा एवं वातावरणीय समस्या सम्बन्धी
                        <small>Natural Disaster, Security and Environment
                          <strong>भाग ९</strong></small>
                        </a>
                      </li>

                      <li class="nav-item">
                        <a href="#tab_8" data-toggle="tab">
                          घर भाडा विवरण
                          <small>Rental Details
                            <strong>भाग १०</strong></small>
                          </a>
                        </li>

                      </ul>
                    </div>
                    <div class="col-md-10 col-sm-10 col-10">
                      <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                          <header>
                            <i class="fa fa-eye">
                              @lang('commonField.links.createHouseDetails')
                            </i>
                          </header>
                          {{-- accordion starts from here --}}

                          <div class="accordion" id="accordionExample">
                            <div class="card z-depth-0 bordered">
                              <div class="card-header" id="headingOne"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="<?php echo $helpTab=='houseMem' ? 'true' : 'false';   ?>" aria-controls="collapseOne">
                                <h4 class="mb-0">
                                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                                  aria-expanded="true" aria-controls="collapseOne">
                                  परिवार सदस्यको विवरण : Family Member Details
                                </button>
                              </h4>
                            </div>
                            <div id="collapseOne" class="collapse <?php echo $helpTab=='houseMem' ? 'show' : '';   ?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                              <div class="card-body">
                                <div class="row">
                                  <!-- member -->
                                  <p>
                                    <strong>परिवार सदस्यको विवरण (परिवारमुलीबाट शुरु गर्ने र जेष्ठ सदस्यअनुसार क्रम मिलाएर उल्लेख गर्ने)
                                    </strong>
                                  </p>
                                  <p>
                                    <button class="btnAddMember btn btn-success btn-sm pull-right">
                                      <i class="fa fa-plus">Add Member Details</i>
                                    </button>
                                  </p>
                                  <div class="memberFrm" style="display: none;">
                                    {{ Form::open(['route'=>'insertCitizenFromHome','name'=>'frmFamilyMemberDetails']) }}
                                    @include('admin.houseDetails.familyMember.member')
                                    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
                                    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
                                    {{ Form::hidden('citizen_infos_id',$hsDetails->citizen_infos_id )}}
                                    <p class="p-4">
                                      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
                                        <span>@lang('commonField.button.back')</span>
                                      </button>
                                      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
                                    </p>
                                    {{ Form::close() }}
                                  </div>
                                </div>
                                <div class="form-group col-md-12">
                                  <!-- abouve me -->
                                  <div>
                                    <p>
                                      १.परिवार सदस्यको विवरण (परिवारमुलीबाट शुरु गर्ने र जेष्ठ सदस्यअनुसार क्रम मिलाएर उल्लेख गर्ने)े
                                    </p>
                                  </div>

                                  @if(!empty($getMyFamilyDetails) && $getMyFamilyDetails)

                                  <table border="1" class="table table-responsive table-scrollable">
                                    <thead>
                                      <tr>
                                        <th>क्र.सं</th>
                                        <th>
                                          @lang('commonField.personal_information.nata')
                                        </th>
                                        <th>नाम थर</th>
                                        <th>लिङ्ग</th>
                                        <th>उमेर(पुरा भएको)</th>
                                        <th>जात जाती</th>
                                        <th>धर्म</th>
                                        <th>मातृ भाषा</th>
                                        <th> शैक्षिक स्तर(५ वर्ष माथिका लागी मात्र)</th>
                                        <th> बसोबासको अवस्था(स्वदेशमा अन्यत्र वा विदेशमा बसेको सदस्यको हकमा यसपछिको विवरण नलिने)</th>
                                        <th>पेशा(१० बर्ष भन्दा माथी)</th>
                                        <th>वैवाहिक स्थिति (१० बर्ष भन्दा माथी)</th>
                                        <th>अपाङ्गताको स्थिति</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                      $HouseOwnerDetails = getParentsName($hsDetails->citizen_infos_id);

                                      $jatJatiData = isset($HouseOwnerDetails->jatjatis_id) ? getTableData('jatjatis', $HouseOwnerDetails->jatjatis_id) : '';

                                      $religiousesData = isset($HouseOwnerDetails->religiouses_id) ? getTableData('religiouses', $HouseOwnerDetails->religiouses_id) : '';

                                      $sysLanguagesData = isset($HouseOwnerDetails->sys_languages_id) ? getTableData('sys_languages', $HouseOwnerDetails->sys_languages_id) : '';

                                      $qualificationsData = isset($HouseOwnerDetails->qualifications_id) ? getTableData('qualifications', $HouseOwnerDetails->qualifications_id) : '';

                                      $occupationsData = isset($HouseOwnerDetails->occupations_id) ? getTableData('occupations', $HouseOwnerDetails->occupations_id) : '';

                                      $fmLivingData = isset($HouseOwnerDetails->family_living_id) ? getTableData('living_fors', $HouseOwnerDetails->family_living_id) : '';

                      //dd($HouseOwnerDetails);

                                      $lang = getLan();
                                      ?>
                                      <tr>
                                        <td>1</td>
                                        <td>@lang('commonField.personal_information.gharMuli')</td>
                                        <td>
                                          <a href="{{ route('citizenInfo.show', $HouseOwnerDetails->idEncrip) }}" target="_blank">
                                            {{ $lan =='nep' ? $HouseOwnerDetails->fnameNep .''. $HouseOwnerDetails->mnameNep .' '.$HouseOwnerDetails->lnameNep :  $HouseOwnerDetails->fnameEng .''. $HouseOwnerDetails->mnameEng .' '.$HouseOwnerDetails->lnameEng   }}
                                          </a>
                                        </td>
                                        <td>{{ isset($HouseOwnerDetails->gender) ? $HouseOwnerDetails->gender : '' }}</td>
                                        <td>{{ isset($HouseOwnerDetails->dobAD) ? $HouseOwnerDetails->dobAD : '' }}</td>
                                        <td>
                                         <?php
                                         if ($jatJatiData) {
                                           echo isset($jatJatiData) ? (($lang == 'np') ? $jatJatiData->nameNep : $jatJatiData->nameEng) : '';
                                         }
                                         ?>
                                       </td>
                                       <td>
                                         <?php
                                         if ($religiousesData) {
                                          echo isset($religiousesData) ? (($lang == 'np') ? $religiousesData->nameNep : $religiousesData->nameEng) : '';
                                        }
                                        ?>
                                      </td>
                                      <td>
                                        <?php
                                        if ($sysLanguagesData) {
                                         echo isset($sysLanguagesData) ? (($lang == 'np') ? $sysLanguagesData->langNep : $sysLanguagesData->langEng) : '';
                                       }
                                       ?>
                                     </td>
                                     <td>
                                      <?php
                                      if ($qualificationsData) {
                                       echo isset($qualificationsData) ? (($lang == 'np') ? $qualificationsData->nameNep : $qualificationsData->nameEng) : '';
                                     }
                                     ?>          
                                   </td>
                                   <!--nameNep nameEng -->
                                   <td>
                                    <?php
                                    if ($fmLivingData) {
                                     echo isset($fmLivingData) ? (($lang == 'np') ? $fmLivingData->nameNep : $fmLivingData->nameEng) : '';
                                   }
                                   ?>
                                 </td>
                                 <td>
                                  <?php
                                  if ($occupationsData) {
                                   echo isset($occupationsData) ? (($lang == 'np') ? $occupationsData->nameNep : $occupationsData->nameEng) : '';
                                 }
                                 ?>
                               </td>
                               <!-- nameNep nameEng-->
                               <td>
                                <!-- maritail  -->
                                sdfadsf
                              </td>
                              <td>
                                <!-- disablity -->
                                sfsdf
                              </td>
                            </tr>
                            <?php 
                                $count = 2;
                               ?>
                            @foreach($getMyFamilyDetails as $familyDetails)

                            <?php
                      //dd($getMyFamilyDetails);
                            $memberDetails = getParentsName($familyDetails->relation_id);
                            $jatJatiData = getTableData('jatjatis', $memberDetails->jatjatis_id);
                            $religiousesData = getTableData('religiouses', $memberDetails->religiouses_id);


                            $sysLanguagesData = getTableData('sys_languages', $memberDetails->sys_languages_id);

                            $qualificationsData = getTableData('qualifications', $memberDetails->qualifications_id);

                            $occupationsData = getTableData('occupations', $memberDetails->occupations_id);

                            $fmLivingData = getTableData('living_fors', $memberDetails->family_living_id);                      
                      //dd($religiousesData);]

                      //dd(isset($jatJatiData));
                            ?>
                            <tr>
                              <td>
                                  {{ $count ++ }}
                                  <a href="#" class="deleteMe" data-url="{{ $memberDetails->idEncrip ? route('citizenInfo.delete', $memberDetails->idEncrip) : '#'  }}" data-name="{{ $lan =='nep' ? $memberDetails->fnameNep .''. $memberDetails->mnameNep .' '.$memberDetails->lnameNep :  $memberDetails->fnameEng .''. $memberDetails->mnameEng .' '.$memberDetails->lnameEng  }}">
                                    <i class="fa fa-trash text-danger"></i>
                                  </a>
                              </td>
                              <td>{{ $familyDetails->relationName }}</td>
                              <td>
                                <a href="{{ $memberDetails->idEncrip ? route('citizenInfo.show', $memberDetails->idEncrip) : '#' }}" target="_blank"> {{ $lan =='nep' ? $memberDetails->fnameNep .''. $memberDetails->mnameNep .' '.$memberDetails->lnameNep :  $memberDetails->fnameEng .''. $memberDetails->mnameEng .' '.$memberDetails->lnameEng   }}
                                </a>
                              </td>
                              <td>{{ isset($memberDetails->gender) ? $memberDetails->gender : '' }}</td>
                              <td>{{ isset($memberDetails->dobAD) ? $memberDetails->dobAD : '' }}</td>
                              <td>                          
                                {{ isset($jatJatiData) ? (($lang == 'np') ? $jatJatiData->nameNep : $jatJatiData->nameEng) : '' }}
                              </td>
                              <td>
                                {{ isset($religiousesData) ? (($lang == 'np') ? $religiousesData->nameNep : $religiousesData->nameEng) : '' }}
                              </td>
                              <td>
                                {{ isset($sysLanguagesData) ? (($lang == 'np') ? $sysLanguagesData->langNep : $sysLanguagesData->langEng) : '' }}                          
                              </td>
                              <td>
                                {{ isset($qualificationsData) ? (($lang == 'np') ? $qualificationsData->nameNep : $qualificationsData->nameEng) : '' }}             
                              </td>
                              <!--nameNep nameEng -->
                              <td>
                                {{ isset($fmLivingData) ? (($lang == 'np') ? $fmLivingData->nameNep : $fmLivingData->nameEng) : '' }}      
                              </td>
                              <td>
                                {{ isset($occupationsData) ? (($lang == 'np') ? $occupationsData->nameNep : $occupationsData->nameEng) : '' }}                                       
                              </td>
                              <!-- nameNep nameEng-->
                              <td>
                                <!-- maritails details -->
                              </td>
                              <td>
                               <!-- disablity -->
                             </td>
                           </tr>
                           @endforeach

                         </tbody>
                       </table>
                       @else
                       <h4>Please insert family details</h4>
                       @endif
                       <br>
                     </div>

                   </div>
                 </div>
               </div>
               <div class="card z-depth-0 bordered">
                <div class="card-header" id="headingTwo"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <h4 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="<?php echo $helpTab =='abroadGoneMem' ? 'true' : 'false'   ?>" aria-controls="collapseTwo">
                      रोजगारीका लागि विदेश गएका विवरण : Abroad
                    </button>
                  </h4>
                </div>
                <div id="collapseTwo" class="collapse <?php echo $helpTab =='abroadGoneMem' ? 'show' : '' ?>" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div class="card-body">
                    <!-- abroad -->
                    <strong>
                     १.यदि परिवारका सदस्य रोजगारीका लागि विदेश गएका छन् भने संख्या, गएको देश र गएको अवधि उल्लेख गर्नुहोस् ।
                   </strong>
                   {{ Form::open(['route'=>'abroadGoneMember','name'=>'frmFamilyAbroadMember', 'class'=>'submitHouseDetails']) }}
                   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
                   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
                   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
                   @include('admin.houseDetails.familyMember.abroad')
                   <p class="p-4">
                    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
                      <span>@lang('commonField.button.back')</span>
                    </button>
                    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
                  </p>
                  {{ Form::close() }}
                </div>
              </div>
            </div>
            <div class="card z-depth-0 bordered">
              <div class="card-header" id="headingThree"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="<?php echo $helpTab =='migration' ? true : false;   ?>" aria-controls="collapseThree">
                <h4 class="mb-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree"
                  aria-expanded="false" aria-controls="collapseThree">
                  वसाई सरी विवरण : Migration
                </button>
              </h4>
            </div>
            <div id="collapseThree" class="collapse <?php echo $helpTab=='migration' ? 'show' : '';   ?>" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                <!-- migration details -->
                {{ Form::open(['route'=>'migrationDetails','name'=>'frmMigrationDetails', 'class'=>'submitHouseDetails']) }}
                {{ Form::hidden('myHouseId',  $hsDetails->id )}}
                {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
                {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
                @include('admin.houseDetails.familyMember.migration')
                <p class="p-2">
                 <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
                   <span>@lang('commonField.button.back')</span>
                 </button>
                 {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
               </p>
               {{ Form::close() }}
             </div>
           </div>
         </div>
         <div class="card z-depth-0 bordered">
          <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
            <h4 class="mb-0">
              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                बच्चा जन्मेको विवरण : Fertility
              </button>
            </h4>
          </div>
          <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
            <div class="card-body">
             <!-- fertility details -->
             {{ Form::open(['route'=>'hsFamilyFertility','name'=>'frmFamilyFertility', 'class'=>'submitHouseDetails']) }}
             {{ Form::hidden('myHouseId',  $hsDetails->id )}}
             {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
             {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
             @include('admin.houseDetails.familyMember.fertility')

             <p class="p-2">
               <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
                 <span>@lang('commonField.button.back')</span>
               </button>
               {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

             </p>
             {{ Form::close() }}

           </div>
         </div>
       </div>
       <div class="card z-depth-0 bordered">
        <div class="card-header" id="headingSix"  data-toggle="collapse" data-target="#collapseSix" aria-expanded="<?php echo $helpTab =='familyDeath' ? true : false;   ?>" aria-controls="collapseSix">
          <h4 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
              मृत्यु भएको विवरण: Family Death
            </button>
          </h4>
        </div>
        <div id="collapseSix" class="collapse <?php echo $helpTab=='familyDeath' ? 'show' : '';   ?>" aria-labelledby="headingSix" data-parent="#accordionExample">
          <div class="card-body">
            <strong>
              तपाइको परिवारमा वितेको १२ महिनामा परिवारका कुनै सदस्य मृत्यु भएको भए सदस्यको विवरण दिनुहोस :
            </strong>
            <!-- death details -->
            {{ Form::open(['route'=>'fmDeathMember','name'=>'frmFmDeathMember', 'class'=>'submitHouseDetails']) }}
            {{ Form::hidden('myHouseId',  $hsDetails->id )}}
            {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
            {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
            @include('admin.houseDetails.familyMember.familyDeath')
            <p class="p-2">
              <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
                <span>@lang('commonField.button.back')</span>
              </button>
              {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

            </p>
            {{ Form::close() }}

          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- accordion ends here --}}
</div>
<!--  -->
<div class="tab-pane fade" id="tab_2">
  <header>
    <i class="fa fa-eye">
      Family Facilities
    </i>
  </header>
  {{-- accordion starts from here --}}

  <div class="accordion" id="accordionExample1">
    <div class="card z-depth-0 bordered">
      <div class="card-header" id="headingSeven"  data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
        <h4 class="mb-0">
          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSeven"
          aria-expanded="true" aria-controls="collapseSeven">
          परिवारले प्रयोग गर्ने खानेपानीको मुख्य स्रोत : Drinking Water
        </button>
      </h4>
    </div>
    <div id="collapseSeven" class="collapse show" aria-labelledby="headingSeven" data-parent="#accordionExample1">
      <div class="card-body">
        {{ Form::open(['route'=>'saveDrinkingWater','name'=>'frmDrinkingWater', 'class'=>'submitHouseDetails']) }}
        {{ Form::hidden('myHouseId',  $hsDetails->id )}}
        {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
        {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
        @include('admin.houseDetails.FamilyFacilities.drinkingWaterFrm')
        <p class="p-4">
          <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
            <span>@lang('commonField.button.back')</span>
          </button>
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

        </p>
        {{ Form::close() }}
      </div>
    </div>
  </div>
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="headingEight"  data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
      <h4 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight"
        aria-expanded="false" aria-controls="collapseEight">
        खाना पकाउन प्रयोग गर्ने मुख्य इन्धन र चुल्हो: Kitchen
      </button>
    </h4>
  </div>
  <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample1">
    <div class="card-body">
      {{ Form::open(['route'=>'houseDetailsInsertKD','name'=>'frmKitchenFrm', 'class'=>'submitHouseDetails']) }}
      @include('admin.houseDetails.FamilyFacilities.kitchenFrm')
      {{ Form::hidden('myHouseId',  $hsDetails->id )}}
      {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
      {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
      <p class="p-4">
        <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
          <span>@lang('commonField.button.back')</span>
        </button>
        {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
      </p>
      {{ Form::close() }}

    </div>
  </div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingNine"  data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
    <h4 class="mb-0">
      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
        बत्तीको मुख्य स्रोत: Light
      </button>
    </h4>
  </div>
  <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample1">
    <div class="card-body">
     {{-- Include Light Form --}}
     {{ Form::open(['route'=>'houseDetailsInsertHL','name'=>'frmLightFrm', 'class'=>'submitHouseDetails']) }}
     @include('admin.houseDetails.FamilyFacilities.lightsFrm')
     {{ Form::hidden('myHouseId',  $hsDetails->id )}}
     {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
     {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
     <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTen" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
    <h4 class="mb-0">
      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTen"
      aria-expanded="false" aria-controls="collapseTen">
      सौचालयको प्रकार : Toilet
    </button>
  </h4>
</div>
<div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionExample1">
  <div class="card-body">
   {{-- Include Toilet Form --}}
   {{ Form::open(['route'=>'houseDetailsInsertHT','name'=>'frmToiletFrm', 'class'=>'submitHouseDetails']) }}
   @include('admin.houseDetails.FamilyFacilities.toiletFrm')
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
  </p>
  {{ Form::close() }}
</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingEleven"  data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
    <h4 class="mb-0">
      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEleven"
      aria-expanded="false" aria-controls="collapseEleven">
      घरबाट निस्कने ठोस फोहरमैला : Waste
    </button>
  </h4>
</div>
<div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordionExample1">
  <div class="card-body">
    {{-- Include Waste Form --}}
    {{ Form::open(['route'=>'houseDetailsInsertHW','name'=>'frmWasteFrm', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.FamilyFacilities.wasteFrm')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}


    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>

      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingtwelve"  data-toggle="collapse" data-target="#collapsetwelve" aria-expanded="false" aria-controls="collapsetwelve">
    <h4 class="mb-0">
      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsetwelve" aria-expanded="false" aria-controls="collapsetwelve">
        घरसम्म पुग्ने बाटोको अवस्था: Road
      </button>
    </h4>
  </div>
  <div id="collapsetwelve" class="collapse" aria-labelledby="headingtwelve" data-parent="#accordionExample1">
    <div class="card-body">
      {{-- Include House Road Form --}}
      {{ Form::open(['route'=>'houseDetailsInsertHR','name'=>'frmRoadFrm', 'class'=>'submitHouseDetails']) }}
      @include('admin.houseDetails.FamilyFacilities.roadFrm')
      {{ Form::hidden('myHouseId',  $hsDetails->id )}}
      {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
      {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
      <p class="p-4">
        <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
          <span>@lang('commonField.button.back')</span>
        </button>

        {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

      </p>
      {{ Form::close() }}

    </div>
  </div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirteen"  data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
    <h4 class="mb-0">
      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThirteen"
      aria-expanded="false" aria-controls="collapseThirteen">
      परिवारले उपभोग गरेको सुविधाहरु : Facilities
    </button>
  </h4>
</div>
<div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordionExample1">
  <div class="card-body">
    <!--  electronic devices -->
    {{ Form::open(['route'=>'houseDetailsInsertED','name'=>'frmFacilitiesFrm', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.FamilyFacilities.facilitiesFrm')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
</div>
{{-- accordion ends here --}}
</div>
<!--  -->
<div class="tab-pane fade" id="tab_3">
  <header>
    <i class="fa fa-eye">
      Children Education
    </i>
  </header>
  {{-- accordion starts from here --}}

  <div class="accordion" id="accordionExample2">
    <div class="card z-depth-0 bordered">
      <div class="card-header" id="headingFifteen"  data-toggle="collapse" data-target="#collapseFifteen"
      aria-expanded="true" aria-controls="collapseFifteen">
      <h4 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFifteen"
        aria-expanded="true" aria-controls="collapseFifteen">
        बालबालिकाको अध्ययन विवरण : child Education
      </button>
    </h4>
  </div>
  <div id="collapseFifteen" class="collapse show" aria-labelledby="headingFifteen" data-parent="#accordionExample2">
    <div class="card-body">
     {{-- Include Waste Form --}}

     {{ Form::open(['route'=>'houseDetailsInsertCE','name'=>'frmChildrenEducationFrm', 'class'=>'submitHouseDetails']) }}
     @include('admin.houseDetails.childrenEducation.childEducation')
     {{ Form::hidden('myHouseId',  $hsDetails->id )}}
     {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
     {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
     <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingSixteen"  data-toggle="collapse" data-target="#collapseSixteen"
  aria-expanded="false" aria-controls="collapseSixteen">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSixteen"
    aria-expanded="false" aria-controls="collapseSixteen">
    घरबाट नजिकको सामुदायिक विद्यालय जान लाग्ने समय विवरण : School Distance
  </button>
</h4>
</div>
<div id="collapseSixteen" class="collapse" aria-labelledby="headingSixteen" data-parent="#accordionExample2">
  <div class="card-body">
    {{-- Include school Distance Form --}}

    {{ Form::open(['route'=>'houseDetailsInsertSD','name'=>'frmSchoolDistanceFrm', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childrenEducation.schoolDistance')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}


    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingSeventeen"  data-toggle="collapse" data-target="#collapseSeventeen"
  aria-expanded="false" aria-controls="collapseSeventeen">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeventeen"
    aria-expanded="false" aria-controls="collapseSeventeen">
    विद्यालय भर्ना नभएका बालबालिकाको विवरण : School Unadmitted
  </button>
</h4>
</div>
<div id="collapseSeventeen" class="collapse" aria-labelledby="headingSeventeen" data-parent="#accordionExample2">
  <div class="card-body">
   {{-- Include School Unadmit Form --}}

   {{ Form::open(['route'=>'houseDetailsInsertSU','name'=>'frmSchoolUnadmitFrm', 'class'=>'submitHouseDetails']) }}
   @include('admin.houseDetails.childrenEducation.unadmitSchool')
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}


   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
    {{ Form::close() }}

  </p>
</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingEighteen" data-toggle="collapse" data-target="#collapseEighteen"
  aria-expanded="false" aria-controls="collapseEighteen">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEighteen"
    aria-expanded="false" aria-controls="collapseEighteen">
    बिचैमा विद्यालय जान छाडेका बालबालिकाहरुको विवरण : Education Drop
  </button>
</h4>
</div>
<div id="collapseEighteen" class="collapse" aria-labelledby="headingEighteen" data-parent="#accordionExample2">
  <div class="card-body">

   {{ Form::open(['route'=>'houseDetailsInsertEDUD','name'=>'frmEducationDropFrm', 'class'=>'submitHouseDetails']) }}
   @include('admin.houseDetails.childrenEducation.educationDrop')
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}


   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

  </p>
  {{ Form::close() }}
</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingNineteen"  data-toggle="collapse" data-target="#collapseNineteen"
  aria-expanded="false" aria-controls="collapseNineteen">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNineteen"
    aria-expanded="false" aria-controls="collapseNineteen">
    विकास केन्द्र वा पूर्व प्रा.वि. जाने बालबालिकाको विवरण : Montessori
  </button>
</h4>
</div>
<div id="collapseNineteen" class="collapse" aria-labelledby="headingNineteen" data-parent="#accordionExample2">
  <div class="card-body">
    {{-- Include child Montessori Form --}}

    {{ Form::open(['route'=>'houseDetailsInsertCM','name'=>'frmChildMontessoriFrm', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childrenEducation.childMontessori')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwenty"  data-toggle="collapse" data-target="#collapseTwenty"
  aria-expanded="false" aria-controls="collapseTwenty">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwenty"
    aria-expanded="false" aria-controls="collapseTwenty">
    बालबालिका बालक्लव⁄संगठनमा आवद्ध विवरण : child Club Involvement
  </button>
</h4>
</div>
<div id="collapseTwenty" class="collapse" aria-labelledby="headingTwenty" data-parent="#accordionExample2">
  <div class="card-body">
    {{-- Include Child Club Form --}}
    {{ Form::open(['route'=>'houseDetailsInsertCCI','name'=>'frmChildClubInvolvementFrm', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childrenEducation.childClubInvolve')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwentyOne"  data-toggle="collapse" data-target="#collapseTwentyOne"
  aria-expanded="false" aria-controls="collapseTwentyOne">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyOne"
    aria-expanded="false" aria-controls="collapseTwentyOne">
    बालबालिकाहरुको रुचीको विवरण : Child Hobby
  </button>
</h4>
</div>
<div id="collapseTwentyOne" class="collapse" aria-labelledby="headingTwentyOne" data-parent="#accordionExample2">
  <div class="card-body">
    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'houseDetailsInsertCH','name'=>'frmChildHobbyFrm', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childrenEducation.childHobby')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
{{-- accordion ends here --}}

</div>
</div>
<!--  -->
<div class="tab-pane fade" id="tab_4">
  <header>
    <i class="fa fa-eye">
      Health and Cleaness
    </i>
  </header>
  {{-- accordion starts from here --}}

  <div class="accordion" id="accordionExample3">
    <div class="card z-depth-0 bordered">
      <div class="card-header" id="headingTwentytwo"  data-toggle="collapse" data-target="#collapseTwentytwo"
      aria-expanded="true" aria-controls="collapseTwentytwo">
      <h4 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwentytwo"
        aria-expanded="true" aria-controls="collapseTwentytwo">
        परिवारको स्वास्थ्य विवरण : Family Health Position
      </button>
    </h4>
  </div>
  <div id="collapseTwentytwo" class="collapse show" aria-labelledby="headingTwentytwo" data-parent="#accordionExample3">
    <div class="card-body">

     {{ Form::open(['route'=>'hsFamilyHealthPosition','name'=>'frmFamiyHealthPosition', 'class'=>'submitHouseDetails']) }}
     {{ Form::hidden('myHouseId',  $hsDetails->id )}}
     {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
     {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
     @include('admin.houseDetails.healthAndCleaness.fmHealthPos')
     <p class="p-4">
       <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
         <span>@lang('commonField.button.back')</span>
       </button>
       {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

     </p>
     {{ Form::close() }}
   </div>
 </div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwentyThree"  data-toggle="collapse" data-target="#collapseTwentyThree"
  aria-expanded="<?php echo $helpTab =='femaleDisease' ? true : false;   ?>" aria-controls="collapseTwentyThree">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyThree"
    aria-expanded="false" aria-controls="collapseTwentyThree">
    महिला रोगहरुको विवरण : Female Disease
  </button>
</h4>
</div>
<div id="collapseTwentyThree" class="collapse <?php echo $helpTab =='femaleDisease' ? 'show' :'';   ?>" aria-labelledby="headingTwentyThree" data-parent="#accordionExample3">
  <div class="card-body">
   {{ Form::open(['route'=>'fmFemaleDisease','name'=>'frmFemaleDisease', 'class'=>'submitHouseDetails']) }}
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

   @include('admin.houseDetails.healthAndCleaness.fmFemaleDisease')
   <p class="p-4">
     <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
       <span>@lang('commonField.button.back')</span>
     </button>
     {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

   </p>
   {{ Form::close() }}
 </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwentyFour"  data-toggle="collapse" data-target="#collapseTwentyFour"
  aria-expanded="false" aria-controls="collapseTwentyFour">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyFour"
    aria-expanded="false" aria-controls="collapseTwentyFour">
    बालबालिका खोप विवरण : infant Vaccine
  </button>
</h4>
</div>
<div id="collapseTwentyFour" class="collapse" aria-labelledby="headingTwentyFour" data-parent="#accordionExample3">
  <div class="card-body">
    {{ Form::open(['route'=>'infantVaccine','name'=>'frmInfantVacc', 'class'=>'submitHouseDetails']) }}
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    @include('admin.houseDetails.healthAndCleaness.infantVaccine')
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwentyFive" data-toggle="collapse" data-target="#collapseTwentyFive"
  aria-expanded="false" aria-controls="collapseTwentyFive">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyFive"
    aria-expanded="false" aria-controls="collapseTwentyFive">
    बालबालिकाको स्वास्थ्य सेवा सम्बन्धी विवरण : Child Health Care
  </button>
</h4>
</div>
<div id="collapseTwentyFive" class="collapse" aria-labelledby="headingTwentyFive" data-parent="#accordionExample3">
  <div class="card-body">

   {{-- Include Child Hobby Form --}}
   {{ Form::open(['route'=>'hsChildHealthCare','name'=>'frmChildHealthCareFrm', 'class'=>'submitHouseDetails']) }}
   @include('admin.houseDetails.healthAndCleaness.fmChildHealthCare')
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

  </p>
  {{ Form::close() }}
</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwentySix"  data-toggle="collapse" data-target="#collapseTwentySix"
  aria-expanded="false" aria-controls="collapseTwentySix">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentySix"
    aria-expanded="false" aria-controls="collapseTwentySix">
    गर्भवती हुँदा क्रियाकलाप विवरण : Pregnancy Activity
  </button>
</h4>
</div>
<div id="collapseTwentySix" class="collapse" aria-labelledby="headingTwentySix" data-parent="#accordionExample3">
  <div class="card-body">

    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsPregnancyAction','name'=>'frmPregActionFrm', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.healthAndCleaness.pregAct')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwentySeven"  data-toggle="collapse" data-target="#collapseTwentySeven"
  aria-expanded="false" aria-controls="collapseTwentySeven">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentySeven"
    aria-expanded="false" aria-controls="collapseTwentySeven">
    सुत्केरी आमाले प्राप्त गरेको स्वास्थ्य सेवा सम्बन्धी विवरण : Preginancy Health Service
  </button>
</h4>
</div>
<div id="collapseTwentySeven" class="collapse" aria-labelledby="headingTwentySeven" data-parent="#accordionExample3">
  <div class="card-body">

    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsPregnancyHealthService','name'=>'frmPregHealthService', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.healthAndCleaness.fmPregHealthService')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwentyEight"  data-toggle="collapse" data-target="#collapseTwentyEight"
  aria-expanded="false" aria-controls="collapseTwentyEight">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyEight"
    aria-expanded="false" aria-controls="collapseTwentyEight">
    एचआइभी संक्रमित आमाबाट जन्मेको बच्चाको विवरण : HIV Preginancy
  </button>
</h4>
</div>
<div id="collapseTwentyEight" class="collapse" aria-labelledby="headingTwentyEight" data-parent="#accordionExample3">
  <div class="card-body">
    {{ Form::open(['route'=>'hsHIVPregnancy','name'=>'frmHIVPregnancy', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.healthAndCleaness.fmHIVPreg')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingTwentyNine"  data-toggle="collapse" data-target="#collapseTwentyNine"
  aria-expanded="false" aria-controls="collapseTwentyNine">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwentyNine"
    aria-expanded="false" aria-controls="collapseTwentyNine">
    खानेपानीको विवरण : Use Drinking Water
  </button>
</h4>
</div>
<div id="collapseTwentyNine" class="collapse" aria-labelledby="headingTwentyNine" data-parent="#accordionExample3">
  <div class="card-body">
    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsFmUsingDrinkingWater','name'=>'frmFmUsingDrinkingWater', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.healthAndCleaness.fmUseDrinkingWater')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirty"  data-toggle="collapse" data-target="#collapseThirty"
  aria-expanded="false" aria-controls="collapseThirty">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThirty"
    aria-expanded="false" aria-controls="collapseThirty">
    हात धुने बानीको विवरण : Hand Wash Process
  </button>
</h4>
</div>
<div id="collapseThirty" class="collapse" aria-labelledby="headingThirty" data-parent="#accordionExample3">
  <div class="card-body">
    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsHandWashProcess','name'=>'frmHandWashProcess', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.healthAndCleaness.fmHandwashProcess')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

      {{ Form::close() }}
    </p>

  </div>
</div>
</div>
</div>
{{-- accordion ends here --}}


</div>
<!--  -->
<div class="tab-pane fade" id="tab_5">
  <header>
    <i class="fa fa-eye">
      Childcare and Involvement
    </i>
  </header>
  {{-- accordion starts from here --}}

  <div class="accordion" id="accordionExample4">
    <div class="card z-depth-0 bordered">
      <div class="card-header" id="headingThirtyOne"  data-toggle="collapse" data-target="#collapseThirtyOne"
      aria-expanded="true" aria-controls="collapseThirtyOne">
      <h4 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThirtyOne"
        aria-expanded="true" aria-controls="collapseThirtyOne">
        बालबालिकाको बाल संरक्षण सम्बन्धी विवरण : Child Security
      </button>
    </h4>
  </div>
  <div id="collapseThirtyOne" class="collapse show" aria-labelledby="headingThirtyOne" data-parent="#accordionExample4">
    <div class="card-body">
      {{-- Include Child Hobby Form --}}
      {{ Form::open(['route'=>'hsChildSecurity','name'=>'frmChildSecurity', 'class'=>'submitHouseDetails']) }}
      @include('admin.houseDetails.childcareAndInvolvement.childSecurity')

      {{ Form::hidden('myHouseId',  $hsDetails->id )}}
      {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
      {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

      <p class="p-4">
        <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
          <span>@lang('commonField.button.back')</span>
        </button>
        {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

      </p>
      {{ Form::close() }}
    </div>
  </div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirtyTwo"  data-toggle="collapse" data-target="#collapseThirtyTwo"
  aria-expanded="false" aria-controls="collapseThirtyTwo">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThirtyTwo"
    aria-expanded="false" aria-controls="collapseThirtyTwo">
    विवाह भएको विवरण : Marriage Details
  </button>
</h4>
</div>
<div id="collapseThirtyTwo" class="collapse" aria-labelledby="headingThirtyTwo" data-parent="#accordionExample4">
  <div class="card-body">
    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsMarriageDetails','name'=>'frmMarriageDetail', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childcareAndInvolvement.fmMarriageDetail')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirtyThree"  data-toggle="collapse" data-target="#collapseThirtyThree"
  aria-expanded="false" aria-controls="collapseThirtyThree">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThirtyThree"
    aria-expanded="false" aria-controls="collapseThirtyThree">
    बालबालिका बसोबास विवरण : Family Living Details
  </button>
</h4>
</div>
<div id="collapseThirtyThree" class="collapse" aria-labelledby="headingThirtyThree" data-parent="#accordionExample4">
  <div class="card-body">
    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsFamilyLivingDetail','name'=>'frmFamilyLiving', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childcareAndInvolvement.fmLivingDetail')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirtyFour" data-toggle="collapse" data-target="#collapseThirtyFour"
  aria-expanded="false" aria-controls="collapseThirtyFour">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThirtyFour"
    aria-expanded="false" aria-controls="collapseThirtyFour">
    अरुको घरको वा आफन्त व्यक्ति बसोबास विवरण : family Other member Living
  </button>
</h4>
</div>
<div id="collapseThirtyFour" class="collapse" aria-labelledby="headingThirtyFour" data-parent="#accordionExample4">
  <div class="card-body">
    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsOtherMemLiveTog','name'=>'frmOtherMemLiveTog', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childcareAndInvolvement.fmOtherMemLiving')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>

<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirtyFive"  data-toggle="collapse" data-target="#collapseThirtyFive"
  aria-expanded="false" aria-controls="collapseThirtyFive">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThirtyFive"
    aria-expanded="false" aria-controls="collapseThirtyFive">
    परिवारको सदस्य महानगरपालिका भित्र काम गर्न गएका विवरण : Family Working Details
  </button>
</h4>
</div>
<div id="collapseThirtyFive" class="collapse" aria-labelledby="headingThirtyFive" data-parent="#accordionExample4">
  <div class="card-body">
    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsFamilyMemWorkingDetail','name'=>'frmFamilyMemWorkDetail', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childcareAndInvolvement.fmWorkersDetail')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>

<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirtySix"  data-toggle="collapse" data-target="#collapseThirtySix"
  aria-expanded="false" aria-controls="collapseThirtySix">
  <h4 class="mb-0">
    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThirtySix"
    aria-expanded="false" aria-controls="collapseThirtySix">
    Child Representative
  </button>
</h4>
</div>
<div id="collapseThirtySix" class="collapse" aria-labelledby="headingThirtySix" data-parent="#accordionExample4">
  <div class="card-body">
    {{-- Include Child Hobby Form --}}
    {{ Form::open(['route'=>'hsFmChildRepresentative','name'=>'frmFamilyChildRepresentative', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.childcareAndInvolvement.fmChildRepresentative')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}

  </div>
</div>
</div>


</div>
{{-- accordion ends here --}}
</div>
<!--  -->
<div class="tab-pane fade" id="tab_6">
 <header>
   <i class="fa fa-eye">
     Economy Condition
   </i>
 </header>
 {{-- accordion starts from here --}}
 <div class="accordion" id="accordionExample5">
  <div class="card z-depth-0 bordered">
    <div class="card-header" id="headingThirtySeven"  data-toggle="collapse" data-target="#collapseThirtySeven"
    aria-expanded="true" aria-controls="collapseThirtySeven">
    <h4 class="mb-0">
      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThirtySeven"
      aria-expanded="true" aria-controls="collapseThirtySeven">
      कृषि कार्यका लागि प्रयोग गरेको जग्गा : Agriculture Details
    </button>
  </h4>
</div>
<div id="collapseThirtySeven" class="collapse show" aria-labelledby="headingThirtySeven" data-parent="#accordionExample5">
  <div class="card-body">
   {{ Form::open(['route'=>'agricultureDetails','name'=>'frmAgricultureDetails', 'class'=>'submitHouseDetails']) }}
   @include('admin.houseDetails.economy.agriDetails')
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

  </p>
  {{ Form::close() }}
</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirtyEight"  data-toggle="collapse" data-target="#collapseThirtyEight" aria-expanded="true" aria-controls="collapseThirtyEight">
    <h4 class="mb-0">
      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThirtyEight"
      aria-expanded="true" aria-controls="collapseThirtyEight">
      वार्षिक बाली उत्पादन तथा बिक्रि विवरण : Agriculture Production
    </button>
  </h4>
</div>
<div id="collapseThirtyEight" class="collapse" aria-labelledby="headingThirtyEight" data-parent="#accordionExample5">
  <div class="card-body">
    {{ Form::open(['route'=>'agricultureProduction','name'=>'frmAgricultureProd', 'class'=>'submitHouseDetails']) }}
    @include('admin.houseDetails.economy.agriProduction')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingThirtyNine"  data-toggle="collapse" data-target="#collapseThirtyNine"
  aria-expanded="true" aria-controls="collapseThirtyNine">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThirtyNine"
    aria-expanded="true" aria-controls="collapseThirtyNine">
    चौपाया तथा पशुपंक्षीहरुको विवरण : Agriculture Animal Details
  </button>
</h4>
</div>
<div id="collapseThirtyNine" class="collapse" aria-labelledby="headingThirtyNine" data-parent="#accordionExample5">
  <div class="card-body">
    {{ Form::open(['route'=>'agricultureAnimalProduction','name'=>'frmAnimalProduct', 'class'=>'submitHouseDetails']) }}      
    @include('admin.houseDetails.economy.agriAnimalDetail')
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}

    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourty"  data-toggle="collapse" data-target="#collapseFourty"
  aria-expanded="true" aria-controls="collapseFourty">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourty"
    aria-expanded="true" aria-controls="collapseFourty">
    माछापालन, मौरीपालन र फूल खेती विवरण : Other Income Source
  </button>
</h4>
</div>
<div id="collapseFourty" class="collapse" aria-labelledby="headingFourty" data-parent="#accordionExample5">
  <div class="card-body">
   {{ Form::open(['route'=>'agriOtherIncomeSource','name'=>'frmOtherIncomeSource', 'class'=>'submitHouseDetails']) }}
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   @include('admin.houseDetails.economy.otherIncomeSource')
   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
  </p>
  {{ Form::close() }}

</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourtyOne"  data-toggle="collapse" data-target="#collapseFourtyOne"
  aria-expanded="true" aria-controls="collapseFourtyOne">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtyOne"
    aria-expanded="true" aria-controls="collapseFourtyOne">
    कृषि उत्पादन विवरण : Yearly Survival
  </button>
</h4>
</div>
<div id="collapseFourtyOne" class="collapse" aria-labelledby="headingFourtyOne" data-parent="#accordionExample5">
  <div class="card-body">
   {{ Form::open(['route'=>'agriYearlySurvival','name'=>'frnYearlySurvival', 'class'=>'submitHouseDetails']) }}
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   @include('admin.houseDetails.economy.AgriYearlySustain')
   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
  </p>
  {{ Form::close() }}

</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourtyTwo"  data-toggle="collapse" data-target="#collapseFourtyTwo"
  aria-expanded="true" aria-controls="collapseFourtyTwo">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtyTwo"
    aria-expanded="true" aria-controls="collapseFourtyTwo">
    घर, उद्योगधन्दा वा व्यवसाय कर विवरण : Tax Pay Details
  </button>
</h4>
</div>
<div id="collapseFourtyTwo" class="collapse" aria-labelledby="headingFourtyTwo" data-parent="#accordionExample5">
  <div class="card-body">
   {{ Form::open(['route'=>'agriTaxPayDetails','name'=>'frmTaxPayDetails', 'class'=>'submitHouseDetails']) }}
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   @include('admin.houseDetails.economy.taxPayment')
   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
  </p>
  {{ Form::close() }}
</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourtyThree"  data-toggle="collapse" data-target="#collapseFourtyThree"
  aria-expanded="true" aria-controls="collapseFourtyThree">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtyThree"
    aria-expanded="true" aria-controls="collapseFourtyThree">
    उद्योग / व्यवसाय / प्रतिष्ठान विवरण : Industry and Business Details
  </button>
</h4>
</div>
<div id="collapseFourtyThree" class="collapse" aria-labelledby="headingFourtyThree" data-parent="#accordionExample5">
  <div class="card-body">
    {{ Form::open(['route'=>'industryBusinessDetail','name'=>'frmindustryBusiness', 'class'=>'submitHouseDetails']) }}   
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

    @include('admin.houseDetails.economy.industryDetails')
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">                
  <div class="card-header" id="headingFiftyFour"  data-toggle="collapse" data-target="#collapseFiftyFour"
  aria-expanded="true" aria-controls="collapseFiftyFour">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFiftyFour"
    aria-expanded="true" aria-controls="collapseFiftyFour">
    प्रतिष्ठान विवरण : organization Details
  </button>
</h4>
</div>
<div id="collapseFiftyFour" class="collapse" aria-labelledby="headingFiftyFour" data-parent="#accordionExample5">
  <div class="card-body">
   {{ Form::open(['route'=>'agriOrgDetails','name'=>'frmOrgDetails', 'class'=>'submitHouseDetails']) }}   
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   @include('admin.houseDetails.economy.organizationDetails')
   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
  </p>
  {{ Form::close() }}
</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourtyFour"  data-toggle="collapse" data-target="#collapseFourtyFour"
  aria-expanded="true" aria-controls="collapseFourtyFour">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtyFour"
    aria-expanded="true" aria-controls="collapseFourtyFour">
    व्यवसायिक सीप तालिम प्राप्त विवरण : Member Training
  </button>
</h4>
</div>
<div id="collapseFourtyFour" class="collapse" aria-labelledby="headingFourtyFour" data-parent="#accordionExample5">
  <div class="card-body">
   {{ Form::open(['route'=>'economicMemTraining','name'=>'frmMemberTraining', 'class'=>'submitHouseDetails']) }}
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   @include('admin.houseDetails.economy.memberTraining')
   <p class="p-4">
    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
      <span>@lang('commonField.button.back')</span>
    </button>
    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
  </p>
  {{ Form::close() }}
</div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourtyFive"  data-toggle="collapse" data-target="#collapseFourtyFive"
  aria-expanded="true" aria-controls="collapseFourtyFive">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtyFive"
    aria-expanded="true" aria-controls="collapseFourtyFive">
    वार्षिक आम्दानी र खर्च विवरण : Income and Expenditure Details
  </button>
</h4>
</div>
<div id="collapseFourtyFive" class="collapse" aria-labelledby="headingFourtyFive" data-parent="#accordionExample5">
  <div class="card-body">
    {{ Form::open(['route'=>'hsIncomeAndExpenses','name'=>'frmIncomeAndExpenses', 'class'=>'submitHouseDetails']) }}
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}

    @include('admin.houseDetails.economy.incomeExpDetail')
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourtySix"  data-toggle="collapse" data-target="#collapseFourtySix"
  aria-expanded="true" aria-controls="collapseFourtySix">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtySix"
    aria-expanded="true" aria-controls="collapseFourtySix">
    ऋण विवरण : loan Details
  </button>
</h4>
</div>
<div id="collapseFourtySix" class="collapse" aria-labelledby="headingFourtySix" data-parent="#accordionExample5">
  <div class="card-body">
    {{ Form::open(['route'=>'agriLoanDetails','name'=>'frmLoanDetails', 'class'=>'submitHouseDetails']) }}
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    @include('admin.houseDetails.economy.loanDetails')
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourtySeven"  data-toggle="collapse" data-target="#collapseFourtySeven"
  aria-expanded="true" aria-controls="collapseFourtySeven">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtySeven"
    aria-expanded="true" aria-controls="collapseFourtySeven">
    संघ संस्था तथा राजनैतिक क्रियाकलापमा संलग्न विवरण : Organization Involvement
  </button>
</h4>
</div>
<div id="collapseFourtySeven" class="collapse" aria-labelledby="headingFourtySeven" data-parent="#accordionExample5">
  <div class="card-body">
    {{ Form::open(['route'=>'agriOrgInvolvement','name'=>'frmOrgInvolvement', 'class'=>'submitHouseDetails']) }}
    {{ Form::hidden('myHouseId',  $hsDetails->id )}}
    {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
    {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
    @include('admin.houseDetails.economy.orgInvolvement')
    <p class="p-4">
      <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
        <span>@lang('commonField.button.back')</span>
      </button>
      {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
    </p>
    {{ Form::close() }}
  </div>
</div>
</div>
</div>

</div>
<!--  -->
<div class="tab-pane fade" id="tab_7">
  <header>
    <i class="fa fa-eye">
      Natural Disaster, Security and Environment
    </i>
  </header>
  {{-- accordion starts from here --}}
  <div class="accordion" id="accordionExample6">
    <div class="card z-depth-0 bordered">
      <div class="card-header" id="headingFourtyEight"  data-toggle="collapse" data-target="#collapseFourtyEight"
      aria-expanded="true" aria-controls="collapseFourtyEight">
      <h4 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtyEight"
        aria-expanded="true" aria-controls="collapseFourtyEight">
        प्राकृतिक विपद वा प्रकोपको समस्या : Problem
      </button>
    </h4>
  </div>
  <div id="collapseFourtyEight" class="collapse show" aria-labelledby="headingFourtyEight" data-parent="#accordionExample6">
    <div class="card-body">
     {{ Form::open(['route'=>'agriNaturalDiseaster','name'=>'frmNaturalDiseaster', 'class'=>'submitHouseDetails']) }}
     {{ Form::hidden('myHouseId',  $hsDetails->id )}}
     {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
     {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
     @include('admin.houseDetails.naturalDisasterSecurityEnv.problems')
     <p class="p-4">
       <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
         <span>@lang('commonField.button.back')</span>
       </button>
       {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
     </p>
     {{ Form::close() }}
   </div>

   <div class="card-body alert alert-default"> <hr>
     {{ Form::open(['route'=>'agriHaveDiseasterProblem','name'=>'frmHaveDiseasterProblem', 'class'=>'submitHouseDetails']) }}
     {{ Form::hidden('myHouseId',  $hsDetails->id )}}
     {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
     {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
     @include('admin.houseDetails.naturalDisasterSecurityEnv.haveNaturalProblem')
     <p class="p-4">
       <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
         <span>@lang('commonField.button.back')</span>
       </button>
       {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
     </p>
     {{ Form::close() }}
   </div>
 </div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFourtyNine"  data-toggle="collapse" data-target="#collapseFourtyNine"
  aria-expanded="true" aria-controls="collapseFourtyNine">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFourtyNine"
    aria-expanded="true" aria-controls="collapseFourtyNine">
    वन्यजन्तुको कारणबाट समस्या : Wild Animal Problem
  </button>
</h4>
</div>
<div id="collapseFourtyNine" class="collapse" aria-labelledby="headingFourtyNine" data-parent="#accordionExample6">
  <div class="card-body">
   {{ Form::open(['route'=>'agriWildAnimalProblem','name'=>'frmWildAnimalProblem', 'class'=>'submitHouseDetails']) }}
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   @include('admin.houseDetails.naturalDisasterSecurityEnv.wildAnimalProblems')
   <p class="p-4">
     <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
       <span>@lang('commonField.button.back')</span>
     </button>
     {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
   </p>
   {{ Form::close() }}
 </div>
</div>
</div>
<div class="card z-depth-0 bordered">
  <div class="card-header" id="headingFifty"  data-toggle="collapse" data-target="#collapseFifty"
  aria-expanded="true" aria-controls="collapseFifty">
  <h4 class="mb-0">
    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFifty"
    aria-expanded="true" aria-controls="collapseFifty">
    अपराधिक घटनाबाट पिडित : Crime Victim
  </button>
</h4>
</div>
<div id="collapseFifty" class="collapse" aria-labelledby="headingFifty" data-parent="#accordionExample6">
  <div class="card-body">
   {{ Form::open(['route'=>'agriCrimeVictim', 'name'=>'frmCrimeVictim', 'class'=>'submitHouseDetails']) }}
   {{ Form::hidden('myHouseId',  $hsDetails->id )}}
   {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
   {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
   @include('admin.houseDetails.naturalDisasterSecurityEnv.crimeVictim')


   <p class="p-4">
     <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
       <span>@lang('commonField.button.back')</span>
     </button>
     {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
   </p>
   {{ Form::close() }}

 </div>
</div>
</div>
     <!--  <div class="accordion" id="accordionExample">
          <div class="card z-depth-0 bordered">
          <div class="card-header" id="headingFiftyOne"  data-toggle="collapse" data-target="#collapseFiftyOne"
          aria-expanded="true" aria-controls="collapseFiftyOne">
            <h4 class="mb-0">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFiftyOne"
                aria-expanded="true" aria-controls="collapseFiftyOne">
              जातजाति, लिगं वा समुदाय विशेष भेदभाव
              </button>
            </h4>
          </div>
          <div id="collapseFiftyOne" class="collapse" aria-labelledby="headingFiftyOne" data-parent="#accordionExample">
            <div class="card-body">
              {{-- @include('admin.houseDetails.naturalDisasterSecurityEnv.discrimination') --}}
            </div>
          </div>
          </div>
        </div> --><div class="card z-depth-0 bordered">
          <div class="card-header" id="headingFiftyTwo"  data-toggle="collapse" data-target="#collapseFiftyTwo"
          aria-expanded="true" aria-controls="collapseFiftyTwo">
          <h4 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFiftyTwo"
            aria-expanded="true" aria-controls="collapseFiftyTwo">
            वातावरणीय समस्या: Environment Problem
          </button>
        </h4>
      </div>
      <div id="collapseFiftyTwo" class="collapse" aria-labelledby="headingFiftyTwo" data-parent="#accordionExample6">
        <div class="card-body">
         {{ Form::open(['route'=>'agriEnvProblem', 'name'=>'frmEnvProb', 'class'=>'submitHouseDetails']) }}
         {{ Form::hidden('myHouseId',  $hsDetails->id )}}
         {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
         {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
         @include('admin.houseDetails.naturalDisasterSecurityEnv.environmentProblem')
         <p class="p-4">
           <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
             <span>@lang('commonField.button.back')</span>
           </button>
           {{ Form::submit(__('commonField.button.create'), ['class' => 'envProblemSubmit btn btn-success pull-right']) }}
         </p>
         {{ Form::close() }}
       </div>
     </div>
   </div>
 </div>
</div>
<div class="tab-pane fade" id="tab_8">
 <header>
   <i class="fa fa-eye">
     House Rentail Details
   </i>
 </header>
 {{-- accordion starts from here --}}
 <div class="accordion" id="accordionExample7">
   <div class="card z-depth-0 bordered">
     <div class="card-header" id="headingFiftyThree"  data-toggle="collapse" data-target="#collapseFiftyThree"
     aria-expanded="true" aria-controls="collapseFiftyThree">
     <h4 class="mb-0">
       <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFiftyThree"
       aria-expanded="true" aria-controls="collapseFiftyThree">
       घर भाडा विवरण : House Rent Details
     </button>
   </h4>
 </div>
 <div id="collapseFourtyEight" class="collapse show" aria-labelledby="headingFourtyEight" data-parent="#accordionExample7">
   <div class="card-body">
     {{ Form::open(['route'=>'agriRentailDetails', 'name'=>'frmRentailDetails', 'class'=>'submitHouseDetails']) }}
     {{ Form::hidden('myHouseId',  $hsDetails->id )}}
     {{ Form::hidden('houseNum',  $hsDetails->hsNum )}}
     {{ Form::hidden('fm_member_id',$hsDetails->citizen_infos_id )}}
     @include('admin.houseDetails.rentalDetails.rental')
     <p class="p-4">
       <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()">
         <span>@lang('commonField.button.back')</span>
       </button>
       {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
     </p>
     {{ Form::close() }}
   </div>
 </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
@section('custom_script')
@include('admin.houseDetails.houseDetailScript')
@include('admin.houseDetails.familyFacilitiesScript')
@include('admin.houseDetails.childrenEducationScript')
@include('admin.houseDetails.healthAndCleanessScript')
@include('admin.houseDetails.childcareAndInvolvementScript')
@include('admin.houseDetails.economyScript')

@endsection

@section('custom_css')
<style type="text/css">
input{
  width: 40px !important;
}
</style>
@endsection
