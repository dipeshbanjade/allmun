@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Arthritis Suppport Recommendation</header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>

	{{ Form::open(['route' => 'admin.munStaffDailyReport.store', 'name' => 'frmMunicipalityStaffDailyReport', 'files' => true]) }}
	@include('admin.municipility.staff.dailyReport._form')
	{{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
	<button type="button" class="btn btn-danger pull-right" onclick="history.back()">BACK
	</button>
	{{ Form::close() }}
</div>

@endsection
@section('customStyle')
<!-- Shifarish CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}
<!-- Shifarish CSS linking -->
@endsection
@section('custom_script')
@include('admin.shifaris.script')
@endsection