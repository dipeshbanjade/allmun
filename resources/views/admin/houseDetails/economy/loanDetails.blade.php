<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
               <?php
                  $loan  =  $loanDetails ? $loanDetails  : '';
                  $jsonLoanTakenFrom = isset($loan->loanTakenFrom) ? json_decode($loan->loanTakenFrom) : '';
                  $jsonloanPurpose   = isset($loan->loanPurpose)   ? json_decode($loan->loanPurpose)   : '';

               ?>
                <div class="form-group col-md-12">
                    <p>
                       १८. तपाईको परिवारमा ऋण छ ? 
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline">
                            १. छ
                            <input type="radio" name="haveLoan" value="1" class="rdoCheckMe" {{ isset($loan->haveLoan) && $loan->haveLoan == 1 ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" name="haveLoan" value="0" class="rdoCheckMe" {{ isset($loan->haveLoan) && $loan->haveLoan == 0 ? 'checked' : '' }}>
                        </label>
                    </div>
                    <p>
                        यदि छ भने ऋण कहाँबाट लिनुभयो ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline">
                            <input type="checkbox" name="loanTakenFrom[]" value="वैंक वा वित्तीय संस्था" class="rdoCheckMe" {{ !empty($loan->loanTakenFrom) && in_array('वैंक वा वित्तीय संस्था', $jsonLoanTakenFrom) ? 'checked' : '' }}>
                             १) वैंक वा वित्तीय संस्था    
                        </label><br /> 
                        <label class="radio-inline">
                            <input type="checkbox" name="loanTakenFrom[]" value="सहकारी संस्था" class="rdoCheckMe"
                            २) सहकारी संस्था
                            {{ !empty($loan->loanTakenFrom) && in_array('सहकारी संस्था', $jsonLoanTakenFrom) ? 'checked' : '' }}>
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="loanTakenFrom[]" value="समूह वा सामुदायिक संस्था " class="rdoCheckMe" {{ !empty($loan->loanTakenFrom) && in_array('समूह वा सामुदायिक संस्था', $jsonLoanTakenFrom) ? 'checked' : '' }}>
                            ३) समूह वा सामुदायिक संस्था 
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="loanTakenFrom[]" value="साहु महाजन" class="rdoCheckMe" {{ !empty($loan->loanTakenFrom) && in_array('साहु महाजन', $jsonLoanTakenFrom) ? 'checked' : '' }}>
                            ४) साहु महाजन
                        </label><br />
                        <label class="radio-inline">
                            {{ Form::text('otherLoanTakenFrom', isset($loan->otherLoanTakenFrom) ? $loan->otherLoanTakenFrom : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य']) }}
                        </label>
                    </div>
                    <p>
                        उक्त ऋण कुन उद्देश्यका लागि लिनु भएको हो ?
                    </p>
                    <!-- ----------------------------------------- -->
                    <div class="col-md-12">
                         <label class="radio-inline">
                            <input type="checkbox" name="loanPurpose[]" value="घरायसी कार्य वा घर खर्च" class="rdoCheckMe" {{ !empty($loan->loanPurpose) && in_array('घरायसी कार्य वा घर खर्च', $jsonloanPurpose) ? 'checked' : '' }}>
                             १) घरायसी कार्य वा घर खर्च            
                        </label><br /> 
                        <label class="radio-inline">
                            <input type="checkbox" name="loanPurpose[]" value="कृषि तथा पशुपालन" class="rdoCheckMe" {{ !empty($loan->loanPurpose) && in_array('कृषि तथा पशुपालन', $jsonloanPurpose) ? 'checked' : '' }}>
                            २) कृषि तथा पशुपालन
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="loanPurpose[]" value="उद्योग व्यापार" class="rdoCheckMe"
                            {{ !empty($loan->loanPurpose) && in_array('उद्योग व्यापार', $jsonloanPurpose) ? 'checked' : '' }}>
                            ३) उद्योग व्यापार
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="loanPurpose[]" value="सामाजिक धार्मिक कार्य" class="rdoCheckMe" {{ !empty($loan->loanPurpose) && in_array('सामाजिक धार्मिक कार्य', $jsonloanPurpose) ? 'checked' : '' }}>
                            ४) सामाजिक धार्मिक कार्य 
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="loanPurpose[]" value="शिक्षा" class="rdoCheckMe"
                            {{ !empty($loan->loanPurpose) && in_array('शिक्षा', $jsonloanPurpose) ? 'checked' : '' }}>
                            ५) शिक्षा
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="loanPurpose[]" value="औषधी उपचार" class="rdoCheckMe" {{ !empty($loan->loanPurpose) && in_array('औषधी उपचार', $jsonloanPurpose) ? 'checked' : '' }}>
                            ६) औषधी उपचार
                        </label><br />
                        <label class="radio-inline">
                            {{ Form::text('otherLoanPurpose', !empty($loan->otherLoanPurpose) ? $loan->otherLoanPurpose : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य']) }}
                        </label>
                    </div>
                </div>
            </div>
            <div class="msgDisplay"></div>
            
    </div>
</div>