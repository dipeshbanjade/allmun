<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWardStaffsTable extends Migration
{
    public function up()
    {
        Schema::create('ward_staffs', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('municipilities_id')->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->text('idEncrip')->nullable();
            
            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->string('refCode', 100)->unique();
            $table->string('profilePic')->nullable();

            $table->text('ward_staffs')->nullable();
            $table->string('citizenNo', 100)->unique()->nullable();


            $table->integer('staff_types_id')->unsigned();
            $table->foreign('staff_types_id')->references('id')->on('staff_types')->onDelete('cascade');

            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

            $table->integer('departments_id')->unsigned();
            $table->foreign('departments_id')->references('id')->on('departments')->onDelete('cascade');

            $table->string('firstNameEng', 100);
            $table->string('firstNameNep', 100);

            $table->string('middleNameEng', 100)->nullable();
            $table->string('middleNameNep', 100)->nullable();

            $table->string('lastNameEng', 100);
            $table->string('lastNameNep', 100);

            $table->integer('provinces_id')->unsigned();
            $table->foreign('provinces_id')->references('id')->on('provinces')->onDelete('cascade');

            $table->integer('districts_id')->unsigned();
            $table->foreign('districts_id')->references('id')->on('districts')->onDelete('cascade');

            $table->date('joingDateNep');
            $table->date('joingDateEng');

            $table->string('citizenImagePath')->nullable();

            $table->string('dob')->nullable();
            $table->string('dobEng')->nullable();
            $table->string('pounchId')->nullable();

            $table->string('wordNo');
            $table->string('villageEng')->nullable();
            $table->string('villageNep');
            $table->string('phoneNumber', 100);
            $table->string('email', 100)->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('ward_staffs');
    }
}
