<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\FamilyRelation;
use App\model\mylogic;
use Validator;
use App\Http\Requests\admin\setting\FamilyRelation\FamilyRelationValidation;

class familyRelationController extends Controller
{
    protected $viewPath  = 'admin.setting.familyRelation';
    protected $familyRelationTbl = 'family_relations';
    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.familyRelationPrefix');
    }

    public function index()
    {
        $page['page_title']       = 'Family Relation';
        $page['page_description'] = 'list of all family relation';

        $allFamilyRelation = FamilyRelation::where(['softDelete' => 0, 'status' => 0])->paginate(config('activityMessage.pagination'));

        $prefix   = myLogic::sysRefCode($tablename = $this->familyRelationTbl, $prefix = $this->prefix);

        return view($this->viewPath . '.index', compact(['page', 'allFamilyRelation', 'prefix'])); 
    }


    public function store(FamilyRelationValidation $request)
    {
        $mySave = FamilyRelation::create([
          'refCode'    => $request->refCode,
          'nameNep'    => $request->nameNep ?? 'N/A',
          'nameEng'    => $request->nameEng,
          'orderColumn'=> $request->orderColumn, 
          'status'  => 0,
          'softDelete' => 0
          ]);
        if ($mySave) {
         return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
     }else{
         return back()->withMessage(config('activityMessage.unSaveMessage'));
     }
 }

 public function edit($id)
 {
   $data = FamilyRelation::findOrFail($id);
   if ($data) {
    return response()->json([
      'success' => true,
      'refCode' => $data->refCode,
      'id'      => $data->id,
      'nameNep' => $data->nameNep,
      'nameEng' => $data->nameEng,
      'orderColumn' => $data->orderColumn
      ]);
}else{
    return response()->json([
      'success' => false,
      'message' => config('activityMessage.dataNotFound')
      ]);
}
}


public function update(Request $request, $id)
{
  $validator = Validator::make($request->all(), [
     'nameEng'  => 'required|min:3|max:50',
     'nameNep'  => 'min:3|max:50',
     ]);

  if ($validator->fails()) {
      return response()->json([
       'success' => false,
       'message' => $validator->errors()->all()
       ]);
  }
  $update          = FamilyRelation::findOrFail($id);
  $update->nameNep = $request->nameNep ?? 'N/A';
  $update->nameEng = $request->nameEng;
  $update->orderColumn = $request->orderColumn;

  $myUpdate = $update->update();
  if ($myUpdate) {
    return response()->json([
      'success' => true,
      'message' => config('activityMessage.updateMessage')
      ]);
}else{
    return response()->json([
      'success'  => false,
      'message'  => config('activityMessage.notUpdated')
      ]);
}
}

public function destroy($id)
{
    $del = FamilyRelation::findOrFail($id);
    $del->softDelete = 1;
    $myDel = $del->update();
    if ($myDel) {
       return MyLogic::successChangeStatus($code = 200);
   }else{
      return MyLogic::successChangeStatus($code = 301);
  }
}
}
