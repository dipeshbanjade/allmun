<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">

        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १.तपाईको परिवारका बिचैमा विद्यालय जान छाडेका ५–१८ बर्ष उमेर समूहका बालबालिकाहरुको संख्या र कारण
                    </p>
                </div>
                <br>
                <table border="1">
                    <thead>
                        <tr>
                            <th>लिङ्ग</th>
                            <th>संख्या	</th>
                            <th>विद्यालय जान छोड्नुका कारणः</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>                                      
                            <td>बालक</td>
                            <td>
                                {{ Form::text('maleChildNum', isset($hsEduDropData->maleChildNum) ? $hsEduDropData->maleChildNum : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '','onchange'=>'checkEduDropReasonMale(this)']) }}
                            </td>
                            <td>
                                @if (isset($whyNotEdu) && count($whyNotEdu)>0)
                                @foreach ($whyNotEdu as $wnEdu)

                                <label>
                                    {{$wnEdu->nameNep}}
                                    <input type="radio" name="maleReasonToLeave" value="{{$wnEdu->nameNep}}" {{ isset($hsEduDropData->maleReasonToLeave) && $hsEduDropData->maleReasonToLeave == $wnEdu->nameNep ? 'checked' : '' }}>

                                </label>

                                @endforeach
                                @else
                                <label>Insert data from the setting</label>
                                @endif
                                {{ Form::text('otherMReasonToLeave',  isset($hsEduDropData->otherMReasonToLeave) ? $hsEduDropData->otherMReasonToLeave : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Other', 'id' => '']) }}
                            </td>

                        </tr>
                        <tr>                                      
                            <td>बालिका</td>
                            <td>
                                {{ Form::text('femaleChildNum',  isset($hsEduDropData->femaleChildNum) ? $hsEduDropData->femaleChildNum : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '','onchange'=>'checkEduDropReasonFemale(this)']) }}
                            </td>
                            <td>
                                @if (isset($whyNotEdu) && count($whyNotEdu)>0)
                                @foreach ($whyNotEdu as $wnEdu)

                                <label>
                                    {{$wnEdu->nameNep}}
                                    <input type="radio" name="femaleReasonToLeave" value="{{$wnEdu->nameNep}}"  {{ isset($hsEduDropData->femaleReasonToLeave) && $hsEduDropData->femaleReasonToLeave == $wnEdu->nameNep ? 'checked' : '' }}>
                                </label>
                                @endforeach
                                @else
                                <label>Insert data from the setting</label>

                                @endif

                                {{ Form::text('otherFReasonToLeave',  isset($hsEduDropData->otherFReasonToLeave) ? $hsEduDropData->otherFReasonToLeave : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Other', 'id' => '']) }}
                            </label>  
                        </td>
                    </tr>                          
                </tbody>
            </table>
            <br>
        </div> 
        <div class="form-group col-md-12">
            <div>
                <p>
                    २.तपाईको परिवारबाट बिद्यालय जान छाडेका बालबालिका सहभागी भएको अनौपचारिक शिक्षा/तालिम लिएको भए निम्न विवरण उल्लेख गर्नुहोस ।                    
                </p>
            </div>
            <br>
            <table border="1">
                <thead>
                    <tr>
                        <th>बालबालिकाको नाम</th>
                        <th>बर्ष</th>
                        <th>अनौपचारिक शिक्षा</th>
                        <th>चेतनामुलक तालिमको नाम</th>
                        <th>सीपमुलक तालिमको नाम</th>
                        <th>विद्यालय भर्नाको लागि उत्प्रेरणा दिने गरेको छ ?</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count = 0;                                 
                    $eduDropTrainingInfo = isset($hsEduDropData->trainingInfo) ? json_decode($hsEduDropData->trainingInfo) : '';
                    ?>  
                    <?php 
                    //Note $getMyFamilyDetails from houseDetailsController
                    foreach($getMyFamilyDetails as $member){
                        $memberInfo = getParentsName($member->relation_id);
                        $memberAge = getAge($memberInfo['dobAD']);
                        if( isset($memberAge) && $memberAge < 19){                        
                            $count++; 
                            $singleEducationDropper = moreHouseDetailsFormUpdate($member->relation_id, $eduDropTrainingInfo, $compareJsonKey = 'trainingId');
                            //dd($singleEducationDropper);
                            ?>
                            <tr>                                      
                                <td>
                                    <input type="hidden" name="trainingInfo[<?php echo $count;  ?>][trainingId]" value="{{ $member->relation_id }}">
                                    <input type="hidden" name="trainingInfo[<?php echo $count;  ?>][trainingName]" value="{{ $member->relationName }}">

                                    {{ Form::text("trainingInfo[$count][childName]", $memberInfo['fnameNep'].' '.$memberInfo['mnameNep'].' '.$memberInfo['lnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name','readonly'=>'', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::text("trainingInfo[$count][age]", $memberAge, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'readonly'=>'', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::text("trainingInfo[$count][training]", isset($singleEducationDropper['training']) ? $singleEducationDropper['training'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::text("trainingInfo[$count][awarenessTraining]",  isset($singleEducationDropper['awarenessTraining']) ? $singleEducationDropper['awarenessTraining'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::text("trainingInfo[$count][skilledTraining]",  isset($singleEducationDropper['skilledTraining']) ? $singleEducationDropper['skilledTraining'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                                </td>
                                <td class="text-center">
                                    <label class="radio-inline">
                                        १.छ   

                                        <input type="radio" name="trainingInfo[<?php echo $count;  ?>][haveInspire]" value="1" {{ isset($singleEducationDropper['haveInspire']) && $singleEducationDropper['haveInspire'] == '1' ? 'checked' : '' }}>
                                    </label> 
                                    <label class="radio-inline">
                                        २.छैन्
                                        <input type="radio" name="trainingInfo[<?php echo $count;  ?>][haveInspire]" value="0" {{ isset($singleEducationDropper['haveInspire']) && $singleEducationDropper['haveInspire'] == '0' ? 'checked' : '' }}>
                                    </label>
                                </td>
                            </tr>
                            <?php }} ?>                                     
                        </tbody>
                    </table>
                    <br>
                </div>    

            </div>
            <div class="msgDisplay"></div>
            
        </div>
    </div>

    <?php
    function getAge($birthDate){
        if(isset($birthDate)){
            $birthDate = date("m-d-Y", strtotime($birthDate));

            $birthDate = explode("-", $birthDate);    
            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
             ? ((date("Y") - $birthDate[2]) - 1)
             : (date("Y") - $birthDate[2]));        
        }else{
            $age = null;
        }
        return $age;
    }
    ?>