<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class temporaryResidentProposalVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:temporary_resident_proposals',
            'chalaniNumber' => 'required|numeric|unique:temporary_resident_proposals',
            'issuedDate' => 'required',
            'residentApplicant' => 'required', 
            'houseOwner' => 'required', 
            'municipalityName' => 'required', 
            'wardNumber' => 'required|numeric', 
            'sabikAddress' => 'required',
            'dateSince' => 'required',
            'nagarikata' => 'required',
            'residentDistrict' => 'required',
            'issuedDateTwo' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
