<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenTypesTable extends Migration
{
    public function up()
    {
        Schema::create('citizen_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode', 100)->unique();
            $table->string('nameEng');
            $table->string('nameNep')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('citizen_types');
    }
}
