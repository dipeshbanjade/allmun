<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class nonProfitOrgVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:non_profit_org_registrations',
            'chalaniNumber' => 'required|unique:non_profit_org_registrations',
            'issuedDate' => 'required',
            'dartaCode' => 'required', 
            'dartaDate' => 'required',
            'companyName' => 'required', 
            'companyAddress' => 'required', 
            'subjectArea' => 'required',
            'businessStartDate' => 'required',
            'companyEmail' => 'required',
            'companyContact' => 'required',
            'operatorName' => 'required',
            'operatorAddress' => 'required',
            'operatorEmail' => 'required',
            'operatorContact' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
