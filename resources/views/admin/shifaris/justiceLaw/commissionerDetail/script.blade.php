<script type="text/javascript">
$(function(){
	/*add buttoin*/
	$("form[name='frmWardcommissionerDetail']").validate({
		rules:{
			municipalityName1 : {				
				minlength : 3,
				maxlength : 50
			},
			municipalityName2 : {				
				minlength : 3,
				maxlength : 50
			},
			wardNumber : {				
				minlength : 1,
				maxlength : 3,
				number : true
			},
			badi : {				
				minlength : 3,
				maxlength : 30
			},
			pratibadi : {				
				minlength : 3,
				maxlength : 30
			},
			caseName : {				
				minlength : 3,
				maxlength : 30
			},
			complaintRequest : {				
				minlength : 10,
				maxlength : 2000
			},
			writtenAnswer : {				
				minlength : 10,
				maxlength : 2000
			},
			agreedOn : {				
				minlength : 3,
				maxlength : 30
			},
			dafaNumber : {				
				minlength : 1,
				maxlength : 30,
				number : true
			},
			municipalityName3 : {				
				minlength : 3,
				maxlength : 30
			},
			badiWard : {				
				number : true,
				minlength : 1,
				maxlength : 3
			},
			badiName : {				
				minlength : 3,
				maxlength : 30
			},
			municipalityName4 : {				
				minlength : 3,
				maxlength : 30
			},
			pratibadiWard : {				
				minlength : 1,
				maxlength : 3,
				number : true
			},
			pratibadiName : {				
				minlength : 3,
				maxlength : 30
			},
			year : {				
				minlength : 2,
				maxlength : 2,
				number : true
			},
			month : {				
				minlength : 1,
				maxlength : 2,
				number : true
			},
			day : {				
				minlength : 1,
				maxlength : 2,
				number : true
			},
			eachday : {				
				minlength : 1,
				maxlength : 5,
				number : true
			}
		},
		messages: {
			municipalityName1 : "Municipality Name is required",
			municipalityName2 : "Municipality Name is required",
			wardNumber : "Ward number should be numeric",
			badi : "Badi is required",
			pratibadi : "Pratibadi is required",
			caseName : "Case Name is required",
			complaintRequest : "Complaint Request is required",
			writtenAnswer : "Written Answer is required",
			agreedOn : "Agreed On is required",
			dafaNumber : "Dafa Number should be numeric",
			municipalityName3 : "Municipality Name is required",
			badiWard : "Badi Ward should be numeric",
			badiName : "Badi Name is required",
			municipalityName4 : "Municipality Name is required",
			pratibadiWard : "Pratibadi Ward should be numeric",
			pratibadiName : "Pratibadi Name is required",
			year : "Year should be numeric",
			month : "Month should be numeric",
			day : "Day should be numeric",
			eachday : "Each Day should be numeric"
		}
	});
});
$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                 dateOfbirth= getMyDob(response.dobAD);
                 // $("input[name='badi']").val(dateOfbirth).trigger('input');
                 // $("input[name='badiName']").val(dateOfbirth).trigger('input');
                $("input[name='complaintRequest']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');

                  // $("input[name='fatherName']").trigger('change');
            
          },complete:function(){
              // setTimeout(location.reload.bind(location), 1500);
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>