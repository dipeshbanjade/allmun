<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class familyHierarchyVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:family_hierarchies',
            'chalaniNum' => 'required|numeric|unique:family_hierarchies',
            'issuedDate' => 'required',
            'sabikAddress' => 'required', 
            'municipalityName' => 'required', 
            'wardNumber' => 'required|numeric', 
            'applicantName' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
