@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
 <div class="card-box">
  <div class="card-body">
    <div class="card-head">
      <header><i class="fa fa-eye">
       &nbsp;@lang('commonField.extra.createProvision')</i>
      </header>
    </div>
   @if(count($allProvince) > 0)
   <div class="table-scrollable" id="dropLabel">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
     <thead>
      <tr>
 <!--       <th>@lang('commonField.extra.sn')</th> -->
        <th>@lang('commonField.extra.sn')</th>
       <th>@lang('commonField.address_information.province')</th>
       <th>@lang('commonField.extra.capital')</th>
       <th>@lang('commonField.extra.governor')</th>
       <th>@lang('commonField.extra.chiefMinister')</th>
       <th>@lang('commonField.address_information.district') </th>
       <th>@lang('commonField.extra.action') </th>
      </tr>
     </thead>
     <tbody>
        @php $count = 1  @endphp
        @foreach($allProvince as $province)
          <tr class="odd gradeX">
           <td>{{ $count ++ }}</td>
           <td >{{ $province->name }}</td>
           <td >{{ $province->capital }}</td>
           <td >{{ $province->governor }}</td>
           <td >{{ $province->chiefMinister }}</td>
           <td >{{ $province->districtNo }}</td>
           <td class="left" width="20%">
             @if(Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun')
              <a href="{{ route('admin.province.edit', $province->id) }}" class="btn btn-primary btn-xs">
               <i class="fa fa-pencil"></i>
              </a>
            <!-- -->
              <!-- <a href="#" class="deleteMe" data-url="{{ route('admin.province.delete', $province->id) }}" id="" >
               <button class="btn btn-danger btn-xs">
               <i class="fa fa-trash-o "></i></button>
             </a> -->
             @else
               <small>@lang('commonField.extra.limited')</small>
             @endif
            </td>
          </tr>
        @endforeach
       </tbody>
      </table>
     </div>
     @endif
    </div>
   </div>
  </div>
@endsection
@section('custom_script')
 @include('admin.setting.province.script')
@endsection