<?php

namespace App\Http\Requests\admin\setting\MigrationReason;

use Illuminate\Foundation\Http\FormRequest;

class MigrationReasonValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:30|unique:migration_reasons',
            'nameEng' => 'required|min:3|max:30'
        ];
    }
}
