<div class="row top-part">
  <div class="col-md-6">
    <p align="left">
      <b class="ps">प. सं.:
        @if(isset($data->refCode))
        {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
        @else
        {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
        @endif
      </b>
    </p>
    <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNum']) }}</b></p>
  </div>
  <div class="col-md-6">
    <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24">
    </p><h4 align="center"><b>विषय: सिफारिस गरिएको बारे।</b></h4>
    {{ Form::hidden('letter_subject', 'सिफारिस गरिएको बारे') }}
    <p></p>
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">श्री 
      {{ Form::text('officeName', 'करदाता सेवा कार्यालय', ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} ,</p>
      <p align="left">  {{ Form::text('officeAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'officeAddress']) }} ।</p>
    </div>
  </div>
  <div class="row content-para">
    <div class="col-md-12">
      <p align="left">उपरोक्त सम्बन्धमा  
        <b>
          {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
        </b> वडा नं.  
        <b>{{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b> (साविक 
        <!--  -->
        {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'organizationAddress']) }}
        <!--  -->
        <b><select onchange="changeSelect(this)" name="organizationType">
          <option value="गा.वि.स." {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
          <option value="नगरपालिका" {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
          <option value="उप महानगरपालिका" {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
          <option value="महानगरपालिका" {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
        </select>
      </b>, वडा नं. 
      {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} )मा बस्ने  
      <b>
        <select onchange="changeSelect(this)" name="title">
          <option value="श्री" {{ !empty($data->title) && $data->title == 'श्री' ? 'selected' : '' }}>श्री</option>
          <option value="सुश्री" {{ !empty($data->title) && $data->title == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
          <option value="श्रीमती" {{ !empty($data->title) && $data->title == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
        </select>
      </b>
      {{ Form::text('name', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
      ले मिति {{ Form::text('panDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'panDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("panDate")']) }} देखि {{ Form::text('shopAddress', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} मा {{ Form::text('shopName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} नामको व्यवसाय संचालन गर्दै आएको हुँदा निजलाई नियम बमोजिम पान न. उपलब्ध गरी दिनुहुन सिफारिस गरिएको व्यहोरा अनुरोध छ।</p>
    </div>
  </div>
  <h2>बोधार्थ&nbsp;  </h2>
  <table class="table table-bordered" id="sabik_address_table">
    <tbody>
      <?php 
      $count = 1;
      $tableData = !empty($data) ? json_decode($data->bodarthaCopy, true) : [' '];
          //dd($tableData);
      foreach($tableData as $singleData){
        $count++;
        ?>
        <tr>
          <td>
            {{ Form::text('bodarthaCopy[]', !empty($singleData) ? $singleData : NULL, ['class'=>'form-control']) }}
          </td>
          @if($count == 2)
          <td class="add-btns" style="border: 0px;">
            <a href="" id="add_more_sabik_address" class="btn btn-success"><span class="fa fa-plus"></span></a>
          </td>
          @else
          <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
          @endif

        </tr>
        <?php } ?>
      </tbody>
    </table>
    <div class="text-right btm-last">
      <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
      <p> <b>
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b></p>
    </div>
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr>
    @include('admin.shifaris.nibedakCommonField')
    <!-- END -->
  </div>
  <div class="col-md-12">
    <hr>
  </div>
</div>
</div>
</div>