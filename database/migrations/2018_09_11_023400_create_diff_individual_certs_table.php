<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiffIndividualCertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diff_individual_certs', function (Blueprint $table) {
            $table->increments('id', 100);

                        $table->string('refCode', 100)->unique();

            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->date('issuedDate');
            $table->string('chalaniNumber');
            $table->string('municipalityName');
            $table->string('wardNumber');
            $table->string('municipalityAddress');
            $table->string('municipalityType');
            $table->string('municipalityWard');
            $table->string('personPrefix');
            $table->string('personName');
            $table->string('citizenshipNumber', 100);
            $table->date('citizenshipDate');
            $table->string('mistakeType');
            $table->string('wrongDetail');
            $table->string('rel_type');            
            $table->string('paperHolder');
            $table->string('paperType');
            $table->string('paperWrongInfo');
            $table->string('paperName');
            $table->string('paperWrongInfo2');
            $table->string('name');
            $table->string('documentName');
            $table->string('documentWrongInfo');
            $table->string('authorizedPerson');            

            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');
            
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diff_individual_certs');
    }
}
