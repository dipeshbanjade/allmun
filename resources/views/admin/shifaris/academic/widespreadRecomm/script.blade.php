<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		// alert('lol');
		$("form[name='frmWardwidespreadRecomm']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum: {
					required : true,
					minlength : 1,
					maxlength : 30					
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNum: {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				organizationAddress : {
					required : true,
					minlength: 3,
					maxlength: 200
				},
				organizationType : {
					required : true
				},
				organizationWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				nameOfChildren : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				childrenTitle : {
					required : true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNum: "Chalani Number is required",
				municipalityName : "Municipality Name is required",
				wardNum : 'Number is required and must be numeric',
				organizationAddress : "Organization Address is required",
				organizationType : "Organization Type is required",
				organizationWard : "Organization Ward is required",
				childrenTitle : "Children Title is required",
				nameOfChildren : "Name of Children is required",
				authorizedPerson : "Authorizing person is required"
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  // $("input[name='fatherName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>