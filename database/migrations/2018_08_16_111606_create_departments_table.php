<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id', 100);

             $table->string('nameNep', 100);
             $table->string('nameEng', 100);

             $table->boolean('status')->default(0);
             $table->boolean('softDelete')->default(0);

            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
