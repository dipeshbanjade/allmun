<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyTypesTable extends Migration
{
    public function up()
    {
        Schema::create('family_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode');
            $table->string('nameNep')->nullable();
            $table->string('nameEng');
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('family_types');
    }
}
