<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class RoomCleaningRecommVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:room_cleaning_recomms',
            'issuedDate' => 'required',
            'organizationOfficer' => 'required',
            'officerName' => 'required',
            'municipalityName' => 'required', 
            'municipalityWard' => 'required', 
            'organizationAddress' => 'required',
            'organizationType' => 'required',
            'organizationWard' => 'required',
            'title' => 'required',
            'nameOfPerson' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'


        ];
    }
}
