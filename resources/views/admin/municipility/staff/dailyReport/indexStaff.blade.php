@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
{{-- Daily Report Log Starts --}}
<div class="col-sm-12 col-md-12">
  <div class="card-box">
    <div class="card-head">
      <header>@lang('commo\nField.extra.dailyReport')</header>
    </div>
    <div class="card-body ">
      <a href="{{ route('munStaffDailyReport.create') }}" class="btn btn-primary pull-right">
        <i class="fa fa-plus"></i>
        @lang('commonField.button.createStaff')
      </a>
      <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
        <!-- table start  -->
        <div class="mdl-tabs__panel is-active p-t-20" id="tab4-panel">
          <div class="table-responsive tableHeight250">
            <table class="table scrollable">
              <tbody>
                <tr>
                  <th>@lang('commonField.extra.sn')</th>
                  <th>@lang('commonField.extra.date')</th>
                  <th>@lang('commonField.personal_information.username')</th>
                  <th>@lang('commonField.extra.subject')</th>
                  <th>@lang('commonField.extra.startTime')</th>
                  <th>@lang('commonField.extra.endTime')</th>
                  <th>@lang('commonField.extra.desc')</th>
                  <th>@lang('commonField.extra.action')</th>
                </tr>
                <tr>
                  <td>1</td>
                  <td>03-01-2017</td>
                  <td>Sunil Thapa</td>
                  <td>Edited</td>                           
                  <td>12:00 AM</td>                           
                  <td>12:00 PM</td>                           
                  <td>This is the description</td>
                  <td>
                    <a class="btn btn-primary btn-xs">
                      <i class="fa fa-eye"></i>
                    </a>
                    <a href="#" class="delete_data" ><button class="btn btn-danger btn-xs">
                      <i class="icon-minus "></i></button></a>   
                    </td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>03-01-2017</td>
                    <td>Sunil Thapa</td>
                    <td>Edited</td>
                    <td>12:00 AM</td>
                    <td>12:00 PM</td>
                    <td>This is the description</td>
                    <td>
                      <a class="btn btn-primary btn-xs">
                        <i class="fa fa-eye"></i>
                      </a>
                      <a href="#" class="delete_data" ><button class="btn btn-danger btn-xs">
                        <i class="icon-minus "></i></button></a>   
                      </td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>03-01-2017</td>
                      <td>Sunil Thapa</td>
                      <td>Edited</td>
                      <td>12:00 AM</td>
                      <td>12:00 PM</td>
                      <td>This is the description</td>
                      <td>
                        <a class="btn btn-primary btn-xs">
                          <i class="fa fa-eye"></i>
                        </a>
                        <a href="#" class="delete_data" ><button class="btn btn-danger btn-xs">
                          <i class="icon-minus "></i></button></a>   
                        </td>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>03-01-2017</td>
                        <td>Sunil Thapa</td>
                        <td>Edited</td>
                        <td>12:00 AM</td>
                        <td>12:00 PM</td>
                        <td>This is the description</td>
                        <td>
                          <a class="btn btn-primary btn-xs">
                            <i class="fa fa-eye"></i>
                          </a>
                          <a href="#" class="delete_data" ><button class="btn btn-danger btn-xs">
                            <i class="icon-minus "></i></button></a>   
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>03-01-2017</td>
                          <td>Sunil Thapa</td>
                          <td>Edited</td>
                          <td>12:00 AM</td>
                          <td>12:00 PM</td>
                          <td>This is the description</td>
                          <td>
                            <a class="btn btn-primary btn-xs">
                              <i class="fa fa-eye"></i>
                            </a>
                            <a href="#" class="delete_data" ><button class="btn btn-danger btn-xs">
                              <i class="icon-minus "></i></button></a>   
                            </td>
                          </tr>
                          <tr>
                            <td>1</td>
                            <td>03-01-2017</td>
                            <td>Sunil Thapa</td>
                            <td>Edited</td>
                            <td>12:00 AM</td>
                            <td>12:00 PM</td>
                            <td>This is the description</td>
                            <td>
                              <a class="btn btn-primary btn-xs">
                                <i class="fa fa-eye"></i>
                              </a>
                              <a href="#" class="delete_data" ><button class="btn btn-danger btn-xs">
                                <i class="icon-minus "></i></button></a>   
                              </td>
                            </tr>
                            <tr>
                              <td>1</td>
                              <td>03-01-2017</td>
                              <td>Sunil Thapa</td>
                              <td>Edited</td>
                              <td>12:00 AM</td>
                              <td>12:00 PM</td>
                              <td>This is the description</td>
                              <td>
                                <a class="btn btn-primary btn-xs">
                                  <i class="fa fa-eye"></i>
                                </a>
                                <a href="#" class="delete_data" ><button class="btn btn-danger btn-xs">
                                  <i class="icon-minus "></i></button></a>   
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {{-- Daily report Log Ends --}}
              @endsection