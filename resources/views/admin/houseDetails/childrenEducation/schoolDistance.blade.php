<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <p>
                    १.तपाईको बसोबास गरेको घरबाट नजिकको सामुदायिक विद्यालय जान लाग्ने समय कति रहेको छ ?
                </p>
                <br>
                <table border="1">
                    <thead>
                        <tr>
                            <th width="50%">अध्ययनरत तह (कक्षा) 	</th>  			
                            <th>१५ मिनेट भन्दा कम</th>
                            <th>१५–३० मिनेट सम्म</th>
                            <th>३०–६० मिनेट सम्म</th>
                            <th>१ घण्टा भन्दा बढी</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php                                 
                        $hsSchoolDistance = isset($hsSDData->educationDistance) ? json_decode($hsSDData->educationDistance) : '';
                        ?>                        

                        <?php $count = 0; ?>
                        @if (isset($educationLevel)&& count($educationLevel)>0)
                        @foreach ($educationLevel as $eLevel)
                        <?php 
                        $count++; 
                        $singleSchoolDistane = moreHouseDetailsFormUpdate($eLevel->id, $hsSchoolDistance, $compareJsonKey = 'levelId');        
                        ?>
                        <tr>
                            <td>
                                <input type="hidden" name="disTime[<?php echo $count;  ?>][levelId]" value="{{ $eLevel->id }}">
                                <input type="hidden" name="disTime[<?php echo $count;  ?>][levelName]" value="{{ $eLevel->nameNep }}">{{ $eLevel->nameNep }}</td>
                                <td class="text-center">                                
                                    <input type="radio" value="15min" name="disTime[<?php echo $count; ?>][value]" {{ !isset($hsSDData) ? 'checked' : ''}}  {{ isset($singleSchoolDistane) && $singleSchoolDistane['value'] == '15min' ? 'checked' : '' }}>
                                </td>
                                <td class="text-center">
                                    <input type="radio" value="15-30min" name="disTime[<?php echo $count; ?>][value]" {{ isset($singleSchoolDistane) && $singleSchoolDistane['value'] == '15-30min' ? 'checked' : '' }}>
                                </td>
                                <td class="text-center">
                                    <input type="radio" value="30-60min" name="disTime[<?php echo $count; ?>][value]" {{ isset($singleSchoolDistane) && $singleSchoolDistane['value'] == '30-60min' ? 'checked' : '' }}>
                                </td>
                                <td class="text-center">
                                    <input type="radio" value="1hrAbove" name="disTime[<?php echo $count; ?>][value]" {{ isset($singleSchoolDistane) && $singleSchoolDistane['value'] == '1hrAbove' ? 'checked' : '' }}>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <label>Please insert data from the setting</label>
                            @endif

                        </tbody>
                    </table>
                    <br>
                </div>     

            </div>
            <div class="msgDisplay"></div>
            
        </div>
    </div>