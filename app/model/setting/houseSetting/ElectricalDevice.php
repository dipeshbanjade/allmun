<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class ElectricalDevice extends Model
{
    protected $table = 'electrical_devices';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
