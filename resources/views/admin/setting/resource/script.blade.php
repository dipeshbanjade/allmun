<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmedithouseWallType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of House Wall Type is required"
            }
        });
    });
    $(function(){
        /*add buttoin*/
        $("form[name='frmhouseWallType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {

                nameEng : "The name of House Wall Type is required"
            }
        });
        var btnAddHouseWall = $('.btnAddHouseWall');
        btnAddHouseWall.on('click', function(e){
          $('#houseWallTypeModel').modal('show'); 
      });

        var btnEdithouseWallType = $('.btnEdithouseWallType');
        btnEdithouseWallType.on('click', function(e) {
        
            e.preventDefault();
            var url = $(this).data('url');
            $.ajax({
             'type': 'GET',
             'url': url,
             success: function (response) {
                $('.houseWallTypeId').val(response.id);
                $('.txtRefCode').val(response.refCode).attr('disabled',true);
                $('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
                $("#edithouseWallTypeModel").modal('show');
            }
        })
        });

        var frmUpdatehouseWallType = $('#frmUpdatehouseWallType');
        frmUpdatehouseWallType.on('submit', function(e){
            e.preventDefault();
            var data = $(this).serialize();
            var id   = $('.houseWallTypeId').val();
            var url  = "{{URL::to('/')}}" + "/setting/houseWall/"+id+"/update";
            $.ajax({
                'type' : 'POST',
                'url'  : url,
                'data'    : data,
                success : function(response){
                    console.log(response);
                    if (response.success==true) {
                        $('.infoDiv').append(response.message).addClass('alert alert-success');
                    }
                    if (response.success==false) {
                        $.each(response.message, function(key, value){
                            $('.infoDiv').append(value).addClass('alert alert-danger');
                        })
                    }
                },complete:function(){
                    $("#edithouseWallTypeModel").modal('hide');
                    setTimeout(location.reload.bind(location), 1500);
                }
            })
            .fail(function (response) {
                alert('data already exists');
            });
            $('#edithouseWallTypeModel').modal('hide');
        });
    });
</script>