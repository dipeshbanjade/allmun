<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १.तपाईको परिवारका बालबालिकाहरुको निम्न विषयमा रुची छ ?   
                    </p>
                    <td>
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" onchange="displayChildHobbyTbl(this)" name="haveHobby" value="1" {{ isset($hsCHData->haveHobby) && $hsCHData->haveHobby == '1' ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" onchange="displayChildHobbyTbl(this)" name="haveHobby" value="0" {{ isset($hsCHData->haveHobby) && $hsCHData->haveHobby == '0' ? 'checked' : '' }} checked="">
                        </label> 
                    </td>
                </div>
                <br>
                <?php
                if(isset($hsCHData->childHobbyData) && $hsCHData->childHobbyData != "null"){
                    //dd($hsCHData);
                    $data = (array) json_decode($hsCHData->childHobbyData);
                    if(!array_key_exists("male", $data)){
                     $data['male'] = [];
                 }else if(!array_key_exists("female", $data)){
                     $data['female'] = [];
                 }
             }else{
                $data['male'] = [];
                $data['female'] = [];
            }
            ?>
               @if (isset($hsCHData->haveHobby) && $hsCHData->haveHobby == '1')
                    <table border="1" class="table table-responsive childHobbyTbl" style="display: block;">
               @else
                    <table border="1" class="table table-responsive childHobbyTbl" style="display: none;">
               @endif
                <thead>
                    <tr>
                        <th>लिङ्ग</th>
                        <th>खेलकूद</th>
                        <th>साहित्य तथा कला</th>
                        <th>लोकगीत/लोकनृत्य</th>
                        <th>विज्ञान तथा प्रविधि</th>
                        <th>रेडियो,टिभी</th>
                        <th>ईमेल,इन्टरनेट</th>
                        <th>पत्रपत्रिका</th>
                        <th>समुदायमा घुलमिल गर्ने</th>
                        <th>अध्ययन गर्ने</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>                                      
                        <td>बालक</td>
                        <td>                                                
                            <input type="checkbox" name="childHobby[male][]" value="game" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'game') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[male][]" value="literature" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'literature') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[male][]" value="songDance" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'songDance') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[male][]" value="science" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'science') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[male][]" value="radioTv" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'radioTv') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[male][]" value="internetMail" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'internetMail') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[male][]" value="news" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'news') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[male][]" value="groupInvolve" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'groupInvolve') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[male][]" value="study" {{ isset($hsCHData->childHobbyData) && isHobby($data['male'], 'study') ? 'checked' : '' }} >
                        </td>
                    </tr>                                
                    <tr>                                      
                        <td>बालिका</td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="game" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'game') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="literature" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'literature') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="songDance" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'songDance') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="science" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'science') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="radioTv" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'radioTv') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="internetMail" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'internetMail') ? 'checked' : '' }} >                            
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="news" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'news') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="groupInvolve" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'groupInvolve') ? 'checked' : '' }} >
                        </td>
                        <td>
                            <input type="checkbox" name="childHobby[female][]" value="study" {{ isset($hsCHData->childHobbyData) && isHobby($data['female'], 'study') ? 'checked' : '' }} >
                        </td>
                    </tr>                                
                </tbody>
            </table>
            <br>
        </div>     

    </div>
    <div class="msgDisplay"></div>
    
</div>
</div>

<?php 
function isHobby($hobbyArray, $hobby){
    if(in_array($hobby, $hobbyArray)){
        return true;
    }else{
        return false;
    }
}
?>