<div class="right_col nep" role="main" style="min-height: 882px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">पुर्जामा घर कायम गर्न सिफारिस</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
         @include('admin.shifaris.municipalityDetail')

         <div class="row top-part">
          <div class="col-md-6">
           <p align="left"><b class="ps">प. सं.:
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
            @endif
          </b></p>
          <p align="left"><b class="cn">च. नं.:
            {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}</b></p>
          </div>
          <div class="col-md-6">
            <p align="right"><b class="mt">मिति :
              {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
            </b></p>
          </div>
        </div>
        <div class="row title-left">
         <div class="col-md-12">
          <p align="left">श्री
            {{ Form::text('accountHolderName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderName', 'required' => 'required']) }}            
            ,
          </p>
          <p align="left">
            {{ Form::text('accountHolderAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderAddress', 'required' => 'required']) }}
          ।</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
          </p><h4 align="center"><b> विषय: सिफारिस सम्बन्धमा।
            <input type="hidden" name="letter_subject" value="सिफारिस सम्बन्धमा">
          </b> </h4>  
          <p></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 content-para">
          <p>उपरोक्त सम्बन्धमा मेरो/हाम्रो नाममा एकलौटी/संयुक्त दर्ता श्रेष्ता भएको वडा नं. <b>
           {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
         </b> (साबिकको ठेगाना 
         {{ Form::text('address', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'address', 'required' => 'required']) }}
         कि.नं. 
         {{ Form::text('kittaNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'kittaNumber', 'required' => 'required']) }}

         
         क्षे.फ. 
         {{ Form::text('area', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'area', 'required' => 'required']) }}
         जग्गाको जग्गाधनी श्रेष्ता पुर्जामा जग्गा मात्र भएको तर फिल्डमा घर भएकोले जग्गाधनी श्रेष्ता पुर्जामा घर कायम गर्न सिफारिस गरी पाऊँ भनी घरजग्गाधनी श्री/सुश्री/श्रीमती 
         
         {{ Form::text('ownerNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'ownerNumber', 'required' => 'required']) }}
         
         ले यस वडा कार्यालयमा निवेदन दिनुभएको हुँदा सो सम्बन्धमा त्यहाँको नियमानुसार जग्गाधनी प्रमाण पुर्जामा घर कायम गरिदिनुहुन सिफारिस गरिन्छ।
       </p>
     </div>
   </div>
   <div class="text-right btm-last">

    <p>
      {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }}
    </p>
    <p> 
      <b> 
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $data->deginations_id == $deg->id? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b> 
    </p>
  </div>
  <!--Nibedak Block  -->

  <hr>

  <!--views for nibedak detail -->

</div>
@include('admin.shifaris.nibedakCommonField')
</div>
</div>
</div>