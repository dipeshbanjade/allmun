<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class hsAgricultureDetails extends Model
{
    protected $table = 'hs_agriculture_details';
}
