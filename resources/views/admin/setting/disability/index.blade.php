@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
 <div class="card-box">
  <div class="card-body">
    <div class="card-head">
      <header><i class="fa fa-eye">
       &nbsp;list of Disable</i>
     </header>
   </div>
   <button id="" class="btnAddDisable btn btn-primary pull-right">
     <i class="fa fa-plus"></i>
     <span>Disable</span>
   </button>

   @if(count($allDisable) > 0)
   <div class="table-scrollable" id="dropLabel">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
     <thead>
      <tr>
       <th>@lang('commonField.extra.sn')</th>
       <th>@lang('commonField.extra.refCode')</th>
       <th>@lang('commonField.houseHoldSetting.houseRoof')</th>
       <th>@lang('commonField.extra.action') </th>
     </tr>
   </thead>
   <tbody>
    @php $count = 1  @endphp
    @foreach($allDisable as $houseRoof)
    <tr class="odd gradeX">
     <td>{{ $count ++ }}</td>
     <td>{{ $houseRoof->refCode }}</td>
     <td >{{ $houseRoof->nameNep ?? 'N/A' }} - {{ $houseRoof->nameEng }} </td>

     <td class="left" >
       
       <a href="#" data-url="{{ route('disable.edit', $houseRoof->id) }}" class="btnEditDisablity btn btn-primary btn-xs" id="" >
         <i class="fa fa-pencil"></i>
       </a>
       <a href="#" class="deleteMe" data-url="{{ route('disable.delete', $houseRoof->id )}}" data-name="{{ $houseRoof->nameEng }}">
        <button class="btn btn-danger btn-xs">
          <i class="fa fa-trash-o "></i>
          </button>
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endif
</div>
</div>
</div>
@endsection
  @section('modelSection')
<div class="modal" id="houseRoofModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;Disablity</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ Form::open(['route' => 'disable.store', 'name'=>'frmDisablity']) }}
        @include('admin.setting.disability._form')
        <div class="col-lg-12 p-t-20 text-right"> 
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
        </div>
        {{ Form::close() }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

      </div>

    </div>
  </div>
</div>


<!-- edit model for houseRoof starts here -->

<div class="modal" id="editDisablityForm">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;Update Disablity</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       {{ Form::open(['name'=>'frmUpdateDisablity', 'id' => 'frmUpdateDisablity']) }}
       @include('admin.setting.disability._form')
       {{ Form::hidden('disableId', null, ['class' => 'disableId']) }}
       <div class="col-lg-12 p-t-20 text-right"> 
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
      </div>
      {{ Form::close() }}
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">

    </div>

  </div>
</div>
</div>
@endsection
@section('custom_script')
<script type="text/javascript">
  $(function(){
    /*add buttoin*/
    $("form[name='frmUpdateDisablity']").validate({
      rules:{
        nameEng : {
          required : true,
          minlength : 3,
          maxlength : 100
        }
      },
      messages: {
        nameEng : "The name of disable required"
      }
    });
  });
  $(function(){
    /*add buttoin*/
    $("form[name='frmDisablity']").validate({
      rules:{

        nameEng : {
          required : true,
          minlength : 3,
          maxlength : 30
        }
      },
      messages: {
        nameEng : "The name of House Roof is required"
      }
    });
    var btnAddDisable = $('.btnAddDisable');
    btnAddDisable.on('click', function(e){
      $('#houseRoofModel').modal('show'); 
    });

    var btnEditDisablity = $('.btnEditDisablity');
    btnEditDisablity.on('click', function(e) {

      e.preventDefault();
      var url = $(this).data('url');
      $.ajax({
        'type': 'GET',
        'url': url,
        success: function (response) {
          $('.disableId').val(response.id);
          $('.txtRefCode').val(response.refCode).attr('disabled',true);
          $('.txtNameNep').val(response.nameNep);
          $('.txtNameEng').val(response.nameEng);
          $("#editDisablityForm").modal('show');
        }
      })
    });

    var frmUpdateDisablity = $('#frmUpdateDisablity');
    frmUpdateDisablity.on('submit', function(e){
      e.preventDefault();
      var data = $(this).serialize();
      var id   = $('.disableId').val();
      var url  = "{{URL::to('/')}}" + "/setting/disable/"+id+"/update";
      $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
          console.log(response);
          if (response.success==true) {
            $('.infoDiv').append(response.message).addClass('alert alert-success');
          }
          if (response.success==false) {
            $.each(response.message, function(key, value){
              $('.infoDiv').append(value).addClass('alert alert-danger');
            })
          }
        },complete:function(){
          $("#editDisablityForm").modal('hide');
          setTimeout(location.reload.bind(location), 1500);
        }
      })
      .fail(function (response) {
        alert('data already exists');
      });
      $('#editDisablityForm').modal('hide');
    });
  });
</script>
@endsection