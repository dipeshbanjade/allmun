<?php

namespace App\model\admin\biomatric;

use Illuminate\Database\Eloquent\Model;

class bioMatric extends Model
{
    protected $table = 'bio_matrics';
    protected $fillable = ['tnxId', 'dvcId', 'dvcIp', 'punchId', 'tnxDateTime', 'mode', 'status', 'softDelete'];
}
