<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १.परिवारमा रहेका पाँच बर्ष भन्दा मुनिका बालबालिकाको बाल संरक्षण सम्बन्धी विवरण
                    </p>

                </div>
                <br>
                <table border="1" class="table ">
                    <thead>
                        <tr>
                            <th>बालक वा बालिकाको नाम</th>
                            <th>उमेर</th>
                            <th>लिङ्ग</th>
                            <th>बालबालिकालाई जन्म दर्ता गरेको छ ?</th>
                            <th>बालबालिकालाई बाल बिकास केन्द्रमा भर्ना गरेको छ ?  </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $count = 0;
                        $childSecurityData = isset($hsCS->childSecurityDetails) ? json_decode($hsCS->childSecurityDetails) : '';
                        //dd($childSecurityData);
                    //Note $getMyFamilyDetails from houseDetailsController
                        foreach($getMyFamilyDetails as $member){
                            $memberInfo = getParentsName($member->relation_id);
                            $memberAge = getAge($memberInfo['dobAD']);
                            if( isset($memberAge) && $memberAge < 5){                        
                                $count++; 
                                $singleChildSecurity = moreHouseDetailsFormUpdate($member->relation_id, $childSecurityData, $compareJsonKey = 'childId');                        
                                ?>
                                <tr>                                      
                                    <td>
                                        <input type="hidden" name="childSecurityDetails[<?php echo $count;  ?>][childId]" value="{{ $member->relation_id }}">

                                        {{ Form::text("childSecurityDetails[$count][childName]", isset($memberInfo) ? $memberInfo['fnameNep'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>
                                    <td>
                                        {{ Form::text("childSecurityDetails[$count][age]", $memberAge, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>
                                    <td>                            
                                        {{ Form::text("childSecurityDetails[$count][gender]", isset($memberInfo) ? $memberInfo['gender'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>
                                    <td>
                                        <label class="radio-inline">
                                            १. छ 
                                            <input type="radio" name="childSecurityDetails[<?php echo $count ?>][haveBirthCert]" value="1" {{ !$singleChildSecurity ? 'checked' : ''}} {{ isset($singleChildSecurity) && $singleChildSecurity['haveBirthCert'] == '1' ? 'checked' : ''}} >
                                        </label> 
                                        <label class="radio-inline">
                                            २. छैन्
                                            <input type="radio" name="childSecurityDetails[<?php echo $count ?>][haveBirthCert]" value="0" {{ isset($singleChildSecurity) && $singleChildSecurity['haveBirthCert'] == '0' ? 'checked' : ''}}>
                                        </label>
                                    </td>
                                    <td>
                                        <label class="radio-inline">
                                            १. छ 
                                            <input type="radio" name="childSecurityDetails[<?php echo $count ?>][haveAdmitChlidEdu]" value="1" {{ !$singleChildSecurity ? 'checked' : ''}} {{ isset($singleChildSecurity) && $singleChildSecurity['haveAdmitChlidEdu'] == '1' ? 'checked' : ''}}>
                                        </label> 
                                        <label class="radio-inline">
                                            २. छैन्
                                            <input type="radio" name="childSecurityDetails[<?php echo $count ?>][haveAdmitChlidEdu]" value="0" {{ isset($singleChildSecurity) && $singleChildSecurity['haveAdmitChlidEdu'] == '0' ? 'checked' : ''}}>
                                        </label>
                                    </td>
                                </tr> 
                                <?php }} ?>                                                            
                            </tbody>
                        </table>
                        <br>
                    </div>     

                </div>
                <div class="msgDisplay"></div>
            </div>
        </div>