<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class commissionerDetailVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'municipalityName1' => 'required',
            'municipalityName2' => 'required',
            'wardNumber' => 'required|numeric',
            'badi' => 'required',
            'pratibadi' => 'required',
            'caseName' => 'required',
            'complaintRequest' => 'required',
            'writtenAnswer' => 'required',
            'agreedOn' => 'required',
            'dafaNumber' => 'required|numeric',
            'municipalityName3' => 'required',
            'badiWard' => 'required|numeric',
            'badiName' => 'required',
            'municipalityName4' => 'required',
            'pratibadiWard' => 'required',
            'pratibadiName' => 'required',
            'year' => 'required|numeric',
            'month' => 'required|numeric',
            'day' => 'required|numeric',
            'eachday' => 'required|numeric',
            'citizenshipNumber' =>'required'
        ];
    }
}
