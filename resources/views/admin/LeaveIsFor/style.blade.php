<style>
.container{
    margin-top: -10px;
}
.clear{
    margin-top: 20px;
}
.wrap{
    padding: 5px;
    background: #ffffff;
    border-radius: 5px;
    box-shadow: 1px 1px 1px 1px #ccc;  
    
}
.wrap h4{
    color: #6672FA;
    font-weight: 600;
    padding: 5px;
}
.table{
    margin-top: -5px;
}
.table .thead-dark th {
    color: #fff;
    background-color: #6672fa;
    border-color: #6672fa;
    padding: 10px !important;
}
.table tbody tr td{
    color: #000;
    background-color: #fff;
    border-color: #ccc;
    padding: 10px !important;
}
a.create-button{
    margin-top: 10px;
}
@media(max-width: 425px){
    .mdl-textfield__label{
        position: relative;
    }    
}

</style>