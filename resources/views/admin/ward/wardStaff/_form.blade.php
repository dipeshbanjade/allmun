<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</a>

  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
    <div class="row">
      <div class="col-sm-12">
        <div class="row pull-right">
        <div class="col-sm-6"><div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
            {{ Form::text('joingDateNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'joining date in nepali', 'id' => 'myDate', 'required', 'title'=>'joining date in nepali']) }}

            <label class = "mdl-textfield__label" >@lang('commonField.extra.joiningDateNep')</label>
          </div>
          </div>
          <div class="col-sm-6"><div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('joingDateEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'id'=> 'myEngDate', 'readonly']) }}

            <label class = "mdl-textfield__label" >@lang('commonField.extra.joiningDateEng')</label>
          </div></div>
        </div>
      </div>
      <div class="col-lg-12"> 
        <!-- profile pic -->
          <p>
              <label>
               <br>
               <img src="{{ isset($data) ? asset($data->profilePic) : '' }}" width="120" height="80" class="img img-thumbnai" id="profilePic"><br>
                <input name="profilePic" type='file' onchange="displayImage(this, 'profilePic');" title="select profile image" />
              </label>
          </p>
      </div>

       <div class="col-lg-12"> 
         <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
          {{ Form::text('email', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'email address', 'title'=>'login email address', 'required'=>true]) }}
          <label class = "mdl-textfield__label" >@lang('commonField.personal_information.email')</label>
        </div>
      </div>


      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
         {{ Form::text('firstNameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'first name in nepali', 'title'=>'first name in nepali', 'required'=>true]) }}
         <label class = "mdl-textfield__label" >@lang('commonField.personal_information.firstNameNep')</label>

       </div>
     </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('middleNameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'middle name in Nepali', 'title'=>'middle name in nepali']) }}
        <label class = "mdl-textfield__label" >@lang('commonField.personal_information.middleNameNep')</label>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('lastNameNep', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'last name in nepali', 'title'=>'last name in nepali', 'required'=>true]) }}
        <label class = "mdl-textfield__label" >@lang('commonField.personal_information.lastNameNep')</label>
      </div>
    </div>

     <div class="col-lg-4"> 
      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('firstNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'First name in english', 'title'=>'first name in english', 'required'=>true]) }}
        <label class = "mdl-textfield__label">@lang('commonField.personal_information.firstNameEng')</label>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('middleNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}

        <label class = "mdl-textfield__label" >@lang('commonField.personal_information.middleNameEng')</label>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('lastNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'lastname in english', 'title'=>'lastname in english', 'title'=>'last name in english']) }}
        <label class = "mdl-textfield__label" >@lang('commonField.personal_information.lastNameEng')</label>
      </div>
    </div>
   
   <div class="col-lg-4"> 
     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width"  style="margin-top: 20px;">
       {{ Form::text('citizenNo', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'citizenship number', 'title'=>'citizenship number']) }}

       <label class = "mdl-textfield__label" >@lang('commonField.extra.citizenNo')</label>
     </div>
   </div>

   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
          <select name="departments_id" class="form-control" required="true" title="select department">
              <option value="">@lang('commonField.extra.department')</option>
              @if(count($deptId) > 0)
                  @foreach($deptId as $dept)
                       <option value="{{ $dept->id }}" {{ isset($data->departments_id) && $data->departments_id == $dept->id ? 'selected' : '' }}>
                            {{ $dept->nameEng }} - {{ $dept->nameNep }} 
                       </option>
                  @endforeach
              @endif
          </select>
     </div>
   </div>

   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
          <select name="roles_id" class="form-control" required="true" title="select users role">
              <option value="">@lang('commonField.extra.selectRole')</option>
              @if(count($roles) > 0)
                  @foreach($roles as $role)
                       <option value="{{ $role->id }}" {{ isset($data->roles_id) && $data->roles_id == $role->id ? 'selected' : '' }}>
                            {{ $role->roleName }}
                       </option>
                  @endforeach
              @endif
          </select>
     </div>
   </div>
   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
           {{ Form::number('pounchId', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'pounch id','title'=>'integrate staff in biomatric']) }}
           <label class = "mdl-textfield__label" >@lang('commonField.employer.linkToBiomatric')</label>
      </div>
   </div>
    @if(!isset($data))
   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
       {{ Form::text('password', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'password', 'required'=>'true', 'title'=>'user password at least 8 character']) }}
       <label class = "mdl-textfield__label" >@lang('commonField.personal_information.password')</label>
     </div>
   </div>
   @endif

   
</div> 
</div>

<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  <div class="card-body row">
    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label id="dropLabel">@lang('commonField.extra.staff_types')</label>
        <select name="staff_types_id" class="form-control">
          <option value="">@lang('commonField.extra.staff_types')</option>
          @if(count($staffTypeId) > 0)
             @foreach($staffTypeId as $staffType)
                <option value="{{ $staffType->id }}" {{ isset($data->staff_types_id)== $staffType->id ? 'selected' : '' }}>
                    {{ $staffType->nameNep }}  - {{ $staffType->nameEng }}
                </option>
             @endforeach
          @endif
        </select>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label id="dropLabel" >@lang('commonField.extra.degination')</label>
        <select name="deginations_id" class="form-control">
          <option value="">@lang('commonField.extra.deginations_id')</option>
          @if(count($deginationId) > 0)
              @foreach($deginationId as $deg)
                 <option value="{{ $deg->id }}" {{ isset($data->deginations_id)== $deg->id ? 'selected' : '' }}>
                        {{ $deg->nameNep }} - {{ $deg->nameEng }}
                 </option>
              @endforeach
          @endif
        </select>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label id="dropLabel">@lang('commonField.extra.provisions')</label>
        <select name="provinces_id" class="form-control">
          <option value="">@lang('commonField.extra.provisions_id')</option>
           @if(count($provincesId) > 0)
               @foreach($provincesId as $provision)
                    <option value="{{ $provision->id }}" {{ isset($data->provinces_id)== $provision->id ? 'selected' : '' }}>
                          {{ $provision->name }}
                    </option>
               @endforeach
           @endif
        </select>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label id="dropLabel">@lang('commonField.address_information.district')</label>
        <select name="districts_id" class="form-control">
          <option value="">@lang('commonField.extra.districts_id')</option>
          @if(count($districtId) > 0)
              @foreach($districtId as $district)
                   <option value="{{ $district->id }}" {{ isset($data->districts_id)== $district->id ? 'selected' : '' }}>
                         {{ $district->districtNameNep }} - {{ $district->districtNameEng }} 
                   </option>
              @endforeach
          @endif
        </select>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width" style="margin-top: 20px;">
        {{ Form::text('wardNo', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'woda number', 'title'=>'woda number']) }}
        <label class = "mdl-textfield__label" >@lang('commonField.address_information.wardNo')</label>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        {{ Form::text('villageNep', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'village name in nepali', 'title'=>'village name in nepali', 'required'=>true]) }}
        <label class = "mdl-textfield__label" >@lang('commonField.extra.villageNep')</label>
      </div>
    </div>

    <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        {{ Form::text('villageEng', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'village name in english', 'title'=>'village name in english', 'required'=>true]) }}

        <label class = "mdl-textfield__label" >@lang('commonField.extra.villageEng') </label>
      </div>
    </div>

   <!--  -->
   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
       {{ Form::text('dob', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'date of birth' , 'id' => 'wrdStaffDob', 'title'=>'date of birth']) }}
       <label class = "mdl-textfield__label" >@lang('commonField.personal_information.dob')</label>
     </div>
   </div>

   <div class="col-lg-4"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
       {{ Form::text('dobEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'date of birth in english' , 'id' => 'wrdStaffEng', 'readonly', 'title'=>'date of birth in english']) }}
       <label class = "mdl-textfield__label" >@lang('commonField.personal_information.dob')</label>
     </div>
   </div>


   <div class="col-lg-4">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
      {{ Form::text('phoneNumber', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'phone number with 10 digit', 'title'=>'phone number with 10 digit']) }}
      
      <label class = "mdl-textfield__label" for = "text5">@lang('commonField.personal_information.phoneNumber')</label>
    </div>                         
  </div>

  <!--  -->
   <div class="col-lg-12">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
     
      <div class="form-group imgCitizen">
        <p>
            <label>
             <br>
             <img src="{{ isset($data) ? asset($data->citizenImagePath) : '' }}" width="120" height="80" class="img img-thumbnai" id="imgCitizen"><br>
              <input name="citizenImagePath" type='file' onchange="displayImage(this, 'imgCitizen');" title="select citizenship" />
            </label>
        </p>
      </div>
      <label class = "mdl-textfield__label" for = "text5">@lang('commonField.extra.citizenImage')</label>
    </div>                         
  </div>
</div>
</div>
</div>