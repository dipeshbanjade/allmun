<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditareaType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of area type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmareaType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of area type is required"
            }
        });
	var btnAddareaType = $('.btnAddareaType');
	btnAddareaType.on('click', function(e){
		$('#areaTypeModel').modal('show'); 
	});

	var btnEditareaType = $('.btnEditareaType');
	btnEditareaType.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.areaTypeId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editareaTypeModel").modal('show');
			}
		})
	});

	var frmUpdateareaType = $('#frmUpdateareaType');
  frmUpdateareaType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.areaTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/areaType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editareaTypeModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editareaTypeModel').modal('hide');
});
});
</script>