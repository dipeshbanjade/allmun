<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class LivingFor extends Model
{
    protected $table = 'living_fors';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
