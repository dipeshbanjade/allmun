<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditbusinessType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 100
                }
            },
            messages: {
                nameEng : "The name of Business Type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmbusinessType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 100
                }
            },
            messages: {
          
                nameEng : "The name of Business Type is required"
            }
        });
	var btnAddBusinessType = $('.btnAddBusinessType');
	btnAddBusinessType.on('click', function(e){
		$('#businessTypeModel').modal('show'); 
	});

	var btnEditbusinessType = $('.btnEditbusinessType');
	btnEditbusinessType.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.businessTypeId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editbusinessTypeModel").modal('show');
			}
		})
	});

	var frmUpdatebusinessType = $('#frmUpdatebusinessType');
  frmUpdatebusinessType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.businessTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/businessType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editbusinessTypeModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editbusinessTypeModel').modal('hide');
});
});
</script>