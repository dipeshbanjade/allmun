<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveTypesTable extends Migration
{
    public function up()
    {
        Schema::create('leave_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refCode', 100);
            $table->string('nameNep');
            $table->string('nameEng')->nullable();
            $table->string('daysLeave');
            $table->boolean('status');
            $table->boolean('softDelete');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('leave_types');
    }
}
