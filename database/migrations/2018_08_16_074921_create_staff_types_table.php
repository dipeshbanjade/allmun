<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTypesTable extends Migration
{
    public function up()
    {
        Schema::create('staff_types', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nameNep', 100);
            $table->string('nameEng', 100);

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('staff_types');
    }
}
