<div class="right_col nep" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">व्यवसाय दर्ता</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
         @include('admin.shifaris.municipalityDetail')
         <div class="row top-part">
          <div class="col-md-6">
           <p align="left"><b class="ps">प. सं.:
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
            @endif
          </b></p>
          <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNumber']) }}
          </b></p>
          <p align="left"><b>दर्ता नं. : {{ Form::text('registrationNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'id' => 'registrationNumber']) }}
          </b></p>
          <p align="left"><b>दर्ता मिति :{{ Form::text('registrationDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'registrationDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("registrationDate")']) }}
          </b></p>
        </div>
        <div class="col-md-6">
          <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
          </p><h4 align="center"><b>विषय :<u>व्यवसाय दर्ता प्रमाण पत्र ।</u></b></h4>
        </div>
      </div>
      <div class="row content-para">
        <div class="col-md-12">
          <div class="col-md-6">
            <p align="left">व्यवसाय/फर्म को नाम :{{ Form::text('businessFarmName', null, ['class'=>'dashed-input-field', 'id' => 'businessFarmName']) }}
            </p>
            <p align="left">व्यवसायीको नाम : {{ Form::text('businessOwnerName', null, ['class'=>'dashed-input-field', 'id' => 'businessOwnerName']) }}
            </p>
            <p align="left">घरधनीको नाम : {{ Form::text('houseOwnerName', null, ['class'=>'dashed-input-field', 'id' => 'houseOwnerName']) }}
            </p>
            <p align="left">व्यवसाय रहेको स्थान :  {{ Form::text('businessLocation', null, ['class'=>'dashed-input-field', 'id' => 'businessLocation']) }}
            </p>
            <p align="left">व्यवसाय रहेको बाटोको नाम : {{ Form::text('businessStreetName', null, ['class'=>'dashed-input-field', 'id' => 'businessStreetName']) }}
            </p>
            <p align="left">व्यवसाय रहेको घर नं. : {{ Form::text('businessHouseNumber', null, ['class'=>'dashed-input-field', 'id' => 'businessHouseNumber']) }}
            </p>
            <p></p>
            <p align="left">व्यवसायको इमेल : {{ Form::text('businessEmail', null, ['class'=>'dashed-input-field', 'id' => 'businessEmail']) }}
            </p>
            <p align="left">फोन नम्बर : {{ Form::text('businessPhoneNumber', null, ['class'=>'dashed-input-field', 'id' => 'businessPhoneNumber']) }}
            </p>
            <p align="left">प्यान नम्बर : {{ Form::text('businessPanNumber', null, ['class'=>'dashed-input-field', 'id' => 'businessPanNumber']) }}
            </p>
          </div>

          <div class="col-md-6">
            <div class="img-box"><p>निवेदकको दुवै कान देखिने पासपोर्ट साईजको फोटो</p></div>
          </div>

          <div class="col-md-6">
            <p><b>अन्य निकायमा दर्ता भएको छ भने</b></p>
            <p>निकाय : {{ Form::text('organizationName', null, ['class'=>'dashed-input-field', 'id' => 'organizationName']) }}</p>
            <p>दर्ता नम्बर : {{ Form::text('organizationRegisterNumber', null, ['class'=>'dashed-input-field', 'id' => 'organizationRegisterNumber']) }}</p>
            <p>मिति : {{ Form::text('organizationRegisterDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'organizationRegisterDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("organizationRegisterDate")']) }}</p>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-bordered" id="business_close_table">
              <thead>
                <tr><th>क्र. सं.</th>
                  <th>व्यवसायको प्रकार</th>
                  <th>व्यवसायको प्रकृति</th>
                  <th>वडा नं.</th>
                  <th>दर्ता नं.</th>
                  <th>टोलको नाम</th>
                  <th>बाटोको नाम</th>
                  <th>घर नं.</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $count = 1;
                $tableData = !empty($data) ? json_decode($data->tableData, true) : [1];
                //dd($tableData);
                foreach($tableData as $singleData){         
                  ?>
                  <tr>
                    <td>
                      <?php $count++ ?>
                      {{ Form::text('', '१', ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('businessType[]', !empty($singleData['businessType']) ? $singleData['businessType'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('businessNature[]', !empty($singleData['businessNature']) ? $singleData['businessNature'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('wardNo[]', !empty($singleData['wardNo']) ? $singleData['wardNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('registrationNo[]', !empty($singleData['registrationNo']) ? $singleData['registrationNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('tollName[]', !empty($singleData['tollName']) ? $singleData['tollName'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('streetName[]', !empty($singleData['streetName']) ? $singleData['streetName'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('houseNo[]', !empty($singleData['houseNo']) ? $singleData['houseNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>

                    @if($count == 2)                    
                    <td class="add-btns" style="border: 0px;">
                      <button type="button" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button>
                    </td>
                    @else
                    <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                    @endif
                  </tr>
                  <?php } ?>

                </tbody>
              </table>
            </div>
            <p style="font-size: 10px;">नेटः यो प्रमाणपत्र स्थानीय स्वायत्तशासन ऐन २०५५ को दफा १३८ बमोजिम 
              {{ Form::text('courtName', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
            बाट जारी गरेको आदेश निर्दैशन र सुचनाको पालन गर्ने जारी गरिएको छ ।</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6">
              {{ Form::text('', null, ['class'=>'dashed-input-field']) }}
              <p>व्यवसायीको दस्तखत</p>
            </div>
            <div class="text-right col-md-6">
              <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
              <p> <b>       
                <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
                  @if(count($deginationsId) > 0)
                  @foreach($deginationsId as $deg)
                  <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                    {{ $deg->nameNep }} 
                  </option>
                  @endforeach
                  @endif
                </select>
              </b> </p>
            </div>
          </div>
        </div>
        <!--views for nibedak detail -->
        <div class="clearfix"></div>
        <hr>
        @include('admin.shifaris.nibedakCommonField')
        <!-- END -->
      </div>
      <div class="col-md-12">
        <hr>
      </div>
    </div>
  </div>
</div>