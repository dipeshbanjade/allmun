<script type="text/javascript">
    var onScan = $('.onScan');
    var addButton = $('#btnAddTableRow');
    var clickCount = 1;
    addButton.on('click', function(e){
        e.preventDefault();
        clickCount++;

        var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

        var name = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="name[]">';

        var nata = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="nata[]">';

        var gharNo = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="gharNo[]">';

        var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

        var insertRow = "<tr class='tblRow'>"
        +"<td>"+serialNumber+"</td>"

        +"<td>"+name+"</td>"
        +"<td>"+nata+"</td>"
        +"<td>"+gharNo+"</td>"

        +"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
        +"</tr>";

        $('#scholarship_table').append(insertRow);

        $('.btnRemoveItem').on('click', function(e){
            $(this).parent().parent().remove();
            return false;
        });
    });

    $(function(){
        /*add buttoin*/
        $("form[name='frmWardscholarshipRecomm']").validate({
            rules:{
                refcode: {
                    required: true,
                    minlength: 3,
                    maxlength: 30
                },
                issuedDate : {
                    required : true,
                    date : true
                },
                chalaniNum : {
                    required : true,
                    minlength : 1,
                    maxlength : 30                  
                },
                municipalityName : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                },
                organizationAddress : {
                    required : true,
                    minlength: 3,
                    maxlength: 200
                },
                organizationType : {
                    required : true
                },
                organizationWard : {
                    required : true,
                    minlength : 1,
                    maxlength : 3,
                    number: true
                },
                wardNum : {
                    required : true,
                    minlength : 1,
                    maxlength : 3,
                    number: true
                },
                fatherName : {
                    required : true,
                    minlength : 3,
                    maxlength : 255
                },
                motherName : {
                    required : true,
                    minlength : 3,
                    maxlength : 255
                },
                economyStatus : {
                    required : true
                },
                childrenGender : {
                    required : true
                },
                childrenTitle : {
                    required : true
                },
                nameOfChildren : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                },
                authorizedPerson : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                refcode: "Patra Sankya is required",
                issuedDate : "Issue Date is required",
                chalaniNum : "Chalani Number is required",
                municipalityName : "Municipality Name is required",
                organizationAddress : "Organization Address is required",
                organizationType : "Organization Type is required and must be numeric",
                organizationWard : "Organization Ward is required and must be numeric",
                economyStatus : "Economy Status is required",
                fatherName : "Father Name is required",
                motherName : "Mother Name is required",
                childrenGender : "Children Gender is required",
                childrenTitle : "Children Title is required",
                nameOfChildren : "Name of Children is required",
                authorizedPerson : "Authorizing person is required"
            }
        });
    });
    /*---------------*/
    // var doc = new jsPDF();
    //    var specialElementHandlers = {
    //        '#editor': function (element, renderer) {
    //            return true;
    //        }
    //    };

      // $('#cmd').click(function () {
      //      doc.fromHTML($('#content').html(), 15, 15, {
      //          'width': 170,
      //              'elementHandlers': specialElementHandlers
      //      });
      //      doc.save('sample-file.pdf');
      //  });

      $('#nebedakId').on('change',function(){
        nebedakId = $(this).val();
        if (nebedakId.length >0 ) {
            var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
            $.ajax({
                'type' : 'GET',
                'url'  : url,
                success : function(response){
                    console.log(response);
                        $("input[name='fatherName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
                  
                },complete:function(){
                }
            })
            .fail(function (response) {
                alert('data not found ');
            });
        }else{

        }
      });
</script>