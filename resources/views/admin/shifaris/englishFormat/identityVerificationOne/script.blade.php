<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardidentityVerificationOne']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDate : {
					required : true,
					date :true 
				},
				letterSub : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				appName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				appTitle : {
					required : true					
				},
				municipality : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				ward : {
					required : true,
					number :true
				},
				district : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				former : {
					required : true
				},
				orgAddr : {
					required : true,
					minlength : 3,
					maxlength : 30 
				},
				orgType : {
					required : true
				},
				orgWard : {
					required : true,
					minlength : 3,
					maxlength : 30 
				},	
				docName : {
					required : true,
					minlength : 3,
					maxlength : 30 
				},	
				docNo : {
					required : true,
					number :true
				},	
				userName : {
					required : true,
					minlength : 3,
					maxlength : 30 
				},	
				userNameTwo : {
					required : true,
					minlength : 3,
					maxlength : 30 
				},	
				docNameTwo : {
					required : true,
					minlength : 3,
					maxlength : 30 
				},	
				docNoTwo : {
					required : true,
					number :true
				},
				docDate : {
					required : true,
					date :true
				},
				genderType : {
					required : true
				},
				genderTypeTwo : {
					required : true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30 
				},
				deginations_id : {
					required : true
				}
			},
			messages: {
				refCode : "The refCode field with minimum 3 character is required",
				issuedDate : "issuedDate must be in date format",
				letterSub : "The letter Subject is required",
				appName : "The applicant name is required",
				appTitle : "The applicant title is required",
				municipality : "The municipality field is required",
				ward : "The ward field is required with only numeric character",
				district : "The district field is required ",
				former : "The former field is required",
				orgAddr : "The organization address field is required",
				orgType : "The organization type field is required",
				orgWard : "The organization ward field is required",
				docName : "The docName is required",
				docNo : "The docNo field is required with only number character",
				userName : "The username field is required",
				userNameTwo : "The userNameTwo field is required",
				docNameTwo : "The docnameTwo field is required",
				docNoTwo : "The docNoTwo field is required with only number character",
				docDate : "The docDate field is required",
				genderType : "The genderType field is required",
				genderTypeTwo : "The genderTypeTwo is required",
				authorizedPerson : "The authorizedPerson field is required",
				deginations_id : "The name of degination is required"
			}
		});

	});


	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='appName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>