@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
 <div class="card-box">
  <div class="card-body">
    <div class="card-head">
      <header><i class="fa fa-eye">
       &nbsp;@lang('commonField.extra.familyRelation')</i>
     </header>
   </div>
   <button id="" class="btnAddFamilyReal btn btn-primary pull-right">
     <i class="fa fa-plus"></i>
     <span> @lang('commonField.extra.createFamilyRelation')</span>
   </button>

   @if(count($allFamilyRelation) > 0)
   <div class="table-scrollable" id="dropLabel">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
     <thead>
      <tr>
       <th>@lang('commonField.extra.sn')</th>
       <th>@lang('commonField.extra.refCode')</th>
       <th>@lang('commonField.extra.familyRelation')</th>
       <th>@lang('commonField.houseHoldSetting.orderColumn')</th>
       <th>@lang('commonField.extra.action') </th>
     </tr>
   </thead>
   <tbody>
    @php $count = 1  @endphp
    @foreach($allFamilyRelation as $familyRelation)
    <tr class="odd gradeX">
     <td>{{ $count ++ }}</td>
     <td>{{ $familyRelation->refCode }}</td>
     <td >{{ $familyRelation->nameNep ?? 'N/A' }} - {{ $familyRelation->nameEng }} </td>
     <td>{{ $familyRelation->orderColumn }}</td>

     <td class="left" >
       @if(Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun')
       <a href="#" data-url="{{ route('familyRelation.edit', $familyRelation->id) }}" class="btnEditfamilyRelation btn btn-primary btn-xs" id="" >
         <i class="fa fa-pencil"></i>
       </a>
       <a href="#" class="deleteMe" data-url="{{ route('familyRelation.delete', $familyRelation->id )}}" data-name="{{ $familyRelation->nameEng }}">
        <button class="btn btn-danger btn-xs">
          <i class="fa fa-trash-o "></i>
          </button>
        </a>
        @else
        <small>@lang('commonField.extra.limited')</small>
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endif
</div>
</div>
</div>
@endsection
  @section('modelSection')
<div class="modal" id="familyRelationModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.addFamilyRelation')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ Form::open(['route' => 'familyRelation.store', 'name'=>'frmfamilyRelation']) }}
        @include('admin.setting.familyRelation._form')
        <div class="col-lg-12 p-t-20 text-right"> 
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
        </div>
        {{ Form::close() }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

      </div>

    </div>
  </div>
</div>


<!-- edit model for familyRelation starts here -->

<div class="modal" id="editfamilyRelationModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.editFamilyRelation')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       {{ Form::open(['name'=>'frmeditfamilyRelation', 'id' => 'frmUpdatefamilyRelation']) }}
       @include('admin.setting.familyRelation._form')
       {{ Form::hidden('familyRelationId', null, ['class' => 'familyRelationId']) }}
       <div class="col-lg-12 p-t-20 text-right"> 
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
      </div>
      {{ Form::close() }}
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">

    </div>

  </div>
</div>
</div>

<!-- edit model for familyRelation ends here -->
@endsection

@section('custom_script')
@include('admin.setting.familyRelation.script')
@endsection