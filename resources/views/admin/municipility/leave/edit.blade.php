@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
   <div class="col-sm-12 col-md-12">
     <div class="card-box">
        <div class="card-head">
        	<header>
        		<h3>@lang('sidebarMenu.system_setting.municipility')</h3>
        		<i class="fa fa-edit">
        			@lang('commonField.extra.edit')
        		</i><br>
        	</header>
        </div>
       <div class="card-body">
          {!! Form::model($data, ['method' => 'POST','route' => ['admin.munStaffLeave.update', $data->id]]) !!}
                 @include('admin.municipility.leave._form')
                  <p class="pull-right">
                  	   {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success pull-right']) }}
                  	  <button type="button" class="btn btn-danger pull-right" onclick="history.back()">
                  	      @lang('commonField.button.back')
                  	  </button>
                  </p>
            {{ Form::close() }}
       </div>
       </div>
     </div>
@endsection