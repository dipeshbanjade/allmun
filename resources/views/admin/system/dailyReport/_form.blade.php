<div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="form-group"> 
                <label>
                    @lang('commonField.extra.subject')
                </label>
                {{ Form::text('sub', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
            </div>
            <div class="form-group"> 
                <label >@lang('commonField.extra.date')</label>
                {{ Form::text('date', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'id' => 'currNepDate']) }}
            </div>
            <div class="form-group"> 
                <label>
                @lang('commonField.extra.startTime')</label>
                {{ Form::time('startTime', null, ['class'=>'mdl-textfield__input time_input', 'placeholder'=>'', 'type' => 'time']) }}
            </div>
            <div class="form-group"> 
                <label >@lang('commonField.extra.endTime')</label>
               {{ Form::time('endTime', null, ['class'=>'mdl-textfield__input time_input', 'placeholder'=>'', 'type' => 'time']) }}
            </div>
        </div>
        <div class="col-md-8  col-sm-12"> 
            <!-- summernote -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <a class="changeLanguage dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  data-url="{{ route('changeLanguage', 'en') }}" data-val="en">
                        <img src="{{ asset('lib/img/gb.png') }}" class="position-left" alt="english logo" title="English">
                </a>
                <a class="changeLanguage dropdown-toggle flag" data-toggle="dropdown" aria-expanded="false" data-url="{{ route('changeLanguage', 'np') }}" data-val="np">
                    <img src="{{ asset('lib/img/flag.png') }}" class="position-left" alt="Nepal logo" title="Nepali">
                </a>
                

                        <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                           {{ Form::textarea('desc', null, ['class'=>'summernote mdl-textfield__input', 'placeholder'=>'', 'rows' => '3']) }}
                            <label class = "mdl-textfield__label" for = "text7">@lang('commonField.extra.desc')</label>
                        </div>
                </div>
            </div>
</div>

