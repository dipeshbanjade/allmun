<?php

namespace App\Http\Requests\admin\setting\province;

use Illuminate\Foundation\Http\FormRequest;

class provinceValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:30',
            'capital' => 'required|min:3|max:30',
            'governor' => 'required|min:3|max:30',
            'chiefMinister' => 'required|min:3|max:30',
            'districtNo' => 'required|max:30'
        ];
    }
}
