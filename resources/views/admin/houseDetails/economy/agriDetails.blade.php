<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
           <?php 
             $updateAgriDetails = $agriDetails;    
             $agriDetailsJson = isset($updateAgriDetails->hsAgriDetails) ? json_decode($updateAgriDetails->hsAgriDetails) : '';  
             // dd($agriDetailsJson);
           ?>
            <div class="row color_red">
                <div class="form-group col-md-12">
                    <p>
                       १. तपाईको परिवारले कृषि कार्यका लागि जग्गा प्रयोग गरेको छ ?
                    </p>

                    <div class="col-md-12">
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" onchange="displayHaveAgriTbl(this)" name="haveAgriculture" value="1" class="rdoClickMe" {{ isset($updateAgriDetails) &&  $updateAgriDetails->haveAgriculture == '1' ? 'checked' : ''  }}>
                        </label> 
                        <label class="radio-inline" class="rdoClickMe">
                            २. छैन्
                            <input type="radio" onchange="displayHaveAgriTbl(this)" name="haveAgriculture" value="0" class="rdoClickMe" {{ isset($updateAgriDetails) &&  $updateAgriDetails->haveAgriculture == '0' ? 'checked' : '' }}>
                            Goto -> ८.४  
                        </label>
                    </div>
                </div> 
                <!-- 2 -->
                <div class="form-group col-md-12">
                    <p>
                       २. यदि परिवारले कृषि कार्यका लागि जग्गा प्रयोग गरेको छ भने सो जग्गाको विवरण (क्षेत्रफल उल्लेख गर्ने) 
                    </p>
                    <div class="col-md-12">
                        @if (isset($updateAgriDetails) &&  $updateAgriDetails->haveAgriculture == '1')
                            <table class="table haveAgriDetailTbl" style="display: block;">
                        @else
                         <table class="table haveAgriDetailTbl" style="display: none;">

                        @endif
                            <tr>
                                  <th>क्र.सं</th>
                                  <th></th>
                                  <th>स्वामित्व</th>
                                  <th>
                                        क्षेत्रफल
                                        <small>रोपनी/आना/पैसा</small>
                                  </th>
                            </tr>
                            <tr>
                                <td>१.</td>
                                <td>
                                    <input type="checkbox" class="haveAgriArea"  name="haveAgriArea[haveFamilyArea]" value="1" {{ isset($agriDetailsJson->haveFamilyArea) && $agriDetailsJson->haveFamilyArea == 1 ? 'checked' : '' }}>
                                </td>
                                <td>परिवारको नाममा रहेको जग्गा</td>
                                <td>
                                    {{ Form::text("haveAgriArea[familyArea]", isset($agriDetailsJson->familyArea) ? $agriDetailsJson->familyArea : '', ['class'=>'agriArea mdl-textfield__input dashed-input-field', 'placeholder'=>'रोपनी/आना/पैसा']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>१.</td>
                                <td>
                                    <input type="checkbox" class="haveAgriArea" name="haveAgriArea[haveOtherArea]" value="1" {{ isset($agriDetailsJson->haveOtherArea) && $agriDetailsJson->haveOtherArea == 1 ? 'checked' : '' }}>
                                </td>
                                <td>अरुको नाममा रहेको जग्गा</td>
                                <td>
                                    {{ Form::text("haveAgriArea[otherArea]", isset($agriDetailsJson->otherArea) ? $agriDetailsJson->otherArea : '', ['class'=>'agriArea mdl-textfield__input dashed-input-field', 'placeholder'=>'रोपनी/आना/पैसा']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>१.</td>
                                <td>
                                    <input type="checkbox" class="haveAgriArea" name="haveAgriArea[haveOtherOwner]" value="1" {{ isset($agriDetailsJson->haveOtherOwner) && $agriDetailsJson->haveOtherOwner == 1 ? 'checked' : '' }}>
                                </td>
                                <td>अन्य स्वामित्व खुलाउने</td>
                                <td>
                                    {{ Form::text("haveAgriArea[otherOwner]", isset($agriDetailsJson->otherOwner) ? $agriDetailsJson->otherOwner : '', ['class'=>'agriArea mdl-textfield__input dashed-input-field', 'placeholder'=>'रोपनी/आना/पैसा']) }}
                                </td>
                            </tr>
                </table>
                    </div>
                </div>
            </div>
            <div class="msgDisplay"></div>
            
    </div>
</div>