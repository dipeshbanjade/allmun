@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-head">
            <header>@lang('commonField.extra.dailyReport')</header>
        </div>
        <div class="card-body ">
            <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
                <div class="mdl-tabs__panel is-active p-t-20" id="tab4-panel">
                    <div class="table-responsive ">
                    @if(count($dailyReport) > 0)
                        <?php $lan = getLan();     ?>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
                                <thead>
                                    <tr>
                                        <th width="5%">@lang('commonField.extra.sn')</th>
                                        <th>@lang('commonField.personal_information.staffName')</th>
                                        <th width="12%">@lang('commonField.extra.date')</th>
                                        <th>@lang('commonField.extra.time')</th>
                                        <th>@lang('commonField.extra.subject')</th>
                                        <th>@lang('commonField.extra.desc')</th>
                                        <th>@lang('commonField.extra.appliedOn')</th>
                                    </tr>
                                </thead>
                                <tbody>    
                                <?php  $count = 1;     ?>
                                   @foreach($dailyReport as $dlyRep)
                                <tr>
                                    <td>{{ $count ++ }}</td>
                                    @if($dlyRep->wards_id)
                                       <td>
                                           <?php  $staffWrdName =   getWardStaff($dlyRep->wards_staff_id) ?>

                                           {{ $lan == 'np' ? $staffWrdName['firstNameNep'] .' '. $staffWrdName['middleNameNep'] . ' ' .$staffWrdName['lastNameNep'] :  $staffWrdName['firstNameEng'] .' '. $staffWrdName['middleNameEng'] . ' ' .$staffWrdName['lastNameEng']   }}
                                       </td>
                                       @else
                                         <?php  $staffMunName =   getMunStaff($dlyRep->staff_municipilities_id);  ?>
                                        <td>
                                        {{ $lan == 'np' ? ucfirst($staffMunName['firstNameNep']) . ' ' . $staffMunName['middleNameNep'] .' '. $staffMunName['lastNameNep'] :  $staffMunName['firstNameEng'] . ' ' . $staffMunName['middleNameEng'] .' '. $staffMunName['lastNameEng']  }}
                                        </td>
                                    @endif
                                    <td>{{ $dlyRep->date }}</td>
                                    <td>{{ $dlyRep->startTime }} - {{ $dlyRep->startTime }}</td>
                                    <td>{{ $dlyRep->sub }}</td>

                                    <td title="{!! $dlyRep->desc !!}">
                                    {!! str_limit($dlyRep->desc , 100)
                                    !!}</td>
                                    <td>
                                         {{ $dlyRep->created_at->diffForHumans() }}
                                    </td>
                                </tr>
                                   @endforeach
                                </tbody>
                            </table>
                            {{ $dailyReport->links() }}
                          @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection