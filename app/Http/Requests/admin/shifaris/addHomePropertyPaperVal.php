<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class addHomePropertyPaperVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'refCode' => 'required|min:3|unique:account_navigates',
            'chalaniNumber' =>'required',
            'accountHolderName' =>'required',
            'accountHolderAddress' =>'required',
            'wardNumber' =>'required',
            'address' =>'required',
            'kittaNumber' =>'required',
            'area' =>'required',
            'ownerNumber' =>'required',
            'authorizedPerson' =>'required',
            'deginations_id' =>'required'
        ];
    }
}
