@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
 <div class="card-box">
  <div class="card-body">
    <div class="card-head">
      <header><i class="fa fa-eye">
       &nbsp;@lang('commonField.houseHoldSetting.phone')</i>
     </header>
   </div>
   <button id="" class="btnAddPhone btn btn-primary pull-right">
     <i class="fa fa-plus"></i>
     <span> @lang('commonField.houseHoldSetting.createPhone')</span>
   </button>

   @if(count($allPhone) > 0)
   <div class="table-scrollable" id="dropLabel">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
     <thead>
      <tr>
       <th>@lang('commonField.extra.sn')</th>
       <th>@lang('commonField.extra.refCode')</th>
       <th>@lang('commonField.houseHoldSetting.phone')</th>
       <th>@lang('commonField.houseHoldSetting.brand')</th>
       <th>@lang('commonField.extra.action') </th>
     </tr>
   </thead>
   <tbody>
    @php $count = 1  @endphp
    @foreach($allPhone as $phone)
    <tr class="odd gradeX">
     <td>{{ $count ++ }}</td>
     <td>{{ $phone->refCode }}</td>
     <td >{{ $phone->name }}</td>
      <td>{{ $phone->mobileTag }} </td>

     <td class="left" >
       
       <a href="#" data-url="{{ route('phone.edit', $phone->id) }}" class="btnEditphone btn btn-primary btn-xs" id="" >
         <i class="fa fa-pencil"></i>
       </a>
       <a href="#" class="deleteMe" data-url="{{ route('phone.delete', $phone->id )}}" data-name="{{ $phone->name }}">
        <button class="btn btn-danger btn-xs">
          <i class="fa fa-trash-o "></i>
          </button>
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endif
</div>
</div>
</div>
@endsection
  @section('modelSection')
<div class="modal" id="phoneModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.houseHoldSetting.addPhone')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ Form::open(['route' => 'phone.store', 'name'=>'frmphone']) }}
        @include('admin.setting.phone._form')
        <div class="col-lg-12 p-t-20 text-right"> 
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
        </div>
        {{ Form::close() }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

      </div>

    </div>
  </div>
</div>


<!-- edit model for phone starts here -->

<div class="modal" id="editphoneModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.editPhone')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       {{ Form::open(['name'=>'frmeditphone', 'id' => 'frmUpdatephone']) }}
       @include('admin.setting.phone._form')
       {{ Form::hidden('phoneId', null, ['class' => 'phoneId']) }}
       <div class="col-lg-12 p-t-20 text-right"> 
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
      </div>
      {{ Form::close() }}
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">

    </div>

  </div>
</div>
</div>

<!-- edit model for phone ends here -->
@endsection

@section('custom_script')
@include('admin.setting.phone.script')
@endsection