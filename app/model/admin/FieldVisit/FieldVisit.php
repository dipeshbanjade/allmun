<?php

namespace App\model\admin\FieldVisit;

use Illuminate\Database\Eloquent\Model;

class FieldVisit extends Model
{
    protected $table = 'field_visits';

    public function visit_types(){
    	return $this->belongsTo('App\model\setting\VisitType'::class);
    }
}
