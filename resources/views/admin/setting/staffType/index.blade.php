@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
    <div class="card-body ">
      <div class="card-head">
        <header><i class="fa fa-eye">&nbsp;@lang('commonField.extra.staff_types')</i></header>
      </div>
      <button id="btnStaffType" class="btn btn-primary pull-right">  
           <i class="fa fa-plus"></i>@lang('commonField.extra.createStaffTypes')
      </button>
      <div class="table-scrollable" id="dropLabel">
        @if(count($allStaffType) > 0)
             <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
               <thead>
                 <tr>
                   <th width="5%">@lang('commonField.extra.sn')</th>
                   <th>@lang('commonField.personal_information.name') </th>
                   <th> @lang('commonField.extra.action') </th>
                 </tr>
               </thead>
               <tbody>
               @php  $count = 1  @endphp
                    @foreach($allStaffType as $staffType)
                        <tr class="odd gradeX">
                          <td width="5%">{{ $count ++ }}</td>
                          <td>{{ $staffType->nameNep }} - {{ $staffType->nameEng }}</td>
                          <td> 
                            <a href="#" class="btnEditStaffType btn btn-primary btn-xs"  data-url="{{ route('admin.staffType.edit', $staffType->id)}}">
                              <i class="fa fa-pencil"></i>
                            </a>

                            <a href="#" class="deleteMe" data-url="{{ route('admin.staffType.delete', $staffType->id)}}" data-name="{{ $staffType->nameEng }}">
                                <button class="btn btn-danger btn-xs">
                                  <i class="fa fa-trash-o "></i>
                              </button>
                              </a>


                              <a href="#" class="delete_data" id="" ><button class="btn btn-danger btn-xs">
                                <i class="fa fa-eye-slash"></i></button></a>
                              </td>
                            </tr>
                    @endforeach
                   </tbody>
                 </table>
                 {{ $allStaffType->links() }}
              @else
               <h3 align="center">@lang('commonField.extra.pleaseInsertData')</h3>
             @endif
          </div>
        </div>
      </div>
    </div>
    @endsection
    @section('modelSection')
      <!-- insert data  -->
          <div class="modal" id="modelAddStaffType">
            <div class="modal-dialog">
              <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.addStaffTypes')</i></h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                {{ Form::open(['route' => 'admin.staffType.store', 'name' => 'frmAddStaffType']) }}
                <div class="modal-body">
                  @include('admin.setting.staffType._form')
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                <div class="col-lg-12 p-t-20 text-right"> 
                    {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}

                  <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
                </div>
                </div>
                {{ Form::close() }}

              </div>
            </div>
          </div>
      <!-- end insert data -->
      <!-- update form -->
       <div class="modal" id="modelUpdateStaffType">
         <div class="modal-dialog">
           <div class="modal-content">

             <!-- Modal Header -->
             <div class="modal-header">
               <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.editStaffTypes')</i></h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

             <!-- Modal body -->
             <div class="modal-body">
              {{ Form::open(['name'=>'frmEditStaffType', 'id' => 'frmUpdateStaffType']) }}
               @include('admin.setting.staffType._form')
               {{ Form::hidden('staffTypeId', null, ['class' => 'staffTypeId']) }}
            
             <div class="col-lg-12 p-t-20 text-right"> 
                 {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}

               <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
             </div>
              {{ Form::close() }}
 </div>
             <!-- Modal footer -->
             <div class="modal-footer">
             </div>

           </div>
         </div>
       </div>
       
  
      <!-- update form -->
        
  @endsection
  @section('custom_script')
    @include('admin.setting.staffType.script')
  @endsection