<?php

namespace App\Http\Requests\admin\setting\FamilyRelation;

use Illuminate\Foundation\Http\FormRequest;

class FamilyRelationValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:30|unique:family_relations',
            'nameEng' => 'required|min:3|max:30'
        ];
    }
}
