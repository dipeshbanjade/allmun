<?php

namespace App\Http\Controllers\admin\shifaris\englishFormat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\shifaris\PropertyValue;
use App\model\shifaris\ApplicantUser;
use App\Http\Requests\admin\shifaris\propertyValVal;
use App\model\mylogic;
use DB;
use Auth;


class PropertyValController extends Controller
{
    protected $viewPath = 'admin.shifaris.englishFormat.propertyVal';
    protected $shifarisTableName     = 'property_values';
    protected $appUsrTable     = 'applicant_users';
    protected $deginationsTbl = 'deginations';

    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.propertyValuePrefix');

    }

    public function index()
    {
        $page['page_title']       = 'Property Valuation';
        $page['page_description'] = 'Property Valuation';
        
        $propertyVal = PropertyValue::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));


    }

    public function create()
    {
        $page['page_title']       = 'Property Valuation : create';
        $page['page_description'] = 'Property Valuation details';

        $getUniqueCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);
        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationsTbl, $orderColumn = 'nameNep', $orderby = 'ASC');
        return view($this->viewPath . '.create', compact(['page', 'getUniqueCode', 'deginationsId']));
        


    }

    public function store(propertyValVal $request)
    {
       
       DB::beginTransaction();
       try {

        $propertyValueCount = count($request->property);

        $property = $request->property;
        $owner = $request->owner;
        $plotNo = $request->plotNo;
        $area = $request->area;
        $totalValue = $request->totalValue;
        $location = $request->location;

        $propertyValueArray = [];

        for ($i=0; $i < $propertyValueCount; $i++) {
            array_push($propertyValueArray, [
                'property' => $property[$i],
                'owner' => $owner[$i],
                'plotNo' => $plotNo[$i],
                'area' => $area[$i],
                'totalValue' => $totalValue[$i],
                'location' => $location[$i],
            ]);
        }
        $propertyValueJson = json_encode($propertyValueArray);

        //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


        $isShifarisExist = isset($request->shifarisId) ? PropertyValue::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
        $save = !empty($isShifarisExist['id']) ? PropertyValue::findOrfail($isShifarisExist['id']) : new PropertyValue;

        $save->createdBy = Auth::user()->id;
        
        $save->refCode  = $request->refCode;

        $save->wards_id  = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";

        $save->chalaniNum  = $request->chalaniNum;
        $save->issuedDate  = $request->issuedDate;
        $save->title = $request->title;
        $save->personName  = $request->personName;
        $save->municipality  = $request->municipality;
        $save->ward  = $request->ward;
        $save->district  = $request->district;
        $save->former  = $request->former;
        $save->orgAddr  = $request->orgAddr;
        $save->orgType  = $request->orgType;
        $save->orgWard  = $request->orgWard;
        $save->propertyStatement  = $propertyValueJson;
        
        // $save->owner  = $request->owner;
        // $save->location  = $request->location;
        // $save->plotNo  = $request->plotNo;
        // $save->area  = $request->area;
        // $save->totalValue  = $request->totalValue; 

        $save->totalPropertyValueInNrs  = $request->totalPropertyValueInNrs;
        $save->currencyType  = $request->currencyType;
        $save->perDollarPriceNpr  = $request->perDollarPriceNpr;
        $save->ctype  = $request->ctype;
        $save->totalPropertyValueInDollar  = $request->totalPropertyValueInDollar;
        $save->authorizedPerson  = $request->authorizedPerson;
        $save->deginations_id  = $request->deginations_id;

        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

               //end for login
        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

        // $appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $save->id, $applicantName, $applicantAddress, $citizenshipNumber, $phoneNumber, $email, $dob, $fatherName, $profilePic, $citizenshipImgPath);

                    $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
            ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
        }

    }catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
  }


}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
