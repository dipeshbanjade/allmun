@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12">
    <div class="card-box">
          <div class="card-head">
               <h3>&nbsp;<i class="fa fa-plus"></i> Create Role and Assign Permission</h3>
          </div>
          {{ Form::open(['route'=>'system.role.store', 'name'=>'frmRole']) }}
              @include('admin.acl.role._form')
              <div class="col-lg-12 p-t-20 text-right"> 
              {{ Form::submit('CREATE', ['class' => 'btn btn-success']) }}
              <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
              </div>
          {{ Form::close() }}
    </div>
</div>
@endsection