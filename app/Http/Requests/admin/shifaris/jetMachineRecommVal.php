<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class jetMachineRecommVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:jet_machine_recomms',
            'issuedDate' => 'required',
            'chalaniNumber' => 'required|numeric|unique:jet_machine_recomms',
            'municipality' => 'required',
            'wardNum' => 'required|numeric',
            'companyHead' => 'required',
            'companyName' => 'required',
            'reason' => 'required',
            'roadName' => 'required',
            'jetReason' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
