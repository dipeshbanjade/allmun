<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWRDStaffDailyReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_r_d_staff_daily_reports', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('sub',100);
            $table->date('date');
            $table->string('startTime',100);
            $table->string('endTime',100);
            $table->text('desc');


            $table->integer('wards_staff_id')->unsigned()->nullable();
            $table->foreign('wards_staff_id')->references('id')->on('ward_staffs')->onDelete('cascade'); 

            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_r_d_staff_daily_reports');
    }
}
