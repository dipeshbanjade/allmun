<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmeditDegination']").validate({
			rules:{
				nameNep : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				nameEng : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				nameNep : "The name of degination in Nepali is required",
				nameEng : "The name of degination is required"
			}
		});
	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmDegination']").validate({
			rules:{
				nameNep : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				nameEng : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				nameNep : "The name of degination in Nepali is required",
				nameEng : "The name of degination is required"
			}
		});

		var btnAddDeg = $('#btnAddDeg');
		btnAddDeg.on('click', function(){
			$('#myModal').modal('show');
		});

		
		var btnEditDeg = $('.btnEditDeg');
		btnEditDeg.on('click', function(e){
			e.preventDefault();
			var url = $(this).data('url');
			$.ajax({
				'type': 'GET',
				'url': url,
				success: function (response) {
					console.log(response);
					$('.degId').val(response.id),
					$('.txtrefCode').val(response.refCode);
					$('.txtnameNep').val(response.nameNep);
					$('.txtnameEng').val(response.nameEng);
					$("#updateDeginationForm").modal('show');
				}
			})
		});
		var frmUpdateDeg = $('#frmUpdateDeg');
		frmUpdateDeg.on('submit', function(e){
			e.preventDefault();
			var data = $(this).serialize();
			var id   = $('.degId').val();
			var url  = "{{URL::to('/')}}" + "/setting/degination/"+id+"/update";
			$.ajax({
				'type' : 'POST',
				'url'  : url,
				'data'    : data,
				success : function(response){
					console.log(response);
					if (response.success==true) {
						$('.infoDiv').append(response.message).addClass('alert alert-success');
					}
					if (response.success==false) {
						$.each(response.message, function(key, value){
							$('.infoDiv').append(value).addClass('alert alert-danger');
						})
					}
				},complete:function(){
					$("#updateDeginationForm").modal('hide');
					setTimeout(location.reload.bind(location), 1500);
				}
			})
			.fail(function (response) {
				alert('data already exists');
			});
			$('#updateDeginationForm').modal('hide');
		});
	});
</script>