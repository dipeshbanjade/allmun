<?php

namespace App\Http\Requests\admin\fieldVisit;

use Illuminate\Foundation\Http\FormRequest;

class fieldVisitValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
             'startDate'     => 'required',
              'endDate'       => 'required',
              'visit_types_id'=> 'required',
             'shortDesc'     => 'required|min:6|max:60000'
        ];
    }
}
