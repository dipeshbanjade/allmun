<?php

namespace App\model\shifaris;

use Illuminate\Database\Eloquent\Model;

class OccupationVerification extends Model
{
    Protected $table = 'occupation_verifications';
}
