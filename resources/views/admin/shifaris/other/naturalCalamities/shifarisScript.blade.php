<script type="text/javascript">
var btnAddBodartha = $('#btnAddBodartha');
btnAddBodartha.on('click', function(e){
	e.preventDefault();

	var bodarthaInput = '<input type="text" class="form-control" name="bodartha[]">';
	var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

	var insertRow = "<tr class='tblRow'>"
	+"<td>"+bodarthaInput+"</td>"
	+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
	+"</tr>";

	$('#sabik_address_table').append(insertRow);

	$('.btnRemoveItem').on('click', function(e){
		$(this).parent().parent().remove();
		return false;
	});

});

$(function(){
	/*add buttoin*/
	$("form[name='nrmWardNaturalCalamities']").validate({
		rules:{
			refCode : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			issuedDate : {
				required : true,
				date : true
			},
			chalaniNum : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			companyHead : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			companyAddress : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			candidateAddress : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			candidateName : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			calamitiesLocation : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			calamitiesDate : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			calamitiesName : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			jaminAddress : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			jaminMan : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			jaminTotalMan : {
				required : true,
				minlength : 2,
				maxlength : 30
			},
			bodartha : {
				required : true,
				minlength : 2,
				maxlength : 2000
			},
			authorizedPerson : {
				required : true,
				minlength : 2,
				maxlength : 30
			}
		},
		messages: {
			refCode : "Ref code is required",
			issuedDate : "Issue Date is required",
			chalaniNum : "Chalani Number is required",
			companyHead : "Company Head is required",
			companyAddress : " Company Address is required",
			candidateAddress : "Candidate Address is required",
			candidateName : "Candidate Name is required",
			calamitiesLocation : "Calamitites Location is required",
			calamitiesDate : "Calamities Date is required",
			calamitiesName : "Calamities Name is required",
			jaminAddress : "Jamin Address is required",
			jaminMan : "Jamin Authority is required",
			jaminTotalMan : "Jamin Total Man is required",
			bodartha : "Bodartha is required",
			authorizedPerson : "Authorized person is required"
		}
	});
});
	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='candidateName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>