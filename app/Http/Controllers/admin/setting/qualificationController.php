<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\Qualification;
use App\model\mylogic;
use App\Http\Requests\admin\setting\qualification\qualificationValidation;

class qualificationController extends Controller
{
 protected $viewPath  = 'admin.setting.qualification';
 protected $departmentTbl = 'qualifications';
 protected $prefix;

 public function __construct()
 {
   $this->middleware('auth');
   $this->prefix = config('activityMessage.qualificationPrefix');
 }

 public function index()
 {
  $page['page_title']       = 'Qualification';
  $page['page_description'] = 'list of all qualifications';

  $allQualifications = Qualification::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
  $prefix   = myLogic::sysRefCode($tablename = $this->departmentTbl, $prefix = $this->prefix);
  return view($this->viewPath . '.index', compact(['page', 'allQualifications', 'prefix'])); 
}


public function store(qualificationValidation $request)
{
    $mySave = Qualification::create([
      'refCode' => $request->refCode,
      'nameNep' => $request->nameNep ?? 'N/A',
      'nameEng' => $request->nameEng,
      'status'  => 0,
      'softDelete' => 0
      ]);
    if ($mySave) {
     return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
   }else{
     return back()->withMessage(config('activityMessage.unSaveMessage'));
   }
}

public function edit($id)
{
 $data = Qualification::findOrFail($id);
 if ($data) {
   return response()->json([
     'success' => true,
     'refCode' => $data->refCode,
     'id'      => $data->id,
     'nameNep' => $data->nameNep,
     'nameEng' => $data->nameEng
     ]);
 }else{
   return response()->json([
     'success' => false,
     'message' => config('activityMessage.dataNotFound')
     ]);
 }
}


public function update(qualificationValidation $request, $id)
{
 $update          = Qualification::findOrFail($id);
 $update->refCode = $request->refCode;
 $update->nameNep = $request->nameNep ?? 'N/A';
 $update->nameEng = $request->nameEng;

 $myUpdate = $update->update();
 if ($myUpdate) {
   return response()->json([
     'success' => true,
     'message' => config('activityMessage.updateMessage')
     ]);
 }else{
   return response()->json([
     'success'  => false,
     'message'  => config('activityMessage.notUpdated')
     ]);
 }
}

    public function destroy($id)
    {
       $del = Qualification::findOrFail($id);
           $del->softDelete = 1;
           $myDel = $del->update();
           if ($myDel) {
             return MyLogic::successChangeStatus($code = 200);
           }else{
              return MyLogic::successChangeStatus($code = 301);
           }
    }
  }
