<?php

namespace App\Http\Controllers\admin\setting\houseSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseSetting\disease;
use App\Http\Requests\admin\setting\Disease\DiseaseValidation;
use App\model\mylogic;
use Validator;
class diseaseController extends Controller
{
 protected $viewPath  = 'admin.setting.disease';
 protected $diseaseTbl = 'diseases';
 protected $prefix;

 public function __construct()
 {
  $this->middleware('auth');
  $this->prefix = config('activityMessage.diseasePrefix');
}
public function index()
{
    $page['page_title']       = 'Disease';
    $page['page_description'] = 'list of all Disease';

    $allDisease = disease::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

    return view($this->viewPath . '.index', compact(['page', 'allDisease']));
}


public function store(DiseaseValidation $request)
{
    $prefix   = myLogic::sysRefCode($tablename = $this->diseaseTbl, $prefix = $this->prefix);
    $mySave = disease::create([
       'refCode' => $prefix,
       'nameNep' => $request->nameNep ?? 'N/A',
       'nameEng' => $request->nameEng,
       'tag'     => $request->tag,
       'status'  => 0,
       'softDelete' => 0
       ]);
    if ($mySave) {
        return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
    }else{
        return back()->withMessage(config('activityMessage.unSaveMessage'));
    }
}

public function edit($id)
{
   $data = disease::findOrFail($id);
   if ($data) {
    return response()->json([
      'success' => true,
      'refCode' => $data->refCode,
      'id'      => $data->id,
      'nameNep' => $data->nameNep,
      'nameEng' => $data->nameEng,
      'tag'     => $data->tag
      ]);
}else{
    return response()->json([
      'success' => false,
      'message' => config('activityMessage.dataNotFound')
      ]);
}
}
public function update(Request $request, $id)
{
 $validator = Validator::make($request->all(), [
     'nameEng'  => 'required|min:4|max:50',
     'nameNep'  => 'min:4|max:50',
     'tag'      => 'required'
     ]);

 if ($validator->fails()) {
  return response()->json([
   'success' => false,
   'message' => $validator->errors()->all()
   ]);
}
$update          = disease::findOrFail($id);
$update->nameNep = $request->nameNep ?? 'N/A';
$update->nameEng = $request->nameEng;
$update->tag = $request->tag;



$myUpdate = $update->update();
if ($myUpdate) {
    return response()->json([
      'success' => true,
      'message' => config('activityMessage.updateMessage')
      ]);
}else{
    return response()->json([
      'success'  => false,
      'message'  => config('activityMessage.notUpdated')
      ]);
}
}

public function destroy($id)
{
   $del = disease::findOrFail($id);
   $del->softDelete = 1;
   $myDel = $del->update();
   if ($myDel) {
       return MyLogic::successChangeStatus($code = 200);
   }else{
    return MyLogic::successChangeStatus($code = 301);
}

}
}
