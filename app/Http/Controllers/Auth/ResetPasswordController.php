<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{

    use ResetsPasswords;
    protected $redirectTo = '/dashboard/home';
    public function __construct()
    {
        $this->middleware('auth');
        // $this->prefix      = config('activityMessage.wardStaffPrefix');
    
    }
}
