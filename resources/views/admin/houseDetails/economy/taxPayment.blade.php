<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
                <div class="form-group col-md-12">
                    <?php 
                        $taxPayDetails = $taxPayDetails ? $taxPayDetails : '';
                        
                    ?>
                    <p>
                       ८. यदि तपाईको आफ्नो घर, उद्योगधन्दा वा व्यवसाय छ भने, तपाई नगरपालिकालाई नियमित कर तिर्नुहुन्छ ?  
                    </p>
                    <div>
                        <label class="radio-inline">
                             १. छ
                            <input type="radio" name="havePaidTax" value="1" {{ isset($taxPayDetails->havePaidTax) && $taxPayDetails->havePaidTax ==1 ? 'checked' : ''  }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन
                            <input type="radio" name="havePaidTax" value="0" {{ isset($taxPayDetails->havePaidTax) && $taxPayDetails->havePaidTax ==0 ? 'checked' : ''  }}>
                        </label>
                    </div>
                    <div class="col-md-12">
                        <p>
                            यदि छ भने, कुन कुन कर तिर्नुहुन्छ ?  (बहुउत्तर सम्भव छ)  
                        </p>
                        <label class="radio-inline">
                             
                            १. सम्पत्ति कर
                            <input type="checkbox" name="payTaxName[]" value="सम्पत्ति कर">
                        </label>
                        <label class="radio-inline">
                            २. घर बहाल कर
                            <input type="checkbox" name="payTaxName[]" value="घर बहाल कर">
                        </label>
                        <label class="radio-inline">
                            ३. व्यवसाय कर 
                            <input type="checkbox" name="payTaxName[]" value="व्यवसाय कर">
                        </label>
                        <label class="radio-inline">
                            {{ Form::text('payTaxName[]', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
                        </label>
                    </div>
                </div>
            </div>
            <div class="msgDisplay"></div>
            
    </div>
</div>