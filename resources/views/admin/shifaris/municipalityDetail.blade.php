<?php
    function getChecker(){
        $checker = getMunicipalityData();
        return $checker;
    }
    $logo = getChecker()['munLogo'];
?>
<header class="letter_head"> <!-- hide-on-print class to hide on print-->
    <div class="row">
        <div class="col col-lg-2 col-md-4">
            <p><img id="govLogo" src="{{ asset($logo) }}"> </p>
        </div>
        <div class="col col-lg-8 col-md-6 text-center sifarish_head">
            <h1 class="name_header red-color" style="color: red !important;"><span></span>पोखरा महानगरपालिका</h1> {{-- {{ getChecker()['nameNep'] }} --}}
            <h3 class="red-color""><span></span><span class="nep-number-font red-color">१३</span> नं. वडा कार्यालय</h3>
            <h3 class="sm-head red-color"><span></span>गण्डकी प्रदेश, नेपाल</h3>
        </div>
        <div class="col col-lg-2 col-md-2"></div>
    </div>
</header>