<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class IndustryBusinessDetail extends Model
{
       protected $table = 'industry_business_details';
}
