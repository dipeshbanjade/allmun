<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class VisitType extends Model
{
    protected $table  = 'visit_types';
    protected $fillable = ['refCode', 'nameNep', 'nameEng', 'desc', 'status', 'softDelete'];
}
