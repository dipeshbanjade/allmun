<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Print Cart</title>
		<link rel="stylesheet" href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/card.css') }}" media="print,screen">
	</head>
	<body>
		<div id="background">
			@yield('printCard')
		</div>
		<div class="row">
		    <p class="pull-right align-btn">
		        <button class="btn btn-success btn-md" onclick="printDiv('background')"> Print Card </button>
		        <button class="btn btn-success btn-md" onclick="javascript.history.go(-1)"> Back </button>
		    </p>
		</div>

        <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('lib/js/bootstrap.min.js') }}"></script>
         <script type="text/javascript">
	        function printDiv(divName) {
	             var printContents = document.getElementById(divName).innerHTML;
	             var originalContents = document.body.innerHTML;
	             document.body.innerHTML = printContents;
	             window.print();
	             document.body.innerHTML = originalContents;
	       }
    	</script>
 </body>
 </html>