<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmedithandWashMethod']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of hand wash method is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmhandWashMethod']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of hand wash method is required"
            }
        });
	var btnAddhandWashMethod = $('.btnAddhandWashMethod');
	btnAddhandWashMethod.on('click', function(e){
		$('#handWashMethodModel').modal('show'); 
	});

	var btnEdithandWashMethod = $('.btnEdithandWashMethod');
	btnEdithandWashMethod.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.handWashMethodId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#edithandWashMethodModel").modal('show');
			}
		})
	});

	var frmUpdatehandWashMethod = $('#frmUpdatehandWashMethod');
  frmUpdatehandWashMethod.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.handWashMethodId').val();
    var url  = "{{URL::to('/')}}" + "/setting/handWashMethod/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#edithandWashMethodModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#edithandWashMethodModel').modal('hide');
});
});
</script>