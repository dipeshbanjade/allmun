<div class="row top-part">
  <div class="col-md-6">
   <p align="left">   
    <b class="ps">पत्र संख्या: </b>
    @if(isset($data->refCode))
    {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
    @else
    {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
    @endif
  </p>

  <p align="left">
    <b> च. नं.:
      {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
    </b>
  </p>
</div>
<div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    <h4 align="center">
      <b>बिषय: धारा जडान सिफारिस ।</b>
    </h4>
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left"> 
      <b>श्री 
        {{ Form::text('accountHolderName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderName', 'required' => 'required']) }}
      </b>,
    </p>
    <p align="left">
      <b> 
       {{ Form::text('accountHolderAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderAddress', 'required' => 'required']) }}।
     </b>
   </p>
 </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">
    <p>
     <b> 
       {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
     </b>महा/उपमहा/नगरपालिका/गाउँपालिका वडा नं.
     <b> 
       {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
     </b>
     (साविक ठेगाना 
     <!--  -->
     {{ Form::text('sabikAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'sabikAddress', 'required' => 'required']) }}
     <!--  -->
     )    
     बस्ने श्री/सुश्री/श्रीमती 
     {{ Form::text('ownerName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'ownerName', 'required' => 'required']) }}
     को नाममा दर्ता कायम रहेको कि.न.
     {{ Form::text('kittaNo', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'kittaNo', 'required' => 'required']) }}
     को जग्गामा मिति
     {{ Form::text('buildDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'buildDate',  'onfocus' => 'showNdpCalendarBox("buildDate")']) }}
     मा भवन निर्माण स्विकृति लिनु भई आशिंक वा पुर्ण रुपमा सम्पन्न गर्नुभएको वा अभिलेखिकरण गर्नुभएको हुँदा
     {{ Form::text('category', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'category', 'required' => 'required']) }}
     खानेपानी लाईन नडान गरीदिन हुन सिफारिस साथ अनुरोध गरिन्छ ।
   </p> 
 </div>
</div> 
<h4 align="center"><b>कित्ता नं को विवरण</b> </h4>
<div class="col-md-12">
  <div class="table-responsive">
    <table class="table table-bordered" id="house_kayem_table">
      <thead>
        <tr>
          <th width="2%">क्र.स.</th>
          <th>कि.नं.</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $count = 1;
        $tableData = !empty($data) ? json_decode($data->tableData, true) : [''];
          //dd($tableData);
        foreach($tableData as $singleData){         
          ?>
          <tr>
            <td> {{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => '', 'required' => 'required']) }}</td>
            <td> {{ Form::text('plotNo[]', !empty($singleData['plotNo']) ? $singleData['plotNo'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'plotNo', 'required' => 'required']) }}</td>

            @if($count == 2)            
            <td class="add-btns" style="border: 0px;"><button type="button" id="addTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button></td>
            @else
            <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
            @endif
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <h2>बोधार्थ&nbsp;  </h2>
    <table class="table table-bordered" id="bodartha">
      जानकारिको लागी
      <tbody>
        <?php 
        $count = 1;
        $tableData = !empty($data) ? json_decode($data->bodartha, true) : [''];
          //dd($tableData);
        foreach($tableData as $singleData){       
          $count++;  
          ?>
          <tr>
            <td>
              {{ Form::text('bodarthaCopy[]', !empty($singleData['bodarthaCopy']) ? $singleData['bodarthaCopy'] : NULL, ['class'=>'form-control']) }}
            </td>

            @if($count == 2)            
            
            <td class="add-btns" style="border: 0px;"><button type="button" id="add_bodartha" class="btn btn-success"><span class="fa fa-plus"></span></td>
              @else
              <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
              @endif


            </tr>
            <?php } ?>
          </tbody>

        </table>
        <div class="text-right btm-last">
          <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required"></p>
          <p> <b>
            <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
              @if(count($deginationsId) > 0)
              @foreach($deginationsId as $deg)
              <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                {{ $deg->nameNep }} 
              </option>
              @endforeach
              @endif
            </select>
          </b></p>
        </div>
        <!--views for nibedak detail -->
        <div class="clearfix"></div>
        <hr>
        @include('admin.shifaris.nibedakCommonField')

        <!-- END -->
      </div>
      <div class="col-md-12">
        <hr>
      </div>
    </div>
  </div>
</div>