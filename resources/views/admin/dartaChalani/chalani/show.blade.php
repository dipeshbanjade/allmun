@extends('main.app')
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-body">
            <div class="card-head">
                <header>
                <h3><span>
                <i class="fa fa-eye">&nbsp;@lang('commonField.dartaChalani.chalani')</i></span>: 
                {{ $data->chalaniNo }}</h3>
                </header>
            </div>
               <section class="landing" id="printMe">
                <div class="container">
                <dl class="row card-header">
                  <dd class="col-sm-12 text-center">
                  	  <span>@lang('commonField.dartaChalani.chalaniNo'): 
                  	  &nbsp;&nbsp;{{ $data->chalaniNo }}</span>
                  	  <span>
                  	  @lang('commonField.dartaChalani.date') : 
                  	  &nbsp;&nbsp;{{ $data->chalaniDate }}
                  	  </span>
                      <!--  -->
                  	  <span>
                  	  @lang('commonField.dartaChalani.page') : 
                  	  &nbsp;&nbsp;{{ $data->chalaniPage }}
                  	  </span>

                  	  <span>
                  	  @lang('commonField.dartaChalani.company') : 
                  	  &nbsp;&nbsp;
                  	  @if($data->other_offices_id)
                                    {{ $lang == 'np' ? $data->otherOffices->nameNep : $data->otherOffices->nameEng  }}
                                @endif
                  	  </span>
                  </dd>
                </dl>

                <hr>
                <!--  -->
                <dl class="row card-header">
                  <dd class="col-sm-12 text-center">
                  	  <span>@lang('commonField.dartaChalani.subject'): 
                  	  &nbsp;&nbsp;{{ $data->chalaniSubject }}</span>
                  	  <span><br>
                  	  <span>
                  	  	{{ $data->chalaniKaifiyat }}
                  	  </span>
                  </dd>
                </dl><hr>
              </section>
                <!--  -->
                  <dl class="row">
                    <dt class="col-sm-3">@lang('commonField.personal_information.image')</dt>
                    <dd class="col-sm-9">
                      @if($data->imagePath)
                           <?php   
                              $jsonDecode = json_decode($data->imagePath);
                           ?>
                           @if($jsonDecode)
                              @foreach($jsonDecode as $img)
                                 <img src="{{ asset($img->chalaniImage) }}" width="600" height="500"> <br>
                              @endforeach
                           @endif
                      @endif
                    </dd>
                  </dl>
                  <dl class="row">
                    <dd class="col-sm-12 text-right">
                    <span> 
                      <a href="{{ route('editChalani', $data->idEncript) }}"  title="View & Edit chalani details" class="btn btn-info">
                        <i class="fa fa-fw fa-lg fa-pencil-square-o" data-toggle="modal" data-target="#updateUNit" data-id=""></i>
                    </a>
                    <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> 
                        <span>@lang('commonField.button.back')</span> 
                    </button>
                    <button class="btn btn-info" onclick="printDiv('printMe')">
                      <i class="fa fa-print"></i>
                    </button>
                    </span>
                    </dd>
                  </dl>
                </div>
            </div>
        </div>
</div>
@endsection