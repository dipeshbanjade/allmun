<div class="row top-part">
  <div class="col-md-6">
    <p align="left">
      <b>पत्र संख
        @if(isset($data->refCode))
        {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
        @else
        {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
        @endif
      </b>
    </p>
    <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNumber']) }}</b></p>
  </div>
  <div class="col-md-6">
    <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24">
    </p><h4 align="center"><b>विषय: आन्तरिक बसाई सराई ।</b></h4>
    {{ Form::hidden('letter_subject', 'सिफारिस गरिएको बारे') }}
    <p></p>
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">श्रीस/सुश्री/श्रीमति   
      {{ Form::text('accountHolderName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} ,

    </p>
    <p align="left">  {{ Form::text('accountHolderAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'officeAddress']) }} ।</p>
    
  </div>
</div>
<div class="row content-para">
  <div class="col-md-12">
    <p align="left">
      तपाई
      <b>
        {{ Form::text('applicant', null, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
      </b>
      बाट एक्लै तपशिलमा उल्लेखित परिवार सहित मिति
      <b>
        {{ Form::text('moveDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'moveDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("moveDate")']) }}
      </b>
      देखि 
      <b>{{ Form::text('district', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>
      जिल्ला
      <b>{{ Form::text('municipalityName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>
      महार उपमाहार नगरपालिकार गाँउपालिका वडा नं
      <b>{{ Form::text('wardFrom', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>
      बाट यस
      <b>{{ Form::text('municipalityName2', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>
      वडा नं
      <b>{{ Form::text('ward2', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>
      अन्तर्गत
      <b>{{ Form::text('address', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>
      मा बसाई सरि आउनुभएको व्यहोरा प्रमाणित गरिन्छ ।
      <!--  -->

    </div>
  </div>
  <div class="col-md-12">
    <h4 align="center"><b>तपशिल</b></h4>
    <div class="table-responsive">
      <table class="table table-bordered" id="property_table">
        <thead>
          <tr><th>क्र.सं.</th>
            <th>नाम थर</th>
            <th>निवेदक संगको नाता</th>
            <th>ना.प्र.नं. / जन्मदर्ता नं.</th>
            <th>घर नं. </th>
            <th>बाटोको नाम</th>
            <th>उमेर</th>
          </tr></thead>
          <tbody id="tableInternalMigration">
            <?php 
            $count = 1;
            $tableData = !empty($data) ? json_decode($data->dataTable, true) : [1];
          //dd($tableData);
            foreach($tableData as $singleData){                       
              ?>
              <tr>
                <td>{{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                <td>{{ Form::text('nameCaste[]', !empty($singleData['nameCaste']) ? $singleData['nameCaste'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('relation[]', !empty($singleData['relation']) ? $singleData['relation'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('citizenBirthNo[]', !empty($singleData['citizenBirthNo']) ? $singleData['citizenBirthNo'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('houseNo[]', !empty($singleData['houseNo']) ? $singleData['houseNo'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('streetName[]', !empty($singleData['streetName']) ? $singleData['streetName'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                <td>{{ Form::text('age[]', !empty($singleData['age']) ? $singleData['age'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                @if($count == 2)                
                <td class="add-btns" style="border: 0px;"><a href="" id="addNewRowInTable" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
                @else
                <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                @endif
                
              </tr>
              <?php } ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="text-right btm-last">
      <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
      <p> <b>
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b></p>
    </div>
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr>
    @include('admin.shifaris.nibedakCommonField')
    <!-- END -->
  </div>
  <div class="col-md-12">
    <hr>
  </div>
</div>
</div>
</div>