<script type="text/javascript">
	$(function(){
	  $("form[name='frmSchoolUnadmitFrm']").validate({
	  		rules:{
	  			primaryHealthyMale:{
	  				number:true
	  			},
	  			primaryHealthyFemale:{
	  				number: true
	  			},
	  			primaryUnhealthyMale:{
	  				number:true
	  			},
	  			primaryUnhealthyFemale:{
	  				number:true
	  			},
	  			secondaryHealthyMale:{
	  				number:true
	  			},
	  			secondaryUnhealthyMale:{
	  				number:true
	  			}

	  		},
	  		messages:{
	  			primaryHealthyMale :" This filed should be numeric",
	  			primaryHealthyFemale :" This filed should be numeric",
	  			primaryUnhealthyMale :" This filed should be numeric",
	  			primaryUnhealthyFemale :" This filed should be numeric",
	  			secondaryHealthyMale :" This filed should be numeric",
	  			secondaryUnhealthyMale :" This filed should be numeric"

	  		}
	  });	  	
	  $("form[name='frmEducationDropFrm']").validate({
	  		rules:{
	  			maleChildNum:{
	  				number: true
	  			},
	  			femaleChildNum:{
	  				number:true
	  			},
	  			otherMReasonToLeave:{
	  				required:function(element){
	  					var maleLeaveNumber= $('input[name=maleChildNum]');
	  					var maleReasonToLeave= $("input[name=maleReasonToLeave]:checked");			
	  					return(maleLeaveNumber[0].value != "" &&maleReasonToLeave.val() == null ? true: false);  					
	  				},
	  				minlength:3
	  			},
	  			otherFReasonToLeave:{
	  				required:function(element){
	  					var femaleChildNum= $('input[name=femaleChildNum]');
	  					var femaleReasonToLeave= $("input[name=femaleReasonToLeave]:checked");
	  					return (femaleChildNum[0].value != "" &&femaleReasonToLeave.val() == null ? true:false); 					
	  				},
	  				minlength:3
	  			}

	  		},
	  		messages:{
	  			maleChildNum: " Plesase enter valid number",
	  			femaleChildNum: " Plesase enter valid number",
	  			otherMReasonToLeave: " The other reason should be specified ",
	  			otherFReasonToLeave: " The other reason should be specified "
	  		}
	  });
	  $("form[name='frmChildMontessoriFrm']").validate({
	  		rules:{
	  			maleChildNum:{
	  				number:true
	  			},
	  			femaleChildNum:{
	  				number:true
	  			}
	  		},
	  		messages:{
	  			maleChildNum:"Please enter valid number",
	  			femaleChildNum:"Please enter valid number"
	  		}
	  });
	 $('input[name^=disTime]').prop('required',true);
	  $("form[name='frmChildClubInvolvementFrm']").validate({
	  	ignore:[],
	  		rules:{
	  			"schoolRelated[male]":{ // This filed is required if all field are empty 
	  				required: function(ele){
	  					var hasInvolve= $("input[name=haveChildInvolve]");
	  					var communityRelated= $("input[name='communityRelated[male]']");
	  					var childClubRelated= $("input[name='childClubRelated[male]']");
	  					var schoolRelatedFemale= $("input[name='schoolRelated[female]']");
	  					var communityRelatedFemale= $("input[name='communityRelated[female]']");
	  					var childClubRelatedFemale= $("input[name='childClubRelated[female]']");
	  					if(hasInvolve[0].checked == true && communityRelated.val() == "" && childClubRelated.val() == "" && communityRelatedFemale.val() == "" && childClubRelatedFemale.val() == "" && schoolRelatedFemale.val()==""){
	  						return true
	  					}

	  				},
	  				// minlength:3
	  			}, 
	  			 			
	  		
	  		},
	  		messages:{
	  			"schoolRelated[male]":"Atleast one of this filed is required"
	  			
	  		}
	  });
	  $("form[name='frmChildHobbyFrm']").validate({
	  	ignore:[],
	  		rules:{
	  			"childHobby[male][]":{
	  				required: function(ele){
	  					var hasHobby = $("input[name=haveHobby]");
	  					return(hasHobby[0].checked == 1 && $("input[name='childHobby[female][]']:checked").length <=0 ? true : false);
	  				}
	  			},
	  		},	
	  		messages:{
	  			"childHobby[male][]": "Atleast one of the filed is required"	  		
	  		}
	  });
	});

	function checkChildEdu(select){
		$("form[name='frmChildrenEducationFrm']").validate();
		if(select.value ==1 && select.checked== true){
			$('.childEduTbl').show();
			// $('input[name^=childrenInfo]').val("0");
			// $('input[name^=childrenInfo]').prop("required",true);
			$('input[name^=childrenInfo]').attr('number', true);
		}else{
			$('.childEduTbl').hide();
			$('input[name^=childrenInfo]').val("");
		}
	}
	function checkEduDropReasonMale(select){ // unheck the radio button if  leave male number is zero
		if (select.value == "" || select.value == null){
			$("input[name=maleReasonToLeave]").each(function(){
				$(this).prop('checked',false);
			});
			$("input[name=otherMReasonToLeave]").val("");
		}
		
	}

	function checkEduDropReasonFemale(select){ // uncheck radio button if education drop feamle number is equal to zero
		if (select.value == "" || select.value == null){
			$("input[name=femaleReasonToLeave]").each(function(){
				$(this).prop('checked',false);
			});
			$("input[name=otherFReasonToLeave]").val("");
		}
	}

	function displayChildClubTbl(select){
		if(select.value == 1){
			$(".childClubTbl").show();
		}else{
			$(".childClubTbl").hide();
			$('input[name^=schoolRelated]').val('');
			$('input[name^=communityRelated]').val('');
			$('input[name^=childClubRelated]').val('');
		}
	}

	function displayChildHobbyTbl(select){
		if(select.value == 1){
			$('.childHobbyTbl').show();
			
		}else{
			$('.childHobbyTbl').hide();
			$("input[name^=childHobby").each(function(){
				$(this).prop('checked',false);
			})
		}
	}
</script>