<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
        <div class="form-group col-md-12">{{-- 12 --}}
            <p>
                १. परिवारमा एचआइभी संक्रमित आमाबाट जन्मेको बच्चा छ ?                          
            </p>
            <br>
            <td>
                <label class="radio-inline">
                    १. छ 
                    <input type="radio" onchange="displayHivPregTbl(this)" name="haveHIV" value="1" {{ isset($hsHIVP) && $hsHIVP->haveHIV == '1' ? 'checked' : '' }}>
                </label> 
                <label class="radio-inline">
                    २. छैन्
                    <input type="radio" onchange="displayHivPregTbl(this)" name="haveHIV" value="0" {{ isset($hsHIVP) && $hsHIVP->haveHIV == '0' ? 'checked' : '' }} checked="">
                </label> 
            </td>
            <br>
            यदि छ भने तलको विवरण भर्नुहोस् । (विगत ५ वर्ष भित्र)
            @if (isset($hsHIVP) && $hsHIVP->haveHIV == '1')
                <table border="1" class="hivPregTbl" style="display: block">
            @else
                 <table border="1" class="hivPregTbl" style="display: none;">       
            @endif
            
            
                <thead>
                    <tr>
                        <th> सि.न.</th>
                        <th>यच.आइ.भि. लागेको छ ?</th>
                        <th>बालक वा बालिकाको नाम</th>
                        <th>उमेर</th>                        
                        <th>एआरभी (ARV) प्रोफाइलेक्सिस् सेवा <br> १) पाएको २) नपाएको</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = 0;
                    $hsHIVPregInfo = isset($hsHIVP->hsHIVPregDetails) ? json_decode($hsHIVP->hsHIVPregDetails) : '';
                    //Note $getMyFamilyDetails from houseDetailsController
                    foreach($getMyFamilyDetails as $member){
                        $memberInfo = getParentsName($member->relation_id);
                    //dd($memberInfo);                
                        $memberAge = convertMyAge($memberInfo['dobAD']);

                        if(isset($memberAge) && $memberAge <= 5){                        
                            $count++; 
                            $HIVPregSingleDetail = moreHouseDetailsFormUpdate($member->relation_id, $hsHIVPregInfo, $compareJsonKey = 'childId');
                            //dd($singleEducationDropper);
                            ?>
                            <tr> 
                                <td>
                                    {{ $count }}
                                </td>
                                <td>
                                    <input type="hidden" name="hsHIVPregDetails[<?php echo $count;  ?>][childId]" value="{{ $member->relation_id }}">

                                    <label class="radio-inline">
                                        १. छ 
                                        <input type="radio" class="hsHIVPregDetails" name="hsHIVPregDetails[<?php echo $count; ?>][childHaveHIV]" value="1" {{ isset($HIVPregSingleDetail['childHaveHIV']) && $HIVPregSingleDetail['childHaveHIV'] == '1' ? 'checked' : '' }} >
                                    </label> 
                                    
                                </td>
                                <td>
                                    {{ Form::text("hsHIVPregDetails[$count][childName]", $memberInfo['fnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::text("hsHIVPregDetails[$count][childAge]", $memberAge, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>' ', 'id' => '']) }}
                                </td>

                                <td>
                                    <label class="radio-inline">
                                        १. छ 
                                        <input type="radio" name="hsHIVPregDetails[<?php echo $count; ?>][haveProphylaxis]" value="1" {{ isset($HIVPregSingleDetail['haveProphylaxis']) && $HIVPregSingleDetail['haveProphylaxis'] == '1' ? 'checked' : '' }} >
                                    </label> 
                                    <label class="radio-inline">
                                        २. छैन्
                                        <input type="radio" name="hsHIVPregDetails[<?php echo $count; ?>][haveProphylaxis]" value="0" {{ isset($HIVPregSingleDetail['haveProphylaxis']) && $HIVPregSingleDetail['haveProphylaxis'] == '0' ? 'checked' : '' }}>
                                    </label> 
                                </td>
                            </tr>
                            <?php }} ?>                                     
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="msgDisplay"></div>
        </div>
    </div>