<?php

namespace App\Http\Requests\admin\setting\electricity;

use Illuminate\Foundation\Http\FormRequest;

class ElectricityValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:electricities',
            'nameEng' => 'required|min:4:min:50'
        ];
    }
}
