<?php

namespace App\Http\Controllers\admin\setting\houseSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseSetting\chuloType;
use App\model\mylogic;
use App\Http\Requests\admin\setting\chuloType\chuloTypeValidation;
use Validator;
class chuloTypeController extends Controller
{

   protected $viewPath  = 'admin.setting.chuloType';
   protected $chuloTypeTbl = 'chulo_types';
   protected $prefix;

   public function __construct()
   {
    $this->middleware('auth');
    $this->prefix = config('activityMessage.chuloTypePrefix');
}
public function index()
{
    $page['page_title']       = 'Chulo Type';
    $page['page_description'] = 'list of all Chulo Type';

    $allChuloType = chuloType::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
    $prefix   = myLogic::sysRefCode($tablename = $this->chuloTypeTbl, $prefix = $this->prefix);
    return view($this->viewPath . '.index', compact(['page', 'allChuloType', 'prefix'])); 
}

public function store(chuloTypeValidation $request)
{
       $mySave = chuloType::create([
         'refCode' => $request->refCode,
         'nameNep' => $request->nameNep ?? 'N/A',
         'nameEng' => $request->nameEng,
         'status'  => 0,
         'softDelete' => 0
         ]);
       if ($mySave) {
        return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
    }else{
        return back()->withMessage(config('activityMessage.unSaveMessage'));
    }
}

public function edit($id)
{
     $data = chuloType::findOrFail($id);
     if ($data) {
        return response()->json([
          'success' => true,
          'refCode' => $data->refCode,
          'id'      => $data->id,
          'nameNep' => $data->nameNep,
          'nameEng' => $data->nameEng
          ]);
    }else{
        return response()->json([
          'success' => false,
          'message' => config('activityMessage.dataNotFound')
          ]);
    }
}


public function update(Request $request, $id)
{
       $validator = Validator::make($request->all(), [
           'nameEng'  => 'required|min:4|max:50',
           'nameNep'  => 'min:4|max:50',
           ]);

       if ($validator->fails()) {
          return response()->json([
             'success' => false,
             'message' => $validator->errors()->all()
             ]);
      }
      $update          = chuloType::findOrFail($id);
      $update->nameNep = $request->nameNep ?? 'N/A';
      $update->nameEng = $request->nameEng;

      $myUpdate = $update->update();
      if ($myUpdate) {
        return response()->json([
          'success' => true,
          'message' => config('activityMessage.updateMessage')
          ]);
    }else{
        return response()->json([
          'success'  => false,
          'message'  => config('activityMessage.notUpdated')
          ]);
    }
}


public function destroy($id)
{
     $del = chuloType::findOrFail($id);
     $del->softDelete = 1;
     $myDel = $del->update();
     if ($myDel) {
         return MyLogic::successChangeStatus($code = 200);
     }else{
        return MyLogic::successChangeStatus($code = 301);
    }
}
}
