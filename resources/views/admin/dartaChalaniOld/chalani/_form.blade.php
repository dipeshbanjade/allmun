<style>
  .form-select-add{
    width: 88%; 
    height: 36px !important;
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .form-add-button{
    margin-top: -36px;
  }
  .box-body textarea{
    width: 90% !important;
    height: 35px !important;
    border-left: 0;
    border-top: 0;
    border-right: 0;
    border-radius: 0;
    border-bottom: 2px solid #ccc;
    resize: none;
  }
   textarea::-webkit-input-placeholder {
   text-align: center;
  }

  textarea:-moz-placeholder { /* Firefox 18- */
     text-align: center;  
  }

  textarea::-moz-placeholder {  /* Firefox 19+ */
     text-align: center;  
  }

  textarea:-ms-input-placeholder {  
     text-align: center; 
  }
    
</style>
<div class="container">
  <p class="star" align="right">
    <b class="mt">मिति :</b>{{ Form::text('chalaniDate', null, ['class'=>'txtChalaniDate dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'chalani date', 'id' => 'chalaniDate', 'onfocus' => 'showNdpCalendarBox("chalaniDate")', 'title'=>'chalani date']) }}
  </p>    
  <div class="box-body" align="center">
    <div class="form-group">
      {{ Form::textarea('chalaniSubject', null, ['class'=> 'form-control', 'placeholder'=>'Chalani Subject', 'title'=>'enter Chalani subject']) }}
    </div>
  </div>
</div>
<div class="container" style="margin-top: 20px;">
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12">
        <div class="form-group">
          <label>@lang('commonField.dartaChalani.chalaniNo')</label>    
          {{ Form::text('chalaniNo', null, ['class'=> 'form-control col-sm-12 txtChlaniNo', 'placeholder'=>'Chalani number', 'title'=>'enter Chalani Number']) }}
        </div>
      <div class="form-group" style="margin-top: 10px;">
        <label>पत्र  संख्या</label>
        {{ Form::text('chalaniPage', null, ['class'=> 'form-control txtChlaniPage', 'placeholder'=>'Chalani page', 'title'=>'enter Chalani page']) }} 
      </div>
    <div class="form-group" style="margin-top: 10px;">
      <label>पत्र मिति</label>
        {{ Form::text('chalaniPageDate', null, ['class'=>'form-control dashed-input-field ndp-nepali-calendar date-input-field','title'=>'Chalani page date', 'placeholder'=>'2075/06/05', 'id' => 'chalaniPageDate', 'onfocus' => 'showNdpCalendarBox("chalaniPageDate")']) }}
    </div>  
    <div class="form-group" style="margin-top: 10px;">
      <label>चलानी भण्डार</label>
      {{ Form::text('chalaniStorePlace', null, ['class'=>'form-control dashed-input-field ', 'placeholder'=>'Storage rag name','title'=>'physical storage place name']) }}
    </div>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-12">
      <div class="form-group">
        <label>कार्यालय</label>
          <select class="form-control form-select-add" name="other_offices_id" title="select company name">
            <option value="">Select company name</option>
              @if($companyList)
                 @foreach ($companyList as $cName)
                   <option value="{{$cName->id}}" {{ isset($data) && $data->other_offices_id ==$cName->id ? 'selected' : ''  }}>{{ $lang == 'np' ? $cName->nameNep : $cName->nameEng }}</option>
                 @endforeach
              @endif
          </select>
          <button type="button" id="btnAddCompanyDetail"  class="btn btn-primary btn-md form-add-button pull-right"><i class="fa fa-plus"></i><span></span></button>
      </div>    
      <div class="form-group" style="margin-top: 15px;">
        <label>चलानी टिकट</label>
        {{ Form::text('chalaniTicket', null, ['class'=> 'form-control txtChlaniTicket', 'placeholder'=>'Chalani Ticket', 'title'=>'enter Chalani Ticket', 'title'=>'chalani ticket']) }} 
      </div>
      <div class="form-group" style="margin-top: 15px;">
        <label>कैफियत </label>
          {{ Form::textarea('chalaniKaifiyat', null, ['class'=> 'form-control txtChlaniTicket', 'placeholder'=>'Remarks', 'title'=>'enter remarks']) }}      
      </div>
    </div>
  </div>

</div>