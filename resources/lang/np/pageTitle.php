<?php
   return [
       'department'        => [
          'department_title'       =>'विभाग',
          'department_description' => 'विभाग वर्णन'
       ],
      'degination'        => [
          'degination_title'       =>'पद',
          'degination_description' => 'पद वर्णन'
       ],
      'staffType'  => [
         'staffType_title'  => 'कर्मचारीका प्रकारहरू',
         'staff_description' => 'कर्मचारी वर्णन'
       ]
   ];
?>
