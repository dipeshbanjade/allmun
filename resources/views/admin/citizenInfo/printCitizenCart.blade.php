@extends('main.printerLayout')
    @section('printCard')
    @include('partials.qrCode.full.qrlib')
    <div id="Layer1" ><img src="{{ asset('image/img/pkr/Layer1.png') }}" height="528px"></div>
    <div id="Layer8">
        <?php

              $codeContents = $printCitizen->idEncrip;
               $svgTagId   = 'id-of-svg';
               $saveToFile = false;
               $imageWidth = 250;  // gives the size to QR code
               $myQr = QRcode::svg($codeContents, $svgTagId, $saveToFile, QR_ECLEVEL_L, $imageWidth);

              echo $myQr;
              ?>    
    </div>
    <div id="Logo"><img src="{{ asset('image/img/pkr/Logo.png') }}"></div>
    <div id="M"><img src="{{ asset('image/img/pkr/M.png') }}"></div>

    <div id="citizenNumber"><label class=" card-font">नागरिकता नं : </label><label id="citizenshipNumber" class="card-font nepKalimati">{{ $printCitizen->citizenNo }}</label></div>

    <div id="Logocopy"><img src="{{ asset('image/img/pkr/Logocopy.png') }}"></div>
    <div id="M_0"><img src="{{ asset('image/img/pkr/M_0.png') }}"></div>
    <div id="SaritaThapa" class="pkhCardDynamic boldText" >{!! $printCitizen->fnameNep.' '. $printCitizen->mnameNep.' '.$printCitizen->lnameNep !!}</div>
    <div id="M_1"><img src="{{ asset('image/img/pkr/M_1.png') }}"></div>
    <div id="layer_9" class="pkhCardDynamic boldText"><label class="nepKalimati">{!! $printCitizen->wardNo !!}</label></div>
    <div id="copy" class="pkhCardDynamic boldText"> <label class="nepKalimati">{!! $printCitizen->phoneNumber !!}</label></div>
    <div id="Layer7"><img src="{{ asset('image/img/pkr/Layer7.png') }}"></div>
    <div id="Layer6"><img src="{{ asset($printCitizen->profilePic) }}" width="211" height="233"></div>
    <div id="kfvfdxfgukflnsf"><img src="{{ asset('image/img/pkr/kfvfdxfgukflnsf.png') }}"></div>
    <div id="gj8fsfofno"><img src="{{ asset('image/img/pkr/gj8fsfofno.png') }}"></div>
    <div id="s39xF8sfsL"><img src="{{ asset('image/img/pkr/s34F8xsfsL.png') }}"></div>
    <div id="u08sLkbzgkfn"><img src="{{ asset('image/img/pkr/u08sLkbzgkfn.png') }}"></div>
    <div id="Rectangle1"><img src="{{ asset('image/img/pkr/Rectangle1.png') }}"></div>
    @endsection