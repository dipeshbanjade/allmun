<?php

namespace App\model\admin\ward;

use Illuminate\Database\Eloquent\Model;

class WardStaff extends Model
{
    protected $table = 'ward_staffs';

    public function users(){
    	return $this->belongsTo('App\User'::class);
    }

   public function provinces(){
   	return $this->belongsTo('App\model\setting\Province'::class);
   }

   public function districts(){
   	return $this->belongsTo('App\model\setting\District'::class);
   }

   public function staff_types(){
   	return $this->belongsTo('App\model\setting\StaffType'::class);
   }

   public function deginations(){
   	return $this->belongsTo('App\model\setting\Degination'::class);
   }

   public function departments(){
   	return $this->belongsTo('App\model\setting\Department'::class);
   }

}
