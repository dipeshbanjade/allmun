<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmedittreatmentArea']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of water availabililty is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmtreatmentArea']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of water availabililty is required"
            }
        });
    var btnAddtreatmentArea = $('.btnAddtreatmentArea');
    btnAddtreatmentArea.on('click', function(e){
        $('#treatmentAreaModel').modal('show'); 
    });

    var btnEdittreatmentArea = $('.btnEdittreatmentArea');
    btnEdittreatmentArea.on('click', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        $.ajax({
            'type': 'GET',
            'url': url,
            success: function (response) {
                $('.treatmentAreaId').val(response.id);
                $('.txtRefCode').val(response.refCode).attr('disabled',true);
                $('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
                $("#edittreatmentAreaModel").modal('show');
            }
        })
    });

    var frmUpdatetreatmentArea = $('#frmUpdatetreatmentArea');
  frmUpdatetreatmentArea.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.treatmentAreaId').val();
    var url  = "{{URL::to('/')}}" + "/setting/treatmentArea/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#edittreatmentAreaModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#edittreatmentAreaModel').modal('hide');
});
});
</script>
