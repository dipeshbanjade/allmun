<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditfamilyRelation']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of Family Relation is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='familyRelationModel']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of Family Relation is required"
            }
        });
	var btnAddFamilyReal = $('.btnAddFamilyReal');
	btnAddFamilyReal.on('click', function(e){
		$('#familyRelationModel').modal('show'); 
	});

	var btnEditfamilyRelation = $('.btnEditfamilyRelation');
	btnEditfamilyRelation.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.familyRelationId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
				$('.txtOrderColumn').val(response.orderColumn);
				$("#editfamilyRelationModel").modal('show');
			}
		})
	});

	var frmUpdatefamilyRelation = $('#frmUpdatefamilyRelation');
  frmUpdatefamilyRelation.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.familyRelationId').val();
    var url  = "{{URL::to('/')}}" + "/setting/familyRelation/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editfamilyRelationModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editfamilyRelationModel').modal('hide');
});
});
</script>