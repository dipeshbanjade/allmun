<div class="right_col nep" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">नता प्रमाणित</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
         @include('admin.shifaris.municipalityDetail')
         <div class="row top-part">
          <div class="col-md-6">
           <p align="left"><b class="ps">प. सं.:
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
            @endif
          </b></p>
          <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNum']) }}
          </b></p>
        </div>
        <div class="col-md-6">
          <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
            <b>अनुसुची ३१</b>
          </p>
          <h4 align="center"><b>नता प्रमाणित प्रमाणपत्र</b></h4>
          <p></p>
        </div>
      </div>
      <div class="row content-para">
        <div class="col-md-12">
          <p align="left">
            श्री/सुश्री/श्रीमती 
            {{ Form::text('name', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
          </p>
          <p>
            <b>
              {{ Form::text('municipality', isset($data->municipality) ? $data->municipality : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
            </b> महा/उपमाहा/नगरपालिका/गाउँपालिका वडा नं.  
            <b>
              {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            </b>
          </p>
          <p>
            देहायका व्यक्तिसँग देहाय बमोमिको नाता सम्बन्ध रहेकोले सो नाता सम्बन्ध प्रमाणित गरि पाउँ भनी
            {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            महा/उपमाहा/नगरपालिका/गाउँपालिका वडा नं. 
            {{ Form::text('ward', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            वडा कार्यालयमा मिति 
            {{ Form::text('providedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'providedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("providedDate")']) }}
            मा दिनुभएको दरखास्त बमोजिम यस कार्यालयबाट आवश्या जाँचबसझ गरी बुझ्दा तािईको देहाय बमोजिमको व्यक्तिसँग देहाय बमोजिमको नाता सम्बन्ध कायम रहेको देखिएकोले नाता प्रमाणित गरि यो प्रमाणपत्र दिइएको छ ।
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-bordered" id="business_close_table">
              <thead>
                <tr><th>क्र.स.</th>
                  <th>नाता सम्बन्ध कायम गरेको व्यक्तिको नाम </th>
                  <th>नाता</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $count = 1;
                $tableData = !empty($data) ? json_decode($data->dataTable, true) : [1];
          //dd($tableData);
                foreach($tableData as $singleData){         
                  ?>
                  <tr>
                    <td>
                      {{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('relationPerson[]', !empty($singleData['relationPerson']) ? $singleData['relationPerson'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    <td>
                      {{ Form::text('relation[]', !empty($singleData['relation']) ? $singleData['relation'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    </td>
                    @if($count == 2)
                    <td class="add-btns" style="border: 0px;">
                      <button type="button" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button>
                    </td>
                    @else
                    <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                    @endif
                    <!-- <td><a href="" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></a></td> -->
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <p align="left"><b>दरखास्त्वालाको दस्तखत ः–</b></p>
          <p align="left"><b>ल्याप्चे सहिछाप</b></p>
          <div class="col-md-3">
            <div class="img-box"><p>दायाँ</p></div>
          </div>
          <div class="col-md-3">
            <div class="img-box"><p>बायाँ</p></div>
          </div>
        </div>
        <div class="text-right btm-last">
          <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
          <p> <b>       
           <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
            @if(count($deginationsId) > 0)
            @foreach($deginationsId as $deg)
            <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
              {{ $deg->nameNep }} 
            </option>
            @endforeach
            @endif
          </select>
        </b> </p>
      </div>
      <!--views for nibedak detail -->
      <div class="clearfix"></div>
      <hr>
      @include('admin.shifaris.nibedakCommonField')
      <!-- END -->
    </div>
    <div class="col-md-12">
      <hr>
    </div>
  </div>
</div>
</div>