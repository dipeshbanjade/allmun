<script type="text/javascript">
$(function(){

	$("form[name='frmeditdrinkingWater']").validate({
		rules:{
			nameEng : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			nameEng : "The name of Drinking Water is required"
		}
	});
});
$(function(){

	$("form[name='frmdrinkingWater']").validate({
		rules:{

			nameEng : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {

			nameEng : "The name of Drinking Water is required"
		}
	});
	  var btnAddDrinkingWater = $('.btnAddDrinkingWater');
	  btnAddDrinkingWater.on('click', function(e){
	    $('#drinkingWaterModel').modal('show'); 
	});

	  var btnEditdrinkingWater = $('.btnEditdrinkingWater');
	  btnEditdrinkingWater.on('click', function(e) {
	  
	      e.preventDefault();
	      var url = $(this).data('url');
	      $.ajax({
	       'type': 'GET',
	       'url': url,
	       success: function (response) {
	          $('.drinkingWaterId').val(response.id);
	          $('.txtRefCode').val(response.refCode).attr('disabled',true);
	          $('.txtNameNep').val(response.nameNep);
	          $('.txtNameEng').val(response.nameEng);
	          $("#editdrinkingWaterModel").modal('show');
	      }
	  })
	  });

	  var frmUpdatedrinkingWater = $('#frmUpdatedrinkingWater');
	  frmUpdatedrinkingWater.on('submit', function(e){
	      e.preventDefault();
	      var data = $(this).serialize();
	      var id   = $('.drinkingWaterId').val();
	      var url  = "{{URL::to('/')}}" + "/setting/drinkingWater/"+id+"/update";
	      $.ajax({
	          'type' : 'POST',
	          'url'  : url,
	          'data'    : data,
	          success : function(response){
	              console.log(response);
	              if (response.success==true) {
	                  $('.infoDiv').append(response.message).addClass('alert alert-success');
	              }
	              if (response.success==false) {
	                  $.each(response.message, function(key, value){
	                      $('.infoDiv').append(value).addClass('alert alert-danger');
	                  })
	              }
	          },complete:function(){
	              $("#editdrinkingWaterModel").modal('hide');
	              setTimeout(location.reload.bind(location), 1500);
	          }
	      })
	      .fail(function (response) {
	          alert('data already exists');
	      });
	      $('#editdrinkingWaterModel').modal('hide');
	  });
	  });

</script>