<div class="row top-part">
  <div class="col-md-6">
   <p align="left">
     <b class="ps">पत्र संख्याः
      @if(isset($data->refCode))
      {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
      @else
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
      @endif
    </b>
    
  </p>
  <p align="left">
   <b> च. नं.:
     {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
   </b>
 </p>
</div>
<div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">
     <b>श्री 
       भूमिसुधार कार्यालय 
     </b>,
   </p>
   <p align="left">
     <b>
       {{ Form::text('officeAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'officeAddress', 'required' => 'required']) }}।
     </b>
   </p>
 </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    <h4 align="center">
      <b>विषय: सिफारिस सम्बन्धमा।</b>
    </h4>
  </div>
</div>

<div class="row">
  <div class="col-md-12 content-para">
    <p>उपरोक्त सम्बन्धमा मेरो नाममा दर्ता श्रेष्ता भएको वडा नं. 
     <b>
      {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
    </b>
    (साबिकको ठेगाना 
    <!--  -->
    {{ Form::text('address', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'address', 'required' => 'required']) }}
    <!--  -->)
    स्थित सिट नं. 
    <!--  -->
    {{ Form::text('seatNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'seatNumber', 'required' => 'required']) }}
    <!--  -->
    को कि. नं. 
    <!--  -->
    {{ Form::text('kittaNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'kittaNumber', 'required' => 'required']) }}
    <!--  -->
    क्षे. फ. 
    {{ Form::text('area', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'area', 'required' => 'required']) }}
     भएको जग्गाधनी श्रेष्ता पुर्जाको मोही श्री/सुश्री/श्रीमती
    {{ Form::text('mohiName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'mohiName', 'required' => 'required']) }} 
     कायम भएको र मोहीले जोत कमोद केही नगरेकोले मोही लगत कट्टा गर्न मोही स्वंयमको मञ्जुरी सनाखत रहेकोले मोही लगत कट्टाको लागि सिफारिस गरी पाऊँ भनी जग्गाधनी श्री/सुश्री/श्रीमती 
    {{ Form::text('landOwnerName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'landOwnerName', 'required' => 'required']) }}  
     ले यस वडा कार्यालयमा निवेदन दिनु भएकामा सो सम्बन्धमा मिति 
    {{ Form::text('sarjaminDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'sarjaminDate',  'onfocus' => 'showNdpCalendarBox("sarjaminDate")']) }}
  </p> 
</div>
</div>
<div class="text-right btm-last">
  <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required" value="{{!empty($data)? $data->authorizedPerson : '' }}"></p>
  <p> 
    <b>
      <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
        @if(count($deginationsId) > 0)
        @foreach($deginationsId as $deg)
        <option value="{{ $deg->id }}" {{ isset($data->deginations_id ) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
          {{ $deg->nameNep }} 
        </option>
        @endforeach
        @endif
      </select>
    </b>
  </p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>