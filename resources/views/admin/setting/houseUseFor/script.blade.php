<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmedithouseUseFor']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of electrical device is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmhouseUseFor']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of electrical device is required"
            }
        });
	var btnAddhouseUseFor = $('.btnAddhouseUseFor');
	btnAddhouseUseFor.on('click', function(e){
		$('#houseUseForModel').modal('show'); 
	});

	var btnEdithouseUseFor = $('.btnEdithouseUseFor');
	btnEdithouseUseFor.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.houseUseForId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#edithouseUseForModel").modal('show');
			}
		})
	});

	var frmUpdatehouseUseFor = $('#frmUpdatehouseUseFor');
  frmUpdatehouseUseFor.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.houseUseForId').val();
    var url  = "{{URL::to('/')}}" + "/setting/houseUseFor/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#edithouseUseForModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#edithouseUseForModel').modal('hide');
});
});
</script>