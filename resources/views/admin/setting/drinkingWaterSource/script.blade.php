<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditdrinkingWaterSource']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of drinking water source is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmdrinkingWaterSource']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of drinking water source is required"
            }
        });
	var btnAdddrinkingWaterSource = $('.btnAdddrinkingWaterSource');
	btnAdddrinkingWaterSource.on('click', function(e){
		$('#drinkingWaterSourceModel').modal('show'); 
	});

	var btnEditdrinkingWaterSource = $('.btnEditdrinkingWaterSource');
	btnEditdrinkingWaterSource.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.drinkingWaterSourceId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editdrinkingWaterSourceModel").modal('show');
			}
		})
	});

	var frmUpdatedrinkingWaterSource = $('#frmUpdatedrinkingWaterSource');
  frmUpdatedrinkingWaterSource.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.drinkingWaterSourceId').val();
    var url  = "{{URL::to('/')}}" + "/setting/drinkingWaterSource/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editdrinkingWaterSourceModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editdrinkingWaterSourceModel').modal('hide');
});
});
</script>