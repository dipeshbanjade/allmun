<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class diffEngSpellCertVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:diff_eng_spell_certs',
            'issuedDate' => 'required',
            'chalaniNumber' => 'required|numeric|unique:diff_eng_spell_certs',
            'municipalityName' => 'required',
            'wardNumber' => 'required|numeric',
            'municipalityAddress' => 'required',
            'municipalityType' => 'required',
            'municipalityWard' => 'required|numeric',
            'personPrefix' => 'required',
            'personName' => 'required',
            'trueDocument' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
