<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBijuliJadanShifarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bijuli_jadan_shifaris', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->string('refCode')->unique();

            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 




            $table->date('issuedDate');
            $table->string('chalaniNumber');   

            $table->string('accountHolderName');   
            $table->string('accountHolderAddress');   

            $table->string('municipalityName');
            $table->string('wardNumber');
            $table->string('sabikAddress');
            $table->string('ownerName');            
            $table->string('kittaNo');            
            $table->date('buildDate');            
            $table->string('ampere');            
            $table->string('category');            

            $table->text('tableData');

            $table->text('bodartha');




            
            $table->string('authorizedPerson');

            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bijuli_jadan_shifaris');
    }
}
