<div class="card-body row">
  <div class="row">

  <div class="col-sm-6">
    <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
      <label class = "mdl-textfield__label">RefCode</label>
    </div>
  </div>

  <div class="col-sm-6">
    <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      {{ Form::text('citizenNo', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
      <label class = "mdl-textfield__label">Citizen Number</label>
    </div>
  </div>

    <div class="col-sm-6">
      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('name', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">Enter full name</label>
      </div>
    </div>
    <div class="col-sm-6">
      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::email('email', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">Email</label>
      </div>
    </div>

     <div class="col-sm-6">
      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('phoneNumber', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">Phone Number</label>
      </div>
    </div>
   <!--  -->
   <div class="col-sm-6">
     <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      <label id="dropLabel">Select Staff Role</label>
      <select name="roles_id" class="form-control">
       <option value="">Select user role</option>
        @if(count($roleId) > 0)
           @foreach($roleId as $role)
               <option value="{{ $role->id }}">
                 {{ $role->roleName }}
               </option>
           @endforeach
        @endif
     </select>
   </div>
 </div>
 <!--  -->
 <div class="col-sm-6">
     <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      <label id="dropLabel">Department</label>
      <select name="departments_id" class="form-control">
       <option value="">Select department</option>
        @if(count($dept) > 0)
           @foreach($dept as $dep)
               <option value="{{ $dep->id }}">
                 {{ $dep->nameNep }} - {{ $dep->nameEng }}
               </option>
           @endforeach
        @endif
     </select>
   </div>
 </div>
 <!--  -->
 <div class="col-sm-6">
     <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      <label id="dropLabel">Staff Type</label>
      <select name="staff_types_id" class="form-control">
       <option value="">Select Staff Type</option>
        @if(count($staffType) > 0)
           @foreach($staffType as $st)
               <option value="{{ $st->id }}">
                 {{ $st->nameNep }} - {{ $st->nameEng }}
               </option>
           @endforeach
        @endif
     </select>
   </div>
 </div>
 <!--  -->
 <div class="col-sm-6">
     <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      <label id="dropLabel">Degination</label>
      <select name="deginations_id" class="form-control">
       <option value="">Select Degination</option>
        @if(count($deg) > 0)
           @foreach($deg as $deg)
               <option value="{{ $deg->id }}">
                 {{ $deg->nameNep }} - {{ $deg->nameEng }}
               </option>
           @endforeach
        @endif
     </select>
   </div>
 </div>
 <!--  -->
 <div class="col-sm-6">
     <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      <label id="dropLabel">Provinces</label>
      <select name="provinces_id" class="form-control">
       <option value="">Provinces</option>
        @if(count($prov) > 0)
           @foreach($prov as $prov)
               <option value="{{ $prov->id }}">
                 {{ $prov->name }}
               </option>
           @endforeach
        @endif
     </select>
   </div>
 </div>
 <!--  -->
 <div class="col-sm-6">
     <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
      <label id="dropLabel">District</label>
      <select name="districts_id" class="form-control">
       <option value="">Select district</option>
        @if(count($dis) > 0)
           @foreach($dis as $dis)
               <option value="{{ $dis->id }}">
                 {{ $dis->districtNameNep }} -  {{ $dis->districtNameEng }}
               </option>
           @endforeach
        @endif
     </select>
   </div>
 </div>
 <!--  -->
 <div class="col-sm-6">
   <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
     <label id="dropLabel" class="col-md-4 col-form-label text-md-right">Password</label>
     <input id="password" type="password" class="form-control" name="password" required>
     <span class="invalid-feedback" role="alert">
       <strong></strong>
     </span>
   </div>
 </div>

 <div class="col-sm-6">
  <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
   <label for="password-confirm" id="dropLabel" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
   <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
 </div>
</div>

</div>
</div>