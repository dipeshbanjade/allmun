<script type="text/javascript">
    $(function(){
        /*add buttoin*/
        $("form[name='frmeditjatjati']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 100
                }
            },
            messages: {
  
                nameEng : "The name of JatJati is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmjatjati']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 100
                }
            },
            messages: {
      
                nameEng : "The name of JatJati is required"
            }
        });

	var btnAddJatjati = $('.btnAddJatjati');
	btnAddJatjati.on('click', function(e){
		$('#jatjatiModel').modal('show'); 
	});

	var btnEditjatjati = $('.btnEditjatjati');
	btnEditjatjati.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.jatjatiId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editJatJatiModel").modal('show');
			}
		})
	});

	var frmUpdatejatjati = $('#frmUpdatejatjati');
  frmUpdatejatjati.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.jatjatiId').val();
    var url  = "{{URL::to('/')}}" + "/setting/jatjati/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editJatJatiModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editJatJatiModel').modal('hide');
});

  });
</script>