<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVarasisAuthioritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('varasis_authiorities', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->string('refCode', 100)->unique();

            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 


            $table->date('issuedDate');
            $table->string('chalaniNum');
            $table->string('successorAge');
            $table->string('successorName');
            $table->string('successorMunicipality');
            $table->string('successorWard');
            $table->string('casePerson');
            $table->string('caseName');
            $table->string('caseMunicipality');
            $table->string('casereason');
            $table->string('caseRunMunicipality');
            $table->string('agreedOn');
            $table->string('manisMunicipality');
            $table->string('manisWard');
            $table->string('manisAge');
            $table->string('manisName');
            $table->string('caseOnMunicipality');
            $table->string('rohabar');
            $table->string('transfer');
            $table->string('year');
            $table->string('month');
            $table->string('day');
            $table->string('eachday');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');
            
            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');
            
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('varasis_authiorities');
    }
}
