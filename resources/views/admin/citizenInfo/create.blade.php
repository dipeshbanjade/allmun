@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
    
    <div class="card-head">
        <header>Citizen Form</header>
    </div>
    {{ Form::open(['route' => 'citizenInfo.store', 'name' => 'frmcitizenInfo', 'files' => true, 'enctype' => 'multipart/form-data']) }}
    @include('admin.citizenInfo._form')
    <!--  -->
    <p class="p-4">
        <button type="button" class="btn btn-danger pull-right margin-left-5" onclick="history.back()"> <span>@lang('commonField.button.back')</span> 
        </button>
         {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success pull-right']) }}
        {{ Form::close() }}
    </p>
</div>


@endsection
@section('customStyle')
<!-- Citizen Form CSS linking -->
<style type="text/css">
    .card-head{
        background:#D5D5D5;
    }
</style>


<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/citizenForm/styles.css') }}">

<!-- Citizen Form CSS linking -->
<!-- pic edit css -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/citizenForm/picedit.min.css') }}">
<!-- Photo editor js -->
@endsection
@section('custom_script')
@include('admin.citizenInfo.script')
<script type="text/javascript" src="{{ asset('lib/js/picedit.js') }}"></script>
@endsection