<script type="text/javascript">
    $(function(){
        /*add buttoin*/
        $("form[name='frmeditLeaveType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                },
                daysLeave : {
                    required : true,
                    number : true
                }
            },
            messages: {
                nameEng : "The name of Leave Type is required",
                daysLeave : "The days leave field is required with only number character"
            }
        });

    });
    $(function(){
        /*add buttoin*/
        $("form[name='frmLeaveType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                },
                daysLeave : {
                    required : true,
                    number : true
                }
            },
            messages: {
                nameEng : "The name of Leave Type is required",
                daysLeave : "The days leave field is required with only number character"
            }
        });


        var btnAddLeaveType = $('.btnAddLeaveType');
        btnAddLeaveType.on('click', function(){
            $('#myModal').modal('show');
        });


        var btnEditLeaveType = $('.btnEditLeaveType');
        btnEditLeaveType.on('click', function(e){
            e.preventDefault();
            var url = $(this).data('url');
            $.ajax({
                'type': 'GET',
                'url': url,
                success: function (response) {
                    console.log(response);
                    $('.leaveTypeId').val(response.id),
                    $('.txtnameNep').val(response.nameNep);
                    $('.txtnameEng').val(response.nameEng);
                    $('.txtdaysLeave').val(response.daysLeave);
                    $("#editLeaveTypeModel").modal('show');
                }
            })
        });

        var frmUpdateLeaveType = $('#frmUpdateLeaveType');
        frmUpdateLeaveType.on('submit', function(e){
            e.preventDefault();
            var data = $(this).serialize();
            var id   = $('.leaveTypeId').val();
            var url  = "{{URL::to('/')}}" + "/setting/leaveType/"+id+"/update";
            $.ajax({
                'type' : 'POST',
                'url'  : url,
                'data'    : data,
                success : function(response){
                    console.log(response);
                    if (response.success==true) {
                        $('.infoDiv').append(response.message).addClass('alert alert-success');
                    }
                    if (response.success==false) {
                        $.each(response.message, function(key, value){
                            $('.infoDiv').append(value).addClass('alert alert-danger');
                        })
                    }
                },complete:function(){
                    $("#editLeaveTypeModel").modal('hide');
                    setTimeout(location.reload.bind(location), 1500);
                }
            })
            .fail(function (response) {
                alert('data already exists');
            });
            $('#editLeaveTypeModel').modal('hide');
        });

    });
</script>