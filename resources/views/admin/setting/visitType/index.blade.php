@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
                <header><i class="fa fa-eye">&nbsp;@lang('commonField.extra.visit_type')</i></header>
            </div>

            <button id="" class="btnAddVisitType btn btn-primary pull-right"><i class="fa fa-plus"></i><span>@lang('commonField.extra.createVisitType')</span></button>
            <div class="table-scrollable" id="dropLabel">
              @if(count($allVisitType) > 0)
                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
                    <thead>
                        <tr>
                            <th>@lang('commonField.extra.sn')</th>
                            <th>@lang('commonField.extra.refCode')</th>
                            <th>@lang('commonField.extra.visit_type') </th>
                            <th>@lang('commonField.extra.desc')</th>
                            <th> @lang('commonField.extra.action') </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php  $count = 1   @endphp
                                @foreach($allVisitType as $visitType)
                                   <tr class="odd gradeX">
                                       <td width="5%">{{ $count ++ }}</td>
                                       <td>{{ $visitType->refCode }}</td>
                                       <td>{{ $visitType->nameNep }}  - {{ $visitType->nameEng }} </td>
                                       <td>{{ $visitType->desc }} </td>
                                       <td>                                
                                           <a href="" class="btn btn-primary btn-xs">
                                               <i class="fa fa-pencil updateVisitType" data-toggle="modal" data-target="#updateVisitType" data-id="{{ $visitType->id }}" data-url="{!! route('admin.visitType.edit', $visitType->id) !!}"></i>
                                           </a>
                                          
                                           <!--  -->
                                           <a href="#" class="txt-color-red deleteMe" 
                                                 data-url="{!! route('admin.visitType.delete', $visitType->id ) !!}" title="delete visit type" data-name="{{ $visitType->refCode }}" data-id = "{{ $visitType->id }}">
                                               <i class="btn btn-danger btn-xs fa fa-trash-o"></i>
                                            </a>

                                               <a href="#" class="delete_data" id="" >
                                                     <button class="btn btn-danger btn-xs">
                                                         <i class="fa fa-eye-slash"></i>
                                                     </button>
                                                   </a>
                                               </td>
                                           </tr>
                                @endforeach
                            </tbody>
                        </table>
                         <?php echo $allVisitType->render(); ?>
                         @else
                         <h3>@lang('commonField.extra.pleaseInsertData')</h3>
                      @endif
                        
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('modelSection')
        <div class="modal" id="frmAddVisitType">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title" id="dropLabel"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.addVisitType')</i></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    {{ Form::open(['route' => 'admin.visitType.store', 'name' => 'frmVisitType']) }}
                    <div class="modal-body">
                        @include('admin.setting.visitType._form')
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <div class="col-lg-12 p-t-20 text-right">
                            {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
                            <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
                        </div>
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>

        <!--  -->
        <!-- edit model -->
        <div class="modal" id="frmUpdateVisitType">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title" id="dropLabel">
                           <i class="fa fa-plus">&nbsp;@lang('commonField.extra.editVisitType')</i></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    {{ Form::open(['name' => 'frmEditVisitType', 'id' => 'frmUpdateVisitTyp']) }}
                    <div class="modal-body">
                        {{ Form::hidden('visitTypeId',null, ['class' => 'visitTypeId']) }}
                        @include('admin.setting.visitType._form')
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <div class="col-lg-12 p-t-20 text-right">
                            {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
                            <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
                        </div>
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
        @endsection
<!-- modal ended here -->
@section('custom_script')
     @include('admin.setting.visitType.script')
@endsection