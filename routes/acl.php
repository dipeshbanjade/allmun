<?php
Route::prefix('acl')->group(function(){
  Route::get('/role', ['uses' => 'admin\setting\acl\role\roleController@index', 'userlevel'=>['user-1', 'user-2', 'user-3']])->name('system.role.index');

  Route::post('/role/store',      'admin\setting\acl\role\roleController@store')->name('system.role.store');

  Route::get('/role/{id}/edit',   'admin\setting\acl\role\roleController@edit')->name('system.role.edit');


  Route::post('role/{id}/update', 'admin\setting\acl\role\roleController@update')->name('system.role.update');

  Route::get('/role/{id}',        'admin\setting\acl\role\roleController@destroy')->name('system.role.delete');
  
  Route::get('roleChangeStatus', 'admin\setting\acl\role\roleController@changeStatus')->name('system.role.changeStatus');

  /*----------------------permission-----------------------------------------*/


  Route::get('/permission', 'admin\setting\acl\permission\permissionController@index')->name('system.permission.index');

  Route::post('/permission/store',      'admin\setting\acl\permission\permissionController@store')->name('system.permission.store');

  Route::get('/permission/{id}/edit',   'admin\setting\acl\permission\permissionController@edit')->name('system.permission.edit');


  Route::post('permission/{id}/update', 'admin\setting\acl\permission\permissionController@update')->name('system.permission.update');

  


  /*-------------Assign role permission----------------*/
  Route::get('/assignRolePermission', 'admin\setting\acl\rolePermission\assignPermissionRoleController@index')->name('system.assignRolePermission.index');

    Route::get('/assignRolePermission/create', 'admin\setting\acl\rolePermission\assignPermissionRoleController@create')->name('system.assignRolePermission.create');
    
  Route::post('/assignRolePermission/store',      
       'admin\setting\acl\rolePermission\assignPermissionRoleController@store')->name('system.assignRolePermission.store');

  Route::get('/assignRolePermission/{id}/edit',   'admin\setting\acl\rolePermission\assignPermissionRoleController@edit')->name('system.assignRolePermission.edit');

  Route::post('assignRolePermission/{id}/update', 'admin\setting\acl\rolePermission\assignPermissionRoleController@update')->name('system.assignRolePermission.update');

  Route::post('/assignRolePermission/{id}',        'admin\setting\acl\rolePermission\assignPermissionRoleController@destroy')->name('system.assignRolePermission.delete');




  /*user and role*/   // getting this route problem
  Route::get('/userDetails', 'admin\setting\acl\userDetails\userDetailsController@index')->name('system.userDetails.index');
  

    Route::get('/userDetails/create', 'admin\setting\acl\userDetails\userDetailsController@create')->name('system.userDetails.create');
    
  Route::post('/userDetails/store',      
       'admin\setting\acl\userDetails\userDetailsController@store')->name('system.userDetails.store');

});

?>