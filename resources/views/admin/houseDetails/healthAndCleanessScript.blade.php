<script type="text/javascript">
$(function(){
  $("form[name='frmFamiyHealthPosition']").validate({
  		rules:{
  			otherTreatmentArea:{
  				required:function(ele){
  					return $('[name=treatmentArea]:checked').length<=0;
  				}
  			},
  			timeToHealthCenter:{
  				required: true
  			},
  			timeToHospital:{
  				required:true
  			},
  			haveLTDisease:{
  				required: true
  			},
  			hsLTDiseaseDetail:{
  				required:true
  			}
  		},
  		messages:{
  			otherTreatmentArea : " The treatment area is required",
  			timeToHealthCenter: " The total time taken to reach Health center is required",
  			timeToHospital: "The total time taken to reach hospital is required",
  			haveLTDisease: " This filed is required",
  			hsLTDiseaseDetail: " This filed is required"
  		}
  });


});	
if($("input[name=haveLTDisease]")[0].checked == true && $("input[name=haveLTDisease]")[0].value == 1){
	$(".familyDiseaseTbl").show();
}
function displayFDiseaseTbl(select){ // display and hide long term disease tbl of fmHealthPos php
	if(select.value==1){
		$(".familyDiseaseTbl").show();
	}else{
		$(".familyDiseaseTbl").hide();
		$("input[name^=hsLTDiseaseDetail]").each(function(){
			$(this).val('');
		});
	}
}

// female disease discription tab no 2 form name = fmFemaleDisease
$('.haveDisease').change(function(){ //This require the disease name if the haveDisease filed is selected
	var name= $(this).attr('name');
	var index= name.substr(16,1);
	$("form[name='frmFemaleDisease']").validate();
	var hsFemaleDisease= $("input[name^=hsFemaleDisease]");
	if($(this).is(':checked') == true){
		hsFemaleDisease.filter('input[name*="['+index+'][diseaseName]"]').each(function(){
			$(this).rules("add",{
				required:true,
				messages:{
					required: "Atleast one of the disease name should be selected"
				}
			});
		});		
	}else{
		hsFemaleDisease.filter('input[name*="['+index+'][diseaseName]"]').each(function(){
			$(this).rules("add",{
				required:false
			});
		});		
	}
});


/*----------------------Error in*/
// Child vactine description tab no 3 
// form name fmFemaleDisease
 // $('.haveVaccine').change(function(){
 // 	var name= $(this).attr('name');
 // 	var index= name.substr(10,1);
 // 	$("form[name='frmInfantVacc']").validate();
 // 	var infantVac= $("input[name^='infantVac']");
	// if($(this).is(':checked') == true){
	// 	// var i= $("input[name*='infantVac[2][times]']");
	// 	// alert(i[0].value);
		
	// 	// $('input[name*="infantVac['+index+'][times]"').rules("add", {
	// 	//        required: true,
	// 	//        messages: {
	// 	//            required: "Atleast one should be selected"
	// 	//        }
 // 	// 		});
	// 	infantVac.filter('input[name*="['+index+'][times]"]').each(function(){
	// 		$(this).rules("add", {
	// 	       required: true,
	// 	       messages: {
	// 	           required: "Atleast one should be selected"
	// 	       }
 // 			});
	// 	});		
	// }else{
	// 	infantVac.filter('input[name*="['+index+'][times]"]').each(function(){
	// 		$(this).rules("add", {
	// 	       required: false,
	// 	    });
	// 	});		
	// }
 // });

// Pregnancy Activity
// Form name pregAct
$(".hsPregAction").change(function(){
	var name= $(this).attr('name');
	var index= name.substr(20,1);
	$("form[name='frmPregActionFrm']").validate();
 	var hsPregActionDetails= $("input[name^=hsPregActionDetails]");
 	if($(this).is(':checked')== true){
 		hsPregActionDetails.filter('input[name*="['+index+'][pregWomenAge]"]').each(function(){
 			$(this).rules("add", {
 			       required: true,
 			       number:true,
 			       messages: {
 			           required: "Age is required"
 			       }
 			});
 		});
 		hsPregActionDetails.filter('input[name*="['+index+'][treatmentCount]"]').each(function(){
 			$(this).rules("add", {
 			       required: true,
 			       number:true,
 			       messages: {
 			           required: "The number of tratment is required"
 			       }
 			});
 		}); 		
 		hsPregActionDetails.filter('input[name*="['+index+'][tetanusInjCount]"]').each(function(){
 				$(this).rules("add", {
 			       required: true,
 			       number:true,
 			       messages: {
 			           required: "The total number of tetanaus injection taken is required"
 			       }
 			   });
 		}); 		
 		hsPregActionDetails.filter('input[name*="['+index+'][ironTabletCount]"]').each(function(){
 			$(this).rules("add", {
		       required: true,
		       number:true,
		       messages: {
		           required: "The total number of iron tablet taken is required"
		       }
		   });
 		});
 	}else{
 		// hsPregActionDetails.filter('input[name*="['+index+'][pregWomenAge]"]').each(function(){
			// $(this).rules("add", {
		 //       required: false,
		 //    });
 		// 	$(this).val('');
 		// });
 		hsPregActionDetails.filter('input[name*="['+index+'][treatmentCount]"]').each(function(){
			$(this).rules("add", {
		       required: false,
		    });
 			$(this).val('');
 		}); 		
 		hsPregActionDetails.filter('input[name*="['+index+'][tetanusInjCount]"]').each(function(){
			$(this).rules("add", {
		       required: false,
		    });
 			$(this).val('');
 		}); 		
 		hsPregActionDetails.filter('input[name*="['+index+'][ironTabletCount]"]').each(function(){
			$(this).rules("add", {
		       required: false,
		    });
 			$(this).val('');
 		});
 		hsPregActionDetails.filter('input[name*="['+index+'][havHookWormMed]"]').each(function(){
			$(this).rules("add", {
		       required: false,
		    });
 			$(this).prop('checked',false);
 		}); 		
 		hsPregActionDetails.filter('input[name*="['+index+'][havePregTreatment]"]').each(function(){
			$(this).rules("add", {
		       required: false,
		    });
 			$(this).prop('checked',false);
 		}); 		
 		hsPregActionDetails.filter('input[name*="['+index+'][otherTreatment]"]').each(function(){
			$(this).val("");
 		});
 	}
});

/*------------------------------fmHivPreg------------------*/
function displayHivPregTbl(select){
	if(select.value == 1){
		$('.hivPregTbl').show();
	}else{
		$('.hivPregTbl').hide();
		$("input[name^=hsHIVPregDetails]").prop("checked",false);
	}
}
$('.hsHIVPregDetails').change(function(){
	var name= $(this).attr('name');
	var index= name.substr(17,1);
	$("form[name='frmHIVPregnancy']").validate();
	var hsHIVPregDetails= $("input[name^=hsHIVPregDetails]");
	if($(this).is(':checked')== true){
	
		hsHIVPregDetails.filter('input[name*="['+index+'][haveProphylaxis]"]').each(function() {
			$(this).rules('add',{
				required:true,
			})
	    });

	}else{
		hsHIVPregDetails.filter('input[name*="['+index+'][haveProphylaxis]"]').each(function() {
			$(this).prop('required',false);
		});
	}

});


$('.requireFiltrationType').change(function(){
	var name = $(this).attr('name');
	var index= name.substr(24,1);
	$("form[name='frmFmUsingDrinkingWater']").validate();
	var hsFmUsDrinkWaterDetails = $('input[name^=hsFmUsDrinkWaterDetails]:checkbox');
	if($(this).is(':checked') == true){
		 var length=$('input[name*="hsFmUsDrinkWaterDetails['+index+']"]:checked').length;
		 if(length <=1){
				$('input[name*="hsFmUsDrinkWaterDetails['+index+'][isDirectConsumption]"').rules('add',{
					// required:function(){
					// 	if(($('input[name^="hsFmUsDrinkWaterDetails['+index+'"]:checked').length)<=2){
					// 		return true;
					// 	}
					// }
					// required: true,
				});
		 	
		 }
			
		// 	hsFmUsDrinkWaterDetails.filter('input[name*="['+index+']"]').each(function(){
		
		// 	$(this).rules("add",{
		// 		required:true,
		// 		minlength:1,
		// 		 messages: {
		// 	            required: "Name is Mandatory"
		// 	        }
		// 	});
		// });
	}
});

</script>