<div class="right_col nep" role="main" style="min-height: 521px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">फरक फरक व्यहोरा प्रमाणित</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">

         @include('admin.shifaris.municipalityDetail')
         
         <div class="row top-part">
          <div class="col-md-6">
           <p align="left">
            <b>@lang('commonField.extra.refCode'):</b>
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
            @endif
          </p>
          <p align="left">
            <b> च. नं.:
             {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
           </b>
         </p>
       </div>
       <div class="col-md-6">
        <p align="right">
          <b class="mt">मिति :
           {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
         </b>
       </p>
     </div>
   </div>
   <div class="row">
    <div class="col-md-12">
      <p align="center" class="font-size-24"></p>
      <h4 align="center"><b>विषय: प्रमाणित सिफारिस गरिएको बारे ।</b> </h4>
      {{ Form::hidden('letter_subject', 'प्रमाणित सिफारिस गरिएको बारे', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
    </div>
  </div>
  <div class="row content-para">
    <div class="col-md-12">
      <p align="left"> उपरोक्त विषयमा 
        <b>
          {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
        </b>
        वडा नं.
        <b>
          {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
        </b>
        (साविक 
        <!--SABIK ADDRESS START -->
        {{ Form::text('municipalityAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityAddress', 'required' => 'required']) }}
        <!-- SABIK ADDRESS END -->
        <b>
         <select onchange="changeSelect(this)" name="municipalityType">
          <option value="गा.वि.स." 
          {{ !empty($data->municipalityType) && $data->municipalityType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
          <option value="नगरपालिका" 
          {{ !empty($data->municipalityType) && $data->municipalityType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
          <option value="उप महानगरपालिका" 
          {{ !empty($data->municipalityType) && $data->municipalityType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
          <option value="महानगरपालिका" 
          {{ !empty($data->municipalityType) && $data->municipalityType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
        </select>
      </b>
      , वडा नं. 
      {{ Form::text('municipalityWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityWard', 'required' => 'required']) }}
      ) निवासी 
      <b>
        <select onchange="changeSelect(this)" name="personPrefix">
      <option value="श्री"
      {{ !empty($data->personPrefix) && $data->personPrefix == 'श्री' ? 'selected' : '' }}>श्री</option>
      <option value="सुश्री"
      {{ !empty($data->personPrefix) && $data->personPrefix == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
      <option value="श्रीमती"
      {{ !empty($data->personPrefix) && $data->personPrefix == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
        </select>
      </b> 
      {{ Form::text('personName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'personName', 'required' => 'required']) }} 
      ले दिनु भएको निवेदन अनुसार निजको नागरिकता प्रमाण पत्र नं. 
      {{ Form::text('citizenshipNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'citizenshipNumber', 'required' => 'required']) }} 
      मिति 
      {{ Form::text('citizenshipDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'citizenshipDate',  'onfocus' => 'showNdpCalendarBox("citizenshipDate")']) }}
      मा नागरिकता प्रमाण पत्रमा  
      <select onchange="changeSelect(this)" name="mistakeType" onchange="myFunction(event)">
        <option value="नाम थर" {{ !empty($data->mistakeType) && $data->mistakeType == 'नाम थर' ? 'selected' : '' }}>नाम थर</option>
        <option value="जन्ममिति" {{ !empty($data->mistakeType) && $data->mistakeType == 'जन्ममिति' ? 'selected' : '' }}>जन्ममिति</option>
        <option value="नाम थर,जन्ममिति" {{ !empty($data->mistakeType) && $data->mistakeType == 'नाम थर,जन्ममिति' ? 'selected' : '' }}>नाम थर,जन्ममिति</option>
      </select> 
      {{ Form::text('wrongDetail', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wrongDetail', 'required' => 'required']) }} 
      र निजको 
      <select onchange="changeSelect(this)" name="rel_type" onchange="relation(event)">
        <option value="श्रीमान" {{ !empty($data->rel_type) && $data->rel_type == 'श्रीमान' ? 'selected' : '' }}>श्रीमान</option>
        <option value="श्रीमती" {{ !empty($data->rel_type) && $data->rel_type == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
        <option value="आफ्नो" {{ !empty($data->rel_type) && $data->rel_type == 'आफ्नो' ? 'selected' : '' }}>आफ्नो</option>
      </select>
      <input-group id="relnameanddetail">
      {{ Form::text('paperHolder', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'paperHolder', 'required' => 'required']) }} 
      को
    </input-group>&nbsp; 
    {{ Form::text('paperType', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'paperType', 'required' => 'required']) }} 
    मा  नाम थर
    {{ Form::text('paperWrongInfo', 'नाम थर', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'paperWrongInfo', 'required' => 'required', 'readonly' => 'readonly']) }} 
    {{ Form::text('paperName', 'नाम थर', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'paperName', 'required' => 'required']) }} &nbsp;
    भई फरक परेकाले उल्लेखित फरक फरक 
    {{ Form::text('paperWrongInfo2', 'नाम थर', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'paperWrongInfo2', 'required' => 'required', 'readonly' => 'readonly']) }} 
    फरक व्यक्तिको नभई एकै&nbsp;व्यक्तिको भएकाले निजको 
    {{ Form::text('name', 'श्रीमानको', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'rel', 'required' => 'required', 'readonly' => 'readonly']) }} 
    &nbsp;&nbsp;&nbsp;
    {{ Form::text('documentName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'documentName', 'required' => 'required']) }} 
    मा उल्लेखित बमोजिम  
    {{ Form::text('documentWrongInfo', 'नाम थर', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'paperWrong2', 'required' => 'required']) }}
    संशोधन गरी निजलाई नागरिकता प्रमाण पत्र प्रतिलिपि उपलब्ध गराई दिनहुन प्रमाणित सिफारिस गरिएको&nbsp;व्यहोरा अनुरोध छ।&nbsp;
  </p>
</div>
</div>
<div class="text-right btm-last">
  <p>   
    {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }}
  </p>
  <p> <b>
    <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
      @if(count($deginationsId) > 0)
      @foreach($deginationsId as $deg)
      <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
        {{ $deg->nameNep }} 
      </option>
      @endforeach
      @endif
    </select>
  </b> </p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>
</div>
</div>
</div>
</div>