<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardrecommCourt']").validate({
			rules:{
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				courtAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number : true
				},
				organizationAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				organizationType : {
					required : true					
				},
				organizationWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number : true
				},
				firstPersonTitle : {
					required : true					
				},
				firstPersonName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				firstAccusion : {
					required : true					
				},
				secondPersonTitle : {
					required : true					
				},
				secondPersonName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				secondAccusion : {
					required : true		
				},
				districtCourt : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				case : {
					required : true,
					minlength : 3,
					maxlength : 200
				}
			},
			messages: {

				issuedDate : "Issue Date is required",
				chalaniNum : "Chalani Number is required",
				courtAddress : "Court Address is required",
				municipalityName : "Municipality Name is required",
				municipalityWard : "Municipality Ward is required",
				organizationAddress : "Organization Address is required",
				organizationType : "Organization Type is required",
				organizationWard : "Organization Ward is required",
				firstPersonTitle : "First Person Title is required",
				firstPersonName : "First Person Name is required",
				firstAccusion : "First Accusion is required",
				secondPersonTitle : "Second Person Title is required",
				secondPersonName : "Second Person Name is required",
				secondAccusion : "Second Accusion is required",
				districtCourt : "District Court is required",
				case : "Case is required"
			}
		});
	});
	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='firstPersonName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>