@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('custom_css')
 <style>
  .modal-title{
      padding-left: 330px;
  }
  .modal .modal-header {
    border-bottom: 3px solid #255E8A;
  }
  @media(max-width: 768px){
    .modal-title{
      padding-left: 160px;
    } 
  }
  @media(max-width: 425px){
    .modal-title{
      padding-left: 130px;
    }
  } 
  @media(max-width: 375px){
    .modal-title{
      padding-left: 110px;
    }
  }
  @media(max-width: 320px){
    .modal-title{
      padding-left: 85px;
    }
  } 
</style> 
@endsection
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
                <header>
                <h3><span>
                <i class="fa fa-eye">&nbsp;@lang('commonField.dartaChalani.chalani')</i></span></h3></header>
            </div>

            <button id="" class="chalaniFrm btn btn-primary pull-right"><i class="fa fa-plus"></i><span>@lang('commonField.dartaChalani.createChalani')</span></button>
            <div class="table-scrollable" id="dropLabel">
              <table id="example4" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example4_info">
                         <thead>
                          <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">
                              @lang('commonField.extra.sn')
                            </th>
                            
                            <th class="sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">@lang('commonField.dartaChalani.date')</th>

                            <th class="sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 150px;">@lang('commonField.dartaChalani.chalaniNo').</th>


                            <th class="sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">@lang('commonField.dartaChalani.subject')</th> 
                            <th class="sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 300px;">@lang('commonField.dartaChalani.page')</th>

                            <th class="sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">
                              @lang('commonField.dartaChalani.office')
                            </th> 

                            <th class="sorting" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">
                              @lang('commonField.extra.time')
                            </th> 

                          <th style="width: 250px;">@lang('commonField.extra.action')</th>
                        </tr>
                  </thead>
                  <tbody>
                    <?php 
                         $count = 1;
                    ?>
                    @if($challaniList)
                       @foreach($challaniList as $chalani)
                          <?php 
                             $name =  getUserDetails($chalani->users_id);   
                           ?>
                          <tr>
                             <td title="{!! $name->name !!}">{{ $count ++ }}</td>
                             <td>{{ $chalani->chalaniDate }}</td>
                             <td>{{ $chalani->chalaniNo }}</td>
                             <td>{{  $chalani->chalaniSubject }}</td>
                             <td>{{ $chalani->chalaniPage }}</td>
                             <td>
                                @if($chalani->other_offices_id)
                                    {{ $lang == 'np' ? $chalani->otherOffices->nameNep : $chalani->otherOffices->nameEng  }}
                                @endif
                            </td>
                            <td>{{ $chalani->created_at->diffForHumans() }}</td>
                             <td>
                          
                              <span> 
                                <a href="{{ route('editChalani', $chalani->idEncript) }}"  title="View & Edit chalani details">
                                  <i class="fa fa-fw fa-lg fa-pencil-square-o" data-toggle="modal" data-target="#updateUNit" data-id=""></i>
                              </a>
                              </span>

                              <span>
                                <a href="" class="txt-color-red deleteMe" 
                                    title="delete chalani" data-name="{{ $chalani->chalaniNo }}" data-id ="{{ $chalani->id }}" data-url="{{ route('deleteChallani', $chalani->id) }}">
                                   <i class="fa fa-fw fa-lg fa-trash-o deletable text-danger"> </i> </a>
                               </span>
                             </td>
                          </tr>
                       @endforeach
                    @endif
                  </tbody>
                </table>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
        @endsection
        @section('modelSection')
         <div id="frmAddChalani" class="modal fade bd-example-modal-lg" role="dialog" style="z-index: 9996;">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 align="center">@lang('commonField.dartaChalani.addChalani')</h4>
                  </div>
                  <div class="modal-body">
                       {!! Form::open(['route'=>'challaniStore', 'name'=>'frmchalani', 'method'=>'post', 'id'=>'addChalani']) !!}
                            @include('admin.dartaChalani.chalani._form')
                            <p class="pull-right">
                                {!! Form::submit(__('commonField.button.create'), ['class'=>'btn btn-success']) !!}
                                <!-- <input type="submit" name="" class="btn btn-success"> -->
                                <button class="btn btn-danger" onclick="window.history.go(0); return false;">@lang('commonField.button.close')</button>
                          </p>
                      {!! Form::close() !!}
                       {{-- <button id="btnAddCompanyDetail"  class="btn btn-primary pull-right"><i class="fa fa-plus"></i><span></span></button> --}}
                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>

              </div>
            </div>

            <!-- modal update  -->
            <!-- modal -->
                  <div class="row">
                    <div id="updateChalani" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Update Chalani</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['id'=>'frmUpdatecompany', 'name'=>'frmCountry']) !!}
                                {{ Form::hidden('companyId',null, ['class'=>'companyId']) }}
                               
                                @include('admin.dartaChalani.chalani._form')

                                <div class="form-group col-sm-12 ">
                                   <p class="pull-right">
                                      {{ Form::submit(__('commonField.button.update'), ['class'=>'btn btn-success frmUpdatecompany']) }}


                                <button class="btn btn-danger" onclick="window.history.go(0); return false;">@lang('commonField.button.close')</button>
                                 </p>
                                </div>
                           {!! Form::close() !!}
                        </div>
                        <div class="modal-footer">
                               
                      </div>
                    </div>
                  </div>
                  </div>
                  </div>

            {{-- model add Comapny --}}
           <div id="frmAddcompanyModel" class="modal fade" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('commonField.dartaChalani.addCompany')</h4>
                  </div>
                  <div class="modal-body">
                       {!! Form::open(['route'=>'company.store', 'name'=>'frmcompany', 'method'=>'post', 'id'=>'addcompany']) !!}
                       <!-- Requireee companu form -->
                            <p class="pull-right">
                                {{-- {!! Form::submit(__('commonField.button.create'), ['class'=>'btn btn-success']) !!} --}}
                                <input type="submit" name="" class="btn btn-success">
                                 @include('admin.company._form')
                                  <button type="button" class="btn btn-danger" id="closeCompanyModel">@lang('commonField.button.close')</button>
                          </p>
                      {!! Form::close() !!}
                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>

              </div>
            </div>
     @endsection
<!-- modal ended here -->
@section('custom_script')
      <script type="text/javascript">
    $(function(){
      $('.chalaniFrm').on('click', function(e){

        $('#addChalani')[0].reset();
         $('#frmAddChalani').modal('show');
      });  

 
      // function addCompanyFrm(){
      //   // alert(":sadfasdf");
      //    $('#frmAddChalani').modal('hide');
      //   $('#addcompany')[0].reset();
      //    $('#frmAddCompany').modal('show');
      // }
      $('#btnAddCompanyDetail').on('click',function(){
  
        $('#frmAddcompanyModel').modal('show');

        $('#addcompany')[0].reset();
      });

      $('#closeCompanyModel').on('click',function (){
        $('#frmAddcompanyModel').modal('hide');
      });
      /*--------------------------------------*/
        $("form[name='frmchalani']").validate({
         rules:{
          chalaniDate:{
            required:true
          },
          chalaniSubject:{
            required:true,
            minlength:5,
            maxlength:255,
            alphanumeric:true,
          },
          chalaniNo:{
            required:true,
            number:true
          },
          chalaniPage:{
            required:true,
            number:true
          },
          chalaniPageDate:{
              required:true
          },
          other_office_id:{
            required:true
          },
          chalaniTicket:{
            required:true
          }        
         },
         messages: {
          chalaniDate : " Chalani date is required",
          chalaniSubject : " Chalani subject is required",
          chalaniNo : " Chalani number is required",
          chalaniPage : " Chalani page is required",
          chalaniPageDate : "Patra date is required ",
          other_office_id : " Office id is required",
          chalaniTicket : " Chalani is ticket is required"
         }
        });

        $("form[name='frmcompany']").validate({
         rules:{
          nameNep : {
            required: true,
            minlength : 6,
            alphanumeric: true,
            maxlength : 255
          },   
          nameEng : {
            required: true,
            minlength : 6,
            maxlength : 255
          },
          phoneNumber : {
            number:true,
            minlength : 10,
            maxlength : 10
          },
          landlineNumber : {
            minlength: 9,
            maxlength : 9
          },
          email : {
            email : true,
          }
         },
         messages: {
           nameNep     : "Please enter company name in nepali with more than 6 character", 
           nameEng     : "Please enter company name in english with more than 6 character",
           phoneNumber  : "enter correct number with 10 digit",
           addr    : "company full address",
           email   : "email address must be in correct format"
         }
        });
      });
  </script>

  <!-- edit script -->
   <script type="text/javascript">
    /*------update data---------*/
    var btnUpdatecompany = $('.btnUpdatecompany');
    var name           = $('.name');
        btnUpdatecompany.on('click', function() {
            var url = $(this).data('url');
            $.ajax({
                'type': 'GET',
                'url': url,
                success: function (response) {
                  console.log(response);
                    $('.companyId').val(response.id),
                    $('.txtNameNep').val(response.nameNep);
                    $('.txtNameEng').val(response.nameEng);
                    $('.txtPhoneNumber').val(response.phoneNumber);
                    $('.txtLandLineNumber').val(response.landlineNumber);
                    $('.txtEmail').val(response.email);
                    $('.txtAddr').val(response.addr);
                    $('.txtcompanyDetails').val(response.companyDetails);
                    $("#updateChalani").modal('show');
                }
            })
        });

        /*----------------update---------------*/
        var frmUpdatecompany = $('#frmUpdatecompany');
        frmUpdatecompany.on('submit', function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var id   = $('.companyId').val();
        var url  = "{{URL::to('/')}}" + "/setting/company/"+id+"/update";
        $.ajax({
          'type' : 'POST',
          'url'  : url,
          data    : data,
          success : function(response){
           console.log(response);
           if (response.success==true) {
               $('.infoDiv').append(response.message).addClass('alert alert-success');
               $("#updateChalani").modal('hide');
           }
           if (response.success==false) {
            $.each(response.message, function(key, value){
                $('.infoDiv').append(value).addClass('alert alert-danger');
           })
         }

          },complete:function(){
           setTimeout(location.reload.bind(location), 3000);
          }
        })
        .fail(function (response) {
            alert('error while insert');
        });
        $('#updateChalani').modal('hide');
        });
  </script>
@endsection