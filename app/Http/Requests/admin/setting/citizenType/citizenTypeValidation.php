<?php

namespace App\Http\Requests\admin\setting\citizenType;

use Illuminate\Foundation\Http\FormRequest;

class citizenTypeValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:4|max:50|unique:citizen_types',
            'nameEng' => 'required|min:4|max:200'
        ];
    }
}
