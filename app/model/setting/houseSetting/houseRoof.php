<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class houseRoof extends Model
{
    protected $table = 'house_roofs';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
