<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="personalInfo-tab" data-toggle="tab" href="#personalInfo" role="tab" aria-controls="personalInfo" aria-selected="true">Personal Info</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="identity-tab" data-toggle="tab" href="#identity" role="tab" aria-controls="identity" aria-selected="false">Identity</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" id="otherInfo-tab" data-toggle="tab" href="#otherInfo" role="tab" aria-controls="otherInfo" aria-selected="false">Other</a>
  </li>

</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="personalInfo" role="tabpanel" aria-labelledby="personalInfo-tab">
   <div class="row">
      
     <div class="col-sm-6 col-md-6 col-sm-offset-6 col-md-offset-6 pull-right">
            <!-- sdfsdf -->
            @if(isset($data))
                <img src="{{ asset($data->profilePic) }}" width="200" height="200">
            @endif
            <input type="file" name="profilePic" id="thebox">
     </div>
 </div><hr>
 <!--  -->
 <div class="">
    <div class="card-box">

        <div class="card-body ">
            <div class="card-head">
                <header><i class="fa fa-user"></i>&nbsp;@lang('commonField.extra.nameAndAddress')</header>
            </div>
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
                        <div class="form-group">
                            <label style="margin-right: 15px;">@lang('commonField.personal_information.gender')</label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="Male" checked="checked">
                                @lang('commonField.personal_information.male')
                            </label> 
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="Female">
                                @lang('commonField.personal_information.female')
                            </label> 
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="Others">
                                @lang('commonField.personal_information.others')
                            </label> <br><hr>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameNep')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="First Name Nepali" name="fnameNep"> -->

                            {{ Form::text('fnameNep', null, ['class'=>'nepText1 mdl-textfield__input', 'placeholder'=>'First Name Nepali']) }} 
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameNep')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="Middle Name Nepali" name="mnameNep"> -->
                            {{ Form::text('mnameNep', null, ['class'=>'nepText2 mdl-textfield__input', 'placeholder'=>'Middle Name Nepali']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.lastNameNep')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="Surname Nepali" name="lnameNep"> -->
                            {{ Form::text('lnameNep', null, ['class'=>'nepText3 mdl-textfield__input', 'placeholder'=>'Surname Nepali']) }}
                        </div>
                    </div>
                  
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameEng')</label>
                            <!--  <input type="text" class="form-control input-md" placeholder="First Name" name="fnameEng"> -->
                            {{ Form::text('fnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'First Name']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameEng')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="Middle Name" name="mnameEng"> -->
                            {{ Form::text('mnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Middle Name']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.lastNameEng')</label>
                            <!--  <input type="text" class="form-control input-md" placeholder="Surname" name="lnameEng"> -->
                            {{ Form::text('lnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Surname']) }}
                            <br>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<fieldset>
<div class="">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
                <header><i class="fa fa-map-marker"></i>&nbsp;@lang('commonField.address_information.addressDetails')</header>
            </div>
            <div class="form-row">
                    <legend>
                         
                    </legend>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.birthPlace')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="place of Birth" name="birthplace"> -->
              
                            {{ Form::text('birthplace', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'place of Birth']) }}
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.dob') (B.S.)</label>
                           <!--  <input type="text" class="form-control input-md" id="myDate" name="dobBS"> -->
              
                            {{ Form::text('dobBS', null, ['class'=>'mdl-textfield__input', 'id'=>'myDate', 'placeholder' => '2047-06-05']) }}
                        </div>
                    </div>
              
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.dob') (A.D.)</label>
                           <!--  <input type="text" class="form-control input-md dashed-input-field ndp-nepali-calendar" id="myEngDate" name="dobAD"> -->
              
                            {{ Form::text('dobAD', null, ['class'=>'ndp-nepali-calendar mdl-textfield__input', 'id'=>'myEngDate', 'placeholder'=>'1990-09-21']) }}
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.ward')</label>
                            <!-- <input type="text" class="form-control  input-md" placeholder="Ward" name="wardNo"> -->
                            {{ Form::text('wardNo', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Ward Number']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.address_information.villageEng')</label>
                            <!-- <input type="text" class="form-control input-md" placeholder="Town/Village" name="villageNameEng"> -->
                            {{ Form::text('villageNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Town/Village']) }}
                        </div>
                    </div> 
                    
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.address_information.villageNep')</label>
                            <!--  <input type="text" class="form-control input-md" placeholder="Town/Village" name="villageNameNep"> -->

                            {{ Form::text('villageNameNep', null, ['class'=>'nepText4 mdl-textfield__input', 'placeholder' => 'Town/Village']) }}
                        </div>
                    </div>
      
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.email')</label>
                            <!-- <input type="email" class="form-control input-md" placeholder="Email" name="email"> -->
                    
                            {{ Form::email('email', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'email']) }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.phoneNumber')</label>
                            <!-- <input type="number" class="form-control input-md" placeholder="Phone Number" maxlength="10" name="phoneNumber"> -->
                    
                            {{ Form::text('phoneNumber', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Phone Number']) }}
                        </div>
                    </div>
            </div>
            <div class="form-row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.address_information.district')</label>
                            <select class="form-control mdl-textfield__input" name="districts_id">
                                <option value="">@lang('commonField.extra.districts_id')</option>
                                @if(count($districtId) > 0)  <!-- selecting leave type -->
                                @foreach($districtId as $dis)
                                <option value="{{ $dis->id }}" {{ isset($data->districts_id)  && $data->districts_id == $dis->id ? 'selected' : ''}} {{(old('dis')==$dis->id)? 'selected':''}}>
                                   {{ $dis->districtNameNep }} - {{ $dis->districtNameEng }}
                               </option>
                               @endforeach
                               @endif
                           </select>
              
                       </div>
                   </div>
                   <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                        <label>@lang('commonField.address_information.province')</label>
                        <select class="form-control mdl-textfield__input" name="provinces_id">
                            <option value="">@lang('commonField.extra.provisions_id')</option>
                            @if(count($provisonId) > 0)  <!-- selecting leave type -->
                            @foreach($provisonId as $pro)
                            <option value="{{ $pro->id }}" {{ isset($data->provisions_id)  && $data->districts_id == $pro->id ? 'selected' : ''}} {{(old('pro')==$pro->id)? 'selected':''}}>
                               {{ $pro->name }}
                           </option>
                           @endforeach
                           @endif
                       </select>
                   </div>
               </div>
            </div>
    </div>
</div>
  
            <!-- GrandFather -->
<div class="">
        <div class="card-box">
          <div class="card-body ">
            <div class="card-head">
              <header><i class="fa fa-address-book"></i>&nbsp;@lang('commonField.personal_information.gfDetails')</header>
            </div>
            <div class="form-row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.citizenshipNumber')</label>
                            {{ Form::text('gfCitizenNum', isset($grandFatherData) ? $grandFatherData->citizenNo : '', ['class'=>'gfCitizenNum mdl-textfield__input', 'placeholder' => 'Grand Father citizenship number']) }}
                            <span class="gfOptionValue"></span>
                        </div>
                        </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.placeofIssueCitizenship')</label>
                            {{ Form::text('gfCitizenIssuePlace', isset($grandFatherData) ? $grandFatherData->citizenshipIssuePlace : '', ['class'=>'gfCitizenIssuePlace mdl-textfield__input', 'placeholder' => 'Grand father citizenship Issue Place', 'required']) }}
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.citizenshipIssueDate')(B.S)</label>
                            {{ Form::text('gfcitizenIssueDate', isset($grandFatherData) ? $grandFatherData->citizenshipIssueDate : '', ['class'=>'gfcitizenIssueDate mdl-textfield__input', 'placeholder' => 'yyyy-mm-dd', 'id' => 'myDate2']) }}
                        </div>
                    </div>
                
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameNep')</label>
                            {{ Form::text('gfFNameNep', null, ['class'=>'nepText5 gfFNameNep mdl-textfield__input', 'placeholder'=>'Grand father First Name Nepali']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameNep')</label>
                            {{ Form::text('gfMNameNep', null, ['class'=>'nepText6 gfMNameNep mdl-textfield__input', 'placeholder'=>'Grand father middle Name in Nepali']) }} 
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameNep')</label>
                            {{ Form::text('gfLnameNep', null, ['class'=>'nepText7 gfLnameNep mdl-textfield__input', 'placeholder'=>'Grand father last Name in Nepali']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameEng')</label>
                            {{ Form::text('gfFNameEng', null, ['class'=>'gfFNameEng mdl-textfield__input', 'placeholder'=>'Grand father First Name English']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameEng')</label>
                            {{ Form::text('gfMNameEng', null, ['class'=>'gfMNameEng mdl-textfield__input', 'placeholder'=>'Grand father middle Name in English']) }} 
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.lastNameEng')</label>
                            {{ Form::text('gfLnameEng', null, ['class'=>'gfLnameEng mdl-textfield__input', 'placeholder'=>'Grand father last Name in English']) }} 
                        </div>
                    </div>
            </div>
        </div>
</div>
<!-- Father -->
<div class="">
        <div class="card-box">
          <div class="card-body ">
            <div class="card-head">
              <header><i class="fa fa-address-book"></i>&nbsp;@lang('commonField.personal_information.fDetails')</header>
            </div>
            <div class="form-row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.citizenshipNumber')</label>
                            {{ Form::text('fCitizenNum', null, ['class'=>'fCitizenNum mdl-textfield__input', 'placeholder' => 'Father citizenship number']) }}
                        </div>
                        </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.placeofIssueCitizenship')</label>
                            {{ Form::text('ftCitizenIssuePlace', isset($fatherData) ? $fatherData->citizenshipIssuePlace : '', ['class'=>'mdl-textfield__input', 'placeholder' => 'Father citizenship Issue Place', 'required']) }}
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.citizenshipIssueDate')(B.S)</label>
                            {{ Form::text('ftcitizenIssueDate', isset($fatherData) ? $fatherData->citizenshipIssueDate : '', ['class'=>'ftcitizenIssueDate mdl-textfield__input', 'placeholder' => 'yyyy-mm-dd', 'id' => 'myDate3']) }}
                        </div>
                    </div>
                
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameNep')</label>
                            {{ Form::text('ftFNameNep', isset($fatherData) ? $fatherData->fnameNep : '', ['class'=>'nepText8 ftFNameNep mdl-textfield__input', 'placeholder'=>'Father First Name Nepali']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameNep')</label>
                            {{ Form::text('ftMNameNep', isset($fatherData) ? $fatherData->mnameNep : '', ['class'=>'nepText9 ftMNameNep mdl-textfield__input', 'placeholder'=>'Father middle Name in Nepali']) }} 
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameNep')</label>
                            {{ Form::text('ftLnameNep', null, ['class'=>'nepText10 ftLnameNep mdl-textfield__input', 'placeholder'=>'Father last Name in Nepali']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameEng')</label>
                            {{ Form::text('ftFNameEng', null, ['class'=>'ftFNameEng mdl-textfield__input', 'placeholder'=>'Father First Name English']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameEng')</label>
                            {{ Form::text('ftMNameEng', null, ['class'=>'ftMNameEng mdl-textfield__input', 'placeholder'=>'Father middle Name in English']) }} 
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.lastNameEng')</label>
                            {{ Form::text('ftLnameEng', null, ['class'=>'ftLnameEng mdl-textfield__input', 'placeholder'=>'Father last Name in English']) }} 
                        </div>
                    </div>
            </div>
        </div>
</div>
{{-- Mother  --}}
<div class="">
        <div class="card-box">
          <div class="card-body ">
            <div class="card-head">
              <header><i class="fa fa-address-book"></i>&nbsp;@lang('commonField.personal_information.mDetails')</header>
            </div>
            <div class="form-row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.citizenshipNumber')</label>
                            {{ Form::text('mtCitizenNum', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Mother citizenship number']) }}
                        </div>
                        </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.placeofIssueCitizenship')</label>
                            {{ Form::text('mtCitizenIssuePlace', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Mother citizenship Issue Place']) }}
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.extra.citizenshipIssueDate')(B.S)</label>
                            {{ Form::text('mtcitizenIssueDate', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'yyyy-mm-dd', 'id' => 'myDate4']) }}
                        </div>
                    </div>
                
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameNep')</label>
                            {{ Form::text('mtFNameNep', null, ['class'=>'nepText13 mdl-textfield__input', 'placeholder'=>'Mother First Name Nepali']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameNep')</label>
                            {{ Form::text('mtMNameNep', null, ['class'=>'nepText11 mdl-textfield__input', 'placeholder'=>'Mother middle Name in Nepali']) }} 
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameNep')</label>
                            {{ Form::text('mtLnameNep', null, ['class'=>'nepText12 mdl-textfield__input', 'placeholder'=>'Mother last Name in Nepali']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.firstNameEng')</label>
                            {{ Form::text('mtFNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Mother First Name English']) }} 
                        </div>
                    </div>
                    <!--  -->
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.middleNameEng')</label>
                            {{ Form::text('mtMNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Mother middle Name in English']) }} 
                        </div>
                    </div>
            
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label>@lang('commonField.personal_information.lastNameEng')</label>
                            {{ Form::text('mtLnameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Mother last Name in English']) }} 
                        </div>
                    </div>
            </div>
        </div>
</div>

<div class="card-box">
        <div class="card-body ">
          <div class="card-head">
            <header><i class="fa fa-address-book"></i>&nbsp;@lang('commonField.personal_information.marriageDetails')</header>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
                <div class="form-group">
                    <label style="margin-right: 15px;">@lang('commonField.extra.maritialStatus')?</label>
                    <label class="radio-inline">
                        <input type="radio" name="maritialStauts" class="marrExists" value="N" checked="checked">
                         @lang('commonField.extra.no')
                    </label> 
                    <label class="radio-inline">
                        <input type="radio" name="maritialStauts" class="marrExists" value="Y">
                         @lang('commonField.extra.yes')
                    </label> 
                </div>
            </div>
      </div>

      <div class="row marriageStatus hide p-5">
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.personal_information.citizenshipNumber')</label>
                             {{ Form::text('mgCitizenNum', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'citizenship number']) }}
                         </div>
                         </div>
             
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.extra.placeofIssueCitizenship')</label>
                             {{ Form::text('mgCitizenIssuePlace', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'citizenship Issue Place', 'required']) }}
                         </div>
                     </div>
             
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.extra.citizenshipIssueDate')(B.S)</label>
                             {{ Form::text('mgcitizenIssueDate', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'yyyy-mm-dd', 'id' => 'myDate5']) }}
                         </div>
                     </div>
                 
                     <!--  -->
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.personal_information.firstNameNep')</label>
                             {{ Form::text('mgFNameNep', null, ['class'=>'nepText16 mdl-textfield__input', 'placeholder'=>'First Name Nepali']) }} 
                         </div>
                     </div>
                     <!--  -->
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.personal_information.middleNameNep')</label>
                             {{ Form::text('mgMNameNep', null, ['class'=>'nepText17 mdl-textfield__input', 'placeholder'=>'Middle Name in Nepali']) }} 
                         </div>
                     </div>
             
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.personal_information.middleNameNep')</label>
                             {{ Form::text('mgLnameNep', null, ['class'=>'nepText18 mdl-textfield__input', 'placeholder'=>'Last Name in Nepali']) }} 
                         </div>
                     </div>
                     <!--  -->
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.personal_information.firstNameEng')</label>
                             {{ Form::text('mgFNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'First Name English']) }} 
                         </div>
                     </div>
                     <!--  -->
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.personal_information.middleNameEng')</label>
                             {{ Form::text('mgMNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Middle Name in English']) }} 
                         </div>
                     </div>
             
                     <div class="col-xs-12 col-sm-6 col-md-4">
                         <div class="form-group">
                             <label>@lang('commonField.personal_information.lastNameEng')</label>
                             {{ Form::text('mgLnameEngmtMNameEngmtMNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'ast Name in English']) }} 
                         </div>
                     </div>
              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                        <label>@lang('commonField.extra.son')</label>
                        {{ Form::number('son', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Number of son']) }}
                  </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                        <label>@lang('commonField.extra.daughter')</label>
                        {{ Form::number('daughter', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Number of son']) }}
                  </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                        <label>@lang('commonField.extra.other')</label>
                        {{ Form::number('other', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Number of third gender']) }}
                  </div>
              </div>
         </div>
</fieldset> 
</div>
<!-- identity Detials -->
<div class="tab-pane fade" id="identity" role="tabpanel" aria-labelledby="identity-tab">
  <fieldset>
      <div class="card-box">
      <div class="card-body ">
        <div class="card-head">
          <legend><label>Identity Details</label></legend>
          </div>
          <div class="form-row">
          <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                  <label style="margin-right: 15px;">@lang('commonField.extra.citizenNo')?</label>
                  <label class="radio-inline">
                      <input type="radio" name="haveCitizen" class="citizenExists" value="Y">
                      @lang('commonField.extra.yes')
                  </label> 

                  <label class="radio-inline">
                      <input type="radio" name="haveCitizen" class="citizenExists" value="N" checked="checked">
                      @lang('commonField.extra.no')
                  </label> <br>
              </div>
          </div>
          <div class="citizenDiv row hide">
              <div class="col-xs-12 col-sm-3 col-md-3">
                  <div class="form-group">
                      <label>@lang('commonField.extra.citizenNo')</label>
                      {{ Form::text('citizenNo', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Citizenship number', 'required']) }}
                  </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                  <div class="form-group">
                      <label>@lang('commonField.extra.citizenshipIssueDate')(B.S)</label>
                       {{ Form::text('citizenshipIssueDate', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'yyyy-mm-dd', 'id' => 'myDate6']) }}
                  </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                  <div class="form-group">
                      <label>@lang('commonField.extra.placeofIssueCitizenship')</label>
                      {{ Form::text('citizenshipIssuePlace', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Place of Issue']) }}
                  </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                  <div class="form-group">
                      <label>@lang('commonField.extra.citizenType')</label>
                      <select class="form-control" name="citizen_types_id">
                          <option value="">@lang('commonField.extra.citizenTypeId')</option>
                          @if(count($citizenTypesId) > 0)  <!-- selecting leave type -->
                          @foreach($citizenTypesId as $citizentyp)
                          <option value="{{ $citizentyp->id }}" {{ isset($data->citizen_types_id)  && $data->citizen_types_id == $citizentyp->id ? 'selected' : ''}} {{(old('citizentyp')==$citizentyp->id)? 'selected':''}}>
                             {{ $citizentyp->nameNep }} - {{ $citizentyp->nameEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>
          </div>
          <br><hr>
          <!--  -->
              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.jatjati')</label>
                      <select class="form-control" name="jatjatis_id">
                          <option value="">@lang('commonField.extra.jatjati')</option>
                          @if(count($jatJatiId) > 0)  <!-- selecting leave type -->
                          @foreach($jatJatiId as $jatJati)
                          <option value="{{ $jatJati->id }}" {{ isset($data->jatjatis_id)  && $data->jatjatis_id == $jatJati->id ? 'selected' : ''}} {{(old('jatJati')==$jatJati->id)? 'selected':''}}>
                             {{ $jatJati->nameNep }} - {{ $jatJati->nameEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.nationality')</label>
                      <select name="nationalities_id" class="form-control">
                            <option value="">@lang('commonField.extra.nationality')</option>
                             @if(count($nationalityId) > 0)
                                @foreach($nationalityId as $nation)
                                     <option value="{{ $nation->id }}" {{ isset($data->nationalities_id)  && $data->nationalities_id == $nation->id ? 'selected' : ''}} {{(old('nation')==$nation->id)? 'selected':''}}>
                                           {{ $nation->nameNep }}  - {{ $nation->nameEng }}
                                     </option>
                                @endforeach
                             @endif
                      </select>
                  </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.language') </label>
                      <select class="form-control" name="sys_languages_id">
                          <option value="">@lang('commonField.extra.language_id')</option>
                          @if(count($syslangId) > 0)  <!-- selecting leave type -->
                          @foreach($syslangId as $lang)
                          <option value="{{ $lang->id }}" {{ isset($data->sys_languages_id)  && $data->sys_languages_id == $lang->id ? 'selected' : ''}} {{(old('lang')==$lang->id)? 'selected':''}}>
                             {{ $lang->langNep }} - {{ $lang->langEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="form-group">
                      <label>@lang('commonField.extra.religious') </label>
                      <select class="form-control" name="religiouses_id">
                          <option value="">@lang('commonField.extra.religious_id')</option>
                          @if(count($religiousId) > 0)  <!-- selecting leave type -->
                          @foreach($religiousId as $religious)
                          <option value="{{ $religious->id }}" {{ isset($data->religiouses_id)  && $data->religiouses_id == $religious->id ? 'selected' : ''}} {{(old('religious')==$religious->id)? 'selected':''}}>
                             {{ $religious->nameNep }} - {{ $religious->nameEng}}
                         </option>
                         @endforeach
                         @endif
                     </select>
                 </div>
              </div>
          </div>
       </div>  
      </div>
  </fieldset> 
</div>
<!--  -->
<div class="tab-pane fade" id="otherInfo" role="tabpanel" aria-labelledby="otherInfo-tab">
<div class="card-box">
    <div class="card-body">
    <div class="card-head">
        <legend><label>other Information</label></legend>
    </div>
        

        <div class="form-row">

         <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
              <label>@lang('commonField.personal_information.qualification')</label>
              <select class="form-control" name="qualifications_id">
                  <option value="">@lang('commonField.extra.qualification_id')</option>
                  @if(count($qualificationId) > 0)  <!-- selecting leave type -->
                  @foreach($qualificationId as $qua)
                  <option value="{{ $qua->id }}" {{ isset($data->qualifications_id)  && $data->qualifications_id == $qua->id ? 'selected' : ''}} {{(old('qua')==$qua->id)? 'selected':''}}>
                     {{ $qua->nameNep }} - {{ $qua->nameEng}}
                 </option>
                 @endforeach
                 @endif
             </select>
         </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label>@lang('commonField.extra.occupation')</label>
                <select class="form-control" name="occupations_id">
                    <option value="">@lang('commonField.extra.occupation_id')</option>
                    @if(count($occupationId) > 0)  <!-- selecting leave type -->
                    @foreach($occupationId as $occup)
                    <option value="{{ $occup->id }}" {{ isset($data->occupations_id)  && $data->occupations_id == $occup->id ? 'selected' : ''}} {{(old('occup')==$occup->id)? 'selected':''}}>
                       {{ $occup->nameNep }} - {{ $occup->nameEng}}
                   </option>
                   @endforeach
                   @endif
               </select>
           </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label style="margin-right: 15px;">@lang('commonField.extra.passportExists')?</label>
                <label class="radio-inline">
                    <input type="radio" name="havePassport" class="pwdExists" value="N" checked="checked">
                    @lang('commonField.extra.no')
                </label> 
                <label class="radio-inline">
                    <input type="radio" name="havePassport" class="pwdExists" value="Y">
                    @lang('commonField.extra.yes')
                </label> 
            </div>
        </div>

         <div class="col-xs-12 col-sm-6 col-md-4 passportDiv hide">
            <div class="form-group">
                <label> @lang('commonField.extra.passportNum')</label>
                <!-- <input type="text" class="form-control input-md" placeholder="Passport Number" name="passportNumber"> -->

                {{ Form::text('passportNumber', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Passport Number']) }}


            </div>
        </div>
        <!--  -->

        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label>@lang('commonField.extra.annualIncome')</label>
               <!--  <input type="text" class="form-control input-md" placeholder="Annual Income" name="annualIncome"> -->
                {{ Form::text('annualIncome', null, ['class'=>'mdl-textfield__input', 'placeholder' => 'Annual Income']) }}
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label> @lang('commonField.extra.citizenImage')</label>
                <div class="form-group imgCitizen">
                  <p class="pull-right">
                      <label>
                       <br>
                       <img src="{{ isset($data->citizenImgPath) ? asset($data->citizenImgPath) : '' }}" width="120" height="80" class="img img-thumbnai" id="imgCitizen"><br>
                        <input name="citizenImgPath" type='file' onchange="displayImage(this, 'imgCitizen');" title="select citizenship" />
                      </label>
                  </p>
                </div>
            </div>
        </div>
        </div>

    </div>
</div>
</div>
</div>
<label class="pull-right mb-3">
       @lang('commonField.button.next')
    <i class="fa fa-angle-double-right nextMe" style="font-size: 40px;">
    </i>
</label>
</div>