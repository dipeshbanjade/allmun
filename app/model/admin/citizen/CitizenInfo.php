<?php

namespace App\model\admin\citizen;

use Illuminate\Database\Eloquent\Model;

class CitizenInfo extends Model
{
    protected $table = 'citizen_infos';

    public function provinces(){
    	return $this->belongsTo('App\model\setting\Province'::class);
    }

    public function districts(){
    	return $this->belongsTo('App\model\setting\District'::class);
    }

    public function qualifications(){
    	return $this->belongsTo('App\model\setting\Qualification'::class);
    }

    public function occupations(){
    	return $this->belongsTo('App\model\setting\occupation'::class);
    } 

    public function citizenTypes(){
    	return $this->belongsTo('App\model\setting\CitizenType'::class);
    }

    public function nationalities(){
        return $this->belongsTo('App\model\admin\setting\Nationality'::class);
    }

    public function sys_languages(){
        return $this->belongsTo('App\model\setting\sysLanguage'::class);
    }

    public function religiouses(){
        return $this->belongsTo('App\model\setting\Religious'::class);
    }

    public function jatjatis(){
        return $this->belongsTo('App\model\setting\Jatjati'::class);
    }
}
