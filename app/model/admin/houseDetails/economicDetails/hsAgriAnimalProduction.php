<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class hsAgriAnimalProduction extends Model
{
	protected $table = 'hs_agri_animal_productions';
}
