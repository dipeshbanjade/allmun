<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App;
use Session;
use App\model\shifaris\ApplicantUser;
use App\model\mylogic;
use App\model\admin\biomatric\bioMatric;
use Carbon\Carbon;
use App\model\shifaris\CitizenShifaris;

class HomeController extends Controller
{
    protected $viewPath = 'admin.shifaris';
     protected $deginationTable =  'deginations';
    public function __construct()
    {
        $this->middleware('auth')->except('getIndexPage', 'biomatric');
    }

    public function index()
    {
        $page['page_title']       =  'Admin : Dashboard';
        $page['page_description'] =  'Admin : muncipility and wada details panel';

        $hsCount      = count($this->getTotalHouseNo());
        $getDobStaff    = mylogic::getBirthList();
        // dd($getDobStaff);
        $citizenCount = $this->getTotalCitizen();
        $maleCount    = $this->getTotalMale();
        $femaleCount  = $this->getTotalFemale();
        $withCitizen  = mylogic::citizenWithCitizenCount();
        $withOutCitizen = mylogic::citizenWithoutCitizenCount();
        $disableCitizen  = mylogic::getDisableCitizen();

        $maleJobHoder    = mylogic::getCountJobHolder('male');
        $femaleJobHolder = mylogic::getCountJobHolder('female');
        $otherJobHolder  = mylogic::getCountJobHolder('other');
        $wardList        = mylogic::getListOfWard();
        $occCount        = mylogic::getOccupationWiseCount('farmer');
        $getOfficialLeave = mylogic::getOfficialLeave();

        $todayAtten = mylogic::getCurrentAtten();
        // dd($todayAtten);

        return view('admin.home', compact(['page', 'getDobStaff','citizenCount', 'hsCount', 'maleCount', 'femaleCount', 'withCitizen', 'withOutCitizen', 'disableCitizen', 'occCount', 'maleJobHoder', 'femaleJobHolder', 'otherJobHolder', 'wardList', 'todayAtten', 'today', 'getOfficialLeave']));
    }

    public function viewUserManual(){
        $page['page_title']       =  'Admin : User manual';
        $page['page_description'] =  'Admin : user manual';

        return view('admin.userManual.userManual', compact(['page']));
    }

    public function getIndexPage(){
        $page['page_title'] = 'Muncipility-Wada';
        $page['page_description'] = 'first login page of muncipility ';
        
        if (!Auth::user()) {
            return view('index', compact(['page']));
        }else{
            return redirect()->route('home')->withmessage('Please logout first');
        }
    }

    public function adminlogout(){
        // Auth::guard('sisadmin')->logout();
        Auth::logout();
        return redirect('/');
    }

    public function getNibedakDetail($id){
        if(!$id){
            return response()->json(
                [
                    'success' => falcse,
                    'message' => 'Please Insert Citizenship number properly'
                ]
            );
        }
        $findUserDetail = ApplicantUser::where(['citizenshipNumber' => $id, 'status' => 0, 'softDelete' => 0])->first();
  // dd($findUserDetail);
  if($findUserDetail){
      return response()->json(
          [
           'success' => true,
           'applicantName' => $findUserDetail['applicantName'],
           'applicantAddress' => $findUserDetail['applicantAddress'],
           'citizenshipNumber' => $findUserDetail['citizenshipNumber'],
           'id' => $findUserDetail['id']
       ]
   );
    } 
    }  
    public function allShifaris(){
        $page['page_title']       =  'All Shifaris';
        $page['page_description'] =  'All Shifaris';

        return view('admin.shifaris.allShifaris', compact(['page']));
    }

    public function searchShifarisIndex(){
       $page['page_title']       =  'All Shifaris';
       $page['page_description'] =  'All Shifaris';

       if (Auth::user()->userLevel == 'mun') {
        $allShifarisData = CitizenShifaris::where(['status' => 0, 'softDelete' => 0])->orderBy('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

        return view($this->viewPath.'.searchShifaris', compact(['page', 'allShifarisData']));
      }

      $allShifarisData = CitizenShifaris::where(['wards_id' => Auth::user()->wards_id, 'status' => 0, 'softDelete' => 0])->orderBy('created_at', 'DESC')->paginate(config('activityMessage.pagination'));

      return view($this->viewPath.'.searchShifaris', compact(['page', 'allShifarisData']));
    }

    public function shifarisSearch(Request $request){
     $page['page_title']       =  'All Shifaris';
     $page['page_description'] =  'All Shifaris';

     $requestCitizenId = $request->citizenId;
     $requestCitizenNum = $request->citizenNum;
     $requestTable = $request->shifarisTable;

     if (Auth::user()->userLevel == 'wrd') {

       if($requestTable == 'all'){
         if (!empty($requestCitizenId)) {
           $shifarisData = CitizenShifaris::where(['citizen_infos_id' => $requestCitizenId, 'status' => 0, 'softDelete' => 0])->orderBy('created_at', 'DESC')->get();
         }else if(!empty($requestCitizenNum)){
           $shifarisData = CitizenShifaris::where(['citizenNum' => $requestCitizenNum, 'status' => 0, 'softDelete' => 0])->orderBy('created_at', 'DESC')->get();
         }else{
           $shifarisData = CitizenShifaris::where(['status' => 0, 'softDelete' => 0])->orderBy('created_at', 'DESC')->paginate(config('activityMessage.pagination'));
         }
       }else{
         if(!empty($requestCitizenId)){
           $shifarisData = CitizenShifaris::
           where(['citizen_infos_id' => $requestCitizenId, 'shifarish_name' => $requestTable, 'status' => 0, 'softDelete' => 0])
           ->orderBy('created_at', 'DESC')->get();
         }else if(!empty($requestCitizenNum)){
           $shifarisData = CitizenShifaris::
           where(['citizenNum' => $requestCitizenNum, 'shifarish_name' => $requestTable, 'status'=> 0, 'softDelete' => 0])->orderBy('created_at', 'DESC')->get();
         }else{
           $shifarisData = CitizenShifaris::
           where(['shifarish_name' => $requestTable, 'status'=> 0, 'softDelete' => 0])->orderBy('created_at', 'DESC')->get();
         }
       }
     }

     if(!empty($shifarisData)){
       return view('admin.shifaris.searchShifaris', compact(['page', 'shifarisData']));
     }else{
       return view('admin.shifaris.searchShifaris')->withMessage(config('activityMessage.idNotFound'));
     }
    }

    public function getShifarisData($id){
      $citizenInfo = explode('$sep!', $id);
     $citizenData =  mylogic::getCitizenNameById($citizenInfo[1]);
     // $citizenRelationId= getRelation($id);
     // $citiaen_realtioin= mylogic::getCitizenNameById($citizenRelationId)
     // $occupationName = mylogic::getOccupationNameById()
     return response()->json($citizenData);
    }

    public function shifarisEdit($slug, $tableName, $shifarisViewPath){
     // dd($slug.$tableName.$shifarisViewPath);
     $page['page_title']       =  'Edit / Update Shifaris';
     $page['page_description'] =  'Edit / Update Shifaris';

     $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationTable, $orderColumn = 'nameNep', $orderby = 'ASC');

     $decryptId = mylogic::idDecryption($slug);
     // dd($decryptId);

     $data = DB::table($tableName)->where(['id' => $decryptId])->first();
     // dd($data);

     return view($shifarisViewPath.'.edit', compact(['page', 'deginationsId', 'data']));
    }

    public function switchLanguage(Request $request){
        $locale = $request->lan;
        if (Session::has('locale')) {
            Session::put('locale', $locale);
        }else{
            Session::put('locale', $locale);
        }
        // App::setLocale(Session::has('locale') ? Session::get('locale') : Config::get('app.locale'));
        return response()->json(
             [
                'success' => true,
                'message' => 'successfully change language please wait still browser refresh'
             ]
            );
    }
    public function accessPermission(){
        $page['page_title']       =  'Admin : Access dined';
        $page['page_description'] =  'Admin : donot have sufficient permission';
        return view('admin.404', compact(['page']));
    }

    
    public function biomatric(){
        $data_backk = json_decode(file_get_contents('php://input'));
             $data_back = $data_backk->{"trans"};
             if(count($data_back) > 0){
                 $responseMessage["transStatus"] = array();
                 foreach ($data_back as $dataRow) {
                     $reponseTrans = [];
                     $att = bioMatric::create([
                               'tnxId' => $dataRow->{"txnId"},
                               'dvcId' => $dataRow->{"dvcId"},
                               'dvcIp' => $dataRow->{"dvcIP"},
                               'punchId' =>$dataRow->{"punchId"},
                               'tnxDateTime' =>$dataRow->{"txnDateTime"},
                               'mode'     => $dataRow->{"mode"},
                               'status'   => 0,
                               'softDelete' => 0
                             ]);

                     if($att) $reponseTrans["status"] = 1;
                     else $reponseTrans["status"] = 0;
                     $reponseTrans["txnId"] = $dataRow->{"txnId"};
                     $responseMessage["transStatus"][] = $reponseTrans;
                 }
                 header("Content-Type: application/json");
                 echo json_encode($responseMessage);
               }
    }

  public function getTotalHouseNo(){
    return mylogic::houseCount(Auth::user()->wards_id);
  }

  public function getTotalCitizen(){
    return mylogic::getTotalCitizenNo(Auth::user()->wards_id);
  }

  public function getTotalMale(){
    return mylogic::getGenderCount('male');
  }
  public function getTotalFemale(){
    return mylogic::getGenderCount('female');
  }

  public function getCountCitizenWithCitizen(){
    return mylogic::citizenWithCitizenCount();
  }

  public function getCurrentAttendance(){
    return mylogic::getCurrentAtten();
  }

  
}