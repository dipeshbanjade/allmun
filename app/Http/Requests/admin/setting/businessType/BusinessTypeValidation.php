<?php

namespace App\Http\Requests\admin\setting\businessType;

use Illuminate\Foundation\Http\FormRequest;

class BusinessTypeValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nameEng' => 'required|min:4|max:100'
        ];
    }
}
