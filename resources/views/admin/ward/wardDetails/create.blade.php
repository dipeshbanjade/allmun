@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
    <div class="col-sm-12 col-md-12">
          <div class="card-box">
            <div class="card-body ">

             <div class="card-head">
              <header>
                 <h3><i class="fa fa-plus">&nbsp;@lang('commonField.extra.createWard')</i></h3></header>
            </div>
            <!--  -->
         {{ Form::open(['route' => 'wardDetails.store', 'files' => true, 'name' => 'creatWardFrm']) }}
		    <div class="card-body row">
		         @include('admin.ward.wardDetails._form')                          
		    </div>
        <div class="modal-footer">
            <div class="col-lg-12 p-t-20 text-right"> 
            {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="history.go(-1)">@lang('commonField.button.back')</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    </div>
@endsection
@section('custom_script')
   @include('admin.ward.wardDetails.script')
@endsection

