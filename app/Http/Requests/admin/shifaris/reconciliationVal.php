<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class reconciliationVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'caseName' => 'required',
            'badiAnswer' => 'required',
            'pratiBadiAnswer' => 'required',
            'dafaNumber' => 'required',
            'badi' => 'required',
            'pratiBadi' => 'required',
            'rohabaarName' => 'required',
            'year' => 'required|numeric',
            'month' => 'required|numeric',
            'day' => 'required|numeric',
            'eachday' => 'required|numeric',
            'citizenshipNumber' =>'required'
        ];
    }
}
