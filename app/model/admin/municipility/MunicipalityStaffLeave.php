<?php

namespace App\model\admin\municipility;

use Illuminate\Database\Eloquent\Model;

class MunicipalityStaffLeave extends Model
{
    protected $table = 'municipality_staff_leaves';

    public function leaveTypesIds(){
    	return $this->belongsTo('App\model\setting\LeaveType'::class);
    }

    public function users(){
    	return $this->belongsTo(User::class);
    }
}
