<?php

namespace App\Http\Requests\admin\municipality;

use Illuminate\Foundation\Http\FormRequest;

class leaveValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [            
            'leave_types_id' => 'required',
            'noOfDays'        => 'required',
            'shortNoteEng'    => 'required|min:8|max:6000',
            'startDate'       => 'required|date',
            'endDate'         => 'required|date'
        ];
    }
}
