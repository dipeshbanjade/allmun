<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHsCrimeVictimsTable extends Migration
{
    public function up()
    {
        Schema::create('hs_crime_victims', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->integer('createdBy')->unsigned()->nullable();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('house_details_id')->nullable()->unsigned();
            $table->foreign('house_details_id')->references('id')->on('house_details')->onDelete('cascade'); 

            $table->integer('citizen_infos_id')->nullable()->unsigned();
            $table->foreign('citizen_infos_id')->references('id')->on('citizen_infos')->onDelete('cascade');

            $table->boolean('haveCrimeVictim')->default(0);
            $table->text('crimeName')->nullable();
            $table->text('otherCrimeName')->nullable();
            $table->boolean('haveDiscrimination')->default(0);

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('hs_crime_victims');
    }
}
