<div class="card-body row">
         <div class="col-lg-4"> 
           @if(isset($data))
            <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
              {{ Form::text('uniqueCode', isset($data->uniqueCode) ? isset($data->uniqueCode) : '', ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'readonly']) }}
              <label class = "mdl-textfield__label">@lang('commonField.extra.uniqueCode')</label>
            </div>
               @else
              <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                {{ Form::text('uniqueCode', isset($getUniqueCode) ? $getUniqueCode : '', ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
                <label class = "mdl-textfield__label">@lang('commonField.extra.uniqueCode')</label>
              </div>
          @endif

        </div>
       <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
          {{ Form::text('nameNep', null, ['class'=>'UnicodeKeyboardTextarea mdl-textfield__input', 'placeholder'=>'name in nepali', 'title'=>'municipality name in nepali', 'required'=>'true']) }}
          <label class = "mdl-textfield__label" > @lang('commonField.personal_information.nameNep')</label>
        </div>
      </div>
      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
          {{ Form::text('nameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'municipality name in english', 'title'=>'muncipality name in english', 'required'=>true]) }}
          <label class = "mdl-textfield__label" > @lang('commonField.personal_information.nameEng') </label>
        </div>
      </div>
      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
          {{ Form::text('landLineNumber', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'muncipality landline number', 'title'=>'muncipality landline number', 'required'=>true]) }}
          <label class = "mdl-textfield__label" > @lang('commonField.personal_information.landLineNumber') </label>
        </div>
      </div>
      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
          {{ Form::text('faxNumber', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'fax number', 'title'=>'muncipality fax number']) }}
          <label class = "mdl-textfield__label" > @lang('commonField.personal_information.faxNumber') </label>
        </div>
      </div> 
      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
          {{ Form::text('addrNep', null, ['class'=>'UnicodeKeyboardTextarea mdl-textfield__input', 'placeholder'=>'muncipality address in nepali', 'title'=>'muncipality address in nepali']) }}
          <label class = "mdl-textfield__label" > @lang('commonField.personal_information.addrNep')  </label>
        </div>
      </div>
      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
          {{ Form::text('addrEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'muncipality address in english', 'title'=>'muncipality address in english']) }}
          <label class = "mdl-textfield__label" > @lang('commonField.personal_information.addrEng')</label>
        </div>
      </div>
      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
          <label style="color: black;"> @lang('commonField.extra.provisions_id')</label>
          <select name="provinces_id" class="form-control" title="select province">
            <option value="">@lang('commonField.extra.provisions_id')</option>
            @if(count($provincesId) > 0)
                @foreach($provincesId as $provinces)
                     <option value="{{ $provinces->id }}" {{ isset($data->provinces_id) && $data->provinces_id ==  $provinces->id ? 'selected' : '' }}>
                          {{ $provinces->name }} 
                     </option>
                @endforeach
            @endif
          </select>
        </div>
      </div>    
   
      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
          <label style="color: black;">@lang('commonField.extra.districts_id')</label>
          <select name="districts_id" class="form-control" title="select district name">
            <option value="">@lang('commonField.extra.districts_id')</option>
            @if(count($districtId) > 0)
                @foreach($districtId as $prov)
                     <option value="{{ $prov->id  }}" {{ isset($data->districts_id) && $data->districts_id == $prov->id ? 'selected' : '' }}>
                          {{ $prov->districtNameNep }}  - {{ $prov->districtNameEng }} 
                     </option>
                @endforeach
            @endif
          </select>
        </div>
      </div> 

      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" style="margin-top: 20px;">
          {{ Form::text('muncipilityHead', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'municipality head name']) }}
          <label class = "mdl-textfield__label" >  @lang('commonField.extra.muncipilityHead') </label>
        </div>
      </div>
     <!--  -->
      <div class="col-lg-4"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" style="margin-top: 20px;">
          {{ Form::number('bioDeviceId', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'Biomatric identity number', 'title'=>'biomatric identity number']) }}
          <label class = "mdl-textfield__label" > @lang('commonField.employer.biomatricIdNo') </label>
        </div>
      </div>
      <!--  -->
      <div class="col-lg-12">
       <label class="control-label col-md-3" style="color: black;">@lang('commonField.extra.uploadLogo')
       </label>
       <!--  -->
            <p class="pull-right">
               <label>
                <br>
                <img src="{{ isset($data) ? asset($data->munLogo) : '' }}" width="120" height="80" class="img img-thumbnai" id="munLogo"><br>
                 <input name="munLogo" type='file' onchange="displayImage(this, 'munLogo');" title="select ward logo" />
               </label>
           </p>
     </div>