<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class closeVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:close_businesses',
            'issuedDate' => 'required',
            'chalaniNumber' => 'required|unique:close_businesses',
            'municipality' => 'required',
            'wardNumber' => 'required|numeric',
            'organizationAddress' => 'required',
            'organizationType' => 'required',
            'organizationWard' => 'required|numeric',
            'title' => 'required',
            'name' => 'required',
            'closedDate' => 'required',
            'viewDate' => 'required',
            // 'businessCloseDetail' => 'required',
            'authorizedPerson' => 'required'

        ];
    }
}
