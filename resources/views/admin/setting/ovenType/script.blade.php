<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditovenType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of oven type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmovenType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of oven type is required"
            }
        });
	var btnAddovenType = $('.btnAddovenType');
	btnAddovenType.on('click', function(e){
		$('#ovenTypeModel').modal('show'); 
	});

	var btnEditovenType = $('.btnEditovenType');
	btnEditovenType.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.ovenTypeId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editovenTypeModel").modal('show');
			}
		})
	});

	var frmUpdateovenType = $('#frmUpdateovenType');
  frmUpdateovenType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.ovenTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/ovenType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editovenTypeModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editovenTypeModel').modal('hide');
});
});
</script>