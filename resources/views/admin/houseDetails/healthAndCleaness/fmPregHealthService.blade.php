<div class="form-row">
  <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
    <div class="row color_red">
        <div class="form-group col-md-12">{{-- 12 --}}
            <p>
                १.विगत ५ वर्ष भित्र तपाईको परिवारका सुत्केरी आमाले प्राप्त गरेको स्वास्थ्य सेवा सम्बन्धी विवरण               
            </p>
            <br>
            <table border="1">
                <thead>
                    <tr>
                        <th width="15%">गर्भणी पटक</th>
                        <th>उमेर</th>
                        <th>स्वास्थ्य परीक्षण पटक</th>
                        <th>प्रसुति गरिएको स्थान</th>
                        <th>भिटामिन ए को सेवन</th>
                        <th>बच्चा जन्मेपछि आमाले खाएको आइरन चक्की संख्या </th>
                        <th>स्तनपान गराएको अवधि (महिना)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $childNumArr = [
                        'पहिलो बच्चा जन्मदा',
                        'दोस्रो बच्चा जन्मदा',
                        'तेश्रो बच्चा जन्मदा',
                        '4th बच्चा जन्मदा',
                        '5th बच्चा जन्मदा'
                    ];                    
                    $pregnancyHealthServiceDetail = isset($hsPHS->hsPregHealthServiceDetails) ? json_decode($hsPHS->hsPregHealthServiceDetails) : '';

                    for($count = 1; $count <= 5; $count++ ){
                        $pregHealthServiceSingle = moreHouseDetailsFormUpdate($count, $pregnancyHealthServiceDetail, $compareJsonKey = 'childReference');
                        ?>
                        <tr> 
                            <td>
                                {{ $childNumArr[($count - 1)] }}                            
                            </td>
                            <td>
                                <input type="hidden" name="hsPregHealthServiceDetails[<?php echo $count;  ?>][childReference]" value="{{ $count }}">

                                {{ Form::number("hsPregHealthServiceDetails[$count][age]", isset($pregHealthServiceSingle['age']) ? $pregHealthServiceSingle['age'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'number', 'age' => '']) }}
                            </td>
                            <td>
                                {{ Form::number("hsPregHealthServiceDetails[$count][healthCheckupCount]",  isset($pregHealthServiceSingle['healthCheckupCount']) ? $pregHealthServiceSingle['healthCheckupCount'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("hsPregHealthServiceDetails[$count][deliveryLocation]",  isset($pregHealthServiceSingle['deliveryLocation']) ? $pregHealthServiceSingle['deliveryLocation'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'text', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("hsPregHealthServiceDetails[$count][haveVitaminA]",  isset($pregHealthServiceSingle['haveVitaminA']) ? $pregHealthServiceSingle['haveVitaminA'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'text', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("hsPregHealthServiceDetails[$count][ironTabletCount]",  isset($pregHealthServiceSingle['ironTabletCount']) ? $pregHealthServiceSingle['ironTabletCount'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'text', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("hsPregHealthServiceDetails[$count][satnapanDuration]",  isset($pregHealthServiceSingle['satnapanDuration']) ? $pregHealthServiceSingle['satnapanDuration'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="msgDisplay"></div>
</div>
</div>