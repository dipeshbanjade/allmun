<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissioner_details', function (Blueprint $table) {
            $table->increments('id', 100);

            $table->string('refCode', 100)->unique();

            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->date('issuedDate');
            $table->string('chalaniNum');
            $table->string('municipalityName1');
            $table->string('municipalityName2');
            $table->string('wardNumber');
            $table->string('badi');
            $table->string('pratibadi');
            $table->string('caseName');
            $table->string('complaintRequest');
            $table->string('writtenAnswer');
            $table->string('agreedOn');
            $table->string('dafaNumber');
            $table->string('municipalityName3');
            $table->string('badiWard');
            $table->string('badiName');
            $table->string('municipalityName4');
            $table->string('pratibadiWard');
            $table->string('pratibadiName');
            $table->string('year');
            $table->string('month');
            $table->string('day');
            $table->string('eachday');

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');
            
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissioner_details');
    }
}
