@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
        <div class="card-box">
            <div class="card-body">
            <div class="card-head">
                <header>
                   <i class="fa fa-eye"></i>&nbsp;@lang('commonField.links.fieldVisit')
                </header>
            </div>
            <?php $lan = getLan();    ?>
                <div class="table-scrollable" id="dropLabel">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
                        <thead>
                            <tr>
                                <th width="4%">@lang('commonField.extra.sn')</th>
                                <th>@lang('commonField.personal_information.staffName')</th>
                                <th>@lang('commonField.extra.department')</th>
                                <th>@lang('commonField.links.fieldVisit')</th>
                                <th>@lang('commonField.extra.startDate')</th>
                                <th>@lang('commonField.extra.endDate')</th>
                                <th>@lang('commonField.extra.desc')</th>
                                <th>@lang('commonField.extra.approvedBy')</th>
                                <th width="15%">@lang('commonField.extra.action') </th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php $count = 1;   ?>
                           @if(count($fieldVst) > 0)
                             @foreach($fieldVst as $report)
                             <?php
                               
                                $staffName = Auth::user()->userLevel == 'mun' ? getMunStaff($report->staff_municipilities_id) : getWardStaff($report->wards_staff_id);
                              ?>
                            <tr class="odd gradeX">
                                <td>{{ $count ++ }}</td>
                                <td>
                                    {{ $lan == 'np' ? $staffName->firstNameNep .' '. $staffName->middleNameNep .' '. $staffName->lastNameNep : $staffName->firstNameEng .' '. $staffName->middleNameEng .' '. $staffName->lastNameEng    }}
                                </td>
                                <td>{{ $lan == 'np' ? $staffName->departmentNameNep : $staffName->departmentNameEng }}</td>
                                
                                <td>{{ $report->visit_types->nameNep }} - {{ $report->visit_types->nameEng }}</td>
                                <td>{{ $report->startDate }} : {{ $report->startTime }} </td>
                                <td>{{ $report->endDate }} : {{ $report->endTime }} </td>
                                <td title="{!! $report->shortDesc !!}">
                                    {!! str_limit($report->shortDesc , 100) !!}
                                </td>
                                <td>
                                    <?php
                                        if (!empty($report->approveBy)) {
                                            $approveName = Auth::user()->userLevel == 'mun' ? getMunStaff($report->approveBy) : getWardStaff($report->approveBy); 
                                        }
                                    ?>
                                    <span>
                                       <a href="#" data-url="{!! route('approveFieldVisit') !!}"  data-status = "{{ $report->approveStatus == 0 ? 0 : 1 }}" data-id = "{{ $report->id }}" class="changeStatus">
                                       <button class="<?php echo $report->approveStatus==0 ? 'btn btn-danger btn-sm': 'btn btn-success btn-sm'   ?>">
                                         <?php 
                                         echo  $report->approveStatus== 0 ? 'Pending' : 'Approve';    
                                         ?>
                                      </button>
                                        </a>
                                    </span>
                                </td>
                                 <td>
                                    @if($report->approveStatus==0)
                                        <a href="" class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <!--  -->
                                        <a href="" class="delete_data" id="" >
                                        <button class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash-o "></i></button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                              @endforeach
                            @endif
                            </tbody>
                        </table>
                    
                    </div>
                </div>
            </div>
</div>
@endsection