@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
 <div class="col-sm-12">
  <div class="card-box">
    <div class="card-head">
     <header>Permission</header>
     <button id="btnAddRole" class="btn btn-primary btn-xs btn btn-default btn-lg m-b-10 pull-right"><i class="fa fa-plus"><span>Add</span></i></button>
   </div>
   <div class="card-body ">
    <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
 
      <div class="table-scrollable">
         <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" id="example4">
           <thead>
             <tr>
               <th>SN</th>
               <th>Permission Name</th>
               <th>Module Name</th>
               <th>Route Name</th>
               <th>Decription</th>
             </tr>
           </thead>
           <tbody>
             @php  $count = 1  @endphp
             @if(count($permissionData) > 0)
                @foreach($permissionData as $per)
                  <tr>
                    <td>{{ $count ++ }}</td>
                    <td>{{ $per->perName }}</td>
                    <td>{{ $per->moduleName }}</td>
                     <td>{{ $per->routeName }}</td>
                     <td>{{ $per->desc }}</td>
                    </tr>
                @endforeach
             @endif
             
             </tbody>
           </table>
           {{ $permissionData->links() }}
           <div class="text-center">
             <button class="btn btn-outline-primary btn-round btn-sm">Load
               More</button>
             </div>
           </div>
         
       </div>
     </div>
   </div>
 </div>
@endsection
@section('modelSection')
<div class="modal fade" id="frmRole">
  <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <div class="card-head">
         <header><i class="fa fa-plus">&nbsp;Add Permission</i></header>
       </div>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
       {{ Form::open(['route' => 'system.permission.store', 'name'=>'frmRole']) }}
           @include('admin.acl.permission._form')
           <div class="col-lg-12 p-t-20 text-right"> 
           {{ Form::submit('CREATE', ['class' => 'btn btn-success']) }}
           <button class="btn btn-danger" data-dismiss="modal">CANCEL</button>
           </div>
       {{ Form::close() }}
     </div>
     <div class="modal-footer">
     </div>
   </div>
 </div>
</div>
@endsection

@section('custom_script')
<script type="text/javascript">
 var btnAddRole = $('#btnAddRole');
 btnAddRole.on('click', function(){
   $('#frmRole').modal('show');
 });
 $(function(){
  $("form[name='frmRole']").validate({
   rules:{
    perName : {
      required : true,
      minlength : 3,
      maxlength : 100
    },
    moduleName : {
      required : true
    },
    routeName : {
      required : true,
      minlength : 3,
      maxlength : 100
    }
  },
  messages: {
    perName : "Permission name required with min with 3 character",
    moduleName : "Module name required with min with 3 character",
    routeName : "route name required with min with 3 character"
  }
});
});
</script>
@endsection