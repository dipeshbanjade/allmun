<?php

namespace App\model\admin\company;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table    = 'other_offices';
    protected $fillable = ['nameEng','nameNep','phoneNumber','landlineNumber','email', 'addr', 'companyDetails', 'status', 'softDelete', 'municipilities_id', 'wards_id', 'users_id'];
}
