<div class="row top-part">
  <div class="col-md-6">
    <p align="left">
      <b>पत्र संख
        @if(isset($data->refCode))
        {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
        @else
        {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
        @endif
      </b>
    </p>
    <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNumber']) }}</b></p>
  </div>
  <div class="col-md-6">
    <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24">
    </p><h4 align="center"><b>विषय: सिफारिस गरिएको बारे।</b></h4>
    {{ Form::hidden('letter_subject', 'सिफारिस गरिएको बारे') }}
    <p></p>
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">श्री 
      {{ Form::text('accountHolderName', 'करदाता सेवा कार्यालय', ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} ,
      
    </p>
    <p align="left">  {{ Form::text('accountHolderAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'accountHolderAddress']) }} ।</p>
    
  </div>
</div>
<div class="row content-para">
  <div class="col-md-12">
    <p align="left">
      निवेदक श्री/सुश्री/श्रीमती
      <b>
        {{ Form::text('ownerName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'ownerName']) }}
      </b>
      को 
      <b>{{ Form::text('nata', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'nata']) }}</b>
      नाता पर्ने श्री 
      <b>{{ Form::text('natedarName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'natedarName']) }}</b>
      को मिति
      <b>{{ Form::text('date', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'date', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("date")']) }}</b>
      मा मृत्यु भएको हुनाले निज मृतकका नाममा दर्ता कायम रहेको तल उल्लेखित विवरणको घरजग्गा नामसारीको लागि श्री/सुश्री/श्रीमती
      <b>{{ Form::text('applicant', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'applicant']) }}</b>
      ले निवेदन दिनु भएकोमा मृतकका हकदारहरु नाता प्रमाण्ति प्रमाण पत्रमा उल्लेखित भए अनुसार रहेकोले निज मृतकका 
      नाममा रहेको सो घर/जग्गा त्याहाँको नियमानुसार हकदारहरुको नाममा नामसारीको लागि सिफारिससाथ अनुरोध गरिन्छ ।
      <!--  -->

    </div>
  </div>
  <div class="col-md-12">
    <h4 align="center"><b>मृतकका हकदारको विवरण</b></h4>
    <div class="table-responsive">
      <table class="table table-bordered" id="property_table">
        <thead>
          <tr><th>क्र.सं.</th>
            <th>हकदारको नाम </th>
            <th>नाता</th>
            <th>बाबु / पतिको नाम</th>
            <th>नगरिकता नं.</th>
            <th>घर नं. </th>
            <th>कित्ता नं. </th>
            <th>बाटोको नाम</th>
          </tr></thead>
          <tbody id="tableBody1">
            <?php
              $count=1;
              $tableData1= !empty($data)? json_decode($data->tableData1,true) : [''];
            ?>
            @foreach ($tableData1 as $singleData)
              <tr>
                <td>{{$count++}}</td>
                <td>{{ Form::text('hakdarName[]', !empty($singleData['hakdarName']) ? $singleData['hakdarName'] : '' , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('nataRelation[]', !empty($singleData['nataRelation']) ? $singleData['nataRelation'] : '' , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('fathHubName[]', !empty($singleData['fathHubName']) ? $singleData['fathHubName'] : '' , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('citizenNo[]', !empty($singleData['citizenNo']) ? $singleData['citizenNo'] : '' , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('houseNo[]', !empty($singleData['houseNo']) ? $singleData['houseNo'] : '' , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                <td>{{ Form::text('kittaNo[]', !empty($singleData['kittaNo']) ? $singleData['kittaNo'] : '' , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                <td>{{ Form::text('batoName[]', !empty($singleData['batoName']) ? $singleData['batoName'] : '' , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                @if ($count ==2)
                 <td class="add-btns" style="border: 0px;"><a href="" id="addNewRowInTable" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
                @else
                  <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                @endif
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <h4 align="center"><b>नामसारी गर्ने घर/जग्गा विवरण</b></h4>
    <div class="table-responsive">
      <table class="table table-bordered" id="property_table">
        <thead>
          <tr><th>क्र.सं.</th>
            <th>वडा </th>
            <th>सिट नं.</th>
            <th>कित्ता</th>
            <th>क्षेत्रफल</th>
            <th>घर नं.</th>
            <th>कित्ता नं.</th>
            <th>बाटोको नाम / बाटोको प्रकार   </th>
            <th>कैफियत</th>
          </tr></thead>
          <tbody id="tableBody2">
            <?php
              $count2=1;
              $tableData2= !empty($data)? json_decode($data->tableData2,true):[1];
            ?>
            @foreach ($tableData2 as $singleData)
              <tr>
                <td>{{$count2++}}</td>
                <td>{{ Form::text('wardNo[]', !empty($singleData['wardNo']) ? $singleData['wardNo'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('seatNo[]', !empty($singleData['seatNo']) ? $singleData['seatNo'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('kitta[]', !empty($singleData['kitta']) ? $singleData['kitta'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('area[]', !empty($singleData['area']) ? $singleData['area'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                <td>{{ Form::text('houseNo2[]', !empty($singleData['houseNo2']) ? $singleData['houseNo2'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                <td>{{ Form::text('kittaNo2[]', !empty($singleData['kittaNo2']) ? $singleData['kittaNo2'] : NULL , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                <td>{{ Form::text('streetType[]', !empty($singleData['streetType']) ? $singleData['streetType'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalValue']) }}</td>
                <td>{{ Form::text('kaifiyat[]', !empty($singleData['kaifiyat']) ? $singleData['kaifiyat'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'totalValue']) }}</td>
                @if ($count2==2)
                  <td class="add-btns" style="border: 0px;"><a href="" id="addNewRowInTable2" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
                @else
                  <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                @endif
              </tr>
              {{-- expr --}}
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="text-right btm-last">
    <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
    <p> <b>
      <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
        @if(count($deginationsId) > 0)
        @foreach($deginationsId as $deg)
        <option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
          {{ $deg->nameNep }} 
        </option>
        @endforeach
        @endif
      </select>
    </b></p>
  </div>
  <!--views for nibedak detail -->
  <div class="clearfix"></div>
  <hr>
  @include('admin.shifaris.nibedakCommonField')
  <!-- END -->
</div>
<div class="col-md-12">
  <hr>
</div>
</div>
</div>
</div>