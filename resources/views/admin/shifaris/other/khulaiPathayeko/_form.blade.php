<div class="row top-part">
  <div class="col-md-6">
    <p align="left">
      <b>पत्र संख
        @if(isset($data->refCode))
        {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
        @else
        {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
        @endif
      </b>
    </p>
    <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'id' => 'chalaniNum']) }}</b></p>
  </div>
  <div class="col-md-6">
    <p align="right"><b class="mt">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</b></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24">
    </p><h4 align="center"><b>विषय:खुलाई पठाएको।</b></h4>
    {{ Form::hidden('letter_subject', 'सिफारिस गरिएको बारे') }}
    <p></p>
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">श्री 
      {{ Form::text('karyalayaName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }} ,
      कार्यालय,
    </p>
    <p align="left">  {{ Form::text('karyalayaAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'karyalayaAddress']) }} ।</p>
    
  </div>
</div>
<div class="row content-para">
  <div class="col-md-12">
    <p align="left">त्याहाँ सम्मानितअदालतको मिति 
      <b>
        {{ Form::text('date', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'date', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("date")']) }}
      </b> च.नं. 
      <b>{{ Form::text('chalaniNum', null, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>
       को पत्रानुसार यस वडा कार्यालयबाट प्राविधिक मुल्यांकन गरी यसै पत्र साथ कार्यरत प्राविधिको सक्कल प्रतिबेदन संलग्न राखी पठाइएको व्यहोरा अनुरोध छ ।
     <!--  -->
     </div>
</div>

<div class="text-right btm-last">
  <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
  <p> <b>
    <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
      @if(count($deginationsId) > 0)
      @foreach($deginationsId as $deg)
      <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
        {{ $deg->nameNep }} 
      </option>
      @endforeach
      @endif
    </select>
  </b></p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
</div>
<div class="col-md-12">
  <hr>
</div>
</div>
</div>
</div>