<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenInfosTable extends Migration
{
    public function up()
    {
        Schema::create('citizen_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('idEncrip')->nullable();
            
            $table->integer('createdBy')->unsigned()->nullable();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            
             $table->integer('family_living_id')->nullable()->unsigned();
            $table->foreign('family_living_id')->references('id')->on('living_fors')->onDelete('cascade');
            

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->string('refCode', 100)->unique();
            $table->boolean('haveCitizen')->default(0);
             $table->string('printCount')->nullable();
            $table->string('citizenNo', 100)->nullable();


            $table->string('profilePic')->nullable();
            $table->string('annualIncome')->nullable();

            $table->enum('gender', ['male', 'female', 'other']);
            
            $table->string('fnameNep');
            $table->string('mnameNep')->nullable();
            $table->string('lnameNep')->nullable();

            $table->string('fnameEng');
            $table->string('mnameEng')->nullable();
            $table->string('lnameEng');
            $table->integer('gharmuli_id')->nullable();

            $table->date('dobAD')->nullable();
            $table->date('dobBS');

            $table->integer('provinces_id')->unsigned()->nullable();
            $table->foreign('provinces_id')->references('id')->on('provinces')->onDelete('cascade');

            $table->integer('nationalities_id')->unsigned()->nullable();
            $table->foreign('nationalities_id')->references('id')->on('nationalities')->onDelete('cascade');

            $table->integer('districts_id')->unsigned()->nullable();
            $table->foreign('districts_id')->references('id')->on('districts')->onDelete('cascade');
            
            /*-----------------------------------------*/
            $table->integer('qualifications_id')->unsigned()->nullable();
            $table->foreign('qualifications_id')->references('id')->on('qualifications')->onDelete('cascade');
            /*------------------------------*/
            
            $table->integer('occupations_id')->unsigned()->nullable();
            $table->foreign('occupations_id')->references('id')->on('occupations')->onDelete('cascade');


            $table->integer('citizen_types_id')->unsigned()->nullable();
            $table->foreign('citizen_types_id')->references('id')->on('citizen_types')->onDelete('cascade');
            /*------------------------------*/ 

            $table->integer('religiouses_id')->unsigned()->nullable();
            $table->foreign('religiouses_id')->references('id')->on('religiouses')->onDelete('cascade');
            /*------------------------------*/

            $table->integer('sys_languages_id')->unsigned()->nullable();
            $table->foreign('sys_languages_id')->references('id')->on('sys_languages')->onDelete('cascade');
            /*------------------------------*/

            $table->integer('jatjatis_id')->unsigned()->nullable();
            $table->foreign('jatjatis_id')->references('id')->on('jatjatis')->onDelete('cascade');
            /*------------------------------*/

            $table->string('wardNo')->nullable();

            $table->string('villageNameEng');
            $table->string('villageNameNep')->nullable();

            $table->string('phoneNumber', 50)->nullable();
            $table->string('email', 100)->nullable();

            $table->integer('maritialStauts_id')->nullable();
            $table->integer('disabilities_id')->nullable();

            $table->boolean('havePassport')->default(0);
            $table->string('passportNumber')->nullable();


            $table->string('citizenshipIssuePlace')->nullable();
            $table->date('citizenshipIssueDate')->nullable();

            $table->string('birthplace')->nullable();

             /*--------------------------------------*/
             $table->string('houseNumber')->nullable();
             $table->string('MunHouseNumber')->nullable();

             $table->integer('maritials_id')->nullable();
             $table->string('son')->nullable();
             $table->string('daughter')->nullable();
             $table->string('other')->nullable();
             $table->text('diseaseName')->nullable();
             $table->text('abroadGone')->nullable();
             
            $table->boolean('isHouseOwner')->default(0);
            $table->boolean('isDeath')->default(0);
            $table->boolean('isdisabled')->default(0);
            $table->string('citizenImgPath')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('citizen_infos');
    }
}
