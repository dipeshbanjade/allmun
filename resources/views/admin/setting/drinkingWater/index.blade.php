@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
 <div class="card-box">
  <div class="card-body">
    <div class="card-head">
      <header><i class="fa fa-eye">
       &nbsp;@lang('commonField.houseHoldSetting.drinkingWater')</i>
     </header>
   </div>
   <button id="" class="btnAddDrinkingWater btn btn-primary pull-right">
     <i class="fa fa-plus"></i>
     <span> @lang('commonField.houseHoldSetting.createDrinkingWater')</span>
   </button>

   @if(count($allDrinkingWater) > 0)
   <div class="table-scrollable" id="dropLabel">
    <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
     <thead>
      <tr>
       <th>@lang('commonField.extra.sn')</th>
       <th>@lang('commonField.extra.refCode')</th>
       <th>@lang('commonField.houseHoldSetting.drinkingWater')</th>
       <th>@lang('commonField.extra.action') </th>
     </tr>
   </thead>
   <tbody>
    @php $count = 1  @endphp
    @foreach($allDrinkingWater as $drinkingWater)
    <tr class="odd gradeX">
     <td>{{ $count ++ }}</td>
     <td>{{ $drinkingWater->refCode }}</td>
     <td >{{ $drinkingWater->nameNep ?? 'N/A' }} - {{ $drinkingWater->nameEng }} </td>

     <td class="left" >
       
       <a href="#" data-url="{{ route('drinkingWater.edit', $drinkingWater->id) }}" class="btnEditdrinkingWater btn btn-primary btn-xs" id="" >
         <i class="fa fa-pencil"></i>
       </a>
       <a href="#" class="deleteMe" data-url="{{ route('drinkingWater.delete', $drinkingWater->id )}}" data-name="{{ $drinkingWater->nameEng }}">
        <button class="btn btn-danger btn-xs">
          <i class="fa fa-trash-o "></i>
          </button>
        </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endif
</div>
</div>
</div>
@endsection
  @section('modelSection')
<div class="modal" id="drinkingWaterModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.houseHoldSetting.addDrinkingWater')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ Form::open(['route' => 'drinkingWater.store', 'name'=>'frmdrinkingWater']) }}
        @include('admin.setting.drinkingWater._form')
        <div class="col-lg-12 p-t-20 text-right"> 
          {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
          <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
        </div>
        {{ Form::close() }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">

      </div>

    </div>
  </div>
</div>


<!-- edit model for drinkingWater starts here -->

<div class="modal" id="editdrinkingWaterModel">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.houseHoldSetting.editDrinkingWater')</i></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       {{ Form::open(['name'=>'frmeditdrinkingWater', 'id' => 'frmUpdatedrinkingWater']) }}
       @include('admin.setting.drinkingWater._form')
       {{ Form::hidden('drinkingWaterId', null, ['class' => 'drinkingWaterId']) }}
       <div class="col-lg-12 p-t-20 text-right"> 
        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
      </div>
      {{ Form::close() }}
    </div>

    <!-- Modal footer -->
    <div class="modal-footer">

    </div>

  </div>
</div>
</div>

<!-- edit model for drinkingWater ends here -->
@endsection

@section('custom_script')
@include('admin.setting.drinkingWater.script')
@endsection