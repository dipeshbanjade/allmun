<style type="text/css">
table th, table td{
    border: 1px solid black;
    text-align: center;
}
</style>
<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <p>
                    १.तपाईको परिवारमा बसोबास गर्नु हुने कुनै महिला सदस्यले वितेको १२ महिनामा आफ्नै कोखबाट बच्चा जन्माउनु भएको छ ?
                </p>
                <div>
                    <label class="radio-inline">
                        १. छ 
                        <input type="radio" onchange="checkFertility(this)" name="haveGivenBirth" value="1" {{ isset($familyFertilityDetails) && $familyFertilityDetails->haveGivenBirth == '1' ? 'checked' : '' }} >
                    </label> 
                    <label class="radio-inline">
                        २. छैन्
                        <input type="radio" onchange="checkFertility(this)" name="haveGivenBirth" value="0" {{ isset($familyFertilityDetails) && $familyFertilityDetails->haveGivenBirth == '0' ? 'checked' : '' }}>
                    </label> 
                </div>
                <br>
                @if (isset($familyFertilityDetails) && $familyFertilityDetails->haveGivenBirth == '1')
                   <table class="table fertilityTable" style="display: block;">
                @else
                    <table class="table fertilityTable"  style="display: none;">
                @endif
                
                  <tr>
                    <th rowspan="2">बच्चाको नाम</th>
                    <th rowspan="2">लिङ्ग</th>
                    <th colspan="2">जन्मेको मिति</th>
                    <th colspan="2">जन्मेको स्थान</th>
                </tr>
                <tr>
                    <td>साल</td>
                    <td>महिना</td>
                    <td>घरमा</td>
                    <td>अस्पताल वा स्वास्थ्य संस्थामा</td>
                </tr>
                <?php
                $count = 0;
                $hsFertilityInfoDetail = isset($familyFertilityDetails->hsFertilityInfo) ? json_decode($familyFertilityDetails->hsFertilityInfo) : '';
                    //Note $getMyFamilyDetails from houseDetailsController
                foreach($getMyFamilyDetails as $member){
                    $memberInfo = getParentsName($member->relation_id);
                    //dd($memberInfo);                
                    $memberAge = convertMyAge($memberInfo['dobAD']);

                    if(isset($memberAge) && $memberAge <= 1){                        
                        $count++; 
                        $singleHsFertility = moreHouseDetailsFormUpdate($member->relation_id, $hsFertilityInfoDetail, $compareJsonKey = 'childId');
                            //dd($singleEducationDropper);
                        ?>
                        <tr>
                            <td>
                                <input type="hidden" name="hsFertilityInfo[<?php echo $count;  ?>][childId]" value="{{ $member->relation_id }}">                                
                                {{ Form::text("hsFertilityInfo[$count][childName]", $memberInfo['fnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("hsFertilityInfo[$count][childGender]", $memberInfo['gender'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("hsFertilityInfo[$count][birthYear]", isset($singleHsFertility) ? $singleHsFertility['birthYear'] : '' , ['class'=>'mdl-textfield__input birthLocation dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                {{ Form::text("hsFertilityInfo[$count][birthMonth]",  isset($singleHsFertility) ? $singleHsFertility['birthMonth'] : '', ['class'=>' birthLocation mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                            </td>
                            <td>
                                <input type="radio" class="birthLocation" name="hsFertilityInfo[<?php echo $count; ?>][birthLocation]" value="house" {{ isset($singleHsFertility['birthLocation']) && $singleHsFertility['birthLocation'] == 'house' ? 'checked' : '' }}>
                            </td>
                            <td>
                                <input type="radio" class="birthLocation" name="hsFertilityInfo[<?php echo $count; ?>][birthLocation]" value="hospital" {{ isset($singleHsFertility['birthLocation']) && $singleHsFertility['birthLocation'] == 'hospital' ? 'checked' : '' }}>
                            </td>
                        </tr>
                        <?php }} ?>                                     

                    </table>
                    <br>
                </div>     
            </div>
            <div class="msgDisplay"></div>
        </div>
    </div>
