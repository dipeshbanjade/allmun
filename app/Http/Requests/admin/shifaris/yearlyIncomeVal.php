<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class yearlyIncomeVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:yearly_income_certs',
            'chalaniNum' => 'required|unique:yearly_income_certs',
            'issuedDate' => 'required',
            'topic' => 'required', 
            'personAddress' => 'required',
            'personDetails' => 'required',
            'detail' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
