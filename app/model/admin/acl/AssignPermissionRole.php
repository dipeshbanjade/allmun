<?php

namespace App\model\admin\acl;
use Illuminate\Database\Eloquent\Model;
class AssignPermissionRole extends Model
{
    protected $table = 'permission_role';
    protected $fillable = ['permissions_id', 'roles_id'];
    public $timestamps  = false;

}
