<?php

namespace App\model\shifaris;

use Illuminate\Database\Eloquent\Model;

class TaxClearCertification extends Model
{
    protected $table = 'tax_clear_certifications';
}
