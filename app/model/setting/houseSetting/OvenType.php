<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class OvenType extends Model
{
 	protected $table = 'oven_types';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
