<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditreligion']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of religion is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmreligion']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of religion is required"
            }
        });
  var btnAddReligion = $('.btnAddReligion');
  btnAddReligion.on('click', function(e){
      $('#religionModel').modal('show'); 
  });

  var btnEditReligion = $('.btnEditReligion');
  btnEditReligion.on('click', function(e) {
      e.preventDefault();
      var url = $(this).data('url');
      $.ajax({
        'type': 'GET',
        'url': url,
        success: function (response) {
            $('.religiousId').val(response.id);
            $('.txtRefCode').val(response.refCode).attr('disabled',true);
            $('.txtNameNep').val(response.nameNep);
            $('.txtNameEng').val(response.nameEng);
            $("#editReligiousModel").modal('show');
        }
    })
  });


  var frmUpdatereligion = $('#frmUpdatereligion');
  frmUpdatereligion.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.religiousId').val();
    var url  = "{{URL::to('/')}}" + "/setting/religion/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editReligiousModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editReligiousModel').modal('hide');
});
});


</script>