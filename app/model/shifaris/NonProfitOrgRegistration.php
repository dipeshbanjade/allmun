<?php

namespace App\model\shifaris;

use Illuminate\Database\Eloquent\Model;

class NonProfitOrgRegistration extends Model
{
	protected $table = 'non_profit_org_registrations';
}
