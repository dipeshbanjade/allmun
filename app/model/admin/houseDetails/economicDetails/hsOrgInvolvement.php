<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class hsOrgInvolvement extends Model
{
    protected $table = 'hs_org_involvements';
}
