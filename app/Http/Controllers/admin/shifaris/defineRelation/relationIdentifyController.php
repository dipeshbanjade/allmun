<?php

namespace App\Http\Controllers\admin\shifaris\defineRelation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\RelationIdentify;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\relationIdentityVal;
use DB;
use Auth;

use App\model\mylogic;

class relationIdentifyController extends Controller
{
    protected $viewPath = 'admin.shifaris.defineRelation.relationIdentify';
    protected $shifarisTableName     = 'relation_identifies';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixRelationIdentify');

    }

    public function index()
    {
        $page['page_title']       = 'Relation Identify';
        $page['page_description'] = 'Relation Identify';

        $instRecomd = RelationIdentify::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        
    }

    
    public function create()
    {
        $page['page_title']       = 'Relation Identify : create';
        $page['page_description'] = 'Relation Identify details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(relationIdentityVal $request)
    {
        //

        DB::beginTransaction();
        try {
            $relationIdentifyCount = count($request->relationPerson);

            $relationPerson = $request->relationPerson;
            $relation = $request->relation;

            $relationIdentifyArray = [];

            for ($i=0; $i < $relationIdentifyCount; $i++) {
                array_push($relationIdentifyArray, [
                    'relationPerson' => $relationPerson[$i],
                    'relation' => $relation[$i]
                ]);
            }
            $relationIdentifyJson = json_encode($relationIdentifyArray);


                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

            $isShifarisExist = isset($request->shifarisId) ? RelationIdentify::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? RelationIdentify::findOrfail($isShifarisExist['id']) : new RelationIdentify;

            $save->createdBy = Auth::user()->id;

            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";

            $save->issuedDate  = $request->issuedDate;            
            $save->chalaniNum = $request->chalaniNum;
            $save->name = $request->name;
            $save->municipalityName = $request->municipalityName;
            $save->wardNumber = $request->wardNumber;
            $save->municipality = $request->municipality;
            $save->ward = $request->ward;
            $save->providedDate = $request->providedDate;

            $save->dataTable = $relationIdentifyJson;                

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;
            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
             DB::commit();
             return response()->json([
                 'success' => true,
                 'message' => config('activityMessage.saveMessage')
             ]);
                   // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
         }else{
             return response()->json([
                 'success' => false,
                 'message' => config('activityMessage.unSaveMessage')
             ]);
                   // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     }catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
    }


}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
