<?php

namespace App\model\admin\houseDetails\childrenEducation;

use Illuminate\Database\Eloquent\Model;

class ChildClubInvolve extends Model
{
    protected $table = 'child_club_involves';
}
