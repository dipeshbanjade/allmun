<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class occupation extends Model
{
    protected $table = 'occupations';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
