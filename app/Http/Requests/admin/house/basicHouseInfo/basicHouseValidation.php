<?php

namespace App\Http\Requests\admin\house\basicHouseInfo;

use Illuminate\Foundation\Http\FormRequest;

class basicHouseValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'hsNum'           => 'required|min:3|max:30|unique:house_details',
            'fmHeadPh' => 'required',
            'hsEstd'        => 'required'
        ];
    }
}
