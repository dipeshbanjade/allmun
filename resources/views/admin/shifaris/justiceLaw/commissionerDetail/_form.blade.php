<?php $checker = getMunicipalityData(); ?>

<div class="row top-part">
  <div class="col-md-6 ps-cn">
    <p><b class="ps">प. सं.:</b>
      @if(isset($data->refCode))
      {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
      @else
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
      @endif
    </p>
    <p align="left" class="star">
      <b class="cn">च. नं.:</b>
      {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
    </p>
  </div>
  <div class="col-md-6">
    <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
  </div>
</div>

<div class="col-md-12">
  <div>
    <h5>
      <b>श्री  
       {{ Form::text('municipalityName1', isset($data->municipalityName1) ? $data->municipalityName1 : $checker->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName1', 'required' => 'required']) }}
       महा/उप/नगर/गाउँ-पालिकाको कार्यालयमा चढाएको
     </b>
   </h5>
 </div>
 <p align="center" class="font-size-24"></p>
 <h3 align="center"><b><u>संयुक्त दरखास्त पत्र।</u></b></h3>
</div>
  <div class="col-md-12">
    <p align="center">
      {{ Form::text('municipalityName2', isset($data->municipalityName2) ? $data->municipalityName2 : $checker->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName2', 'required' => 'required']) }}
      महा/उपमहा/नगरपालिका/गाउँपालिका वडा न   
      {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
      बस्ने वर्ष 
      {{ Form::text('badi', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'badi', 'required' => 'required']) }}
    </p>
    <p align="center">
      ऐ. ऐ.  बस्ने वर्ष  
      {{ Form::text('pratibadi', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'pratibadi', 'required' => 'required']) }}
    </p>
    <p class="corner" style="width: 10%;" align="right">संयुक्त  दर्खास्तवाला <br>वादी<br>प्रतिवादी </p> 
    <div class="clearfix"></div>
    <div class="col-md-12">
      <p align="center" class="font-size-24" style="padding-top: 20px;"></p>
      <h4 align="center">
        <b>मुद्दा: 
          {{ Form::text('caseName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'caseName', 'required' => 'required']) }}।
        </b>
      </h4>
    </div>
    <p>हामी संयुक्त दर्खास्तवाला लाग्ने दस्तुर साथै राखी निम्न व्यहोरा निवेदन गर्दछौ।<br>१. 
     {{ Form::text('complaintRequest', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'complaintRequest', 'required' => 'required']) }}
     वादीको उजुरी निवेदन।<br> २. 
     {{ Form::text('writtenAnswer', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'writtenAnswer', 'required' => 'required']) }}
     लिखित जबाफ।<br> ३. यसरी हामी बीच मुद्दा परेतापनि 
     {{ Form::text('agreedOn', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'agreedOn', 'required' => 'required']) }}
     सहमत भई मिलापत्र गर्न मञ्जुर भई मुलुकी एन अदालती बन्दोबस्तको
     {{ Form::text('dafaNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'dafaNumber', 'required' => 'required']) }}
     न. बमोजिम मिलापत्र गरी पाउन यो संयुक्त दरखास्त पेश गरेको छौ।<br> ४. लेखिएको व्यहोरा ठीक  साँचो छ, झुट्ठा ठहरे कानुन बमोजिम सँहुला बुझाँउला।
   </p>
   <h4 align="right"><b> संयुक्त दरखास्तवाला</b></h4>
   <p> वादी
     {{ Form::text('municipalityName3', isset($data->municipalityName3) ? $data->municipalityName3 : $checker->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName3', 'required' => 'required']) }}
     महा/उपमहा/नगरपालिका/गाउँपालिका वडा न  
     {{ Form::text('badiWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'badiWard', 'required' => 'required']) }} 
     बस्ने वर्ष 
     {{ Form::text('badiName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'badiName', 'required' => 'required']) }} १
   </p> 
   <br>
   <p> प्रतिवादी
     {{ Form::text('municipalityName4', isset($data->municipalityName4) ? $data->municipalityName4 : $checker->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName4', 'required' => 'required']) }}
     महा/उपमहा/नगरपालिका/गाउँपालिका वडा न 
     {{ Form::text('pratibadiWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'pratibadiWard', 'required' => 'required']) }} 
     बस्ने वर्ष 
     {{ Form::text('pratibadiName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'pratibadiName', 'required' => 'required']) }} १  <br>
   </p>
   <p>ईति सम्बत् २०
     {{ Form::text('year', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'year', 'required' => 'required']) }}
     साल
     {{ Form::text('month', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'month', 'required' => 'required']) }}
     महिना 
     {{ Form::text('day', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'day', 'required' => 'required']) }} 
     गते रोज
     {{ Form::text('eachday', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'eachday', 'required' => 'required']) }}
     शुभम.......
   </p>
   <!--views for nibedak detail -->
   <div class="clearfix"></div>
   <hr>

   @include('admin.shifaris.nibedakCommonField')
   <!-- END -->
   <div class="col-md-12">
    <hr>
  </div>
</div>
</div>