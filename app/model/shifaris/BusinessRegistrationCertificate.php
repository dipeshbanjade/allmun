<?php

namespace App\model\shifaris;

use Illuminate\Database\Eloquent\Model;

class BusinessRegistrationCertificate extends Model
{
    //
    protected $table = 'business_registration_certificate';
}