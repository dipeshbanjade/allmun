@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>New Business Pan Number <small> नयाँ कारोबार पान नं</small></header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>
	<div class="right_col nep" role="main" style="min-height: 570px;">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">

					<div class="x_title">
						<h3 class="title-x">नयाँ कारोबार पान नं</h3>

						<div class="clearfix"></div>
					</div>
					<div class="x_content" id="block">
						<div id="printingDiv">
							@include('admin.shifaris.municipalityDetail')
							
							{{ Form::open(['route' => 'admin.permAccounting.store', 'name' => 'frmWardpermAccounting', 'class'=>'sifarishSubmitForm']) }}
							@include('admin.shifaris.business.permAccounting._form')
						</div>
					</div>
					<!-- <p class="pull-right">
						<button type="submit" class="btn btn-success pull-right">Save</button>
						<button type="button" class="btn btn-success pull-right" onclick="printDiv()">Print</button>
					</p> -->
					@include('admin.shifaris.commonButton')
					{{ Form::close() }}
					<!-- <input type="hidden" id="getValue" value="<?php 
						// echo asset('lib/css/sifarish.css');
					?>"	> -->
				</div>
			</div>
			<div class="msgDisplay" style="z-index: 999999 !important"></div>

		</div>
	</div>

	@endsection
	@section('customStyle')
	<!-- Shifarish CSS linking -->
	<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
	{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}

	<!-- Shifarish CSS linking -->
	@endsection
	@section('custom_script')
	@include('admin.shifaris.script')
	@include('admin.shifaris.business.permAccounting.script')
	@endsection