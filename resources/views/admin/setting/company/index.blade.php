@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
                <header><i class="fa fa-eye">&nbsp;@lang('commonField.extra.company')</i></header>
            </div>

            <button id="" class="companyFrm btn btn-primary pull-right"><i class="fa fa-plus"></i><span>@lang('commonField.extra.createCompany')</span></button>
            <div class="table-scrollable" id="dropLabel">
              <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                         <thead>
                          <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">sn</th>
                            
                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">@lang('commonField.extra.company')</th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">@lang('commonField.personal_information.phoneNumber')</th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">@lang('commonField.personal_information.address')</th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">@lang('commonField.personal_information.email')</th>

                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 250px;">@lang('commonField.personal_information.details')</th>

                          <th style="width: 250px;">@lang('commonField.extra.action')</th>
                        </tr>
                  </thead>
                  <tbody>
                      @if(count($companies) > 0)
                          @php $count = 1    @endphp
                            @foreach($companies as $company)
                              <tr>
                                 <td>{{ $count ++ }}</td>
                                 <td>{{ $company->nameNep }}</td>
                                 <td>{{ $company->phoneNumber }} // {{ $company->landlineNumber }}</td>
                                 <td>{{ $company->addr }}</td>
                                 <td>{{ $company->email }}</td>
                                 <td title="{{ $company->companyDetails }}">{!! str_limit($company->companyDetails, 150) !!}</td>
                                 <td>
                            
                                  <span> 
                                    <a href="#"  title="Edit">
                                      <i class="fa fa-fw fa-lg fa-pencil-square-o btnUpdatecompany" data-toggle="modal" data-target="#updateUNit" data-id="{{ $company->id }}" data-url="{!! route('company.edit', $company->id) !!}"></i>
                                  </a>
                                  </span>

                                  <span>
                                    <a href="" class="txt-color-red deleteMe" 
                                        data-url="{!! route('company.delete', $company->id ) !!}" title="delete company name" data-name="{{ $company->name }}" data-id = "{{ $company->id }}">
                                       <i class="fa fa-fw fa-lg fa-trash-o deletable text-danger"> </i> </a>
                                   </span>
                                 </td>
                              </tr>
                            @endforeach()
                      @endif
                  </tbody>
                  
                </table>
                </div>
              </div>
              <?php echo $companies->render(); ?>
              <!-- /.box-body -->
            </div>
            </div>

        @endsection
        @section('modelSection')
         <div id="frmAddcompany" class="modal fade" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">@lang('commonField.extra.addCompany')</h4>
                  </div>
                  <div class="modal-body">
                       {!! Form::open(['route'=>'company.store', 'name'=>'frmcompany', 'method'=>'post', 'id'=>'addcompany']) !!}
                            @include('admin.setting.company._form')
                            <p class="pull-right">
                                {!! Form::submit(__('commonField.button.create'), ['class'=>'btn btn-success']) !!}

                                <button class="btn btn-danger" onclick="window.history.go(0); return false;">@lang('commonField.button.close')</button>
                          </p>
                      {!! Form::close() !!}
                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>

              </div>
            </div>

            <!-- modal update  -->
            <!-- modal -->
                  <div class="row">
                    <div id="modalcompany" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Update Company</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['id'=>'frmUpdatecompany', 'name'=>'frmCountry']) !!}
                                {{ Form::hidden('companyId',null, ['class'=>'companyId']) }}
                               
                                @include('admin.setting.company._form')

                                <div class="form-group col-sm-12 ">
                                   <p class="pull-right">
                                      {{ Form::submit(__('commonField.button.update'), ['class'=>'btn btn-success frmUpdatecompany']) }}


                                <button class="btn btn-danger" onclick="window.history.go(0); return false;">@lang('commonField.button.close')</button>
                                 </p>
                                </div>
                           {!! Form::close() !!}
                        </div>
                        <div class="modal-footer">
                               
                      </div>
                    </div>
                  </div>
                  </div>
                  </div>
        @endsection
<!-- modal ended here -->
@section('custom_script')
      <script type="text/javascript">
    $(function(){
      $('.companyFrm').on('click', function(){
        $('#addcompany')[0].reset();
         $('#frmAddcompany').modal('show');
      });
      /*--------------------------------------*/
        $("form[name='frmcompany']").validate({
         rules:{
          nameNep : {
            required: true,
            minlength : 6,
            alphanumeric: true,
            maxlength : 255
          },   
          nameEng : {
            required: true,
            minlength : 6,
            maxlength : 255
          },
          phoneNumber : {
            number:true,
            minlength : 10,
            maxlength : 10
          },
          landlineNumber : {
            minlength: 9,
            maxlength : 9
          },
          email : {
            email : true,
          }
         },
         messages: {
           nameNep     : "Please enter company name in nepali with more than 6 character", 
           nameEng     : "Please enter company name in english with more than 6 character",
           phoneNumber  : "enter correct number with 10 digit",
           addr    : "company full address",
           email   : "email address must be in correct format"
         }
        });
      });
  </script>

  <!-- edit script -->
   <script type="text/javascript">
    /*------update data---------*/
    var btnUpdatecompany = $('.btnUpdatecompany');
    var name           = $('.name');
        btnUpdatecompany.on('click', function() {
            var url = $(this).data('url');
            $.ajax({
                'type': 'GET',
                'url': url,
                success: function (response) {
                  console.log(response);
                    $('.companyId').val(response.id),
                    $('.txtNameNep').val(response.nameNep);
                    $('.txtNameEng').val(response.nameEng);
                    $('.txtPhoneNumber').val(response.phoneNumber);
                    $('.txtLandLineNumber').val(response.landlineNumber);
                    $('.txtEmail').val(response.email);
                    $('.txtAddr').val(response.addr);
                    $('.txtcompanyDetails').val(response.companyDetails);
                    $("#modalcompany").modal('show');
                }
            })
        });

        /*----------------update---------------*/
        var frmUpdatecompany = $('#frmUpdatecompany');
        frmUpdatecompany.on('submit', function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var id   = $('.companyId').val();
        var url  = "{{URL::to('/')}}" + "/setting/company/"+id+"/update";
        $.ajax({
          'type' : 'POST',
          'url'  : url,
          data    : data,
          success : function(response){
           console.log(response);
           if (response.success==true) {
               $('.infoDiv').append(response.message).addClass('alert alert-success');
               $("#modalcompany").modal('hide');
           }
           if (response.success==false) {
            $.each(response.message, function(key, value){
                $('.infoDiv').append(value).addClass('alert alert-danger');
           })
         }

          },complete:function(){
           setTimeout(location.reload.bind(location), 3000);
          }
        })
        .fail(function (response) {
            alert('error while insert');
        });
        $('#modalcompany').modal('hide');
        });
  </script>
@endsection