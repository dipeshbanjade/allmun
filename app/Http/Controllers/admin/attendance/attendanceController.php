<?php

namespace App\Http\Controllers\admin\attendance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\municipility\StaffMunicipility;
use App\model\admin\ward\WardStaff;
use App\model\setting\Department;
use App\model\mylogic;
use Excel;
use DB;

use Auth;

class attendanceController extends Controller
{
    protected $viewPath     = 'admin.attendance';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()    // muncipality attendance
    {
        $page['page_title']       = 'Attendance';
        $page['page_description'] = 'staff attendance';
        $dept = $this->getDepartment();
        
        $usrLbl =  getUsrlevel();
        switch ($usrLbl) {
            case 'dev':
                return back()->withMessage(config('activityMessage.developer'));
                break;

            case 'mun':
                    $staff = mylogic::getStaffDetails();
                    return view($this->viewPath . '.index', compact(['page', 'staff', 'dept']));
                break;

             case 'wrd':
                    $staff = mylogic::getStaffDetails();
                    return view($this->viewPath . '.index', compact(['page', 'staff', 'dept']));
                 break;
            
            default:
                # code...
                break;
        }
    }

    public function getStaffAttendance(Request $request){
      $page['page_title']       = 'Attendance';
      $page['page_description'] = 'staff attendance';

      $dept      = $this->getDepartment();
      $startDate = $request->startDate;
      $endDate   = $request->endDate;
      $pounchId   = $request->pounchId;

      $nepStartDate = $request->startDateNep;
      $nepEndDate   = $request->endDateNep;

      $logInUsr = getUsrlevel();
      $deviceId  = $logInUsr == 'mun' ? mylogic::getMunicipalityDetails()['bioDeviceId'] : mylogic::getWardDetailsById()['bioDeviceId'];

      $tag = isset($request->tag) ? $request->tag : '';
      $lan = getLan();
      $staffName = $this->getStaffNameWithPounchId($pounchId);
      $staffNameLan = $lan == 'np' ? $staffName->firstNameNep . ' ' . $staffName->middleNameNep . ' ' . $staffName->lastNameNep : $staffName->firstNameEng . ' ' . $staffName->middleNameEng . ' ' . $staffName->lastNameEng; 
      /*-----------------------*/
      switch ($logInUsr) {
          case 'mun':
              $staff = mylogic::getStaffDetails();
              $attendance = $deviceId &&  $pounchId ? $this->getAttendance($startDate, $endDate, $deviceId, $pounchId) : '';

              $tag && $tag == 'withFile' ?  $this->exportAttendance($staffNameLan,$startDate, $endDate, $nepStartDate,$nepEndDate, $attendance) : '';
              return view($this->viewPath . '.index', compact(['page', 'staff', 'dept', 'attendance', 'nepStartDate', 'nepEndDate', 'startDate', 'endDate', 'staffNameLan']));
              break;

          case 'wrd':
              $staff = mylogic::getStaffDetails();
              $attendance = $deviceId &&  $pounchId ? $this->getAttendance($startDate, $endDate, $deviceId, $pounchId) : '';
              
              $tag && $tag == 'withFile' ?  $this->exportAttendance($staffNameLan, $startDate, $endDate, $nepStartDate,$nepEndDate, $attendance) : '';

              return view($this->viewPath . '.index', compact(['page', 'staff', 'dept', 'attendance', 'nepStartDate', 'nepEndDate', 'startDate', 'endDate', 'staffNameLan']));
              break;
          default: 
              return back()->withMessage(config('activityMessage.dataNotFound'));
              break;
      }
    }

    private function getAttendance($startDate, $endDate, $deviceId, $pounchId){
       $query = DB::select(
                'SELECT ANY_VALUE(min(CAST(tnxDateTime AS TIME))) AS MINTIME, ANY_VALUE(MAX(CAST(tnxDateTime AS TIME))) AS MAXTIME, MAX(id), MIN(id), DATE(tnxDateTime) as myDate, punchId FROM bio_matrics WHERE punchId = "'.$pounchId.'" AND  DATE(tnxDateTime) BETWEEN  "'.$startDate.'"  AND "'.$endDate.'" AND dvcId = "'.$deviceId.'" GROUP BY DATE(tnxDateTime) ORDER BY DATE(tnxDateTime)' 
        );
       return $query;
    }

    private function getDepartment(){
      return Department::orderBy('nameEng', 'ASC')->get();
    }

    public function exportAttendance($staffName,$startDate, $endDate, $nepStartDate,$nepEndDate, $attendance){
      $staffName = isset($staffName) ? $staffName : 'Attendance';
      Excel::create('Attendance', function($excel) use($staffName, $startDate, $endDate,  $nepStartDate, $nepEndDate, $attendance){
       $excel->sheet('Attendance', function($sheet) use($staffName, $startDate, $endDate,  $nepStartDate, $nepEndDate, $attendance){
         $sheet->loadView($this->viewPath . '.exportAtten', compact(['staffName', 'nepStartDate', 'nepEndDate', 'attendance', 'startDate', 'endDate']));
       });
      })->export('xlsx');
      return true;
    }

    private function getStaffNameWithPounchId($pounchId){
     if (Auth::user()->userLevel == 'mun') {
       $staffName = StaffMunicipility::select('firstNameEng', 'middleNameEng', 'lastNameEng','firstNameNep', 'middleNameNep', 'lastNameNep')->where(['status'=>0, 'softDelete'=>0, 'pounchId'=>$pounchId])->first();
       return $staffName;
     }else{
       $staffName = WardStaff::select('firstNameEng', 'middleNameEng', 'lastNameEng','firstNameNep', 'middleNameNep', 'lastNameNep')->where(['status'=>0, 'softDelete'=>0, 'pounchId'=>$pounchId])->first();
       return $staffName;
     }
    }
}
