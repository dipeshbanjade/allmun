<?php

namespace App\Http\Controllers\admin\municipility;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\municipility\StaffMunicipility;
use App\Http\Requests\admin\municipality\municipalityStaffVal;
use App\model\mylogic;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use DB;
use Validator;

class staffMunicipilityController extends Controller
{
    protected $viewPath = 'admin.municipility.staff';
    protected $munStfTbl = 'staff_municipilities';
    protected $staffTypeTbl = 'staff_types';
    protected $provincesTbl = 'provinces';
    protected $deginationTbl = 'deginations';
    protected $districtTbl = 'districts';
    protected $deptTbl     = 'departments';
    protected $prefix;

    protected $imgProfile;
    protected $imgCitizen;
    protected $munId;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix       = config('activityMessage.prefixMunStaff');
        $this->imgProfile   = config('activityMessage.munStaffImg'); 
        $this->imgCitizen   = config('activityMessage.munStaffCitizenImg'); 
        $this->munId        = myLogic::getMunicipalityDetails()['id'];
    }


    public function index()
    {
        $page['page_title']        = 'Municipality : List';  //TODO from language file
        $page['page_description']  = 'Municipality staff details';

        $allStaff = StaffMunicipility::with(['provinces', 'districts', 'staff_types', 'deginations', 'departments'])->orderby('created_at', 'DESC')->where(['softDelete'=>0])->paginate(config('activityMessage.pagination'));

        foreach ($allStaff as $staff) {
          $role = User::select('roles_id')->where('staffs_id', $staff->id)->first();
          $allStaff->roleName = mylogic::getRoleBy($role->roles_id);
        }

      if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
          return view($this->viewPath . '.index', compact(['page', 'allStaff']));
      }else{
        return back()->withMessage(config('activityMessage.unAuthorized'));
      }
    }

    public function create()
    {
        $page['page_title']        = 'Municipality : Create';  //TODO from language file
        $page['page_description']  = 'Municipality staff details';

        $staffTypeId = mylogic::getDrowDownData($tablename = $this->staffTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        $deginationId = mylogic::getDrowDownData($tablename = $this->deginationTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        $provincesId = mylogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
        $districtId = mylogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');
        $deptId = mylogic::getDrowDownData($tablename = $this->deptTbl, $orderColumn = 'nameEng', $orderby = 'ASC');

        $munExists = myLogic::getMunicipalityDetails();

        $roles = myLogic::getSysRole($roleFor = 'muncipality');

        //check user 
        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
             if (count($munExists) > 0) {   // checking if the municipality exist or not
                return view($this->viewPath . '.create', compact(['page', 'staffTypeId', 'deginationId', 'provincesId', 'districtId', 'roles', 'districtId', 'deptId']));
             }else{
                return back()->withMessage(config('activityMessage.createMunicipilityFirst'));
             }
        }else{
            return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
        }
    }

          public function store(municipalityStaffVal $request)
          {
            $v = Validator::make($request->all(), [
                   'email'    => 'required|unique:users',
                   'roles_id' => 'required',
                   'password' => 'required|min:6|max:30'

               ]);

               if ($v->fails())
               {
                   return redirect()->back()->withErrors($v->errors())->withInput();
               }  //validation for user table
               if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
                        DB::beginTransaction();
                        $refCode = mylogic::sysRefCode($tablename = $this->munStfTbl, $prefix = $this->prefix);
                        try {
                                $save = new StaffMunicipility;
                                $save->municipilities_id  = $this->munId;
                                $save->refCode            = $refCode;

                                if ($request->hasFile('profilePic')) {
                                    $imgFile = $request->file('profilePic');
                                    $filename = str_replace(' ', '', mylogic::getRandNumber()). '.' . $imgFile->getClientOriginalExtension();

                                    $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgProfile, $filename);

                                    $save->profilePic =  $this->imgProfile . $filename;
                                }

                                $save->firstNameNep     = $request->firstNameNep ?? '';
                                $save->firstNameEng     = $request->firstNameEng;
                                $save->departments_id     = $request->departments_id;

                                $save->middleNameNep    = $request->middleNameNep ?? ''; 
                                $save->middleNameEng    = $request->middleNameEng ?? ''; 

                                $save->lastNameNep      = $request->lastNameNep ?? ''; 
                                $save->lastNameEng      = $request->lastNameEng;

                                $save->email            = $request->email ?? 'N/A';
                                $save->citizenNo        = $request->citizenNo;

                                $save->staff_types_id   = $request->staff_types_id;
                                $save->deginations_id   = $request->deginations_id;

                                $save->provinces_id    = $request->provinces_id; 
                                $save->districts_id     = $request->districts_id; 

                                $save->joingDateNep      = $request->joingDateNep ?? 'N/A';
                                $save->joingDateEng      = $request->joingDateEng ?? 'N/A';

                                $save->wardNo           = $request->wardNo ?? 'N/A';

                                $save->villageNep       = $request->villageNep ?? 'N/A'; 
                                $save->villageEng       = $request->villageEng; 

                                $save->dob              = $request->dob ?? 'N/A'; 
                                $save->dobEng              = $request->dobEng ?? 'N/A'; 

                                $save->phoneNumber      = $request->phoneNumber;
                                $save->pounchId         = $request->pounchId;

                                if ($request->hasFile('citizenImagePath')) {
                                 $imgFile = $request->file('citizenImagePath');
                                 $filename = str_replace(' ', '', mylogic::getRandNumber()). '.' . $imgFile->getClientOriginalExtension();

                                 $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgCitizen, $filename);

                                 $save->citizenImagePath =  $this->imgCitizen . $filename;
                               }

                              $mySave = $save->save();
                              

                              $idEncription  = myLogic::idEncription($save->id);

                              // create login 
                              $login = new User;
                              $login->name               = $request->firstNameEng .'&nbsp;'. $request->middleNameEng .'&nbsp;'. $request->lastName;
                              $login->email              = $request->email;
                              $login->password           = Hash::make($request->password);
                              $login->userLevel          = 'mun';
                              $login->municipilities_id  = $this->munId;
                              $login->roles_id           = $request->roles_id;
                              $login->staffs_id          = $save->id;
                              $login->idEncrip           = $idEncription;
                              $roleLogin                 =  $login->save();
                              // login
               
                              //end for login
                              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createAction'), $tableName = $this->munStfTbl, $tblId = $save->id);

                              $save->idEncrip  = $idEncription;
                              $save->save();  // making encription ID

                             if ($mySave && $roleLogin && $log) {
                                 DB::commit();
                                return back()->withMessage(config('activityMessage.saveMessage') .$request->refCode);
                            }else{
                                return back()->withMessage(config('activityMessage.unSaveMessage'));
                            }
                        } catch (Exception $e) {
                             DB::rollback();
                             return back()->withMessage(config('activityMessage.dataNotInserted'));
                        }
                    /*----------------------------------------------------------------*/
              }else{
                  return back()->withMessage(config('activityMessage.accessDenied'));
              }  
      }
      public function show($slug)
      {
           $page['page_title']       = 'Municipality :Show Staff Details';   //TODO change lana
           $page['page_description'] = 'Municipality Staff Details';
           $lang = 0;
           $details = StaffMunicipility::where('idEncrip', $slug)->first();
           $myRole = User::select('roles_id')->where(['staffs_id' => $details->id])->first();
           $details->roles_id = $myRole->roles_id ? mylogic::getRoleBy($myRole->roles_id)['roleName'] : '';

           return view($this->viewPath . '.show', compact(['page', 'details']));
       }


       public function edit($slug)
       {
            $page['page_title']       = 'Municipality :edit Staff Details';   //TODO change lana
            $page['page_description'] = 'Municipality Staff Details';
             
            // $roles = myLogic::getSysRole($roleFor = 'ward');
            $staffTypeId = mylogic::getDrowDownData($tablename = $this->staffTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
            $deginationId = mylogic::getDrowDownData($tablename = $this->deginationTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
            $provincesId = mylogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
            $districtId = mylogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');
           $deptId = mylogic::getDrowDownData($tablename = $this->deptTbl, $orderColumn = 'nameEng', $orderby = 'ASC');


            $data = StaffMunicipility::where('idEncrip', $slug)->first();
            $myRole = User::select('roles_id')->where(['staffs_id' => $data->id])->first();
            $data->roles_id = $myRole->roles_id ? $myRole->roles_id : '';

            $roles = myLogic::getSysRole($roleFor = 'muncipality');

            return view($this->viewPath . '.edit', compact(['page', 'data', 'roles', 'staffTypeId', 'deginationId', 'provincesId', 'districtId', 'deptId']));
        }


        public function update(Request $request, $id)
        {
           $validator = Validator::make($request->all(), [
             'firstNameNep'   => 'required|min:3|max:60',
             'firstNameEng'   => 'required|min:3|max:60',
             'lastNameNep'   => 'required|min:3|max:60',
             'lastNameEng'   => 'required|min:3|max:60',
             'citizenNo'   => 'required|min:3|max:60',
             'staff_types_id'   => 'required',
             'deginations_id'   => 'required',
             'provinces_id'   => 'required',
             'districts_id'   => 'required',
             'joingDateNep'   => 'required',
             'wardNo'   => 'required',
             'villageNep'   => 'required|min:3|max:60', 
             'villageEng'   => 'required|min:3|max:60', 
             'phoneNumber'   => 'required|min:10|max:15',
             'roles_id'  => 'required',
             'provinces_id'   => 'required',
             'districts_id'   => 'required',
            'email'     =>'required|email|unique:staff_municipilities,email,'.$request->id,
            'pounchId'  => 'unique:staff_municipilities,pounchId,'.$request->id,
             ]);

           if ($validator->fails()) {
             return back()->withErrors($validator)->withInput();
           }
           if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
                    DB::beginTransaction();
                    try {
                      /*--------------------------------*/
                      $update = StaffMunicipility::findOrFail($id);
                      $update->firstNameNep  = $request->firstNameNep;
                      $update->firstNameEng  = $request->firstNameEng;
                      $update->middleNameNep  = $request->middleNameNep; 
                      $update->middleNameEng  = $request->middleNameEng; 
                      $update->lastNameNep  = $request->lastNameNep; 
                      $update->lastNameEng  = $request->lastNameEng;
                      $update->email  = $request->email;
                      $update->citizenNo  = $request->citizenNo;
                      $update->staff_types_id  = $request->staff_types_id;
                      $update->deginations_id  = $request->deginations_id;
                      $update->provinces_id  = $request->provinces_id; 
                      $update->districts_id  = $request->districts_id; 
                      $update->joingDateNep  = $request->joingDateNep;
                      $update->wardNo  = $request->wardNo;
                      $update->villageNep  = $request->villageNep; 
                      $update->villageEng  = $request->villageEng; 
                      $update->phoneNumber  = $request->phoneNumber;
                      $update->dob          = $request->dob;
                      $update->dobEng       = $request->dobEng;
                      $update->pounchId     = $request->pounchId;

                      /*---------*/
                      if ($request->hasFile('citizenImagePath')) {  //
                          $imgFile = $request->file('citizenImagePath');
                          $filename = str_replace(' ', '', mylogic::getRandNumber()). '.' . $imgFile->getClientOriginalExtension();
                          $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgCitizen, $filename);
                          $update->citizenImagePath =  $this->imgCitizen.$filename;
                      }

                      if ($request->hasFile('profilePic')) {
                          $imgFile = $request->file('profilePic');
                          $filename = str_replace(' ', '', mylogic::getRandNumber()). '.' . $imgFile->getClientOriginalExtension();

                          $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgProfile, $filename);

                          $update->profilePic =  $this->imgProfile . $filename;
                      }

                      $myUpdate = $update->update();

                      $updUser = User::where(['staffs_id' => $update->id])->first();
                      $updUser->roles_id  = $request->roles_id;
                      $updUser->email     = $request->email;
                      $updUser->name      = $request->firstNameEng .'&nbsp;'. $request->middleNameEng .'&nbsp;'. $request->lastName;

                      $usrUpdate = $updUser->update();

                      $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateMunicipalityStaff'), $tableName = $this->munStfTbl, $tblId = $update->id);


                      if ($usrUpdate && $myUpdate && $log) {
                          DB::commit();
                         return back()->withMessage(config('activityMessage.updateMessage') .$request->refCode);
                     }else{
                         return back()->withMessage(config('activityMessage.notUpdated'));
                     }
                    } catch (Exception $e) {
                             DB::rollback();
                             return back()->withMessage(config('activityMessage.dataNotInserted'));
                        }
                    /*----------------------------------------------------------------*/
              }else{
                  return back()->withMessage(config('activityMessage.accessDenied'));
              }
         if ($myUpdate) {
            return back()->withMessage(config('activityMessage.updateMessage'));
        }else{
            return back()->withMessage(config('activityMessage.notUpdated'));
        }
    }


    public function destroy($id)
    {
      $delId = StaffMunicipility::findOrFail($id);
       $delId->softDelete = 1;
          $myDel = $delId->update();
       if ($myDel) {
           return mylogic::successDeleteMessage($code = 200);
        }else{
             return mylogic::unSuccessDeleteMessage($code = 401);
        }
    }
}