<script type="text/javascript">
       submitMe = $('.submitHouseDetails');
       submitMe.on('submit', function(e){
       e.preventDefault();
       var data = $(this).serialize();
       var url     = $(this).attr('action');
       var msgDisplay = $('.msgDisplay');
       $.ajax({
           'type' : 'POST',
           'url'  : url,
           'data' : data,
           success : function(response){
               console.log(response);
               if (response.success==true) {
                   msgDisplay.toggle().addClass('alert alert-success');
                   msgDisplay.fadeIn().html(response.message);
               }
               if (response.success==false) {
                   $.each(response.message, function(key, value){
                       msgDisplay.append(value).addClass('alert alert-danger');
                   })
               }
               setTimeout(function() {
                 msgDisplay.fadeOut("slow");
               }, 2000);
           }
       })
       .fail(function (response) {
           alert('Oops your data is not save. some of the field is missing');
       });
       });

   $(document).on('dblclick','.rdoClickMe',function(){
      if(this.checked){ // if true
         $(this).prop('checked', !this.checked); // then !this.checked == false
      }
   });

   var btnAddMoreRental = $('.btnAddMoreRental');
   btnAddMoreRental.on('click', function(e){
   
   });

  $(function(){
    $("form[name='frmFamilyMemberDetails']").validate({
      rules:{
        gender:{
          required: true
        },
        fnameEng:{
          required: true,
          minlength:3
        },
        lnameEng:{
          required: true,
          minlength:3
        },
        dobBS:{
          required:true,
          date:true
        },
        maritialStauts:{
          required: true
        },
        jatjatis_id:{
          required: true
        },
        sys_languages_id:{
          required: true
        },
        religiouses_id:{
          required: true
        },
        family_relations_id:{
          required:true
        },
        qualifications_id:{
          required:function(element){
            var dob= $('#myEngDate').val();
            var age= getMyDob(dob);
            if(age > 5){
              return true;                      
            }
          }
        },
        occupations_id:{
          required: function(element){
            var dob= $('#myEngDate').val();
            var age= Math.abs(getMyDob(dob));
            if(age >10){
              return true;                      
            }
          }
        }
      },
      messages: {
        fnameEng: " First name in english is required ",
        lnameEng: " Last name in english is required ",
        gender: " Gender is required ",
        dobBS: "Date of birth is required",
        maritialStauts: "Marital status is required",
        jatjatis_id: "Jat jati is required",
        sys_languages_id: "Mother tounge is required",
        religiouses_id: "Religious  name is required",
        family_relations_id: "Retlation type is required",
        qualifications_id: "Qualification is required",
        occupations_id: "Occupation type is required",
      }
    });
    $("form[name='frmMigrationDetails']").validate({
        rules:{
          haveMigration:{
            required:true
          },
          migrationPlace:{
            required:function(element){
              var hsMigration= $('input[name=haveMigration]');
              return (hsMigration[0].checked == true? true: false);
            }
          },
          migrationDate:{
            required:function(element){
              var hsMigration= $('input[name=haveMigration]');
              return (hsMigration[0].checked == true? true: false);
            },
            date: true
          },
          migrationReason:{
            required:function(element){
              var hsMigration= $('input[name=haveMigration]');
              return (hsMigration[0].checked == true? true: false);
            },
            minlength:3,
            maxlength:255
          }
        },
        messages:{
          haveMigration:"Migration status required",
          migrationPlace:"Place from where you migrate is required",
          migrationDate:"Migration date is required",
          migrationReason:"Migration reason is required"
        }
    });
    $("form[name='frmFamilyFertility']").validate({
      rules:{
        haveGivenBirth:{
          required: true
        },
        
        
      }
      

    });
    $(".birthLocation[]").rules("add",{
        required:function(element){

          var haveGivenBirth= $('input[name=haveGivenBirth]');
          return (haveGivenBirth[0].checked== true? true : false);
        }
    });
        
  });
   var btnAddMember = $('.btnAddMember');
   btnAddMember.on('click', function(e){
   e.preventDefault();
     $('.memberFrm').toggle();
   });

     

//Abrod frm required
function makeRequired(select){
  var name= select.name;
  var index= name.match(/\d/g);
  // alert(index);
  $("form[name='frmFamilyAbroadMember']").validate();
  var yearDuration = document.getElementsByClassName('yearDuration');
  var abraodCountry = document.getElementsByClassName('abroadCountry');

  if(select.checked == true){
    $('input[name*="hsAbroad['+index+'][country]"]').rules("add",{
      required:true,
    });
    $('input[name^="hsAbroad['+index+'][duration]"]').rules("add",{
      required:true,
      number:true
    });
  }else{
    $('input[name^="hsAbroad['+index+'][country]"]').rules("add",{
      required:false,
    });
    $('input[name^="hsAbroad['+index+'][duration]"]').rules("add",{
      required:false,
    });
    $('input[name^="hsAbroad['+index+'][duration]"]').val("");
    $('input[name^="hsAbroad['+index+'][country]"]').prop("selectedIndex",0);

  }
}

function checkMigrationReason(select){
  if(select.value == 1){
    $('.migrationReasonDiv').show();
  }else{
    $('.migrationReasonDiv').hide();
     $(".migrationReason").prop('required',false);
     $(".migrationReason").val('');

  }
}
function checkFertility(select){
  $("form[name='frmFamilyFertility']").validate();
  if(select.value == 1){
    $('.fertilityTable').show();
     $('input[name*=birthYear').each(function(){
      $(this).rules("add",{
        required:true,
        number:true,
        minlength:4
      });
    }); 
     $('input[name*=birthMonth').each(function(){
      $(this).rules("add",{
        required:true,
        number:true,
        minlength:1,
        maxlength:2,
      });
    });    
    $('input[name^=hsFertilityInfo]:radio').each(function(){
      $(this).rules("add",{
        required:true,
      });
      $(this).val("");
    });
  }else{
     $('.fertilityTable').hide();
     $('input[name*=birthYear').each(function(){
      $(this).rules("add",{
        required:false,
      });
      $(this).val("");
    }); 
     $('input[name*=birthMonth').each(function(){
      $(this).rules("add",{
        required:false,
      });
      $(this).val("");
    });    
    $('input[name^=hsFertilityInfo]:radio').each(function(){
      $(this).rules("add",{
        required:false,
      });
      $(this).prop("checked",false);
    });

  }
}

function deathCheck(select){
  var name= select.name;
  var index= name.substr(9,1);
  $("form[name='frmFmDeathMember']").validate();
  if(select.checked == true){
    $('input[name^="deathMem['+index+'][deathReason]"]').rules("add",{
      required:true,
      minlength:3
    });
  }else{
    $('input[name^="deathMem['+index+'][deathReason]"]').rules("add",{
      required:false,
    });
    $('input[name^="deathMem['+index+'][deathReason]"]').val("");
  }
}
$(':radio').change(function (event) {
var id = $(this).data('id');
$('#' + id).removeClass('none').siblings().addClass('none');
});



</script>
