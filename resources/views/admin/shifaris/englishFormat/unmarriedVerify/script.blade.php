<script type="text/javascript">
$(function(){
	/*add buttoin*/
	$("form[name='frmWardunmarriedVerify']").validate({
		rules:{

			refCode : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			issuedDate : {
				required : true,
				date : true
			},
			letterSub : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			appName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			appRelation : {
				required : true
			},
			fatherName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			motherName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			citizenNo : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipality : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			ward : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			district : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			former : {
				required : true
			},
			orgAddr : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			orgType : {
				required : true
			},
			orgWard : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			appTitleOpt : {
				required : true
			},
			appGenderOpt : {
				required : true
			},
			dateInAD : {
				required : true,
				date : true
			},
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {

			refCode : "Ref Code is required",
			issuedDate : "Issue Date is required",
			letterSub : "Letter Subject is required",
			appName : "Applicant Name is required",
			appRelation : "Applicant Relation is required",
			fatherName : "Father Name is required",
			motherName : "Mother Name is required",
			citizenNo : "Citizen No is required",
			municipality : "Municipality is required",
			ward : "Ward is required",
			district : "District is required",
			former : "Former is required",
			orgAddr : "Org Address is required",
			orgType : "Org Type is required",
			orgWard : "Org Ward is required",
			appTitleOpt : "Applicant  Title Option is required",
			appGenderOpt : "Applicant Gender Option is required",
			dateInAD : "Date In AD is required",
			authorizedPerson : "Authorized Person is required"
		}
	});
});
$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                  $("input[name='appName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
            
          },complete:function(){
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>