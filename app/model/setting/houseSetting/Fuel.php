<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class Fuel extends Model
{
	protected $table = 'fuels';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
