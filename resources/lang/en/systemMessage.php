<?php
   return [
        'crudMessage' => [
          'createMessage' => 'Create Message',
          'updateMessage' => 'Update Message',
          'deleteMessage' => 'Delete Message'
        ]
   ];
?>