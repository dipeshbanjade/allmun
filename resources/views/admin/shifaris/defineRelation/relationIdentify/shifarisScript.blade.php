<script type="text/javascript">

	var addButton = $('#btnAddTableRow');
	var clickCount = 1;

	addButton.on('click', function(e){
		e.preventDefault();
		clickCount++;

		var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + clickCount + '">';

		var relationPerson = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="relationPerson[]">';

		var relation = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="relation[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+serialNumber+"</td>"
		+"<td>"+relationPerson+"</td>"
		+"<td>"+relation+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#business_close_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmRelationIdentify']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 10
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number: true
				},
				name : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				wardNumber : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				municipality : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				ward : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				providedDate : {
					required : true,
					date: true
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
			},
			messages: {
				refCode : "Reference Code is required",
				issuedDate : "Issue Date is required",
				chalaniNum : "Chalani Number must be number",
				name : "Applicant Name must be number",
				municipalityName : "Municipality is required",
				wardNumber : "Ward Number must be number",
				municipality : "Municipality is required",
				ward : "Ward Number must be number",
				providedDate : "Date is required",
				authorizedPerson : "Authorized Person is required"
			}
		});
	});
</script>