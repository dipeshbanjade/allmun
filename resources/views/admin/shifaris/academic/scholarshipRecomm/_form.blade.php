<div class="row top-part">
  <div class="col-md-6 ps-cn">
    <p><b class="ps">प. सं.:</b>
      @if(isset($data->refCode))
      {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
      @else
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
      @endif
    </p>
    <p align="left" class="star">
      <b class="cn">च. नं.:</b>
      {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
    </p>
  </div>
  <div class="col-md-6">
    <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center"><h4 align="center"><b class="sub">विषय: छात्रवृत्ति सिफारिस।</b></h4></p>
    {{ Form::hidden('letter_subject', 'छात्रवृत्ति सिफारिस') }}
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left"><b>जो सँग सम्बन्धित छ।</b></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">
    <p>उपरोक्त विषयमा  <b> 
      {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </b> 
      वडा नं. 
      {{ Form::text('wardNum', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
      (साविक
      <!--  -->
      {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
      <!--  -->
      <b> 
        <select onchange="changeSelect(this)" name="organizationType" onchange="changeSelect(this)">
          <option value="गा.वि.स." 
          {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
          <option value="नगरपालिका" 
          {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
          <option value="उप महानगरपालिका" 
          {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
          <option value="महानगरपालिका" 
          {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
        </select>
      </b>, वडा नं. 
      {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
      )
      अन्तर्गत 
      <b><select onchange="changeSelect(this)" name="addressType">
        <option value="स्थायी" 
        {{ !empty($data->addressType) && $data->addressType == 'स्थायी' ? 'selected' : '' }}>स्थायी</option>
        <option value="अस्थायी"
        {{ !empty($data->addressType) && $data->addressType == 'अस्थायी' ? 'selected' : '' }}>अस्थायी</option>
      </select>
    </b>
    बसोबास गर्ने श्री 
    {{ Form::text('fatherName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><span class="star"></span> तथा श्रीमती 
    {{ Form::text('motherName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
    <span class="star"></span> को आर्थिक अवस्था 
    <b><select onchange="changeSelect(this)" name="economyStatus">
      <option value="कमजोर" 
      {{ !empty($data->economyStatus) && $data->economyStatus == 'कमजोर' ? 'selected' : '' }}>कमजोर</option>
      <option value="न्युन" 
      {{ !empty($data->economyStatus) && $data->economyStatus == 'न्युन' ? 'selected' : '' }}>न्युन</option>
    </select>
  </b>
  भएको हुँदा निजहरूको देहाय बमोजिमका 
  <b><select onchange="changeSelect(this)" name="childrenGender">
    <option value="छोरा"
      {{ !empty($data->childrenGender) && $data->childrenGender == 'छोरा' ? 'selected' : '' }}>छोरा</option>
      <option value="छोरी"
        {{ !empty($data->childrenGender) && $data->childrenGender == 'छोरी' ? 'selected' : '' }}>छोरी</option>
      </select>
    </b>
    <b><select onchange="changeSelect(this)" name="childrenTitle">
      <option value="श्री"
      {{ !empty($data->childrenTitle) && $data->childrenTitle == 'श्री' ? 'selected' : '' }}>श्री</option>
      <option value="सुश्री"
      {{ !empty($data->childrenTitle) && $data->childrenTitle == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
      <option value="श्रीमती"
      {{ !empty($data->childrenTitle) && $data->childrenTitle == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
    </select>
  </b>
  {{ Form::text('nameOfChildren', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}

लाई नियमानुसार छात्रवृत्तिको लागि सिफारिस गरिन्छ।</p>  
<div class="col-md-12">
  <div class="table-responsive">
    <table class="table table-bordered" id="scholarship_table">
      <thead>
        <tr><th>क्र.स.</th>  
          <th>नाम</th>
          <th>नाता</th>
          <th>घर नं</th>
        </tr></thead>
        <tbody>
          <?php 
          $count = 1;
          $tableData = !empty($data) ? json_decode($data->tableData, true) : [1];
          //dd($tableData);
          foreach($tableData as $singleData){         
            ?>
            <tr>
              <td>
                {{ $count++ }}
              </td>
              <td>
                {{ Form::text('name[]', !empty($singleData['name']) ? $singleData['name'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
              </td>
              <td>
                {{ Form::text('nata[]',  !empty($singleData['nata']) ? $singleData['nata'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
              </td>
              <td>
                {{ Form::text('gharNo[]',  !empty($singleData['gharNo']) ? $singleData['gharNo'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
              </td>
              @if($count == 2)
              <td class="add-btns" style="border: 0px;">
                <button type="button" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button>
              </td>
              @else
              <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
              @endif
              <!-- <td><a href="" id="btnAddTableRow" class="btn btn-success"><span class="fa fa-plus"></span></a></td> -->
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
  </div>
</div>
<div class="text-right btm-last">
  <p>
    {{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
  </p>
  <p> <b>
    <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
      @if(count($deginationsId) > 0)
      @foreach($deginationsId as $deg)
      <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
        {{ $deg->nameNep }} 
      </option>
      @endforeach
      @endif
    </select>
  </b></p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
{{ Form::text('idEncript', null, ['class'=>'onScan form-control', 'placeholder'=>'Please scan your card']) }}
<!-- END -->
</div>      
<div class="col-md-12">
  <p><i><span class="star"></span> * लागेको फिल्डहरु अनिवार्य रुपमा भर्नुपर्ने हुन्छ।</i> </p>
  <hr>
</div>