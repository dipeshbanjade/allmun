<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class QualityWater extends Model
{
    protected $table = 'quality_waters';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
