@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
	<div class="card-box">
		<div class="card-body ">
			<div class="card-head">
				<header><i class="fa fa-eye"></i>&nbsp;@lang('commonField.extra.familyType')</header>
			</div>
			<button id="" class="btnAddfamilyType btn btn-primary pull-right">
				<i class="fa fa-plus"></i>
				<span> @lang('commonField.extra.familyType')</span>
			</button>
			<div class="table-scrollable" style="color:black;">
				<table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
					<thead>
						<tr>
							<th>@lang('commonField.extra.sn')</th>
							<th>@lang('commonField.personal_information.name') </th>
							<th> @lang('commonField.extra.action') </th>
						</tr>
					</thead>
					<tbody>
						<tr class="odd gradeX">
							@php  $count = 1  @endphp
							@if(count($familyType) > 0)
							@foreach($familyType as $family)
							<td>{{ $count ++ }}</td>
							<td>{{ $family->nameNep ?? 'N/A'}} {{ $family->nameEng }}</td>
							
							<td>
								<a href="" class="btnEditFamilyType btn btn-primary btn-xs" id="" data-url="{{ route('familyType.edit',$family->id)}}">
									<i class="fa fa-pencil"></i>
								</a>

								<a href="#" class="deleteMe" data-url="{{ route('familyType.delete',$family->id)}}" data-name="{{ $family->nameEng }}">
								<button class="btn btn-danger btn-xs">
									<i class="fa fa-trash-o "></i></button>
									</a>
								</tr>
								@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		@endsection
		@section('modelSection')
		<div class="modal" id="familyTypeModel">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.familyType')</i></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						{{ Form::open(['route' => 'familyType.store', 'name'=>'frmfamilyType']) }}
						@include('admin.setting.familyType._form')
						<div class="col-lg-12 p-t-20 text-right"> 
							{{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}
							<button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.back')</button>
						</div>
						{{ Form::close() }}
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">

					</div>

				</div>
			</div>
		</div>
		<!-- Edit familyType Model -->
		<div class="modal" id="editFamilyTypeModel">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.familyType')</i></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
						 {{ Form::open(['name'=>'frmeditfamilyType', 'id' => 'frmUpdateFamilyType']) }}
						     @include('admin.setting.familyType._form')
						     {{ Form::hidden('familyTypeId', null, ['class' => 'familyTypeId']) }}
							<div class="col-lg-12 p-t-20 text-right"> 
								{{ Form::submit('CREATE', ['class' => 'btn btn-success']) }}
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
					  {{ Form::close() }}
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">

					</div>

				</div>
			</div>
		</div>
@endsection
@section('custom_script')
@include('admin.setting.familyType.script')
@endsection