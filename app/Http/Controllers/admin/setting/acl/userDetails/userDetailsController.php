<?php

namespace App\Http\Controllers\admin\setting\acl\userDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\model\mylogic;
use Illuminate\Support\Facades\Hash;
use App\model\admin\municipility\StaffMunicipility;
use App\model\admin\ward\WardStaff;
use App\Http\Requests\admin\setting\acl\userDetailsVal;
use DB;
use Auth;

class userDetailsController extends Controller
{
    protected $viewPath = 'admin.acl.user';
    protected $roleTbl  = 'roles';
    protected $wardsTbl = 'wards';

    protected $wardStaff = 'ward_staffs';            // 
    protected $munStaff  = 'staff_municipilities';   // 

    protected $usrTbl          = 'users';
    protected $staffTypeTbl    = 'staff_types';
    protected $degTbl          = 'deginations';
    protected $provTbl         = 'provinces';
    protected $disTbl          = 'districts';
    protected $deptTbl        = 'departments';

    protected $prefix;
    

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.staffPrefix');

    }

    public function index()
    {
        $page['page_title']       = ': User List';
        $page['page_description'] = ' :List of all user';

        $allUsr = User::orderby('created_at', 'DESC')->where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
              

        return view($this->viewPath . '.index', compact(['page', 'allUsr']));
    }

    public function create()
    {
        $page['page_title']       = ': Create  User';
        $page['page_description'] = ' :create new users';

        $refCode  = mylogic::sysRefCode($tablename = $this->usrTbl, $prefix = $this->prefix);
        $roleId   =  myLogic::getDrowDownData($tablename = $this->roleTbl, $orderColumn = 'roleName', $orderby = 'ASC');
        $wards    = myLogic::getDrowDownData($tablename = $this->wardsTbl, $orderColumn = 'nameEng', $orderby = 'ASC');

        $staffType  = myLogic::getDrowDownData($tablename = $this->staffTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        $deg        = myLogic::getDrowDownData($tablename = $this->degTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
        $prov       = myLogic::getDrowDownData($tablename = $this->provTbl, $orderColumn = 'name', $orderby = 'ASC');
        $dis        = myLogic::getDrowDownData($tablename = $this->disTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');
        $dept       = myLogic::getDrowDownData($tablename = $this->deptTbl, $orderColumn = 'nameEng', $orderby = 'ASC');

        $munExists = myLogic::getMunicipalityDetails();

        if (!count($wards) > 0 && !count($munExists) > 0 ) {
            return back()->withMessage('please create ward and municipality first');
        }
        return view($this->viewPath . '.create', compact(['page', 'roleId', 'wards', 'staffType', 'deg', 'prov', 'dis', 'dept', 'refCode']));
    }
    public function store(userDetailsVal $request)
    {
      if (Auth::user()->userLevel === 'dev') {
            DB::beginTransaction();
            try {
                $userType = $request->selectRoleFor;
                $munId    = myLogic::getMunicipalityDetails()['id'];

                $ms  = $userType == 'mun' ? new StaffMunicipility : new WardStaff;
                $ms->municipilities_id  = $munId;

                if ($userType == 'wrd') {
                    $ms->wards_id       = $request->wards_id;
                }
                $ms->refCode            = $request->refCode;
                $ms->citizenNo          = $request->citizenNo;
                $ms->staff_types_id     = $request->staff_types_id;
                $ms->deginations_id     = $request->deginations_id;
                $ms->firstNameEng       = $request->name;
                $ms->firstNameNep       = $request->name;
                $ms->provinces_id       = $request->provinces_id;
                $ms->districts_id       = $request->districts_id;
                $ms->email              = $request->email;
                $ms->departments_id     = $request->departments_id;
                $ms->phoneNumber        = $request->phoneNumber;

                $ms->departments_id     = $request->departments_id;
                $ms->departments_id     = $request->departments_id;

                $ms->villageNep       = 'N/A';
                $ms->villageEng        = 'N/A';

                $ms->lastNameNep        = 'N/A';
                $ms->lastNameEng        = 'N/A';
                $msSave = $ms->save();

                /*user table*/
                 $usr = new User;
                 $usr->name      = $request->name;
                 $usr->email     = $request->email;
                 $usr->password  = Hash::make($request->password);
                 $usr->userLevel = $userType;
                 $usr->municipilities_id = $munId;

                 if ($userType == 'wrd') {
                    $usr ->wards_id = $request->wards_id; 
                 }
                 $usr->roles_id  = $request->roles_id;
                 $usr->staffs_id  = $ms->id;
                 $usr->isAdmin    = 1;
                 $usr->isExists    = 1;

                 

                 $usrSave = $usr->save();

                if ($msSave && $usrSave) {
                    DB::commit();
                    return redirect()->route('system.userDetails.index')->withMessage('user successfully create');
                }else{
                    return back()->withmessage('Oops try it again');
                }
            }catch (Exception $e) {
                 DB::rollback();
                 return back()->withMessage(config('activityMessage.dataNotInserted'));
            }
       }else{
              return back()->withMessage(config('activityMessage.accessDenied'));
           }
     }
    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
