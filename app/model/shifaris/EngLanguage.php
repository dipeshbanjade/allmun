<?php

namespace App\model\shifaris;

use Illuminate\Database\Eloquent\Model;

class EngLanguage extends Model
{
    protected $table = 'eng_languages';
}
