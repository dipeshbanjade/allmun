<?php

namespace App\Http\Controllers\admin\houseDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\admin\houseDetails\healthCleaness\HsFamilyHealthPosition;
use App\model\admin\houseDetails\healthCleaness\hsFamilyFemaleDisease;
use App\model\admin\houseDetails\healthCleaness\hsInfantVaccine;
use App\model\admin\houseDetails\healthCleaness\HsChildHealthCare;
use App\model\admin\houseDetails\healthCleaness\HsPregnancyAction;
use App\model\admin\houseDetails\healthCleaness\HsPregnancyHealthService;
use App\model\admin\houseDetails\healthCleaness\HsFmHIVPregnancy;
use App\model\admin\houseDetails\healthCleaness\HsFmUsingDrinkingWater;
use App\model\admin\houseDetails\healthCleaness\HsHandWashProcess;

use App\model\admin\citizen\CitizenInfo;

use App\model\mylogic;
use Auth;
use DB;
use Validator;

class healthAndCleanessController extends Controller
{
  public function __construct()
  {
   $this->middleware('auth');
  }
public function insertFemaleDisease(Request $request){  // female disease record
  $femaleDisease = $request->hsFemaleDisease; // cacheing array data
  $fmFemaleDisArr = []; 
  DB::beginTransaction(); 
  foreach ($femaleDisease as $fmDisease) {
      if (isset($fmDisease['femaleId'])) {   // updating disease name in citizen table 
       $updateCitizenDeath = CitizenInfo::findOrfail($fmDisease['femaleId']);
       $updateCitizenDeath->diseaseName = isset($fmDisease['diseaseName']) ? json_encode($fmDisease['diseaseName']) : '';
       $updateCitizenDeath->update();
   }

   if (isset($fmDisease['haveDisease']) && $fmDisease['haveDisease'] == 1) {
       $fmFemaleDisArr[] = $fmDisease;
   }
}
$fmDiseaseDetails = json_encode($fmFemaleDisArr);

if ($request->myHouseId && $request->fm_member_id) {
 $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

 $isExistsHsNum = hsFamilyFemaleDisease::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
 if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
  try {
             $femaleDisease  = !$isExistsHsNum['house_details_id'] ? new hsFamilyFemaleDisease : hsFamilyFemaleDisease::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $femaleDisease->createdBy         = Auth::user()->id;
             $femaleDisease->municipilities_id = $ishouseExists['municipilities_id'];
             $femaleDisease->wards_id          = $ishouseExists['wards_id'];
             $femaleDisease->house_details_id  = $ishouseExists['id'];

             $femaleDisease->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $femaleDisease->hsDiseaseDetails     = $fmDiseaseDetails;
             $femaleDisease->status            = 0;
             $femaleDisease->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $femaleDisease->save() : $femaleDisease->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_family_female_diseases', $tblId = $femaleDisease->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           }else{
                return response()->json([
                 'success' => false,
                 'message' => config('activityMessage.unSaveMessage')
                 ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
}else{ 
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{ 
   return back()->withMessage('House number is missing please try to insert it from browser');
}
}

public function insertChildrenHealthCare(Request $request){  
    DB::beginTransaction();
    $hsChildHealthCare = json_encode($request->hsChildHealthCare);

    if ($request->myHouseId && $request->fm_member_id) {
     $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

     $isExistsHsNum = HsChildHealthCare::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
     if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
      try {
       $chc  = !$isExistsHsNum['house_details_id'] ? new HsChildHealthCare : HsChildHealthCare::findOrfail($isExistsHsNum['id']);

       $chc->createdBy         = Auth::user()->id;
       $chc->municipilities_id = $ishouseExists['municipilities_id'];
       $chc->wards_id          = $ishouseExists['wards_id'];
       $chc->house_details_id  = $ishouseExists['id'];

       $chc->citizen_info_id  = $ishouseExists['citizen_infos_id'];

       $chc->hsChildHealthCare  = $hsChildHealthCare;
       $chc->status            = 0;
       $chc->softDelete        = 0;
       $mySave = !$isExistsHsNum['house_details_id'] ? $chc->save() : $chc->update();

       $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_child_health_cares', $tblId = $chc->id);
       if ($mySave && $log) {
        DB::commit();
        return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
        // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
    }else{
        return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
       // return back()->withMessage(config('activityMessage.unSaveMessage'));
   }
} catch (Exception $e) {
 DB::rollback();
 return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}else{   
   return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{    
 return back()->withMessage('House number is missing please try to insert it from browser');
}
}

//
public function insertFamilyHealthPosition(Request $request){  
  DB::beginTransaction();
  $hsLTDiseaseDetail = json_encode($request->hsLTDiseaseDetail);

  if ($request->myHouseId && $request->fm_member_id) {
   $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

   $isExistsHsNum = HsFamilyHealthPosition::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
   if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
    try {
     $fhp  = !$isExistsHsNum['house_details_id'] ? new HsFamilyHealthPosition : HsFamilyHealthPosition::findOrfail($isExistsHsNum['id']);

     $fhp->createdBy         = Auth::user()->id;
     $fhp->municipilities_id = $ishouseExists['municipilities_id'];
     $fhp->wards_id          = $ishouseExists['wards_id'];
     $fhp->house_details_id  = $ishouseExists['id'];

     $fhp->treatmentArea = $request->treatmentArea;
     $fhp->otherTreatmentArea = $request->otherTreatmentArea;
     $fhp->timeToHealthCenter = $request->timeToHealthCenter;
     $fhp->timeToHospital = $request->timeToHospital;
     $fhp->haveLTDisease = $request->haveLTDisease;
     $fhp->hsLTDiseaseDetail = $hsLTDiseaseDetail;

     $fhp->status            = 0;
     $fhp->softDelete        = 0;
     $mySave = !$isExistsHsNum['house_details_id'] ? $fhp->save() : $fhp->update();

     $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_family_health_positions', $tblId = $fhp->id);
     if ($mySave && $log) {
      DB::commit();
      return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
      // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
  }else{
      return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
     // return back()->withMessage(config('activityMessage.unSaveMessage'));
 }
} catch (Exception $e) {
   DB::rollback();
   return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}else{   
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{    
 return back()->withMessage('House number is missing please try to insert it from browser');
}
}

public function insertHIVPregnancy(Request $request){  
  $hivPreg = $request->hsHIVPregDetails; // cacheing array data
  $hivPregArr = []; 
  DB::beginTransaction(); 
  foreach ($hivPreg as $hivPregEach) {
      // if (isset($hivPreg['femaleId'])) { 
      //    $updateCitizenDeath = CitizenInfo::findOrfail($fmDisease['femaleId']);
      //    $updateCitizenDeath->diseaseName = isset($fmDisease['diseaseName']) ? json_encode($fmDisease['diseaseName']) : '';
      //    $updateCitizenDeath->update();
      // }

    if (isset($hivPregEach['childHaveHIV']) && $hivPregEach['childHaveHIV'] == 1) {
     $hivPregArr[] = $hivPregEach;
 }
}
$hsHIVPregDetails = json_encode($hivPregArr);
if ($request->myHouseId && $request->fm_member_id) {
   $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);

   $isExistsHsNum = HsFmHIVPregnancy::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();
   if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
    try {
     $hivPreg  = !$isExistsHsNum['house_details_id'] ? new HsFmHIVPregnancy : HsFmHIVPregnancy::findOrfail($isExistsHsNum['id']);

     $hivPreg->createdBy         = Auth::user()->id;
     $hivPreg->municipilities_id = $ishouseExists['municipilities_id'];
     $hivPreg->wards_id          = $ishouseExists['wards_id'];
     $hivPreg->house_details_id  = $ishouseExists['id'];
     $hivPreg->citizen_infos_id  = $ishouseExists['citizen_infos_id'];

     $hivPreg->haveHIV = $request->haveHIV;
     $hivPreg->hsHIVPregDetails = $hsHIVPregDetails;
     $hivPreg->status            = 0;
     $hivPreg->softDelete        = 0;
     $mySave = !$isExistsHsNum['house_details_id'] ? $hivPreg->save() : $hivPreg->update();

     $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_fm_h_i_v_pregnancies', $tblId = $hivPreg->id);
     if ($mySave && $log) {
      DB::commit();
      return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
      // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
  }else{
      return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
     // return back()->withMessage(config('activityMessage.unSaveMessage'));
 }
} catch (Exception $e) {
   DB::rollback();
   return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}else{   
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{    
 return back()->withMessage('House number is missing please try to insert it from browser');
}
}


public function insertInfantVaccine(Request $request){
  $infantVaccine = $request->infantVac; // cacheing array data
  $infantVaccineArr = []; 
  DB::beginTransaction(); 
  foreach ($infantVaccine as $infVac) {
      // if (isset($infantVaccine['femaleId'])) { 
      //    $updateCitizenDeath = CitizenInfo::findOrfail($fmDisease['femaleId']);
      //    $updateCitizenDeath->diseaseName = isset($fmDisease['diseaseName']) ? json_encode($fmDisease['diseaseName']) : '';
      //    $updateCitizenDeath->update();
      // }

   if (isset($infVac['times']) && isset($infVac['haveVaccine']) && $infVac['haveVaccine']==1 && isset($infVac['times']) &&  $infVac['times'] != null) {
     $infantVaccineArr[] = $infVac;
 }
}
$infantVaccDetails = json_encode($infantVaccineArr);

if ($request->myHouseId && $request->fm_member_id) {
   $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
   $isExistsHsNum = hsInfantVaccine::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

   if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
    try {
             $infantVacRep  = !$isExistsHsNum['house_details_id'] ? new hsInfantVaccine : hsInfantVaccine::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $infantVacRep->createdBy         = Auth::user()->id;
             $infantVacRep->municipilities_id = $ishouseExists['municipilities_id'];
             $infantVacRep->wards_id          = $ishouseExists['wards_id'];
             $infantVacRep->house_details_id  = $ishouseExists['id'];

             $infantVacRep->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $infantVacRep->hsInfantVacDetails = $infantVaccDetails;
             $infantVacRep->status            = 0;
             $infantVacRep->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $infantVacRep->save() : $infantVacRep->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_infant_vaccines', $tblId = $infantVacRep->id);
             if ($mySave && $log) {
               DB::commit();
                return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           }else{
             return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
}else{ 
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{ 
   return back()->withMessage('House number is missing please try to insert it from browser');
}
}


public function insertPregnancyAction(Request $request){
    $pregActionArr = $request->hsPregActionDetails; // cacheing array data
    $hsPregActionDetailsArr = []; 

    DB::beginTransaction();   

    foreach ($pregActionArr as $pregAction) {    
     if (isset($pregAction['isPregWithinFive']) && $pregAction['isPregWithinFive'] == 1 ) {
       $hsPregActionDetailsArr[] = $pregAction;
   }
}
$hsPregActionDetails = json_encode($hsPregActionDetailsArr);   

if ($request->myHouseId && $request->fm_member_id) {

    $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
    $isExistsHsNum = HsPregnancyAction::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

    if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
      try {
             $pregAction  = !$isExistsHsNum['house_details_id'] ? new HsPregnancyAction : HsPregnancyAction::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $pregAction->createdBy         = Auth::user()->id;
             $pregAction->municipilities_id = $ishouseExists['municipilities_id'];
             $pregAction->wards_id          = $ishouseExists['wards_id'];
             $pregAction->house_details_id  = $ishouseExists['id'];

             $pregAction->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $pregAction->hsPregActionDetails = $hsPregActionDetails;

             $pregAction->status            = 0;
             $pregAction->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $pregAction->save() : $pregAction->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_pregnancy_actions', $tblId = $pregAction->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
}else{ 
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{ 
   return back()->withMessage('House number is missing please try to insert it from browser');
}
}

public function insertPregnancyHealthService(Request $request){
    DB::beginTransaction();   

    $hsPregHealthServiceDetails = json_encode($request->hsPregHealthServiceDetails);   

    if ($request->myHouseId && $request->fm_member_id) {

      $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
      $isExistsHsNum = HsPregnancyHealthService::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

      if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
        try {
             $pregHealthService  = !$isExistsHsNum['house_details_id'] ? new HsPregnancyHealthService : HsPregnancyHealthService::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $pregHealthService->createdBy         = Auth::user()->id;
             $pregHealthService->municipilities_id = $ishouseExists['municipilities_id'];
             $pregHealthService->wards_id          = $ishouseExists['wards_id'];
             $pregHealthService->house_details_id  = $ishouseExists['id'];

             $pregHealthService->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $pregHealthService->hsPregHealthServiceDetails = $hsPregHealthServiceDetails;

             $pregHealthService->status            = 0;
             $pregHealthService->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $pregHealthService->save() : $pregHealthService->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_pregnancy_health_services', $tblId = $pregHealthService->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
}else{ 
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{ 
   return back()->withMessage('House number is missing please try to insert it from browser');
}
}

public function insertFmUsingDrinkingWater(Request $request){
    $fmDrinkingWaterArr = $request->hsFmUsDrinkWaterDetails; // cacheing array data
    $fmSelectedDrinkingWDetailsArr = [];
    DB::beginTransaction();   
    foreach ($fmDrinkingWaterArr as $singleWaterSource) {    
     if (isset($singleWaterSource['sourceUsed']) && $singleWaterSource['sourceUsed'] == 1 ) {
       $fmSelectedDrinkingWDetailsArr[] = $singleWaterSource;
   }
}
$hsFmUsDrinkWaterDetails = json_encode($fmSelectedDrinkingWDetailsArr);   

if ($request->myHouseId && $request->fm_member_id) {

    $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
    $isExistsHsNum = HsFmUsingDrinkingWater::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

    if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
      try {
             $fmUseDrinkingWater  = !$isExistsHsNum['house_details_id'] ? new HsFmUsingDrinkingWater : HsFmUsingDrinkingWater::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $fmUseDrinkingWater->createdBy         = Auth::user()->id;
             $fmUseDrinkingWater->municipilities_id = $ishouseExists['municipilities_id'];
             $fmUseDrinkingWater->wards_id          = $ishouseExists['wards_id'];
             $fmUseDrinkingWater->house_details_id  = $ishouseExists['id'];

             $fmUseDrinkingWater->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $fmUseDrinkingWater->hsFmUsDrinkWaterDetails = $hsFmUsDrinkWaterDetails;

             $fmUseDrinkingWater->status            = 0;
             $fmUseDrinkingWater->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $fmUseDrinkingWater->save() : $fmUseDrinkingWater->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_fm_using_drinking_waters', $tblId = $fmUseDrinkingWater->id);
             if ($mySave && $log) {
               DB::commit();
               return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
}else{ 
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{ 
   return back()->withMessage('House number is missing please try to insert it from browser');
}
}


public function insertHandWashProcess(Request $request){
    $handWashProcessArr = $request->hsHandWashProcessDetails; // cacheing array data
    $handWashProcessSelectedArray = [];
    DB::beginTransaction();   
    foreach ($handWashProcessArr as $handWashProcess) {    
     if (isset($handWashProcess['handWashProcessUsed']) && $handWashProcess['handWashProcessUsed'] == 1 ) {
       $handWashProcessSelectedArray[] = $handWashProcess;
   }
}
$hsHandWashProcessDetails = json_encode($handWashProcessSelectedArray);
if ($request->myHouseId && $request->fm_member_id) {

    $ishouseExists = myLogic::getIndividualHouseDetails($request->myHouseId);
    $isExistsHsNum = HsHandWashProcess::select('house_details_id', 'id')->where(['house_details_id'=>$request->myHouseId])->first();

    if (count($ishouseExists) > 0 && $ishouseExists['hsNum'] === $request->houseNum) {
      try {
             $handWashProcess  = !$isExistsHsNum['house_details_id'] ? new HsHandWashProcess : HsHandWashProcess::findOrfail($isExistsHsNum['id']);   // insert only who get female disease

             $handWashProcess->createdBy         = Auth::user()->id;
             $handWashProcess->municipilities_id = $ishouseExists['municipilities_id'];
             $handWashProcess->wards_id          = $ishouseExists['wards_id'];
             $handWashProcess->house_details_id  = $ishouseExists['id'];

             $handWashProcess->citizen_infos_id  = $ishouseExists['citizen_infos_id'];  // ghar muli ID

             $handWashProcess->hsHandWashProcessDetails = $hsHandWashProcessDetails;

             $handWashProcess->status            = 0;
             $handWashProcess->softDelete        = 0;
             $mySave = !$isExistsHsNum['house_details_id'] ? $handWashProcess->save() : $handWashProcess->update();

             $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails'), $tableName = 'hs_hand_wash_processes', $tblId = $handWashProcess->id);
             if ($mySave && $log) {
               DB::commit();
                return response()->json([
                'success' => true,
                'message' => config('activityMessage.saveMessage')
                ]);
               // return redirect()->back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum'])->with(['mainTab'=>'HealthNCleaness', 'helpTab' => 'femaleDisease']);
               // return back()->withMessage(config('activityMessage.saveMessage'). $ishouseExists['hsNum']);
           }else{
             return response()->json([
                'success' => false,
                'message' => config('activityMessage.unSaveMessage')
                ]);
              // return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
      } catch (Exception $e) {
       DB::rollback();
       return back()->withMessage(config('activityMessage.dataNotInserted'));
   }
}else{ 
 return back()->withMessage(config('activityMessage.idNotFound'));
}
}else{ 
   return back()->withMessage('House number is missing please try to insert it from browser');
}
}


}
