<script type="text/javascript">
$(function(){

	$("form[name='frmeditchuloType']").validate({
		rules:{
			nameEng : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {
			nameEng : "The name of Chulo Type is required"
		}
	});
});
$(function(){

	$("form[name='frmchuloType']").validate({
		rules:{

			nameEng : {
				required : true,
				minlength : 3,
				maxlength : 30
			}
		},
		messages: {

			nameEng : "The name of Chulo Type is required"
		}
	});
	  var btnAddChuloType = $('.btnAddChuloType');
	  btnAddChuloType.on('click', function(e){
	    $('#chuloTypeModel').modal('show'); 
	});

	  var btnEditchuloType = $('.btnEditchuloType');
	  btnEditchuloType.on('click', function(e) {
	  
	      e.preventDefault();
	      var url = $(this).data('url');
	      $.ajax({
	       'type': 'GET',
	       'url': url,
	       success: function (response) {
	          $('.chuloTypeId').val(response.id);
	          $('.txtRefCode').val(response.refCode).attr('disabled',true);
	          $('.txtNameNep').val(response.nameNep);
	          $('.txtNameEng').val(response.nameEng);
	          $("#editchuloTypeModel").modal('show');
	      }
	  })
	  });

	  var frmUpdatechuloType = $('#frmUpdatechuloType');
	  frmUpdatechuloType.on('submit', function(e){
	      e.preventDefault();
	      var data = $(this).serialize();
	      var id   = $('.chuloTypeId').val();
	      var url  = "{{URL::to('/')}}" + "/setting/chuloType/"+id+"/update";
	      $.ajax({
	          'type' : 'POST',
	          'url'  : url,
	          'data'    : data,
	          success : function(response){
	              console.log(response);
	              if (response.success==true) {
	                  $('.infoDiv').append(response.message).addClass('alert alert-success');
	              }
	              if (response.success==false) {
	                  $.each(response.message, function(key, value){
	                      $('.infoDiv').append(value).addClass('alert alert-danger');
	                  })
	              }
	          },complete:function(){
	              $("#editchuloTypeModel").modal('hide');
	              setTimeout(location.reload.bind(location), 1500);
	          }
	      })
	      .fail(function (response) {
	          alert('data already exists');
	      });
	      $('#editchuloTypeModel').modal('hide');
	  });
	  });

</script>