<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditvaccineType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of vaccine type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmvaccineType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of vaccine type is required"
            }
        });
	var btnAddvaccineType = $('.btnAddvaccineType');
	btnAddvaccineType.on('click', function(e){
		$('#vaccineTypeModel').modal('show'); 
	});

	var btnEditvaccineType = $('.btnEditvaccineType');
	btnEditvaccineType.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
                
				$('.vaccineTypeId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
                $('.txtNameEng').val(response.nameEng);
				$('.txttag').val(response.tag).change();
				$("#editvaccineTypeModel").modal('show');
			}
		})
	});

	var frmUpdatevaccineType = $('#frmUpdatevaccineType');
  frmUpdatevaccineType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.vaccineTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/vaccineType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editvaccineTypeModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editvaccineTypeModel').modal('hide');
});
});
</script>