<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardbusinessAccounting']").validate({
			rules:{

				fiscalYear : {
					required : true					
				},
				address : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				companyName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDate : {
					required : true,
					date : true
				}
			},
			messages: {
				fiscalYear: "Fiscal year is required",
				address : "Address is required",
				companyName: "Company Name is required",
				issuedDate: " is required"
			}
		});
	});
</script>