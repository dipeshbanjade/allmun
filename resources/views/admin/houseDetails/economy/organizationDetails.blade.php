<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row color_red">

            <!-- 13 -->
            <div class="form-group col-md-12" id="pratisthan">
                <p>
                 १४. तपाईको परिवारले संचालन गरेको व्यवसायिक प्रतिष्ठानको विवरण दिनुहोस ।  
             </p>
             <div class="col-md-12">
                <table class="table">
                    <tr>
                      <th rowspan="2">@lang('commonField.extra.sn')</th>
                      <th rowspan="2">प्रतिष्ठानको किसिम</th>
                      <th rowspan="2">अनुमानित लगानी रु.</th>
                      <th colspan="2">संलग्न जनशक्ति</th>
                      <th rowspan="2">सरदर वार्षिक आम्दानी, रु.हजारमा</th>
                  </tr>
                  <tr>
                      <th>पुरुष</th>
                      <th>महिला</th>
                  </tr>
                  <?php       
                  $hsOrgDetailArr = isset($hsOrgDetailsData->hsOrgDetails) ? json_decode($hsOrgDetailsData->hsOrgDetails, true) : '';

                  for($count = 1; $count <= 5; $count++ ){                    
                    ?>
                    <tr>
                        <td>{{ $count }}</td>
                        <td>
                            {{ Form::text("hsOrgDetails[$count][orgType]", isset($hsOrgDetailArr[$count]['orgType']) ? $hsOrgDetailArr[$count]['orgType'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'organization type name']) }}         
                        </td>
                        <td>
                            {{ Form::number("hsOrgDetails[$count][investment]",  isset($hsOrgDetailArr[$count]['investment']) ? $hsOrgDetailArr[$count]['investment'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Investment']) }}
                        </td>
                        <td>
                            {{ Form::number("hsOrgDetails[$count][male]",  isset($hsOrgDetailArr[$count]['male']) ? $hsOrgDetailArr[$count]['male'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Male number']) }}
                        </td>
                        <td>
                            {{ Form::number("hsOrgDetails[$count][female]",  isset($hsOrgDetailArr[$count]['female']) ? $hsOrgDetailArr[$count]['female'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'female number']) }}
                        </td>
                        <td>
                            {{ Form::number("hsOrgDetails[$count][YearlyIncome]",  isset($hsOrgDetailArr[$count]['YearlyIncome']) ? $hsOrgDetailArr[$count]['YearlyIncome'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Yearly income']) }}
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <!--  -->
            </table>
        </div>
    </div> 
</div>
<div class="msgDisplay"></div>

</div>
</div>