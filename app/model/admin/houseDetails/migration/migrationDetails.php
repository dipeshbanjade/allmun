<?php

namespace App\model\admin\houseDetails\migration;

use Illuminate\Database\Eloquent\Model;

class migrationDetails extends Model
{
    protected $table = 'migration_details';
}
