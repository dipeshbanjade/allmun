<?php

namespace App\Http\Requests\admin\dartaChalani;

use Illuminate\Foundation\Http\FormRequest;

class chalaniVal extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'chalaniNo' => 'required|min:3|max:100|unique:chalanis',
            'chalaniDate' => 'required'
        ];
    }
}
