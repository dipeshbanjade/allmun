@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-body ">
            <div class="card-head">
                <header><i class="fa fa-eye">&nbsp;@lang('commonField.extra.leaveType')</i></header>
            </div>
            <button id="" class="btnAddLeaveType btn btn-primary pull-right">
                <i class="fa fa-plus"></i>
                 <span>@lang('commonField.extra.createLeaveType') </span>   
            </button>
            <div class="table-scrollable" id="dropLabel">
                <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4">
                    <thead>
                        <tr>
                            <th width="5%">@lang('commonField.extra.sn')</th>
                            <th>@lang('commonField.extra.refCode')</th>
                            <th>@lang('commonField.personal_information.name') </th>
                            <th>@lang('commonField.extra.daysLeave')</th>
                            <th> @lang('commonField.extra.action') </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php  $count = 1   @endphp
                        @if(count($allLeaveType) > 0)
                        @foreach($allLeaveType as $leaveType) 
                        <tr class="odd gradeX">
                            <td width="5%">{{ $count ++ }}</td>
                            <td>{{ $leaveType->refCode }}</td>
                            <td>{{ $leaveType->nameNep }}  - {{ $leaveType->nameEng }}</td>
                            <td>{{ $leaveType->daysLeave }}</td>
                            <td>
                                <a href="" class="btnEditLeaveType btn btn-primary btn-xs" id="" data-url="{{ route('admin.leaveType.edit',$leaveType->id)}}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" class="deleteMe" id="" data-url="{{ route('admin.leaveType.delete',$leaveType->id)}}" data-name="{{ $leaveType->refCode }}"><button class="btn btn-danger btn-xs">
                                    <i class="fa fa-trash-o "></i></button></a>
                                    <a href="#" class="delete_data" id="" ><button class="btn btn-danger btn-xs">
                                        <i class="fa fa-eye-slash"></i></button></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                        </table>
                        {{ $allLeaveType->links() }}
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('modelSection')
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.addLeaveType')</i></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    {{ Form::open(['route' => 'admin.leaveType.store', 'name' => 'frmLeaveType']) }}
                    <div class="modal-body">
                        @include('admin.setting.leaveType._form')
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <div class="col-lg-12 p-t-20 text-right"> 
                            {{ Form::submit(__('commonField.button.create'), ['class' => 'btn btn-success']) }}

                            <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

        <!-- edit leavetype modal -->
        <div class="modal" id="editLeaveTypeModel">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-plus">&nbsp;@lang('commonField.extra.editLeaveType')</i></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                       {{ Form::open(['name'=>'frmeditLeaveType', 'id' => 'frmUpdateLeaveType']) }}
                       @include('admin.setting.leaveType._form')
                       {{ Form::hidden('leaveTypeId', null, ['class' => 'leaveTypeId']) }}
                       <div class="col-lg-12 p-t-20 text-right"> 
                        {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success']) }}
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('commonField.button.close')</button>
                    </div>
                    {{ Form::close() }}
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">

                </div>

            </div>
        </div>
    </div>
    @endsection
    @section('custom_script')
    @include('admin.setting.leaveType.script')
    @endsection