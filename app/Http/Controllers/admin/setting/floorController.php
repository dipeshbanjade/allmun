<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\Floor;
use App\model\mylogic;
use App\Http\Requests\admin\setting\Floor\floorValidation;
use Validator;
class floorController extends Controller
{
    protected $viewPath  = 'admin.setting.floor';
    protected $floorTbl = 'floors';
    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.FloorPrefix');
    }
    public function index()
    {
        $page['page_title']       = 'Floor';
        $page['page_description'] = 'list of all Floor';

        $allFloor = Floor::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        $prefix   = myLogic::sysRefCode($tablename = $this->floorTbl, $prefix = $this->prefix);
        return view($this->viewPath . '.index', compact(['page', 'allFloor', 'prefix'])); 
    }

    public function store(floorValidation $request)
    {
           $mySave = Floor::create([
             'refCode' => $request->refCode,
             'nameNep' => $request->nameNep ?? 'N/A',
             'nameEng' => $request->nameEng,
             'status'  => 0,
             'softDelete' => 0
             ]);
           if ($mySave) {
            return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function edit($id)
    {
         $data = Floor::findOrFail($id);
         if ($data) {
            return response()->json([
              'success' => true,
              'refCode' => $data->refCode,
              'id'      => $data->id,
              'nameNep' => $data->nameNep,
              'nameEng' => $data->nameEng
              ]);
        }else{
            return response()->json([
              'success' => false,
              'message' => config('activityMessage.dataNotFound')
              ]);
        }
    }


    public function update(Request $request, $id)
    {
          $validator = Validator::make($request->all(), [
           'nameEng'  => 'required|numeric'
           ]);

          if ($validator->fails()) {
              return response()->json([
                 'success' => false,
                 'message' => $validator->errors()->all()
                 ]);
          }
          $update          = Floor::findOrFail($id);
          $update->nameNep = $request->nameNep ?? 'N/A';
          $update->nameEng = $request->nameEng;

          $myUpdate = $update->update();
          if ($myUpdate) {
            return response()->json([
              'success' => true,
              'message' => config('activityMessage.updateMessage')
              ]);
        }else{
            return response()->json([
              'success'  => false,
              'message'  => config('activityMessage.notUpdated')
              ]);
        }
    }


    public function destroy($id)
    {
            $del = Floor::findOrFail($id);
            $del->softDelete = 1;
            $myDel = $del->update();
            if ($myDel) {
             return MyLogic::successChangeStatus($code = 200);
         }else{
          return MyLogic::successChangeStatus($code = 301);
        }
    }
}
