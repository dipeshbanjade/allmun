<?php

namespace App\Http\Controllers\admin\shifaris\other;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\consumerCommitte;
use App\model\shifaris\ApplicantUser;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\consumerCommitteVal;
use DB;
use Auth;

use App\model\mylogic;

class ConsumerCommitteController extends Controller
{
    protected $viewPath = 'admin.shifaris.other.consumerCommitte';
    protected $shifarisTableName     = 'consumer_committes';
    protected $appUsrTable     = 'applicant_users';
    protected $deginationsTbl = 'deginations';

    protected $prefix;


    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixConsumerCommitte');
        
    }

    public function index()
    {
        $page['page_title']       = 'Consumer Committee Constituted';
        $page['page_description'] = 'Consumer Committee Constituted';

        
        $propertyVal = consumerCommitte::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        

    }

    public function create()
    {
        $page['page_title']       = 'Consumer Committee Constituted : create';
        $page['page_description'] = 'Consumer Committee Constituted details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);
        // dd($getUniqueCode);
        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deginationsTbl, $orderColumn = 'nameNep', $orderby = 'ASC');
        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));
        
    }

    public function store(consumerCommitteVal $request)
    {
        //
        
     DB::beginTransaction();
     try {

                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);
        
        $isShifarisExist = isset($request->shifarisId) ? consumerCommitte::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
        
        $save = !empty($isShifarisExist['id']) ? consumerCommitte::findOrfail($isShifarisExist['id']) : new consumerCommitte;



        $save->createdBy = Auth::user()->id;

        $save->refCode  = $request->refCode;

        $save->wards_id  = Auth::user()->wards_id ?? NULL;

        $save->municipilities_id = getMunicipalityData()['id'] ?? "";
        $save->issuedDate  = $request->issuedDate;
        $save->chalaniNum = $request->chalaniNum;
        $save->orgName  = $request->orgName;
        $save->orgLocation  = $request->orgLocation;
        $save->fiscalYear  = $request->fiscalYear;
        $save->municipalityName  = $request->municipalityName;
        $save->wardNum  = $request->wardNum;
        $save->organizationAddress  = $request->organizationAddress;
        $save->organizationType  = $request->organizationType;
        $save->organizationWard  = $request->organizationWard;
        $save->businessName  = $request->businessName;
        $save->projectAmount  = $request->projectAmount;
        
        $committeCount = count($request->name);
        $name = $request->name;
        $post = $request->post;
        $remark = $request->remark;
        $committeArray = [];

        for ($i=0; $i < $committeCount; $i++) {
            array_push($committeArray, [
                'name' => $name[$i],
                'post' => $post[$i],
                'remark' => $remark[$i] ?? 'N/A'
            ]);
        }
        $save->meetingStatement = json_encode($committeArray);

        $save->authorizedPerson  = $request->authorizedPerson;
        $save->deginations_id  = $request->deginations_id;

        $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

        $mySave = $save->save();

                       //end for login
        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

        $citizenShifarish = true;
        if(empty($isShifarisExist['id'])){
            $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
        }            
        if ($mySave && $log && $citizenShifarish) {
         DB::commit();
         return response()->json([
             'success' => true,
             'message' => config('activityMessage.saveMessage')
         ]);
                   // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
     }else{
         return response()->json([
             'success' => false,
             'message' => config('activityMessage.unSaveMessage')
         ]);
                   // return back()->withMessage(config('activityMessage.unSaveMessage'));
     }
 }catch (Exception $e) {
    DB::rollback();
    return back()->withMessage(config('activityMessage.dataNotInserted'));
}

}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
