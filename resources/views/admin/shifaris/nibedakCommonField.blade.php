<section class="bibaran">
  <!-- <fieldset>
    <legend>निवेदकको विवरण</legend>
    <div class="form-group">
      <label>निवेदकको नागरिकता नं.</label>
      <input type="text" name="citizenshipNumber" id="citizenshipNumber" class="form-control">
    </div>
    <div class="form-group">
      <label>निवेदकको नाम</label>
      <input type="text" name="applicantName" id="applicantName" class="form-control">
    </div>
    <div class="form-group">
      <label>निवेदकको ठेगाना</label><input type="text" name="applicantAddress" id="applicantAddress" class="form-control">
    </div>    
    <div class="form-group">
      <label>निवेदक (NID) </label>
      <input type="text" name="nibedak_nid" id='id' class="form-control">
    </div>
  </fieldset> -->
  <?php
     $citizenInfo = getCitizenForSifarish();
  ?>
  @if(!empty($citizenInfo))

      <label>@lang('commonField.extra.citizen')
      </label><br>

      <span>
        <?php  $lan = getLan(); ?>
        <select id="nebedakId" name="citizenshipNumber" class="sysSelect2" required >
          <option value="">@lang('commonField.extra.citizen')</option>
          @if(count($citizenInfo))
          @foreach($citizenInfo as $citizen)
          <option value="{{  $citizen->id.'$sep!'.$citizen->citizenNo }}" {{ isset($data->applicant_users_id) && $data->applicant_users_id == $citizen->id ? 'selected' : ''}} {{(old('citizen')==$citizen->id)? 'selected':''}}>
            <label><b>CitizenNo : {{ $citizen->citizenNo }}</b> </label>
            <label>
              <b>Name : {{ $lan=="np" ? $citizen->fnameNep . $citizen->mnameNep . $citizen->lnameNep : $citizen->fnameEng . $citizen->mnameEng . $citizen->lnameEng }}</b> 
            </label>
          </option> 
          @endforeach
          @endif
        </select><br><hr>
      </span>
  @endif
</section>
