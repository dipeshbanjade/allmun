<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardjetMachineRecomm']").validate({
			rules:{
				refCode:{
					required: true,
					minlength: 3
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNumber : {
					required : true,
					minlength : 2,
					maxlength : 30
				},
				companyHead : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				companyName : {
					required : true,
					minlength : 3,
					maxlength : 2000
				},
				reason : {
					required : true
				},
				municipality : {
					required : true,
					minlength: 3,
					maxlength: 30
				},
				wardNum : {
					required : true,
					minlength: 1,
					maxlength: 3,
					number: true
				},
				jetReason : {
					required : true
				},
				roadName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				refCode: "Code is required",
				issuedDate : "Issue Date is required",
				chalaniNumber : "Chalani Number is required",
				companyHead : "Company Head is required",
				companyName : "Company Name is required",
				roadName : "Road Name is required",
				wardNum: "Ward Number is required",
				municipality: "Municipality Name is required",
				reason : "Reason is required",
				jetReason : "Jet Reason is required",
				authorizedPerson : "Authorizing person is required"
			}
		});
	});

	
	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='houseOwnerName']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>