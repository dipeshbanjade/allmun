<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $table = 'floors';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
