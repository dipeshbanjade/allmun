<?php

namespace App\Http\Controllers\admin\ward;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\ward\WardStaff;
use App\model\myLogic;
use App\Http\Requests\admin\ward\wardStaffValidation;
use Validator;
use Auth;
use App\model\admin\acl\Role;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;

class wardStaffController extends Controller
{
    protected $viewPath      = 'admin.ward.wardStaff';
    protected $wardStaffTbl  = 'ward_staffs';

    protected $staffTypeTbl = 'staff_types';
    protected $provincesTbl = 'provinces';
    protected $deginationTbl = 'deginations';
    protected $districtTbl = 'districts';

    protected $prefix;

    protected $imgProfile;
    protected $imgCitizen;

    protected $munId;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.wardPrefix');
        
        $this->imgProfile   = config('activityMessage.wardStaffImg'); 
        $this->imgCitizen   = config('activityMessage.wardStaffCitizenImg');
        $this->munId        = myLogic::getMunicipalityDetails()['id'];

    }

    public function index()
    {
        $page['page_title']       = 'Ward Staff List';
        $page['page_description'] = 'list of all ward Staff';

        $allStaff = WardStaff::orderby('firstNameEng', 'ASC')->where(['softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->paginate(config('activityMessage.pagination'));
        /*Checking for auth*/
        if (Auth::user()->userLevel === 'wrd'  || Auth::user()->userLevel === 'dev') {
            return view($this->viewPath . '.index', compact(['page', 'allStaff']));
        }else{
            return back()->withMessage(config('activityMessage.unAuthorized'));
        }
    }
    public function create()
    {
       $page['page_title']        = 'Ward : Create';  //TODO from language file
       $page['page_description']  = 'Ward staff details';

       $staffTypeId = mylogic::getDrowDownData($tablename = $this->staffTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
       $deginationId = mylogic::getDrowDownData($tablename = $this->deginationTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
       $provincesId = mylogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
       $districtId = mylogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');

       $deptId   = mylogic::getDrowDownData($tablename = 'departments', $orderColumn = 'nameEng', $orderby = 'ASC');
     
       // $refCode  = mylogic::sysRefCode($tablename = $this->wardStaffTbl, $prefix = $this->prefix);
       $getWard = mylogic::getAllWardDetails();

       $roles = myLogic::getSysRole($roleFor = 'ward');

       if (count($getWard) > 0) {  // checking ward exists or not
             if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd') {
                     return view($this->viewPath . '.create', compact(['page', 'staffTypeId', 'deginationId', 'provincesId', 'districtId', 'refCode', 'getWard', 'roles', 'deptId']));
             }else{
                 return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
             }
       }else{
        return back()->withMessage(config('activityMessage.createWardFirst'));
       }

    }

    public function store(wardStaffValidation $request)
    {
      if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd') {
        DB::beginTransaction();
        $refCode = mylogic::sysRefCode($tablename = $this->wardStaffTbl, $prefix = $this->prefix);
        try {
                $save = new WardStaff;
                $save->municipilities_id  = myLogic::getMunicipalityDetails()['id']; 
                $save->wards_id       = Auth::user()->wards_id; 
                $save->refCode        = $refCode;
                if ($request->hasFile('profilePic')) {
                    $imgFile = $request->file('profilePic');
                    $filename = str_replace(' ', '', $refCode). '.' . $imgFile->getClientOriginalExtension();

                    $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgProfile, $filename);

                    $save->profilePic =  $this->imgProfile . $filename;
                }
                $save->firstNameNep     = $request->firstNameNep ?? '';
                $save->firstNameEng     = $request->firstNameEng;

                $save->middleNameNep    = $request->middleNameNep ?? ''; 
                $save->middleNameEng    = $request->middleNameEng ?? ''; 

                $save->lastNameNep      = $request->lastNameNep ?? ''; 
                $save->lastNameEng      = $request->lastNameEng;

                $save->email            = $request->email ?? 'N/A';
                $save->citizenNo        = $request->citizenNo;

                $save->staff_types_id   = $request->staff_types_id;
                $save->deginations_id   = $request->deginations_id;

                $save->provinces_id    = $request->provinces_id; 
                $save->districts_id     = $request->districts_id; 
                $save->joingDateNep      = $request->joingDateNep ?? 'N/A';
                $save->joingDateEng      = $request->joingDateEng ?? 'N/A';
                $save->wardNo           = $request->wardNo ?? 'N/A';
                $save->villageNep       = $request->villageNep ?? 'N/A'; 
                $save->villageEng       = $request->villageEng; 
                $save->dob              = $request->dob ?? 'N/A'; 
                $save->dobEng           = $request->dobEng ?? 'N/A'; 
                $save->phoneNumber      = $request->phoneNumber;
                $save->departments_id   = $request->departments_id;
                $save->pounchId         = $request->pounchId;

                if ($request->hasFile('citizenImagePath')) {
                 $imgFile = $request->file('citizenImagePath');
                 $filename = str_replace(' ', '', $refCode). '.' . $imgFile->getClientOriginalExtension();

                 $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgCitizen, $filename);

                 $save->citizenImagePath =  $this->imgCitizen . $filename;
               }
               $mySave = $save->save();
               $encriptId = myLogic::idEncription($save->id);

               $save->idEncrip  = $encriptId;  // making Id encription
               $save->save();
              // create login 

              $validator = Validator::make($request->all(), [
                        'email' => 'required|email|unique:users',
                    ]);

              if ($validator->fails()) {
                  return back()->withErrors($validator)->withInput();
              }

              $login = new User;
              $login->name               = $request->firstNameEng .'&nbsp;'. $request->middleNameEng .'&nbsp;'. $request->lastName;
              $login->email              = $request->email;
              $login->password           = Hash::make($request->password);
              $login->userLevel          = 'wrd';
              $login->municipilities_id  = $this->munId;
              $login->wards_id           =Auth::user()->wards_id;
              $login->roles_id           = $request->roles_id;
              $login->staffs_id          = $save->id;

              $roleLogin =  $login->save();
              $login->idEncrip = $encriptId;
              $login->save();
              // login
            
              $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createAction'), $tableName = $this->wardStaffTbl, $tblId = $save->id);

             if ($mySave && $roleLogin && $log) {
                 DB::commit();
                return back()->withMessage(config('activityMessage.saveMessage') .$refCode);
            }else{
                return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        } catch (Exception $e) {
             DB::rollback();
             return back()->withMessage(config('activityMessage.dataNotInserted'));
        }
              /*----------------------------------------------------------------*/
      }else{
          return back()->withMessage(config('activityMessage.accessDenied'));
      }
    }
    public function show($slug)
    {
        if ($slug) {
          $page['page_title']        = 'Staff Detail';
          $page['page_description']  = 'Ward Staff Details profile';

          $details = WardStaff::where(['status' => 0, 'softDelete' => 0, 'idEncrip' => $slug])->first();
          
          $details = WardStaff::where('idEncrip', $slug)->first();
          $myRole = User::select('roles_id')->where(['staffs_id' => $details->id])->first();
          $details->roles_id = $myRole->roles_id ? mylogic::getRoleBy($myRole->roles_id)['roleName'] : '';

          return view($this->viewPath . '.show', compact(['page', 'details']));
        }
    }

    public function edit($slug)
    {
      if (Auth::user()->userLevel =='wrd' || Auth::user()->userLevel =='dev') {
          $page['page_title']       = 'Ward Staff Edit';
          $page['page_description'] = 'list of all ward staff';
          
           if ($slug) {
              $roles = myLogic::getSysRole($roleFor = 'ward');
              $staffTypeId = mylogic::getDrowDownData($tablename = $this->staffTypeTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
              $deginationId = mylogic::getDrowDownData($tablename = $this->deginationTbl, $orderColumn = 'nameEng', $orderby = 'ASC');
              $provincesId = mylogic::getDrowDownData($tablename = $this->provincesTbl, $orderColumn = 'name', $orderby = 'ASC');
              $districtId = mylogic::getDrowDownData($tablename = $this->districtTbl, $orderColumn = 'districtNameEng', $orderby = 'ASC');

             $deptId   = mylogic::getDrowDownData($tablename = 'departments', $orderColumn = 'nameEng', $orderby = 'ASC');
              $data = WardStaff::where(['status' => 0, 'softDelete' => 0, 'idEncrip' => $slug])->first();
              $usrRoleId = User::select('roles_id')->where(['staffs_id'=>$data->id])->first();
              $data->roles_id = $usrRoleId->roles_id ? $usrRoleId->roles_id : '';
              return view($this->viewPath . '.edit', compact(['page', 'data', 'roles', 'staffTypeId', 'deginationId', 'provincesId', 'districtId', 'deptId']));
           }else{
             return redirect()->route('home')->withMessage(config('activityMessage.idNotFount'));
           }

      }else{
        return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
      }
    }

    public function update(Request $request, $id)
    {
          $validator = Validator::make($request->all(), [
            'firstNameNep'   => 'required|min:3|max:60',
            'firstNameEng'   => 'required|min:3|max:60',
            'lastNameNep'   => 'required|min:3|max:60',
            'lastNameEng'   => 'required|min:3|max:60',
            'citizenNo'   => 'required|min:4|max:60',
            'staff_types_id'   => 'required',
            'deginations_id'   => 'required',
            'provinces_id'   => 'required',
            'districts_id'   => 'required',
            'joingDateNep'   => 'required',
            'wardNo'       => 'required',
            'villageNep'   => 'required|min:3|max:60', 
            'villageEng'   => 'required|min:3|max:60', 
            'phoneNumber'   => 'required|min:10|max:15',
            'roles_id'       => 'required',
            'provinces_id'   => 'required',
            'districts_id'   => 'required',
            'email'          =>'required|email|unique:ward_staffs,email,'.$request->id,
            'pounchId'      =>'required|unique:ward_staffs,pounchId,'.$request->id
            ]);

          if ($validator->fails()) {
               return back()->withErrors($validator)->withInput();
            }
                 DB::beginTransaction();
                 try {
                    $update = WardStaff::findOrFail($id);
                    $update->firstNameNep  = $request->firstNameNep;
                    $update->firstNameEng  = $request->firstNameEng;
                    $update->middleNameNep  = $request->middleNameNep ?? 'N/A'; 
                    $update->middleNameEng  = $request->middleNameEng ?? 'N/A'; 
                    $update->lastNameNep  = $request->lastNameNep; 
                    $update->lastNameEng  = $request->lastNameEng;
                    $update->email  = $request->email;
                    $update->citizenNo  = $request->citizenNo;
                    $update->staff_types_id  = $request->staff_types_id;
                    $update->deginations_id  = $request->deginations_id;
                    $update->provinces_id  = $request->provinces_id; 
                    $update->districts_id  = $request->districts_id; 
                    $update->joingDateNep  = $request->joingDateNep;
                    $update->joingDateEng  = $request->joingDateEng;
                    $update->wardNo  = $request->wardNo;
                    $update->villageNep  = $request->villageNep; 
                    $update->villageEng  = $request->villageEng; 
                    $update->phoneNumber  = $request->phoneNumber;
                    $update->dob          = $request->dob;
                    $update->dobEng       = $request->dobEng;
                    $update->departments_id = $request->departments_id;
                    /*--------------------------------------------*/
                    if ($request->hasFile('profilePic')) {
                        $profilePic = 
                        $imgFile = $request->file('profilePic');
                        $filename = str_replace(' ', '', $update->refCode). '.' . $imgFile->getClientOriginalExtension();

                        $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imgProfile, $filename);

                        $update->profilePic =  $this->imgProfile . $filename;
                    }
                    /*----------------------------------------------*/
                    if ($request->hasFile('citizenImagePath')) {
                        $imgFile = $request->file('citizenImagePath');
                        $filename = str_replace(' ', '', $request->nameEng). '.' . $imgFile->getClientOriginalExtension();

                        $imgPath = mylogic::cropImage($imgFile, 250, 250, $this->imagePath, $filename);
                        $update->citizenImagePath =  $this->imagePath.$filename;
                    }
                    $myUpdate = $update->update();

                    if (!empty($request->roles_id)) {
                      $updUser = User::where(['staffs_id' => $update->id])->first();
                      $updUser->roles_id  = $request->roles_id;
                      $updUser->email     = $request->email;
                      $updUser->name      = $request->firstNameEng .'&nbsp;'. $request->middleNameEng .'&nbsp;'. $request->lastName;
                      $usrUpdate = $updUser->update();
                    }
                    
                    $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateWardStaff'), $tableName = $this->wardStaffTbl, $tblId = $update->id);

                    if ($myUpdate) {
                      DB::commit();
                       return back()->withMessage(config('activityMessage.updateMessage'));
                   }else{
                       return back()->withMessage(config('activityMessage.notUpdated'));
                   }
                 }catch (Exception $e) {
                             DB::rollback();
                             return back()->withMessage(config('activityMessage.dataNotInserted'));
                }
    }

    public function destroy($id)
    {
        $del = WardStaff::findOrFail($id);
        $del->softDelete = 1;
        $myDel = $del->update();
        if ($myDel) {
          return MyLogic::successChangeStatus($code = 200);
        }else{
           return MyLogic::successChangeStatus($code = 301);
        }
    }

    public function changeStatus(Request $request){
       $id  = $request->id;
       $status = $request->status==1 ? 0 : 1;
       $change = mylogic::sysChangeStatus($tablename = $this->wardStaffTbl, $Id = $id, $status = $status);

       if ($change) {
           return MyLogic::successChangeStatus($code = 200);
       }else{
           return MyLogic::idNotFoundMessage($code = 301);
       }
    }


}
