<script type="text/javascript">
	var btnAddTableRow = $('#addTableRow');
	btnAddTableRow.on('click', function(e){
		e.preventDefault();

		var bodartha = '<input type="text" class="form-control" name="bodartha[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+bodartha+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#sabik_address_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmWardengLanguage']").validate({
			rules:{
				refCode : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDate : {
					required : true,
					date : true
				},
				nameOfOffice : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				officeAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				subject : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				letterDesc : {
					required : true,
					minlength : 20,
					maxlength : 3000
				},
				accountOpeningDetail : {
					required : true,
					minlength : 3
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {
				refCode : "Ref code is required",
				issuedDate : "Issue Date is required",
				nameOfOffice : "Name Of Office is required",
				officeAddress : "Office address is required",
				subject : "Subject is required",
				letterDesc : "Letter Description is required",
				accountOpeningDetail : "Account Openeing Detail is required",
				authorizedPerson : "Authorized Person is required"
			}
		});
	});

</script>