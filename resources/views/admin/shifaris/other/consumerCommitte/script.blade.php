<script type="text/javascript">
var btnAddBodartha = $('#add');
var clickCount = 1;
btnAddBodartha.on('click', function(e){
	e.preventDefault();		
	clickCount++;
	var no = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="" value="'+clickCount+'">';
	var name = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="name[]">';
	var post = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="post[]">';
	var remark = '<input type="text" class="dashed-input-field" name="remark[]">';

	var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

	var insertRow = "<tr class='tblRow'>"
	+"<td>"+no+"</td>"
	+"<td>"+name+"</td>"
	+"<td>"+post+"</td>"
	+"<td>"+remark+"</td>"
	+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
	+"</tr>";

	$('#accountTable').append(insertRow);

	$('.btnRemoveItem').on('click', function(e){
		$(this).parent().parent().remove();
		return false;
	});
});

$(function(){
	/*add buttoin*/
	$("form[name='frmWardconsumerCommitte']").validate({
		rules:{


			refCode : {
				required : true,
				minlength : 3
			},
			issuedDate : {
				required : true,
				date : true
			},
			chalaniNum : {
				required : true,
				minlength : 3,
				maxlength : 50
			},
			orgName : {
				required : true,
				minlength : 3,
				maxlength : 50
			},
			orgLocation : {
				required : true,
				minlength : 3,
				maxlength : 50
			},
			fiscalYear : {
				required : true,
				minlength : 3,
				maxlength : 50
			},
			municipalityName : {
				required : true,
				minlength : 3,
				maxlength : 50
			},
			wardNum : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number : true
			},
			organizationAddress : {
				required : true,
				minlength : 3,
				maxlength : 50
			},
			organizationType : {
				required : true
			},
			organizationWard : {
				required : true,
				minlength : 1,
				maxlength : 3
			},
			businessName : {
				required : true,
				minlength : 3,
				maxlength : 50
			},
			projectAmount : {
				required : true,
				minlength : 3,
				maxlength : 50
			},				
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 50
			}
		},
		messages: {
			refCode : "RefCode  is required",
			issuedDate : "Issued date  is required",
			chalaniNum : "Chalani Number is required",
			orgName : " Org name is required",
			orgLocation : " Org location is required",
			fiscalYear : " Fiscal year is required",
			municipalityName : " Municipality Name is required",
			wardNum : "Ward Number is required",
			organizationAddress : "Organization Address is required",
			organizationType : " Organization Type is required",
			organizationWard : " organication Ward is required",
			businessName : "Business name is required",
			projectAmount : "Project Amount is required",
			meetingStatement : " Meeting Statement required",
			authorizedPerson : " Authorized Person is required"
		}
	});
});
</script>