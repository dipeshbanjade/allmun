<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class birthDateVerifyNepVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:birth_date_verify_neps',
            'chalaniNumber' => 'required|numeric|unique:birth_date_verify_neps',
            'fatherName' => 'required',
            'motherName' => 'required',
            'municipalityName' => 'required',
            'wardNumber' => 'required',
            'sabikAddress' => 'required',
            'personName' => 'required',
            'birthDate' => 'required',
            'birthPlace' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
