<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class MigrationReason extends Model
{
    protected $table = 'migration_reasons';
    protected $fillable = ['refCode','nameEng','nameNep','desc','status','softDelete'];

}
