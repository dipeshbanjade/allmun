<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class ScholarshipRecommVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:scholarship_recomms',
            'chalaniNum' => 'required|unique:scholarship_recomms',
            'issuedDate' => 'required',
            'municipalityName' => 'required', 
            'organizationAddress' => 'required',
            'organizationType' => 'required',
            'organizationWard' => 'required|numeric',
            'wardNum' => 'required|numeric',
            'addressType' => 'required',
            'fatherName' => 'required',
            'motherName' => 'required',
            'economyStatus' => 'required',
            'childrenGender' => 'required',
            'childrenTitle' => 'required',
            'nameOfChildren' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
