<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class houseType extends Model
{
    protected $table = 'house_types';
    protected $fillable = ['refCode','houseNep','houseEng','status','softDelete'];
}
