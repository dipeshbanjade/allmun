@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
    <div class="card-body">

        <a href="{{ route('system.userDetails.create') }}" class="btn btn-primary pull-right"><i class='fa fa-plus'></i>&nbsp;Create User</a>
        <div class="clearfix"></div>

           <ul class="nav nav-tabs">
             <li class="nav-item">
               <a class="nav-link active" data-toggle="tab" href="#home">Super User</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="tab" href="#menu1">Municipility</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" data-toggle="tab" href="#menu2">Wards</a>
             </li>
           </ul>

           <!-- Tab panes -->
           <div class="tab-content">
             <div class="tab-pane container active" id="home">
                  <div class="card-head">
                       <header>Super User</header>
                   </div>

                   <div class="table-scrollable" style="color:black;">
                         <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle"  id="example4">
                         <thead>
                          <tr>
                          <th>sn</th>
                          <th>Email</th>
                          <th>Name</th>
                          <th>Role</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        @if(count($allUsr) > 0)
                          @php $count = 1  @endphp
                          @foreach($allUsr as $usr)
                          @if($usr->userLevel == 'dev')
                          <tbody>
                          <tr>
                              <td>{{ $count ++ }}</td>
                              <td>{{ $usr->email }}</td>
                              <td>{!! $usr->name !!}</td>
                              <td>Super Admin</td>
                              <td>
                                <a href="" class="btn btn-primary btn-xs">
                                  <i class="fa fa-eye"></i>
                                </a>
                                <!--  -->
                                <a href="{{ route('adminGetStaffDetails', $usr->staffs_id) }}" class="btn btn-primary btn-xs" title="udpate staff">
                                  <i class="fa fa-pencil"></i>
                                </a>
                                <!--  -->
                                 <a href="#" class="delete_data" id="" >
                                      <button class="btn btn-danger btn-xs">
                                      <i class="fa fa-trash-o "></i></button>
                                  </a>

                                  <!--  -->
                                  <a href="{{ route('adminChangePwd', $usr->id) }}" class="btn btn-primary btn-xs" title="change password">
                                      <i class="fa fa-lock"></i>
                                  </a>
                              </td>
                          </tr>
                          @endif
                          @endforeach
                        @endif
                        </tbody>
                      </table>
                      </div>
      
             </div>
             <!--  -->
             <div class="tab-pane container fade" id="menu1">
                  <div class="card-head">
                       <header>Municipility User</header>
                   </div>
                   <div class="table-scrollable" style="color:black;">
                         <table class="example5 table table-striped table-bordered table-hover table-checkable order-column valign-middle ">
                        <thead>
                        <tr>
                          <th>sn</th>
                          <th>Role</th>
                          <th>Email</th>
                          <th>Name</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        @if(count($allUsr) > 0)
                          @php $count = 1  @endphp
                          @foreach($allUsr as $usr)
                          @if($usr->userLevel === 'mun')
                          <tbody>
                          <tr>
                              <td>{{ $count ++ }}</td>
                              <td>{{ $usr->roles->roleName }}</td>
                              <td>{{ $usr->email }}</td>
                              <td>{!! $usr->name !!}</td>
                              <td>
                                <a href="" class="btn btn-primary btn-xs">
                                  <i class="fa fa-eye"></i>
                                </a>
                                <!--  -->
                                <a href="{{ route('adminGetStaffDetails', $usr->staffs_id) }}" class="btn btn-primary btn-xs">
                                  <i class="fa fa-pencil"></i>
                                </a>
                                <!--  -->
                                 <a href="#" class="delete_data" id="" >
                                      <button class="btn btn-danger btn-xs">
                                      <i class="fas fa-toggle-on"></i></button>
                                  </a>
                                  <!-- change pasword -->
                                  <a href="{{ route('adminChangePwd', $usr->staffs_id) }}" class="btn btn-primary btn-xs" title="change password">
                                      <i class="fa fa-lock"></i>
                                  </a>    
                              </td>
                          </tr>
                          @endif
                          @endforeach
                        @endif
                        </tbody>
                      </table>
               </div>
             </div>
             <!--  -->
             <div class="tab-pane container fade" id="menu2">
                <div class="card-head">
                     <header>Ward User</header>
                 </div>
           <div class="table-scrollable" style="color:black;">
                 <table class="example5 table table-striped table-bordered table-hover table-checkable order-column valign-middle ">
                        <thead>
                         <tr>
                           <th>sn</th>
                           <th>Ward</th>
                           <th>Role</th>
                           <th>Email</th>
                           <th>Name</th>
                           <th>Action</th>
                         </tr>
                         </thead>
                         @if(count($allUsr) > 0)
                           @php $count = 1  @endphp
                           @foreach($allUsr as $usr)
                           @if($usr->userLevel === 'wrd')
                           <tbody>
                           <tr>
                               <td>{{ $count ++ }}</td>
                               <td>{{ $usr->wards_id }}</td>
                               <td>{{ $usr->roles->roleName }}</td>
                               <td>{{ $usr->email }}</td>
                               <td>{!! $usr->name !!}</td>
                               <td>
                                 <a href="" class="btn btn-primary btn-xs">
                                   <i class="fa fa-eye"></i>
                                 </a>
                                 <!--  -->
                                 <a href="{{ route('adminGetStaffDetails', $usr->staffs_id) }}" class="btn btn-primary btn-xs">
                                   <i class="fa fa-pencil"></i>
                                 </a>
                                 <!--  -->
                                  <a href="#" class="delete_data" id="" >
                                       <button class="btn btn-danger btn-xs">
                                       <i class="fa fa-trash-o "></i></button>
                                   </a>
                                   <!--  -->
                                   <a href="{{ route('adminChangePwd', $usr->staffs_id) }}" class="btn btn-primary btn-xs" title="change password">
                                       <i class="fa fa-lock"></i>
                                    </a>
                               </td>
                           </tr>
                           @endif
                           @endforeach
                         @endif
                         </tbody>
                       </table>
                    </div>
                  
             </div>

           </div>
     </div>
  </div>
</div>
@endsection
@section('custom_script')
  @include('admin.acl.user.script')
@endsection