<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class marriageVerifyVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:marriage_verifies',
            'issuedDate' => 'required',
            'letterSub' => 'required',
            'brideName' => 'required',
            'bridefatherName' => 'required',
            'brideGroomName' => 'required',
            'brideGroomfatherName' => 'required',
            'municipality' => 'required',
            'ward' => 'required',
            'dateInBS' => 'required',
            'dateInAD' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
