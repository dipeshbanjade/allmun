<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class AccountNavigateVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:account_navigates',
            'chalaniNum' => 'required|unique:account_navigates',
            'issuedDate' => 'required',
            'organizationName' => 'required', 
            'organizationLocation' => 'required',
            'municipalityName' => 'required', 
            'wardNum' => 'required|numeric', 
            'accountNum' => 'required|numeric',
            'purpose' => 'required',
            'year' => 'required|numeric',
            'interest' => 'required',
            'name' => 'required',
            'post' => 'required',
            'authorizedPerson' => 'required',
            'citizenshipNumber' =>'required'
        ];
    }
}
