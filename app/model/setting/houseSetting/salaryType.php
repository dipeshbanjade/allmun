<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class salaryType extends Model
{
    protected $table = 'salary_types';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
