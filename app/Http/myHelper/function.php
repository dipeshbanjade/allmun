<?php

use App\model\admin\municipility\MunicipilityDetails;
use App\model\admin\citizen\CitizenInfo;
use App\model\admin\acl\Role;
use App\model\admin\municipility;
use App\model\admin\municipility\StaffMunicipility;
use App\model\admin\ward\WardStaff;
use App\model\setting\Department;
use App\model\admin\ward\WardDetails;
use App\model\setting\LeaveType;
use App\model\setting\Degination;

function getUsrlevel(){
  return Auth::user()->userLevel;
}
function getMunicipalityData(){
 $munDetails = MunicipilityDetails::latest()->first();
 return $munDetails ? $munDetails : '';
}  

function getWardDetails($id){
 return WardDetails::select('nameEng', 'nameNep')->where(['id' => $id])->first();
}

function getParentsName($id){
 return CitizenInfo::select('citizenNo', 'fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'gender', 'dobAD', 'jatjatis_id', 'religiouses_id', 'sys_languages_id', 'qualifications_id', 'occupations_id', 'maritialStauts_id','isdisabled', 'id', 'family_living_id', 'idEncrip')->where(['status' => 0, 'softDelete' => 0, 'id' => $id])->first();
}
function getCitizenDetails($id){
  return CitizenInfo::select('id', 'fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng')->where(['id'=>$id, 'status'=>0, 'softDelete'=>0])->first();
}

function getRoleName($id){
  return Role::select('roleName')->where('id', $id)->first();
}

function getMunStaff($id){
  $staffNameNDept =  StaffMunicipility::select('firstNameEng', 'firstNameNep', 'middleNameEng', 'middleNameNep', 'lastNameEng', 'lastNameNep', 'departments_id')->where(['id' => $id])->first();
  if (isset($staffNameNDept->departments_id)) {
   $deptmentName = getDepartMent($staffNameNDept->departments_id);
   $staffNameNDept->departmentNameNep = $deptmentName['nameNep'];
   $staffNameNDept->departmentNameEng = $deptmentName['nameEng'];
 }
 return $staffNameNDept;
}

function getWardStaff($id){
  $staffNameNDept = WardStaff::select('firstNameEng', 'firstNameNep', 'middleNameEng', 'middleNameNep', 'lastNameEng', 'lastNameNep', 'departments_id')->where(['id' => $id])->first();
  if (isset($staffNameNDept->departments_id)) {
   $deptmentName = getDepartMent($staffNameNDept->departments_id);
   $staffNameNDept->departmentNameNep = $deptmentName['nameNep'];
   $staffNameNDept->departmentNameEng = $deptmentName['nameEng'];
 }
 return $staffNameNDept;
}

function getDepartMent($id){
 return Department::select('nameNep', 'nameEng')->where(['id' => $id])->first();
}

function getleaveType($id){
 return LeaveType::select('nameNep', 'nameEng')->where(['id' => $id])->first();
}

function getLan(){
  return $myLan = Config::get('app.locale');
}

function getViewParentData($id, $tag){
 $relationId = DB::table('citizen_relations')->select('citizens_id', 'relationName', 'relation_id')->where(['citizens_id'=>$id, 'relationName'=>$tag])->first();
}

function moreHouseDetailsFormUpdate($compareKey, $jsonData, $compareJsonKey){
 $jsonData =  (array) $jsonData;
 if(isset($jsonData[0]) and $jsonData[0] == ""){
   return false;
 }else{
   foreach ($jsonData as $singleData) {
     try {
      $singleData =  (array) $singleData;
      if ($compareKey == $singleData[$compareJsonKey]) {
        return $singleData;
      } 
    } catch (Exception $e) {
      return false;      
    }
  }
}
}

function convertMyAge($myAge){
      // dd($myAge);
  if (isset($myAge)) {
    $currentAge = (date('Y') - date('Y',strtotime($myAge)));
    return $currentAge;
        # code...
  }else{
    return null;
  }
}

function getTableData($tableName, $key){
  $returnData =  DB::table($tableName)->where('id', $key)->first();
  return $returnData;
}

function getCitizenForSifarish(){
  $citizenInfo = CitizenInfo::select('fnameNep', 'id', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'citizenNo')->where(['softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->where('citizenNo', '<>', '')->get();
  return $citizenInfo;
}

function getRelation($id){
  return DB::table('citizen_relations')->select('relation_id')->where('citizens_id', $id)->get();
}

function getUserDetails($id){
  return DB::table('users')->select('name')->where(['id'=>$id])->first();
}
function getWardStaffNumber($id){
 return WardStaff::select('firstNameEng', 'firstNameNep', 'middleNameEng', 'middleNameNep', 'lastNameEng', 'lastNameNep')->where(['wards_id' => $id])->get();
}


function checkAttendance($attendance, $myDate){
    if ($attendance) {
      foreach ($attendance as $myAtten) {
       if ($myDate == $myAtten->myDate) {
          $myTime['minTime'] = $myAtten->MAXTIME;
          $myTime['maxTime'] = $myAtten->MAXTIME;
          $totalWrk  = (strtotime($myAtten->MAXTIME) - strtotime($myAtten->MINTIME)) / 3600;
          $myTime['totalWorking'] = $totalWrk;
          return $myTime;
       }
      }
    }
}

function getDeginationNameById($id){
  return Degination::select('nameNep', 'nameEng')->where(['id'=>$id])->first();
}
?>
