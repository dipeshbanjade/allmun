@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
   <div class="col-sm-12 col-md-12">
     <div class="card-box">
       <div class="card-head">
       	<header>
       		<i class="fa fa-edit">
       			@lang('commonField.extra.edit')
       			<h3>@lang('commonField.extra.citizen')</h3>
       		</i>
       	</header>
       </div>

       <div class="card-body">
            {!! Form::model($data, ['method' => 'POST','route' => ['citizenInfo.update', $data->id], 'files'=>true, 'name' => 'frmcitizenInfo']) !!}
                 @include('admin.citizenInfo._form')

                 <p class="pull-right">
                 	   {{ Form::submit(__('commonField.button.update'), ['class' => 'btn btn-success pull-right']) }}
                 	  <button type="button" class="btn btn-danger pull-right" onclick="history.back()">
                 	      @lang('commonField.button.back')
                 	  </button>
                 </p>
            {{ Form::close() }}
       </div>
       </div>
     </div>
@endsection
@section('customStyle')
<!-- Citizen Form CSS linking -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/citizenForm/styles.css') }}">

<!-- Citizen Form CSS linking -->
<!-- pic edit css -->
<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/citizenForm/picedit.min.css') }}">
<!-- Photo editor js -->
@endsection
@section('custom_script')
@include('admin.citizenInfo.script')
<script type="text/javascript" src="{{ asset('lib/js/picedit.js') }}"></script>
@endsection