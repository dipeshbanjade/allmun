<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class TreatmentArea extends Model
{
    protected $table = 'treatment_areas';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
