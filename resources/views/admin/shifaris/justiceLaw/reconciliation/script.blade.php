<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardreconciliation']").validate({
			rules:{
				caseName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				badiAnswer : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				pratiBadiAnswer : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				dafaNumber : {
					required : true,
					minlength : 1,
					maxlength : 30,
					number:true
				},
				badi : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				pratiBadi : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				rohabaarName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				year : {
					required : true,
					minlength : 2,
					maxlength : 2,
					number:true
				},
				month : {
					required : true,
					minlength : 1,
					maxlength : 2,
					number:true
				},
				day : {
					required : true,
					minlength : 1,
					maxlength : 2,
					number:true
				},
				eachday : {
					required : true,
					minlength : 1,
					maxlength : 7,
					number:true
				}
			},
			messages: {
				caseName : "Case Name is required",
				badiAnswer : "Badi Answer is required",
				pratiBadiAnswer : "Prati Badhi Answer is required",
				dafaNumber : "Dafa Number is required and it must be numberic",
				pratiBadi : "Prati Badi is required",
				badi : "Badi is required",
				rohabaarName : "Rohabaar is required",
				year : "Year is required and it must be numberic",
				month : "Month is required and it must be numberic",
				day : "Day is required and it must be numberic",
				eachday : "Per Day is required and it must be numberic"
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='badiAnswer']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	                  $("input[name='badi']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>