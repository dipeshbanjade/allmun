<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class verifyAnnualIncomeVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'refCode' => 'required|min:3|unique:non_profit_org_registrations',
            'issuedDate' => 'required',
            'title' => 'required',
            'personName' => 'required',
            'municipality' => 'required',
            'ward' => 'required',
            'district' => 'required',
            'former' => 'required',
            'orgAddr' => 'required',
            'orgType' => 'required',
            'orgWard' => 'required',            
            'valuationInNrs' => 'required',
            'perDollarNrs' => 'required',
            'currency_type' => 'required',
            'totalValueInDollars' => 'required',
            'authorizedPerson' => 'required'

        ];
    }
}
