<div class="right_col eng" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x eng">Verification of Annual Income</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
          @include('admin.shifaris.municipalityDetail')
          
          <div class="row top-part">
            <div class="col-md-6 ps-cn">
              <p><b class="ps">प. सं.:</b>
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
                @endif
              </p>
              <p align="left" class="star">
                <b class="cn">च. नं.:</b>
                {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
              </p>
            </div>
            <div class="col-md-6">
              <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
            </div>
          </div>
          <div class="row top-part">
            <div class="col-md-12">
              <h3 align="center"><b> Subject: Verification of Annual Income  </b> </h3>
              <input type="hidden" name="" value="प्रमाणितसिफारिसगरिएकोबारे">
              <h4 align="center"><b>To Whom It May Concern </b></h4>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 content-para">
              <p align="left">This is to certify that 
                <select onchange="changeSelect(this)" name="title">
                  <!-- <option value="" > -->
                    <option value="Mr" {{(!empty($data->title) && $data->title == "Mr" )? 'selected' : ''}}>Mr</option>
                    <option value="Mrs" {{(!empty($data->title) && $data->title == "Mrs" )? 'selected' : ''}}>Mrs</option>
                    <option value="Miss" {{(!empty($data->title) && $data->title == "Miss" )? 'selected' : ''}}>Miss</option>
                  </select>
                  {{ Form::text('personName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                  permanent resident of 
                  <b>
                    {{ Form::text('municipality', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                  </b>, Ward No 
                  <b>
                    {{ Form::text('ward', Auth::user()->wards_id , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                  </b>,
                  <b>
                    {{ Form::text('district', isset($data->district) ? $data->district : getChecker()->districts->districtNameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                    <!-- <input type="text" class="dashed-input-field" value="Ilam" placeholder="   *" required="required" name="district"> -->
                  </b> Nepal(
                  <select onchange="changeSelect(this)" name="former">
                    <option value="Former" {{(!empty($data->former) && $data->former == 'Former') ? 'selected' : ""}}>Former</option>
                    <option value="Previously designated as" {{(!empty($data->former) && $data->former == 'Previously designated as') ? 'selected' : ""}}>Previously designated as</option>
                  </select>
                  <b>
                    <!--  -->
                    {{ Form::text('orgAddr', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'orgAddr']) }}
                    <!--  -->
                  </b>
                  <b> 
                    <select onchange="changeSelect(this)" name="orgType">
                      <option value="V.D.C" {{(!empty($data->orgType) && $data->orgType == "V.D.C")? 'selected' :''}}>V.D.C</option>
                      <option value="Municipality" {{(!empty($data->orgType) && $data->orgType == "Municipality")? 'selected' :''}}>Municipality</option>
                      <option value="Sub- Metropolitian" {{(!empty($data->orgType) && $data->orgType == "Sub- Metropolitian")? 'selected' :''}}>Sub- Metropolitian</option>
                      <option value="Metropolitian" {{(!empty($data->orgType) && $data->orgType == "Metropolitian")? 'selected' :''}}> Metropolitian</option>
                    </select>
                  </b>, Ward No. 
                  {{ Form::text('orgWard', Auth::user()->wards_id , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'orgWard']) }}
                  ) has submitted an application for the verification of the Annual Income  of 
                  {{ Form::text('', 'his' , ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'mrormiss', 'readonly' => 'readonly']) }}
                  family from different sources as mentioned below.
                </p>
              </div>
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-bordered" id="annual_income_verification_table">
                    <thead>
                      <tr><th>S.N.</th>
                        <th>Source Of Income</th>
                        <th>Income Holder</th>
                        <th>Relation With Applicants</th>
                        <th>Annual Income (NPR)</th>
                        <th>Remarks</th>
                      </tr></thead>
                      <tbody>
                        <?php
                          $count =1;
                          $incomeStatement = !empty($data)? json_decode($data->incomeStatement,true) :[1];
                        ?>
                        @foreach ($incomeStatement as $singleData)
                          <tr>
                            <td>{{$count++}} </td>
                            <td>{{ Form::text('incomeSource[]', !empty($singleData['incomeSource']) ? $singleData['incomeSource'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                            <td>{{ Form::text('incomeHolderName[]', !empty($singleData['incomeHolderName']) ? $singleData['incomeHolderName'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} </td>
                            <td>{{ Form::text('relationWithSeeker[]', !empty($singleData['relationWithSeeker']) ? $singleData['relationWithSeeker'] : NULL , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</td>
                            <td>{{ Form::text('annualIncome[]', !empty($singleData['annualIncome']) ? $singleData['annualIncome'] : NULL , ['class'=>'dashed-input-field annual_income', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'annual_income']) }}</td>
                            <td>{{ Form::text('remarks[]', !empty($singleData['remarks']) ? $singleData['remarks'] : NULL , ['class'=>'dashed-input-field']) }}</td>
                            @if ($count == 2)
                              <td class="add-btns" style="border: 0px;"><a href="" id="add_more_income_source" class="btn btn-success"><span class="fa fa-plus"></span></a></td>
                            @else
                              <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
                            @endif
                          </tr>
                          {{-- expr --}}
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-md-12">
                  <p> Total Valuation of income source is NRs. 
                    {{ Form::text('valuationInNrs', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'valuationInNrs']) }}<br></p>
                    <p>Note: Today's Exchange Rate 
                      <select onchange="changeSelect(this)" name="currency_type">
                        <option value="USD($)" {{(!empty($data->currency_type) && $data->currency_type == "USD($)") ? 'selected' : ''}}>USD($)</option>
                        <option value="AUD($)" {{(!empty($data->currency_type) && $data->currency_type == "AUD($)") ? 'selected' : ''}}>AUD($)</option>
                        <option value="Pound(£)" {{(!empty($data->currency_type) && $data->currency_type == "Pound(£)") ? 'selected' : ''}}>Pound(£)</option>
                        <option value="Euro(€)" {{(!empty($data->currency_type) && $data->currency_type == "Euro(€)") ? 'selected' : ''}}>Euro(€)</option>
                      </select>
                      1 = NRs. 
                      {{ Form::text('perDollarNrs', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'per_dollar_nrs']) }}
                    </p>
                    <p> which is equivalent to 
                      {{ Form::text('totalValueInDollars', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'total_valuation_dollar']) }}
                    </p>
                    <p> (Source : Nepal Rastra Bank)</p>
                    <div class="text-right btm-last">
                      <p>
                        {{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
                      </p>
                      <p>   <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">
                        <option value="">@lang('commonField.extra.deginations_id')</option>
                        @if(count($deginationsId) > 0)  <!-- selecting leave type -->
                        @foreach($deginationsId as $deg)
                        <option value="{{ $deg->id }}" {{(!empty($data) && $data->deginations_id == $deg->id) ? 'selected' : ''}}>
                          {{ $deg->nameNep }}
                        </option>
                        @endforeach
                        @endif
                      </select>
                    </p>
                  </div>
                  <!--views for nibedak detail -->
                  <div class="clearfix"></div>
                  <hr>
                  @include('admin.shifaris.nibedakCommonField')
                  <!-- END -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>