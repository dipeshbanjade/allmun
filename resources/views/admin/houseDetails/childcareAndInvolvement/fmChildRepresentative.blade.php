<div class="form-group col-md-12">
   <div>
    <p>
        १.   तपाईको परि   बालबालिकाको प्रतिनिधित्वको अवस्था ( १२ - १८  बर्षका बालबालिका  )  
    </p>
    <div>
        <?php 
        $count = 0;
        ?>
        <table class="table fmWorkingDetailTbl" style="display: block;">
            <tr>
                <th>सि.न</th>
                <th style="width: 250px;">बालबालिकाको नाम</th>
                <th style="width: 100px;">उमेर</th>
                <th style="width: 100px;">कक्षा</th>
                <th colspan="3" style="width: 250px;">नगरपालिका वडा</th>
                <th colspan="3" style="width: 250px;">विद्यालय व्या. समिति</th>
                <th colspan="3" style="width: 250px;">स्वास्थ्य केन्द्र </th>                           
            </tr>
            <tbody>
                <?php                
                $childRepresentativeData = isset($childRep->childRepresentativeDetail) ? json_decode($childRep->childRepresentativeDetail, true) : [];
                //dd($childRepresentativeData[$count]);
                foreach($getMyFamilyDetails as $member){
                    $memberInfo = getParentsName($member->relation_id);
                    $memberAge = getAge($memberInfo['dobAD']);
                    if( isset($memberAge) && ($memberAge > 11 && $memberAge < 19)){                        
                        $count++; 
                        //$singleChildSecurity = moreHouseDetailsFormUpdate($member->relation_id, $childSecurityData, $compareJsonKey = 'childId');                        
                        ?>
                        <tr>                        
                            <td>
                             {{ $count }} 
                         </td>
                         <td>
                            {{ Form::text("childRepresentativeDetail[$count][childName]", isset($memberInfo) ? $memberInfo['fnameNep'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '', 'readonly'=>'']) }}
                        </td>
                        <td>
                            {{ Form::text("childRepresentativeDetail[$count][childAge]", $memberAge, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '', 'readonly'=>'']) }}            
                        </td>
                        <td>
                            {{ Form::text("childRepresentativeDetail[$count][class]", (array_key_exists($count, $childRepresentativeData) &&  array_key_exists('class', $childRepresentativeData[$count])) ? $childRepresentativeData[$count]['class'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '', ]) }}
                        </td>
                        <td>
                            <label class="radio-inline">
                                छ 
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][commWard]" value="2" checked="">
                            </label>
                        </td>
                        <td>
                            <label class="radio-inline">
                                छैन्
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][commWard]" value="1" {{ !empty($childRepresentativeData[$count]['commWard']) && $childRepresentativeData[$count]['commWard'] == '1' ? 'checked' : '' }}>
                            </label>
                        </td>
                        <td>
                            <label class="radio-inline">
                                थाहा छैन्
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][commWard]" value="3" {{ !empty($childRepresentativeData[$count]['commWard']) && $childRepresentativeData[$count]['commWard'] == '3' ? 'checked' : '' }}>
                            </label>
                        </td>
                        <td>
                            <label class="radio-inline">
                                छ 
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][schoolComm]" value="2" checked="">
                            </label>
                        </td>
                        <td>
                            <label class="radio-inline">
                                छैन्
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][schoolComm]" value="1" {{ !empty($childRepresentativeData[$count]['schoolComm']) && $childRepresentativeData[$count]['schoolComm'] == '1' ? 'checked' : '' }}>
                            </label>
                        </td>
                        <td>
                            <label class="radio-inline">
                                थाहा छैन्
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][schoolComm]" value="3" {{ !empty($childRepresentativeData[$count]['schoolComm']) && $childRepresentativeData[$count]['schoolComm'] == '3' ? 'checked' : '' }}>
                            </label>
                        </td>
                        <td>
                            <label class="radio-inline">
                                छ 
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][healthCenter]" value="2" checked="">
                            </label>
                        </td>
                        <td>
                            <label class="radio-inline">
                                छैन्
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][healthCenter]" value="1" {{ !empty($childRepresentativeData[$count]['healthCenter']) && $childRepresentativeData[$count]['healthCenter'] == '1' ? 'checked' : '' }}>
                            </label>
                        </td>
                        <td>
                            <label class="radio-inline">
                                थाहा छैन्
                                <input type="radio" name="childRepresentativeDetail[<?php echo $count; ?>][healthCenter]" value="3" {{ !empty($childRepresentativeData[$count]['healthCenter']) && $childRepresentativeData[$count]['healthCenter'] == '3' ? 'checked' : '' }}>
                            </label>
                        </td>
                    </tr>   
                    <?php }} //dd($childRepresentativeData);?>                             
                </tbody>                                
            </table>
        </div>
    </div>
</div>   
<div class="form-group col-md-12" style="margin-top: 50px;">
    <div>
        <p>
            २.  विगत ५ वर्समा तपाईको घरपरिवारबाट बालबालिकाहरुले घर छाडेका छन्  ? 
        </p>
    </div>
    <div>
        <label class="radio-inline">
            छ 
            <input type="radio" name="haveChildLeft" value="1" checked="">
        </label>
        <label class="radio-inline">
            छैन्
            <input type="radio" name="haveChildLeft" value="3"  {{ !empty($childRep->haveChildLeft) && $childRep->haveChildLeft == '3' ? 'checked' : '' }}>
        </label>
    </div>
    <div class="msgDisplay"></div>

</div>