<div class="row top-part">
  <div class="col-md-6">
   <p align="left">   
    <b class="ps">पत्र संख्या: </b>
    @if(isset($data->refCode))
    {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
    @else
    {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
    @endif
  </p>

  <p align="left">
    <b> च. नं.:
      {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
    </b>
  </p>
</div>
<div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    <h4 align="center">
      <b>विषय:घरबाटो प्रमाणित ।</b>
    </h4>
  </div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left"> 
      <b>श्री 
        {{ Form::text('accountHolderName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderName', 'required' => 'required']) }}
      </b>,
    </p>
    <p align="left">
      <b> 
       {{ Form::text('accountHolderAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderAddress', 'required' => 'required']) }}।
     </b>
   </p>
 </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">
    <p>
     <b> 
       {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
     </b>महा/उपमहा/नगरपालिका/गाउँपालिका वडा नं.
     <b> 
       {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
     </b>
     (साविक ठेगाना 
     <!--  -->
     {{ Form::text('sabikAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'sabikAddress', 'required' => 'required']) }}
     <!--  -->
     )    
     अन्तर्गत श्री/सुश्री/श्रीमती 
     {{ Form::text('shrestaName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'shrestaName', 'required' => 'required']) }}
     को नाममा त्यास कार्यालयमा श्रेस्ता दर्ता कायम रहेको जग्गाको घरबाटो तपशिलमा उल्लेखित विवरण अनुसार भएको व्यहोरा प्रमाणित गरिन्छ ।
   </p> 
 </div>
</div> 
<h4 align="center"><b>घरबाटोको विवरण</b> </h4>
<div class="col-md-12">
  <div class="table-responsive">
    <table class="table table-bordered" id="house_kayem_table">
      <thead>
        <tr>
          <th width="2%">क्र.स.</th>
          <th>वडा नं. </th>
          <th>सिट नं.</th>
          <th>कि.नं.</th>
          <th>क्षेत्रफल</th>
          <th>घर भएको/नभएको</th>
          <th>बटोको प्रकार</th>
          <th>कैफियत </th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $count = 1;
        $tableData = !empty($data) ? json_decode($data->tableData, true) : [1];
          //dd($tableData);
        foreach($tableData as $singleData){         
          ?>
          <tr>
            <td> {{ Form::text('', $count++, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => '', 'required' => 'required']) }}</td>
            <td> {{ Form::text('wardNo[]', !empty($singleData['wardNo']) ? $singleData['wardNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => 'wardNo', 'required' => 'required']) }}</td>
            <td> {{ Form::text('seatNo[]', !empty($singleData['seatNo']) ? $singleData['seatNo'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => 'seatNo', 'required' => 'required']) }}</td>
            <td> {{ Form::text('kittaNo[]', !empty($singleData['kittaNo']) ? $singleData['kittaNo'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'kittaNo', 'required' => 'required']) }}</td>
            <td> {{ Form::text('area[]', !empty($singleData['area']) ? $singleData['area'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => 'area', 'required' => 'required']) }}</td>
            <td> {{ Form::text('gharStatus[]', !empty($singleData['gharStatus']) ? $singleData['gharStatus'] : NULL, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'gharStatus', 'required' => 'required']) }}</td>
            <td> {{ Form::text('streetType[]', !empty($singleData['streetType']) ? $singleData['streetType'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => 'streetType', 'required' => 'required']) }}</td>
            <td>{{ Form::text('remarks[]', !empty($singleData['remarks']) ? $singleData['remarks'] : NULL, ['class'=>'dashed-input-small-field', 'placeholder'=>'    *', 'id' => 'remarks', 'required' => 'required']) }} </td>
            

            @if($count == 2)
            <td class="add-btns" style="border: 0px;"><button type="button" id="addTableRow" class="btn btn-success"><span class="fa fa-plus"></span></button></td>
            @else
            <td class='cancel-btns' style='border: 0px;'><a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a></td>
            @endif

          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="text-right btm-last">
      <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required"></p>
      <p> <b>
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </b></p>
    </div>
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr>
    @include('admin.shifaris.nibedakCommonField')

    <!-- END -->
  </div>
  <div class="col-md-12">
    <hr>
  </div>
</div>
</div>
</div>