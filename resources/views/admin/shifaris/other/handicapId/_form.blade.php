<div class="row top-part">
  <div class="col-md-6">
   <p align="left"><b class="ps">प. सं.:
     @if(isset($data->refCode))
     {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
     @else
     {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
     @endif
   </b>
 </p>

 <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}</b></p>
</div>
<div class="col-md-6">
  <p align="right"><b class="mt">मिति :
    {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
  </b>
</p>
</div>
</div>
<div class="row title-left">
 <div class="col-md-12">
  <p align="left">श्री
    <b>महिला तथा बाल विकस कार्यालय </b>
  ,</p>
  <p align="left">
    {{ Form::text('officeAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'officeAddress']) }}
    ।
  </p>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24">
    </p><h4 align="center"><b> विषय: सिफारिस सम्बन्धमा।
      <input type="hidden" name="letter_subject" value="सिफारिस सम्बन्धमा">
    </b> </h4>  
    <p></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12 content-para">

    <p>उपरोक्त सम्बन्धमा <b>
      {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}

    </b> वडा नं. <b>{{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber']) }}</b> (साबिकको ठेगाना 
    <!--  -->
    {{ Form::text('sabikAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'sabikAddress']) }} बस्ने
    <b> <select onchange="changeSelect(this)" name="patientPrefix">
      <option value="श्री"
      {{ !empty($data->patientPrefix) && $data->patientPrefix == 'श्री' ? 'selected' : '' }}>श्री</option>
      <option value="सुश्री"
      {{ !empty($data->patientPrefix) && $data->patientPrefix == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
      <option value="श्रीमती"
      {{ !empty($data->patientPrefix) && $data->patientPrefix == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
    </select>
  </b>
  {{ Form::text('applicant', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'applicant']) }} (अपाङ्गताको किसिम उल्लेख गर्ने) {{ Form::text('type', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'type']) }} अपाङ्ग भएकोले अपाङ्ग परिचय पत्र बनाउनको लागि “सिफारिस गरी गरी" भनी यस वडा कार्यालयमा पर्न आएको निवेदन सम्बन्धमा तहाँको नियमानुसार अपाङ परिचयपत्रको लागि सिफारिस गरिन्छ |</p>

</div>
</div>
<div class="text-right btm-last">
  <p>
    {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'sabikAddress']) }}
  </p>
  <p> <b> 
    <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
      @if(count($deginationsId) > 0)
      @foreach($deginationsId as $deg)
      <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
        {{ $deg->nameNep }} 
      </option>
      @endforeach
      @endif
    </select>
  </b> </p>
</div>
<!--Nibedak Block  -->
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>
