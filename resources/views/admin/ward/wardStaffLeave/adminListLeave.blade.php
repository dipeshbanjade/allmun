@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12" id="myDIV" >
  <div class="card-box">
    <div class="card-body">
      <div class="card-head">
        <header><i class="fa fa-eye"></i><span>@lang('commonField.extra.createLeave')</span></header>
      </div>
      <div class="row">
          <div class="col-md-12 col-sm-12">
              <div class="panel tab-border card-box">
                  <header class="panel-heading panel-heading-gray custom-tab tab-head">
                      <ul class="nav nav-tabs">
                          <li class="nav-item"><a href="#home" data-toggle="tab" class="active">@lang('commonField.extra.approved')</a>
                          </li>
                          <li class="nav-item"><a href="#about" data-toggle="tab">@lang('commonField.extra.pending')</a>
                          </li>
                      </ul>
                  </header>
                  <div class="panel-body">
                      <div class="tab-content">
                          <div class="tab-pane active" id="home">
                              <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="withOutcitizen">
                                <thead>
                                  <tr>
                                    <th width="5%">@lang('commonField.extra.sn')</th>
                                    <th width="5%">@lang('commonField.extra.house')</th>
                                    <th width="7%">@lang('commonField.personal_information.image')</th>
                                    <th width="5%">@lang('commonField.extra.citizenNo')</th>
                                    <th>@lang('commonField.personal_information.fullName')</th>
                                    <th>@lang('commonField.extra.fatherName')</th>
                                    <th>@lang('commonField.personal_information.phoneNumber')</th>
                                    <th width="25%"> @lang('commonField.extra.action') </th>
                                  </tr>
                                </thead>
                                    <tbody>
                                       <tr>
                                          <td>asdlkfjlsadkf</td>
                                          <td>asdlkfjlsadkf</td>
                                          <td>asdlkfjlsadkf</td>
                                          <td>asdlkfjlsadkf</td>
                                          <td>asdlkfjlsadkf</td>
                                          <td>asdlkfjlsadkf</td>
                                          <td>asdlkfjlsadkf</td>
                                          <td>asdlkfjlsadkf</td>
                                       </tr>
                                    </tbody>
                                  </table>
                          </div>
                          <div class="tab-pane" id="about">
                             <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle"  id="example4">
                               <thead>
                                 <tr>
                                   <th>@lang('commonField.extra.sn')</th>
                                   <th>@lang('commonField.extra.department')</th>
                                   <th>@lang('commonField.personal_information.staffName')</th>
                                   <th>@lang('commonField.extra.leaveType')</th>
                                   <th>@lang('commonField.extra.startDate')</th>
                                   <th>@lang('commonField.extra.noOfDays')</th>
                                   <th>@lang('commonField.extra.appliedOn')</th>
                                   <th>@lang('commonField.extra.desc')</th>
                                   <th>@lang('commonField.extra.action')</th>
                                 </tr>
                               </thead>
                               <tbody>
                                 <?php 
                                    $count = 1;  
                                    $lang  = getLan();
                                 ?>
                                 @if(count($allLeave) > 0)
                                    @foreach($allLeave as $leave)
                                       <?php  
                                          $leaveName = getleaveType($leave->leave_types_id);   
                                          $lvName    = $leaveName['nameEng'] .'-'. $leaveName['nameNep']; 
                                          $staffName = getWardStaff($leave->users_id);
                                          $stName    = $staffName['firstNameEng'] . $staffName['middleNameEng'] . $staffName['lastNameEng'];
                                   
                                       ?>
                                       <tr class="odd gradeX">
                                         <td>{{ $count ++ }}</td>
                                         <td>
                                           @if(isset($staffName->departments_id))
                                              {{ $lang == 'np' ? $staffName->departmentNameNep : $staffName->departmentNameEng }}
                                           @endif
                                         </td>

                                         <td>{{ $lang == 'np' ? ucfirst($staffName->firstNameNep) . ' ' . $staffName->middleNameNep . ' '. $staffName->lastNameNep : ucfirst($staffName->firstNameEng) . ' ' . $staffName->middleNameEng . ' '. $staffName->lastNameEng  }}</td>
                                         <td>{{ $lang == 'np' ? $leaveName->nameNep : $leaveName->nameEng  }}</td>
                                         <td >{{ $leave->startDate }} to {{ $leave->endDate  }}</td>
                                         <td >{{ $leave->noOfDays }} days</td>
                                         <td>{{ $leave->created_at->diffForHumans() }}</td>
                                         <td title="{!! $leave->shortNoteEng !!}">
                                             <?php
                                                 $strLimit = str_limit($leave->shortNoteEng, 100);
                                             ?>
                                            {!! $strLimit !!}
                                         </td>
                                         <td>
                                               <span>
                                                  <a href="#" data-url="{!! route('wardStaffLeave.changeStatus') !!}"  data-status = "{{ $leave->approvedBy == '' ? 0 : 1 }}" data-id = "{{ $leave->id }}" class="changeStatus">
                                                  <button class="<?php echo !$leave->approvedBy=='' ? 'btn btn-success btn-sm': 'btn btn-danger btn-sm'   ?>">
                                                    <?php 
                                                    echo  !$leave->approvedBy== '' ? 'Approve' : 'Pending';    
                                                    ?>
                                                 </button>
                                                   </a>
                                               </span>
                                         </td>
                                        
                                         </tr>
                                    @endforeach
                                 @endif
                                 
                                 </tbody>
                               </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection