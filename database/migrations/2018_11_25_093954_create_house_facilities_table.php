<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseFacilitiesTable extends Migration
{
    public function up()
    {
        Schema::create('house_facilities', function (Blueprint $table) {
            $table->increments('id', 100);
            
            $table->integer('createdBy')->unsigned()->nullable();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->integer('house_details_id')->nullable()->unsigned();
            $table->foreign('house_details_id')->references('id')->on('house_details')->onDelete('cascade');
            $table->text('deviceName')->nullable();

            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('house_facilities');
    }
}
