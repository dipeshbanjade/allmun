<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectricitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electricities', function (Blueprint $table) {
            $table->increments('id',100);
            $table->string('refCode',100)->unique();
            $table->string('nameNep')->nullable();
            $table->string('nameEng')->nullable();
            $table->boolean('status')->default(0);
            $table->integer('sortColumn')->nullable();
            $table->boolean('softDelete')->default(0);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electricities');
    }
}
