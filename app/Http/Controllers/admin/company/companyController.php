<?php

namespace App\Http\Controllers\admin\company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\company\Company;
use App\model\myLogic;
use Auth;
// use App\Http\Requests\admin\setting\branch\branchDetailsVal;
use Validator;
class companyController extends Controller
{
    protected $viewPath  = 'admin.company';
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
      $page['page_title']       = 'Company Details';
      $page['page_description'] = 'all Company details';
      
      if (Auth::user()->userLevel == 'mun') {
          $companies = Company::where(['status' => 0, 'softDelete' => 0, 'municipilities_id', '<>', ''])->paginate(config('activityMessage.pagination'));
         return view($this->viewPath . '.index', compact(['page', 'companies'])); 
      }

      $companies = Company::where(['status' => 0, 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->paginate(config('activityMessage.pagination'));
      return view($this->viewPath . '.index', compact(['page', 'companies'])); 

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
         'nameEng'         => 'required|min:4|max:200'
         ]);

        if ($validator->fails()) {
            return back()->withMessage($errors);
        }
        /*-------------------------------------*/
        $mySave = Company::create([
           'nameEng'       => $request->nameEng,
           'nameNep'       => $request->nameNep,
           'phoneNumber' => $request->phoneNumber,
           'landlineNumber' => $request->landlineNumber,
           'email'          => $request->email,
           'addr'           => $request->addr,
           'companyDetails'  => $request->companyDetails,
           'municipilities_id' => Auth::user()->userLevel == 'mun' ? Auth::user()->municipilities_id : NULL,
           'wards_id'          => Auth::user()->userLevel == 'wrd' ? Auth::user()->wards_id : NULL,
           'status'           => 0,
           'softDelete'        => 0,
           'users_id'          => Auth::user()->id
       ]);
      if ($mySave) {
        return back()->withMessage(config('activityMessage.saveMessage'));
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Company::findOrFail($id);
        if (!$data) { abort (404); }
           return response()->json([
           'id'            => $data->id,
           'nameEng'          => $data->nameEng,
           'nameNep'          => $data->nameNep,
           'phoneNumber'   => $data->phoneNumber,
           'landlineNumber' => $data->landlineNumber,
           'email'          => $data->email,
           'addr'           => $data->addr,
           'companyDetails'  => $data->companyDetails
       ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
         'nameEng'         => 'required|min:4|max:200'
         ]);

        if ($validator->fails()) {
            return back()->withMessage($errors);
        }
        $update          = Company::findOrFail($id);
        $update->nameEng     = $request->nameEng;
        $update->nameNep     = $request->nameNep;
        $update->phoneNumber = $request->phoneNumber;
        $update->landlineNumber = $request->landlineNumber;
        $update->email          = $request->email;
        $update->addr          = $request->addr;
        $update->companyDetails          = $request->companyDetails;
        $update->users_id          = Auth::user()->id;


        $myUpdate = $update->update();
        if ($myUpdate) {
          return response()->json([
            'success' => true,
            'message' => config('activityMessage.updateMessage')
            ]);
      }else{
          return response()->json([
            'success'  => false,
            'message'  => config('activityMessage.notUpdated')
            ]);
      }         
    }

    public function destroy($id)
    {
        $del = Company::findOrFail($id);
        $del->softDelete = 1;
        $myDel = $del->update();
        if ($myDel) {
         return MyLogic::successChangeStatus($code = 200);
         }else{
          return MyLogic::successChangeStatus($code = 301);
        }
    }
}
