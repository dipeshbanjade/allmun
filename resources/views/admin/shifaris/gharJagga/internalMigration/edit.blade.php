@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="card-box col-sm-12 col-md-12">
	<div class="card-body">
		<i class="fa fa-plus"></i>
	</div>

	<div class="card-head">
		<header>Internal Migration</header>
		{{-- <header>@lang('sidebarMenu.staff_setting.createStaff')</header> --}}
	</div>
	<div class="right_col nep" role="main" style="min-height: 570px;">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">

					<div class="x_title">
						<h3 class="title-x">आन्तरिक बसाई सराई </h3>

						<div class="clearfix"></div>
					</div>
					<div class="x_content" id="block">
						<div id="printingDiv">
							@include('admin.shifaris.municipalityDetail')
							{!! Form::model($data, ['method' => 'POST','route' => ['internalMigration.store', $data->id], 'files'=>true, 'name' => 'formInternalMigrationEdit', 'class'=>'sifarishSubmitForm']) !!}						
							{{ Form::hidden('shifarisId',  $data->id )}}

							@include('admin.shifaris.gharJagga.internalMigration._form')
						</div>
					</div>
					
					@include('admin.shifaris.commonButton')
					{{ Form::close() }}
					<div class="msgDisplay" style="z-index: 999999 !important"></div>

					<!-- <input type="hidden" id="getValue" value="<?php 
						// echo asset('lib/css/sifarish.css');
					?>"	> -->
				</div>
			</div>
		</div>
	</div>

	@endsection
	@section('customStyle')
	<!-- Shifarish CSS linking -->
	<link rel="stylesheet" type="text/css" href="{{ asset('lib/css/sifarish.css') }}" media="print,screen">
	{!! Html::style('lib/css/jquery.mCustomScrollbar.min.css') !!}

	<!-- Shifarish CSS linking -->
	@endsection
	@section('custom_script')
	@include('admin.shifaris.script')
	@include('admin.shifaris.gharJagga.internalMigration.shifarisScript')
	@endsection