<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class hsOrgTaxPay extends Model
{
    protected $table = 'hs_org_tax_pays';
}
