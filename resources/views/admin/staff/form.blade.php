 <div class="wizard-header text-center">
      <h3 class="wizard-title">Create your profile</h3>
      <p class="category">This information will let us know more about you.</p>
    </div>

    <div class="wizard-navigation col-lg-6 p-t-20" style="margin: 0 auto;">
      <div class="progress-with-circle">
       <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
     </div>
     <ul>
      <li>
        <a href="#about" data-toggle="tab">
          <div class="icon-circle">
            <i class="ti-user"></i>
          </div>
          Normal
        </a>
      </li>
      <li>
        <a href="#account" data-toggle="tab">
          <div class="icon-circle">
            <i class="ti-settings"></i>
          </div>
          Details
        </a>
      </li>
      
    </ul>
  </div>
  <div class="tab-content">
    <div class="tab-pane" id="about">
      <div class="row">
        <div class="card-body row">
          <div class="col-lg-12 p-t-20"> 
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
              {{ Form::text('staffCode', null, ['class'=>'mdl-textfield__input col-sm-4', 'placeholder'=>'']) }}

              <label class = "mdl-textfield__label" >Staff Code</label>
            </div>
          </div>
          <div class="col-lg-6 p-t-20"> 
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
             {{ Form::text('firstNameNep', null, ['class'=>'UnicodeKeyboardTextarea mdl-textfield__input', 'placeholder'=>'']) }}
             <label class = "mdl-textfield__label" > firstname Nepali</label>

           </div>
         </div>
         <div class="col-lg-6 p-t-20"> 
          <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('firstNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}

            <label class = "mdl-textfield__label">First Name</label>

          </div>
        </div>
        <div class="col-lg-6 p-t-20"> 
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('middleNameNep', null, ['class'=>'UnicodeKeyboardTextarea mdl-textfield__input', 'placeholder'=>'']) }}

            <label class = "mdl-textfield__label" >Middle Name Nepali</label>
          </div>
        </div>
        <div class="col-lg-6 p-t-20"> 
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('middleNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}

            <label class = "mdl-textfield__label" >Middle Name</label>
          </div>
        </div>
        <div class="col-lg-6 p-t-20"> 
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('lastNameNep', null, ['class'=>'UnicodeKeyboardTextarea mdl-textfield__input', 'placeholder'=>'']) }}

            <label class = "mdl-textfield__label" >Last Name Nepali</label>
          </div>
        </div>
        <div class="col-lg-6 p-t-20"> 
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('lastNameEng', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}

            <label class = "mdl-textfield__label" >Last Name</label>
          </div>
        </div>
        <div class="col-lg-6 p-t-20"> 
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('username', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
            <label class = "mdl-textfield__label" >Username</label>
          </div>
        </div>
        <div class="col-lg-6 p-t-20"> 
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('password', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}

            <label class = "mdl-textfield__label" >Password</label>
          </div>
        </div>
        <div class="col-lg-6 p-t-20"> 
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
            {{ Form::text('confirmPassword', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
            <label class = "mdl-textfield__label" >Confirm Password</label>
          </div>
        </div>
        <div class="col-lg-6 p-t-20"> 
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
           {{ Form::text('email', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
           <label class = "mdl-textfield__label" >Email</label>
           <span class="mdl-textfield__error">Enter Valid Email Address!</span>
         </div>
       </div>
       <div class="col-lg-6 p-t-20"> 
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
          <label >Role</label>
          <select name="roles_id" class="form-control">
            <option>super</option>
            <option>normal</option>
          </select>
        </div>
      </div> 
    </div>                                      
  </div>
</div>

<div class="tab-pane" id="account">
  <h5 class="info-text"> Details</h5>
  <div class="row">
   <div class="card-body row">
    
    
     
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        {{ Form::text('citizenNo', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'']) }}

        <label class = "mdl-textfield__label" >Citizen No:</label>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label >Municipality</label>
        <select name="municipality_id" class="form-control">
          <option>kathmandu</option>
          <option>dharan</option>
        </select>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label >Ward</label>
        <select name="wards_id" class="form-control">
          <option>1</option>
          <option>2</option>
        </select>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label >Staff Type</label>
        <select name="staff_types_id" class="form-control">
          <option>1</option>
          <option>2</option>
        </select>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label >Designation</label>
        <select name="designation_id" class="form-control">
          <option>staff</option>
          <option>demo</option>
        </select>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label >Provision</label>
        <select name="provisions_id" class="form-control">
          <option>staff</option>
          <option>demo</option>
        </select>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        <label >District</label>
        <select name="districts_id" class="form-control">
          <option>dharan</option>
          <option>demo</option>
        </select>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('joiningDate', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'', 'id'=>'dateOfBirth']) }}
        
        <label class = "mdl-textfield__label" >Joining Date</label>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
        {{ Form::text('wardNo', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'']) }}

        <label class = "mdl-textfield__label" >Ward No:</label>
      </div>
    </div>
    <div class="col-lg-6 p-t-20"> 
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
       {{ Form::text('email', null, ['class'=>'mdl-textfield__input', 'placeholder'=>'']) }}
       <label class = "mdl-textfield__label" >Email</label>
       <span class="mdl-textfield__error">Enter Valid Email Address!</span>
     </div>
   </div>
   <div class="col-lg-6 p-t-20"> 
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
      {{ Form::text('villageNep', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'']) }}

      <label class = "mdl-textfield__label" >Village Nepali</label>
    </div>
  </div>
  <div class="col-lg-6 p-t-20"> 
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
      {{ Form::text('villageEng', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'']) }}

      <label class = "mdl-textfield__label" >Village </label>
    </div>
  </div>
  <div class="col-lg-6 p-t-20">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
      {{ Form::text('phoneNumber', null, ['class'=>'mdl-textfield__input ', 'placeholder'=>'']) }}
      
      <label class = "mdl-textfield__label" for = "text5">Phone Number</label>
      
      <span class="mdl-textfield__error">Number required!</span>
    </div>                         
  </div>
</div>
</div>
</div>

</div>
<div class="wizard-footer">
  <div class="pull-right">
    <input type='button' class='btn btn-next btn-fill btn-warning btn-wd' name='next' value='Next' />
    <input type='button' class='btn btn-finish btn-fill btn-warning btn-wd' name='finish' value='Finish' />
  </div>

  <div class="pull-left">
    <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
  </div>
  <div class="clearfix"></div>
</div>