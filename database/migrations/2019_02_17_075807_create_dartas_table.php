<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDartasTable extends Migration
{
    public function up()
    {
        Schema::create('dartas', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->integer('municipilities_id')->nullable()->unsigned();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade');

            $table->integer('wards_id')->nullable()->unsigned();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade');

            $table->string('idEncript');

            $table->integer('users_id')->nullable()->unsigned();
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('dartaNo')->nullable();
            $table->string('dartaDate')->nullable();
            $table->string('dartaPage')->nullable();
            $table->string('dartaPageDate')->nullable();

            $table->integer('other_offices_id')->nullable()->unsigned();
            $table->foreign('other_offices_id')->references('id')->on('other_offices')->onDelete('cascade');

            $table->string('dartaSubject')->nullable();
            $table->integer('dartaIsFor')->nullable()->unsigned();
            $table->foreign('dartaIsFor')->references('id')->on('users')->onDelete('cascade');
            $table->string('dartaReceiveDate')->nullable();

            $table->text('dartaKaifiyat')->nullable();
            $table->string('dartaStorePlace')->nullable();
            $table->string('dartaReciverDate')->nullable();
            $table->string('imagePath')->nullable();

            $table->integer('departments_id')->unsigned();
            $table->foreign('departments_id')->references('id')->on('departments')->onDelete('cascade');

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('dartas');
    }
}
