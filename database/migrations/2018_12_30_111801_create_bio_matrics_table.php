<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBioMatricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bio_matrics', function (Blueprint $table) {
            $table->increments('id', 100);
            $table->string('tnxId', 50)->nullable();
            $table->string('dvcId', 50)->nullable();
            $table->string('dvcIp', 255)->nullable();
            $table->string('punchId', 255)->nullable();
            $table->string('tnxDateTime', 255)->nullable();
            $table->string('mode', 10)->nullable();

            $table->boolean('status')->default(0);
            $table->boolean('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bio_matrics');
    }
}
