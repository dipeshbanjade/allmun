<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १.तपाईको परिवारमा रहेका विद्यालय भर्ना नभएका बालबालिका संख्या?
                    </p>
                </div>
                <br>
                <table border="1">
                    <header>
                       <b>आधारभुत उमेर समूह (५–१२ बर्ष)</b> 
                   </header>
                   <thead>
                    <tr>
                        <th>विवरण</th>
                        <th>बालक</th>
                        <th>बालिका</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>                                      
                        <td>
                            सामान्य शारीरिक अवस्था
                        </td>
                        <td>
                            {{ Form::text('primaryHealthyMale', isset($hsSUData->primaryHealthyMale) ? $hsSUData->primaryHealthyMale : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                        </td>
                        <td>
                            {{ Form::text('primaryHealthyFemale',  isset($hsSUData->primaryHealthyFemale) ? $hsSUData->primaryHealthyFemale : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                        </td>
                    </tr> 
                    <tr>                                      
                        <td>
                            असामान्य शारीरिक अवस्था
                        </td>
                        <td>
                            {{ Form::text('primaryUnhealthyMale',  isset($hsSUData->primaryUnhealthyMale) ? $hsSUData->primaryUnhealthyMale : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                        </td>
                        <td>
                            {{ Form::text('primaryUnhealthyFemale',  isset($hsSUData->primaryUnhealthyFemale) ? $hsSUData->primaryUnhealthyFemale : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                        </td>
                    </tr>            
                </tbody>
            </table>
            <br>
            <table border="1">
                <header>
                   <b>माध्यमिक उमेर समूह (१३–१८ बर्ष)</b> 
               </header>
               <thead>
                <tr>
                    <th>विवरण</th>
                    <th>बालक</th>
                    <th>बालिका</th>
                </tr>
            </thead>
            <tbody>
                <tr>                                      
                    <td>
                        सामान्य शारीरिक अवस्था
                    </td>
                    <td>
                        {{ Form::text('secondaryHealthyMale',  isset($hsSUData->secondaryHealthyMale) ? $hsSUData->secondaryHealthyMale : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                    </td>
                    <td>
                        {{ Form::text('secondaryHealthyFemale',  isset($hsSUData->secondaryHealthyFemale) ? $hsSUData->secondaryHealthyFemale : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                    </td>
                </tr> 
                <tr>                                      
                    <td>
                        असामान्य शारीरिक अवस्था 
                    </td>
                    <td>
                        {{ Form::text('secondaryUnhealthyMale',  isset($hsSUData->secondaryUnhealthyMale) ? $hsSUData->secondaryUnhealthyMale : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                    </td>
                    <td>
                        {{ Form::text('secondaryUnhealthyFemale',  isset($hsSUData->secondaryUnhealthyFemale) ? $hsSUData->secondaryUnhealthyFemale : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Name', 'id' => '']) }}
                    </td>
                </tr>                              
            </tbody>
        </table>
    </div>        
</div>
            <div class="msgDisplay"></div>

</div>
</div>