<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class FamilyType extends Model
{
  protected $table = 'family_types';
  protected $fillable = ['refCode', 'nameNep','nameEng', 'status', 'softDelete'];

}
