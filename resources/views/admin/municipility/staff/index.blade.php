@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
  <div class="card-box">
      <div class="card-body">
             <a href="{{ route('admin.munStaff.create') }}" class="btn btn-primary pull-right">
                <i class="fa fa-plus"></i>
                @lang('commonField.button.createStaff')
             </a>
      <div class="table-scrollable">
              @if(count($allStaff) > 0)
              <label><h4>@lang('commonField.extra.NoOfStaff') : <strong>{{ count($allStaff) }}</strong></h4></label>
                  <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle" style="color:black;" id="example4" role="grid" aria-describedby="example4_info">
                    <thead>
                      <tr>
                        <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 5px;">@lang('commonField.extra.sn')</th>


                        <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 40px;">@lang('commonField.personal_information.image')</th>

                        <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">@lang('commonField.personal_information.staffName')</th>

                        <!--  -->
                        <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">@lang('commonField.extra.staff') / @lang('commonField.extra.department')</th>
                        <!--  -->

                        <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 30px;">@lang('commonField.personal_information.phoneNumber')</th>

                        <th class="sorting_asc" tabindex="0" aria-controls="example4" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 30px;">@lang('commonField.employer.biomatricIdNo')</th>

                        <th width="15%"> @lang('commonField.extra.action') </th>
                      </tr>
                    </thead>
                    <tbody>
                      @php  $count = 1   @endphp
                          @foreach($allStaff as $staff)
                              <tr class="odd gradeX">
                                <td>{{ $count ++ }}</td>
                                <td><img src="{{ asset($staff->profilePic) }}" width="40" height="40"></td>
                                <td>
                                    <a href="{{ route('admin.munStaff.show', $staff->idEncrip) }}"> 

                                    {{ $lang == 'np' ? $staff->firstNameNep .' '. $staff->middleNameNep .''. $staff->lastNameNep : $staff->firstNameEng .' '. $staff->middleNameEng .''. $staff->lastNameEng }}  
                                    </a>
                                  </td>

                                <td>
                                    @if($staff->departments_id)
                                    {{ $lang == 'np' ? $staff->departments->nameNep  : $staff->departments->nameEng  }}
                                    <small>
                                         {{ $staff->staff_types_id ?  $staff->staff_types->nameEng : '' }}
                                    </small>
                                    @endif
                                </td>

                                <td>{{ $staff->phoneNumber }}</td>
                                <td>{{ $staff->pounchId }}</td>

                                <td>
                                  
                                  <a href="{{ route('admin.munStaff.edit', $staff->idEncrip) }}" class="btn btn-primary btn-xs">
                                    <i class="fa fa-pencil"></i>
                                  </a>
                                  <!--  -->
                                  <a href="{{ route('adminChangePwd', $staff->idEncrip) }}" class="btn btn-primary btn-xs" title="change password">
                                      <i class="fa fa-lock"></i>
                                  </a>

                                  <a href="#" class="deleteMe" id="" data-url="{{ route('admin.munStaff.delete', $staff->id )}}" data-name="{{ $staff->firstNameEng}}"><button class="btn btn-danger btn-xs">
                                    <i class="fa fa-trash-o "></i></button></a>

                                </td>
                              </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{ $allStaff->links() }}
                 @else
                 <h3 align="center">
                     @lang('commonField.extra.pleaseInsertData');
                 </h3>
              @endif
             
                </div>
              </div>
  </div>
</div>
@endsection