<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <div class="row color_red">
            <div class="form-group col-md-12">
                <div>
                    <p>
                        १. विगत तीन बर्ष भित्र तपाईको घर परिवारमा कसैको विवाह भएको छ ? (वि.सं.२०७१ साल देखि २०७४ साल सम्म) 
                    </p>
                    <div>
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" onchange="displayMarriageDetailTbl(this)" name="haveMarriageInFive" value="1"  {{ isset($hsMD) && $hsMD->haveMarriageInFive == 1 ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" onchange="displayMarriageDetailTbl(this)" name="haveMarriageInFive" value="0" {{ isset($hsMD) && $hsMD->haveMarriageInFive == 0 ? 'checked' : '' }}>
                            यदि विवाह भएको भए तलको विवरण उल्लेख गर्नुहोस् ।  
                        </label>
                    </div>
                </div>
                <br>
                @if (isset($hsMD) && $hsMD->haveMarriageInFive == 1)
                <table class="table marriageDetailTbl" style="display: block;">
                    @else
                    <table class="table marriageDetailTbl" style="display: none;">
                        @endif

                        <tr>
                            <th>सि.न.</th>
                            <th>विवाहित</th>
                            <th>विवाहित व्यक्तिको नाम</th>
                            <th>लिङ्ग </th>
                            <th>विवाह भएको साल </th>
                            <th>विवाह हुँदाको उमेर</th>
                            <th style="border: 0px;">
                                <div id="addNewRowInTable" class="btn btn-success" style="padding:2px;">
                                    <i class="fa fa-plus"></i>
                                </div>
                            </th>
                        </tr>
                        <tbody id="addMarriageInfo">
                            <tr>
                                <?php 
                                $count = 0;
                                $marrigeDetailInfo = isset($hsMD->marriageDetail) ? json_decode($hsMD->marriageDetail) : '';
                                $arrayCount = 0;
                                $extOthMember = [];
                                
                                if(!empty($marrigeDetailInfo)){
                                    foreach($marrigeDetailInfo as $eachDetail){
                                        $eachDetail = (array) $eachDetail;
                                        if(isset($eachDetail['othMember']) && $eachDetail['othMember'] == '1'){
                                            $extOthMember[] = $eachDetail; 
                                        }
                                    }
                                }

                                foreach($getMyFamilyDetails as $member){
                                    $memberInfo = getParentsName($member->relation_id);
                                    $memberAge = getAge($memberInfo['dobAD']);

                                    $count++;                                   
                                    $singleMarriageDetail = moreHouseDetailsFormUpdate($member->relation_id, $marrigeDetailInfo, $compareJsonKey = 'memberId');
                                    ?>
                                    <td>
                                        {{ $count }}
                                    </td>
                                    <td>
                                        <input type="checkbox" class="isMarried" onchange="requireMarriageDetail(this)" name="marriageDetail[<?php echo $count ?>][selectMarriageDetail]" value="1" {{ isset($singleMarriageDetail['selectMarriageDetail']) ? 'checked' : '' }} >
                                    </td>
                                    <td>
                                        <input type="hidden" name="marriageDetail[<?php echo $count ?>][memberId]" value="{{ $member->relation_id }}">

                                        {{ Form::text("marriageDetail[$count][fname]", $memberInfo['fnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => ''], ['readonly']) }}
                                    </td>
                                    <td>
                                        {{ Form::text("marriageDetail[$count][gender]", $memberInfo['gender'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => ''],['readonly']) }}
                                    </td>

                                    <td>
                                        {{ Form::number("marriageDetail[$count][marriedDate]", isset($singleMarriageDetail['marriedDate']) ? $singleMarriageDetail['marriedDate'] : '', ['class'=>'mdl-textfield__input dashed-input-field marriageDate', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>
                                    <td>
                                        {{ Form::number("marriageDetail[$count][ageAtMarriage]",  isset($singleMarriageDetail['ageAtMarriage']) ? $singleMarriageDetail['ageAtMarriage'] : '', ['class'=>'mdl-textfield__input dashed-input-field marriageAge', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>
                                </tr>
                                <?php } ?>

                                <?php 
                                foreach($extOthMember as $memberData){
                                    $singleMarriageDetail = $memberData;
                                    //dd($singleMarriageDetail);
                                    ?>

                                    <td>
                                        {{ ++$count }}
                                    </td>
                                    <td>
                                        <input type="checkbox" class="isMarried" onchange="requireMarriageDetail(this)" name="marriageDetail[<?php echo $count ?>][selectMarriageDetail]" value="1" {{ isset($singleMarriageDetail['selectMarriageDetail']) ? 'checked' : '' }} >
                                    </td>
                                    <td>
                                        <input type="hidden" name="marriageDetail[<?php echo $count ?>][othMember]" value="1">

                                        {{ Form::text("marriageDetail[$count][fnameNep]", $singleMarriageDetail['fnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>
                                    <td>
                                        {{ Form::text("marriageDetail[$count][gender]", $singleMarriageDetail['gender'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>

                                    <td>
                                        {{ Form::number("marriageDetail[$count][marriedDate]", isset($singleMarriageDetail['marriedDate']) ? $singleMarriageDetail['marriedDate'] : '', ['class'=>'mdl-textfield__input dashed-input-field marriageDate', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>
                                    <td>
                                        {{ Form::number("marriageDetail[$count][ageAtMarriage]",  isset($singleMarriageDetail['ageAtMarriage']) ? $singleMarriageDetail['ageAtMarriage'] : '', ['class'=>'mdl-textfield__input dashed-input-field marriageAge', 'placeholder'=>'Number', 'id' => '']) }}
                                    </td>
                                </tr>
                                <?php

                            }
                            ?>                            
                        </tbody>
                    </table>
                    <br>
                </div>     

            </div>
        </div>
        <div class="msgDisplay"></div>
    </div>

    <script type="text/javascript">
       var addRowBtn = document.getElementById('addNewRowInTable');
       var othMemCount = 2222;

       addRowBtn.addEventListener('click', function(){
        othMemCount++;
        var no = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="">';

        var hiddenName = '<input type="hidden" name="marriageDetail[' + othMemCount + '][othMember]" value="1">';

        var tickMarried = '<input type="checkbox" class="isMarried" onchange="requireMarriageDetail(this)" name="marriageDetail[' + othMemCount + '][selectMarriageDetail]" value="1"  checked="">';

        var fnameNep = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="marriageDetail['+ othMemCount +'][fnameNep]">';
        var gender = '<input type="text" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="marriageDetail['+ othMemCount +'][gender]">';
        var marriedDate = '<input type="number" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="marriageDetail['+ othMemCount +'][marriedDate]">';
        var ageAtMarriage = '<input type="number" class="mdl-textfield__input dashed-input-field" placeholder="   *" name="marriageDetail['+ othMemCount +'][ageAtMarriage]">';

        var cancelButton = '<div class="btn btn-danger btnRemoveItem"><i class="fa fa-minus"></i></div>';

        var insertNewRow = "<tr class='tblRow'>"
        +"<td>"+no+"</td>"            
        +"<td>"+hiddenName+tickMarried+"</td>"
        +"<td>"+fnameNep+"</td>"
        +"<td>"+gender+"</td>"
        +"<td>"+marriedDate+"</td>"
        +"<td>"+ageAtMarriage+"</td>"
        +"<td>"+cancelButton+"</td>"
        +"</tr>";

        $('#addMarriageInfo').append(insertNewRow);                        
        $('.btnRemoveItem').on('click', function(e){
            $(this).parent().parent().remove();
            return false;
        });
    });
</script>