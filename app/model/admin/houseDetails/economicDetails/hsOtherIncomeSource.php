<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class hsOtherIncomeSource extends Model
{
   protected $table = 'hs_other_income_sources';
}
