<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
{!! Html::style('lib/fonts/simple-line-icons/simple-line-icons.min.css') !!}
{!! Html::style('lib/fonts/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('lib/fonts/material-design-icons/material-icon.css') !!}
{!! Html::style('lib/bootstrap/bootstrap.min.css') !!}
{!! Html::style('lib/summernote/summernote.css') !!}
{!! Html::style('lib/material/material.min.css') !!}
{!! Html::style('lib/css/material_style.css') !!}
{!! Html::style('lib/pages/inbox.min.css') !!}
{!! Html::style('lib/css/theme_style.css') !!}
{!! Html::style('lib/css/plugins.min.css') !!}
{!! Html::style('lib/css/style.css') !!}
{!! Html::style('lib/css/responsive.css') !!}
{!! Html::style('lib/css/theme-color.css') !!}
{!! Html::style('lib/css/dataTables.bootstrap4.min.css') !!}
{!! Html::style('lib/img/favicon.ico') !!}
{!! Html::style('nepalidatepicker/nepali.datepicker.v2.2.min.css') !!}
{!! Html::style('lib/fonts/kalimati Regular.tff') !!} 
 @yield('customStyle')
