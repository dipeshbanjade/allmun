<?php

namespace App\Http\Requests\admin\leaveIsFor;

use Illuminate\Foundation\Http\FormRequest;

class leaveIsForVal extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
                'dateFromEng' => 'required',
                'dateFromNep'       => 'required',
                'leaveFor'         => 'required'
        ];
    }
}
