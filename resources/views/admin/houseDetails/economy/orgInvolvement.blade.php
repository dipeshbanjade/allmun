<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
                <div class="form-group col-md-12">
                <?php
                  $orgInvolvement = $orgInv ? $orgInv : '';
                  $jsonOrgName    = !empty($orgInvolvement->orgInvName) ? json_decode($orgInvolvement->orgInvName) : '';
                ?>
                    <p>
                       १९. तपाईको परिवारको कुनै सदस्य कुनै संघ संस्था तथा राजनैतिक क्रियाकलापमा संलग्न भएका छन् ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline">
                            १. छ  
                            <input type="radio" name="haveOrgInv" value="1" class="rdoClickMe" {{ !empty($orgInvolvement->orgInvName) && $orgInv->haveOrgInv == 1 ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                           २. छैन  
                            <input type="radio" name="haveOrgInv" value="0" class="rdoClickMe" {{ !empty($orgInvolvement->orgInvName) && $orgInv->haveOrgInv == 0 ? 'checked' : '' }}>
                        </label>
                        <label>
                           यदि छ भने कुन कुन संघ संस्था तथा राजनैतिक क्रियाकलापमा संलग्न भएका छन् ? (बहुउत्तर सम्भव छ)
                        </label>
                        <!--  -->
                        <label class="radio-inline">
                            १. स्थानीय तह
                            <input type="checkbox" name="orgInvName[]" value="स्थानीय तह" 
                            {{ !empty($orgInvolvement->orgInvName) && in_array('स्थानीय तह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                           २. स्थानीय योजना तर्जुमा वा कार्यान्वयन समिति
                            <input type="checkbox" name="orgInvName[]" value="स्थानीय योजना तर्जुमा वा कार्यान्वयन समिति" {{ !empty($orgInvolvement->orgInvName) && in_array('स्थानीय योजना तर्जुमा वा कार्यान्वयन समिति', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            ३. स्थानीय स्वास्थ्य संस्था व्यवस्थापन समिति
                            <input type="checkbox" name="orgInvName[]" value="" {{ !empty($orgInvolvement->orgInvName) && in_array('स्थानीय स्वास्थ्य संस्था व्यवस्थापन समिति', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            ४. विद्यालय व्यवस्थापन समिति
                            <input type="checkbox" name="orgInvName[]" value="स्थानीय स्वास्थ्य संस्था व्यवस्थापन समिति" {{ !empty($orgInvolvement->orgInvName) && in_array('विद्यालय व्यवस्थापन समिति', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            ५. कृषक समूह 
                            <input type="checkbox" name="orgInvName[]" value="कृषक समूह" {{ !empty($orgInvolvement->orgInvName) && in_array('कृषक समूह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            ६. वन उपभोक्ता समूह
                            <input type="checkbox" name="orgInvName[]" value="वन उपभोक्ता समूह" {{ !empty($orgInvolvement->orgInvName) && in_array('वन उपभोक्ता समूह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            ७. सहकारी संस्था
                            <input type="checkbox" name="orgInvName[]" value="सहकारी संस्था" {{ !empty($orgInvolvement->orgInvName) && in_array('सहकारी संस्था', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                           ८. बचत तथा ऋण समूह
                            <input type="checkbox" name="orgInvName[]" value="बचत तथा ऋण समूह" {{ !empty($orgInvolvement->orgInvName) && in_array('बचत तथा ऋण समूह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            ९. आमा / महिला समूह
                            <input type="checkbox" name="orgInvName[]" value="आमा / महिला समूह" {{ !empty($orgInvolvement->orgInvName) && in_array('आमा / महिला समूह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            १०. खानेपानी उपभोक्ता समूह
                            <input type="checkbox" name="orgInvName[]" value="खानेपानी उपभोक्ता समूह" {{ !empty($orgInvolvement->orgInvName) && in_array('खानेपानी उपभोक्ता समूह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            ११. सिंचाई / जल उपभोक्ता समूह
                            <input type="checkbox" name="orgInvName[]" value="सिंचाई / जल उपभोक्ता समूह" {{ !empty($orgInvolvement->orgInvName) && in_array('सिंचाई / जल उपभोक्ता समूह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            १२. आयआर्जन समूह
                            <input type="checkbox" name="orgInvName[]" value="आयआर्जन समूह" {{ !empty($orgInvolvement->orgInvName) && in_array('आयआर्जन समूह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            १३. जातिगत समूह
                            <input type="checkbox" name="orgInvName[]" value=" जातिगत समूह" {{ !empty($orgInvolvement->orgInvName) && in_array('जातिगत समूह', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                            १४. स्थानीय बाल समूह / बालक्लव
                            <input type="checkbox" name="orgInvName[]" value="स्थानीय बाल समूह / बालक्लव" {{ !empty($orgInvolvement->orgInvName) && in_array('स्थानीय बाल समूह / बालक्लव', $jsonOrgName) ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline">
                             {{ Form::text('otherOrgName', isset($orgInv->otherOrgName) ? $orgInv->otherOrgName : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
                        </label>
                    </div>
                </div>
            </div>
            <div class="msgDisplay"></div>
            
    </div>
</div>