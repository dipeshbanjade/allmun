<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumerCommittesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumer_committes', function (Blueprint $table) {
            $table->increments('id', 100);

                        $table->string('refCode', 100)->unique();

            $table->integer('wards_id')->unsigned()->nullable();
            $table->foreign('wards_id')->references('id')->on('wards')->onDelete('cascade'); 

            $table->integer('municipilities_id')->unsigned()->nullable();
            $table->foreign('municipilities_id')->references('id')->on('municipilities')->onDelete('cascade'); 

            $table->date('issuedDate');
            $table->string('chalaniNum');
            $table->string('orgName');
            $table->string('orgLocation');
            $table->string('fiscalYear');
            $table->string('municipalityName');
            $table->string('wardNum');
            $table->string('organizationAddress');
            $table->enum('organizationType', ['गा.वि.स.', 'नगरपालिका', 'उप महानगरपालिका', 'महानगरपालिका']);
            $table->string('organizationWard');
            $table->string('businessName');
            $table->string('projectAmount');
            $table->text('meetingStatement');

            $table->string('authorizedPerson');
            
            $table->integer('deginations_id')->unsigned();
            $table->foreign('deginations_id')->references('id')->on('deginations')->onDelete('cascade'); 

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');

            $table->integer('applicant_users_id')->unsigned();
            $table->foreign('applicant_users_id')->references('id')->on('citizen_infos')->onDelete('cascade');             
            $table->string('status')->default(0);
            $table->string('softDelete')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumer_committes');
    }
}
