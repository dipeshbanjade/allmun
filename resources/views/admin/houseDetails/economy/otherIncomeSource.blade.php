<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
                <div class="form-group col-md-12">
                <?php
                     // data come from controller to display added data in form
                     $otherIncomeMachaPal = isset($agriOtherIncome->machaPal) ? json_decode($agriOtherIncome->machaPal) : '';
                     $otherIncomeMauriPal = isset($agriOtherIncome->mauriPal) ? json_decode($agriOtherIncome->mauriPal) : '';
                     $otherIncomeFulKheti = isset($agriOtherIncome->fulKheti) ? json_decode($agriOtherIncome->fulKheti) : '';
                ?>
                    <p>
                      ६. तपाईको परिवारमा माछापालन, मौरीपालन र फूल खेती गरिएको छ भने विवरण दिनुहोस् ? 
                    </p>
                    <table class="table">
                            <tr>
                                  <th>क्र.सं</th>
                                  <th></th>
                                  <th>खेती</th>
                                  <th>विवरण</th>
                            </tr>
                            <tr>
                                <td>१.</td>
                                <td>
                                    <input type="checkbox" name="machaPal[haveMachaPal]" value="1" {{ isset($otherIncomeMachaPal->haveMachaPal) && $otherIncomeMachaPal->haveMachaPal == 1  ? 'checked'  : '' }}>
                                </td>
                                <td>
                                   माछापालन
                                </td>
                                <td>
                                <div class="form-row margin-btm">
                                    <div class="col">
                                        <label class="">
                                            माछापालन पोखरी संख्याः
                                        </label>
                                        {{ Form::text("machaPal[poundNo]", isset($otherIncomeMachaPal->poundNo) ? $otherIncomeMachaPal->poundNo : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'माछापालन पोखरी संख्या']) }}                                                
                                    </div>
                                    <div class="col">
                                        <label class="">
                                            पोखरीको क्षेत्रफल रोपनीः
                                        </label>
                                        {{ Form::text("machaPal[area]", isset($otherIncomeMachaPal->area) ? $otherIncomeMachaPal->area :'', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'पोखरीको क्षेत्रफल']) }}
                                    </div>
                                    <div class="col">
                                        <label class="">
                                           बार्षिक उत्पादनः
                                        </label>
                                        {{ Form::text("machaPal[anualIncome]", isset($otherIncomeMachaPal->anualIncome) ? $otherIncomeMachaPal->anualIncome :'', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'पोखरीको क्षेत्रफल']) }}
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>१.</td>
                                <td>
                                    <input type="checkbox" name="mauriPal[haveMauriPal]" value="1" {{ isset($otherIncomeMauriPal->haveMauriPal) && $otherIncomeMauriPal->haveMauriPal == 1 ? 'checked' : ''  }}>
                                </td>
                                <td>
                                   मौरीपालन
                                </td>
                                <td>
                                    <div class="form-row margin-btm">
                                            <div class="col">
                                                <label class="">
                                                    मौरी घार संख्याः
                                                </label>
                                                {{ Form::text('mauriPal[number]', isset($otherIncomeMauriPal->number) ? $otherIncomeMauriPal->number : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'मौरी घार संख्या']) }}                                                
                                            </div>
                                            <div class="col">
                                                <label class="">
                                                    बार्षिक मह उत्पादनः
                                                </label>
                                                {{ Form::text('mauriPal[production]', isset($otherIncomeMauriPal->production) ? $otherIncomeMauriPal->production : '' , ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'बार्षिक मह उत्पादन']) }}
                                            </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>१.</td>
                                <td>
                                    <input type="checkbox" name="fulKheti[haveFulKheti]" value="1" {{ isset($otherIncomeFulKheti->haveFulKheti) &&  $otherIncomeFulKheti->haveFulKheti == 1 ? 'checked' : '' }}>
                                </td>
                                <td>
                                   फूल खेती
                                </td>
                                <td>
                                    <div class="form-row margin-btm">
                                            <div class="col">
                                                <label class="">
                                                    फूल खेती गरेको जग्गाको क्षेत्रफल:
                                                </label>
                                                {{ Form::text('fulKheti[area]', isset($otherIncomeFulKheti->area) ? $otherIncomeFulKheti->area : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'जग्गाको क्षेत्रफल']) }}                                                
                                            </div>
                                            <div class="col">
                                                <label class="">
                                                    वार्षिक आम्दानी ः
                                                </label>
                                                {{ Form::text('fulKheti[production]', isset($otherIncomeFulKheti->production) ? $otherIncomeFulKheti->production : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'वार्षिक आम्दानी']) }}
                                            </div>
                                    </div>
                                </td>
                            </tr>
                </table>
                </div>
            </div>
            <div class="msgDisplay"></div>
            
    </div>
</div>
@section('custom_css')
<style>
.table tr td label{
    float:left;
}
.margin-btm{
    margin-bottom:10px !important;
}
</style>
@endsection