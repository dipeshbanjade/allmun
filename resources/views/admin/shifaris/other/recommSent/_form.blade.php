<div class="right_col nep" role="main" style="min-height: 521px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">खुलाई पठाएको</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">


          @include('admin.shifaris.municipalityDetail')

          <div class="row top-part">
            <div class="col-md-6">
              <p align="left">
               <b>@lang('commonField.extra.refCode'):</b>
               @if(isset($data->refCode))
               {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
               @else
               {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
               @endif
             </p>
             <p align="left">
               <b> च. नं.:
                 {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
               </b>
             </p>
           </div>
           <div class="col-md-6">
            <p align="right">
              <b class="mt">मिति :
                {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issued_date',  'onfocus' => 'showNdpCalendarBox("issued_date")']) }}
              </b>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <p align="center" class="font-size-24">
            </p><h4 align="center"><b>विषय: खुलाई पठाएको।</b> </h4>
            {{ Form::hidden('letter_subject', 'खुलाई पठाएको।', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
            <p></p>
          </div>
        </div>
        <div class="row title-left">
          <div class="col-md-12">
           <p>श्री
             {{ Form::text('companyHead', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'companyHead', 'required' => 'required']) }}
             कार्यालय,
           </p>
           <p>
            {{ Form::text('companyAddress', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'companyAddress', 'required' => 'required']) }}
            ।</p>
          </div>
        </div>
        <div class="row">
         <div class="col-md-12 content-para">
          <p>
            तहाँ सम्मानित अदालतको मिति  
            {{ Form::text('mailDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'letter_date',  'onfocus' => 'showNdpCalendarBox("letter_date")']) }}
            च.न. 
            {{ Form::text('mailNumber', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'mailNumber', 'required' => 'required']) }}
            को पत्रानुसार यस वडा कार्यालयबाट प्राविधिक मूल्याङ्कन गरी यसै पत्रसाथ कार्यरत प्राविधिकको सक्कल प्रतिवेदन संग्लन राखी पठाइएको व्यहोरा अनुरोध छ।
          </p> 
        </div>
        <div class="col-md-12">
        </div>
        <div class="col-md-12">
         <div class="text-right btm-last">
          <p>
            {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field star', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }} 
            <p>
             <b>
              <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
                @if(count($deginationsId) > 0)
                @foreach($deginationsId as $deg)
                <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
                  {{ $deg->nameNep }} 
                </option>
                @endforeach
                @endif
              </select>
            </b> 
          </p>
        </div>
        <!--views for nibedak detail -->
        <div class="clearfix"></div>
        <hr>
        @include('admin.shifaris.nibedakCommonField')
        <!-- END -->
        <div class="col-md-12">
          <hr>
        </div>
      </div>
    </div>
  </div>
</div>