<div class="right_col eng" role="main" style="min-height: 570px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x eng">Unmarried Verification</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
          @include('admin.shifaris.municipalityDetail')
          
          <div class="row top-part">
            <div class="col-md-6 ps-cn">
              <p><b class="ps">प. सं.:</b>
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
                @endif
              </p>
              <p align="left" class="star">
                <b class="cn">च. नं.:</b>
                {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
              </p>
            </div>
            <div class="col-md-6">
              <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
            </div>
          </div>
          <div class="row top-part">
            <div class="col-md-12">
             <h3 align="center"><b>  Subject:Unmarried Verification  </b></h3>
             {{ Form::hidden('letterSub', 'Unmarried Verification') }}
             <h4 align="center"><b> To Whom It May Concern </b></h4>
           </div>
         </div>
         <div class="row">
           <div class="col-md-12 content-para">
            <p align="left">This is to certify that <select onchange="changeSelect(this)" name="appTitleOpt">
              <option value="Mr" {{!empty($data->appTitleOpt) && $data->appTitleOpt == 'Mr' ? 'selected' : '' }}>Mr</option>
              <option value="Mrs" {{!empty($data->appTitleOpt) && $data->appTitleOpt == 'Mrs' ? 'selected' : '' }}>Mrs</option>
              <option value="Miss" {{!empty($data->appTitleOpt) && $data->appTitleOpt == 'Miss' ? 'selected' : '' }}>Miss</option>
            </select>
            {{ Form::text('appName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}
            <select onchange="changeSelect(this)" name="appGenderOpt">
              <option value="son" {{!empty($data->appGenderOpt) && $data->appGenderOpt == 'son' ? 'selected' : '' }}>son</option>
              <option value="daughter" {{!empty($data->appGenderOpt) && $data->appGenderOpt == 'daughter' ? 'selected' : '' }}>daughter</option>
            </select>
            of Mr.<b> {{ Form::text('fatherName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b> and Mrs.<b>{{ Form::text('motherName', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>  having citizenship no. <b>{{ Form::text('citizenNo', null, ['class'=>'dashed-input-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>,
            permanent resident of <b>{{ Form::text('municipality', isset($data->municipality) ? $data->municipality : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b>, Ward No <b>{{ Form::text('ward', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder' => '   *  ', 'required' => 'required']) }}</b>,<b>{{ Form::text('district', isset($data->district) ? $data->district : getChecker()->districts->districtNameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b> Nepal(
            <select onchange="changeSelect(this)" name="former">
              <option value="Former"  {{!empty($data->former) && $data->former == 'Former' ? 'selected' : '' }}>Former</option>
              <option value="Previously designated as" {{!empty($data->former) && $data->former == 'Previously designated as' ? 'selected' : '' }}>Previously designated as</option>
            </select>
            <!--  -->
            {{ Form::text('orgAddr', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'orgAddr']) }}
            <!--  -->
            <b> <select onchange="changeSelect(this)" name="orgType">
              <option value="V.D.C"  {{!empty($data->orgType) && $data->orgType == 'V.D.C' ? 'selected' : '' }}>V.D.C</option>
              <option value="Municipality" {{!empty($data->orgType) && $data->orgType == 'Municipality' ? 'selected' : '' }}>Municipality</option>
              <option value="Sub- Metropolitian" {{!empty($data->orgType) && $data->orgType == 'Sub- Metropolitian' ? 'selected' : '' }}>Sub- Metropolitian</option>
              <option value="Metropolitian" {{!empty($data->orgType) && $data->orgType == 'Metropolitian' ? 'selected' : '' }}> Metropolitian</option>
            </select>
          </b>, Ward No. {{ Form::text('orgWard', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} )
          has submitted an application letter for marital status Certificate at this Municipality, and according to witnesses at ward level, 
          <select onchange="changeSelect(this)" name="appRelation">
            <option value="he" {{!empty($data->appRelation) && $data->appRelation == 'he' ? 'selected' : '' }}>he</option>
            <option value="she" {{!empty($data->appRelation) && $data->appRelation == 'she' ? 'selected' : '' }}>she</option>
          </select>
          has been found to be single in marital status till <b>
            {{ Form::text('dateInAD', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'dateInAD', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("dateInAD")']) }}</b>A.D.
          </p>
        </div>
      </div>
      <div class="col-md-12">
        <div class="text-right btm-last">
          <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
          <p>   <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">
            <option value="">@lang('commonField.extra.deginations_id')</option>
            @if(count($deginationsId) > 0)  <!-- selecting leave type -->
            @foreach($deginationsId as $deg)
            <option value="{{ $deg->id }}" {{!empty($data) && $data->deginations_id == $deg->id ? 'selected' :''}}>
              {{ $deg->nameNep }}
            </option>
            @endforeach
            @endif
          </select>
        </p>
      </div>
      <!--views for nibedak detail -->
      <div class="clearfix"></div>
      <hr>
      @include('admin.shifaris.nibedakCommonField')

      <!-- END -->
    </div>
  </div>

</div>
</div>
</div>
</div>
</div>