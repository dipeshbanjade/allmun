<script type="text/javascript">
	var btnAddBodartha = $('#add');
	var clickCount = 1;
	btnAddBodartha.on('click', function(e){
		e.preventDefault();		
		clickCount++;
		var no = '<input type="text" class="dashed-input-small-field" placeholder="   *" required="required" name="" value="'+clickCount+'">';
		var name = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="name[]">';
		var post = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="post[]">';
		var remark = '<input type="text" class="dashed-input-field" name="remark[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+no+"</td>"
		+"<td>"+name+"</td>"
		+"<td>"+post+"</td>"
		+"<td>"+remark+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#accountTable').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});

	$(function(){
		/*add buttoin*/
		$("form[name='frmWardaccountNavigate']").validate({
			rules:{
				refCode: {
					required: true,
					minlength: 3,
					maxlength: 20
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum: {
					required: true,
					minlength: 3,
					maxlength: 30					
				},
				organizationName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				organizationLocation : {
					required : true,
					minlength: 3,
					maxlength: 30
				},
				municipalityName : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				wardNum : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				purpose: {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				year : {
					required : true,
					minlength : 4,
					maxlength : 4,
					number: true
				},
				interest : {
					required : true,
					minlength : 1,
					maxlength : 3
				},
				accountNum : {
					required : true,
					minlength: 7,
					maxlength: 20,
					number: true
				},
				name : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				post : {
					required : true,
					minlength : 5,
					maxlength : 30
				},
				authorizedPerson : {
					required : true,
					minlength: 3,
					maxlength: 20
				}
			},
			messages: {
				refCode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNum : "Chalani Number is required",
				organizationName : "Organization Name Name is required",
				organizationLocation : "Organization Location is required",
				municipalityName : "Municipality Name is required",
				wardNum : "Ward Number is required and must be numeric",
				purpose : "Propose for shifaris is required",
				accountNum : "Account Number is required",
				year : "Year is required",
				interest : "Interest is required",
				name : "Person Name is required",
				post : "Person Post is required",
				authorizedPerson : "Authorizing person is required"
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='name[]']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>