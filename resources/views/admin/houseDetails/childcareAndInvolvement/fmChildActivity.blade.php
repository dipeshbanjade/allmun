<div class="form-row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
            <form action="" class="gharBibaran">
                <div class="row color_red">
                        <div class="form-group col-md-12">
                                <div>
                                    <p>
                                        १. तपाईको परिवारका निम्न निकायहरुमा बालबालिकाको प्रतिनिधित्वको अवस्था (१२–१८ बर्षका बालबालिका) 
                                    </p>
                                </div>
                                <br>
                                <table class="table">
                                        <tr>
                                            <th rowspan="2">सि.न</th>
                                            <th rowspan="2">बालबालिकाको नाम</th>
                                            <th rowspan="2">उमेर</th>
                                            <th rowspan="2">कक्षा</th>
                                            <th colspan="3">नगरपालिका⁄वडा</th>
                                            <th colspan="3">विद्यालय व्या.समिति  </th>
                                            <th colspan="3">स्वास्थ्य केन्द्र</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                १) छ                  
                                            </td>
                                            <td>
                                                २) छैन
                                            </td>
                                            <td>
                                                ३) थाहा छैन
                                            </td>
                                            <td>
                                                १) छ
                                            </td>
                                            <td>
                                                २) छैन
                                            </td>
                                            <td>
                                                 ३) थाहा छैन
                                            </td>
                                            <td>
                                                १) छ
                                            </td>
                                            <td>
                                                २) छैन
                                            </td>
                                            <td>
                                                 ३) थाहा छैन
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                {{ Form::text('number', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '', 'required' => 'required']) }}
                                            </td>
                                            <td>
                                                {{ Form::text('number', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '', 'required' => 'required']) }}
                                            </td>
                                            <td>
                                                {{ Form::text('number', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Number', 'id' => '', 'required' => 'required']) }}
                                            </td>
                                            <td>
                                                <input type="radio" name="municipality" value="">
                                            </td>
                                            <td>
                                                <input type="radio" name="municipality" value="">
                                            </td>
                                            <td>
                                                <input type="radio" name="municipality" value="">
                                            </td>
                                            <td>
                                                <input type="radio" name="school" value="">
                                            </td>
                                            <td>
                                                <input type="radio" name="school" value="">
                                            </td>
                                            <td>
                                                <input type="radio" name="school" value="">
                                            </td>
                                            <td>
                                                <input type="radio" name="healthpost" value="">
                                            </td>
                                            <td>
                                                <input type="radio" name="healthpost" value="">
                                            </td>
                                            <td>
                                                <input type="radio" name="healthpost" value="">
                                            </td>
                                        </tr>
                                </table>
                                <br>
                        </div>     
                        
                </div>
            </form>
        </div>
        <div class="msgDisplay"></div>
    </div>