<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class businessRegistrationVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:business_registration_certificate',
            'issuedDate' => 'required',
            'chalaniNumber' => 'required|unique:business_registration_certificate',
            'registrationNumber' => 'required|numeric',
            'registrationDate' => 'required',
            'businessFarmName' => 'required',
            'businessOwnerName' => 'required',
            'houseOwnerName' => 'required',
            'businessLocation' => 'required',
            'businessStreetName' => 'required',
            'businessHouseNumber' => 'required',
            'businessEmail' => 'required',
            'businessPhoneNumber' => 'required',
            'businessPanNumber' => 'required|numeric',
            'organizationName' => 'required',
            'organizationRegisterNumber' => 'required',
            'organizationRegisterDate' => 'required',
            'courtName' => 'required',
            'authorizedPerson' => 'required'

        ];
    }
}
