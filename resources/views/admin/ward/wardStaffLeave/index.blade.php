@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12" id="myDIV">
  <div class="card-box">
    <div class="card-body ">
     <a href="{{ route('wardStaffLeave.create') }}" class="btn btn-primary pull-right">
      <i class="fa fa-plus">&nbsp;@lang('commonField.extra.createLeave')</i>
    </a>

    <div class="card-head">
      <header><i class="fa fa-eye"></i><span>@lang('commonField.extra.createLeave')</span></header>
    </div>
    <div class="table-scrollable" style="color:black;">
      <table class="table table-striped table-bordered table-hover table-checkable order-column valign-middle"  id="example4">
        <thead>
          <tr>
            <th>@lang('commonField.extra.sn')</th>
            <th>@lang('commonField.extra.leaveType')</th>
            <th>@lang('commonField.extra.startDate')</th>
            <th>@lang('commonField.extra.noOfDays')</th>
            <th>@lang('commonField.extra.appliedOn')</th>
            <th>@lang('commonField.extra.desc')</th>
            <th>@lang('commonField.extra.approvedBy')</th>
            <th>@lang('commonField.extra.action')</th>
          </tr>
        </thead>
        <tbody>
         <?php  
             $count = 1;   
             $lang = getLan();
         ?>
         @if(count($wardStaffLeave) > 0)
            @foreach($wardStaffLeave as $leave)
               <?php  
                  $leaveName = getleaveType($leave->leave_types_id);   
                  $lvName    = $leaveName['nameEng'] .'-'. $leaveName['nameNep']; 
               ?>
               <tr class="odd gradeX">
                 <td>{{ $count ++ }}</td>
                 <td>{{ $lang == 'np' ? $leaveName->nameNep : $leaveName->nameEng }}</td>
                 <td >{{ $leave->startDate }} to {{ $leave->endDate  }}</td>
                 <td >{{ $leave->noOfDays }} days</td>
                 <td>{{ $leave->created_at->diffForHumans() }}</td>
                 <td title="{!! $leave->shortNoteEng !!}">
                     <?php
                         $strLimit = str_limit($leave->shortNoteEng, 100);
                     ?>
                    {!! $strLimit !!}
                 </td>
                 <td>
                    <button class="{{ $leave->approvedBy ? 'btn btn-success' : 'btn-danger' }} btn-sm">
                       @if(isset($leave->approvedBy))
                          <?php
                             $approvedBy = getWardStaff($leave->approvedBy);
                           ?>
                       @endif
                        {{ 

                            isset($leave->approvedBy) ? $lang == 'np' ? ucfirst($approvedBy->firstNameNep) . ' '. $approvedBy->middleNameNep . ' ' . $approvedBy->lastNameNep   : ucfirst($approvedBy->firstNameEng) . ' '. $approvedBy->middleNameEng . ' ' . $approvedBy->lastNameEng  : 'Pending' 
                        }}
                    </button>
                 </td>

                 <td>
                   @if($leave->approvedBy == '')
                   <a href="{{ route('wardStaffLeave.edit', $leave->id) }}" class="btn btn-primary btn-xs">
                     <i class="fa fa-pencil"></i>
                   </a>

                    <a href="#"  class="deleteMe" data-url="{!! route('wardStaffLeave.delete', $leave->id ) !!}" data-name="{{ $leave->startDate }}">
                       <button class="btn btn-danger btn-xs">
                         <i class="fa fa-trash-o "></i></button>
                    </a>
                    @endif
                     
                   </td>
                 </tr>
            @endforeach
         @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection