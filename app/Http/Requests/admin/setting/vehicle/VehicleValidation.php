<?php

namespace App\Http\Requests\admin\setting\vehicle;

use Illuminate\Foundation\Http\FormRequest;

class VehicleValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'refCode' => 'required|min:4|max:50|unique:vehicles',
            'nameEng' => 'required|min:4:min:50'            
        ];
    }
}
