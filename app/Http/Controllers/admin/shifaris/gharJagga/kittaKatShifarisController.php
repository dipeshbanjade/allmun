<?php

namespace App\Http\Controllers\admin\shifaris\gharJagga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\KittaKatShifaris;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\kittaKatShifarisVal;
use DB;
use Auth;

use App\model\mylogic;

class kittaKatShifarisController extends Controller
{

    protected $viewPath = 'admin.shifaris.gharJagga.kittaKatShifaris';
    protected $shifarisTableName     = 'kitta_kat_shifaris';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;
    protected $refCode;

    public function __construct(){
        $this->middleware('auth');
        $this->prefix   = config('activityMessage.prefixKittaKatShifaris');
        $this->refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['page_title']       = 'Kitta Kat Shifarish';
        $page['page_description'] = 'Kitta Kat Shifarish';


            $instRecomd = KittaKatShifaris::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $page['page_title']       = 'Kitta Kat Shifarish';
        $page['page_description'] = 'Kitta Kat Shifarish';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
            $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 

            return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(kittaKatShifarisVal $request)
    {
        
            DB::beginTransaction();
            try {
                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $this->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);


                $save = new KittaKatShifaris;

                $save->createdBy = Auth::user()->id;

                $save->refCode = $request->refCode;

                $save->wards_id = Auth::user()->wards_id ?? NULL;

                $save->municipilities_id = getMunicipalityData()['id'] ?? "";

                $save->issuedDate = $request->issuedDate;
                $save->chalaniNumber = $request->chalaniNumber;
                $save->officeAddress = $request->officeAddress;
                $save->wardNumber = $request->wardNumber;
                $save->address = $request->address;
                $save->seatNumber = $request->seatNumber;
                $save->kittaNumber = $request->kittaNumber;
                $save->area = $request->area;
                $save->mohiName = $request->mohiName;
                $save->landOwnerName = $request->landOwnerName;
                $save->sarjaminDate = $request->sarjaminDate;
                $save->authorizedPerson = $request->authorizedPerson;

                $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

                $mySave = $save->save();

                $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
$request->chalaniNum = $request->chalaniNumber;
                            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                    DB::commit();
                    return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
                }else{
                    return back()->withMessage(config('activityMessage.unSaveMessage'));
                }
            }catch (Exception $e) {
                DB::rollback();
                return back()->withMessage(config('activityMessage.dataNotInserted'));
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
