<?php

namespace App\model\admin\houseDetails\healthCleaness;

use Illuminate\Database\Eloquent\Model;

class HsHandWashProcess extends Model
{
    protected $table = 'hs_hand_wash_processes';
}
