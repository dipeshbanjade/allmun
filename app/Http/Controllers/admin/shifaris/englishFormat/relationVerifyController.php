<?php

namespace App\Http\Controllers\admin\shifaris\englishFormat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\RelationVerify;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\relationVerifyVal;
use DB;
use Auth;

use App\model\mylogic;
class RelationVerifyController extends Controller
{
    protected $viewPath = 'admin.shifaris.englishFormat.relationVerify';
    protected $shifarisTableName     = 'relation_verifies';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixRelationVerify');

    }

    public function index()
    {
        $page['page_title']       = 'Relationship Verification';
        $page['page_description'] = 'Relationship Verification';

        $instRecomd = RelationVerify::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        
    }

    public function create()
    {
        $page['page_title']       = 'Relationship Verification : create';
        $page['page_description'] = 'Relationship Verification details';

        $getUniqueCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
        return view($this->viewPath . '.create', compact(['page', 'getUniqueCode', 'deginationsId']));

    }

    public function store(relationVerifyVal $request)
    {
        
        DB::beginTransaction();
        try {
            $relativeVerifyCount = count($request->relativeTitle);

            $relativeTitle = $request->relativeTitle;
            $relativeName = $request->relativeName;
            $relativeRelation = $request->relativeRelation;
            $relativeVerifyArray = [];

            for ($i=0; $i < $relativeVerifyCount; $i++) {
                array_push($relativeVerifyArray, [
                    'relativeTitle' => $relativeTitle[$i],
                    'relativeName' => $relativeName[$i],
                    'relativeRelation' => $relativeRelation[$i]
                ]);
            }

            $relativeVerifyJson = json_encode($relativeVerifyArray);

            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

            $isShifarisExist = isset($request->shifarisId) ? RelationVerify::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? RelationVerify::findOrfail($isShifarisExist['id']) : new RelationVerify;

            $save->createdBy = Auth::user()->id;
            
            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";
            $save->issuedDate  = $request->issuedDate;            
            $save->chalaniNum  = $request->chalaniNum;            
            $save->title = $request->title;
            $save->name = $request->name;
            $save->municipalityName = $request->municipalityName;
            $save->wardNumber = $request->wardNumber;
            $save->districtName = $request->districtName;
            $save->former = $request->former;
            $save->preVDCName = $request->preVDCName;
            $save->municipalityType = $request->municipalityType;
            $save->preWardNumber = $request->preWardNumber;
            $save->titleType = $request->titleType;

            $save->relationVerifyDetail = $relativeVerifyJson;

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;
            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

                        $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
                DB::commit();
                return response()->json([
                    'success' => true,
                    'message' => config('activityMessage.saveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => config('activityMessage.unSaveMessage')
                ]);
                // return back()->withMessage(config('activityMessage.unSaveMessage'));
            }
        }catch (Exception $e) {
          DB::rollback();
          return back()->withMessage(config('activityMessage.dataNotInserted'));
      }
      

  }

  public function show($id)
  {
        //
  }

  public function edit($id)
  {
        //
  }

  public function update(Request $request, $id)
  {
        //
  }

  public function destroy($id)
  {
        //
  }

  public function changeStatus(){

  }
}
