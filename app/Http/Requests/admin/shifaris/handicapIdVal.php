<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class handicapIdVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'issuedDate' => 'required',
            'chalaniNumber' => 'required',
            'officeAdddfress' => 'required',
            'wardNumber' => 'required',
            'sabikAddress' => 'required',
            'applicant' => 'required',
            'type' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
