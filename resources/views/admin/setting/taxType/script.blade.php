<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmedittaxType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of Tax Type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmtaxType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of Tax Type is required"
            }
        });
	var btnAddTaxType = $('.btnAddTaxType');
	btnAddTaxType.on('click', function(e){
		$('#taxTypeModel').modal('show'); 
	});

	var btnEdittaxType = $('.btnEdittaxType');
	btnEdittaxType.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.taxTypeId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editTaxTypeModel").modal('show');
			}
		})
	});

	var frmUpdatetaxType = $('#frmUpdatetaxType');
  frmUpdatetaxType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.taxTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/taxType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editTaxTypeModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editTaxTypeModel').modal('hide');
});
});
</script>