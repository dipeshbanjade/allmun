<div class="row top-part">
  <div class="col-md-6">
   <p align="left">
     <b>पत्र संख्य
      @if(isset($data->refCode))
      {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
      @else
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
      @endif
     </b>
     
   </p>
   <p align="left">
     <b> च. नं.:
       {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}
     </b>
   </p>
 </div>
 <div class="col-md-6">
  <p align="right">
    <b class="mt">मिति :
      {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
    </b>
  </p>
</div>
</div>
<div class="row title-left">
  <div class="col-md-12">
    <p align="left">
     <b>श्री 
       {{ Form::text('accountHolderName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderName', 'required' => 'required']) }}
     </b>,
   </p>
   <p align="left">
     <b>
       {{ Form::text('accountHolderAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'accountHolderAddress', 'required' => 'required']) }}।
     </b>
   </p>
 </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center" class="font-size-24"></p>
    
    <h4 align="center">
      <b>विषय: सिफारिस सम्बन्धमा।</b>
    </h4>
    {{ Form::hidden('letter_subject', 'खाता बन्द गरिदिने सम्बन्धमा', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}
  </div>
</div>
 


<div class="row">
  <div class="col-md-12 content-para">
    <p>उपरोक्त सम्बन्धमा 
      <b>
       {{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'municipalityName', 'required' => 'required']) }}
     </b> 
    महा/उपमाहा/नगरपालिका/गाउँपालिका वडा नं. 
    <!--  -->
    {{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'wardNumber', 'required' => 'required']) }}
    <!--  -->)
    स्थित श्री/सुश्री/श्रीमती 
    <!--  -->
    {{ Form::text('houseOwnerName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'houseOwnerName', 'required' => 'required']) }}
    <!--  -->
    को नाममा रहेको कि. नं. 
    <!--  -->
    {{ Form::text('kittaNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'kittaNumber', 'required' => 'required']) }}
    <!--  -->
     क्षे. फ
      {{ Form::text('area', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'area', 'required' => 'required']) }}
    मा बनेको धर धत्कीई पाताल भएकोले सोही अनुसारको सिफरिस गरी पाऊँ भनी यस वडा कार्यालयमा पर्न आएको निवेदन सम्बन्धमा उल्लेखित कि. नं. मा बनेको घर पाताल भएको व्यहोराको सिफारिस गरिन्छ । 
    
  </p> 
  
</div>
</div>
<div class="text-right btm-last">
  <p><input type="text" class="dashed-input-field star" placeholder="   *" name="authorizedPerson" id="" required="required" value="{{!empty($data)? $data->authorizedPerson : '' }}"></p>
  <p> 
    <b>
      <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
        @if(count($deginationsId) > 0)
        @foreach($deginationsId as $deg)
        <option value="{{ $deg->id }}" {{ isset($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
          {{ $deg->nameNep }} 
        </option>
        @endforeach
        @endif
      </select>
    </b>
  </p>
</div>
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>