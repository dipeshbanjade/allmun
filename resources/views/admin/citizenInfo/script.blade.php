<script type="text/javascript">
$(function() {
     // var grandFatherId   =  $('#grndFtherId');
     // grandFatherId.on('keypress', function(e){
     //   var grdFtherId  = $(this).val();
     //   if (grdFtherId.length >=4) {
     //           var url  = "{{URL::to('/')}}" + "/dashboard/getParentDetails/"+grdFtherId;
     //           $.ajax({
     //                   'type': 'GET',
     //                   'url': url,
     //                   success: function (response) {
     //                        var parentData = '';
     //                        if (response) {
     //                           var result = $.parseJSON(response);
     //                           $.each( result, function(key, value) { 
     //                           parentData  += "<tr class='getId'>"+
     //                                              "<td>"+value['refCode']+"</td>"
     //                                              +"<td>"+value['fnameEng'] +"</td>"
     //                                          +"</tr>";
     //                            $(".responseGrandFather").append(parentData); 
     //                           }); 
     //                        } 
     //                     console.log(result);
     //                   }
     //           });
     //       }
     //   });
    $('#thebox').picEdit();
    $('.marrExists').on('change', function(e){
        e.preventDefault();
         var sts = $(this).val();
         
         if (sts === 'Y') {
            $('.marriageStatus').removeClass('hide');
         }else{
            $('.marriageStatus').addClass('hide');
         }
         return false;
    }); 
    $('.pwdExists').on('change', function(e){
        e.preventDefault();
         var sts = $(this).val();
         
         if (sts === 'Y') {
            $('.passportDiv').removeClass('hide');
         }else{
            $('.passportDiv').addClass('hide');
         }
         return false;
    });
    /*----------*/
    $('.citizenExists').on('change', function(e){
        e.preventDefault();
         var sts = $(this).val();
         
         if (sts === 'Y') {
            $('.citizenDiv').removeClass('hide');
         }else{
            $('.citizenDiv').addClass('hide');
         }
         return false;
    });
    $.validator.addMethod("numCharOnly", function(value, element) {
    return this.optional(element) || value === "NA" ||
        value.match(/^[0-9,\+-]+$/);
}, "Please enter a valid number, or 'NA'");
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please"); 
    /*form validation */
    $(function(){
            $("form[name='frmcitizenInfo']").validate({ // TODO : Form name should be placed
                rules:{
                    fnameEng:{
                        required:true,
                        minlength:3,
                        maxlength:255
                    },
                    lnameEng:{
                        required:true,
                        minlength:3,
                        maxlength:255
                    },
                    dobBS:{
                        required:true                
                    },
                    dobAD:{
                        required:true
                    },
                    gfFNameEng:{
                        required:true,
                        minlength:3,
                        maxlength:255
                    },
                    gfLnameEng:{
                        required:true,
                        minlength:3,
                        maxlength:255
                    },
                    ftFNameEng:{
                        required:true,
                        minlength:3,
                        maxlength:255
                    },
                    ftLnameEng:{
                        required:true,
                        minlength:3,
                        maxlength:255
                    },
                    mtFNameEng:{
                        required:true,
                        minlength:3,
                        maxlength:255
                    },
                    mtLnameEng:{
                        required:true,
                        minlength:3,
                        maxlength:255
                    },
                    phoneNumber : {
                        number : true,
                        minlength : 9,
                        maxlength  : 10
                    },
                    gender : {
                        required : true
                    },
                    villageNameEng : {
                        required : true
                    }
                },
                messages: {
                    villageNameEng : "village name in english is required",
                    fnameEng: " First name in english is required",
                    lnameEng: " Last name in english is required",
                    dobBS: " The date of birth in nepali is required",
                    dobAD: " The date of birth in english is required",
                    gfFNameEng: " The grand father first name in english is required",
                    gfLnameEng: " The grand father last  name is required",
                    ftFNameEng: " The father first  name is required",
                    ftLnameEng: " The father last name is required",
                    mtFNameEng: " The mother first name is required ",
                    mtLnameEng: " The mother last name is required",
                    phoneNumber : "phone number must be 10 digit number",
                    gender      : "select gender first"
                }
            });
        });
    /*======= grandfather form input autofill ===========*/
    var gfCitizenNum = $('.gfCitizenNum');  // grand father
    gfCitizenNum.on('keyup', function(e){
        e.preventDefault();
        var gfCitizenNum = $(this).val();
        if (gfCitizenNum.length >= 4) {
            var url  = "{{URL::to('/')}}" + "/dashboard/getCitizenRelationData";
            var data = {citizenNo : gfCitizenNum};
            $.ajax({
                'type' : 'GET',
                'url'  : url,
                'data' : data,
                success:function(response){
                    console.log(response);
                   if (response.success == true) {
                    console.log(response);
                    $('.gfCitizenIssuePlace').val(response.citizenshipIssuePlace);
                    $('.gfCitizenIssueDate').val(response.citizenshipIssueDate);
                    $('.gfFNameNep').val(response.fnameNep);
                    $('.gfMNameNep').val(response.mnameNep);
                    $('.gfLnameNep').val(response.lnameNep);
                    
                    $('.gfFNameEng').val(response.fnameEng);
                    $('.gfMNameEng').val(response.mnameEng);
                    $('.gfLnameEng').val(response.lnameEng);
                   }
                   
                }
            });
        }
    });
    /*====== father input form autofill ======*/
    var fCitizenNum = $('.fCitizenNum');  
    fCitizenNum.on('keyup', function(e){
        e.preventDefault();
        var fCitizenNum = $(this).val();
        if (fCitizenNum.length >= 4) {
            var url  = "{{URL::to('/')}}" + "/dashboard/getCitizenRelationData";
            var data = {citizenNo : fCitizenNum};
            $.ajax({
                'type' : 'GET',
                'url'  : url,
                'data' : data,
                success:function(response){
                    console.log(response);
                    if (response.success == true) {
                        console.log(response);
                        $('.ftCitizenIssuePlace').val(response.citizenshipIssuePlace);
                        $('.ftCitizenIssueDate').val(response.citizenshipIssueDate);
                        $('.ftFNameNep').val(response.fnameNep);
                        $('.ftMNameNep').val(response.mnameNep);
                        $('.ftLnameNep').val(response.lnameNep);
                        $('.ftFNameEng').val(response.fnameEng);
                        $('.ftMNameEng').val(response.mnameEng);
                        $('.ftLnameEng').val(response.lnameEng);
                    }
                }
            });
        }
    });
    /*----------- Mother details to form input -----------*/
    var mCitizenNum = $('.mCitizenNum'); 
    mCitizenNum.on('keyup', function(e){
        e.preventDefault();
        var mCitizenNum = $(this).val();
        if (mCitizenNum.length >= 4) {
            var url  = "{{URL::to('/')}}" + "/dashboard/getCitizenRelationData";
            var data = {citizenNo : mCitizenNum};
            $.ajax({
                'type' : 'GET',
                'url'  : url,
                'data' : data,
                success:function(response){
                    console.log(response);
                    if (response.success == true) {
                        console.log(response);
                        $('.mCitizenIssuePlace').val(response.citizenshipIssuePlace);
                        $('.mCitizenIssueDate').val(response.citizenshipIssueDate);
                        $('.mtFNameNep').val(response.fnameNep);
                        $('.mtMNameNep').val(response.mnameNep);
                        $('.mtLnameNep').val(response.lnameNep);
                        $('.mFNameEng').val(response.fnameEng);
                        $('.mMNameEng').val(response.mnameEng);
                        $('.mLnameEng').val(response.lnameEng);
                    }
                }
            });
        }
    });
    /*----------- Daughter-in-law details to form input -----------*/
    var mgCitizenNum = $('.mgCitizenNum');
    mgCitizenNum.on('keyup', function(e){
        e.preventDefault();
        var mgCitizenNum = $(this).val();
        if (mgCitizenNum.length >= 4) {
            var url  = "{{URL::to('/')}}" + "/dashboard/getCitizenRelationData";
            var data = {citizenNo : mgCitizenNum};
            $.ajax({
                'type' : 'GET',
                'url'  : url,
                'data' : data,
                success:function(response){
                    console.log(response);
                    if (response.success == true) {
                        console.log(response);
                        $('.mgCitizenIssuePlace').val(response.citizenshipIssuePlace);
                        $('.mgcitizenIssueDate').val(response.citizenshipIssueDate);
                        $('.mgFNameNep').val(response.fnameNep);
                        $('.mgMNameNep').val(response.mnameNep);
                        $('.mgLnameNep').val(response.lnameNep);
                        $('.mgFNameEng').val(response.fnameEng);
                        $('.mgMNameEng').val(response.mnameEng);
                        $('.mgLnameEng').val(response.lnameEng);
                    }
                }
            });
        }
    });
});
</script>