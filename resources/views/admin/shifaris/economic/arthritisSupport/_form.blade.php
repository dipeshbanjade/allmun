<div class="right_col nep" role="main" style="min-height: 882px;">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x">उपचारमा आर्थीक सहायता सिफारिस</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="block">
         @include('admin.shifaris.municipalityDetail')

         <div class="row top-part">
          <div class="col-md-6">
           <p align="left"><b class="ps">प. सं.:
            @if(isset($data->refCode))
            {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
            @else
            {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
            @endif
          </b></p>
          <p align="left"><b class="cn">च. नं.:{{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNumber']) }}</b></p>
        </div>
        <div class="col-md-6">
          <p align="right"><b class="mt">मिति :
            {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
          </b></p>
        </div>
      </div>
      <div class="row title-left">
       <div class="col-md-12">
        <p align="left">श्री
          {{ Form::text('officeName', 'जिल्ला जनस्वास्थ्य कार्यालय', ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'officeName']) }}
          <p align="left">
            {{ Form::text('officeAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'officeAddress']) }}।</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p align="center" class="font-size-24">
          </p><h4 align="center"><b> विषय: सिफारिस सम्बन्धमा।
            {{ Form::hidden('letter_subject', 'सिफारिस सम्बन्धमा') }}
          </b> </h4>  
          <p></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 content-para">
          <p>उपरोक्त विषयमा <b>{{ Form::text('municipalityName', isset($data->municipalityName) ? $data->municipalityName : getChecker()->nameNep , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b> वडा नं. <b>{{ Form::text('wardNumber', Auth::user()->wards_id, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }}</b> (साविक 
            <!--  -->
            {{ Form::text('organizationAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}
            <!--  -->
            <b><select onchange="changeSelect(this)" name="organizationType">
              <option value="गा.वि.स." 
              {{ !empty($data->organizationType) && $data->organizationType == 'गा.वि.स.' ? 'selected' : '' }}>गा.वि.स.</option>
              <option value="नगरपालिका" 
              {{ !empty($data->organizationType) && $data->organizationType == 'नगरपालिका' ? 'selected' : '' }}>नगरपालिका</option>
              <option value="उप महानगरपालिका" 
              {{ !empty($data->organizationType) && $data->organizationType == 'उप महानगरपालिका' ? 'selected' : '' }}>उप महानगरपालिका</option>
              <option value="महानगरपालिका" 
              {{ !empty($data->organizationType) && $data->organizationType == 'महानगरपालिका' ? 'selected' : '' }}> महानगरपालिका</option>
            </select>
          </b>, वडा नं. {{ Form::text('organizationWard', Auth::user()->wards_id, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}) बस्ने
          <b> <select onchange="changeSelect(this)" name="patientPrefix">
            <option value="श्री" {{ !empty($data->patientPrefix) && $data->patientPrefix == 'श्री' ? 'selected' : '' }}>श्री</option>
            <option value="सुश्री" {{ !empty($data->patientPrefix) && $data->patientPrefix == 'सुश्री' ? 'selected' : '' }}>सुश्री</option>
            <option value="श्रीमती" {{ !empty($data->patientPrefix) && $data->patientPrefix == 'श्रीमती' ? 'selected' : '' }}>श्रीमती</option>
          </select>
        </b>
        {{ Form::text('patientName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} को वार्षिक आम्दानी रु. ५००००।भन्दा कम भएको र निज {{ Form::text('disease', null, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} बाट पीडित भई {{ Form::text('hospital', null, ['class'=>'dashed-input-small-field', 'placeholder'=>'  *', 'required'=>'required']) }} अस्पतालमा उपचार गराउँदै आइरहेको र हाल थप उपचारको लागि लाग्ने लागत जुटाउन मेरो आर्थिक अवस्था कमजोर भएको कारणले नि:शुल्क उपचार गर्न सिफारिस पाऊँ भनी निवेदन दिनु भएको हुँदा निज निवेदक विपन्न परिवार अन्तर्गत पर्ने भएकोले त्यहाँको नियमानुसार आर्थिक सहायता उपलब्ध गराई दिनुहुन सिफारिस गरिन्छ।</p>
      </div>
      <div class="col-md-6">
      </div>
      </div>
      <div class="text-right btm-last">
        <p>{{ Form::text('authorizedPerson', null , ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
        <p> <b> 
          <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
            @if(count($deginationsId) > 0)
            @foreach($deginationsId as $deg)
            <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
              {{ $deg->nameNep }} 
            </option>
            @endforeach
            @endif
          </select>
        </b> </p>
      </div>
      <!--Nibedak Block  -->
      <!--views for nibedak detail -->
      <div class="clearfix"></div>
      <hr>
      @include('admin.shifaris.nibedakCommonField')
      <!-- END -->
      <div class="col-md-12">
        <hr>
      </div>
    </div>
  </div>
</div>
</div>