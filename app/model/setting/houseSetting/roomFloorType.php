<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class roomFloorType extends Model
{
    protected $table = 'room_floor_types';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
