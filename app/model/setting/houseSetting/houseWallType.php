<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class houseWallType extends Model
{
	protected $table = 'house_wall_types';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
