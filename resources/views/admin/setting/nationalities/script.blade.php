.<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditnationality']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of Nationality is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmnationality']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of Nationality is required"
            }
        });
	var btnAddnationality = $('.btnAddnationality');
	btnAddnationality.on('click', function(e){
		$('#nationalityModel').modal('show'); 
	});

	var btnEditnationality = $('.btnEditnationality');
	btnEditnationality.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.nationId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editNationModel").modal('show');
			}
		})
	});

	var frmUpdatenationality = $('#frmUpdatenationality');
  frmUpdatenationality.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.nationId').val();
    var url  = "{{URL::to('/')}}" + "/setting/nationality/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editNationModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editNationModel').modal('hide');
});
});
</script>