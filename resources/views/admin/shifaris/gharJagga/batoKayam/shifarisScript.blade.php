<script type="text/javascript">
	$(function(){
		/*add buttoin*/
		$("form[name='formBatoKayam']").validate({
			rules:{

				refCode : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					number : true,
					minlength : 1,
					maxlength : 3
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				issuedDate : {
					required : true
				},
				chalaniNumber : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				accountHolderName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				accountHolderAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				municipalityName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				wardNumber : {
					required : true,
					number : true,
					minlength : 1,
					maxlength : 3
				},
				address : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				plotNumber : {
					required : true,				
					minlength : 1,
					maxlength : 4
				},
				area : {
					required : true,
					minlength : 1,
					maxlength : 10
				},
				eastWidth : {
					required : true,
					minlength : 1,
					maxlength : 10
				},
				length : {
					required : true,
					minlength : 1,
					maxlength : 10
				},
				landOwnerName : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				deginations_id : {
					required : true					
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 30
				}
			},
			messages: {

				refCode : "Ref Code is required",
				wardNumber : "Ward Id is required",
				municipalityName : "Municipality Id is required",
				issuedDate : "Issue Date is required with valid date format (YYY-MM-DD)",
				chalaniNumber : "Chalani Number is required",
				accountHolderName : "Account Holder Name is required",
				accountHolderAddress : "Account Holder Address is required",
				municipalityName : "Municipality Name is required",
				wardNumber : "Ward Number is required",
				address : "Address is required",
				plotNumber : "Plot number is required",
				area : "Area required",
				eastWidth : "EastWidth is required and must be number",
				length : "Length Value is required is required",
				landOwnerName : "Land Owner Name is required",
				deginations_id : "Degination Id required",
				authorizedPerson : "Authorized Person (Hakim ko nam) is required"

			}
		});
	});

</script>