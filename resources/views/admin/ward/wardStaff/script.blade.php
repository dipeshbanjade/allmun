<script type="text/javascript">
	$.validator.addMethod("minAge", function(value, element, min) {
		var today = new Date();
		var birthDate = new Date(value);
		var age = today.getFullYear() - birthDate.getFullYear();

		if (age > min+1) {
			return true;
		}

		var m = today.getMonth() - birthDate.getMonth();

		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}

		return age >= min;
	}, "You are not old enough!");
	$(function(){
		$("form[name='frmWardStaff']").validate({
			rules:{
				firstNameNep : {
					minlength : 3,
					maxlength : 30
				},
				firstNameEng : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				lastNameNep : {
					minlength : 3,
					maxlength :30
				},
				lastNameEng : {
					required :true,
					minlength : 3,
					maxlength : 30
				},
				password : {
					required : true,
					minlength : 8,
					maxlength : 30
				},
				citizenNo : {
					required : true,
					minlength : 5,
					maxlength: 15
				},
				joingDateNep : {
					required : true,
					date: true
				},
				wardNo : {
					required : true,
					minlength: 1,
					maxlength: 3,
					number: true
				},
				villageNep : {
					minlength : 3,
					maxlength : 30
				},
				villageEng : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				phoneNumber : {
					required : true,
					minlength : 10,
					maxlength: 10,
					number: true
				},
				email: {
					email: true
				},
				roles_id:{
					required: true
				},
				staff_types_id:{
					required: true
				},
				deginations_id:{
					required: true
				},
				provinces_id: {
					required: true
				},
				districts_id: {
					required: true
				},
				dob:{
					required: true,
					date: true
				},
				joingDateNep : {
					required : true
				}
	       // profilePic : {
	       // 	 required: true,
        //      extension: "jpg|png|jpeg"
	       // },
	       // citizenImagePath : {
	       //  required: true,
        //      extension: "jpg|png|jpeg"
	       // }
	   },
	   messages: {
	   	refCode: "Code is required",
	   	firstnameEng : "The First name is required",
	   	lastNameEng : "The Last Name  is required",
	   	password : "Enter a password with minimun 8 character long ",
	   	citizenNo : "The Citizen No field is required",
	   	joingDateNep : "Joining Date is required",
	   	wardNo : "Ward Number field is required",
	   	villageEng : "Village Name field is required with minimun 3 character and maximum 30 character",
	   	phoneNumber : "Phone Number field is required with 10 digits",
	   	// profilePic  : "file extension is not match",
	   	// citizenImagePath : "select correct file format required",
	   	email: {
	   		email: "Please enter valid email address"
	   	},
	   	roles_id: "Staff role is required",
	   	staff_types_id: "This field is required",
	   	deginations_id: "Designation of staff is required",
	   	provinces_id: "Please select province id",
	   	districts_id: "Please select district",
	   	joingDateNep : "joining Date is required",
	   	dob: {
	   		required: "Birth date is required"
	   	} 
	   }
	});
		/*----------------------*/



	});

</script>