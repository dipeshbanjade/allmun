<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
            <?php
             $naturalDis     = isset($naturalDiseaster) ? $naturalDiseaster : '';
             $jsonNaturalDis = isset($naturalDis->diseasterName) ? json_decode($naturalDis->diseasterName) : '';
            ?>
                <div class="form-group col-md-12">
                    <p>
                       १. तपाईको परिवार बसोवास गरेको स्थानमा प्राकृतिक विपद वा प्रकोपको खतरा तथा भू धरातलीय जोखिमहरु छन् ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline">
                            १. छ 
                            <input type="radio" name="haveNaturalDiseaster" value="1" class="rdoClickMe" {{ isset($naturalDis->haveNaturalDiseaster) && $naturalDis->haveNaturalDiseaster == 1 ? 'checked' : '' }}>
                        </label> 
                        <label class="radio-inline">
                            २. छैन्
                            <input type="radio" name="haveNaturalDiseaster" value="0" class="rdoClickMe" {{ isset($naturalDis->haveNaturalDiseaster) && $naturalDis->haveNaturalDiseaster == 0  ? 'checked' : '' }}>
                        </label>
                        <label class="radio-inline col-md-12">
                            <p>
                                यदि छ भने तपाईको परिवार बसोवास गरेको स्थानमा कस्तो कस्तो किसिमको प्राकृतिक विपद वा प्रकोपको खतराहरु छन् ? (बहुउत्तर सम्भव छ) 
                            </p>
                        </label>
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="सुख्खा खडेरी" {{ !empty($naturalDis->diseasterName) && in_array('सुख्खा खडेरी', $jsonNaturalDis) ? 'checked' : '' }}>
                            १. सुख्खा खडेरी
                        </label><br />

                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="आगलागी (बस्तीमा)" {{ !empty($naturalDis->diseasterName) && in_array('आगलागी (बस्तीमा)', $jsonNaturalDis) ? 'checked' : '' }}>
                            २. आगलागी (बस्तीमा)
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="बन डडेलो" {{ !empty($naturalDis->diseasterName) && in_array('बन डडेलो', $jsonNaturalDis) ? 'checked' : '' }}>
                            ३. बन डडेलो
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="बाढी / ठूलो भल (पानी) आउने" {{ !empty($naturalDis->diseasterName) && in_array('बाढी / ठूलो भल (पानी) आउने', $jsonNaturalDis) ? 'checked' : '' }}>
                            ४. बाढी / ठूलो भल (पानी) आउने
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="डुवान" {{ !empty($naturalDis->diseasterName) && in_array('डुवान', $jsonNaturalDis) ? 'checked' : '' }}>
                            ५. डुवान
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="आँधी÷हुरीबतास" {{ !empty($naturalDis->diseasterName) && in_array('आँधी÷हुरीबतास', $jsonNaturalDis) ? 'checked' : '' }}>
                            ६. आँधी÷हुरीबतास
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="चट्याङ्ग" {{ !empty($naturalDis->diseasterName) && in_array('चट्याङ्ग', $jsonNaturalDis) ? 'checked' : '' }}>
                            ७. चट्याङ्ग 
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="असिना" {{ !empty($naturalDis->diseasterName) && in_array('असिना', $jsonNaturalDis) ? 'checked' : '' }}>
                             ८. असिना
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="जमिन भासिने / भ्वाङ पर्ने" {{ !empty($naturalDis->diseasterName) && in_array('जमिन भासिने / भ्वाङ पर्ने', $jsonNaturalDis) ? 'checked' : '' }}>
                            ९. जमिन भासिने / भ्वाङ पर्ने
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="अतिवृष्टि" {{ !empty($naturalDis->diseasterName) && in_array('अतिवृष्टि', $jsonNaturalDis) ? 'checked' : '' }}>
                            १०. अतिवृष्टि
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="खण्डबृष्टि" {{ !empty($naturalDis->diseasterName) && in_array('खण्डबृष्टि', $jsonNaturalDis) ? 'checked' : '' }}>
                            ११. खण्डबृष्टि
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="पहिरो / भूक्षय" {{ !empty($naturalDis->diseasterName) && in_array('पहिरो / भूक्षय', $jsonNaturalDis) ? 'checked' : '' }}>
                            १२. पहिरो / भूक्षय
                        </label><br />
                        <label class="radio-inline">
                            <input type="checkbox" name="diseasterName[]" value="कृषि बालीमा रोग÷किराको प्रकोप" {{ !empty($naturalDis->diseasterName) && in_array('कृषि बालीमा रोग÷किराको प्रकोप', $jsonNaturalDis) ? 'checked' : '' }}>
                            १३. कृषि बालीमा रोग÷किराको प्रकोप
                        </label><br />
                        <label class="radio-inline">
                             {{ Form::text('otherNaturalDiseasterName', isset($naturalDis->otherNaturalDiseasterName) ? $naturalDis->otherNaturalDiseasterName : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)']) }}
                        </label>
                    </div>
                </div> 
            </div>
            <div class="msgDisplay"></div>
    </div>
</div>