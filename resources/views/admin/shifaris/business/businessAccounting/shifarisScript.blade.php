<script type="text/javascript">
var btnAddBodartha = $('#btnAddTableRow');
btnAddBodartha.on('click', function(e){
	e.preventDefault();	//alert('hello');

	var bodarthaInput = '<input type="text" class="form-control" name="bodartha[]">';
	var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

	var insertRow = "<tr class='tblRow'>"
	+"<td>"+bodarthaInput+"</td>"
	+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
	+"</tr>";

	$('#sabik_address_table').append(insertRow);

	$('.btnRemoveItem').on('click', function(e){
		$(this).parent().parent().remove();
		return false;
	});

});
$(function(){
		/*add buttoin*/
		$("form[name='frmWardbusinessAccounting']").validate({
			rules:{
				issuedDate : {
					required : true,
					date : true
				},
				officeAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				chalaniNum : {
					required : true,
					minlength : 1,
					maxlength : 30					
				},
				wardNum : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				organizationAddress : {
					required : true,
					minlength : 5,
					maxlength : 2000
				},
				organizationWard : {
					required : true,
					minlength : 1,
					maxlength : 3,
					number: true
				},
				shopAddress : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				pan : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				name : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				shopName : {
					required : true,
					minlength : 3,
					maxlength : 255
				},
				newShop : {
					required : true,
					minlength : 5,
					maxlength : 255
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				issuedDate : "Issue Date is required",
				chalaniNum : "Chalani Number is required",
				officeAddress : "Office Address is required",
				wardNum : "Ward Number is required and numeric",
				organizationAddress : "Organization Address is required",
				organizationWard : "Organization Ward is required",
				name : "Person Name is required",
				shopAddress : "Shop Address is required",
				pan : "Pan number is required",
				shopName : "Shop Name is required",
				newShop : "Business Status is required",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});
	$('#nebedakId').on('change',function(){
		nebedakId = $(this).val();
		if (nebedakId.length >0 ) {
			var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
			$.ajax({
				'type' : 'GET',
				'url'  : url,
				success : function(response){
					console.log(response);
					$("input[name='name']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');

// code for gender determination and option change partial work

					// if(response.gender == 'female'){
					// 	$("select[name='title'] option[value=श्री]").attr('selected', true);
					// 	changeSelect($("select[name='title']"));
					// 	// $("select[name='title'] option[value=श्री]").trigger('change');
					// }else{
					// 	$("select[name='title'] option[value=सुश्री]").attr('selected', true);	
					// 	changeSelect($("select[name='title']"));
					// 	// $("select[name='title'] option[value=सुश्री]").trigger('change');				
					// 	// $("input[name='title']").val('सुश्री');
					// 	// $("input[name='name']")
					// }
					
				},complete:function(){
				}
			})
			.fail(function (response) {
				alert('data not found ');
			});
		}else{

		}
	});
</script>