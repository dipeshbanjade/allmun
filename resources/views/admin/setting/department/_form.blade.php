<div class="card-body row">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
    {{ Form::text('nameNep', null, ['class'=>'mdl-textfield__input txtnameNep', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">@lang('commonField.personal_information.nameNep')</label>
    </div>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
        {{ Form::text('nameEng', null, ['class'=>'mdl-textfield__input txtnameEng', 'placeholder'=>'']) }}
        <label class = "mdl-textfield__label">@lang('commonField.personal_information.nameEng')</label>
    </div>
</div>