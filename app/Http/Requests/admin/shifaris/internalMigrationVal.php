<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class internalMigrationVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refCode' => 'required|min:3|unique:internal_migrations',
            'chalaniNumber' => 'required|unique:internal_migrations',

            'accountHolderAddress' => 'required',
            'applicant' => 'required',
            'moveDate' => 'required',
            'district' => 'required',
            'municipalityName' => 'required',
            'wardFrom' => 'required',
            'municipalityName2' => 'required',
            'ward2' => 'required',
            'address' => 'required'
        ];
    }
}
