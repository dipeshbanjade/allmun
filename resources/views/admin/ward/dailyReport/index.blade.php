@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
{{-- Daily Report Log Starts --}}
<div class="col-sm-12 col-md-12">
    <div class="card-box">
        <div class="card-head">
            <header>@lang('commonField.extra.dailyReport')</header>
        </div>
        <div class="card-body ">
            <a href="{{ route('dailyReport.create'') }}" type="button" class="btn btn-primary pull-right"><i class="fa fa-plus"></i><span> @lang('commonField.extra.createDailyReport')</span></a> 
            <div class="mdl-tabs mdl-js-tabs is-upgraded" data-upgraded=",MaterialTabs">
                <!-- table start  -->
                <div class="mdl-tabs__panel is-active p-t-20" id="tab4-panel">
                    <div class="table-responsive tableHeight250">
                        <table class="table scrollable">
                            <tbody>
                                <tr>
                                    <th>@lang('commonField.extra.sn')</th>
                                    <th>@lang('commonField.extra.date')</th>
                                    <th>@lang('commonField.extra.subject')</th>
                                    <th>@lang('commonField.extra.startTime')</th>
                                    <th>@lang('commonField.extra.endTime')</th>
                                    <th>@lang('commonField.extra.desc')</th>
                                    <th>@lang('commonField.extra.action')</th>
                                </tr>
                                @php $count = 1   @endphp
                                @foreach($allStaffDR as $staffDR)
                                <tr>
                                    <td>{{ $count ++ }}</td>
                                    <td>{{ $staffDR-> date}}</td>
                                    <td>{{ $staffDR-> sub}}</td>
                                    <td>{{ $staffDR-> startTime}}</td>                           
                                    <td>{{ $staffDR-> endTime}}</td>                           
                                    <td>{{ $staffDR-> desc}}</td> 
                                    <td>
                                        <a href="{{ route('dailyReport.edit' , $staffDR->id) }}" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="#" class="deleteMe" data-url="{!! route('dailyReport.delete', $staffDR->id ) !!}" data-name="{{ $staffDR->sub }}">
                                            <i class="fa fa-trash "></i></a>   
                                    </td>
                                </tr>
                                     @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Daily report Log Ends --}}
    @endsection
