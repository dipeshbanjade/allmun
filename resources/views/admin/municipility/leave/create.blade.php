@extends('main.app')
@section('page_title', $page['page_title'])
@section('page_description', $page['page_description'])
@section('content')
<div class="col-sm-12 col-md-12">
<div class="card-box ">

	<div class="card-body">
	<div class="card-head">
          <header><i class="fa fa-plus">&nbsp;@lang('commonField.extra.createLeave')</i></header>
        </div>
	     {{ Form::open(['route' => 'admin.munStaffLeave.store', 'name' => 'frmMunLeave']) }}
            @include('admin.municipility.leave._form')
             {{ Form::submit('CREATE', ['class' => 'btn btn-success pull-right']) }}
            <button type="button" class="btn btn-default pull-right" onclick="history.back()">BACK
            </button>
	     {{ Form::close() }}
	</div>
</div>
</div>
@endsection
@section('custom_script')
   @include('admin.municipility.leave.script')
@endsection