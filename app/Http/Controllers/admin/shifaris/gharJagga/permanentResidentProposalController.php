<?php

namespace App\Http\Controllers\admin\shifaris\gharJagga;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\PermanentResidentProposal;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\permanentResidentProposalVal;
use DB;
use Auth;

use App\model\mylogic;

class permanentResidentProposalController extends Controller
{
    protected $viewPath = 'admin.shifaris.gharJagga.permanentResidentProposal';
    protected $shifarisTableName     = 'permanent_resident_proposals';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;
    protected $refCode;

    public function __construct(){
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixPermanentResidentProposal');
        $this->refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

    }

    public function index()
    {
        //
        $page['page_title']       = 'Permanent Resident Proposal';
        $page['page_description'] = 'Permanent Resident Proposal';


        $instRecomd = permanentResidentProposal::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

    }

    public function create()
    {
        //
        $page['page_title']       = 'Permanent Resident Proposal : create';
        $page['page_description'] = 'Permanent Resident Proposal : details';

        $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);

        
        $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 

        return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));

    }

    public function store(permanentResidentProposalVal $request)
    {
        //

        DB::beginTransaction();
        try {
            $permResidentCount = count($request->tole);

            $tole = $request->tole;
            $streetName = $request->streetName;
            $houseNo = $request->houseNo;

            $permResidentArray = [];

            for ($i=0; $i < $permResidentCount; $i++) {
                array_push($permResidentArray, [
                    'tole' => $tole[$i],
                    'streetName' => $streetName[$i],
                    'houseNo' => $houseNo[$i]
                ]);
            }
            $permResidentJson = json_encode($permResidentArray);


                //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);
            $isShifarisExist = isset($request->shifarisId) ? PermanentResidentProposal::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
            
            $save = !empty($isShifarisExist['id']) ? PermanentResidentProposal::findOrfail($isShifarisExist['id']) : new PermanentResidentProposal;

            $save->createdBy = Auth::user()->id;

            $save->refCode  = $request->refCode;

            $save->wards_id  = Auth::user()->wards_id ?? NULL;

            $save->municipilities_id = getMunicipalityData()['id'] ?? "";

            $save->issuedDate  = $request->issuedDate;            
            $save->chalaniNumber = $request->chalaniNumber;
            $save->residentApplicant = $request->residentApplicant;
            $save->nijName = $request->nijName;
            $save->municipalityName = $request->municipalityName;
            $save->wardNumber = $request->wardNumber;
            $save->sabikAddress = $request->sabikAddress;
            $save->bigatDate = $request->bigatDate;
            $save->nagarikata = $request->nagarikata;
            $save->residentDistrict = $request->residentDistrict;
            $save->issuedDateTwo = $request->issuedDateTwo;

            $save->tableData = $permResidentJson;                

            $save->authorizedPerson = $request->authorizedPerson;
            $save->deginations_id  = $request->deginations_id;
            $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];             

            $mySave = $save->save();

            $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);
            $request->chalaniNum = $request->chalaniNumber;
            $citizenShifarish = true;
            if(empty($isShifarisExist['id'])){
                $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
            }            
            if ($mySave && $log && $citizenShifarish) {
             DB::commit();
             return response()->json([
                 'success' => true,
                 'message' => config('activityMessage.saveMessage')
             ]);
                   // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
         }else{
             return response()->json([
                 'success' => false,
                 'message' => config('activityMessage.unSaveMessage')
             ]);
                   // return back()->withMessage(config('activityMessage.unSaveMessage'));
         }
     }catch (Exception $e) {
        DB::rollback();
        return back()->withMessage(config('activityMessage.dataNotInserted'));
    }
    
}

public function show($id)
{
        //
}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}
}
