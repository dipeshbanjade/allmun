<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class WorkType extends Model
{
    protected $table = 'work_types';
    protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
