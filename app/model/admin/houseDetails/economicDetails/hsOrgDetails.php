<?php

namespace App\model\admin\houseDetails\economicDetails;

use Illuminate\Database\Eloquent\Model;

class hsOrgDetails extends Model
{
    protected $table = 'hs_org_details';
}
