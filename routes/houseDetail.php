<?php
  Route::prefix('houseDetail')->group(function(){
      Route::get('/houseDetails',             'admin\houseDetails\houseDetailsController@index')->name('houseDetails.index');
      
      Route::get('/createHouse/create',       'admin\houseDetails\houseDetailsController@create')->name('houseDetails.create');
      // Route::get('/houseDetails/create',      'admin\houseDetails\houseDetailsController@create')->name('houseDetails.create');
      Route::post('houseDetails/store',       'admin\houseDetails\houseDetailsController@store')->name('houseDetails.store');
      Route::get('/houseDetails/{id}',        'admin\houseDetails\houseDetailsController@show')->name('houseDetails.show');
      Route::get('houseDetails/{slug}/edit',    'admin\houseDetails\houseDetailsController@edit')->name('houseDetails.edit');
      Route::post('houseDetails/{id}/update', 'admin\houseDetails\houseDetailsController@update')->name('houseDetails.update');
      Route::get('/houseDetailsDelete/{id}',   'admin\houseDetails\houseDetailsController@destroy')->name('houseDetails.delete');

      Route::get('changeGharmuli/{slug}', 'admin\houseDetails\houseDetailsController@changeGharMuli')->name('changeGharMuli'); 


  Route::get('/houseMoreDetails/{slug}',   'admin\houseDetails\houseDetailsController@insertMoreDetails')->name('addHsMoreDetails'); 
  Route::post('houseDetailsDk', 'admin\houseDetails\familyFacilityController@insertDrinkingWater')->name('saveDrinkingWater');
  Route::post('houseDetailsInsertKD', 'admin\houseDetails\familyFacilityController@insertHouseKitchen')->name('houseDetailsInsertKD');
  Route::post('houseDetailsInsertHL', 'admin\houseDetails\familyFacilityController@insertHouseLights')->name('houseDetailsInsertHL');
  Route::post('houseDetailsInsertHT', 'admin\houseDetails\familyFacilityController@insertHouseToilet')->name('houseDetailsInsertHT');
  Route::post('houseDetailsInsertHW', 'admin\houseDetails\familyFacilityController@insertHouseWaste')->name('houseDetailsInsertHW');
  Route::post('houseDetailsInsertHR', 'admin\houseDetails\familyFacilityController@insertHouseRoad')->name('houseDetailsInsertHR');

     Route::post('houseDetailsInsertED', 'admin\houseDetails\familyFacilityController@insertElectronicDevices')->name('houseDetailsInsertED');  // 

    //Children Education     
     Route::post('houseDetailsInsertCE', 'admin\houseDetails\childEducationController@insertChildrenEducation')->name('houseDetailsInsertCE');
     Route::post('houseDetailsInsertSD', 'admin\houseDetails\childEducationController@insertSchoolDistance')->name('houseDetailsInsertSD');
     Route::post('houseDetailsInsertCM', 'admin\houseDetails\childEducationController@insertChildMontessoris')->name('houseDetailsInsertCM');
     Route::post('houseDetailsInsertCCI', 'admin\houseDetails\childEducationController@insertChildClubInvolve')->name('houseDetailsInsertCCI');
     Route::post('houseDetailsInsertSU', 'admin\houseDetails\childEducationController@insertSchoolUnadmit')->name('houseDetailsInsertSU');
     Route::post('houseDetailsInsertCH', 'admin\houseDetails\childEducationController@insertChildHobby')->name('houseDetailsInsertCH');
     Route::post('houseDetailsInsertEDUD', 'admin\houseDetails\childEducationController@insertEducationDrop')->name('houseDetailsInsertEDUD');

     /*drinking water*/

      //migration
     Route::post('houseDetailsInsertMigration', 'admin\citizenInfo\citizenInfoController@migrationDetails')->name('migrationDetails');

      // family Death record
     Route::post('houseDetailsInsertDeathReport', 'admin\citizenInfo\citizenInfoController@insertDeathMember')->name('fmDeathMember');

     Route::post('houseDetailsInsertHsFertility', 'admin\citizenInfo\citizenInfoController@insertHsFertility')->name('hsFamilyFertility');

// Health and Cleaness
     Route::post('houseDetailsInsertFFemaleDisease', 'admin\houseDetails\healthAndCleanessController@insertFemaleDisease')->name('fmFemaleDisease');
     Route::post('houseDetailsInsertInfantVaccine', 'admin\houseDetails\healthAndCleanessController@insertInfantVaccine')->name('infantVaccine');
     Route::post('houseDetailsInsertCHC', 'admin\houseDetails\healthAndCleanessController@insertChildrenHealthCare')->name('hsChildHealthCare');
     Route::post('houseDetailsInsertFHP', 'admin\houseDetails\healthAndCleanessController@insertFamilyHealthPosition')->name('hsFamilyHealthPosition');
     Route::post('houseDetailsInsertHIVP', 'admin\houseDetails\healthAndCleanessController@insertHIVPregnancy')->name('hsHIVPregnancy');
     Route::post('houseDetailsInsertPA', 'admin\houseDetails\healthAndCleanessController@insertPregnancyAction')->name('hsPregnancyAction');
     Route::post('houseDetailsInsertPHS', 'admin\houseDetails\healthAndCleanessController@insertPregnancyHealthService')->name('hsPregnancyHealthService');
     Route::post('houseDetailsInsertFUDW', 'admin\houseDetails\healthAndCleanessController@insertFmUsingDrinkingWater')->name('hsFmUsingDrinkingWater');
     Route::post('houseDetailsInsertHWP', 'admin\houseDetails\healthAndCleanessController@insertHandWashProcess')->name('hsHandWashProcess');

//Child Care and involvement
     Route::post('houseDetailsInsertCS', 'admin\houseDetails\childcareInvolvementController@insertChildSecurity')->name('hsChildSecurity');
     Route::post('houseDetailsInsertMD', 'admin\houseDetails\childcareInvolvementController@insertMarriageDetails')->name('hsMarriageDetails');
     Route::post('houseDetailsInsertFL', 'admin\houseDetails\childcareInvolvementController@insertFamilyLiving')->name('hsFamilyLivingDetail');      
     Route::post('houseDetailsInsertFMWD', 'admin\houseDetails\childcareInvolvementController@insertFmMemWorking')->name('hsFamilyMemWorkingDetail');     
     Route::post('houseDetailsInsertFMCR', 'admin\houseDetails\childcareInvolvementController@insertFmChildRepresentative')->name('hsFmChildRepresentative');     
     Route::post('houseDetailsInsertOMLT', 'admin\houseDetails\childcareInvolvementController@insertOtherMemLivingTog')->name('hsOtherMemLiveTog');     
     
      /*agriculture route*/
     Route::post('houseDetailsInsertAgriDetails', 'admin\houseDetails\economicDetails@agricultureDetails')->name('agricultureDetails');              // Agriculture Details

     Route::post('houseDetailMemberTraining', 'admin\houseDetails\economicDetails@memberTraining')->name('economicMemTraining');

     Route::post('houseDetailsInsertAgriProduction', 'admin\houseDetails\economicDetails@agricultureProductionDetails')->name('agricultureProduction');   //Agriculture

     Route::post('houseDetailsInsertAnimalProduction', 'admin\houseDetails\economicDetails@agricultureAnimalProduction')->name('agricultureAnimalProduction');  // Animal

     Route::post('houseDetailsInsertOtherIncomeSource', 'admin\houseDetails\economicDetails@otherIncomeSource')->name('agriOtherIncomeSource');  // other income detrails

     Route::post('houseDetailsInsertYearlySurvival', 'admin\houseDetails\economicDetails@agricultureSurvival')->name('agriYearlySurvival');  // Yearly survival

     Route::post('houseDetailsInsertTaxPayDetrails', 'admin\houseDetails\economicDetails@taxPayDetails')->name('agriTaxPayDetails');
      
     Route::post('houseDetailsInsertOrgDetails', 'admin\houseDetails\economicDetails@orgDetails')->name('agriOrgDetails');  //org Details 

     Route::post('houseDetailsIndustryBusiness', 'admin\houseDetails\economicDetails@industryBusinessDetail')->name('industryBusinessDetail');  // Yearly survival 
     
     Route::post('houseDetailsInsertLoanDetails', 'admin\houseDetails\economicDetails@loanDetails')->name('agriLoanDetails');   // laon details

     Route::post('houseDetailsInsertOrgInvolvement', 'admin\houseDetails\economicDetails@orgInvolvement')->name('agriOrgInvolvement');

     Route::post('houseDetailsRentailsDetails', 'admin\houseDetails\economicDetails@rentailDetails')->name('agriRentailDetails'); 

     Route::post('houseDetailsIncomeAndExpenditure', 'admin\houseDetails\economicDetails@incomeAndExpensesDetail')->name('hsIncomeAndExpenses'); 

     /*natural diseaster*/
    Route::post('houseDetailsInsertNaturalDiseaster', 'admin\houseDetails\naturalDiseaster@naturalDiseaster')->name('agriNaturalDiseaster');  // natural diseaster


    Route::post('houseDetailsInsertDiseasterProblem', 'admin\houseDetails\naturalDiseaster@haveGetDiseasterProblem')->name('agriHaveDiseasterProblem');  // get natural diseaster

    Route::post('houseDetailsWildAnimalProblem', 'admin\houseDetails\naturalDiseaster@wildAnimalProblem')->name('agriWildAnimalProblem');  // wild animal problem

    Route::post('houseDetailsCrimeVictim', 'admin\houseDetails\naturalDiseaster@crimeVictim')->name('agriCrimeVictim');

    Route::post('houseDetailsenvironmentProblem', 'admin\houseDetails\naturalDiseaster@environmentProblem')->name('agriEnvProblem');
  });
?>
