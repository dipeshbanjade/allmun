<?php

namespace App\Http\Requests\admin\setting\SalaryType;

use Illuminate\Foundation\Http\FormRequest;

class SalaryTypeValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

   
    public function rules()
    {
        return [
            'nameEng' => 'required|min:3|max:50'
            
        ];
    }
}
