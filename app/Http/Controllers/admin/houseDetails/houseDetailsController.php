<?php

namespace App\Http\Controllers\admin\houseDetails;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\admin\houseDetails\HouseDetails;
use App\model\admin\citizen\CitizenInfo;
use App\Http\Requests\admin\house\basicHouseInfo\basicHouseValidation;
use App\model\setting\houseSetting\Country;
use App\model\admin\citizen\CitizenRelation;


use App\model\admin\ward\WardDetails;

use App\model\mylogic;
use Auth;
use DB;
use Validator;

class houseDetailsController extends Controller
{
 protected $viewPath = 'admin.houseDetails';
 protected $houseDetailsTbl = 'house_details';
 protected $citizenInfoTbl = 'citizen_infos';
 protected $houseTypeTbl = 'house_types';

 protected $familyTypeTbl = 'family_types';
 protected $roomUseForTbl = 'room_use_fors';
 protected $samiptoTbl    = 'swamiptos';
 protected $houseUseForTbl    = 'house_use_fors';
 protected $houseFoundationTbl    = 'house_foundation_types';
 protected $roomFloorTypeTbl    = 'room_floor_types';
 protected $hsWallTypeTbl    = 'house_wall_types';
 protected $hsRoofTbl    = 'house_roofs';

 protected $tblWaterSource = 'drinking_water_sources';
 protected $tblKitchen     = 'house_kitchens';
 protected $tblFmRelation  = 'family_relations';

 protected $whyNotTbl= 'why_nots';
 protected $drinkingWaterSourceTbl='drinking_water_sources';
 protected $qualityDrinkingTbl='quality_waters';
 protected $waterAvailabilitiesTbl='water_availabilities';

 protected $cookingFuelTbl='fuels';
 protected $ovenTypeTbl='oven_types';
 protected $lightSoureTbl='light_sources';
 protected $toiletTypeTbl='toilet_types';
 protected $wasteManagementTbl='waste_managements';
 protected $roadTbl='roads';
 protected $tblCountry = 'countries';
 protected $electricalDeviceTbl='electrical_devices';

//children Education
 protected $educationLevelTbl='education_levels';


 protected $qualificationTbl  = 'qualifications';
 protected $occupationTbl     = 'occupations';
 protected $citizenTypesTbl   = 'citizen_types';
 protected $religiousesTbl    = 'religiouses';
 protected $sysLangTbl        = 'sys_languages';
 protected $districtTbl       = 'districts';
 protected $nationalityTbl    = 'nationalities';
 protected $jatjatis          = 'jatjatis'; 

 // health and cleaness
 protected $diseaseTbl = 'diseases';
 protected $treatmentAreaTbl='treatment_areas';


 protected $vaccineTbl='vaccine_types';

 protected $refCode;
 protected $houseImagePath;
 protected $imageWidth;
 protected $imageHeight;

 protected $citizenId;

 public function __construct()
 {
  $this->middleware('auth');
  $this->refCode  = config('activityMessage.HouseDetails');
  $this->houseImagePath  = config('activityMessage.houseImagePath');
  $this->imageWidth      = config('activityMessage.imageWidth');
  $this->imageHeight     = config('activityMessage.imageHeight');
}

public function index()
{
  $page['page_title']       = 'House Details'; 
  $page['page_description'] = 'House details list';


  if (Auth::user()->userLevel === 'wrd') {
    $house = HouseDetails::select('refCode', 'citizen_infos_id', 'hsNum', 'hsTypeNam', 'memLiving','fmHeadPh', 'hsEstd', 'hsImgPath', 'identifier', 'surName', 'surPhone', 'toleName', 'fmMem')->where(['status' => 0 , 'softDelete' => 0, 'wards_id'=>Auth::user()->wards_id])->orderby('created_at', 'DESC')->paginate(config('activityMessage.pagination'));
    return view($this->viewPath . '.index', compact(['page', 'house']));
  }
  if (Auth::user()->userLevel === 'mun') {
    $house = HouseDetails::select('refCode', 'citizen_infos_id', 'hsNum', 'hsTypeNam', 'memLiving','fmHeadPh', 'hsEstd', 'hsImgPath', 'identifier', 'surName', 'surPhone', 'toleName')->where(['status' => 0, 'softDelete' => 0, 'municipilities_id'=>Auth::user()->municipilities_id])->paginate(config('activityMessage.pagination'));
    return view($this->viewPath . '.index', compact(['page', 'house']));
  }
  return redirect()->route('home')->withMessage(config('activityMessage.idNotFound'));
}

public function create()
{
  $page['page_title']       = 'Create House Details'; 
  $page['page_description'] = 'Create individual house Details';

  $houseTypeId = mylogic::getDrowDownData($tablename = $this->houseTypeTbl, $orderColumn = 'created_at', $orderby = 'ASC');
  $houseUseForId = mylogic::getDrowDownData($tablename = $this->houseUseForTbl, $orderColumn = 'created_at', $orderby = 'ASC');
  $houseFoundationId = mylogic::getDrowDownData($tablename = $this->houseFoundationTbl, $orderColumn = 'created_at', $orderby = 'ASC');
  $roomFloorTypeId = mylogic::getDrowDownData($tablename = $this->roomFloorTypeTbl, $orderColumn = 'created_at', $orderby = 'ASC');
  $familyTypeId = mylogic::getDrowDownData($tablename = $this->familyTypeTbl, $orderColumn = 'created_at', $orderby = 'ASC');
  $roomUseForId  = mylogic::getDrowDownData($tablename = $this->roomUseForTbl, $orderColumn = 'created_at', $orderby = 'ASC');
  $hsWallTypeId  = mylogic::getDrowDownData($tablename = $this->hsWallTypeTbl, $orderColumn = 'created_at', $orderby = 'ASC');
  $hsRoof  = mylogic::getDrowDownData($tablename = $this->hsRoofTbl, $orderColumn = 'created_at', $orderby = 'ASC');

  $samiptoHsId     = mylogic::drowDownDataByTagFilter($tablename = $this->samiptoTbl,$filterCol = 'tagName',$filterVal = 'house', $orderColumn = 'created_at', $orderby = 'ASC');

  $samiptoLdId     = mylogic::drowDownDataByTagFilter($tablename = $this->samiptoTbl,$filterCol = 'tagName',$filterVal = 'land', $orderColumn = 'created_at', $orderby = 'ASC');

  $disablitiyStatus = mylogic::getDrowDownData($tablename = 'disabilities', $orderColumn = 'created_at', $orderby = 'ASC');

  $maritialStatus = mylogic::getDrowDownData($tablename ='maritial_statuses', $orderColumn = 'created_at', $orderby = 'ASC');


  $citizenInfo = $this->getCitizenInfo();

  $listOfWrds = WardDetails::select('nameNep', 'nameEng', 'id')->where(['status'=> 0, 'softDelete'=> 0])->get();

  $wardList = myLogic::getWardList();

  return view($this->viewPath . '.create', compact(['page', 'houseTypeId', 'familyTypeId', 'roomUseForId', 'samiptoHsId','samiptoLdId', 'citizenInfo', 'houseUseForId', 'houseFoundationId', 'roomFloorTypeId', 'hsWallTypeId', 'hsRoof', 'wardList', 'disablitiyStatus', 'maritialStatus']));
}

    public function getCitizenInfo(){   // for citizen in drop down
      if (Auth::user()->userLevel ==='mun') {
        $citizenInfos = CitizenInfo::select('id', 'fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'citizenNo')->whereNotNull('citizenNo')->where(['status' => 0, 'softDelete' =>0, 'isDeath' =>0, ])->get();
        return $citizenInfos;
      }
      if (Auth::user()->userLevel ==='wrd') {
        $citizenInfos = CitizenInfo::select('id', 'fnameNep', 'mnameNep', 'lnameNep', 'fnameEng', 'mnameEng', 'lnameEng', 'citizenNo')->whereNotNull('citizenNo')->where(['status' => 0, 'softDelete' =>0, 'isDeath' =>0,'municipilities_id'=>Auth::user()->municipilities_id, 'wards_id' => Auth::user()->wards_id, 'isHouseOwner'=>1])->get();
        return $citizenInfos;
      }
      return redirect()->route('home')->withMessage('Oops user leve is miss match error code');
      
    }

    public function store(basicHouseValidation $request)
    {
      $date = mylogic::getRandNumber();
      $date2 = mylogic::getRandNumber();
     $isHouseNumExists = $this->checkMyHouseNumber($request->hsNum);
     if ($isHouseNumExists['hsNum']) {
      return back()->withInput()->withMessage(config('activityMessage.houseNumAlreadyExists'));
    }
    if (!$request->citizen_infos_id && !$request->hsNum) {
      return back()->withInput()->withMessage(config('activityMessage.houseAlreadyExists'));
    }

        DB::beginTransaction();    //sysRefCode
        $refCode  = mylogic::sysRefCode($tablename = $this->houseDetailsTbl, $prefix = $this->refCode);
        $identify = myLogic::idEncription($refCode); 

        try {
         $save = new HouseDetails;
         $save->surName              = $request->surName;
         $save->surPhone             = $request->surPhone;
         $save->refCode              = $refCode;
         $save->createdBy            = Auth::user()->id;
         $save->municipilities_id    = getMunicipalityData()['id'];
         $save->wards_id             = Auth::user()->wards_id;
         $save->citizen_infos_id     = $request->citizen_infos_id;
         $save->identifier           = $identify;

         $save->toleName               = $request->toleName;
         $save->streetName             = $request->streetName;
         $save->hsNum                  = $request->hsNum;
         $save->munHsNum               = $request->munHsNum;
         $save->fmTypeNam               = $request->fmTypeNam;
         $save->fmHeadPh               = $request->fmHeadPh;
         $save->hsLandLineNum          = $request->hsLandLineNum;
         $save->hsTypeNam               = $request->hsTypeNam;
         $save->noOfRom               = $request->noOfRom;
         $save->romUsgForNam               = $request->romUsgForNam;
         $save->wrdKhanda               = $request->wrdKhanda;
         $save->locAreaNam               = $request->locAreaNam;
         $save->fmMem               = $request->fmMem;
         $save->memLiving               = $request->memLiving;
         $save->hsSamipto               = $request->hsSamipto;
         $save->otherHsSamipto               = $request->otherHsSamipto;
         $save->landSamipto               = $request->landSamipto;
         $save->otherLandSamipto               = $request->otherLandSamipto;
         $save->numOfHs               = $request->numOfHs;
         $save->florNum               = $request->florNum;
         $save->romNum               = $request->romNum;
         $save->hsUsgFor               = $request->hsUsgFor;
         $save->areaOfHs               = $request->areaOfHs;
         $save->hsEstd               = $request->hsEstd;
         $save->hsFndNam               = $request->hsFndNam;
         $save->romFlorNam               = $request->romFlorNam;
         $save->otherRomFlorNam               = $request->otherRomFlorNam;
         $save->hsWallNam               = $request->hsWallNam;
         $save->otherHsWallNam               = $request->otherHsWallNam;
         $save->hsRofNam                    = $request->hsRofNam;
         $save->otherHsRofNam               = $request->otherHsRofNam;
         $save->isHsMadeByRule               = $request->isHsMadeByRule;
         $save->isHsEqResis               = $request->isHsEqResis;
         $save->haveParking               = $request->haveParking;
         $save->havePlantation               = $request->havePlantation;
         $save->haveGarden               = $request->haveGarden;
         $save->isHsOwnByHead               = $request->isHsOwnByHead;
         $save->longi               = $request->longi;
         $save->lati               = $request->lati;
         $save->numOfHs               = $request->numOfHs;
         $save->isHsOwnByHead          = 'छ';
         $save->haveSameKitchen        = $request->haveSameKitchen;

         if ($request->hasFile('hsImgPath')) {
          $imgFile = $request->file('hsImgPath');
          $filename = str_replace(' ', '', $date). '.' . $imgFile->getClientOriginalExtension();
                // dd($this->imageWidth .  $this->imageHeight . $filename. $this->houseImagePath);
          $imgPath = mylogic::cropImage($imgFile, $this->imageWidth, $this->imageHeight,$this->houseImagePath,  $filename);
          $save->hsImgPath =  $this->houseImagePath . $filename;
        }

        $mySave = $save->save();

        $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.createBasicHouseDetails') . $save->refCode, $tableName = $this->houseDetailsTbl, $tblId = $save->id);

        /*updating house number in citizen info*/
        $UpdateUsr = CitizenInfo::findOrFail($request->citizen_infos_id);
        $UpdateUsr->houseNumber = $request->hsNum;
        $usrUpdate =$UpdateUsr->update();

        if ($mySave && $log && $usrUpdate) {
          DB::commit();
          return redirect()->route('houseDetails.index')->withMessage(config('activityMessage.saveMessage'). $request->hsNum);
                // return back()->withMessage(config('activityMessage.saveMessage') .$request->hsNum);
        }else{
         return back()->withMessage(config('activityMessage.unSaveMessage'));
       }
     }
     catch (Exception $e) {
      DB::rollback();
      return back()->withMessage(config('activityMessage.dataNotInserted'));
    }
  }
  public function checkMyHouseNumber($houseNumber){
   return HouseDetails::where(['hsNum'=>$houseNumber])->first();
 }

 public function show($id)
 {
        //
 }
 public function edit($id)
 {
          // dd('helo');
  $page['page_title']       = 'Create House Details'; 
  $page['page_description'] = 'Create individual house Details';

  $houseTypeId = mylogic::getDrowDownData($tablename = $this->houseTypeTbl, $orderColumn = 'created_at', $orderby = 'DESC');
  $houseUseForId = mylogic::getDrowDownData($tablename = $this->houseUseForTbl, $orderColumn = 'created_at', $orderby = 'DESC');
  $houseFoundationId = mylogic::getDrowDownData($tablename = $this->houseFoundationTbl, $orderColumn = 'created_at', $orderby = 'DESC');
  $roomFloorTypeId = mylogic::getDrowDownData($tablename = $this->roomFloorTypeTbl, $orderColumn = 'created_at', $orderby = 'DESC');
  $familyTypeId = mylogic::getDrowDownData($tablename = $this->familyTypeTbl, $orderColumn = 'created_at', $orderby = 'DESC');
  $roomUseForId  = mylogic::getDrowDownData($tablename = $this->roomUseForTbl, $orderColumn = 'created_at', $orderby = 'DESC');
  $hsWallTypeId  = mylogic::getDrowDownData($tablename = $this->hsWallTypeTbl, $orderColumn = 'created_at', $orderby = 'DESC');
  $hsRoof  = mylogic::getDrowDownData($tablename = $this->hsRoofTbl, $orderColumn = 'created_at', $orderby = 'DESC');

  $samiptoHsId     = mylogic::drowDownDataByTagFilter($tablename = $this->samiptoTbl,$filterCol = 'tagName',$filterVal = 'house', $orderColumn = 'created_at', $orderby = 'DESC');

  $samiptoLdId     = mylogic::drowDownDataByTagFilter($tablename = $this->samiptoTbl,$filterCol = 'tagName',$filterVal = 'land', $orderColumn = 'created_at', $orderby = 'DESC');

  $houseMinInfo = HouseDetails::select('id', 'identifier')->where(['identifier' => $id])->first();

  $data = HouseDetails::findOrfail($houseMinInfo->id); 

  $citizenInfo = $this->getCitizenInfo();

  return view($this->viewPath . '.edit', compact(['page', 'data' , 'houseTypeId', 'familyTypeId', 'roomUseForId', 'samiptoHsId','samiptoLdId', 'citizenInfo', 'houseUseForId', 'houseFoundationId', 'roomFloorTypeId', 'hsWallTypeId', 'hsRoof']));
}


public function update(Request $request, $id)
{
  $validator = Validator::make($request->all(), [
    'hsNum' => 'required|min:3|max:30|unique:house_details,hsNum,' . $request->id,
    'fmHeadPh' => 'required',
    'hsEstd'        => 'required'
  ]);
  if ($validator->fails()) {
   return back()->withErrors($validator)->withInput();
 } 
 $date = mylogic::getRandNumber();
      $date2 = mylogic::getRandNumber();

 if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'wrd' || Auth::user()->userLevel === 'mun') {
   DB::beginTransaction();
   try {
    $update  = HouseDetails::findOrFail($id);

    if ($request->hasFile('hsImgPath')) {
      $imgFile = $request->file('hsImgPath');
      $filename = str_replace(' ', '', $date). '.' . $imgFile->getClientOriginalExtension();
                    // dd($this->imageWidth .  $this->imageHeight . $filename. $this->houseImagePath);
      
      $imgPath = mylogic::cropImage($imgFile, $this->imageWidth, $this->imageHeight,$this->houseImagePath,  $filename);
      $update->hsImgPath =  $this->houseImagePath . $filename;
    }

    $update->createdBy            = Auth::user()->id;
    $update->surName              = $request->surName;
    $update->surPhone             = $request->surPhone;
    $update->municipilities_id    = getMunicipalityData()['id'];
    $update->wards_id             = Auth::user()->wards_id;
    $update->citizen_infos_id     = $request->citizen_infos_id;

    $update->toleName               = $request->toleName;
    $update->streetName             = $request->streetName;
    $update->hsNum                  = $request->hsNum;
    $update->munHsNum               = $request->munHsNum;
    $update->fmTypeNam               = $request->fmTypeNam;
    $update->fmHeadPh               = $request->fmHeadPh;
    $update->hsLandLineNum          = $request->hsLandLineNum;
    $update->hsTypeNam               = $request->hsTypeNam;
    $update->noOfRom               = $request->noOfRom;
    $update->romUsgForNam               = $request->romUsgForNam;
    $update->wrdKhanda               = $request->wrdKhanda;
    $update->locAreaNam               = $request->locAreaNam;
    $update->fmMem               = $request->fmMem;
    $update->memLiving               = $request->memLiving;
    $update->hsSamipto               = $request->hsSamipto;
    $update->otherHsSamipto               = $request->otherHsSamipto;
    $update->landSamipto               = $request->landSamipto;
    $update->otherLandSamipto               = $request->otherLandSamipto;
    $update->numOfHs               = $request->numOfHs;
    $update->florNum               = $request->florNum;
    $update->romNum               = $request->romNum;
    $update->hsUsgFor               = $request->hsUsgFor;
    $update->areaOfHs               = $request->areaOfHs;
    $update->hsEstd               = $request->hsEstd;
    $update->hsFndNam               = $request->hsFndNam;
    $update->romFlorNam               = $request->romFlorNam;
    $update->otherRomFlorNam               = $request->otherRomFlorNam;
    $update->hsWallNam               = $request->hsWallNam;
    $update->otherHsWallNam               = $request->otherHsWallNam;
    $update->hsRofNam                    = $request->hsRofNam;
    $update->otherHsRofNam               = $request->otherHsRofNam;
    $update->isHsMadeByRule               = $request->isHsMadeByRule;
    $update->isHsEqResis               = $request->isHsEqResis;
    $update->haveParking               = $request->haveParking;
    $update->havePlantation               = $request->havePlantation;
    $update->haveGarden               = $request->haveGarden;
    $update->longi               = $request->longi;
    $update->lati               = $request->lati;
    $update->numOfHs               = $request->numOfHs;
    $update->haveSameKitchen       = $request->haveSameKitchen;

    $myupdate = $update->update();

    $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updatedCitizenInfo') . $update->refCode, $tableName = $this->houseDetailsTbl, $tblId = $update->id);

    if ($myupdate && $log) {
      DB::commit();
      return back()->withMessage(config('activityMessage.updatedCitizenInfo') .$request->refCode);
    }else{
     return back()->withMessage(config('activityMessage.unSaveMessage'));
   }
 }catch (Exception $e) {
  DB::rollback();
  return back()->withMessage(config('activityMessage.dataNotInserted'));
}
}else{
  return redirect()->route('home')->withMessage(config('activityMessage.unAuthorized'));
}
}

public function destroy($id)
{
        //
}
public function insertMoreDetails($id){
 $page['page_title']       = 'House more details'; 
 $page['page_description'] = 'insert house more details';  

 $hsDetails = HouseDetails::with('citizen_infos')->select('citizen_infos_id','hsNum','fmHeadPh', 'id')->where(['status' => 0, 'softDelete' => 0, 'identifier' => $id])->first();


 /*citizen information */
 $qualificationId    = mylogic::getDrowDownData($tablename = $this->qualificationTbl, $orderColumn = 'created_at', $orderby = 'ASC');

 $citizenTypesId     = mylogic::getDrowDownData($tablename = $this->citizenTypesTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 
 $occupationId = mylogic::getDrowDownData($tablename = $this->occupationTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $religiousId = mylogic::getDrowDownData($tablename = $this->religiousesTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $syslangId = mylogic::getDrowDownData($tablename = $this->sysLangTbl, $orderColumn = 'created_at', $orderby = 'ASC');
 $jatJatiId = mylogic::getDrowDownData($tablename = $this->jatjatis, $orderColumn = 'created_at', $orderby = 'ASC');
 
 $familyLivingId = mylogic::getDrowDownData($tablename = 'living_fors', $orderColumn = 'created_at', $orderby = 'ASC');

 // health and cleanesss
 $treatmentArea=myLogic::getDrowDownData($tablename=$this->treatmentAreaTbl,$orderColumn='created_at',$orderby='ASC');

 $longTermDisease = mylogic::drowDownDataByTagFilter($tablename = $this->diseaseTbl,$filterCol = 'tag',$filterVal = 'long', $orderColumn = 'created_at', $orderby = 'ASC');

// facing and udating data
 $hsCEData          = $this->getHouseMoreDetailData($tablename='children_educations', $hsDetails['id']);
 $hsSDData          = $this->getHouseMoreDetailData($tablename='school_distances', $hsDetails['id']);
 $hsCMData          = $this->getHouseMoreDetailData($tablename='child_montessoris', $hsDetails['id']);
 $hsCCIData          = $this->getHouseMoreDetailData($tablename='child_club_involves', $hsDetails['id']);
 $hsSUData          = $this->getHouseMoreDetailData($tablename='school_unadmits', $hsDetails['id']);
 $hsCHData          = $this->getHouseMoreDetailData($tablename='child_hobbies', $hsDetails['id']);
 $hsEduDropData     = $this->getHouseMoreDetailData($tablename='education_drops', $hsDetails['id']);

 $hsfemaleDisease     = $this->getHouseMoreDetailData($tablename='hs_family_female_diseases', $hsDetails['id']);


 $hsDrinkingWaterData = $this->getHouseMoreDetailData($tablename='house_drinking_waters', $hsDetails['id']);
 $hsKitchenData       = $this->getHouseMoreDetailData($tablename='house_kitchens', $hsDetails['id']);
 $hsLightData       = $this->getHouseMoreDetailData($tablename='house_lights', $hsDetails['id']);
 $hsToiletData       = $this->getHouseMoreDetailData($tablename='house_toilets', $hsDetails['id']);
 $hsWasteData       = $this->getHouseMoreDetailData($tablename='house_wastes', $hsDetails['id']);
 $hsRoadData       = $this->getHouseMoreDetailData($tablename='house_roads', $hsDetails['id']);
 $hsEdData          = $this->getHouseMoreDetailData($tablename='house_electronic_devices', $hsDetails['id']);

 $hsAbroadMem       = $this->getHouseMoreDetailData($tablename='abroad_details', $hsDetails['id']);
 $migrationDetails   = $this->getHouseMoreDetailData($tablename='migration_details', $hsDetails['id']);
 $familyDeathDetails  = $this->getHouseMoreDetailData($tablename='fm_death_details', $hsDetails['id']);
 $familyFertilityDetails  = $this->getHouseMoreDetailData($tablename='hs_fm_fertilities', $hsDetails['id']);


 $agriDetails      = $this->getHouseMoreDetailData($tablename='hs_agriculture_details', $hsDetails['id']);
 
 $agriProdDetails      = $this->getHouseMoreDetailData($tablename='hs_agriculture_productions', $hsDetails['id']);
 $animalProduct      = $this->getHouseMoreDetailData($tablename='hs_agri_animal_productions', $hsDetails['id']);
 
 $memTrain = $this->getHouseMoreDetailData($tablename='hs_member_trainings', $hsDetails['id']);

 $childRep = $this->getHouseMoreDetailData($tablename='hs_child_representatives', $hsDetails['id']);


 $industryBusinessData      = $this->getHouseMoreDetailData($tablename='industry_business_details', $hsDetails['id']);

 $incomeAndExpensesData      = $this->getHouseMoreDetailData($tablename='hs_income_expens_details', $hsDetails['id']);


 $agriOtherIncome  = $this->getHouseMoreDetailData($tablename='hs_other_income_sources', $hsDetails['id']);
 $yearlySurvival  = $this->getHouseMoreDetailData($tablename='hs_agri_pro_survials', $hsDetails['id']);
 $taxPayDetails   = $this->getHouseMoreDetailData($tablename='hs_org_tax_pays', $hsDetails['id']);
 $hsOrgDetailsData      = $this->getHouseMoreDetailData($tablename='hs_org_details', $hsDetails['id']);


 $loanDetails     = $this->getHouseMoreDetailData($tablename='hs_loan_details', $hsDetails['id']);
 $orgInv          = $this->getHouseMoreDetailData($tablename='hs_org_involvements', $hsDetails['id']);
 $naturalDiseaster = $this->getHouseMoreDetailData($tablename='hs_natural_diseasters', $hsDetails['id']);
 $haveDisProblem = $this->getHouseMoreDetailData($tablename='hs_have_natural_diseasters', $hsDetails['id']);
 $wildAnimalProblem = $this->getHouseMoreDetailData($tablename='hs_have_wild_animal_pros', $hsDetails['id']);
 $crimeVictim = $this->getHouseMoreDetailData($tablename='hs_crime_victims', $hsDetails['id']);
 $envProblem = $this->getHouseMoreDetailData($tablename='hs_environment_problems', $hsDetails['id']);
 $rentailDetails = $this->getHouseMoreDetailData($tablename='hs_rentail_details', $hsDetails['id']);

 /**********************/
 /*Health and Cleaness*/
 $hsCHC  = $this->getHouseMoreDetailData($tablename='hs_child_health_cares', $hsDetails['id']);
 $hsFHP  = $this->getHouseMoreDetailData($tablename='hs_family_health_positions', $hsDetails['id']);
 $hsHIVP  = $this->getHouseMoreDetailData($tablename='hs_fm_h_i_v_pregnancies', $hsDetails['id']);
 $hsPA  = $this->getHouseMoreDetailData($tablename='hs_pregnancy_actions', $hsDetails['id']);
 $hsPHS  = $this->getHouseMoreDetailData($tablename='hs_pregnancy_health_services', $hsDetails['id']);
 $hsFUDW  = $this->getHouseMoreDetailData($tablename='hs_fm_using_drinking_waters', $hsDetails['id']);
 $hsHWP  = $this->getHouseMoreDetailData($tablename='hs_hand_wash_processes', $hsDetails['id']);

 /**********************/
 /*  Childcare and Involvement */
 $hsCS  = $this->getHouseMoreDetailData($tablename='hs_child_securities', $hsDetails['id']);
 $hsMD  = $this->getHouseMoreDetailData($tablename='hs_marriage_details', $hsDetails['id']);
 $hsFLD  = $this->getHouseMoreDetailData($tablename='hs_family_living_details', $hsDetails['id']);
 $hsFMWD  = $this->getHouseMoreDetailData($tablename='hs_fm_mem_working_details', $hsDetails['id']);
 $hsOML  = $this->getHouseMoreDetailData($tablename='hs_other_mem_living_togs', $hsDetails['id']);


 $infantVaccine  = $this->getHouseMoreDetailData($tablename='hs_infant_vaccines', $hsDetails['id']);
 /*------------*/
 /*family relation*/
 $fmRelationId = mylogic::getDrowDownData($tablename = $this->tblFmRelation, $orderColumn = 'nameEng', $orderby = 'ASC');
 $countryId = mylogic::getDrowDownData($tablename = $this->tblCountry, $orderColumn = 'nameEng', $orderby = 'ASC');
 // $hsEdData           = json_decode($hsEdData->hsDevices);

 $disablitiyStatus = mylogic::getDrowDownData($tablename = 'disabilities', $orderColumn = 'created_at', $orderby = 'ASC');

 $maritialStatus = mylogic::getDrowDownData($tablename ='maritial_statuses', $orderColumn = 'nameEng', $orderby = 'ASC');

 if ($hsDetails && $hsDetails['citizen_infos_id']){
   /* Drinking Water*/
   $waterSource = mylogic::getDrowDownData($tablename = $this->tblWaterSource, $orderColumn = 'created_at', $orderby = 'ASC');
   $drinkingWaterSource = mylogic::getDrowDownData($tablename = $this->drinkingWaterSourceTbl, $orderColumn = 'created_at', $orderby = 'ASC');
   $waterAvailabilities = mylogic::getDrowDownData($tablename = $this->waterAvailabilitiesTbl, $orderColumn = 'created_at', $orderby = 'ASC');
   $qualityWaters = mylogic::getDrowDownData($tablename = $this->qualityDrinkingTbl, $orderColumn = 'created_at', $orderby = 'ASC');
   $samiptoHsId     = mylogic::drowDownDataByTagFilter($tablename = $this->samiptoTbl,$filterCol = 'tagName',$filterVal = 'drinkingWater', $orderColumn = 'created_at', $orderby = 'ASC');
   $whyNotId     = mylogic::drowDownDataByTagFilter($tablename = $this->whyNotTbl,$filterCol = 'tag',$filterVal = 'drinkingWater', $orderColumn = 'created_at', $orderby = 'ASC');

   /* Kitchen*/
   $cookingFuel = mylogic::getDrowDownData($tablename = $this->cookingFuelTbl, $orderColumn = 'created_at', $orderby = 'ASC');
   $ovenType = mylogic::getDrowDownData($tablename = $this->ovenTypeTbl, $orderColumn = 'created_at', $orderby = 'ASC');

   /*Light*/
   $lightSource= mylogic::getDrowDownData($tablename = $this->lightSoureTbl, $orderColumn = 'created_at', $orderby = 'ASC');
   $whyNotLight    = mylogic::drowDownDataByTagFilter($tablename = $this->whyNotTbl,$filterCol = 'tag',$filterVal = 'electricity', $orderColumn = 'created_at', $orderby = 'ASC');

   /*Toilet*/
   $toiletType=mylogic::getDrowDownData($tablename = $this->toiletTypeTbl, $orderColumn = 'created_at', $orderby = 'ASC');

   /*Waste Management*/
   $wasteMangement    = mylogic::drowDownDataByTagFilter($tablename = $this->wasteManagementTbl,$filterCol = 'tag',$filterVal = 'Waste', $orderColumn = 'created_at', $orderby = 'ASC');
   $wasteWaterMangement    = mylogic::drowDownDataByTagFilter($tablename = $this->wasteManagementTbl,$filterCol = 'tag',$filterVal = 'Water', $orderColumn = 'created_at', $orderby = 'ASC');
   /*Road*/
   $road=mylogic::getDrowDownData($tablename = $this->roadTbl, $orderColumn = 'created_at', $orderby = 'ASC');
   /*Facilities*/
   $electricalDevice=mylogic::getDrowDownData($tablename = $this->electricalDeviceTbl, $orderColumn = 'created_at', $orderby = 'ASC');
   $educationLevel=mylogic::getDrowDownData($tablename = $this->educationLevelTbl, $orderColumn = 'created_at', $orderby = 'ASC');
   $vaccineName=myLogic::getDrowDownData($tablename=$this->vaccineTbl,$orderColumn='created_at',$orderby='ASC');
   $allDisease=myLogic::getDrowDownData($tablename=$this->diseaseTbl,$orderColumn='created_at',$orderby='ASC');
   $longTermDisease = mylogic::drowDownDataByTagFilter($tablename = $this->diseaseTbl,$filterCol = 'tag',$filterVal = 'long', $orderColumn = 'created_at', $orderby = 'ASC');
   /*Family Member */
   $familyMember = $this->getMemberNumber($hsDetails['citizen_infos_id']);

   /*Children Education */
   /*Education Drop */   
   $whyNotEdu = mylogic::drowDownDataByTagFilter($tablename = $this->whyNotTbl,$filterCol = 'tag',$filterVal = 'educationDrop', $orderColumn = 'created_at', $orderby = 'ASC');

   $getMyFamilyDetails  =  mylogic::getMyFamilyDetails($hsDetails['citizen_infos_id']); 
   return view($this->viewPath . '.houseMoreDetails', compact(['page', 'hsDetails', 'waterSource','samiptoHsId','whyNotId','drinkingWaterSource','qualityWaters','waterAvailabilities','cookingFuel','ovenType','lightSource','whyNotLight','toiletType','wasteMangement','wasteWaterMangement','road','electricalDevice', 'educationLevel', 'hsDrinkingWaterData', 'hsKitchenData', 'hsLightData', 'hsToiletData', 'hsWasteData', 'hsRoadData', 'familyMember', 'hsEdData',  'hsCEData', 'hsSDData', 'hsCMData', 'hsCCIData', 'qualificationId', 'occupationId', 'religiousId', 'syslangId', 'jatJatiId', 'fmRelationId', 'getMyFamilyDetails', 'countryId', 'hsAbroadMem', 'migrationDetails', 'hsSUData', 'hsCHData', 'whyNotEdu', 'hsEduDropData', 'familyDeathDetails', 'vaccineName', 'allDisease', 'longTermDisease', 'hsfemaleDisease', 'infantVaccine', 'hsCHC','treatmentArea', 'hsFHP', 'familyFertilityDetails', 'hsHIVP', 'hsPA', 'hsPHS', 'hsFUDW', 'hsHWP', 'hsCS', 'hsMD', 'hsFLD', 'hsFMWD', 'hsOML', 'agriDetails', 'agriOtherIncome', 'yearlySurvival', 'taxPayDetails', 'loanDetails', 'orgInv', 'naturalDiseaster', 'haveDisProblem', 'wildAnimalProblem', 'crimeVictim', 'envProblem', 'agriProdDetails', 'rentailDetails', 'incomeAndExpensesData', 'animalProduct', 'industryBusinessData', 'hsOrgDetailsData', 'familyLivingId', 'memTrain', 'childRep', 'disablitiyStatus', 'maritialStatus', 'citizenTypesId']));
 }
 else{
  return back()->withMessage(config('activityMessage.idNotFound'));
}
}

public function getHouseMoreDetailData($tablename, $houseDetailsId){  
 if (Auth::user()->userLevel =='mun' || Auth::user()->userLevel == 'dev') {
  $returnData =  DB::table($tablename)->where('municipilities_id', Auth::user()->municipilities_id)->where('house_details_id', $houseDetailsId)->first();
  return $returnData;
}

if (Auth::user()->userLevel =='wrd') {
  $returnData =  DB::table($tablename)->where('wards_id', Auth::user()->wards_id)->where('house_details_id', $houseDetailsId)->first();
  return $returnData;
}
}

  public function getMemberNumber($id){   // member house details
    $familyMember = CitizenInfo::select('son', 'daughter', 'other')->where(['id'=>$id])->first();
    return $familyMember;
  }
  
  public function changeGharMuli($muliSlug){
    $page['page_title']       = 'Change Ghar Muli';
    $page['page_description'] = 'change existing ghar muli';

    $muliId = mylogic::getCitizenByIdEncript($muliSlug)['id'];
    $getRelationData = CitizenRelation::select('citizens_id', 'relationName', 'relation_id')->where(['citizens_id' => $muliId])->get();

    return view($this->viewPath . '.changeGharMuli', compact(['page', 'getRelationData']));
    
     
  }

}
