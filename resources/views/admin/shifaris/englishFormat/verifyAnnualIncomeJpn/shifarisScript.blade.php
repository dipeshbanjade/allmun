<script type="text/javascript">
var currencyType = $('#currency_type');
var currencyRate = $('#exchange_rate');
var addRowBtn = $('#addNewRowInTable');
var clickCount = 1;
var totalAnnualIncome = 0;
var totalAnnualIncomeFCurrency = 0;
var firstYearInput = document.querySelectorAll('.first_year_income');
var firstYearInputLength = firstYearInput.length;

addRowBtn.on('click', function(e){
	var firstYearInput = document.getElementsByClassName('first_year_income');
	var firstYearInputLength = firstYearInput.length + 1;

	e.preventDefault();

	clickCount++;

	var serialNumber = '<input type="text" class="dashed-input-small-field" required="required" name="" value="' + firstYearInputLength + '">';

	var sourceIncome = '<input type="text" class="dashed-input-field" placeholder="   *" required="required" name="incomeSource[]">';

	var firstYear = '<input type="text" class="dashed-input-field first_year_income" placeholder="   *" required="required" name="firstYear[]">';
	var secondYear = '<input type="text" class="dashed-input-field second_year_income" placeholder="   *" required="required" name="secondYear[]">';
	var thirdYear = '<input type="text" class="dashed-input-field third_year_income" placeholder="   *" required="required" name="thirdYear[]">';

	var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

	var insertNewRow = "<tr class='tblRow'>"
	+"<td>"+serialNumber+"</td>"
	+"<td>"+sourceIncome+"</td>"
	+"<td>"+firstYear+"</td>"
	+"<td>"+secondYear+"</td>"
	+"<td>"+thirdYear+"</td>"
	+"<td class='cancel-btns' style='border:0px;' >"+cancelButton+"</td>"
	+"</tr>";

	$('#annualIncomeJapanTd').append(insertNewRow);

	$('.btnRemoveItem').on('click', function(e){
		$(this).parent().parent().remove();
		calcAllTotalAmount();
		manageSerialNumber();
		return false;
	});

	addListener();
});

currencyRate.on('keyup', function(e){
	calcAllTotalAmount();
});

function manageSerialNumber(){
	var firstYearInput = document.getElementsByClassName('first_year_income');

	var firstYearInputLength = firstYearInput.length;
}


function addListener(){
	var firstYearInput = document.getElementsByClassName('first_year_income');
	var secondYearInput = document.getElementsByClassName('second_year_income');
	var thirdYearInput = document.getElementsByClassName('third_year_income');

	var rowNumber = firstYearInput.length - 1;

	firstYearInput[rowNumber].addEventListener('keyup', function(){
		calcTotalAmount('.first_year_income');
	});

	secondYearInput[rowNumber].addEventListener('keyup', function(){
		calcTotalAmount('.second_year_income');
	});

	thirdYearInput[rowNumber].addEventListener('keyup', function(){
		calcTotalAmount('.third_year_income');
	});
}

function calcTotalAmount(className){
	totalAnnualIncome = 0;
	var inputField = document.querySelectorAll(className);
	var inputFieldLength = inputField.length;

	for (var i = 0; i < inputFieldLength; i++) {
		if(inputField[i].value == '' || inputField[i].value == null){
			continue;
		}
		totalAnnualIncome += parseInt(inputField[i].value);
	}

	var totalNrsFieldToBeUpdate;
	var totalForeignFieldToBeUpdate;

	if(className == '.first_year_income'){
		totalFieldToBeUpdate = '#totalNrsInFirst';
		totalForeignFieldToBeUpdate = '#totalUsdInFirst';
	}else if(className == '.second_year_income'){
		totalFieldToBeUpdate = '#totalNrsInSecond';
		totalForeignFieldToBeUpdate = '#totalUsdInSecond';
	}else if(className == '.third_year_income'){
		totalFieldToBeUpdate = '#totalNrsInThird';
		totalForeignFieldToBeUpdate = '#totalUsdInThird';
	}

	addtotalAmmount(totalAnnualIncome, totalFieldToBeUpdate, totalForeignFieldToBeUpdate);
}

function amountInForeignCurrency(totalAmt, foreignCurrencyRate){
	return totalAmt/foreignCurrencyRate;
}

function addtotalAmmount(totalAnnualIncome, totalFieldToBeUpdate, totalForeignFieldToBeUpdate){

	$(totalFieldToBeUpdate).val(totalAnnualIncome);

	var foreignCurrencyRate = currencyRate.val();
	$(totalForeignFieldToBeUpdate).val(amountInForeignCurrency(totalAnnualIncome, foreignCurrencyRate));
	$('#totalNrs').val($('#totalNrsInThird').val());
	$('#total_usd').val($('#totalUsdInThird').val());
}

function initiateKeyup(){
	$('.first_year_income').first().on('keyup', function(){
		calcTotalAmount('.first_year_income');
	});

	$('.second_year_income').first().on('keyup', function(){
		calcTotalAmount('.second_year_income');
	});

	$('.third_year_income').first().on('keyup', function(){
		calcTotalAmount('.third_year_income');
	});
}

function calcAllTotalAmount(){
	calcTotalAmount('.first_year_income');
	calcTotalAmount('.second_year_income');
	calcTotalAmount('.third_year_income');
}

initiateKeyup();
calcAllTotalAmount();

currencyType.on('change', function(){
	var ctype = document.querySelectorAll('#ctype');
	var currency = ctype.length;
	for (var i = 0; i < currency; i++) {
		ctype[i].value = this.value;
	}
});

$(function(){
	/*add buttoin*/
	
	$("form[name='frmWardverifyAnnualIncomeJpn']").validate({
		rules:{
			issuedDate : {
				required : true,
				date : true
			},
			title : {
				required : true
			},
			personName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			relationWithSeeker : {
				required : true
			},
			fatherName : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			municipality : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			ward : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			district : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			former : {
				required : true
			},
			orgAddr : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			orgType : {
				required : true
			},
			orgWard : {
				required : true,
				minlength : 1,
				maxlength : 3,
				number: true
			},
			japanAnnualIncomeDetail : {
				required : true,
				minlength : 3,
				
			},
			currency_type: {
				required: true
			},
			totalNrsInFirst : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			totalNrsInSecond : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			totalNrsInThird : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			totalUsdInFirst : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			totalUsdInSecond : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			totalUsdInThird : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			total_nrs : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			ctype : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			totalUsd : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			authorizedPerson : {
				required : true,
				minlength : 3,
				maxlength : 30
			},
			exchangeRate : {
				required : true,
				minlength : 1,
				maxlength : 30
			}
		},
		messages: {

			issuedDate : "Issue Date is required",
			title : "Title is required",
			personName : "Person Name is required",
			relationWithSeeker : "Relation must be defined",
			fatherName : "Father Name is required",
			municipality : "Municipality is required",
			ward : "Ward Number must be number",
			district : "District is required",
			former : "Former is required",
			orgAddr : "Organization Address is required",
			orgType : "Organization Type is required",
			orgWard : "Organization Ward must be number",
			japanAnnualIncomeDetail : "Income Detail is required",
			currency_type: "Currency Type is required",
			totalNrsInFirst : "Total income of fiscal Year 2016/2017 is required",
			totalNrsInSecond : "Total income of fiscal Year 2017/2018 is required",
			totalNrsInThird : "Total income of fiscal Year 2018/2019 is required",
			totalUsdInFirst : "Total income of fiscal Year 2016/2017 in USD is required",
			totalUsdInSecond : "Total income of fiscal Year 2017/2018 in USD is required",
			totalUsdInThird : "Total income of fiscal Year 2018/2019 in USD is required",
			total_nrs : "Total income in Nrs. is required",
			ctype : "Currency Type is required",
			totalUsd : "Total income in USD is required",
			authorizedPerson : "Authorized person is required",
			exchangeRate : "Exchange Rate is required"

		}
	});
});
$('#nebedakId').on('change',function(){
  nebedakId = $(this).val();
  if (nebedakId.length >0 ) {
      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
      $.ajax({
          'type' : 'GET',
          'url'  : url,
          success : function(response){
              console.log(response);
                  $("input[name='personName']").val(response.fnameEng + ' ' + response.mnameEng+ ' '+ response.lnameEng).trigger('input');
            
          },complete:function(){
          }
      })
      .fail(function (response) {
          alert('data not found ');
      });
  }else{

  }
});
</script>