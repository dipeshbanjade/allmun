<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\LeaveType;
use App\model\myLogic;

class leaveTypeController extends Controller
{

    protected $viewPath = 'admin.setting.leaveType';
    protected $leaveType = 'leave_types';
    protected $refCode;


    public function __construct()
    {
        $this->middleware('auth');
        $this->refCode = config('activityMessage.leaveType');
    }

    public function index()
    {
        $page['page_title']       = 'Leave Type : List';
        $page['page_description'] = 'list of all leave type';

        $allLeaveType = LeaveType::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

        $refCode   = myLogic::sysRefCode($tablename = $this->leaveType, $prefix = $this->refCode);

        return view($this->viewPath . '.index', compact(['page', 'allLeaveType', 'refCode']));
    }
  
    public function store(Request $request)
    {
        $myStore = LeaveType::create([
                   'refCode' => $request->refCode,
                   'nameNep' => $request->nameNep ?? 'N/A',
                   'nameEng' => $request->nameEng,
                   'daysLeave' => $request->daysLeave,
                   'status'  => 0,
                   'softDelete' => 0
                  ]);

        if ($myStore) {
            return back()->withMessage(config('activityMessage.saveMessage'));
        }else{
            return back()->withMessage(config('activityMessage.unSaveMessage'));
        }
    }

    public function edit($id)
    {
        $data = LeaveType::findOrFail($id);
        if ($data) {
            return response()->json([
                    'success' => true,
                    'id'      => $data->id,
                    'refCode' => $data->refCode,
                    'nameNep' => $data->nameNep,
                    'nameEng' => $data->nameEng,
                    'daysLeave' => $data->daysLeave
                   ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => config('activityMessage.dataNotFound')
                ]);
        }
    }

    public function update(Request $request, $id)
    {
        // $validator = Validator::make($request->all(), [
        //      'refCode'      => 'required|unique:leave_types,refCode,'.$request->id,
        //      'nameEng'  => 'required|min:4|max:20'

        //  ]);

        // if ($validator->fails()) {
        //     return response()->json([
        //            'success' => false,
        //            'message' => $validator->errors()->all()
        //         ]);
        // }
        $update = LeaveType::findOrFail($id);
        $update->refCode = $request->refCode;
        $update->nameNep = $request->nameNep ?? 'N/A';
        $update->nameEng = $request->nameEng;
        $update->daysLeave = $request->daysLeave;

       $myUpdate = $update->update();
        if ($myUpdate) {
            return back()->withMessage(config('activityMessage.updateMessage'));
        }else{
            return back()->withMessage(config('activityMessage.notUpdated'));
        }
    }

    public function destroy($id)
    {
        $del = LeaveType::findOrFail($id);
        $del->softDelete = 1;
        $myDel = $del->update();
        if ($myDel) {
          return MyLogic::successChangeStatus($code = 200);
        }else{
           return MyLogic::successChangeStatus($code = 301);
        }
    }

    public function changeStatus(){
     
    }


}
