<?php

namespace App\Http\Controllers\admin\shifaris\other;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\shifaris\ApplicantUser;
use App\model\shifaris\DiffEngSpellCert;
use App\model\setting\Degination;
use App\Http\Requests\admin\shifaris\diffEngSpellCertVal;
use DB;
use Auth;

use App\model\mylogic;

class DiffEngSpellCertController extends Controller
{
  protected $viewPath = 'admin.shifaris.other.diffEngSpellCert';
  protected $shifarisTableName     = 'diff_eng_spell_certs';
  protected $appUsrTable     = 'applicant_users';
  protected $deg             = 'deginations';     
  protected $prefix;



  public function __construct()
  {
    $this->middleware('auth');
    $this->prefix      = config('activityMessage.prefixDiffEngSpellCert');
  }

  public function index()
  {
    $page['page_title']       = 'Different English Spell Certified';
    $page['page_description'] = 'Different English Spell Certified';

    $instRecomd = DiffEngSpellCert::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));

  }

  public function create()
  {
    $page['page_title']       = 'Different English Spell Certified : create';
    $page['page_description'] = 'Different English Spell Certified details';

    $refCode  = mylogic::sysRefCode($tablename = $this->shifarisTableName, $prefix = $this->prefix);


    $deginationsId = myLogic::getDrowDownData($tablename = $this->deg, $orderColumn = 'nameNep', $orderby = 'ASC'); 
        // dd($deginationsId);
    return view($this->viewPath . '.create', compact(['page', 'refCode', 'deginationsId']));

  }

  public function store(diffEngSpellCertVal $request)
  {

   DB::beginTransaction();
   try {

    $diffHijjeCount = count($request->diffHijje);

    $diffHijje = $request->diffHijje;
    $diffPaper = $request->diffPaper;

    $diffHijjeArray = [];

    for ($i=0; $i < $diffHijjeCount; $i++) {
      array_push($diffHijjeArray, [
        'diffHijje' => $diffHijje[$i],
        'diffPaper' => $diffPaper[$i]
      ]);
    }
    $diffHijjeJson = json_encode($diffHijjeArray);

            //$appUsr = myLogic::applicantUser($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->appUsrTable, $tblId = $request->refCode, $request->applicantName, $request->applicantAddress, $request->citizenshipNumber, $request->phoneNumber, $request->email, $request->dob, $request->fatherName, $request->profilePic, $request->citizenshipImgPath);

    
    $isShifarisExist = isset($request->shifarisId) ? DiffEngSpellCert::select('id')->where(['id'=>$request->shifarisId])->first() : NULL;
    
    $save = !empty($isShifarisExist['id']) ? DiffEngSpellCert::findOrfail($isShifarisExist['id']) : new DiffEngSpellCert;

    $save->createdBy = Auth::user()->id;

    $save->refCode  = $request->refCode;

    $save->wards_id  = Auth::user()->wards_id ?? NULL;

    $save->municipilities_id = getMunicipalityData()['id'] ?? "";
    $save->issuedDate  = $request->issuedDate;            
    $save->chalaniNumber = $request->chalaniNumber;
    $save->municipalityName = $request->municipalityName;
    $save->wardNumber = $request->wardNumber;
    $save->municipalityAddress = $request->municipalityAddress;
    $save->municipalityType = $request->municipalityType;
    $save->municipalityWard = $request->municipalityWard;
    $save->personPrefix = $request->personPrefix;
    $save->personName = $request->personName;
    $save->trueDocument = $request->trueDocument;            
    $save->dataTable = $diffHijjeJson;

    $save->authorizedPerson = $request->authorizedPerson;
    $save->deginations_id  = $request->deginations_id;
    $citizenInfo = explode('$sep!', $request->citizenshipNumber);        
            $save->applicant_users_id  = $citizenInfo[0];              

    $mySave = $save->save();

    $log = myLogic::insertInLogFile($userId = Auth::user()->id, $action = config('activityMessage.updateForm') . 'Municipality', $tableName = $this->shifarisTableName, $tblId = $save->id);

    $request->chalaniNum = $request->chalaniNumber;

    $citizenShifarish = true;
    if(empty($isShifarisExist['id'])){
      $citizenShifarish = myLogic::insertCitizenShifarish($citizenInfo[0], $this->shifarisTableName, $save->id, $request->chalaniNum, $request->issuedDate, $this->viewPath, $citizenInfo[1]);            
    }            
    if ($mySave && $log && $citizenShifarish) {
     DB::commit();
     return response()->json([
       'success' => true,
       'message' => config('activityMessage.saveMessage')
     ]);
               // return back()->withMessage(config('activityMessage.saveMessage') .$request->nameEng);
   }else{
     return response()->json([
       'success' => false,
       'message' => config('activityMessage.unSaveMessage')
     ]);
               // return back()->withMessage(config('activityMessage.unSaveMessage'));
   }
 }catch (Exception $e) {
  DB::rollback();
  return back()->withMessage(config('activityMessage.dataNotInserted'));
}


}

public function show($id)
{

}

public function edit($id)
{
        //
}

public function update(Request $request, $id)
{
        //
}

public function destroy($id)
{
        //
}

public function changeStatus(){

}
}
