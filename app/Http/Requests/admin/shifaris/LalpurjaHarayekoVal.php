<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class LalpurjaHarayekoVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'refCode' => 'required|min:3|unique:lalpurja_harayekos',
            'issuedDate' => 'required',
            'chalaniNumber' => 'required|unique:lalpurja_harayekos',
            'accountHolderName' => 'required',
            'accountHolderAddress' => 'required',
            'applicantName1' => 'required',
            'municipalityName' => 'required',
            'wardNumber1' => 'required',
            'address1' => 'required',
            'kittaNumber' => 'required',
            'area' => 'required',
            'wardNumber2' => 'required',
            'address2' => 'required',
            'applicantName2' => 'required',
            'applicant' => 'required',
            'citizenshipNumber' => 'required',
            'issuedDate2' => 'required',
            'fatherName' => 'required',
            'grandFatherName' => 'required',
            'authorizedPerson' => 'required',
            'deginations_id' => 'required',
        ];
    }
}
