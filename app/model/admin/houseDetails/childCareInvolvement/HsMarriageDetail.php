<?php

namespace App\model\admin\houseDetails\childCareInvolvement;

use Illuminate\Database\Eloquent\Model;

class HsMarriageDetail extends Model
{
	protected $table = 'hs_marriage_details';
}
