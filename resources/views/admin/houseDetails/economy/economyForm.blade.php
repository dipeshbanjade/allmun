<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row color_red">
                <!-- 1 -->
                <div class="form-group col-md-12">
                    <p>
                       १. तपाईको परिवारले कृषि कार्यका लागि जग्गा प्रयोग गरेको छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                            Goto -> ८.४  
                        </label>
                    </div>
                </div> 
                <!-- 2 -->
                <div class="form-group col-md-12">
                    <p>
                       २. यदि परिवारले कृषि कार्यका लागि जग्गा प्रयोग गरेको छ भने सो जग्गाको विवरण (क्षेत्रफल उल्लेख गर्ने) 
                    </p>
                    <div class="col-md-12">
                        <label class="col-md-12 col-sm-12 col-lg-6 ">
                            २.१. आफ्नो परिवारको नाममा रहेको जग्गाः
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Area', 'id' => '', 'required' => 'required']) }}
                            (रोपनी/आना/पैसा)
                        </label> 
                        <label class="col-md-12 col-sm-12 col-lg-6 ">
                            २.२ परिवार बाहेक अरुको नाममा रहेको जग्गाः
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Area', 'id' => '', 'required' => 'required']) }}
                            (रोपनी/आना/पैसा) 
                        </label>
                        <label class="col-md-12 col-sm-12 col-lg-6 ">
                            २.३ अन्य स्वामित्व खुलाउनेः
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Area', 'id' => '', 'required' => 'required']) }}
                            (रोपनी/आना/पैसा) 
                        </label>
                    </div>
                </div>
                <!-- 3 -->
                <div class="form-group col-md-12">
                    <p>
                       ३. तपाईको परिवारको वार्षिक बाली उत्पादन तथा बिक्रि परिमाण कति रहेको छ ? (गत कृषि वर्ष भित्र )
                    </p>
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                  <th>बाली</th>
                                  <th>उत्पादन (क्विन्टल)</th>
                                  <th>खेती गरिएको जग्गाकोक्षेत्रफल, रोपनी</th>
                                  <th>विक्री परिमाण (क्विन्टल)</th>
                            </tr>
                            <!-- First row for annaBali starts from here -->
                            <tr>
                                  <th>(क) अन्नबाली </th>
                                  <th colspan="3"></th>
                            </tr>
                            <tr>
                                <td>
                                   धान          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   मकै   
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   गहुँ          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   कोदो                                    
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अन्य          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <!-- First row for annaBali Ends here -->

                            <!-- Second row  starts from here -->
                            <tr>
                                  <th>(ख) दलहन बाली</th>
                                  <th colspan="3"></th>
                            </tr>
                            <tr>
                                <td>
                                   मास                    
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   मकै   
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   रहर            
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   मुसरो                                                
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   चना                    
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   सिमी                             
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   भट्टमास                              
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अन्य                             
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <!-- Second row  Ends here -->

                            <!-- Third row  starts from here -->
                            <tr>
                                  <th>(ग) तेलहन बाली</th>
                                  <th colspan="3"></th>
                            </tr>
                            <tr>
                                <td>
                                   तोरी/सस्र्यु          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   आलस/तिल   
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   सूर्यमुखी                      
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अन्य                                     
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <!-- Third row  Ends here -->

                            <!-- Fourth row  starts from here -->
                            <tr>
                                  <th>(घ) तरकारी बाली</th>
                                  <th colspan="3"></th>
                            </tr>
                            <tr>
                                <td>
                                   आलु                    
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   बन्दागोभी/काउली÷रायो   
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   बोडी                               
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   गोलभेंडा                                              
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   काँक्रो, लौका,फर्सी                                              
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   च्याउ 
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   करेला, घिरौला, चिचिण्डो 
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अन्य 
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <!-- Fourth row  Ends here -->

                            <!-- Fifth row  starts from here -->
                            <tr>
                                  <th>(ङ) मसला बाली</th>
                                  <th colspan="3"></th>
                            </tr>
                            <tr>
                                <td>
                                   अदुवा/वेसार                    
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   प्याज/लसुन   
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   खुर्सानी                                        
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अन्य                                                       
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>

                            <!-- Fifth row  Ends here -->

                            <!-- Sixth row  starts from here -->
                            <tr>
                                  <th>(च) फलफूल</th>
                                  <th colspan="3"></th>
                            </tr>
                            <tr>
                                <td>
                                   आँप                              
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   लिची           
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   केरा                                                 
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   मेवा                                                                
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   सुन्तला/कागती/निवुवा (अमिलोेजात)                                                                 
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   नासपति/आरुवखडा                                                                   
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   रुख कटहर                                                                
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   भुई कटहर                                                                
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अम्बा                                                                            
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   किवी                                                                         
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   कफी                                                                          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अलैची                                                                            
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   उखु                                                                                      
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>


                            <!-- Sixth row  Ends here -->

                        </table>
                    </div>
                </div> 
                <!-- 4 -->
                <div class="form-group col-md-12">
                    <p>
                       ४. तपाईको परिवारले कुनै चौपाया तथा पंक्षी पाल्नु भएको छ ? 
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                            Goto -> ८.६
                        </label>
                    </div>
                </div>
                <!-- 5 -->
                <div class="form-group col-md-12">
                    <p>
                       ५. यदि छ भने, चौपाया तथा पशुपंक्षीहरुको विवरण दिनुहोस (संख्या उल्लेख गर्ने) । 
                    </p>
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                  <th>क्र.सं</th>
                                  <th>चौपाया तथा पशुपंक्षी</th>
                                  <th>संख्या</th>
                                  <th>दुध उत्पादन(लिटर)</th>
                                  <th>मासु उत्पादन(के.जि.)</th>
                                  <th>अण्डा(संख्या)</th>
                                  <th>उन(के.जि.)</th>
                                  <th>हाड / छाला(के.जि.)</th>
                                  <th>अन्य</th>
                                  <th>विक्रीबाट बार्षिक आम्दानी (रु.)</th>
                            </tr>
                            <tr>
                                <td>१.</td>
                                <td>गाईगोरु / बाच्छाबाच्छी</td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>२.</td>
                                <td>राँगाभैसी / पाडापाडी</td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>३.</td>
                                <td>भेडा / बाखा</td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>४.</td>
                                <td>सुँगुर / वंगुर</td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>५.</td>
                                <td>कुखुरा / हाँस</td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>६.</td>
                                <td>परेवा / बट्टाई</td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- 6 -->
                <div class="form-group col-md-12">
                    <p>
                      ६. तपाईको परिवारमा माछापालन, मौरीपालन र फूल खेती गरिएको छ भने विवरण दिनुहोस् ? 
                    </p>
                    <div class="col-md-12">
                        <label class="col-md-6 ">
                            १. माछापालन पोखरी संख्याः
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'numbers', 'id' => '', 'required' => 'required']) }}
                            
                        </label> 
                        <label class="col-md-6">
                            पोखरीको क्षेत्रफलः
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'area', 'id' => '', 'required' => 'required']) }}
                        </label>
                        <label class="col-md-6">
                             रोपनी  बार्षिक उत्पादनः 
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'K.G.', 'id' => '', 'required' => 'required']) }}
                        </label>
                    </div>
                    <br>
                    <br>
                    <div class="col-md-12">
                        <label class="col-md-6 ">
                            २. मौरी घार संख्याः
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'numbers', 'id' => '', 'required' => 'required']) }}
                        </label> 
                        <label class="col-md-6">
                            बार्षिक मह उत्पादनः 
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'K.G.', 'id' => '', 'required' => 'required']) }}
                           
                        </label>
                    </div>
                    <br>
                    <br>
                    <div class="col-md-12">
                        <label class="col-md-6 ">
                            ३. फूल खेती गरेको जग्गाको क्षेत्रफल:
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'रोपनी', 'id' => '', 'required' => 'required']) }}
                        </label> 
                        <label class="col-md-6">
                            फूल खेतीबाट भएको वार्षिक आम्दानी ः 
                                {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'Rs', 'id' => '', 'required' => 'required']) }}
                        </label>
                    </div>
                </div>
                <!-- 7 -->
                <div class="form-group col-md-12">
                    <p>
                       ७. आफ्नो कृषि उत्पादनले तपाईको परिवारलाई कति महिना खान पुग्छ ? (एकमा मात्र ठीक चिन्ह लगाउनुहोस) ।
                    </p>
                    <div>
                        <label class="radio-inline col-md-6">
                            १) ३ महिनासम्म
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २) ४ देखि ६ महिना सम्म
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३) ७ देखि ९ महिनासम्म
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ४) ९ महिना भन्दा बढी 
                            <input type="radio" name="radio" value="">
                        </label>
                    </div>
                </div> 
                <!-- 8 -->
                <div class="form-group col-md-12">
                    <p>
                       ८. यदि तपाईको आफ्नो घर, उद्योगधन्दा वा व्यवसाय छ भने, तपाई नगरपालिकालाई नियमित कर तिर्नुहुन्छ ?  
                    </p>
                    <div>
                        <label class="radio-inline col-md-6">
                             १. छ
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन
                            <input type="radio" name="radio" value="">
                        </label>
                    </div>
                    <div class="col-md-12">
                        <p>
                            यदि छ भने, कुन कुन कर तिर्नुहुन्छ ?  (बहुउत्तर सम्भव छ)  
                        </p>
                        <label class="radio-inline col-md-6">
                             १. सम्पत्ति कर
                            <input type="radio" name="tax" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            २. घर बहाल कर
                            <input type="radio" name="tax" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३. व्यवसाय कर 
                            <input type="radio" name="tax" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                           
                            {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)', 'id' => '', 'required' => 'required']) }}
                        </label>
                    </div>
                </div>
                <!-- 9 -->
                <div class="form-group col-md-12">
                    <p>
                       ९. तपाईको परिवारका कुनै सदस्यले यस महानगरपालिका भित्र उद्योग / व्यवसाय / प्रतिष्ठान संचालन गर्नु भएको छ ?  
                    </p>
                    <div>
                        <label class="radio-inline col-md-6">
                             १. छ
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन
                            <input type="radio" name="radio" value="">
                        </label>
                    </div>
                    <div class="col-md-12">
                        <p>
                            यदि भएमा परिवार (सवै सदस्यको जोडेर) ले संचालनमा ल्याएका कति वटा छन् ?   
                        </p>
                        <label class="radio-inline">
                            {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'वटा', 'id' => '', 'required' => 'required']) }}
                        </label>
                    </div>
                </div>
                <!-- 10 -->
                <div class="form-group col-md-12">
                    <p>
                       १०. तपाईको परिवारले संचालन गरेका प्रतिष्ठान सरकारी निकायमा दर्ता गर्नु भएको छ ? 
                    </p>
                    <div>
                        <label class="radio-inline col-md-6">
                             १. छ
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन
                            <input type="radio" name="radio" value="">
                             goto ->१३
                        </label>
                    </div>
                    <div class="col-md-12">
                        <p>
                            यदि १ भन्दा बढी प्रतिष्ठान भएमा,
                        </p>
                        <label class="radio-inline col-md-6">
                            १. दर्ता गरेको संख्या ः
                            {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'number', 'id' => '', 'required' => 'required']) }}
                        </label>
                        <label class="radio-inline col-md-6">
                            २. दर्ता नगरेको संख्या ः
                            {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'number', 'id' => '', 'required' => 'required']) }}
                        </label>
                    </div>
                </div>
                <!-- 11 -->
                <div class="form-group col-md-12">
                    <p>
                       ११. तपाईको परिवारले संचालन गरेको व्यवसायको स्थायी लेखा नम्बर लिनु भएको छ ?
                    </p>
                    <div>
                        <label class="radio-inline col-md-6">
                             १. छ
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन
                            <input type="radio" name="radio" value="">
                             goto ->१३
                        </label>
                    </div>
                </div>
                <!-- 12 -->
                <div class="form-group col-md-12">
                    <p>
                       १२. तपाईको परिवारले संचालन गरेको प्रतिष्ठान / व्यवसाय कुन कुन सरकारी निकायमा दर्ता छ ? 
                    </p>
                    <div>
                        <label class="radio-inline col-md-6">
                             (क) घरेलु तथा साना उद्योग कार्यालय / शाखा
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            (ख) कम्पनी रजिष्टारको कार्यालय 
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            (ग) उद्योग विभाग 
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            (घ) वाणिज्य विभाग 
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            (ङ) जिल्ला समन्वय समिति
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            (च) पो.ले.महानगरपालिका 
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (उल्लेख गर्ने)', 'id' => '', 'required' => 'required']) }}
                        </label>
                    </div>
                </div>
                <!-- 13 -->
                <div class="form-group col-md-12">
                    <p>
                       १३. तपाईको परिवारले संचालन गरेको व्यवसायिक प्रतिष्ठानको विवरण दिनुहोस ।  
                    </p>
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                  <th rowspan="2">प्रतिष्ठानको किसिम</th>
                                  <th rowspan="2">अनुमानित लगानी रु.</th>
                                  <th colspan="2">संलग्न जनशक्ति</th>
                                  <th rowspan="2">सरदर वार्षिक आम्दानी, रु.हजारमा</th>
                            </tr>
                            <tr>
                                  <th>पुरुष</th>
                                  <th>महिला</th>
                            </tr>
                            <tr>
                                <td>
                                   {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}         
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div> 
                <!-- 14 -->
                <div class="form-group col-md-12">
                    <p>
                      १४. तपाईले संचालन गर्नु भएको प्रतिष्ठान / व्यवसाय / फर्म तथा कम्पनीको लेखा / श्रेस्ता राख्ने गरेको छ ?  
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value=""> 
                        </label>
                    </div>
                </div>
                <!-- 15-->
                <div class="form-group col-md-12">
                    <p>
                       १५. परिवारका कुनै सदस्यले विगत ३ बर्षमा व्यवसायिक सीप तालिम प्राप्त गरेका छन ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                        </label>
                        <br>
                        यदि भए सोको विवरण दिनुहोस् । 
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                  <th>सीप विकासका क्षेत्रहरु</th>
                                  <th>महिला</th>
                                  <th>पुरुष</th>
                            </tr>
                            <tr>
                                <td>
                                   सूचना तथा प्रविधि, इलेक्ट्रीकल र इलेक्ट्रोनिक्स (कम्प्युटर, विद्युत, मोवाईल, रेडियो घडि आदि)          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   सिलाई बुनाई, बुटिक, सृंगार, पार्लर आदि             
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   निर्माण सम्बन्धी (म्यासन, कार्पेन्ट्री आदि)          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   इञ्जिनियरिङ्ग, अटोमोवाइल र मेकानिक्स          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   कृषि सम्बन्धी (जेटी, जेटीए र खाद्य प्रशोधन आदि)          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   जनस्वास्थ्य सम्बन्धी          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   पशुस्वास्थ्य सम्बन्धी          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   वन सम्बन्धी          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   पर्यटन टुर गाइड, ट्राभल र सत्कार          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   कला सम्बन्धी          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अन्य           
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- 16 -->
                <div class="form-group col-md-12">
                    <p>
                       १६. परिवारको वार्षिक आम्दानी र खर्च (वितेको १२ महिनामा आधारित) सम्बन्धी विवरण
                    </p>
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                  <th>आम्दानी विवरण</th>
                                  <th>रकम रु. (हजारमा)</th>
                                  <th>खर्च विवरण</th>
                                  <th>रकम रु. (हजारमा)</th>
                            </tr>
                            <tr>
                                <td>
                                   कृषि तथा पशुपालनबाट          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    खाद्यान्न तथा खाना खर्च
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   व्यापार तथा उद्योगबाट          
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    लत्ता कपडा सम्बन्धी खर्च 
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   नोकरी तथा पेन्सनबाट         
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    शिक्षा सम्बन्धी खर्च
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   वैदेशिक रोजगारबाट         
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    स्वास्थ्य उपचार खर्च
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   ज्याला मजदुरीबाट         
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    चाडपर्व तथा मनोरञ्जन खर्च
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   अन्य         
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <td>
                                    कृषि तथा अन्य उत्पादन खर्च
                                </td>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                   जम्मा                 
                                </th>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                                <th>
                                    जम्मा   
                                </th>
                                <td>
                                    {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'required' => 'required']) }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- 17 -->
                <div class="form-group col-md-12">
                    <p>
                       १७. परिवारका कुनै सदस्यको वैंक तथा वित्तीय संस्थामा खाता छ ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ 
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                        </label>
                    </div>
                </div>
                <!-- 18 -->
                <div class="form-group col-md-12">
                    <p>
                       १८. तपाईको परिवारमा ऋण छ ? यदि छ भने ऋण कहाँबाट लिनुभयो ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                             १) वैंक वा वित्तीय संस्था    
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                            २) सहकारी संस्था
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            २. छैन्
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३) समूह वा सामुदायिक संस्था 
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ४) साहु महाजन
                            <input type="radio" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य', 'id' => '', 'required' => 'required']) }}
                        </label>
                    </div>
                </div>
                <!-- 19 -->
                <div class="form-group col-md-12">
                    <p>
                       १९. तपाईको परिवारको कुनै सदस्य कुनै संघ संस्था तथा राजनैतिक क्रियाकलापमा संलग्न भएका छन् ?
                    </p>
                    <div class="col-md-12">
                        <label class="radio-inline col-md-6">
                            १. छ  
                            <input type="radio" name="radio" value="">
                        </label> 
                        <label class="radio-inline col-md-6">
                           २. छैन  
                            <input type="radio" name="radio" value="">
                        </label>
                        <label>
                            यदि छ भने कुन कुन संघ संस्था तथा राजनैतिक क्रियाकलापमा संलग्न भएका छन् ? (बहुउत्तर सम्भव छ) 
                        </label>
                        <label class="radio-inline col-md-6">
                            १. स्थानीय तह
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                           २. स्थानीय योजना तर्जुमा वा कार्यान्वयन समिति
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ३. स्थानीय स्वास्थ्य संस्था व्यवस्थापन समिति
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ४. विद्यालय व्यवस्थापन समिति
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ५. कृषक समूह 
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ६. वन उपभोक्ता समूह
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ७. सहकारी संस्था
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                           ८. बचत तथा ऋण समूह
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ९. आमा / महिला समूह
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            १०. खानेपानी उपभोक्ता समूह
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            ११. सिंचाई / जल उपभोक्ता समूह
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            १२. आयआर्जन समूह
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            १३. जातिगत समूह
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                            १४. स्थानीय बाल समूह / बालक्लव
                            <input type="checkbox" name="radio" value="">
                        </label>
                        <label class="radio-inline col-md-6">
                             {{ Form::text('', null, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'अन्य (खुलाउने)', 'id' => '', 'required' => 'required']) }}
                        </label>
                    </div>
                </div>

            </div>
            <div class="msgDisplay"></div>
    </div>
</div>