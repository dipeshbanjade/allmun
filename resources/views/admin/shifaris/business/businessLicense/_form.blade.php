<div class="row top-part">
  <div class="col-md-6">
    <p align="left">
      <b class="ps">
        प. सं.:
        @if(isset($data->refCode))
        {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'', 'readonly']) }}
        @else
        {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'']) }}
        @endif
      </b></p>
      <p align="left"><b class="cn">च. नं.:
        {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
      </b></p>
    </div>
    <div class="col-md-6">
      <p align="right"><b class="mt">मिति :{{ Form::text('fiscalYear', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'fiscalYear', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("fiscalYear")']) }}</b></p>
    </div>
  </div>
  <div class="col-md-12">
   <div class="img-box"><p>निवेदकको दुवै कान देखिने पासपोर्ट साइजको फोटो</p></div>
 </div>
 <div class="content-para">
  <p> स्थानीय सरकार सञ्चालन एेन २०७४ को दफा (११)  को उप दफा (२)  को देहाय छ (१०)  बमोजिम प्रचलित कानुनको परिधिभित्र रही निर्माण व्यवसाय सञ्चालन गर्न {{ Form::text('address', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} स्थित कार्यालय भएको प्रो. श्री {{ Form::text('companyName', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }} फर्म वा कम्पनीलाई"घ" वर्गको इजाजतपत्र प्रदान गरीएको छ |
  </p>
  <div class="col-md-9">

  </div>
  <div class="col-md-3">
    <p><b>इजाजतपत्र दिनेको :  </b></p>
    <div id="other_organization_resiter">
      <p align="left">दस्तखत :{{ Form::text('', null, ['class'=>'dashed-input-field']) }}</p>
      <p align="left">नाम:{{ Form::text('authorizedBy', null, ['class'=>'dashed-input-field', 'placeholder'=>'  *', 'required'=>'required']) }}</p>
      <p align="left">पद:
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </p>
      <p align="left">मिति :{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }} </p>
    </div>
  </div>
  <!--views for nibedak detail -->
  <div class="col-md-6">

  </div>
</div>
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')

<!-- END -->
</div>
</div>
</div>






