<script type="text/javascript">
	var btnAddBodartha = $('#add_more_barsik_amdani');
	var clickCount = 1;
	btnAddBodartha.on('click', function(e){
		e.preventDefault();		
		clickCount++;
		var no = clickCount;
		var detail = '<input type="text" class="dashed-input-field" placeholder="  *" name="detail[]" required="required">';
		var areaDetail = '<input type="text" class="dashed-input-small-field" name="areaDetail[]">';
		var monthlyIncome = '<input type="text" class="dashed-input-field" name="monthlyIncome[]">';
		var rate = '<input type="text" class="dashed-input-small-field" name="rate[]">';
		var yearlyIncome = '<input type="text" class="dashed-input-field" name="yearlyIncome[]">';
		var remarks = '<input type="text" class="dashed-input-field" name="remarks[]">';

		var cancelButton = '<a href="" class="btn btn-danger btnRemoveItem"><span class="fa fa-close"></span></a>';

		var insertRow = "<tr class='tblRow'>"
		+"<td>"+no+"</td>"
		+"<td>"+detail+"</td>"
		+"<td>"+areaDetail+"</td>"
		+"<td>"+monthlyIncome+"</td>"
		+"<td>"+rate+"</td>"
		+"<td>"+yearlyIncome+"</td>"
		+"<td>"+remarks+"</td>"
		+"<td class='cancel-btns' style='border: 0px;'>"+cancelButton+"</td>"
		+"</tr>";

		$('#amdani_bibaran_table').append(insertRow);

		$('.btnRemoveItem').on('click', function(e){
			$(this).parent().parent().remove();
			return false;
		});

	});
	$(function(){
		/*add buttoin*/
		$("form[name='frmWardyearlyIncomeCert']").validate({
			rules:{
				refcode: {
					required: true,
					minlength: 3,
					maxlength: 30
				},
				issuedDate : {
					required : true,
					date : true
				},
				chalaniNum : {
					required : true,
					minlength : 1,
					maxlength : 30
				},
				topic : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				personAddress : {
					required : true,
					minlength : 3,
					maxlength : 30
				},
				personDetails : {
					required : true,
					minlength : 5					
				},
				detail : {
					required : true,
					minlength : 1,
					maxlength : 2000
				},
				authorizedPerson : {
					required : true,
					minlength : 3,
					maxlength : 25
				}
			},
			messages: {
				refcode: "Patra Sankhya is required",
				issuedDate : "Issue Date is required",
				chalaniNum : "Chalani Number is required",
				topic : "Topic is required",
				personAddress : "Person Address is required",
				personDetails : "Person Details is required",
				detail : "Details is required",
				authorizedPerson : "Authorizing person is required",
			}
		});
	});

	$('#nebedakId').on('change',function(){
	  nebedakId = $(this).val();
	  if (nebedakId.length >0 ) {
	      var url=  "{{URL::to('/')}}" + '/shifaris/getScanShifarisData/' + nebedakId;
	      $.ajax({
	          'type' : 'GET',
	          'url'  : url,
	          success : function(response){
	              console.log(response);
	                  $("input[name='personAddress']").val(response.fnameNep + ' ' + response.mnameNep+ ' '+ response.lnameNep).trigger('input');
	            
	          },complete:function(){
	          }
	      })
	      .fail(function (response) {
	          alert('data not found ');
	      });
	  }else{

	  }
	});
</script>