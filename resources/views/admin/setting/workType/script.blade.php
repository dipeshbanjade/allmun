<script type="text/javascript">
$(function(){
        /*add buttoin*/
        $("form[name='frmeditworkType']").validate({
            rules:{
                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
                nameEng : "The name of Work Type is required"
            }
        });
    });
$(function(){
        /*add buttoin*/
        $("form[name='frmworkType']").validate({
            rules:{

                nameEng : {
                    required : true,
                    minlength : 3,
                    maxlength : 30
                }
            },
            messages: {
          
                nameEng : "The name of Work Type is required"
            }
        });
	var btnAddWorkType = $('.btnAddWorkType');
	btnAddWorkType.on('click', function(e){
		$('#workTypeModel').modal('show'); 
	});

	var btnEditworkType = $('.btnEditworkType');
	btnEditworkType.on('click', function(e) {
		e.preventDefault();
		var url = $(this).data('url');
		$.ajax({
			'type': 'GET',
			'url': url,
			success: function (response) {
				$('.workTypeId').val(response.id);
				$('.txtRefCode').val(response.refCode).attr('disabled',true);
				$('.txtNameNep').val(response.nameNep);
				$('.txtNameEng').val(response.nameEng);
				$("#editworkTypeModel").modal('show');
			}
		})
	});

	var frmUpdateworkType = $('#frmUpdateworkType');
  frmUpdateworkType.on('submit', function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var id   = $('.workTypeId').val();
    var url  = "{{URL::to('/')}}" + "/setting/workType/"+id+"/update";
    $.ajax({
        'type' : 'POST',
        'url'  : url,
        'data'    : data,
        success : function(response){
            console.log(response);
            if (response.success==true) {
                $('.infoDiv').append(response.message).addClass('alert alert-success');
            }
            if (response.success==false) {
                $.each(response.message, function(key, value){
                    $('.infoDiv').append(value).addClass('alert alert-danger');
                })
            }
        },complete:function(){
            $("#editworkTypeModel").modal('hide');
            setTimeout(location.reload.bind(location), 1500);
        }
    })
    .fail(function (response) {
        alert('data already exists');
    });
    $('#editworkTypeModel').modal('hide');
});
});
</script>
