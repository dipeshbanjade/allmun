<?php

namespace App\Http\Requests\admin\ward;

use Illuminate\Foundation\Http\FormRequest;

class wardDetailsValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'nameNep'         => 'required|min:3|max:60',
            'nameEng'         => 'required|min:3|max:60|unique:wards',
            'landLineNumber'  => 'required|max:10|unique:wards',
            'addrNep'         => 'required|min:3|max:100',
            'addrEng'         => 'required|min:3|max:100',
            'provinces_id'    => 'required',
            'districts_id'    => 'required'
        ];
    }
}
