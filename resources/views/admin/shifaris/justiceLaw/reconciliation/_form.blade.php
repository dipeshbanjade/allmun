<?php $checker = getMunicipalityData(); ?>
<div class="row top-part">
  <div class="col-md-6 ps-cn">
    <p><b class="ps">प. सं.:</b>
      @if(isset($data->refCode))
      {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field patra', 'placeholder'=>'', 'readonly']) }}
      @else
      {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field patra', 'placeholder'=>'']) }}
      @endif
    </p>
    <p align="left" class="star">
      <b class="cn">च. नं.:</b>
      {{ Form::text('chalaniNum', null, ['class'=>'dashed-input-field patra', 'placeholder'=>'  *', 'required'=>'required', 'id' => 'chalaniNum']) }}
    </p>
  </div>
  <div class="col-md-6">
    <p align="right" class="star"><b class="mt">मिति :</b>{{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'  *', 'id' => 'issuedDate', 'required'=>'required', 'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}</p>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <p align="center"><h4 align="center"><b class="sub">विषय: मिलापत्र सिफारिस</b></h4></p>
    {{ Form::hidden('letter_subject', 'छात्रवृत्ति सिफारिस') }}
  </div>
</div>

<div class="col-md-12">
  <p align="left">
   लिखितम हामी तपसिलका मानिसहरु आगे हामीहरु बीच यस<br>
   <b>
    <?php echo isset($data->municipalityName) ? $data->municipalityName : $checker->nameNep; ?>
  </b> 
  महा/उप/नगर/गाउँ-पालिका वडा न.
  कार्यालयमा 
  {{ Form::text('caseName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'caseName', 'required' => 'required']) }}
  सम्बन्धमा मुद्दा परेकोमा 
  {{ Form::text('badiAnswer', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'badiAnswer', 'required' => 'required']) }} 
  भन्ने व्यहोराको बादीको उजुरी निवेदन।
  {{ Form::text('pratiBadiAnswer', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'pratiBadiAnswer', 'required' => 'required']) }} 
  को लिखित जबाफ।अब उप्रान्त झगडा नगरी एकआपसमा मिली बसौँ भनी सल्लाह भई  हामी दुवै वादी र प्रतिउत्तर जिकिर छाडी अब उप्रान्त यसै विषय लाई कहिँकतै कुनै व्यहोराको उजुर बाजुर नगरी एक आपसमा असल सम्बन्ध कायम गरी शान्ति र सौहार्दपूर्ण रूपमा बस्न सहमत भई मिलापत्र बमोजिम भएन भन्ने बाहेक यसै विषय मा पुन: कहिँकतै व्यहोराको उजुर बाजुर नगर्ने कुरामा सहमत भई मिलापत्रको मतलब परिणाम समेत जानी बुझी राजीखुशी साथ यस कार्यालयको रोहबरमा मुलुकी एन अदालती बन्दोबस्तको  
  {{ Form::text('dafaNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'dafaNumber', 'required' => 'required']) }} 
  न. बमोजिम मिलापत्र गरी  पाँऊ।<br>
</p><h4 align="center"><b> संयुक्त दरखास्तवाला</b></h4>
<h5 align="center"> 
  <b> बादी</b>
  {{ Form::text('badi', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'badi', 'required' => 'required']) }}।
</h5><br>
<h5 align="center">
  <b> प्रतिवादी</b>
  {{ Form::text('pratiBadi', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'pratiBadi', 'required' => 'required']) }}।
</h5><br>
<h5 align="center">
  <b> रोहबर</b>
  {{ Form::text('rohabaarName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'rohabaarName', 'required' => 'required']) }}।
</h5><br>
ईति सम्बत् २० 
{{ Form::text('year', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'year', 'required' => 'required']) }}
साल
{{ Form::text('month', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'month', 'required' => 'required']) }}
महिना 
{{ Form::text('day', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'day', 'required' => 'required']) }} 
गते रोज
{{ Form::text('eachday', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'eachday', 'required' => 'required']) }}
शुभम.......
<!--views for nibedak detail -->
<div class="clearfix"></div>
<hr>
@include('admin.shifaris.nibedakCommonField')
<!-- END -->
<div class="col-md-12">
  <hr>
</div>
</div>