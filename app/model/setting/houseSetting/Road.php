<?php

namespace App\model\setting\houseSetting;

use Illuminate\Database\Eloquent\Model;

class Road extends Model
{
	protected $table = 'roads';
	protected $fillable = ['refCode','nameEng','nameNep','status','softDelete'];
}
