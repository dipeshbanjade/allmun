<?php

namespace App\Http\Requests\admin\setting\roomFloor;

use Illuminate\Foundation\Http\FormRequest;

class RoomFloorTypeValidation extends FormRequest
{
 
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nameEng' => 'required|min:4|max:50'
        ];
    }
}