<?php

namespace App\Http\Controllers\admin\setting\houseSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\houseSetting\WhyNot;
use App\model\mylogic;
use App\Http\Requests\admin\setting\whyNot\WhyNotVal;
use Validator;

class whyNotController extends Controller
{
    protected $viewPath  = 'admin.setting.whyNot';
    protected $whyNot = 'why_nots';
    protected $prefix;

    public function __construct()
    {
         $this->middleware('auth');
         $this->prefix = config('activityMessage.whyNotPrefix');
    }

    public function index()
    {
        $page['page_title'] = 'Why Not';
        $page['page_description'] = 'List of all Why Not';
        $allwhyNot= WhyNot::where(['softDelete'=>0])->paginate(config('activityMessage.pagination'));
         return view($this->viewPath . '.index', compact(['page', 'allwhyNot', 'prefix']));
    }

   
    public function store(Request $request)
    {
        $prefix   = myLogic::sysRefCode($tablename = $this->whyNot, $prefix = $this->prefix);
          $mySave = WhyNot::create([
             'refCode' => $prefix,
             'nameNep' => $request->nameNep ?? 'N/A',
             'nameEng' => $request->nameEng,
             'tag' => $request->tag,
             'status'  => 0,
             'softDelete' => 0
         ]);
           if ($mySave) {
              return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
          }else{
              return back()->withMessage(config('activityMessage.unSaveMessage'));
          }
    }


    
    public function edit($id)
    {
        $data = WhyNot::findOrFail($id);
         if ($data) {
            return response()->json([
              'success' => true,
              'refCode' => $data->refCode,
              'id'      => $data->id,
              'nameNep' => $data->nameNep,
              'nameEng' => $data->nameEng,
              'tag' => $data->tag,
              ]);

        }else{
            return response()->json([
              'success' => false,
              'message' => config('activityMessage.dataNotFound')
              ]);
        }

    }

   
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
         'nameEng'  => 'required|min:4|max:200',
         'nameNep'  => 'min:4|max:200',
         'tag'  => 'required',
        ]);

        if ($validator->fails()) {
          return response()->json([
           'success' => false,
           'message' => $validator->errors()->all()
            ]);
        }
      $update          = WhyNot::findOrFail($id);
      $update->nameNep = $request->nameNep ?? 'N/A';
      $update->nameEng = $request->nameEng;
      $update->tag = $request->tag;

      $myUpdate = $update->update();
      if ($myUpdate) {
        return response()->json([
          'success' => true,
          'message' => config('activityMessage.updateMessage')
        ]);
        }else{
            return response()->json([
              'success'  => false,
              'message'  => config('activityMessage.notUpdated')
          ]);
        }
    }

  
    public function destroy($id)
    {
       $del = WhyNot::findOrFail($id);
       $del->softDelete = 1;
       $myDel = $del->update();
       if ($myDel) {
           return MyLogic::successChangeStatus($code = 200);
       }else{
          return MyLogic::successChangeStatus($code = 301);
       }
    }
}
