<div class="form-row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
            <div class="row color_red">
            
                <div class="form-group col-md-6">{{-- 9 --}}
                        <p>
                            ९. तपाईको परिवारले प्रयोग गर्ने बत्तीको मुख्य स्रोत के हो ?
                        </p><br>
                        @if (isset($lightSource) && count($lightSource)>0)
                            @foreach ($lightSource as $lSource)
                                <label class="radio-inline">
                                    <input type="radio" class="rdoClickMe" title="double click for reset" name="lightSrc" value="{{$lSource->nameNep}}" {{ isset($hsLightData) && $hsLightData->lightSrc == $lSource->nameNep ? 'checked="checked"' : ''  }}>
                                    {{$lSource->nameNep}}
                                </label> 
                                <br />
                            @endforeach
                        @else
                            <label>Insert Data from setting</label>
                        @endif
                </div>
        
                <div class="form-group col-md-6">{{-- 10 --}}
                        <p>
                            १०. तपाईको परिवारले वत्ती बाल्न बिजुलीको प्रयोग गर्नुभएको छैन भने किन प्रयोग नगर्नु भएको हो ?
                        </p><br>
                            @if (isset($whyNotLight) && count($whyNotLight)>0)
                                @foreach ($whyNotLight as $wnLight)
                                <label class="radio-inline">
                                   <input type="radio" class="rdoClickMe" title="double click for reset" name="whyNotReasonNam" value="{{$wnLight->nameNep}}" {{ isset($hsLightData) && $hsLightData->whyNotReasonNam == $wnLight->nameNep ? 'checked="checked"' : ''  }}>
                                   {{$wnLight->nameNep}}
                                </label> 
                                <br />
                                @endforeach
                            @else
                                <label>Insert data from the setting</label>
                            @endif
                            
                            {{ Form::text('otherWhyNotReasonNam', isset($hsLightData->otherWhyNotReasonNam) ? $hsLightData->otherWhyNotReasonNam : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'others']) }}
                        </label>
                            <br>
                </div>
            
            </div>
            <div class="msgDisplay"></div>
    </div>
</div>