<div class="right_col nep" role="main" style="min-height: 882px;">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3 class="title-x"> गैर नाफामुलुक संस्था दर्ता</h3>
          <div class="clearfix"></div>
        </div>
        <div class="x_content col-md-12" id="block">
          @include('admin.shifaris.municipalityDetail')
          <div class="row top-part">
            <div class="col-md-6">
              <p align="left"> 
                <b>@lang('commonField.extra.refCode'):</b>
                @if(isset($data->refCode))
                {{ Form::text('refCode', $data->refCode ?? '', ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'readonly']) }}
                @else
                {{ Form::text('refCode', isset($refCode) ? $refCode : '', ['class'=>'dashed-input-field', 'placeholder'=>'    *']) }}
                @endif
              </p>
              <p align="left">
                <b class="cn">च. नं.:
                  {{ Form::text('chalaniNumber', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'chalaniNumber']) }}

                </b>
              </p>
            </div>
            <div class="col-md-6">
              <p align="right"><b class="mt">मिति : </b>
                {{ Form::text('issuedDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'issuedDate',  'onfocus' => 'showNdpCalendarBox("issuedDate")']) }}
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p align="center" class="font-size-24">
              </p><h4 align="center"><b>विषय: गैर नाफामूलक संस्था दर्ता प्रमाण पत्र।</b></h4>
              {{ Form::hidden('letter_subject', 'गैर नाफामूलक संस्था दर्ता प्रमाण पत्र', ['class'=>'dashed-input-field', 'placeholder'=>'    *', ]) }}

            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
             <p align="left">दर्ता नं. :
               {{ Form::text('dartaCode', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'required' => 'required']) }}
             </p>

             <p align="left">दर्ता मिति :
               {{ Form::text('dartaDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'dartaDate',  'onfocus' => 'showNdpCalendarBox("dartaDate")']) }}
             </p>
             
           </div>
           <div class="col-md-6">
            <div class="img-box"><p>संस्थाको छाप वा फोटो </p></div>
          </div>
        </div>
        <div class="margin-top"></div>
        <div class="row">
          <div class="col-md-6 letter-margin">
            <p align="left"><span class="no"> १) </span>संस्थाको नाम :
              {{ Form::text('companyName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyName', 'required' => 'required']) }}
            </p>
            
            <p align="left">ठेगाना :
             {{ Form::text('companyAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyAddress', 'required' => 'required']) }}
           </p>
           <p align="left">विषयगत  क्षेत्र  :
            {{ Form::text('subjectArea', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'subjectArea', 'required' => 'required']) }}
          </p>
          <p align="left">संस्थाको कारोवार शुरु भएको मिति :
           {{ Form::text('businessStartDate', null, ['class'=>'dashed-input-field ndp-nepali-calendar date-input-field', 'placeholder'=>'    *', 'required'=>'required', 'id' => 'businessStartDate',  'onfocus' => 'showNdpCalendarBox("businessStartDate")']) }}
         </p>

         <p align="left">ई-मेल :
           <span class="eng">
             {{ Form::text('companyEmail', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyEmail', 'required' => 'required']) }}
           </span>
         </p>
         <p align="left">सम्पर्क फोन नं. :
          {{ Form::text('companyContact', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'companyContact', 'required' => 'required']) }}
        </p>
        <p align="left">
          <span class="no"> २)</span>सञ्चालक/अध्यक्ष/मुख्य व्यक्तिको नाम, थर :
          {{ Form::text('operatorName', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'operatorName', 'required' => 'required']) }}
        </p>
        <p align="left">ठेगाना :
          {{ Form::text('operatorAddress', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'operatorAddress', 'required' => 'required']) }}
        </p>
        <p align="left">ई-मेल :
          <span class="eng">
            {{ Form::text('operatorEmail', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'operatorEmail', 'required' => 'required']) }}
          </span>
        </p>
        <p align="left">सम्पर्क फोन नं. :
          {{ Form::text('operatorContact', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'operatorContact', 'required' => 'required']) }}
        </p>
      </div>
      <div class="col-md-6">
      </div>
    </div>
    <div class="text-right btm-last">
      <p>
        {{ Form::text('authorizedPerson', null, ['class'=>'dashed-input-field', 'placeholder'=>'    *', 'id' => 'authorizedPerson', 'required' => 'required']) }}
      </p>             
      <p>  
        <select onchange="changeSelect(this)" name="deginations_id" id="deginations_id" class="">            
          @if(count($deginationsId) > 0)
          @foreach($deginationsId as $deg)
          <option value="{{ $deg->id }}" {{ !empty($data->deginations_id) && $data->deginations_id == $deg->id ? 'selected' : '' }}>
            {{ $deg->nameNep }} 
          </option>
          @endforeach
          @endif
        </select>
      </p>
    </div>
    <!--views for nibedak detail -->
    <div class="clearfix"></div>
    <hr />
    @include('admin.shifaris.nibedakCommonField')
    <!-- END -->
  </div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }
</script>