<div class="form-row">
<div class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
<div class="row color_red">
    <div class="form-group col-md-12">{{-- 12 --}}
        <p>
            १. परिवारमा रहेका पाँच बर्ष भन्दा मुनिका बालबालिकाको स्वास्थ्य सेवा सम्बन्धी विवरण                     
        </p>
        <br>
        <table border="1">
            <thead>
                <tr>
                    <th>सि.न.</th>
                    <th>बालक वा बालिकाको नाम</th>
                    <th>उमेर</th>
                    <th>जुकाको औषधी खाएको पटक</th>
                    <th>पोलियो थोपा</th>
                    <th>भिटामिन ए खाएको पटक</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $count = 0;
                $hsChildHealthCareData = isset($hsCHC->hsChildHealthCare) ? json_decode($hsCHC->hsChildHealthCare) : '';
                //Note $getMyFamilyDetails from houseDetailsController
                foreach($getMyFamilyDetails as $member){
                    $memberInfo = getParentsName($member->relation_id);
                    $memberAge = getAge($memberInfo['dobAD']);
                    if( isset($memberAge) && $memberAge < 5){                        
                        $count++; 
                        $singleChildHealthCare = moreHouseDetailsFormUpdate($member->relation_id, $hsChildHealthCareData, $compareJsonKey = 'childId');
                        //dd($memberInfo);
                        //dd($singleChildHealthCare);
                        ?>
                        <tr>                                 
                            <td>
                                <input type="hidden" name="hsChildHealthCare[<?php echo $count;  ?>][childId]" value="{{ $member->relation_id }}">                              
                                {{ $count }}                                   
                            </td>
                            <td>
                                {{ Form::text("hsChildHealthCare[$count][childName]",
                                    $memberInfo['fnameNep'], ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '', 'readonly' => '']) }}
                                </td>
                                <td>
                                    {{ Form::text("hsChildHealthCare[$count][age]", $memberAge, ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>' ', 'id' => '', 'readonly' => '']) }}
                                </td>
                                <td>
                                    {{ Form::number("hsChildHealthCare[$count][hkWrmMedIntakeNum]", isset($singleChildHealthCare['hkWrmMedIntakeNum']) ? $singleChildHealthCare['hkWrmMedIntakeNum'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::number("hsChildHealthCare[$count][polioIntake]", isset($singleChildHealthCare['polioIntake']) ? $singleChildHealthCare['polioIntake'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                </td>
                                <td>
                                    {{ Form::number("hsChildHealthCare[$count][vAintakeNum]", isset($singleChildHealthCare['vAintakeNum']) ? $singleChildHealthCare['vAintakeNum'] : '', ['class'=>'mdl-textfield__input dashed-input-field', 'placeholder'=>'', 'id' => '']) }}
                                </td>
                            </tr>         
                            <?php }} ?>                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="msgDisplay"></div>
    </div>