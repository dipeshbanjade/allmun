<?php

namespace App\Http\Controllers\admin\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\setting\sysLanguage;
use App\model\mylogic;
use App\Http\Requests\admin\setting\Language\LanguageValidation;
use Validator;
class sysLanguageController extends Controller
{
    protected $viewPath  = 'admin.setting.language';
    protected $languagesTbl = 'sys_languages';
    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix = config('activityMessage.languagePrefix');
    }
    public function index()
    {
        $page['page_title']       = 'Language';
        $page['page_description'] = 'list of all Language';

        $allLanguages= sysLanguage::where(['softDelete' => 0])->paginate(config('activityMessage.pagination'));
        $prefix   = myLogic::sysRefCode($tablename = $this->languagesTbl, $prefix = $this->prefix);
        return view($this->viewPath . '.index', compact(['page', 'allLanguages', 'prefix'])); 
    }

    public function store(LanguageValidation $request)
    {
     $mySave = sysLanguage::create([
       'refCode' => $request->refCode,
       'langNep' => $request->langNep ?? 'N/A',
       'langEng' => $request->langEng,
       'status'  => 0,
       'softDelete' => 0
       ]);
     if ($mySave) {
        return back()->withMessage(config('activityMessage.saveMessage') . $request->refCode);
    }else{
        return back()->withMessage(config('activityMessage.unSaveMessage'));
    }
}


public function edit($id)
{
    $data = sysLanguage::findOrFail($id);
    if ($data) {
     return response()->json([
       'success' => true,
       'refCode' => $data->refCode,
       'id'      => $data->id,
       'langNep' => $data->langNep,
       'langEng' => $data->langEng
       ]);
 }else{
     return response()->json([
       'success' => false,
       'message' => config('activityMessage.dataNotFound')
       ]);
 }
}

public function update(Request $request, $id)
{
  $validator = Validator::make($request->all(), [
     'langEng'  => 'required|min:4|max:50',
     'langNep'  => 'min:4|max:50',
     ]);

  if ($validator->fails()) {
      return response()->json([
       'success' => false,
       'message' => $validator->errors()->all()
       ]);
  }
  $update          = sysLanguage::findOrFail($id);
  $update->langNep = $request->langNep ?? 'N/A';
  $update->langEng = $request->langEng;

  $myUpdate = $update->update();
  if ($myUpdate) {
    return response()->json([
      'success' => true,
      'message' => config('activityMessage.updateMessage')
      ]);
}else{
    return response()->json([
      'success'  => false,
      'message'  => config('activityMessage.notUpdated')
      ]);
}
}


public function destroy($id)
{
    $del = sysLanguage::findOrFail($id);
    $del->softDelete = 1;
    $myDel = $del->update();
    if ($myDel) {
       return MyLogic::successChangeStatus($code = 200);
   }else{
      return MyLogic::successChangeStatus($code = 301);
  }
}
}
