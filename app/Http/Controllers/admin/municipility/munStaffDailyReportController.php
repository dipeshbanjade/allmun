<?php

namespace App\Http\Controllers\admin\municipility;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\model\admin\municipility\MunStaffDailyReport;

use App\model\mylogic;

use App\Http\Requests\admin\shifaris\instituteRecommendationVal;
use DB;
use Auth;


class munStaffDailyReportController extends Controller
{
    protected $viewPath = 'admin.municipility.staff.dailyReport';
    protected $table     = 'mun_staff_daily_reports';
    protected $appUsrTable     = 'applicant_users';
    protected $deg             = 'deginations';     
    protected $prefix;

    public function __construct()
    {
        $this->middleware('auth');
        $this->prefix      = config('activityMessage.prefixMunStaffDailyReport');

    }

    public function indexStaff(){
        $page['page_title']       = 'Municipality Staff Daily Report';
        $page['page_description'] = 'Municipality Staff Daily Report';

        $staffDailyReport = MunStaffDailyReport::orderby('created_at', 'DESC')->where(['softDelete'=>0, 'staff_municipilities_id' => Auth::user()->id])->paginate(config('activityMessage.pagination'));

        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
            return view($this->viewPath . '.indexStaff', compact(['page', 'allStaff']));
        }else{
            return back()->withMessage(config('activityMessage.unAuthorized'));
        }
    }

    public function adminViewDailyReport(){
     $page['page_title']       = 'Municipality Staff Daily Report';
     $page['page_description'] = 'Municipality Staff Daily Report';

     $allStaff = MunStaffDailyReport::orderby('created_at', 'DESC')->where(['softDelete'=>0])->paginate(config('activityMessage.pagination'));

     if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
        return view($this->viewPath . '.adminViewDailyReport', compact(['page', 'allStaff']));
    }else{
        return back()->withMessage(config('activityMessage.unAuthorized'));
    }
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['page_title']       = 'Municipality Staff Daily Report';
        $page['page_description'] = 'Municipality Staff Daily Report';

        //check if municipality exists or not
        $munExists = myLogic::getMunicipalityDetails();
        

        //check user 
        if (Auth::user()->userLevel === 'dev' || Auth::user()->userLevel === 'mun') {
            return view($this->viewPath . '.create', compact(['page', 'staffTypeId', 'deginationId', 'provincesId', 'districtId', 'refCode', 'roles']));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}