<?php

namespace App\Http\Requests\admin\shifaris;

use Illuminate\Foundation\Http\FormRequest;

class propertyValVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            //
            'refCode' => 'required|min:3|unique:non_profit_org_registrations',
            'issuedDate' => 'required',
            'title' => 'required',
            'personName' => 'required',
            'municipality' => 'required',
            'ward' => 'required',
            'district' => 'required',
            'former' => 'required',
            'orgAddr' => 'required',
            'orgType' => 'required',
            'orgWard' => 'required',
            'totalPropertyValueInNrs' => 'required',
            'perDollarPriceNpr' => 'required',
            'ctype' => 'required',
            'currencyType' => 'required',
            'totalPropertyValueInDollar' => 'required',
            'authorizedPerson' => 'required'
        ];
    }
}
