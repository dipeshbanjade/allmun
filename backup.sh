#!/bin/bash

#--------------------------------------------------#
# path where file will be backed-up. 			  ##	
# backup Drive location  						   #
backup_path="/media/emunicipality/ward_!3_backup/"

#test location
#backup_path="/var/www/test_backup/"

#--------------------------------------------------#
## directory on Drive where file will be located. ##
backup_dir=$(date +'YEAR-%Y/MONTH-%m/DAY-%d')

backup_dir_public="PUBLIC_BACKUP"
#--------------------------------------------------#
## Give uniqname to file. Y-m-d-H-M ##
unique_filename=$(date +'%Y-%m-%d')

#------------------CREATING BACKUP DIRECTORY--------------------------------#
## Give uniqname to file. ##
{ # try
    cd ${backup_path}
    #save your output

} || { # catch
    # save log for exception 
    exit 1
}
mkdir -p "$backup_dir"
mkdir -p "$backup_dir_public"

#Testing 
#echo "$backup_path$backup_dir"

#------------------SQL DATABASE BACKUP--------------------------------#
#------------------SQL DATABASE BACKUP--------------------------------#
#------------------SQL DATABASE BACKUP--------------------------------#
## Sql Dump of  database with file name backup.sql ##
mysqldump -udbadmin -pp@ssw0rd pkrMun > "$backup_path$backup_dir"/backup.sql
 
#--------------------------------------------------#
## compress and zip backup.sql -> compress to file with extension .gz ##
gzip -k -S  "$unique_filename"_sql_backup.gz "$backup_path$backup_dir"/backup.sql


#--------------------------------------------------#
## change directory to backup location
## compress and zip backup.sql -> compress to file with extension .tar.gz ##
cd "$backup_path$backup_dir" && tar -czvf "$unique_filename"_sql_backup.tar.gz backup.sql
## compress and zip backup.sql -> compress to file with extension .zip ##
zip "$unique_filename"_sql_backup.zip backup.sql

#------------------PUBLIC FOLDER BACKUP--------------------------------#
#------------------PUBLIC FOLDER BACKUP--------------------------------#
#------------------PUBLIC FOLDER BACKUP--------------------------------#
## Sql Dump of municipality database with file name backup.sql ##

#--------------------------------------------------#
## PROJECT DIRECTORY PATH ##
path_to_project='/var/www/municipality'
cp -r -u "$path_to_project"/public "$backup_path$backup_dir_public"/


cd "$backup_path$backup_dir_public"
rm *.tar.gz
rm *.zip

tar -czvf "$unique_filename"_PUBLIC.tar.gz public

zip -r "$unique_filename"_PUBLIC.zip public

