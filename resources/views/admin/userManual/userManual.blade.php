
    <div class="container">
        <div class="accordian" id="accordianExample1">
            <div class="card">
                <div class="card-header" id="headingOne1">
                    <h3 class="mb-0 alert alert-danger text-dark">
                        <button class="btn btn-link btn-lg btn-block text-dark" type="button" data-toggle="collapse"
                            data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1">
                            <h3 class="mb-0">Municipality Guidelines</h3>
                        </button>
                    </h3>
                </div>
                <div id="collapseOne1" class="collapse" aria-labelledby="headingOne1" data-parent="#accordionExample1">
                    <div class="card-body">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            Process of Opening the Software Municipality
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionExample">
                                    <div class="card-body row">
                                        <ul class="col col-5">
                                            <li>Open the browser</li>
                                            <li> Type url: 'municipality.malikadraftserver.com'</li>
                                            <li>Type username and password in the given field</li>
                                            <li>Click on Login</li>
                                        </ul>
                                        <div class="col col-sm-7 pull-right mt-0"><a href="img/1-big.jpg"><img src="img/login.png"
                                                alt="Login img" width="550px;" height="300px;"></a></div>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                            View of the Dashboard
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div><img src="img/dashboard.png" alt="dashboard img" width="900px;"
                                                height="350px;"></div>
                                        <ul>
                                            <li class="list-group-item" role="alert">In left side there is Municipality
                                                name and below it there are menu items/
                                                links as below:

                                            <li><u>Municipality</u>: By clicking on municipality
                                                you can see municipality
                                                details and you can edit it as well.</li>
                                            <li><u>Municipality Admin</u>: Different options are
                                                under municipality admin as
                                                below:
                                                <ul>
                                                    <li><u>Staff</u>: By clicking on staff you can see staff details and
                                                        create staff option. </li>
                                                    <li><u>Create Staff</u></li>
                                                    <li><u>Hajiri register</u></li>
                                                    <li><u>Leave</u></li>
                                                    <li><u>Daily Report</u></li>
                                                    <li><u>Field Visit</u></li>
                                                    <li><u>aadhikarik bida</u></li>
                                                </ul>
                                            </li>
                                            </li>
                                            <li><u>Ward:</u>Different options are under the ward
                                                as below:
                                                <ul>
                                                    <li><u>Ward</u> </li>
                                                    <li><u>Create Ward</u></li>
                                                    <li><u>Ward Admin</u></li>
                                                    <li><u>Create Ward Admin</u></li>

                                                </ul>
                                            </li>
                                            <li><u>citizen:</u>By clicking on citizen you can
                                                create and view citizens</li>
                                            <li><u>Backtigat Ghar Bibaran:</u>We can view and
                                                create the byactigat ghar
                                                bibaran(Individual House Detail)</li>
                                            <li><u>Darta Chalani:</u>We can view and create
                                                Darta and Chalani</li>
                                            <li><u>Shipharis: </u>We can create different types
                                                of sipharis and view those
                                                shipharis</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo2">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="false"
                                            aria-controls="collapseTwo2">
                                            Process of Editing Municipality Information
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo2"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="row">
                                            <img src="img/municipality.png" alt="dashboard img" width="520px;"
                                                height="350px;">
                                            <img src="img/editMun.png" alt="dashboard img" width="520px;"
                                                height="350px;">
                                        </div>
                                        <div class="col col-12 mt-10"></div>
                                        <ul>
                                            <li>Click on Municipality in menu</li>
                                            <li>Click on Municipality</li>
                                            <li>Click on edit button</li>
                                            <li>Fill the fields with required data you want.</li>
                                            <li>Click on Update button</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                            Process of Creating Staff of Municipality
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on Municipality Admin</li>
                                            <li>Click on Create staff</li>
                                            <li>Fill all the fileds by clicking on Profile option
                                                under create staff
                                                form.</li>
                                            <li>Click on Create button</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right">
                                            <img src="img/createStaff.png" alt="dashboard img" width="650px;"
                                                height="350px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapseFour" aria-expanded="false"
                                        aria-controls="collapseFour">
                                        Process of viewing the Hajiri register of the staff
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                data-parent="#accordionExample">
                                <div class="card-body row">

                                    <ul class="col col-4">
                                        <li>Click on Municipality Admin</li>
                                        <li>Click on Hajiri register</li>
                                        <li>Fill the Start Date field by selecting the date from where you want to
                                            view the hajiri of the staff
                                        </li>
                                        <li>Fill the End Date field by selecting the date upto where you want to
                                            view the hajiri of the staff
                                        </li>
                                        <li>Fill the staff field by selecting the staff</li>
                                        <li>If you want to download the Attendance result, Tick on with Download</li>
                                        <li>Click on Search button</li>

                                    </ul>
                                    <div class="col col-sm-8 pull-right">
                                        <img src="img/viewHajiri.png" alt="dashboard img" width="650px;"
                                            height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFive">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapseFive" aria-expanded="false"
                                        aria-controls="collapseFive">
                                        Process of taking Leave
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                data-parent="#accordionExample">
                                <div class="card-body row">

                                    <ul>
                                        <li>Click on username on top right corner</li>
                                        <li>Click on Leave</li>
                                        <li>Clik on Create Leave</li>
                                        <li>Fill the all fields in the form </li>
                                        <li>Click on Create button</li>
                                    </ul>
                                    <div class="col col-sm-8 pull-right">
                                        <img src="img/leave.png" alt="dashboard img" width="700px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSix">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapseSix" aria-expanded="false"
                                        aria-controls="collapseSix">
                                        Process of Viewing and Approving the Pending Leave
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">
                                        <li>Click on Municipality Admin</li>
                                        <li>Click on Leave</li>
                                        <li>The list of Pending and Appoved leave of staff,<br>&nbsp;&nbsp;&nbsp;
                                            is on Screen</li>
                                        <li>If you want to view the Particular Staff,<br>&nbsp;&nbsp;&nbsp;
                                            type name of Staff on Search Box</li>
                                        <li>Click on Pending Button of the staff you want to Approve</li>
                                        <li>Click on Create button</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/viewLeave.png" alt="dashboard img" width="650px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSeven">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false"
                                        aria-controls="collapseSeven">
                                        Process of inserting Daily Report
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                data-parent="#accordionExample">
                                <div class="card-body row">

                                    <ul class="col col-4">
                                        <li>Click on Daily Report icon on the top right side</li>
                                        <li>Click on +Daily Report</li>
                                        <li>Fill all the fields in the form</li>
                                        <li>Click on Create button</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/createDailyReport.png" alt="dashboard img" width="650px;"
                                            height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingEight">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapseEight" aria-expanded="false"
                                        aria-controls="collapseEight">
                                        Process of Creating Field Visit
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                data-parent="#accordionExample">
                                <div class="card-body row">

                                    <ul class="col col-4">
                                        <li>Click on Field Visit icon on the top right side</li>
                                        <li>Click on +Field Visit</li>
                                        <li>Fill all the fields in the form</li>
                                        <li>Click on Create button</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/createFieldVisit.png" alt="dashboard img" width="650px;"
                                            height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                                <div class="card-header" id="headingEight+">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseEight+" aria-expanded="false"
                                            aria-controls="collapseEight+">
                                            Process of Creating Aadhicaric(आधिकारिक) Bidha(विधा)
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseEight+" class="collapse" aria-labelledby="headingEight+"
                                    data-parent="#accordionExample">
                                    <div class="card-body row">

                                        <ul class="col col-4">
                                            <li>Click on Ward Management</li>
                                            <li>Click on Aadhikaric(आधिकारिक) Bidha(विधा)</li>
                                            <li>Click on +Create</li>
                                            <li>Fill the form with the correct details.</li>
                                            <li>Click on Create button</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/aadhiric.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                        <div class="card">
                            <div class="card-header" id="headingNine">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapseNine" aria-expanded="false"
                                        aria-controls="collapseNine">
                                        Process of Creating Ward of the Municipality
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                                data-parent="#accordionExample">
                                <div class="card-body row">

                                    <ul class="col col-4">
                                        <li>Click on Ward</li>
                                        <li>Click on Create Ward</li>
                                        <li>Fill all the fileds in the form</li>
                                        <li>Click on Create button</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/createWard.png" alt="Create Ward img" width="650px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTen">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapseTen" aria-expanded="false"
                                        aria-controls="collapseTen">
                                        Process of Creating Ward Admin of the Municipality
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTen" class="collapse" aria-labelledby="headingTen"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">
                                        <li>Click on Ward</li>
                                        <li>Click on Create Ward Admin</li>
                                        <li>Fill all the fileds in the form</li>
                                        <li>Click on Create button</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/createWardAdmin.png" alt="Create Ward img" width="650px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading11">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapse11" aria-expanded="false"
                                        aria-controls="collapse11">
                                        Process of Creating citizen
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="heading11"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">
                                        <li>Click on citizen</li>
                                        <li>Click on Create Citizen</li>
                                        <li>Fill all the fileds in the form</li>
                                        <li>Click on Create button</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/createCitizen.png" alt="Create Ward img" width="650px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="heading13">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapse13" aria-expanded="false"
                                        aria-controls="collapse13">
                                        Process of creating Byactigat Ghar Bibaran
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="heading13"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">
                                        <li>Click on Backtigat Ghar Bibaran</li>
                                        <li>Click on Backtigat Ghar Bibaran Sirjana Garnuhos</li>
                                        <li>Fill all fields of the form using correct information</li>
                                        <li>Click on create button</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/createByactigat.png" alt="Create Ward img" width="650px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading14">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapse14" aria-expanded="false"
                                        aria-controls="collapse14">
                                        Process of viewing and editing Backtigat Ghar Bibaran
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="heading14"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">

                                        <li>Click on Backtigat Ghar Bibaran in menu</li>
                                        <li>Click on Backtigat Ghar Bibaran</li>
                                        <li>There is the list of the Ghar Details of that particular House owner.
                                        </li>
                                        <li>Click on the required action icon you want in the Action list</li>
                                        <li>If you want to edit, delete, add more details and change gharmuli you
                                            can click on the required icon
                                            respectively.</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/viewByactigat.png" alt="Create Ward img" width="650px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading15">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapse15" aria-expanded="false"
                                        aria-controls="collapse15">
                                        Process of creating and viewing Darta
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse15" class="collapse" aria-labelledby="heading15"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">
                                        <li>Click on Darta Chalani in menu</li>
                                        <li>Click on Darta</li>
                                        <li>There is the list of Darta information.</li>
                                        <li>If you want to Create Darta, Click on +Create Darta button</li>
                                        <li>Fill all the fields in the form</li>
                                        <li>Click on create</li>
                                    </ul>
                                    <div class="col col-sm-8 pull-right">
                                            <img src="img/createChalani.png" alt="Create Ward img" width="335px;" height="350px;">
                                            <img src="img/viewChalani.png" alt="Create Ward img" width="335px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="heading16">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapse16" aria-expanded="false"
                                        aria-controls="collapse16">
                                        Process of creating and viewing Chalani
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse16" class="collapse" aria-labelledby="heading16"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">
                                        <li>Click on Darta Chalani in menu</li>
                                        <li>Click on Chalani</li>
                                        <li>There is the list of Chalani info.</li>
                                        <li>Type Chalani Number in search field.</li>
                                        <li>The particular Chalani is on your list</li>
                                        <li>If you want to Create Chalani, Click on +Create Chalani button</li>
                                        <li>Fill all the fields in the form</li>
                                        <li>Click on create</li>
                                    </ul>
                                     <div class="col col-sm-8 pull-right">
                                            <img src="img/createDarta.png" alt="Create Ward img" width="335px;" height="350px;">
                                            <img src="img/viewDarta.png" alt="Create Ward img" width="335px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading17">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapse17" aria-expanded="false"
                                        aria-controls="collapse17">
                                        Process of creating Shifarish
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse17" class="collapse" aria-labelledby="heading17"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">
                                        <li>Click on Shifarish in menu</li>
                                        <li>Click on Shifarish</li>
                                        <li>Click on required type of Shifarish you want to create</li>
                                        <li>Fill all the fields in the form</li>
                                        <li>Click on Save/Print</li>
                                    </ul>
                                    <div class="col col-sm-8 pull-right">
                                        <img src="img/createShifarish.png" alt="Create Ward img" width="335px;" height="350px;">
                                        <img src="img/createShifarishForm.png" alt="Create Ward img" width="335px;" height="350px;">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading18">
                                <h5 class="mb-0">
                                    <button class="btn alert alert-danger text-dark text-dark" type="button"
                                        data-toggle="collapse" data-target="#collapse18" aria-expanded="false"
                                        aria-controls="collapse18">
                                        Process of Viewing Shifarish
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse18" class="collapse" aria-labelledby="heading18"
                                data-parent="#accordionExample">
                                <div class="card-body row">
                                    <ul class="col col-4">
                                        <li>Click on Shifarish in menu</li>
                                        <li>Click on Shifarish Khoj</li>
                                        <li>There is the list of Shifarish information.</li>
                                        <li>For the particular Shifarish, fill the given fields by correct information
                                            and
                                            click on Search button.</li>
                                        <li>The particular Shifarish will be on screen.</li>
                                    </ul>
                                    <div class="col col-sm-7 pull-right">
                                        <img src="img/viewShifarish.png" alt="Create Ward img" width="650px;" height="350px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ward-->
            <div class="card">
                <div class="card-header" id="headingOne11" data-parent="#accordionExample1">
                    <h3 class="mb-0 alert alert-danger text-dark">
                        <button class="btn btn-link btn-lg btn-block text-dark" type="button" data-toggle="collapse"
                            data-target="#collapseOne11" aria-expanded="true" aria-controls="collapseOne11">
                            <h3 class="mb-0">Ward Guidelines</h3>
                        </button>
                    </h3>
                </div>
                <div id="collapseOne11" class="collapse" aria-labelledby="headingOne11"
                    data-parent="#accordionExample1">
                    <div class="card-body">
                        <div class="accordion" id="accordionExampleI">
                            <div class="card">
                                <div class="card-header" id="headingOneI">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseOneI" aria-expanded="true"
                                            aria-controls="collapseOne1I">
                                            Process of Opening the Software Ward
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOneI" class="collapse show" aria-labelledby="headingOneI"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Open the browser</li>
                                            <li> Type url: 'municipality.malikadraftserver.com'</li>
                                            <li>Type username and password in the given field</li>
                                            <li>Click on Login</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><a href="img/1-big.jpg"><img src="img/login.png"
                                                alt="Login img" width="550px;" height="300px;"></a>
                                            </div>

                                    </div>
                                    </div>
                                </div>

                            <div class="card">
                                <div class="card-header" id="headingTwo22">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseTwo22" aria-expanded="false"
                                            aria-controls="collapseTwo22">
                                            View of the Dashboard
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTwo22" class="collapse" aria-labelledby="headingTwo22"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body">
                                        <div><img src="img/ward.png" alt="dashboard img" width="900px;" height="350px;">
                                        </div>
                                        <ul>
                                            <li class="list-group-item" role="alert">In left side there is
                                                Ward
                                                name and below it there are menu/
                                                links as given below:
                                            <li><u>Ward Management</u>: Different options are
                                                under Ward Management as
                                                below:
                                                <ul>
                                                    <li><u>Staff</u>: By clicking on staff you can see staff
                                                        details</li>
                                                    <li><u>Create Staff</u></li>
                                                    <li><u>Hajiri register</u></li>
                                                    <li><u>Staff Leave</u></li>
                                                    <li><u>Attendance</u></li>
                                                    <li><u>Daily Report</u></li>
                                                    <li><u>Field Visit</u></li>
                                                    <li><u>Office</u></li>
                                                    <li><u>aadhikarik bida</u></li>
                                                </ul>
                                            </li>
                                            </li>
                                            <li><u>citizen:</u>By clicking on citizen you can
                                                create and view citizens</li>
                                            <li><u>Backtigat Ghar Bibaran:</u>We can view and
                                                create the backtigat ghar
                                                bibaran</li>
                                            <li><u>Darta Chalani:</u>We can view and create
                                                Darta and Chalani</li>
                                            <li><u>Shipharis: </u>We can create different types
                                                of sipharis and view those
                                                shipharis</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree3">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseThree3" aria-expanded="false"
                                            aria-controls="collapseThree3">
                                            Process of Creating Staff of Ward
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseThree3" class="collapse" aria-labelledby="headingThree3"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on Ward Management</li>
                                            <li>Click on Create staff</li>
                                            <li>Fill all the fileds by clicking on Profile option under create staff
                                                form.</li>
                                            <li>Click on Create button</li>
                                        </ul>
                                         <div class="col col-sm-8 pull-right mt-0"><img src="img/wCreateStaff.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>

                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingFour4">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseFour4" aria-expanded="false"
                                            aria-controls="collapseFour4">
                                            Process of viewing the Hajiri register of the staff
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFour4" class="collapse" aria-labelledby="headingFour4"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">

                                        <ul class="col col-4">
                                            <li>Click on Ward Management</li>
                                            <li>Click on Hajiri register</li>
                                            <li>Fill the Start Date field by selecting the date from where you want to
                                                view the hajiri of the staff
                                            </li>
                                            <li>Fill the End Date field by selecting the date upto where you want to
                                                view the hajiri of the staff
                                            </li>
                                            <li>Fill the staff field by selecting the staff</li>
                                            <li>If you want to download the Attendance result, Tick on with Download
                                            </li>
                                            <li>Click on Search button</li>

                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wViewHajiri.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFive5">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseFive5" aria-expanded="false"
                                            aria-controls="collapseFive5">
                                            Process of taking Leave
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFive5" class="collapse" aria-labelledby="headingFive5"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">

                                        <ul class="col col-4">
                                            <li>Click on username on top right corner</li>
                                            <li>Click on Leave</li>
                                            <li>Clik on Create Leave</li>
                                            <li>Fill the all fields in the form </li>
                                            <li>Click on Create button</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wCreateLeave.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSix6">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseSix6" aria-expanded="false"
                                            aria-controls="collapseSix6">
                                            Process of Viewing and Approving the Pending Leave
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSix6" class="collapse" aria-labelledby="headingSix6"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">

                                        <ul class="col col-4">
                                            <li>Click on Ward Management</li>
                                            <li>Click on Leave</li>
                                            <li>Leave list is on Screen</li>
                                            <li>Type name of staff in Search</li>
                                            <li>That Particular staff is displayed on List</li>
                                            <li>Click on Pending Button of the staff you want to Approve</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wViewLeave.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSeven7">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseSeven7" aria-expanded="false"
                                            aria-controls="collapseSeven7">
                                            Process of Creating Company/Office
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSeven7" class="collapse" aria-labelledby="headingSeven7"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">

                                        <ul class="col col-4">
                                            <li>Click on Ward Management</li>
                                            <li>Click on Office</li>
                                            <li>Click on +Create Company</li>
                                            <li>Fill the form with the correct details.</li>
                                            <li>Click on Create button</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wCreateOffice.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSeven_7">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseSeven_7" aria-expanded="false"
                                            aria-controls="collapseSeven_7">
                                            Process of Viewing Company/Office
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSeven_7" class="collapse" aria-labelledby="headingSeven_7"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">

                                        <ul class="col col-4">
                                            <li>Click on Ward Management</li>
                                            <li>Click on Office</li>
                                            <li>There is the list of Office displayed on the list.</li>
                                            <li>Type the name of the particular Office.</li>
                                            <li>That Particular list is on your Screen.</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wViewOffice.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEight8">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseEight8" aria-expanded="false"
                                            aria-controls="collapseEight8">
                                            Process of Creating Aadhicaric(आधिकारिक) Bidha(विधा)
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseEight8" class="collapse" aria-labelledby="headingEight8"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">

                                        <ul class="col col-4">
                                            <li>Click on Ward Management</li>
                                            <li>Click on Aadhikaric(आधिकारिक) Bidha(विधा)</li>
                                            <li>Click on +Create</li>
                                            <li>Fill the form with the correct details.</li>
                                            <li>Click on Create button</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wAadhiric.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingNine9">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseNine9" aria-expanded="false"
                                            aria-controls="collapseNine9">
                                            Process of Creating citizen
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseNine9" class="collapse" aria-labelledby="headingNine9"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on citizen</li>
                                            <li>Click on Create Citizen</li>
                                            <li>Fill all the fileds in the form</li>
                                            <li>Click on Create button</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wCreateCitizen.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTen10">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseTen10" aria-expanded="false"
                                            aria-controls="collapseTen10">
                                            Process of viewing and editing citizens inforamtion
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTen10" class="collapse" aria-labelledby="headingTen10"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>In the left side of the </li>
                                            <li>Click on citizen</li>
                                            <li>There is the list of the citizens of that particular ward.</li>
                                            <li>Click on the required action icon you want in the Action list</li>
                                            <li>If you want to print card, edit and delete you can click on the required
                                                icon.</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wViewCitizen.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEleven">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false"
                                            aria-controls="collapseEleven">
                                            Process of creating Byactigat Ghar Bibaran
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on Byactigat Ghar Bibaran</li>
                                            <li>Click on Byactigat Ghar Bibaran Sirjana Garnuhos</li>
                                            <li>Fill all fields of the form using correct information</li>
                                            <li>Click on create button</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wCreateByactigat.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card">
                                <div class="card-header" id="headingTwelve">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false"
                                            aria-controls="collapseTwelve">
                                            Process of viewing and editing Backtigat Ghar Bibaran
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">

                                            <li>Click on Backtigat Ghar Bibaran in menu</li>
                                            <li>Click on Backtigat Ghar Bibaran</li>
                                            <li>There is the list of the Ghar Details of House owners.</li>
                                            <li>Type the House Number in Search field</li>
                                            <li>That particular House Detail is displayed on list.</li>
                                            <li>Click on the required action icon you want in the Action list</li>
                                            <li>If you want to edit, delete, add more details and change gharmuli you
                                                can click on the required icon
                                                respectively.</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wViewByactigat.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEleven+">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseEleven+" aria-expanded="false"
                                            aria-controls="collapseEleven+">
                                            Process of Adding More details on House
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseEleven+" class="collapse" aria-labelledby="headingEleven+"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on Byactigat Ghar Bibaran in Menu</li>
                                            <li>Click on Byactigat Ghar Bibaran</li>
                                            <li>Click on (Add More Detail on House)icon under Action List of the House Owner</li>
                                            <li>Click on the House Detail you want to Add</li>
                                            <li>Fill the Form</li>
                                            <li>Click on create</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wMoreHouseDetails.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThirteen">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false"
                                            aria-controls="collapseThirteen">
                                            Process of creating and viewing Darta
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on Darta Chalani in menu</li>
                                            <li>Click on Darta</li>
                                            <li>There is the list of Darta information.</li>
                                            <li>If you want to Create Darta, Click on +Create Darta button</li>
                                            <li>Fill all the fields in the form</li>
                                            <li>Click on create</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wDarta.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header" id="headingFourteen">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseFourteen" aria-expanded="false"
                                            aria-controls="collapseFourteen">
                                            Process of creating and viewing Chalani
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on Darta Chalani in menu</li>
                                            <li>Click on Chalani</li>
                                            <li>There is the list of Chalani information.</li>
                                            <li>Type Chalani Number in search field.</li>
                                            <li>The particular Chalani is on your list</li>
                                            <li>If you want to Create Chalani, Click on +Create Chalani button</li>
                                            <li>Fill all the fields in the form</li>
                                            <li>Click on create</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wChalani.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFifteen">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseFifteen" aria-expanded="false"
                                            aria-controls="collapseFifteen">
                                            Process of creating Shifarish
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on Shifarish in menu</li>
                                            <li>Click on Shifarish</li>
                                            <li>Click on required type of Shifarish you want to create</li>
                                            <li>Fill all the fields in the form</li>
                                            <li>Click on Save/Print</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wCreateShifarish.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSixteen">
                                    <h5 class="mb-0">
                                        <button class="btn alert alert-danger text-dark text-dark" type="button"
                                            data-toggle="collapse" data-target="#collapseSixteen" aria-expanded="false"
                                            aria-controls="collapseSixteen">
                                            Process of Viewing Shifarish
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSixteen" class="collapse" aria-labelledby="headingSixteen"
                                    data-parent="#accordionExampleI">
                                    <div class="card-body row">
                                        <ul class="col col-4">
                                            <li>Click on Shifarish in menu</li>
                                            <li>Click on Shifarish Khoj</li>
                                            <li>There is the list of Shifarish information.</li>
                                            <li>For the particular Shifarish, fill the given fields by correct
                                                information and
                                                click on Search button.</li>
                                            <li>The particular Shifarish will be on screen.</li>
                                        </ul>
                                        <div class="col col-sm-8 pull-right mt-0"><img src="img/wViewShifarish.png"
                                                alt="Login img" width="650px;" height="300px;">
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
   