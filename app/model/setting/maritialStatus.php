<?php

namespace App\model\setting;

use Illuminate\Database\Eloquent\Model;

class maritialStatus extends Model
{
    protected $table = 'maritial_statuses';
    protected $fillable = ['nameNep', 'nameEng', 'refCode', 'status', 'softDelete'];
}
